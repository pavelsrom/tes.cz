/**
 * Localization strings for the UI Multiselect widget
 *
 * @locale en, en-US
 */

$.extend($.ui.multiselect.locale, {
	addAll:'Přidat všechny',
	removeAll:'Odebrat všechny',
	itemsCount:'#{count} položek vybráno',
	itemsTotal:'#{count} položek celkem',
	busy:'čekejte prosím...',
	errorDataFormat:"Nelze přidat položky, neznámý formát dat",
	errorInsertNode:"Byl problém přidat položku:\n\n\t[#{key}] => #{value}\n\nOperace byla přerušena",
	errorReadonly:"Položka #{option} je jen pro čtení",
	errorRequest:"Problém se vzdáleným volání skriptu (Type: #{status})"
});