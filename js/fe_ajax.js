/* AJAXOVE FUNKCE PRO AJAX PRO FRONT END */



$(function(){
    
    var AJAX_GATEWAY = "php/ajax/ajax.web.gateway.php?lang=cz&action=";
    
    /*
     $( "#search .text, #hledany-vyraz" ).autocomplete({
        delay: 500,
        minLength: 3,
        source: AJAX_GATEWAY + "finder.autocomplete",
        select: function(event, ui){
            location.href = ui.item.url;
        }
        
     }).autocomplete( "instance" )._renderItem = function( ul, item ) {
        return $( "<li>" )
        .append( "<div class='finder-result'><strong>" + item.label + "</strong><p>" + item.desc + "</p></div>" )
        .appendTo( ul );
     };
    */
    
    
    
    
    
    //sledovani prokliku editboxu
    $('.sledovat_prokliky').on('click',function(){
        var id = $(this).attr('id');
        id = id.substr(1,id.length);
        
        $.post(AJAX_GATEWAY + 'set_editbox_click',{id: id});
        
        })
   
})


// slowEach() plugin by "Michael Geary"
		// http://groups.google.com/group/jquery-en/browse_thread/thread/1790787be89b92bc/876c4ba8f01d8443#msg_72c18cfa10b7d7b6
		// callback2 - fired after last element, can be used to build functions chain (added by Orkan)
		jQuery.fn.slowEach = function(interval, callback, callback2) {
			var items = this, i = 0;
			if(!items.length) return;
			function next() {
				(callback.call(items[i], i, items[i]) !== false && ++i < items.length) ? setTimeout(next, interval) : callback2 && callback2.call(items, i, items);
			}
			next();
		};
		
		