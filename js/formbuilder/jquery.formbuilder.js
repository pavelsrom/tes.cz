/**
 * jQuery Form Builder Plugin
 * Copyright (c) 2010 Pavel Srom
 */
 
 

(function($){
	$.fn.formbuilder = function(options) {
		
		// Extend the configuration options with user-provided
		var defaults = {
			save_url: false,
			load_url: false,
            disabled: false,
            disabled_delete: false,
            id_form: 0            
		};
		var opts = $.extend(defaults, options);
		
		return this.each(function() {
			var ul_obj 		= this;
			var field 		= '';
			var field_type 	= '';
			var last_id 	= 1;
			
			$(ul_obj).html('');

			// load existing form data
			if(opts.load_url){
				$.ajax({
					type: "GET",
					url: opts.load_url,
					success: function(xml){
						
						var values 		= '';
						var options 	= false;
						var required 	= false;
			
						$(xml).find('field').each(function(){
							// checkbox type
							if($(this).attr('type') == 'checkbox'){
								options 	= new Array;
								options[0] 	= $(this).attr('title');
								values 		= new Array;
								$(this).find('checkbox').each(function(a){
									values[a] 	 = new Array(2);
									values[a][0] = $(this).text();
									values[a][1] = $(this).attr('checked');
								});
							}
							// radio type
							else if($(this).attr('type') == 'radio'){
								options 	= new Array;
								options[0] 	= $(this).attr('title');
								values 		= new Array;
								$(this).find('radio').each(function(a){
									values[a] 	 = new Array(2);
									values[a][0] = $(this).text();
									values[a][1] = $(this).attr('checked');
								});
							}
							// select type
							else if($(this).attr('type') == 'select'){
								options 	= new Array;
								options[0]  = $(this).attr('title');
								//options[1]  = $(this).attr('multiple');
								values 		= new Array;
								$(this).find('option').each(function(a){
									values[a] = new Array(2);
									values[a][0] = $(this).text();
									values[a][1] = $(this).attr('checked');
								});
							}
							else {
								values = $(this).text();
							}
							
							appendNewField( $(this).attr('type'), values, options, $(this).attr('required'), $(this).attr('id') );
							
						});
					}
				});
			}
			

			// set the form save action
			$(this).after($('<div class="dolni-tlacitka"><button id="save-form" type="submit" name="submit" class="tool_button save_button">Uložit</button></div>').click(function(){
				save();
                $('#message').html("<div class='ok w45'><ul><li>Uloženo</li></ul></div>");
                $.scrollTo($('body'), 400);
                
				return false;
			}));
			
			// Create form control select box and add into the editor
			var select = '';
			select += '<option value="0" title="../img/selectbox-icon/plus.gif">Přidat novou položku...</option>';
            select += '<optgroup label="Obecné prvky">';
            select += '<option value="label" title="../img/selectbox-icon/label.gif">Nadpisek</option>';
            select += '<option value="popis" title="../img/selectbox-icon/desc.gif">Popisek</option>';
			select += '<option value="input_text" title="../img/selectbox-icon/input.gif">Malé textové pole</option>';
			select += '<option value="textarea" title="../img/selectbox-icon/textarea.gif">Velké textové pole</option>';
			select += '<option value="checkbox" title="../img/selectbox-icon/checkbox.gif">Zaškrtávací políčka</option>';
			select += '<option value="radio" title="../img/selectbox-icon/radio.gif">Přepínače</option>';
			select += '<option value="select" title="../img/selectbox-icon/select.gif">Výběrové pole</option>';
            select += '</optgroup>';
            select += '<optgroup label="Konkrétní prvky">';
            select += '<option value="jmeno" title="../img/selectbox-icon/jmeno.gif">Jméno a příjmení</option>';
            select += '<option value="adresa" title="../img/selectbox-icon/adresa.gif">Adresa</option>';
            select += '<option value="email" title="../img/selectbox-icon/email.gif">Email</option>';
            select += '<option value="telefon" title="../img/selectbox-icon/telefon.gif">Telefon</option>';
            select += '<option value="ico" title="../img/selectbox-icon/icodic.gif">Ičo</option>';
            select += '<option value="dic" title="../img/selectbox-icon/icodic.gif">Dič</option>';
            select += '<option value="icq" title="../img/selectbox-icon/icq.gif">ICQ</option>';
            select += '<option value="url" title="../img/selectbox-icon/url.gif">Webová adresa</option>';
            select += '<option value="datum" title="../img/selectbox-icon/datum.gif">Datum</option>';
            select += '<option value="cas" title="../img/selectbox-icon/cas.gif">Čas</option>';
            select += '<option value="int" title="../img/selectbox-icon/cislo.gif">Celé číslo</option>';
            select += '<option value="float" title="../img/selectbox-icon/cislo.gif">Desetinné číslo</option>';
            select += '</optgroup>';
            select += '<optgroup label="Hotové formuláře">';
            select += '<option value="kontaktni_formular1" title="../img/selectbox-icon/formular.gif">Jednoduchý kontaktní formulář</option>';
            select += '<option value="kontaktni_formular2" title="../img/selectbox-icon/formular.gif">Rozšířený kontaktní formulář</option>';
            select += '<option value="vas_dotaz_formular" title="../img/selectbox-icon/formular.gif">Formulář pro dotaz</option>';
            select += '</optgroup>';
            
            if(opts.disabled==false){
			     $(this).before('<select style="width: 300px" name="field_control_top" class="field_control" id="field_control">'+select+'</select>');
                //$(this).after('<select style="width: 250px" name="field_control_bot" id="field_control_bot" class="field_control">'+select+'</select>');
                }
            
            var zobrazit = false;
                 
			$('.field_control').change(function(){
			    zobrazit = true; 
				appendNewField($(this).val());
				$(this).val(0).blur();
				$.scrollTo($('#frm-'+(last_id-1)+'-item'), 800);
				return false;
			});	
			
			// Wrapper for adding a new field
            
            var id_active_item = 0;
            
			var appendNewField = function(type, values, options, required, id_item){
				
				field = '';
				field_type = type;
                
                
                if(id_item=='' || id_item=='undefined' || id_item == null)
                    id_active_item = 0;
                    else
                    id_active_item = id_item;
				
				if(typeof(values) == 'undefined'){ values = ''; }
				
				switch(type){
					case 'input_text':
						appendTextInput(values, required);
						break;
					case 'textarea':
						appendTextarea(values, required);
						break;
					case 'checkbox':
						appendCheckboxGroup(values, options, required);
						break;
					case 'radio':
						appendRadioGroup(values, options, required);
						break;
					case 'select':
						appendSelectList(values, options, required);
						break;
                    case 'label':
						appendLabel(values, required);
						break;
                    case 'popis':
						appendPopis(values, required);
						break;
                    case 'datum':
						appendDatum(values, required);
						break;
                    case 'cas':
						appendCas(values, required);
						break;
                    case 'url':
						appendUrl(values, required);
						break;
                    case 'email':
						appendEmail(values, required);
						break;
                    case 'telefon':
						appendTelefon(values, required);
						break;
                    case 'int':
						appendInt(values, required);
						break;
                    case 'float':
						appendFloat(values, required);
						break;
                    case 'icq':
						appendIcq(values, required);
						break;
                    case 'ico':
						appendIco(values, required);
						break;
                    case 'dic':
						appendDic(values, required);
						break;
                    case 'jmeno':
                        appendNewField('input_text','Jméno',null,'true',null);
                        appendNewField('input_text','Příjmení',null,'true',null);
						break;
                    case 'adresa':
                        appendNewField('input_text','Ulice',null,'true',null);
                        appendNewField('input_text','Město',null,'true',null);
                        appendNewField('input_text','Psč',null,'true',null);
						break;
                    // prednastavene formulare
                     case 'kontaktni_formular1':
                        appendNewField('email','Váš email',null,'true',null);
                        appendNewField('textarea','Text',null,'true',null);
						break;   
                     case 'kontaktni_formular2':
                        appendNewField('input_text','Jméno a příjmení',null,'true',null);
                        appendNewField('email','Email',null,'true',null);
                        appendNewField('input_text','Telefon',null,'true',null);
                        appendNewField('textarea','Text',null,'true',null);
						break;
                    case 'vas_dotaz_formular':
                        appendNewField('input_text','Jméno a příjmení',null,'true',null);
                        appendNewField('email','Email',null,'true',null);
                        appendNewField('textarea','Dotaz',null,'true',null);
						break;
      
				}
			}
			
            var sysClass = "input";
            
        
            var appendIcq = function(values, required){
                if(values=='') values='Icq';
                field += '<label>Název:</label>';
				field += '<input class="fld-icq '+sysClass+'" id="icq-'+last_id+'" type="text" value="'+values+'" />';
                field += '<input type="hidden" name="id-'+last_id+'" value="'+id_active_item+'"/>';
				help = '';
				icon = "<img src='../img/selectbox-icon/icq.gif' alt=''/>";
                
                var legend = 'Icq';
                var titulek= values;
				appendFieldLi(legend,titulek, field, required,help, icon);
            }
            
            var appendIco = function(values, required){
                if(values=='') values='Ičo';
                field += '<label>Název:</label>';
				field += '<input class="fld-ico '+sysClass+'" id="ico-'+last_id+'" type="text" value="'+values+'" />';
                field += '<input type="hidden" name="id-'+last_id+'" value="'+id_active_item+'"/>';
				help = '';
				icon = "<img src='../img/selectbox-icon/icodic.gif' alt=''/>";
                
                var legend = 'Ičo';
                var titulek= values;
				appendFieldLi(legend,titulek, field, required,help, icon);
            }
            
            var appendDic = function(values, required){
                if(values=='') values='Dič';
                field += '<label>Název:</label>';
				field += '<input class="fld-dic '+sysClass+'" id="dic-'+last_id+'" type="text" value="'+values+'" />';
                field += '<input type="hidden" name="id-'+last_id+'" value="'+id_active_item+'"/>';
				help = '';
				icon = "<img src='../img/selectbox-icon/icodic.gif' alt=''/>";
                
                var legend = 'Dič';
                var titulek= values;
				appendFieldLi(legend,titulek, field, required,help, icon);
            }
            
            var appendLabel = function(values){
				if(values=='') values='Nadpisek formuláře';
				field += '<label>Název:</label>';
				field += '<input class="fld-label '+sysClass+'" id="label-'+last_id+'" type="text" value="'+values+'" />';
                field += '<input type="hidden" name="id-'+last_id+'" value="'+id_active_item+'"/>';
                
				help = '';
				icon = "<img src='../img/selectbox-icon/label.gif' alt='' />";
                
                var legend = 'Nadpisek';
                var titulek=values;
				appendFieldSpecialLi(legend,titulek, field, icon);
				
			}
            
            var appendPopis = function(values){
				if(values=='') values='Popisek formuláře, dlouhý text...';
				field += '<label>Text:</label>';
				field += '<textarea class="wysiwygEditor fld-popis '+sysClass+'" id="popis-'+last_id+'" cols="" rows="">'+values+'</textarea>';
                field += '<input type="hidden" name="id-'+last_id+'" value="'+id_active_item+'"/>';
                field += '<a href="javascript:get_tiny_mce_editor();">Zapni editor</a>';
				help = '';
				icon = "<img src='../img/selectbox-icon/desc.gif' alt='' />";
                
                var legend = 'Popisek';
                var titulek= values;
				appendFieldSpecialLi(legend,titulek, field, icon);
				
			}

			var appendDatum = function(values, required){
				if(values=='') values='Datum';
				field += '<label>Název:</label>';
				field += '<input class="fld-datum '+sysClass+'" id="datum-'+last_id+'" type="text" value="'+values+'"/>';
                field += '<input type="hidden" name="id-'+last_id+'" value="'+id_active_item+'"/>';
                
				help = '';
				icon = "<img src='../img/selectbox-icon/datum.gif' alt='' />";
                var legend = 'Datum';
                var titulek= values;
                
				appendFieldLi(legend,titulek, field, required, help, icon);
				
			}
            
			var appendCas = function(values, required){
				if(values=='') values='Čas';
				field += '<label>Název:</label>';
				field += '<input class="fld-cas '+sysClass+'" id="cas-'+last_id+'" type="text" value="'+values+'"/>';
                field += '<input type="hidden" name="id-'+last_id+'" value="'+id_active_item+'"/>';
                
				help = '';
				icon = "<img src='../img/selectbox-icon/cas.gif' alt='' />";
                
                var legend = 'Čas';
                var titulek=values;
				appendFieldLi(legend,titulek, field, required, help, icon);
				
			}
            
            var appendUrl = function(values, required){
				if(values=='') values='Webová adresa';
				field += '<label>Název:</label>';
				field += '<input class="fld-url '+sysClass+'" id="url-'+last_id+'" type="text" value="'+values+'"/>';
                field += '<input type="hidden" name="id-'+last_id+'" value="'+id_active_item+'"/>';
				help = '';
				icon = "<img src='../img/selectbox-icon/url.gif' alt='' />";
                
                var legend = 'Webová adresa';
                var titulek=values;
				appendFieldLi(legend,titulek, field, required, help, icon);
				
			}
            
            var appendEmail = function(values, required){
				if(values=='') values='Email';
				field += '<label>Název:</label>';
				field += '<input class="fld-email '+sysClass+'" id="email-'+last_id+'" type="text" value="'+values+'" />';
                field += '<input type="hidden" name="id-'+last_id+'" value="'+id_active_item+'"/>';
                
				help = '';
				icon = "<img src='../img/selectbox-icon/email.gif' alt='' />";
                
                var legend = 'Emailová adresa';
                var titulek=values;
				appendFieldLi(legend,titulek, field, required, help, icon);
				
			}
            
            var appendTelefon = function(values, required){
				if(values=='') values='Telefon';
				field += '<label>Název:</label>';
				field += '<input class="fld-telefon '+sysClass+'" id="telefon-'+last_id+'" type="text" value="'+values+'" />';
                field += '<input type="hidden" name="id-'+last_id+'" value="'+id_active_item+'"/>';
                
				help = '';
				icon = "<img src='../img/selectbox-icon/telefon.gif' alt='' />";
                
                var legend = 'Telefon';
                var titulek= values;
				appendFieldLi(legend,titulek, field, required, help, icon);
				
			}
            
            var appendInt = function(values, required){
				
				field += '<label>Název:</label>';
				field += '<input class="fld-int '+sysClass+'" id="int-'+last_id+'" type="text" value="'+values+'" />';
                field += '<input type="hidden" name="id-'+last_id+'" value="'+id_active_item+'"/>';
                
				help = '';
				icon = "<img src='../img/selectbox-icon/cislo.gif' alt='' />";
                
                var legend = 'Celé číslo';
                var titulek=values;
				appendFieldLi(legend,titulek, field, required, help, icon);
				
			}
            
            var appendFloat = function(values, required){
				
				field += '<label>Název:</label>';
				field += '<input class="fld-float '+sysClass+'" id="float-'+last_id+'" type="text" value="'+values+'" />';
                field += '<input type="hidden" name="id-'+last_id+'" value="'+id_active_item+'"/>';
                
				help = '';
				icon = "<img src='../img/selectbox-icon/cislo.gif' alt='' />";
                
                var legend = 'Desetinné číslo';
                var titulek=values;
				appendFieldLi(legend,titulek, field, required, help, icon);
				
			}
            
			// single line input type="text"
			var appendTextInput = function(values, required){
				if(values=='') values='';
				field += '<label>Název:</label>';
				field += '<input class="fld-title '+sysClass+'" id="title-'+last_id+'" type="text" value="'+values+'" />';
                field += '<input type="hidden" name="id-'+last_id+'" value="'+id_active_item+'"/>';
                
				help = '';
				icon = "<img src='../img/selectbox-icon/input.gif' alt='' />";
                
                var legend = 'Malé textové pole';
                var titulek=values;
				appendFieldLi(legend,titulek, field, required, help, icon);
				
			}
			
			// multi-line textarea
			var appendTextarea = function(values, required){
				if(values=='') values='Poznámka';
				field += '<label>Název:</label>';
				field += '<input type="text" class=" '+sysClass+'" value="'+values+'" />';
                field += '<input type="hidden" name="id-'+last_id+'" value="'+id_active_item+'"/>';
                
				help = '';
				icon = "<img src='../img/selectbox-icon/textarea.gif' alt='' />";
                
                var legend = 'Velké textové pole';
                var titulek=values;
				appendFieldLi(legend,titulek, field, required, help, icon);
				
			}
			
			// adds a checkbox element
			var appendCheckboxGroup = function(values, options, required){
				
				var title = '';
				if(typeof(options) == 'object'){
					title = options[0];
				}

				field += '<div class="chk_group">';
				field += '<div class="frm-fld"><label>Název:</label>';
				field += '<input type="text" name="title" value="'+title+'" class=" '+sysClass+'"/></div>';
                field += '<input type="hidden" name="id-'+last_id+'" value="'+id_active_item+'"/>';
                
				field += '<div class="false-label">Volby:</div>';
				field += '<div class="fields">';
	
				if(typeof(values) == 'object'){
					for(c in values){
						field += checkboxFieldHtml(values[c]);
					}
				} else {
					field += checkboxFieldHtml('');
				}
				
				field += '<div class="add-area"><a href="#" class="add add_ck"><img src="../img/icon_plus.gif" alt="Přidat" title="Přidat volbu"/></a></div>';
				field += '</div>';
				field += '</div>';
				
				help = '';
                icon = "<img src='../img/selectbox-icon/checkbox.gif' alt='' />";
                
                var legend = 'Zaškrtávací políčka';
                var titulek=title;
                
				appendFieldLi(legend,titulek, field, required, help, icon);
				
				$('.add_ck').live('click', function(){
					$(this).parent().before( checkboxFieldHtml() );
					return false;
				});
			}
			
			// Checkbox field html, since there may be multiple
			var checkboxFieldHtml = function(values){
				
				var checked = false;
				
				if(typeof(values) == 'object'){
					var value = values[0];
					checked = values[1] == 'false' ? false : true;
				} else {
					var value = '';
				}
				
				field = '';
				field += '<div>';
				field += '<input type="checkbox"'+(checked ? ' checked="checked"' : '')+' /><input type="text" value="'+value+'" class=" '+sysClass+'" />';
                if(!opts.disabled_delete){
				    field += '<a href="#" class="remove" title="Opravdu chcete smazat volbu?"><img src="../img/icon_minus.gif" alt="Odebrat" title="Odebrat volbu"/></a>';
                    }
				field += '</div>';
				
				return field;
				
			}
			
			// adds a radio element
			var appendRadioGroup = function(values, options, required){
				
				var title = '';
				if(typeof(options) == 'object'){ title = options[0]; }

				field += '<div class="rd_group">';
				field += '<div class="frm-fld"><label>Název:</label>';
				field += '<input type="text" name="title" value="'+title+'" class="'+sysClass+'"/></div>';
                field += '<input type="hidden" name="id-'+last_id+'" value="'+id_active_item+'"/>';
                
				field += '<div class="false-label">Volby:</div>';
				field += '<div class="fields">';
	
				if(typeof(values) == 'object'){
					for(c in values){
						field += radioFieldHtml(values[c], 'frm-'+last_id+'-fld');
					}
				} else {
					field += radioFieldHtml('', 'frm-'+last_id+'-fld');
				}
				
				field += '<div class="add-area"><a href="#" class="add add_rd"><img src="../img/icon_plus.gif" alt="Přidat"  title="Přidat volbu" /></a></div>';
				field += '</div>';
				field += '</div>';
				help = '';
				icon = "<img src='../img/selectbox-icon/radio.gif' alt='' />";
                
                var legend = 'Přepínací tlačítka'
                var titulek=title;
				appendFieldLi(legend,titulek, field, required, help, icon);
				
				$('.add_rd').live('click', function(){
					$(this).parent().before( radioFieldHtml(false, $(this).parents('.frm-holder').attr('id')) );
					return false;
				});
			}

			// Radio field html, since there may be multiple
			var radioFieldHtml = function(values, name){
				
				var checked = false;
              
                
				if(typeof(values) == 'object'){
					var value = values[0];
					checked = values[1] == 'false' ? false : true;
				} else {
					var value = '';
				}
				
				field = '';
				field += '<div>';
				field += '<input type="radio"'+(checked ? ' checked="checked"' : '')+' name="radio_'+name+'" /><input type="text" value="'+value+'" class=" '+sysClass+'"/>';
                
                if(!opts.disabled_delete)
				    field += '<a href="#" class="remove" title="Opravdu chcete smazat volbu?"><img src="../img/icon_minus.gif" alt="Odebrat" title="Odebrat volbu"/></a>';
                    
				field += '</div>';
				
				return field;
				
			}
			
			// adds a select/option element
			var appendSelectList = function(values, options, required){
				
				var multiple = false;
				var title = '';
				if(typeof(options) == 'object'){
					title = options[0];
					multiple = options[1] == 'true' ? true : false;
				}
				
				field += '<div class="opt_group">';
				field += '<div class="frm-fld"><label>Název:</label>';
				field += '<input type="text" name="title" value="'+title+'" class=" '+sysClass+'" /></div>';
                field += '<input type="hidden" name="id-'+last_id+'" value="'+id_active_item+'"/>';
                
				field += '';
				field += '<div class="false-label">Volby:</div>';
				field += '<div class="fields">';
				//field += '<input type="checkbox" name="multiple"'+(multiple ? 'checked="checked"' : '')+'><label class="auto">Allow Multiple Selections</label>';
	
				if(typeof(values) == 'object'){
					for(c in values){
						field += selectFieldHtml(values[c], multiple);
					}
				} else {
					field += selectFieldHtml('', multiple);
				}
				
				field += '<div class="add-area"><a href="#" class="add add_opt" ><img src="../img/icon_plus.gif"   alt="Přidat"  title="Přidat volbu"/></a></div>';
				field += '</div>';
				field += '</div>';
				help = '';
				icon = "<img src='../img/selectbox-icon/select.gif' alt='' />";
                
                var legend = 'Výběrové pole'
                var titulek=title;
				appendFieldLi(legend,titulek, field, required, help, icon);
				
				$('.add_opt').live('click', function(){
				    //var idp = $(this).parent('li').attr('id');
                    //alert('ahoj'+idp);
                    
					$(this).parent().before( selectFieldHtml('', multiple) );
					return false;
				});
			}

			// Select field html, since there may be multiple
			var selectFieldHtml = function(values, multiple){
			    
                //alert(name);
                name = 'frm-'+(last_id)+'-fld';
				if(multiple){
					return checkboxFieldHtml(values);
				} else {
					return radioFieldHtml(values, name);
				}
			}
			
            var appendFieldSpecialLi = function(legend_name, title, field_html, icon){
                var li = '';
				li += '<li id="frm-'+last_id+'-item" class="'+field_type+'">';
                
                var style = "";
                var toggle= "Skrýt";
                if(!zobrazit){
                    style = 'style="display:none"';
                    toggle= "Zobrazit";
                    }
                    
                    
				li += '<div class="legend">'+icon+'<strong id="txt-title-'+last_id+'" title="'+title+'">'+legend_name+'</strong> <a id="frm-'+last_id+'" class="toggle-form" href="#">'+toggle+'</a></div>';
                
				li += '<div id="frm-'+last_id+'-fld" class="frm-holder"  '+style+'>';
				li += '<div class="frm-elements">';
				li += field;
				li += '</div>';
                
                if(!opts.disabled_delete)
				    li += '<a id="del_'+last_id+'" class="del-button delete-confirm" href="#"  title="Opravdu chcete smazat tuto položku formuláře?"><span>Smazat</span></a>';
                    
				li += '</div>';
				li += '</li>';
				
				$(ul_obj).append(li);
				$('#frm-'+last_id+'-item').hide();
				$('#frm-'+last_id+'-item').animate({opacity: 'show', height: 'show'}, 'slow');
				
				last_id++;
                
                }
			
			// Appends the new field markup to the editor
			var appendFieldLi = function(legend_name, title, field_html, required, help, icon){
				
				if(required){ required = required == 'true' ? true : false; }
							
                var style = "";
                var toggle= "Skrýt";
                if(!zobrazit){
                    style = 'style="display:none"';
                    toggle= "Zobrazit";
                    }
                            
				var li = '';
				li += '<li id="frm-'+last_id+'-item" class="'+field_type+'">';
				li += '<div class="legend">'+icon+'<strong id="txt-title-'+last_id+'" title="'+title+'">'+legend_name+'</strong> <a id="frm-'+last_id+'" class="toggle-form" href="#">'+toggle+'</a></div>';
				li += '<div id="frm-'+last_id+'-fld" class="frm-holder" '+style+'>';
                
				li += '<div class="frm-elements">';
				li += '<div class="frm-fld"><label for="required-'+last_id+'">Povinná položka:</label><input class="required '+sysClass+'" type="checkbox" value="1" name="required-'+last_id+'" id="required-'+last_id+'"'+(required ? ' checked="checked"' : '')+' /></div>';
				li += field;
				li += '</div>';
                
                if(!opts.disabled_delete)
				    li += '<a id="del_'+last_id+'" class="del-button delete-confirm" href="#"  title="Opravdu chcete smazat tuto položku formuláře?"><span>Smazat</span></a>';
                    
				li += '</div>';
				li += '</li>';
				
				$(ul_obj).append(li);
				$('#frm-'+last_id+'-item').hide();
				$('#frm-'+last_id+'-item').animate({opacity: 'show', height: 'show'}, 'slow');
				
				last_id++;
			}
			
			// handle field delete links
			$('.remove').live('click', function(){
				$(this).parent('div').animate({opacity: 'hide', height: 'hide', marginBottom: '0px'}, 'fast', function () {
					$(this).remove();
				});
				return false;
			});
			
			// handle field display/hide
			$('.toggle-form').live('click', function(){
				var target = $(this).attr("id");
				if($(this).html() == 'Skrýt'){
					$(this).removeClass('open').addClass('closed').html('Zobrazit');
					$('#' + target + '-fld').animate({opacity: 'hide', height: 'hide'}, 'slow');
					return false;
				}
				if($(this).html() == 'Zobrazit'){
					$(this).removeClass('closed').addClass('open').html('Skrýt');
					$('#' + target + '-fld').animate({opacity: 'show', height: 'show'}, 'slow');
					return false;
				}
				return false;
			});
			
			// handle delete confirmation
			$('.delete-confirm').live('click', function() {
				var delete_id = $(this).attr("id").replace(/del_/, '');
				confirm($(this).attr('title'), function() {
					$('#frm-'+delete_id+'-item').animate({opacity: 'hide', height: 'hide', marginBottom: '0px'}, 'slow', function () {
						$(this).remove();
					});
				})
				return false;
			});
			
			// saves the serialized data to the server 
			var save = function(){
				if(opts.save_url){
					$.ajax({
						type: "POST",
						url: opts.save_url,
						data: "id_form="+opts.id_form+"&"+$(ul_obj).serializeFormList(),
						success: function(xml){  }
					});
				}
			}
		});
	};
})(jQuery);

/**
 * jQuery Form Builder List Serialization Plugin
 * Copyright (c) 2009 Mike Botsko, Botsko.net LLC (http://www.botsko.net)
 * Originally designed for AspenMSM, a CMS product from Trellis Development
 * Licensed under the MIT (http://www.opensource.org/licenses/mit-license.php)
 * Copyright notice and license must remain intact for legal use
 * Modified from the serialize list plugin
 * http://www.botsko.net/blog/2009/01/jquery_serialize_list_plugin/
 */
(function($){
    
    function esc(str){return str;}
    
	$.fn.serializeFormList = function(options) {
		// Extend the configuration options with user-provided
		var defaults = {
			prepend: 'ul',
			is_child: false,
			attributes: ['class']
		};
		var opts = $.extend(defaults, options);
		var serialStr 	= '';
		
		if(!opts.is_child){ opts.prepend = '&'+opts.prepend; }
		
		// Begin the core plugin
		this.each(function() {
			var ul_obj = this;

			var li_count 	= 0;
			$(this).children().each(function(){
				
				for(att in opts.attributes){
					serialStr += opts.prepend+'['+li_count+']['+opts.attributes[att]+']='+esc($(this).attr(opts.attributes[att]));
					
					// append the form field values
					if(opts.attributes[att] == 'class'){
						
						serialStr += opts.prepend+'['+li_count+'][required]='+esc($('#'+$(this).attr('id')+' input.required').attr('checked'));
					    serialStr += opts.prepend+'['+li_count+'][id]='+esc($('#'+$(this).attr('id')+' input[type=hidden]').val());
                        
						switch($(this).attr(opts.attributes[att])){
							case 'input_text':
								serialStr += opts.prepend+'['+li_count+'][values]='+esc($('#'+$(this).attr('id')+' input[type=text]').val());
								break;
							case 'textarea':
								serialStr += opts.prepend+'['+li_count+'][values]='+esc($('#'+$(this).attr('id')+' input[type=text]').val());
								break;
                            case 'email':
								serialStr += opts.prepend+'['+li_count+'][values]='+esc($('#'+$(this).attr('id')+' input[type=text]').val());
								break;
                            case 'url':
								serialStr += opts.prepend+'['+li_count+'][values]='+esc($('#'+$(this).attr('id')+' input[type=text]').val());
								break;
                            case 'icq':
								serialStr += opts.prepend+'['+li_count+'][values]='+esc($('#'+$(this).attr('id')+' input[type=text]').val());
								break;    
                            case 'ico':
								serialStr += opts.prepend+'['+li_count+'][values]='+esc($('#'+$(this).attr('id')+' input[type=text]').val());
								break;
                            case 'dic':
								serialStr += opts.prepend+'['+li_count+'][values]='+esc($('#'+$(this).attr('id')+' input[type=text]').val());
								break;
                            case 'int':
								serialStr += opts.prepend+'['+li_count+'][values]='+esc($('#'+$(this).attr('id')+' input[type=text]').val());
								break;
                            case 'float':
								serialStr += opts.prepend+'['+li_count+'][values]='+esc($('#'+$(this).attr('id')+' input[type=text]').val());
								break;
                            case 'datum':
								serialStr += opts.prepend+'['+li_count+'][values]='+esc($('#'+$(this).attr('id')+' input[type=text]').val());
								break;
                            case 'cas':
								serialStr += opts.prepend+'['+li_count+'][values]='+esc($('#'+$(this).attr('id')+' input[type=text]').val());
								break;
                            case 'telefon':
								serialStr += opts.prepend+'['+li_count+'][values]='+esc($('#'+$(this).attr('id')+' input[type=text]').val());
								break;
                            case 'label':
								serialStr += opts.prepend+'['+li_count+'][values]='+esc($('#'+$(this).attr('id')+' input[type=text]').val());
								break; 
                            case 'popis':
								serialStr += opts.prepend+'['+li_count+'][values]='+esc($('#'+$(this).attr('id')+' textarea').val());
								break;    
                                
							case 'checkbox':
								var c = 1;
								$('#'+$(this).attr('id')+' input[type=text]').each(function(){
									
									if($(this).attr('name') == 'title'){
										serialStr += opts.prepend+'['+li_count+'][title]='+esc($(this).val());
									} else {
										serialStr += opts.prepend+'['+li_count+'][values]['+c+'][value]='+esc($(this).val());
										serialStr += opts.prepend+'['+li_count+'][values]['+c+'][default]='+$(this).prev().attr('checked');
									}
									c++;
								});
								break;
							case 'radio':
								var c = 1;
								$('#'+$(this).attr('id')+' input[type=text]').each(function(){
									if($(this).attr('name') == 'title'){
										serialStr += opts.prepend+'['+li_count+'][title]='+esc($(this).val());
									} else {
										serialStr += opts.prepend+'['+li_count+'][values]['+c+'][value]='+esc($(this).val());
										serialStr += opts.prepend+'['+li_count+'][values]['+c+'][default]='+$(this).prev().attr('checked');
									}
									c++;
								});
								break;
							case 'select':
								var c = 1;
								
								serialStr += opts.prepend+'['+li_count+'][multiple]='+$('#'+$(this).attr('id')+' input[name=multiple]').attr('checked');
								
								$('#'+$(this).attr('id')+' input[type=text]').each(function(){
									
									if($(this).attr('name') == 'title'){
										serialStr += opts.prepend+'['+li_count+'][title]='+esc($(this).val());
									} else {
										serialStr += opts.prepend+'['+li_count+'][values]['+c+'][value]='+esc($(this).val());
										serialStr += opts.prepend+'['+li_count+'][values]['+c+'][default]='+$(this).prev().attr('checked');
									}
									c++;
								});
							break;
						}
					}
				}
				li_count++;
			});
		});
		return(serialStr);
	};
})(jQuery);