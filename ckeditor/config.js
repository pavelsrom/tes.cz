/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here.
	// For complete reference see:
	// http://docs.ckeditor.com/#!/api/CKEDITOR.config

	// The toolbar groups arrangement, optimized for two toolbar rows.
/*
	config.toolbarGroups = [
		{ name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
		{ name: 'editing',     groups: [ 'find', 'selection', 'spellchecker' ] },
		{ name: 'links' },
		{ name: 'insert' },
		{ name: 'forms' },
		{ name: 'tools' },
		{ name: 'document',	   groups: [ 'mode', 'document', 'doctools' ] },
		{ name: 'others' },
		'/',
		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
		{ name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
		{ name: 'styles' },
		{ name: 'colors' },
		{ name: 'about' }
	];
    */

    config.resize_enabled = false;
    config.entities = false;
    config.entities_greek = false;
    config.entities_latin = false;
    config.htmlEncodeOutput = false;
    config.allowedContent = true;
    config.skin = 'moono-dark';

	// Remove some buttons provided by the standard plugins, which are
	// not needed in the Standard(s) toolbar.
	config.removeButtons = 'Underline,Subscript,Superscript,pbckcode,codemirror';

	// Set the most common block elements.
	config.format_tags = 'p;h1;h2;h3;h4;pre';

	// Simplify the dialog windows.
	config.removeDialogTabs = 'image:advanced;link:advanced';
    
    config.codemirror = {
        showSearchButton: false,
        // Whether or not to show Trailing Spaces
        showTrailingSpace: false,
    
        // Whether or not to highlight all matches of current word/selection
        highlightMatches: false,
    
        // Whether or not to show the format button on the toolbar
        showFormatButton: false,
    
        // Whether or not to show the comment button on the toolbar
        showCommentButton: false,
    
        // Whether or not to show the uncomment button on the toolbar
        showUncommentButton: false
    }
    
    
};



CKEDITOR.on( 'dialogDefinition', function( ev )
	{
		// Take the dialog name and its definition from the event
		// data.
		var dialogName = ev.data.name;
		var dialogDefinition = ev.data.definition;
 
		// Check if the definition is from the dialog we're
		// interested on (the "Link" dialog).
		if ( dialogName == 'link' )
		{
			// Get a reference to the "Link Info" tab.
			// CKEDITOR.dialog.definition.content
			var infoTab = dialogDefinition.getContents( 'info' );
 
			// Add a new UI element to the info box.
			// Use a JSON array named items to grab the list.
			// Modify the onChange event to update the real URL field.
			infoTab.add(
			{
				type : 'vbox',
				id : 'localPageOptions',
				children : [
				{
					type : 'select',
					label : 'Můžete přidat odkaz na stránku vašeho webu',
					id : 'localPage',
					title : 'Můžete přidat odkaz na stránku vašeho webu',
					items: pages_items,
					onChange : function(ev) {
						var diag = CKEDITOR.dialog.getCurrent();
						var url = diag.getContentElement('info','url');
						url.setValue(ev.data.value);
					}
				}]
			}
			);
			// Rewrite the 'onFocus' handler to always focus 'url' field.
			dialogDefinition.onFocus = function()
			{
				var urlField = this.getContentElement( 'info', 'url' );
				urlField.select();
			};
		}
	});
