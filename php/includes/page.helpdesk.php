<?php

/**
 * @author Pavel �rom
 * @copyright 2017
 */

global $private_pages;
global $userSystemMessage;
global $links;


if(!MODULE_PRIVATE_PAGES){
    return;
    }
    
    
    
$jmeno = get_secure_post("jmeno");
$url = get_secure_post("url");
$email = get_secure_post("kontakt");
$telefon = get_secure_post("telefon");
$zprava = get_secure_post("zprava");
$sluzby = get_array_post("sluzba");

$ch1 = in_array(1, $sluzby) ? "checked='checked'" : "";
$ch2 = in_array(2, $sluzby) ? "checked='checked'" : "";
$ch3 = in_array(3, $sluzby) ? "checked='checked'" : "";
$ch4 = in_array(4, $sluzby) ? "checked='checked'" : "";


$c1 = rand(0,9);
$c2 = rand(0,9);

$html .= $userSystemMessage->show_messages(true, "formular-helpdesk");



?>

<div class="formular">
    <form method="post" action="#formular">
        <div class="fl w35">
            <input type="text" name="jmeno" class="input" placeholder="<?php echo 	TJMENO_A_PRIJMENI;?>" value="<?php echo $jmeno;?>"/>
            <input type="email" name="kontakt" class="input" placeholder="<?php echo 	TEMAIL;?>" value="<?php echo $email;?>"/>
            <input type="text" name="telefon" class="input" placeholder="<?php echo 	TTELEFON;?>" value="<?php echo $telefon;?>"/>
            <input type="text" name="url" class="input" placeholder="<?php echo 	TURL_STAVAJICIHO_WEBU;?>" value="<?php echo $url;?>" />
        </div>
        
        <div class="fl w60">
            <h3>M�l/a bych z�jem o ...</h3>
            
            <div class="ch">
                <input type="checkbox" name="sluzba[]" value="1" id="s1" <?php echo $ch1;?>/>
                <label for="s1"><?php echo editable('TSOUVISEJICI_CLANKY',TSOUVISEJICI_CLANKY);?> </label>
            </div>
            <div class="ch">
                <input type="checkbox" name="sluzba[]" value="2" id="s2" <?php echo $ch2;?>/>
                <label for="s2">... napojen� redak�n�ho syst�mu Carmeo na m�j st�vaj�c� web</label>
            </div>
            <div class="ch">
                <input type="checkbox" name="sluzba[]" value="3" id="s3" <?php echo $ch3;?>/>
                <label for="s3">... nov� web s�redak�n�m syst�mem Carmeo</label>
            </div>
            <div class="ch">
                <input type="checkbox" name="sluzba[]" value="4" id="s4" <?php echo $ch4;?>/>
                <label for="s4">... dal�� informace k�redak�n�mu syst�mu Carmeo �i kalkulaci</label>
            </div>
            
            
        </div>
        
        <div class="cleaner"></div>
        <textarea name="zprava" placeholder="<?php echo TZPRAVA;?>"><?php echo $zprava;?></textarea>
        <input type="text" class="input antispam w30 req" name="antispam" placeholder="<?php echo sprintf(TANTISPAM, $c1, $c2);?>" value=""/>
            
        <div class="button">
            <input type="email" name="email" class="nodisplay" value="test@test.cz"/>
            <input type="hidden" name="c1" value="<?php echo $c1;?>"/>
            <input type="hidden" name="c2"  value="<?php echo $c2;?>"/>
            <input type="submit" name="btnOdeslatForm" value="<?php echo TODESLAT;?> &rsaquo;"/>
        </div>
    </form>
</div>