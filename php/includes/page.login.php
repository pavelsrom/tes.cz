<?php

/**
 * @author Pavel Srom
 * @copyright 2010
 */

global $private_pages;
global $userSystemMessage;
global $links;
global $db;

if(!MODULE_PRIVATE_PAGES){
    $private_pages->logout();
    //redirect($links->getUrlHomePage(), "");
    $private_pages->redirect();
    }

if(logged_into_cms() && is_get("user") && is_get("pass"))
{
    $private_pages->logout();
    $logged = $private_pages->login(get_request("user"), get_request("pass"), true);
    
    if($private_pages->is_logged())
    {
        if(isset($_SESSION['referer']))
            unset($_SESSION['referer']);
            
        $ref = $links->get_url(ID_PROFIL);
        redirect($ref,"");
    }
}



if(is_get('logout') && $private_pages->is_logged()){
    $private_pages->logout();   
    $private_pages->redirect(false, false);
}


//potvrzeni registrace
if(is_get("confirm_registrace"))
{
    
    $hash = get_request("confirm_registrace");
    
    $id_uzivatele = $db->get(TABLE_UZIVATELE,"idUzivatele","hash='".$db->secureString($hash)."'");
    $userSystemMessage->set_form_id("login");
    
    if($id_uzivatele > 0)
    {
        $db->update(TABLE_UZIVATELE,array("aktivni" => 1, "datumAktivace" => "now"),"idUzivatele=".$id_uzivatele);
        $db->update(TABLE_EMAILY_PRIJEMCI,array("stav" => 1),"hash='".$db->secureString($hash)."'");
        $userSystemMessage->add_ok(TREGISTRACE_BYLA_USPESNA);
        $private_pages->redirect();
    }
    
}


if($private_pages->is_logged() && !is_get('change_password') && !is_get('forgot_password') && !is_get('no_access'))
{

    Redirect($links->get_url('uvodni'),"");
}

if(is_get('no_access'))
{
    $userSystemMessage->set_form_id("login");
    $userSystemMessage->add_error(TNEMATE_PRISTUP_KE_STRANCE);
}
elseif(is_post('btnSendPassword'))
{
    $private_pages->send_forgot_password(get_post('username'));
}

elseif(is_post('btnChangePassword') && $private_pages->is_logged())
{

    $private_pages->change_password(get_post('pass_old'), get_post('pass_new1'), get_post('pass_new2'));
    
}
elseif(is_post('btnLogin'))
{

    $ref = isset($_SESSION['referer'])?$_SESSION['referer']:$links->get_url(ID_PROFIL);
    //$ref = isset($_SESSION['referer'])?$_SESSION['referer']:$links->get_url('uvodni');
    //$ref = "http://www.serd.cz/servisni-informace.html";
    
    $private_pages->login($_POST['username'], $_POST['password']);
    
    if($private_pages->is_logged())
    {
        global $kosik;
        $kosik->user_logged();
        redirect($ref,"");
    }

    
}


if(is_get('no_access')){
    $html .= $userSystemMessage->show_messages(true, "login");
}

elseif(is_get('change_password') && $private_pages->is_logged())
{
    $html .= '<div class="contacts-form" id="form">';
    $html .= "<h2>".TZMENA_HESLA."</h2>";
    $html .= $userSystemMessage->show_messages(true, "login"); 
    
    $html .=  "<form method='post' action='#form' class='delivery-form'>";
    

    $html .= frm_password(TDOSAVADNI_HESLO, 'pass_old', true);
    $html .= frm_password(TNOVE_HESLO1, 'pass_new1', true);
    $html .= frm_password(TNOVE_HESLO2, 'pass_new2', true);

    $html .= '<div class="basket-steps">';
    $html .= frm_submit('btnChangePassword', TZMENIT_HESLO);
    $html .= '</div>';

    
    $html .=  "</form>";
    $html .=  "</div>";

}
elseif(is_get('forgot_password'))
{
    
    $html .= '<div class="contacts-form" id="form">';
    $html .= "<h3>".TZAPOMNEL_JSEM_HESLO."</h3>";
    $html .= $userSystemMessage->show_messages(true, "login");
    $html .= "<form method='post' action='#form' class='delivery-form' >";
    
        
    $html .= "<div>";
    $html .= frm_text(TPRIHLASOVACI_JMENO, 'username');

    $html .= '<div class="basket-steps">';
    $html .= frm_submit('btnSendPassword', TPOSLAT_NOVE_HESLO);
    $html .= '</div>';

    //$html .= "<input type='text' name='username' value='' class='input' placeholder='".TPRIHLASOVACI_JMENO."'/>";     $html .= "<input type='submit' name='btnSendPassword' value='".TPOSLAT_NOVE_HESLO."' class='btn'/> nebo ";
    $html .= "<a href='".$links->get_url("login")."' class='tlacitko'>".TZPET_NA_PRIHLASENI."</a>";
    $html .= "</div>";
    $html .= "</form>";
    $html .= "</div>";
    
}
else
{

    
    
    //dd($userSystemMessage->show_messages(true, "login"));
    
    $url_registrace = $links->get_url(ID_REGISTRACE)."#form";
    
    $html .= '<div class="contacts-form">';
    //$html .= "<h3>".TLOGIN."</h3>";
    $html .= $userSystemMessage->show_messages(true, "login");


    $html .= "<form method='post' action='#prihlaseni' class='delivery-form' id='prihlaseni'>";
    
    $html .= frm_text(TPRIHLASOVACI_JMENO, 'username');
    $html .= frm_password(THESLO, 'password');

    $html .= '<div class="basket-steps">';
    $html .= frm_submit('btnLogin', TPRIHLASIT_SE);
    $html .= '</div>';

    $html .= '<div class="col odkazy">';
    $html .= "<a href='?forgot_password'>".TZAPOMNEL_JSEM_HESLO."</a><br /><a href='".$url_registrace."' class=''>".TREGISTRACE_UZIVATELE."</a>";
    $html .= "</div>";

    $html .= "</form>";
    $html .= "</div>";  



}

?>