<?php

/**
 * @author Pavel Srom
 * @copyright 2011
 */


if(!defined('SECURITY_CMS')) exit;

$inline_js = "";
$js_files = $css_files = array();

$js_files[] = 'js/jquery.js';
//$js_files[] = 'js/jquery-ui.js';
$js_files[] = 'js/jquery-ui/jquery-ui.min.js';
$js_files[] = 'js/jquery.ui.datepicker-'.(CMS_LANG=='cz' ? 'cs' : CMS_LANG).'.min.js';
$js_files[] = 'js/funkce.js';
$js_files[] = 'js/jquery.scrollto.js';
$js_files[] = 'js/jscolor/jscolor.js';
$js_files[] = 'js/datatables/jquery.datatables.min.js';
//$js_files[] = 'js/uploadify/jquery.uploadify.min.js';
$js_files[] = 'ckeditor/ckeditor.js';
$js_files[] = 'ckfinder/ckfinder.js';
$js_files[] = 'js/jquery.center.min.js';
$js_files[] = 'js/jquery.msg.js';
$js_files[] = 'js/icheck/icheck.min.js';
$js_files[] = 'js/multiselect/js/ui.multiselect.js';
$js_files[] = 'js/multiselect/js/locale/ui-multiselect-'.(CMS_LANG=='cz' ? 'cs' : CMS_LANG).'.js';
$js_files[] = 'js/multiselect/js/plugins/tmpl/jquery.tmpl.1.1.1.js';


$css_files[] = 'js/jquery-ui/jquery-ui.min.css';
$css_files[] = 'js/icheck/skins/minimal/yellow.css';
$css_files[] = THEME_PATH.'style.css';


$css_files[] = THEME_PATH.'we-tools.css';
//$css_files[] = THEME_PATH.'uploadify.css';
$css_files[] = THEME_PATH.'jquery.msg.css';
$css_files[] = THEME_PATH.'font-awesome.min.css';
$css_files[] = 'js/multiselect/css/ui.multiselect.css';

$inline_js .= '

	$(function(){
        
	   $(".image_edit").hide();
		  			
       $(document).on("mouseover", "table.table:not(\'.table-inner\') tr, .image_box-in", function(){ 
            $(this).addClass("light"); 
            $(this).removeClass("unlight");
            });
           
       $(document).on("mouseout", "table.table:not(\'.table-inner\') tr, .image_box-in", function(){ 
            $(this).addClass("unlight"); 
            $(this).removeClass("light");
            });
                       
        
        $.msg("overwriteGlobal", "autoUnblock", false);
        $.msg("overwriteGlobal", "fadeIn", 200);
        $.msg("overwriteGlobal", "fadeOut", 200);
        $.msg("overwriteGlobal", "timeOut", 1000);
        $.msg("overwriteGlobal", "klass", "process_message");
        $.msg("overwriteGlobal", "bgPath", "'.RELATIVE_PATH.THEME_PATH.'img/");
        $.msg("overwriteGlobal", "content", "'.TCEKEJTE_PROSIM.'");
  
        $.extend(true, $.fn.dataTable.defaults, {
            "oLanguage": {
                "sLengthMenu": "'.TZOBRAZENO_X_ZAZNAMU_NA_STRANCE.'",
          		"sZeroRecords": "'.TNENALEZEN_ZADNY_ZAZNAM.'",
                "sEmptyTable": "'.TNENALEZEN_ZADNY_ZAZNAM.'",
                "sInfo": "'.TZOBRAZENO_X_AZ_Y_ZAZNAMU_Z_X_ZAZNAMU.'",
                "sInfoEmpty": "'.TZOBRAZENO_0_AZ_0_ZAZNAMU_Z_0_ZAZNAMU.'",
                "sInfoFiltered": "('.TFILTROVANO_Z_X_ZAZNAMU.')",
                "sProcessing": "'.TVAS_POZADAVEK_SE_ZPRACOVAVA.'",
                "sLoadingRecords": "'.TCEKEJTE_PROSIM.'...",
                "oPaginate": {
                    "sFirst": "'.TPRVNI.'",
                    "sLast": "'.TPOSLEDNI.'",
                    "sNext": "'.TNASLEDUJICI.'",
                    "sPrevious": "'.TPREDCHOZI.'"
                    }
                },
            "bAutoWidth": false,
            "bProcessing": true,
            "bServerSide": true,
            "bStateSave": '.UKLADAT_STAVY_FILTRU_SEZNAMU.',
            "iDisplayLength": '.POCET_ZAZNAMU_NA_JEDNU_STRANKU.',
            "sPaginationType": "full_numbers"
        });
        
        
        $(".kalendar").datepicker({
            dateFormat: "dd.mm.yy"
        })
                
        $( "span.jtip" ).tooltip({
            tooltipClass: "napoveda",
            position: { my: "left+10 top", at: "right center" },
            open: function(event, ui){
                id = $(this).attr("id").substring(2);
                var _elem = ui.tooltip;
                $.get("'.AJAX_GATEWAY.'qtip", {id: id},function(data){
                    _elem.find(".ui-tooltip-content").html(data);
                });   
            },
            content: "'.TCEKEJTE_PROSIM.'" 
         });

           
	});

    
    function custom_confirm(message, callback) {
            $("#confirm").dialog({
              modal: true,  
              title: "'.TPOTVRDTE.'",  
              dialogClass: "simplemodal", 
              resizable: false, 
              buttons : {
                "'.TANO.'" : function() {
                  callback();
                  $(this).dialog("close");
                },
                "'.TNE.'" : function() {
                  $(this).dialog("close");
                }
              }
            });
        
            $("#confirm").html(message).dialog("open");
        }


';

if($aktModule=='statistics' || $aktModule=='home')
{
    $inline_js .= 'Highcharts.setOptions({
                	lang: {
                		thousandsSep: " ",
                        downloadJPEG: "'.TSTAHNOUT.' JPEG '.TOBRAZEK.'",
                        downloadPDF: "'.TSTAHNOUT.' PDF '.TDOKUMENT.'",
                        downloadPNG: "'.TSTAHNOUT.' PNG '.TOBRAZEK.'",
                        downloadSVG: "'.TSTAHNOUT.' SVG '.TVEKTOROVY_OBRAZEK.'",
                        printChart: "'.TVYTISKNOUT_GRAF.'"
                	}
                });';
                
    $js_files[] = 'js/highcharts/js/highcharts.js';
    
    if($aktModule=='statistics')
        $js_files[] = 'js/highcharts/js/modules/exporting.js';
}

if($aktModule=='forms'){
	$js_files[] = "js/editarea/edit_area/edit_area_full.js";
	$inline_js .= "
		editAreaLoader.init({
				id: 'editarea'
				,start_highlight: true
				,allow_toggle: false
				,language: 'cs'
				,syntax: 'html'
				,toolbar: 'search, go_to_line, fullscreen, |, undo, redo, |, select_font, |, syntax_selection, |, change_smooth_selection, highlight, reset_highlight, |, help'
				,syntax_selection_allow: 'css,html,js,php'
				,is_multi_files: false
				,EA_load_callback: 'editAreaLoaded'
				,show_line_colors: true
			});
	
		";
        
    $js_files[] = 'js/formbuilder/jquery.formbuilder.js';
    $js_files[] = 'js/selectbox/jquery.dd.js';
    $css_files[] = 'js/formbuilder/formbuilder.css';
    $css_files[] = 'js/selectbox/dd.css';
	}




if($aktModule=='editboxes'){
    $js_files[] = 'js/cycle/jquery.cycle.all.min.js';
}

if($aktModule=='articles')
{
    $js_files[] = 'js/jquery.autoSuggest.js';
    $css_files[] = THEME_PATH.'autoSuggest.css';
    
}
   
   
   
//--------------------------------------------------------------------------------------------
//nacteni js, css a inline js kodu
//--------------------------------------------------------------------------------------------
   
foreach($css_files AS $css){
    echo "<link rel='stylesheet' type='text/css' href='".RELATIVE_PATH.$css."' />\n";
	}
            
foreach($js_files AS $js)		
    echo "<script type='text/javascript' src='".RELATIVE_PATH.$js."'></script>\n";
         
              	
echo "<script type='text/javascript'>";
echo " <!-- \n";
echo $inline_js;
echo "// -->";
echo "</script>\n";

	

?>