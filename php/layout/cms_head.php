<?php

/**
 * @author Pavel Srom
 * @copyright 2011
 */

if(!defined('SECURITY_CMS')) exit;

?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">
<html xmlns='http://www.w3.org/1999/xhtml' lang='cs' xml:lang='cs'>
<head>
<?php

$am = $modules->GetActiveModule();

if($am !== null)
    $title = $modules->GetActiveModule()->moduleName;
    else
    $title = TSTRANKA_NENALEZENA_NEBO_NEDOSTATECNA_PRAVA;


?>
<title><?php echo CMS_NAME." - ".$title;?></title>
<meta name='description' content='' />
<meta name='robots' content='noindex, nofollow' />
<meta http-equiv='content-type' content='text/html; charset=UTF-8' />
<meta http-equiv='Content-Style-Type' content='text/css' />

<?php include('cms_header_css_js.php'); ?>

</head>
<body>
