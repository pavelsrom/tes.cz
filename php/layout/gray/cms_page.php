<?php

/**
 * @author Pavel Srom
 * @copyright 2011
 */

if(!defined('SECURITY_CMS')) exit;

include(CMS_THEME_PATH.'cms_header.php');

//ShowH1($mod->moduleName);
$h1_data = $mod->getH1();
echo "<h1>".$h1_data['h1']."</h1>";

if($h1_data['text']!='')
    echo "<p>".$h1_data['text']."</p>";
 

$systemMessage->show_messages(); 
$mod->Create();

include(CMS_THEME_PATH.'cms_footer.php');

?>