<?php

/**
 * @author Pavel Srom
 * @copyright 2010
 */

// konfiguracni soubor themes - default

DEFINE('THEME_NAME', 'gray');
DEFINE('THEME_PATH', THEMES_DIR.THEME_NAME."/");

//DEFINE('DIRNAME_MODULES_ICON', THEME_PATH.'icon_modules/');
//DEFINE('DIRNAME_MODULES_ICON_ACTIVE', THEME_PATH.'icon_modules_active/');
DEFINE('DIRNAME_H3_ICON', THEME_PATH.'icon_h3');


//------------------------------------------------------
//   GRAFICKE IKONY
//------------------------------------------------------

DEFINE('ICON_DIR', THEME_PATH.'icon');
DEFINE('ICON_DISABLED_DIR', THEME_PATH.'icon_disabled');

DEFINE("ICON_UVOD_ZAKLADNI_NASTAVENI_USER","img/icon_user_uvod.png");
DEFINE("ICON_UVOD_ZAKLADNI_NASTAVENI_WEB","img/icon_wet_uvod.png");
DEFINE('ICON_GALLERY_CONTENT','icon_gallery_content.png');
DEFINE('ICON_INFO','icon_info.png');
DEFINE('ICON_WARNING','icon_warning.png');
DEFINE('ICON_PLUS','icon_plus.png');
DEFINE('ICON_MINUS','icon_minus.png');
DEFINE('ICON_PLUS_MINUS_DISABLED','icon_disable_plus_minus.png');
DEFINE('ICON_DELETE','icon_delete.png');
DEFINE('ICON_GALLERY','icon_gallery.png');
DEFINE('ICON_STATISTICS','icon_statistics.png');
DEFINE('ICON_SAVE','icon_save.png');
DEFINE('ICON_RESCURE','icon_rescure.png');
DEFINE('ICON_PREVIEW','icon_preview.png');
DEFINE('ICON_TREE','icon_tree.png');
DEFINE('ICON_GALLERY_FOLDER','icon_gallery_folder.png');
DEFINE('ICON_SETTING','icon_setting.png');
DEFINE('ICON_CONTENT','icon_content.png');
DEFINE('ICON_DOWNLOAD','icon_download.png');
DEFINE('ICON_ADD','icon_add.png');
DEFINE('ICON_UP','icon_up.png');
DEFINE('ICON_DOWN','icon_down.png');
DEFINE('ICON_ATTACHMENT','icon_attachment.png');
DEFINE('ICON_TIP','icon_tip.png');
DEFINE('ICON_HELP','icon_help.png');
DEFINE('ICON_DATE','icon_date.png');
DEFINE('ICON_LINK','icon_link.png');
DEFINE('ICON_ARTICLES','icon_articles.png');
DEFINE('ICON_REQ','icon_req.png');
DEFINE('ICON_DISCUSSION','icon_discussion.png');
DEFINE('ICON_EDITBOX_KOLOTOC','icon_editbox_kolotoc.png');
DEFINE('ICON_EDITBOX','icon_editbox.png');
DEFINE('ICON_USER_TIME','icon_user_time.png');

DEFINE('ICON_PLAY','icon_play.png');
DEFINE('ICON_STOP','icon_stop.png');
DEFINE('ICON_PAUSE','icon_pause.png');

DEFINE('ICON_OK','icon_ok.png');
DEFINE('ICON_ERROR','icon_error.png');

//definice ikon pro nadpis h3
DEFINE('H3ICON_HOME', DIRNAME_H3_ICON."/home.png");
DEFINE('H3ICON_PAGE_SETTING', DIRNAME_H3_ICON."/page-setting.png");
DEFINE('H3ICON_PANEL_SETTING', DIRNAME_H3_ICON."/panel-setting.png");
DEFINE('H3ICON_EMAIL', DIRNAME_H3_ICON."/email.png");
DEFINE('H3ICON_SEO', DIRNAME_H3_ICON."/seo.png");
DEFINE('H3ICON_CONTENT', DIRNAME_H3_ICON."/content.png");
DEFINE('H3ICON_USERS', DIRNAME_H3_ICON."/users.png");
DEFINE('H3ICON_PASS', DIRNAME_H3_ICON."/pass.png");
DEFINE('H3ICON_STAT', DIRNAME_H3_ICON."/statistics.png");
DEFINE('H3ICON_DATE', DIRNAME_H3_ICON."/date.png");
DEFINE('H3ICON_RECEIVE', DIRNAME_H3_ICON."/receive.png");
DEFINE('H3ICON_PHOTOS', DIRNAME_H3_ICON."/photos.png");

?>