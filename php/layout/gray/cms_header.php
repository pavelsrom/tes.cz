<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */


if(!defined('SECURITY_CMS')) exit;

$mod = $modules->GetActiveModule();
$jmeno_prihlaseneho = $login_obj->getName();

if(/*$mod == NULL || */!$login_obj->hasDomain() || $modules->GetCountModules()==0 )
{

    $login_obj->Logout();

}


include(THEMES_DIR.'/cms_head.php');

$jazyky = $lang->get_langs();
$jazyky_pol = array();

if(count($jazyky) > 0 && $modules->GetActiveModule())
{
    foreach($jazyky AS $idj => $j)
        {
            if($idj == WEB_LANG_ID)
                $jazyky_pol[] = "<span>".strtoupper($lang->get($idj, 'jazyk'))."</span>";
                else
                $jazyky_pol[] = "<a href='".htmlspecialchars(get_link($modules->GetActiveModule()->moduleSystemName,$modules->GetActiveModule()->get_page(),0,"",0,"",$idj))."' title='".$j['nazev']."'>".strtoupper($lang->get($idj, 'jazyk'))."</a>";
        }
}


$domeny = $domain->userDomainsToArray( $login_obj->getDomains() );
$domeny_pol = array();

if(count($domeny) > 0)
{

    foreach($domeny AS $idd => $dn)
    {
        if($idd == $domain->getId()) 
            $domeny_pol[] = "<span>".$domain->getName()."</span>";
            else
            $domeny_pol[] = "<a href='home?nastavitDomenu=".$idd."' title='".TSPRAVOVAT_TENTO_WEB."'>".$dn."</a>";
    }

}


?>

<!--
<div id="header">
<div id="horni-menu">
<div id="horni-menu-in">
<div class="pol"><?php echo TPRIHLASEN;?>: <b><?php echo $jmeno_prihlaseneho; ?></b> <a href="<?php echo RELATIVE_PATH.ADMIN_SCRIPT."?logout";?>" title="<?php echo TODHLASIT_SE;?>"><?php echo TODHLASIT_SE;?></a></div>

<div class="pol"><?php echo TJAZYKOVA_MUTACE;?>: <?php echo implode("&nbsp;&nbsp;", $jazyky_pol);?></div>
<div class="pol last"><?php echo TSPRAVOVANY_WEB;?>: <?php echo implode("&nbsp;&nbsp;", $domeny_pol);?></div>
<div class="cleaner"></div>
</div>
</div>
</div>
-->

<div id="menu">
<div id="menuIn">

<?php

foreach($modules->modul AS $m)
    if($m->admin_mod == 0 && $m->function == 0 && $m->allowed == 1 && in_array($m->id, $login_obj->getModules())){
        if($m->active) $aktivni = "aktivni"; else $aktivni = "";
        echo "<a href='".htmlspecialchars($m->get_first_page())."' title='".$m->moduleName."' class='".$aktivni."'>
            <span class='fa fa-".$m->icon." fa-2x'></span>
            <br />".$m->moduleName."</a>
		";
		}
/*
foreach($modules->modul AS $m)
    if($m->admin_mod == 1 && $m->function == 0 && $m->allowed == 1 && in_array($m->id, $login_obj->getModules())){
	    $src = RELATIVE_PATH.DIRNAME_MODULES_ICON.$m->icon."' alt='".$m->moduleName;
		if($m->active) $aktivni = "aktivni"; else $aktivni = "";				
		echo "<div class='item ".$aktivni."'>
		<a href='".htmlspecialchars($m->get_first_page())."'>
        <img src='".$src."' title='".$m->moduleName."' />
        </a><br />
		<a class='menu-item-link' href='".htmlspecialchars($m->get_first_page())."'>".$m->moduleName."</a>
		</div>";
		}
*/

?>


<div class="cleaner"></div>
</div>
</div>

<div id="main">
<div id="content">
<div id="contentIn">

