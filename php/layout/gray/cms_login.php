<?php

/**
 * @author Pavel Srom
 * @copyright 2011
 */

if(!defined('SECURITY_CMS')) exit;

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns='http://www.w3.org/1999/xhtml' lang='cs' xml:lang='cs'>
<head><title></title>
<meta name='description' content='' />
<meta name='robots' content='noindex, nofollow' />
<meta http-equiv='content-type' content='text/html; charset=UTF-8' />
<meta http-equiv='Content-Style-Type' content='text/css' />
<link rel='stylesheet' type='text/css' href='<?php echo RELATIVE_PATH;?>php/layout/gray/login.css' />
<script type='text/javascript' src='<?php echo RELATIVE_PATH;?>js/jquery.js'></script>

</head>
<body id="login">
<div id="web">
<div id="header">
    <div id="logo"></div>
</div>

<div id='login-form'>
<div id='login-form-in'>

		
<form method='post' action=''>
<h2>Přihlášení do redakčního systému</h2>
<div class='message'>

<?php 

$systemMessage->show_error_messages(); 
?>

</div>


<table>
<tr>
    <th><?php echo TPRIHLASOVACI_JMENO;?>:</th>
    <td><input type='text' name='login' value=''  class='text'/></td>
</tr>

<tr>
    <th><?php echo THESLO;?>:</th>
    <td><input type='password' name='pass' value=''  class='text'/></td>
</tr>

<tr>
    <td colspan='2' class="tlacitko">
    <input type='submit' name='btnLogin' value='<?php echo TPRIHLASIT;?>' class='submit' />
    </td>
</tr>

</table>
</form>

</div>		
</div>

<div id="footer">
    <div>
        <a id="logo_profiwebik" href="http://www.profiwebik.cz/" title="Tvorba webových stránek Profiwebik.cz"></a>
    </div>


</div>


</div>
</body>
</html>

