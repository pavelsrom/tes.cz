<?php

/**
 * @author Pavel Srom
 * @copyright 2010
 */

//konfigurace databaze

//------------------------------------
//NASTAVENI DATABAZE
//------------------------------------



//ostra db
define('DB_SERVER', '127.0.0.1');
define('DB_USER', 'tesmotors.cz');
define('DB_PASS', 'Z2NmHQazN9Jx');
define('DB_NAME', 'tesmotorscz1');
define('DB_PORT', 3306);



//LOKALNI NASTAVENI DATABAZE
define('LOCAL_DB_SERVER', 'localhost');
define('LOCAL_DB_USER', 'root');
define('LOCAL_DB_PASS', 'root');
define('LOCAL_DB_NAME', 'tes');
define('LOCAL_DB_PORT', 3306);


//-------------------------------------
//  DATABAZOVE TABULKY
//-------------------------------------

define("TABLE_PREFIX", "");

define('TABLE_CMS_MODULY', TABLE_PREFIX.'cms_moduly');
define('TABLE_CMS_PRAVA', TABLE_PREFIX.'cms_prava');
define('TABLE_CMS_DEBUG', TABLE_PREFIX.'cms_debug');
define('TABLE_ANKETY', TABLE_PREFIX.'ankety');
define('TABLE_ANKETY_IP', TABLE_PREFIX.'ankety_ip');
define('TABLE_ANKETY_OTAZKY', TABLE_PREFIX.'ankety_otazky');
define('TABLE_ANKETY_ODPOVEDI', TABLE_PREFIX.'ankety_odpovedi');
define('TABLE_ANKETY_ODPOVEDI_NAZVY', TABLE_PREFIX.'ankety_odpovedi_nazvy');

define('TABLE_DOMENY', TABLE_PREFIX.'domeny');
define('TABLE_DOMENY_JAZYKY', TABLE_PREFIX.'domeny_jazyky');
define('TABLE_DOMENY_MODULY', TABLE_PREFIX.'domeny_moduly');
define('TABLE_EMAILY_KAMPANE', TABLE_PREFIX.'emaily_kampane');
define('TABLE_EMAILY_PRIJEMCI', TABLE_PREFIX.'emaily_prijemci');
define('TABLE_EMAILY_PRILOHY', TABLE_PREFIX.'emaily_prilohy');
define('TABLE_EMAILY_SKUPINY_KAMPANE', TABLE_PREFIX.'emaily_skupiny_kampane');
define('TABLE_EMAILY_SKUPINY_PRIJEMCI', TABLE_PREFIX.'emaily_skupiny_prijemci');
define('TABLE_EMAILY_SKUPINY', TABLE_PREFIX.'emaily_skupiny');
define('TABLE_EMAILY_FRONTA', TABLE_PREFIX.'emaily_fronta');

define('TABLE_FORMULARE', TABLE_PREFIX.'formulare');
define('TABLE_FORMULARE_POLOZKY', TABLE_PREFIX.'formulare_polozky');
define('TABLE_FORMULARE_ZPRAVY', TABLE_PREFIX.'formulare_zpravy');

define('TABLE_SOUVISEJICI_CLANKY', TABLE_PREFIX.'clanky_souvisejici');
define('TABLE_STRANKY', TABLE_PREFIX.'stranky');
define('TABLE_STRANKY_EDITORI', TABLE_PREFIX.'stranky_editori');
define('TABLE_STRANKY_PRIVATNI_UZIVATELE', TABLE_PREFIX.'stranky_privatni_uzivatele');
define('TABLE_STRANKY_SKUPINY', TABLE_PREFIX.'stranky_skupiny');
define('TABLE_STRANKY_FOTKY', TABLE_PREFIX.'stranky_fotky');
define('TABLE_STRANKY_ODKAZY', TABLE_PREFIX.'stranky_odkazy');


define('TABLE_UZIVATELE_DOMENY', TABLE_PREFIX.'uzivatele_domeny');
define('TABLE_UZIVATELE_JAZYKY', TABLE_PREFIX.'uzivatele_jazyky');
define('TABLE_UZIVATELE_MODULY', TABLE_PREFIX.'uzivatele_moduly');
define('TABLE_UZIVATELE', TABLE_PREFIX.'uzivatele');
define('TABLE_UZIVATELE_SKUPINY', TABLE_PREFIX.'uzivatele_skupiny');
define('TABLE_UZIVATELE_SKUPINY_RELACE', TABLE_PREFIX.'uzivatele_skupiny_relace');
define('TABLE_UZIVATELE_LOG', TABLE_PREFIX.'uzivatele_log');


define('TABLE_PANELY', TABLE_PREFIX.'panely');
define('TABLE_PANELY_SABLONY', TABLE_PREFIX.'panely_sablony');
define('TABLE_PANELY_POLOZKY', TABLE_PREFIX.'panely_polozky');
define('TABLE_PANELY_POLOZKY_TYPY', TABLE_PREFIX.'panely_polozky_typy');


define('TABLE_DISKUZE_POLOZKY', TABLE_PREFIX.'diskuze_polozky');
define('TABLE_DISKUZE_FILTR', TABLE_PREFIX.'diskuze_filtr');
define('TABLE_DISKUZE_BLACKLIST', TABLE_PREFIX.'diskuze_blacklist');
define('TABLE_DISKUZE_WHITELIST', TABLE_PREFIX.'diskuze_whitelist');

define('TABLE_SMAJLICI', TABLE_PREFIX.'smajlici');
define('TABLE_SMAJLICI_POLOZKY', TABLE_PREFIX.'smajlici_polozky');
define('TABLE_EDITBOXY', TABLE_PREFIX.'editboxy');
define('TABLE_EDITBOXY_POLOZKY', TABLE_PREFIX.'editboxy_polozky');

define('TABLE_VYHLEDAVANI_VYRAZY', TABLE_PREFIX.'vyhledavani_vyrazy');
define('TABLE_NAPOVEDA', TABLE_PREFIX.'napoveda');

define('TABLE_HTML_SABLONY', TABLE_PREFIX.'html_sablony');

define('TABLE_FACEBOOK', TABLE_PREFIX.'facebook');
define('TABLE_FACEBOOK_PLUGINY', TABLE_PREFIX.'facebook_pluginy');

define('TABLE_PREKLADY_ADMINISTRACE', TABLE_PREFIX.'preklady_administrace');
define('TABLE_PREKLADY', TABLE_PREFIX.'preklady');

define('TABLE_ANKETY_POLOZKY_NAZVY', TABLE_PREFIX.'ankety_polozky_nazvy');
define('TABLE_FORMULARE_POLOZKY_NAZVY', TABLE_PREFIX.'formulare_polozky_nazvy');
define('TABLE_JAZYKY', TABLE_PREFIX.'jazyky');
define('TABLE_ZALOHY',TABLE_PREFIX.'zalohy');
define('TABLE_LOG',TABLE_PREFIX.'log');
define('TABLE_LOG_ERROR',TABLE_PREFIX.'log_error');

define('TABLE_ODKAZY',TABLE_PREFIX.'odkazy');
define('TABLE_ZALOHY_DB',TABLE_PREFIX.'zalohy_db');
define('TABLE_DASHBOARD',TABLE_PREFIX.'dashboard');

define('TABLE_NASTAVENI',TABLE_PREFIX.'nastaveni');
define('TABLE_NASTAVENI_DOMENY',TABLE_PREFIX.'nastaveni_domeny');

define('TABLE_KALENDAR_TERMINY',TABLE_PREFIX.'kalendar_akci_terminy');

define('TABLE_STRANKY_TAGY',TABLE_PREFIX.'stranky_tagy');

define('TABLE_KLICOVA_SLOVA',TABLE_PREFIX.'klicova_slova');
define('TABLE_KLICOVA_SLOVA_POZICE',TABLE_PREFIX.'klicova_slova_pozice');

define('TABLE_PRESMEROVANI', TABLE_PREFIX.'presmerovani');
define('TABLE_PODPORA', TABLE_PREFIX.'podpora');

define('TABLE_ESHOP_KOSIK', TABLE_PREFIX.'eshop_kosik');
define('TABLE_ESHOP_OBJEDNAVKY', TABLE_PREFIX.'eshop_objednavky');
define('TABLE_ESHOP_OBJEDNAVKY_POLOZKY', TABLE_PREFIX.'eshop_objednavky_polozky');
define('TABLE_ESHOP_TYPY_PLATEB', TABLE_PREFIX.'eshop_typy_plateb');
define('TABLE_ESHOP_TYPY_DOPRAVY', TABLE_PREFIX.'eshop_typy_dopravy');
define('TABLE_ESHOP_OBJEDNAVKY_STAVY', TABLE_PREFIX.'eshop_objednavky_stavy');

define('TABLE_KALKULACE', TABLE_PREFIX.'kalkulace');
define('TABLE_KALKULACE_POLOZKY', TABLE_PREFIX.'kalkulace_polozky');




?>