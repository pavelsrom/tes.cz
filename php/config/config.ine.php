<?php

/**
 * @author Pavel Srom
 * @copyright 2014
 */


//--------------------------------------------------------------------
//KOMPLETNI MENU PANELU
//--------------------------------------------------------------------

//nacteni aktivnich modulu pro uzivatele a pro domenu
$mu = $login_obj->getModules();
$md = $domain->data['moduly'];

//prunik prav, zustanou jen ty spolecne pro uzivatele a pro domenu
$m = array_intersect($mu,$md); 

//aktivni moduly
$config_modules = config_modules($m);

$menu = array(
    //menu v casti Uzivatel
    "uzivatel"  => array(
        array(
            "id"        => "nastaveni-uzivatele",
            "icon"      => "nastaveni_uzivatele",
            "nazev"     => TNASTAVENI_UZIVATELE,
            "aktivni"   => false,// MODULE_HOME,
            "odkaz"     => "#",
            "zobrazit"  => false
            ),
        array(
            "id"        => "odhlasit",
            "icon"      => "power-off",
            "nazev"     => TODHLASIT_SE,
            "aktivni"   => true,
            "odkaz"     => $domain->getUrl()."admin?logout",
            "target"    => true,
            "zobrazit"  => true
            ),
        array(
            "id"        => "do-cms",
            "icon"      => "desktop",
            "nazev"     => TVSTUP_DO_CMS,
            "aktivni"   => true,
            "odkaz"     => $vstup_do_administrace_url,
            "target"    => true,
            "zobrazit"  => true
            )
        ),
        
    //menu v casti Web
    "web"   => array(
        array(
            "id"        => "nastaveni-webu",
            "icon"      => "cogs",
            "nazev"     => TNASTAVENI_WEBU,
            "title"     => TNASTAVENI_WEBU." ".$nazev_webu,
            "aktivni"   => $login_obj->UserPrivilege('settings_view') && $login_obj->minPrivilege('admin'),
            "odkaz"     => get_ajax_url('home','settings.load.form',$config_modules),
            "zobrazit"  => true 
            ),
        array(
            "id"        => "preklady-webu",
            "icon"      => "terminal",
            "nazev"     => TPREKLADY_WEBU,
            "title"     => TPREKLADY_WEBU." ".$nazev_webu,
            "aktivni"   => $login_obj->UserPrivilege('settings_view'),
            "odkaz"     => get_ajax_url('home','translation.load.form',$config_modules),
            "zobrazit"  => false 
            ),
        array(
            "id"        => "navstevnost-webu",
            "icon"      => "bar-chart",
            "nazev"     => TNAVSTEVNOST_WEBU,
            "title"     => TNAVSTEVNOST_WEBU." ".$nazev_webu,
            "aktivni"   => MODULE_STATISTICS && $login_obj->UserPrivilege('statistics_view'),
            "odkaz"     => get_ajax_url('home','dashboard.graph.visits',$config_modules),
            "zobrazit"  => false 
            ),
        array(
            "id"        => "najit-stranku",
            "icon"      => "search",
            "nazev"     => TNAJIT_STRANKU,
            "aktivni"   => true,
            "odkaz"     => get_ajax_url('inlineedit','find.pages.form',$config_modules),
            "zobrazit"  => true 
            ),
        ),
    
    //menu v casti Stranka
    "stranka" => array(
        array(
            "id"        => "editace-stranky",
            "icon"      => "cog",
            "nazev"     => TNASTAVENI_STRANKY,
            "title"     => TNASTAVENI_STRANKY." ".$nazev_stranky,
            "aktivni"   => ine_has_privilege($typ) && $login_obj->UserPrivilege('content_edit'),
            "odkaz"     => get_ine_odkaz_editace_stranky($id_stranky, $typ, $config_modules),
            "zobrazit"  => true 
            ),
        array(
            "id"        => "zaloha-obsahu",
            "icon"      => "life-ring",
            "nazev"     => TZALOHA_OBSAHU,
            "title"     => TZALOHA_OBSAHU." ".$nazev_stranky,
            "aktivni"   => ine_has_privilege($typ) && $login_obj->UserPrivilege('content_edit'),
            "odkaz"     => get_ajax_url('wet','content.backup.load.form', $config_modules),
            "zobrazit"  => true 
            ),
            /*
        array(
            "id"        => "refresh-stranky",
            "icon"      => "refresh",
            "nazev"     => TOBNOVIT_STRANKU,
            "aktivni"   => true,
            "odkaz"     => "",
            "zobrazit"  => true 
            ),
            */
        
        array(
            "id"        => "smazat-stranku",
            "icon"      => "trash-o",
            "nazev"     => TSMAZAT_STRANKU,
            "title"     => TSMAZAT_STRANKU." ".$nazev_stranky,
            "aktivni"   => !in_array($typ,array('uvodni','404')) && ine_has_privilege($typ) && $login_obj->UserPrivilege('content_delete'),
            "odkaz"     => get_ajax_url('inlineedit','delete.page.load.form',$config_modules),//get_ine_odkaz_smazat_stranku($id_stranky, $typ, $config_modules),
            "zobrazit"  => true 
            ),
            
        array(
            "id"        => "navstevnost-stranky",
            "icon"      => "bar-chart",
            "nazev"     => TNAVSTEVNOST_STRANKY,
            "title"     => TNAVSTEVNOST_STRANKY." ".$nazev_stranky,
            "aktivni"   => MODULE_STATISTICS && $login_obj->UserPrivilege('statistics_view'),
            "odkaz"     => get_ajax_url('home','dashboard.graph.visits',$config_modules),
            "zobrazit"  => false 
            ),
        array(
            "id"        => "pridat-stranku",
            "icon"      => "plus-circle",
            "nazev"     => TPRIDAT_STRANKU,
            "aktivni"   => MODULE_PAGES && $login_obj->UserPrivilege('content_add'),
            "odkaz"     => get_ajax_url('pages','load.form',$config_modules),
            "zobrazit"  => true 
            ),
        array(
            "id"        => "prejit-na-stranku",
            "icon"      => "search",
            "nazev"     => TVYHLEDAT_STRANKU,
            "aktivni"   => true,
            "odkaz"     => get_ajax_url('pages','find.load.form',$config_modules),
            "zobrazit"  => false 
            ),   
        )
);

?>