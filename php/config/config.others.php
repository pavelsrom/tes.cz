<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

define('ID_PRAVA_PRIVATNI', 10);

define('MULTILANGUAGE_WEB', true); //indikuje zda je na webu vice jazyku nez jeden

define('ERROR_ADMIN_EMAIL', 'pavelsrom@volny.cz');

//nastaveni odesilani posty
/*
define("SMTP_AKTIVNI", 1);
define('SMTP_HOST', 'smtp.profiwebik.cz');
define('SMTP_LOGIN', 'test@profiwebik.cz');
define('SMTP_HESLO', 'Te1009');
define('SMTP_PORT', '25');
define('SMTP_SECURE', 'ssl');
*/

define("SMTP_AKTIVNI", 1);
define('SMTP_HOST', '');
define('SMTP_LOGIN', '');
define('SMTP_HESLO', '');
define('SMTP_PORT', '25'/*'587'*/);
define('SMTP_SECURE', 'ssl');


//nastaveni pro pripojeni na ftp server
define("FTP_PORT", 21);
define("FTP_HOST", "");
define("FTP_LOGIN", "");
define("FTP_PASSWORD", "");


//GOPAY nastaveni
define("GOPAY_OSTRY_PROVOZ", false);
define("GOPAY_URL", GOPAY_OSTRY_PROVOZ ? "https://gate.gopay.cz/api/" : "https://gw.sandbox.gopay.com/api/");
define("GOPAY_GOID", GOPAY_OSTRY_PROVOZ ? "" : "");
define("GOPAY_CLIENT_ID", GOPAY_OSTRY_PROVOZ ? "" : "");
define("GOPAY_CLIENT_SECRET", GOPAY_OSTRY_PROVOZ ? "" : ""); 


//standarni jmeno odesilatele v redakcnim systemu
define('FROM_ADMIN_NAME', 'bornitflexi.cz');
define('FROM_ADMIN_EMAIL', SMTP_LOGIN);

define('META_AUTOR', 'Profiwebik.cz');

define('MAX_BAD_LOGIN', 3);
define('SHOW_ERRORS', true);
define('SEND_SQL_ERROR', true);
define('SEND_FATAL_ERROR', true);
define('SEND_WARNING', true);
define('SEND_NOTICE', false);
define('SEND_HACKING', false);
define('BLOCKED_LOGIN', 0);
define('DEBUG_SQL_QUERY', false);

define('CACHE_ACTIVE',true);
define('CACHE_INTERVAL',20); //v minutach


?>