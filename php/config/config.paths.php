<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

define('PROTOKOL', isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://'); 
define('RELATIVE_PATH', '/');

//--------------------------------------
// ZAKLADNI ADRESARE PRO CMS
//--------------------------------------
define('DIRNAME_IMG', 'img/');
define('DIRNAME_SMILES', DIRNAME_IMG.'/smiles/');
define('DIRNAME_DOMAINS', 'domains/');
define('DIRNAME_MODULES', 'php/modules/');
define('DIRNAME_DB_BACKUP', 'db_backup/');


//--------------------------------------
// ZAKLADNI ADRESARE PRO SPRAVU DOMENY
//--------------------------------------

define('USER_DIRNAME_IMG', 'img/');
define('USER_DIRNAME_CLASSES', 'classes/');
define('USER_DIRNAME_USERUPLOADS', 'userfiles/');
define('USER_DIRNAME_JS', 'js/');
define('USER_DIRNAME_CSS', 'css/');
define('USER_DIRNAME_GALLERY', 'galerie/');
define('USER_DIRNAME_GALLERY_MINI', 'galerie/male/');
define('USER_DIRNAME_GALLERY_STREDNI', 'galerie/stredni/');
define('USER_DIRNAME_GALLERY_MAXI', 'galerie/velke/');
define('USER_DIRNAME_ATTACHMENT', 'prilohy/');
define('USER_DIRNAME_PDF', 'pdf/');
define('USER_DIRNAME_MP3', 'mp3/');
define('USER_DIRNAME_NEWS', 'novinky/');
define('USER_DIRNAME_EDITBOXES', 'editboxy/');
define('USER_DIRNAME_EDITBOXES_MINI', USER_DIRNAME_EDITBOXES.'male/');
define('USER_DIRNAME_EDITBOXES_MAXI', USER_DIRNAME_EDITBOXES.'velke/');

define('USER_DIRNAME_CALENDAR', 'kalendar/');
define('USER_DIRNAME_ARTICLES', 'clanky/');
define('USER_DIRNAME_ARTICLES_MINI', USER_DIRNAME_ARTICLES.'male/');
define('USER_DIRNAME_ARTICLES_MAXI', USER_DIRNAME_ARTICLES.'velke/');
define('USER_DIRNAME_ARTICLES_BOX', USER_DIRNAME_ARTICLES.'box/');
define('USER_DIRNAME_ARTICLES_PEREX', USER_DIRNAME_ARTICLES.'perex/');
define('USER_DIRNAME_ESHOP', 'eshop/');
define('USER_DIRNAME_ESHOP_MINI', USER_DIRNAME_ESHOP.'male/');
define('USER_DIRNAME_ESHOP_STREDNI', USER_DIRNAME_ESHOP.'stredni/');
define('USER_DIRNAME_ESHOP_MAXI', USER_DIRNAME_ESHOP.'velke/');


define('USER_DIRNAME_STRANKY', 'stranky/');
define('USER_DIRNAME_SABLONY', 'sablony/');
define('USER_DIRNAME_STRANKY_MINI', USER_DIRNAME_STRANKY.'male/');
define('USER_DIRNAME_STRANKY_MAXI', USER_DIRNAME_STRANKY.'velke/');

define('USER_DIRNAME_HLAVNI_OBRAZEK', 'hlavni_obrazek/');
define('USER_DIRNAME_HLAVNI_OBRAZEK2', 'hlavni_obrazek2/');

?>