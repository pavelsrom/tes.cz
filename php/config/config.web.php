<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

//urcuje zda se bude v inline editaci zobrazovat i plovouci ovladaci panel 
define('INLINE_EDIT_PLUS', 0);

//platnost cookies - urcuje po jakou dobu neni mozne opakovane hlasovat v ankete, nastaveno na mesic
define('INQUIRY_NO_VOTING', 3600 * 24 * 31);
define('INQUIRY_PREFIX', "anketa_");

define('DEFAULT_LANG', 'en');
define('LANG', (isset($_GET['lang'])?$_GET['lang']:DEFAULT_LANG));
define('AJAX_GATEWAY', "php/ajax/ajax.web.gateway.php?lang=".LANG."&action=");
    


?>