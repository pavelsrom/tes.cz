<?php

/**
 * @author Pavel Srom
 * @copyright 2010
 */


//------------------------------------
//KONFIGURACNI SOUBOR
//------------------------------------



//------------------------------------------------------
//  NASTAVENI CHOVANI SYSTEMU PRI VYSKYTU CHYBY 
//  - PRO SKRIPTY AJAXU A EXTERNI SKRIPTY
//------------------------------------------------------
define('CENTRAL_DOMAIN', 'localhost');
define('CENTRAL_DOMAIN_URL', PROTOKOL.CENTRAL_DOMAIN.RELATIVE_PATH);



//----------------------------------------------------------
//	NASTAVENI CMS
//----------------------------------------------------------

define('THEMES_DIR', 'php/layout/');
define('CMS_THEME', 'gray/');
define('CMS_THEME_PATH', THEMES_DIR.CMS_THEME);
define('CMS_LANG', (isset($_SESSION['jazyk'])?$_SESSION['jazyk']:'cz'));
define('WEB_LANG_ID',isset($_GET['weblang']) ? intval($_GET['weblang']) : 1); //vychozi 1 - cestina
define('DEFAULT_LANG', 'cz');



define('AJAX_GATEWAY_TEMPLATE', RELATIVE_PATH."php/ajax/ajax.system.gateway.php?weblang=".WEB_LANG_ID."&lang=".CMS_LANG."&module_id=MODULE_ID&module=MODULE_NAME&action=");


?>