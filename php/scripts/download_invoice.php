<?php

/**
 * @author Pavel Srom
 * @copyright 2009
 */

/*
https://www.betterminds.cz/invoice/18050106/ABCDE
*/

session_start();

error_reporting(E_ALL);
DEFINE('PRE_PATH','../../');
//nacteni konfigurace

require_once('../config/config.paths.php');
require_once('../config/config.web.php');
require_once('../config/config.db.php');
require_once('../config/config.others.php');


//nacteni knihoven
require_once('../functions/functions.system.php');
require_once('../functions/functions.web.php');
require_once('../functions/functions.error.handlers.php');

//nacteni systemovych trid

require_once('../classes/class.db.php');
require_once('../classes/class.domain.web.php');
require_once('../classes/class.private.pages.web.php');
require_once('../classes/class.url.web.php');

$db = new C_Db();	
$db->Connect();

error_settings();

$domain = new C_Domains();

if($domain->getId() == 0)
{
    Redirect('',503);
    exit;
}

define('WEB_LANG_ID', 1);

define('DOMAIN_ID',$domain->getId());
define('RELATIVE_URL_USER_DOMAIN', $domain->getRelativePath().$domain->getDir());
define('ABSOLUTE_URL_USER_DOMAIN', $domain->getUrl().$domain->getDir());

loadSettings(DOMAIN_ID,"",false);

//$private_pages = new C_PrivatePages($domain->getId());
$links = new C_Url();


//-----------------------------------------
//overovaci proces
//-----------------------------------------

$user_hash = get_request("user_hash");
$cislo_faktury = get_request("cislo_objednavky");

/*
https://www.betterminds.cz/invoice/18050106/ABCDE
*/

if($cislo_faktury == "" || $user_hash == '')
{
    Redirect($links->get_url("404"));
    exit;
}



$user = $db->get(TABLE_UZIVATELE, array("idUzivatele AS id","email"), "hash='".$db->secureString($user_hash)."'");

if($user === false)
{
    echo '<h2>Faktura neexistuje!</h2>';
    Redirect("",404);
    exit;
}




$obj_data = $db->query("SELECT *, DATE_FORMAT(zaplaceno,'%d.%m.%Y') AS vystaveno FROM ".TABLE_ESHOP_OBJEDNAVKY." WHERE cislo_faktury = '".$db->secureString($cislo_faktury)."' AND id_stav = 2 AND id_uzivatel = ".$user->id." LIMIT 1");

if($db->numRows($obj_data) == 0)
{
    echo '<h2>Faktura neexistuje!</h2>';
    Redirect("",404);
    exit;
}

$obj = $db->getObject($obj_data);

$polozky_obj_data = $db->query("SELECT * FROM ".TABLE_ESHOP_OBJEDNAVKY_POLOZKY." WHERE id_objednavka = ".$obj->id);
$html = "";
while($p = $db->getObject($polozky_obj_data))
{
    $html .= '<tr class="polozky vypis">
            <td colspan="3">'.$p->nazev.'</td>
            <th class="cena" style="font-weight: normal;">'.price($p->cena).'</th>
        </tr>'; 
}


$promenne = array(
    "[cislo]" => $cislo_faktury,
    "[vystaveno]" => $obj->vystaveno,
    "[dodavatel]" => $obj->udaje_dodavatele,
    "[odberatel_jmeno]" => $obj->jmeno_uzivatele,
    "[odberatel_email]" => $user->email,
    "[cena]" => price($obj->cena),
    "[paticka]" => PATICKA_FAKTURY,
    "[polozky]" => $html 
    );

$zprava = file_get_contents(PRE_PATH.$domain->getDir()."faktura.html");

foreach($promenne AS $znacka => $hodnota)
    $zprava = str_replace($znacka, $hodnota, $zprava);

$nazev_faktury = "faktura-".$cislo_faktury.".pdf";

require_once PRE_PATH.'vendor/autoload.php';



//$pdf = new mPDF('utf-8','A4');
/*
$pdf = new \Mpdf\Mpdf(['debug' => true,
    'allow_output_buffering' => true]);
*/
$pdf = new \Mpdf\Mpdf(); //margin left,right,top,bottom,header, footer
/*
$pdf->useSubstitutions = false;
$pdf->simpleTables = true;
*/
/*
$stylesheet = file_get_contents(PRE_PATH.$domain->getDir()."css/faktura.css");
$pdf->WriteHTML($stylesheet,1);
*/
$pdf->WriteHTML($zprava);

/*
header("Pragma: public");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Content-Disposition: attachment;filename=".$nazev_faktury);
header("Content-Transfer-Encoding: binary ");
*/

$pdf->output($nazev_faktury, \Mpdf\Output\Destination::DOWNLOAD);

exit;






$ext_data = explode('.',$url_knihy);
$ext = strtolower(end($ext_data));

header("Pragma: private");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Content-Type: application/pdf");
            
header("Content-Disposition: attachment;filename=".$nazev_faktury);
header("Content-Transfer-Encoding: binary ");
header('Content-Length: ' . strlen($obsah_faktury));

flush();
readfile($url_knihy);

exit;













?>