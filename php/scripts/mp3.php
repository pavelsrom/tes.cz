<?php

/**
 * @author Pavel Srom
 * @copyright 2009
 */

session_start();

error_reporting(E_ALL);
DEFINE('PRE_PATH','../../');
//nacteni konfigurace

require_once('../config/config.paths.php');
require_once('../config/config.web.php');
require_once('../config/config.db.php');
require_once('../config/config.others.php');


//nacteni knihoven
require_once('../functions/functions.system.php');
require_once('../functions/functions.web.php');
require_once('../functions/functions.error.handlers.php');

//nacteni systemovych trid

require_once('../classes/class.db.php');
require_once('../classes/class.domain.web.php');
require_once('../classes/class.private.pages.web.php');
require_once('../classes/class.url.web.php');

$db = new C_Db();	
$db->Connect();

error_settings();

$domain = new C_Domains();

if($domain->getId() == 0)
{
    Redirect('',503);
    exit;
}

define('WEB_LANG_ID', 1);

define('DOMAIN_ID',$domain->getId());
define('RELATIVE_URL_USER_DOMAIN', $domain->getRelativePath().$domain->getDir());
define('ABSOLUTE_URL_USER_DOMAIN', $domain->getUrl().$domain->getDir());


loadSettings(DOMAIN_ID,"",false);

$private_pages = new C_PrivatePages($domain->getId());
$links = new C_Url();


//-----------------------------------------
//overovaci proces
//-----------------------------------------

$produkt_hash = get_request("produkt_hash");
$user_hash = get_request("user_hash");

if($produkt_hash == "" || $user_hash == '')
{
    Redirect($links->get_url("404"));
    exit;
}


if($produkt_hash == 'demo')
{
    
    //overeni zda je uzivatel prihlasen k odberu newsletteru
    /*
    $id_uzivatel = $db->get(TABLE_EMAILY_PRIJEMCI,"idPrijemce","hash='".$db->secureString($user_hash)."'");
    
    if($id_uzivatel == 0)
    {
        $id_uzivatel = $db->get(TABLE_UZIVATELE,"idUzivatele","hash='".$db->secureString($user_hash)."' AND aktivni=1");
    }
    
    if($id_uzivatel > 0)
    {
        $url_knihy = PRE_PATH.$domain->getDir().USER_DIRNAME_USERUPLOADS."files/demo/kniha.pdf";
        $nazev_knihy = NAZEV_DEMO_KNIHY;
    }
    else
    {
        echo '<h2>Kniha neexistuje!</h2>';
        Redirect("",404);
        exit;
    }
    
    */
}
//bude se overovat zda je uzivatel prihlasen
/*else*/if($produkt_hash != '')
{
    //overit zda kniha existuje
    $k = $db->get(TABLE_ESHOP_ZAKOUPENE_KNIHY,array('mesic','rok'),"hash='".$db->secureString($produkt_hash)."'");
    
    $id_produkt = 0;
    if($k !== false)
    {
        $id_produkt = $db->get(TABLE_STRANKY,'idStranky',"mesic=".$k->mesic." AND rok=".$k->rok.' AND zobrazit=1');
    }
    
    $id_uzivatel = $db->get(TABLE_UZIVATELE,"idUzivatele","hash='".$db->secureString($user_hash)."'");
    $soubor = $db->get(TABLE_STRANKY,array("soubor","nazev"),"idStranky=".intval($id_produkt));
    //overit zda ma knihu zaplacenou
    
    if($id_produkt > 0 && $id_uzivatel > 0)
    {
        $url_mp3 = PRE_PATH.$domain->getDir().USER_DIRNAME_MP3.str_replace(".pdf", ".mp3", $soubor->soubor);
        $nazev_mp3 = $soubor->nazev;
    }
    else
    {
        echo '<h2>MP3 neexistuje!</h2>';
        Redirect("",404);
        exit;
    }
    
}


header("Pragma: private");
header("Expires: 0");
header('Content-Disposition: inline;filename="'.$nazev_mp3.'"');
header('Content-Type: audio/mpeg');
header('Cache-Control: no-cache');
header('Content-Transfer-Encoding: binary');
header('Accept-Ranges: bytes');
header('Content-Length: ' . filesize($url_mp3));

flush();
readfile($url_mp3);

exit;













?>