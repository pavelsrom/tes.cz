<?php 

error_reporting(E_ALL);
DEFINE('PRE_PATH','../../');
//nacteni konfigurace

require_once('../config/config.paths.php');
require_once('../config/config.web.php');
require_once('../config/config.db.php');
require_once('../config/config.others.php');


//nacteni knihoven
require_once('../functions/functions.system.php');
require_once('../functions/functions.web.php');
require_once('../functions/functions.error.handlers.php');

//nacteni systemovych trid

require_once('../classes/class.db.php');
require_once('../classes/class.domain.web.php');
require_once('../classes/class.private.pages.web.php');
require_once('../classes/class.url.web.php');

$db = new C_Db();	
$db->Connect();

$domain = new C_Domains();

if($domain->getId() == 0)
{
    Redirect('',503);
    exit;
}

define('WEB_LANG_ID', 1);

define('DOMAIN_ID',$domain->getId());
define('RELATIVE_URL_USER_DOMAIN', $domain->getRelativePath().$domain->getDir());
define('ABSOLUTE_URL_USER_DOMAIN', $domain->getUrl().$domain->getDir());



$id = get_int_get("id");

//type = html, url, img
$type = get_request("type", "html");

$produkt = $db->get(TABLE_STRANKY, array("obrazek", "nazev","datumAktualizoval AS aktualizace"), "id_serd = ".$id." AND typ='produkt'");

if($produkt->obrazek == "")
    $url = ABSOLUTE_URL_USER_DOMAIN."img/product-default-maly.jpg";
    else
    $url = ABSOLUTE_URL_USER_DOMAIN.USER_DIRNAME_ESHOP_MINI.$produkt->obrazek."?".str_replace(array(" ",":", "-"), "", $produkt->aktualizace);

if($type == 'url')
{
    echo $url;
    exit;
}
$img = '<img src="'.$url.'" alt="" title="'.$produkt->title.'" />';

if($type == 'img')
{
    echo $img;
    exit;
}




?>
<!DOCTYPE html>
<html lang='cs'>
<head>
    <title></title>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name='description' content='' />
</head>

<body style="text-align:center">

<?= $img ?>

</body>
</html>







