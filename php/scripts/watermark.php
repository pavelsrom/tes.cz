<?php

/**
 * @author Pavel Srom
 * @copyright 2009
 */
//error_reporting(E_ALL);
DEFINE('PRE_PATH','../../');
DEFINE('THEME_PATH','');
include_once('../config/config.paths.php');
include_once('../config/config.admin.php');
include_once('../config/config.others.php');
include_once('../config/config.db.php');

include_once('../functions/functions.system.php');
include_once('../functions/functions.error.handlers.php');
include_once('../classes/class.db.php');

$db = new C_Db();	
$db->Connect();

$idDomeny = get_int_request('domain');

$slozka = get_request('folder');
$ext = get_request('ext');
$file = get_request('file');

$hash = $db->get(TABLE_DOMENY,"hash", "zobrazit=1 AND idDomeny=".$idDomeny);

//file_put_contents("log.txt", $ext);

if($hash == "" || !in_array($slozka, array('stredni','velke')) || $file=='' || $ext == '')
    exit;

loadSettings($idDomeny,$hash);
$dir = getDir($idDomeny);

if($slozka == 'stredni')
    $img = PRE_PATH.$dir."/".USER_DIRNAME_GALLERY_STREDNI.$file.".".$ext;
    else
    $img = PRE_PATH.$dir."/".USER_DIRNAME_GALLERY_MAXI.$file.".".$ext;


$data = $db->Query("SELECT 
    HOUR(MAX(l.datum)) AS hodiny,
    MINUTE(MAX(l.datum)) AS minuty, 
    SECOND(MAX(l.datum)) AS sekundy, 
    MONTH(MAX(l.datum)) AS mesice, 
    DAY(MAX(l.datum)) AS dny, 
    YEAR(MAX(l.datum)) AS roky 
    FROM ".TABLE_LOG." AS l 
    WHERE l.idDomeny=".$idDomeny." 
        AND (l.typObjektu='galerie-nastaveni' OR l.typObjektu='nastaveni')
        AND akce='edit-settings'
    ORDER BY l.datum DESC
    LIMIT 1");
        
$WaterMarkText = "";
$R = $G = $B = "00";




$posledni_zmena = time();
if($db->numRows($data) == 1){
    $wm = $db->getAssoc($data);
    $WaterMarkText = VODOZNAK_TEXT;
    $barvaVodoznaku = BARVA_TEXTU_VE_VODOZNAKU;
    if(strlen($barvaVodoznaku) == 3)
        {
            $R = "0x".substr($barvaVodoznaku, 0, 1);
            $G = "0x".substr($barvaVodoznaku, 1, 1);
            $B = "0x".substr($barvaVodoznaku, 2, 1);
        }
    elseif(strlen($barvaVodoznaku) == 6)
        {
            $R = "0x".substr($barvaVodoznaku, 0, 2);
            $G = "0x".substr($barvaVodoznaku, 2, 2);
            $B = "0x".substr($barvaVodoznaku, 4, 2);
        }
    $posledni_zmena = mktime($wm['hodiny'], $wm['minuty'], $wm['sekundy'], $wm['mesice'], $wm['dny'], $wm['roky']);           $gmt_posledni_zmena = gmdate("D, d M Y H:i:s", $posledni_zmena)." GMT";  
}


if(!file_exists($img))
{
    Redirect('',404);
    exit;
}

//file_put_contents("log.txt", $r.$G.$B);

   
header('Content-Type: image/jpeg');		
Header("Cache-Control: must-revalidate");
header("Pragma: private");
$offset = 60 * 60 * 24 * 7; //tyden
$ExpStr = "Expires: " . gmdate("D, d M Y H:i:s", time() + $offset) . " GMT";
Header($ExpStr);

if(isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) && $_SERVER['HTTP_IF_MODIFIED_SINCE'] == $gmt_posledni_zmena){
	header('Last-Modified: '.$_SERVER['HTTP_IF_MODIFIED_SINCE'],true,304);
	exit;
	}

	
Header("Last-Modified: ".$gmt_posledni_zmena);
Header("Cache-Control: public");



list($width, $height) = getimagesize($img);
$image_p = imagecreatetruecolor($width, $height);
$image = imagecreatefromjpeg($img);
imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width, $height, $width, $height);


$color = imagecolorallocate($image_p, $R, $G, $B);
$font = '../font/verdana.ttf';
$font_size = 12;
imagettftext($image_p, $font_size, 0, $width/2 - (strlen($WaterMarkText) * 4), $height - 20, $color, $font, $WaterMarkText); 

imagejpeg($image_p, null, 80);
imagedestroy($image);
imagedestroy($image_p);

exit;


?>