<?php

/**
 * @author Pavel Srom
 * @copyright 2010
 */

if(!isset($_GET['Hlasuj'])) exit;
	
DEFINE('PRE_PATH','../../');	
//nacteni konfigurace
include_once('../config/config.db.php');
include_once('../config/config.admin.php');
require_once('../classes/function.system.php');
require_once('../classes/functions.error.handlers.php');
require_once('../classes/class.db.php');
require_once('../classes/class.domain.admin.php');
require_once('../classes/class.messages.php');



session_start();

$db = new C_Db();	
$db->Connect();

error_settings();


$domain = new C_Domains();
	
if(!$domain->exists()) exit;


	
$arr = explode('x',$_GET['Hlasuj']);
$idAnkety = $arr[0];
$idPolozky = $arr[1];
$path = $_SERVER['HTTP_REFERER']."#a".$idAnkety;

$ip = $_SERVER['REMOTE_ADDR'];
		
//overeni zda se jiz hlasovalo
if(count($arr)!=2 || !ctype_digit($idAnkety) || !ctype_digit($idPolozky) || $idAnkety<1 || $idPolozky<1) 
	Redirect($path);
	
	
//osetreni aktivni domeny pri prohlizeni celeho webu konkretni domeny
if(isset($_SESSION['page_preview_domain']) && $_SESSION['page_preview_domain']>0)
	$domain->getId() = $_SESSION['page_preview_domain'];

		
$res2 = $db->Query("SELECT a.idDomeny FROM ".TABLE_ANKETY." AS a 
	LEFT JOIN ".TABLE_ANKETY_OTAZKY." AS ap ON a.idAnkety=ap.idAnkety 
	WHERE a.idAnkety=".$idAnkety." 
		AND a.idDomeny=".$domain->getId());



if($db->numRows($res2)==0) Redirect($path);


//hlasovani
$message = new C_SystemMessage(INQUIRY_PREFIX.$idAnkety);

$res = $db->Query("SELECT * FROM ".TABLE_ANKETY_IP." WHERE ip='".$ip."' AND idAnkety=".$idAnkety);
$pocet = $db->numRows($res);
		

		
if($pocet > 0 || isset($_COOKIE['inquiry_'.$idAnkety])) {
	$message->add_error("Můžete hlasovat pouze jednou");
	Redirect($path);
	}


$res = $db->Query("UPDATE ".TABLE_ANKETY_OTAZKY." SET hlasy = hlasy + 1 WHERE idAnkety=".$idAnkety." AND idPolozky=".$idPolozky);

if($res) {
	setcookie( 'inquiry_'.$idAnkety, 'set' , time() + INQUIRY_NO_VOTING);
	$db->Query("INSERT INTO ".TABLE_ANKETY_IP." (idAnkety, ip) VALUES('".$idAnkety."', '".$ip."')");
	$message->add_ok("Váš hlas byl započítán, děkujeme");
	}
	else
	$message->add_error("Můžete hlasovat pouze jednou");

		
Redirect($path);

?>