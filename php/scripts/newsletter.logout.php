<?php

/**
 * @author Pavel Srom
 * @copyright 2010
 */

session_start();

define('PRE_PATH','');
define('SECURITY',1);
include_once('../config/config.paths.php');
include_once('../config/config.db.php');
include_once('../config/config.admin.php');
include_once('../config/config.others.php');
include_once('../functions/functions.system.php');

include_once('../classes/class.db.php');
include_once('../classes/class.domain.web.php');
include_once('../classes/class.messages.php');

if(!is_get('d') || !is_get('p') || !is_get('k')) 
    exit;




$db = new C_Db();	
$db->Connect();

$hash_prijemce = get_request("p");
$hash_domeny = get_request("d");
$idKampane = get_int_request('k');
if($idKampane <= 0) 
    exit;



$domain = new C_Domains($hash_domeny);
if($domain->getId()==0) 
    exit;


$prijemce = $db->get(TABLE_EMAILY_PRIJEMCI,array("idPrijemce AS id","email","idJazyka AS jazyk"),"hash='".$db->secureString($hash_prijemce)."' AND idDomeny=".$domain->getId());

if($prijemce == false) 
    exit;

$idPrijemce = $prijemce->id;

$db->update(TABLE_EMAILY_PRIJEMCI, array("stav" => 2),"idPrijemce=".$idPrijemce." LIMIT 1");

$idRelace = $db->get(TABLE_EMAILY_FRONTA,"id","idKampane=".$idKampane." AND idPrijemce=".$idPrijemce." AND odhlaseno = 0");
if($idRelace > 0)
    $db->query("UPDATE ".TABLE_EMAILY_KAMPANE." SET odhlaseno = odhlaseno + 1 WHERE idKampane=".$idKampane." AND idDomeny=".$domain->getId());
    
//zapocitani Otevreni emailu
$idRelace = $db->get(TABLE_EMAILY_FRONTA,"id","idKampane=".$idKampane." AND idPrijemce=".$idPrijemce." AND otevreno = 0");
if($idRelace > 0)
    $db->query("UPDATE ".TABLE_EMAILY_KAMPANE." SET otevreno = otevreno + 1 WHERE idKampane=".$idKampane." AND idDomeny=".$domain->getId());
    

$db->update(TABLE_EMAILY_FRONTA, array("odhlaseno" => 1,"otevreno" => 1),"idKampane=".$idKampane." AND idPrijemce=".$idPrijemce." LIMIT 1");
$stranka = $db->get(TABLE_STRANKY,array("idStranky AS id","url"),"idJazyka=".$prijemce->jazyk." AND idDomeny=".$domain->getId()." AND typ='email'");

if($stranka == false)
    exit;

$jazyk = $db->get(TABLE_JAZYKY,"jazyk","idJazyka=".$prijemce->jazyk);

$url = UrlPage($stranka->url,$stranka->id, $jazyk);
//$url = $domain->getUrl().$url;

loadTranslations($jazyk, true);

$userSystemMessage = new C_SystemMessage('user');

$userSystemMessage->add_ok(sprintf(TEMAIL_BYL_ODHLASEN_Z_ODBERU,$prijemce->email));


redirect($url);
	
?>