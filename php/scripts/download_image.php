<?php

/**
 * @author Pavel Srom
 * @copyright 2010
 */


if(!isset($_GET['id'])) exit;

$idPolozky = intval($_GET['id']);

if($idPolozky<1) exit;

DEFINE('PRE_PATH','../../');
require_once('../config/config.db.php');
require_once('../config/config.paths.php');
require_once('../config/config.admin.php');
require_once('../functions/functions.admin.php');
require_once('../functions/functions.system.php');
require_once('../functions/functions.error.handlers.php');
require_once('../classes/class.db.php');
require_once('../classes/class.domain.admin.php');
require_once('../classes/class.login.php');



session_start();

$db = new C_Db();	
$db->Connect();

error_settings();

$domain = new C_Domains();
$idDomain = $domain->getId();

if($idDomain == 0) exit;

$gal_dir = PRE_PATH.$domain->getDir().USER_DIRNAME_GALLERY_MAXI;

$data = $db->Query("SELECT obrazek AS soubor FROM ".TABLE_STRANKY." AS g 
    WHERE g.idDomeny = ".$domain->getId()."
        AND g.idStranky=".$idPolozky."
        AND g.zobrazit= 1
        AND g.typ = 'galerie-fotka'
    LIMIT 1
    ");
    
if($db->numRows($data)==0)
{
    Redirect('','404');
    exit;
}
    
$img = $db->getAssoc($data);
$obrazek = $img['soubor'];

$ext_data = explode('.',$obrazek);
$ext = strtolower(end($ext_data));


$obrazek_abs = $gal_dir.$obrazek;

/*
echo $obrazek_abs;
if(file_exists($obrazek_abs)) echo "ok";
exit;
*/


header("Pragma: private");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");

if($ext=='gif'){
	header("Content-Type: image/gif");
	}
elseif($ext=='jpg' || $ext=='jpeg'){
	header("Content-Type: image/jpeg");
	}
elseif($ext=='png'){
	header("Content-Type: image/png");
	}
        
header("Content-Disposition: attachment;filename=".$obrazek);
header("Content-Transfer-Encoding: binary ");
header('Content-Length: ' . filesize($obrazek_abs));

flush();
readfile($obrazek_abs);

exit;



?>