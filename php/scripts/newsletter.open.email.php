<?php

/**
 * @author Pavel Srom
 * @copyright 2011
 */


define('PRE_PATH','');
define('SECURITY',1);
include_once('../config/config.paths.php');
include_once('../config/config.db.php');
include_once('../config/config.admin.php');
include_once('../config/config.others.php');
include_once('../functions/functions.system.php');
include_once('../classes/class.db.php');
include_once('../classes/class.domain.web.php');


error_reporting(E_ALL);

if(!is_get('d') || !is_get('p') || !is_get('k')) 
    exit;
   
$db = new C_Db();	
$db->Connect();
$db->show_error();


$prijemce = get_request("p");
$hash_prijemce = $db->secureString($prijemce);
$hash_domeny = get_request("d");
$idKampane = get_request("k");



if($idKampane <= 0) 
    exit;

$domain = new C_Domains($hash_domeny);

if($domain->getId()==0) exit;

//overeni existence prijemce a prislusnosti k dane domene 
$idPrijemce = $db->get(TABLE_EMAILY_PRIJEMCI,"idPrijemce", "idDomeny=".$domain->getId()." AND hash='".$hash_prijemce."'");


if($idPrijemce == 0)
    exit;

$idRelace = $db->get(TABLE_EMAILY_FRONTA,"id","idKampane=".$idKampane." AND idPrijemce=".$idPrijemce." AND otevreno = 0");
if($idRelace > 0)
    $db->query("UPDATE ".TABLE_EMAILY_KAMPANE." SET otevreno = otevreno + 1 WHERE idKampane=".$idKampane." AND idDomeny=".$domain->getId());


$db->update(TABLE_EMAILY_FRONTA, array("otevreno" => 1),"idKampane=".$idKampane." AND idPrijemce=".$idPrijemce." LIMIT 1");



//pokud existuje vrati kod 200 a ulozi priznak ze byl email otevren
header("Content-Type: image/jpeg");
Header("Pragma: no-cache");
Header("Cache-control: no-cache");
Header("Expires: ".GMDate("D, d m Y H:i:s")." GMT");
echo file_get_contents('../../img/blank.gif');

exit;




?>