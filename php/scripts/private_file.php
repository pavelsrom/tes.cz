<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

session_start();

//error_reporting(E_ALL);
error_reporting(0);


//nacteni konfigurace
include_once('../config/config.paths.php');
include_once('../config/config.web.php');
include_once('../config/config.db.php');
include_once('../config/config.others.php');

include_once('../functions/functions.system.php');

include_once('../classes/class.db.php');
include_once('../classes/class.domain.web.php');
include_once('../classes/class.private.pages.web.php');



$subdir = get_request("subdir");
$id_domeny = get_int_get("domain");
$file = get_request('file');

if($subdir == "" || $id_domeny == 0 || $file == "")
{
    redirect("",404);
    exit();
}

$subdir = is_get("subdir") ? "userfiles/".$subdir."/privatni/" : "userfiles/privatni/";




define("IMG_URL",$subdir);
define('PRE_PATH','');
define('SECURITY',1);

$filename = IMG_URL.$file;

$db = new C_Db();	
$db->Connect();

$domain = new C_Domains("", $id_domeny);

if($domain->getId() == 0)
{
    redirect("",404);
    exit();
}

GetActiveModules();

$private_pages = new C_PrivatePages($domain->getId());

//http://www.carmeo.cz/pavel/php/scripts/private_file.php?domain=1&file=info.exe
//http://carmeo.cz/pavel/domains/0001/userfiles/files/info.exe

//zjisteni zda se soubor nachazi v konkretni privatni skupine, nebo je dostupny pro vsechny privatni skupiny = je umisten v rootu slozky privatni
$file_parts = explode("/",$file);
$id_privatni_skupiny = 0;
if(count($file_parts) > 1)
    $id_privatni_skupiny = intval($file_parts[0]);

$filename = "../../".$domain->getDir().$filename;
/*
echo intval(file_exists($filename));
echo intval((!$private_pages->has_access_user_to_group($id_privatni_skupiny) && $id_privatni_skupiny > 0) || $id_privatni_skupiny == 0);
echo intval($private_pages->is_logged());
echo intval(MODULE_PRIVATE_PAGES);
*/

//pokud uzivatel nema opravneni k dane skupine, pak se vraci 404
if(!MODULE_PRIVATE_PAGES || !$private_pages->is_logged() || (!$private_pages->has_access_user_to_group($id_privatni_skupiny) && $id_privatni_skupiny > 0) || !file_exists($filename))
{
    Redirect("",404);
    exit();
}


//sjisteni mime type souboru
$koncovka=strtolower(pathinfo($filename, PATHINFO_EXTENSION));
$content_types = array(
        "html" => "text/html",
        "htm" => "text/html",
        "txt"  => "text/plain",
        "doc"  => "application/msword",
        "docx" => "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
        "xls"  => "application/excel",
        "xlsx" => "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
        "pdf"  => "application/pdf",
        "xml"  => "text/xml",
        "jpg"  => "image/jpeg",
        "jpeg" => "image/jpeg",
        "jpe" => "image/jpeg",
        "gif"  => "image/gif",
        "png"  => "image/png",
        "zip" => "application/zip",
        "rar" => "application/x-rar-compressed",
        "avi" => "video/avi",
        "ogg" => "video/ogg",
        "bmp" => "image/bmp",
        "mid" => "audio/midi",
        "midi" => "audio/midi",
        "mov" => "video/quicktime",
        "mp2" => "audio/mpeg",
        "mp3" => "audio/mpeg3",
        "mp4" => "video/mp4",
        "mpeg"=> "video/mpeg",
        "mpg" => "video/mpeg",
        "ppt" => "application/mspowerpoint",
        "pps" => "application/mspowerpoint",
        "pptx"=> "application/vnd.openxmlformats-officedocument.presentationml.presentation",
        "ppsx"=> "application/vnd.openxmlformats-officedocument.presentationml.slideshow",
        "psd" => "application/octet-stream",
        "tif" => "image/tiff",
        "tiff"=> "image/tiff",
        "wav" => "audio/wav",
        "bin" => "application/octet-stream",
        "exe" => "application/x-msdownload"
        );  


$mime_type = isset($content_types[$koncovka]) ? $content_types[$koncovka] : "application/x-msdownload";



//zjisteni posledni zmeny souboru a cacheovani obsahu souboru
$posledni_zmena = filemtime($filename);
$gmt_posledni_zmena = gmdate("D, d M Y H:i:s", $posledni_zmena)." GMT"; 

header('Content-Type: '.$mime_type);	
header('Content-Disposition: filename='.basename($filename));	
Header("Cache-Control: must-revalidate");
header("Pragma: private");
$offset = 60 * 60 * 24 * 7; //tyden
$ExpStr = "Expires: " . gmdate("D, d M Y H:i:s", time() + $offset) . " GMT";
Header($ExpStr);

if(isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) && $_SERVER['HTTP_IF_MODIFIED_SINCE'] == $gmt_posledni_zmena){
	header('Last-Modified: '.$_SERVER['HTTP_IF_MODIFIED_SINCE'],true,304);
	exit;
	}

	
Header("Last-Modified: ".$gmt_posledni_zmena);
Header("Cache-Control: public");


//vypsani obsahu souboru
echo file_get_contents($filename);


?>