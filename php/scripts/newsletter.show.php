<?php

/**
 * @author Pavel Srom
 * @copyright 2010
 */

define('PRE_PATH','');
define('SECURITY',1);
include_once('../config/config.paths.php');
include_once('../config/config.db.php');
include_once('../config/config.admin.php');
include_once('../config/config.others.php');
include_once('../functions/functions.system.php');
include_once('../classes/class.db.php');
include_once('../classes/class.domain.web.php');
include_once('../classes/class.db.php');
include_once('../classes/class.newsletters.php');

error_reporting(E_ALL);

if(!is_get('d') || !is_get('p') || !is_get('k')) 
    exit;
    
$db = new C_Db();	
$db->Connect();
$db->show_error();


$prijemce = get_request("p");
$hash_prijemce = $db->secureString($prijemce);
$hash_domeny = get_request("d");
$idKampane = get_request("k");
$html = get_int_get("html");

if($idKampane <= 0) 
    exit;

$domain = new C_Domains($hash_domeny);

if($domain->getId()==0) exit;

loadSettings($domain->getId());

$user = $db->get(TABLE_EMAILY_PRIJEMCI,array("email, idPrijemce AS id"),"hash='".$hash_prijemce."'");
$email = $user->email;
$idPrijemce = $user->id;

if($email == "")
    exit; 


$idDomeny = $domain->getId();

$kampan = C_Campaign::get_instance($idKampane);
$template = $kampan->get_template_for_user($email);

if($template === false)
    exit;

$newsletter = new C_Newsletter($kampan->get_content_html(), $kampan->get_content_text(), $kampan->get_subject(), $kampan->get_lang());

$newsletter->prepare_content($template, true);
$html_verze = $newsletter->get_content_html();


$idRelace = $db->get(TABLE_EMAILY_FRONTA,"id","idKampane=".$idKampane." AND idPrijemce=".$idPrijemce." AND otevreno = 0");
if($idRelace > 0)
    $db->query("UPDATE ".TABLE_EMAILY_KAMPANE." SET otevreno = otevreno + 1 WHERE idKampane=".$idKampane." AND idDomeny=".$domain->getId());

$db->update(TABLE_EMAILY_FRONTA, array("otevreno" => 1),"idKampane=".$idKampane." AND idPrijemce=".$idPrijemce." LIMIT 1");

echo $html_verze;
exit;


?>