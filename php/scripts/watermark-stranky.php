<?php

/**
 * @author Pavel Srom
 * @copyright 2009
 */
//error_reporting(E_ALL);
DEFINE('PRE_PATH','../../');
DEFINE('THEME_PATH','');
include_once('../config/config.paths.php');
include_once('../config/config.admin.php');
include_once('../config/config.others.php');
include_once('../config/config.db.php');

include_once('../functions/functions.system.php');
include_once('../functions/functions.error.handlers.php');
include_once('../classes/class.db.php');
include_once('../classes/class.upload.php');


$db = new C_Db();	
$db->Connect();

$idDomeny = get_int_request('domain');

$slozka = get_request('folder');
$ext = get_request('ext');
$file = get_request('file');

$dir = "domains/0001";

if($slozka == 'male')
    $img = PRE_PATH.$dir."/".USER_DIRNAME_STRANKY_MINI.$file.".".$ext;
    
if($slozka == 'velke')
    $img = PRE_PATH.$dir."/".USER_DIRNAME_STRANKY_MAXI.$file.".".$ext;


if(!file_exists($img))
{
    Redirect('',404);
    exit;
}

$posledni_zmena = filemtime($img);         
$gmt_posledni_zmena = gmdate("D, d M Y H:i:s", $posledni_zmena)." GMT";  

//echo date("d.m.Y", $posledni_zmena);
//file_put_contents("log.txt", $r.$G.$B);

  
header('Content-Type: image/'.$ext);		
Header("Cache-Control: must-revalidate");
header("Pragma: private");
$offset = 60 * 60 * 24 * 7; //tyden
$ExpStr = "Expires: " . gmdate("D, d M Y H:i:s", time() + $offset) . " GMT";
Header($ExpStr);

if(isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) && $_SERVER['HTTP_IF_MODIFIED_SINCE'] == $gmt_posledni_zmena){
	header('Last-Modified: '.$_SERVER['HTTP_IF_MODIFIED_SINCE'],true,304);
	exit;
	}

	
Header("Last-Modified: ".$gmt_posledni_zmena);
Header("Cache-Control: public");


$watermark = PRE_PATH.$dir."/img/watermark1.png";

$handle = new Upload($img);
$handle->image_watermark = $watermark;
echo $handle->process();

exit;





/*
$wm = new Watermark($img);
$wm->setOpacity(.4)->setPosition(Watermark::POSITION_CENTER;
$wm->withImage($watermark);
*/


/*
$imagesize_watermark = getimagesize($watermark);

if($ext == 'png')
{
    $img = imagecreatefrompng($img);
    
    imagealphablending($img, false);
    imagesavealpha($img, true);
    
}
else
    $img = imagecreatefromjpeg($img);



if ($imagesize_watermark && $imagesize_watermark[2] <= 3) {
    $img_watermark = ($imagesize_watermark[2] == 2 ? imagecreatefromjpeg($watermark) : ($imagesize_watermark[2] == 1 ? imagecreatefromgif($watermark) : imagecreatefrompng($watermark)));

    
    imagealphablending($img_watermark, false);
    imagesavealpha($img_watermark, true);

    imagecopymerge($img, $img_watermark, (imagesx($img) - $imagesize_watermark[0]) / 2, (imagesy($img) - $imagesize_watermark[1]) / 2, 0, 0, $imagesize_watermark[0], $imagesize_watermark[1], 20);

    if($ext == 'png')
        imagepng($img);
        else
        imagejpeg($img);

    imagedestroy($img_watermark);


}
else
{
    Redirect('',404);
    exit;
}
*/




exit;


?>