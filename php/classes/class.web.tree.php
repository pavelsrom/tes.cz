<?php

/**
 * @author Pavel Srom
 * @copyright 2010
 */


class C_WebTree{
    
    private $menu_html;
    
    private $aktivni_stranka;
    private $aktivni_stranka_menu;
    
    private $navigation;
    
    private $id_stranek_v_url;
    
    //obsahuje informace o stromu
    private $data;
    
    public function __construct(){
        global $db;
        global $domain;
        global $page;
        
        $this->menu_html = "";
        $this->navigation = array();
        
        $data = $db->query("SELECT idStranky AS id, idRodice AS rodic, nazevProMenu AS menu_nazev, typ, doMenu AS do_menu, zobrazit AS aktivni
            FROM ".TABLE_STRANKY."
            WHERE idDomeny=".$domain->getId()."
                AND idJazyka=".WEB_LANG_ID."
                AND zobrazit=1
                AND doMenu=1
            ORDER BY priorita, idStranky
            ");
        
        while($s = $db->getObject($data))
            $this->data[$s->id] = $s;
        
        
        if(in_array($page->get_type(), array('clanek')))
        {
            foreach($this->data AS $ids => $l)
                if($ids == $page->get_id() && $l['typ'] == 'clanek')
                    $this->aktivni_stranka_menu = array('id' => $l['rodic'], 'aktivni' => true);
        }
        elseif(in_array($page->get_type(), array('novinka')))
        {
            foreach($this->data AS $ids => $l)
                if($l['typ'] == 'novinky')
                    $this->aktivni_stranka_menu = array('id' => $ids, 'aktivni' => true);
        }
        elseif(in_array($page->get_type(), array('produkt')))
        {
            foreach($this->data AS $ids => $l)
                if($ids == $page->get_id() && $l['typ'] == 'produkt')
                    $this->aktivni_stranka_menu = array('id' => $l['rodic'], 'aktivni' => true);
        }
        elseif(in_array($page->get_type(), array('galerie-fotka', 'galerie', 'galerie-slozka')))
        {
            foreach($this->data AS $ids => $l)
                if($l['typ'] == 'stranka-galerie')
                    $this->aktivni_stranka_menu = array('id' => $ids, 'aktivni' => true);
        }
        else
        $this->aktivni_stranka_menu = array('id' => $page->get_id(), 'aktivni' => false);
        $this->aktivni_stranka = $page->get_id(); 
        $this->SetActiveBranch($this->aktivni_stranka);
        
       //echo $this->aktivni_stranka;
       
    }
    
    //allLinks oznacuje zda ma byt cele menu rozbaleno, budou odhaleny vsechny odkazy menu, v opacnem pripade bude otevreno jen to podmenu jehoz hlavni polozka je aktivni
    public function ShowMenu($idRodice=0, $ul_class="", $ul_id="",$allLinks=1,$jquery_menu = false)
    {
        $this->menu_html = "";
        $this->GetMenu($idRodice,$ul_class, $ul_id,$allLinks,$jquery_menu);
        return $this->menu_html;
        
    }
    
    
    private function GetMenu($idRodice=0, $ul_class="", $ul_id="", $allLinks=1, $jquery_menu = true){
       global $links; 
       global $private_pages;
       

       
        $pocet_polozek = $this->CountItems($idRodice);
        if($pocet_polozek==0) return;
        
        $ul_class = $ul_class != "" ? "class='".$ul_class."'" : "";
        $ul_id = $ul_id != "" ? "id='".$ul_id."'" : "";
            
        $this->menu_html .= "<ul ".$ul_class." ".$ul_id.">\n";
        
        $i=1;
        foreach($this->data AS $idStranky => $l){
            if(!$this->isAllowTyp($l['typ']) || $l['rodic']!=$idRodice || $l['do_menu']==0 || $l['aktivni']==0 /*||
                (!MODULE_GALLERY && in_array($l['typ'], array('stranka-galerie','galerie-slozka','galerie-fotka','galerie'))) ||
                (!MODULE_ARTICLES && in_array($l['typ'], array('clanky','clanek','archiv-clanku'))) ||
                (!MODULE_NEWS && in_array($l['typ'], array('novinky','novinka')))*/
                ) continue;
            
            if(!$private_pages->has_access($idStranky))
                continue;
            
            $class="";           
            if($i==1 && $idRodice == 0) $class .= " home";
            if($i==$pocet_polozek) $class .= " posledni";
            
            $class_a = "";
            if((in_array($idStranky, $this->id_stranek_v_url) && $jquery_menu) || ($idStranky == $this->aktivni_stranka_menu['id'] && $idStranky == $this->aktivni_stranka_menu['aktivni']))
                $class_a .= " active";
            elseif($idStranky == $this->aktivni_stranka_menu['id']) 
                $class_a .= " active";
                
            if(!$jquery_menu && $this->isSubmenu($idStranky))
            {
                $class .= ' rozbalena';
                $l['menu_nazev'] = $l['menu_nazev']." <em>&rsaquo;</em>";
            }
                
            if($class!="") $class = "class = '".$class."'";
            
            $em = $i == 1 && $idRodice == 0 && $jquery_menu ? "<em></em>" : "";
            
            $nazev = $em == "" ? $l['menu_nazev'] : '<span class="nodisplay">'.$l['menu_nazev']."</span>".$em;
            
            if($idStranky==$this->aktivni_stranka_menu['id']){
                if($this->aktivni_stranka_menu['aktivni'] || $jquery_menu)
                    $odkaz = "<a href='".$l['url']."' class='".$class_a."'>".$nazev."</a>";
                else
                    $odkaz = "<span>".$nazev."</span>";
                }
                else
                $odkaz = "<a href='".$l['url']."' class='".$class_a."'>".$nazev."</a>";
             
            $this->menu_html .= "<li ".$class.">".$odkaz;
                
            if($allLinks==1 || $idStranky==$this->aktivni_stranka_menu['id'] || $this->isActiveLinkInBranch($idStranky))
                $this->GetMenu($idStranky,"","",$allLinks,$jquery_menu);
                
            $this->menu_html .= "</li>\n";
            
            $i++;
            }
        
        
        $this->menu_html .= "</ul>\n";

        
        
        
    }
    
    
    //vrati menu 1 urovne, tedy hloubky 0
    public function GetMenu1nd()
    {
        
        $result = array();
        
        foreach($this->data AS $l)
            if($this->isAllowTyp($l['typ'])  && $l['aktivni']==1 && $l['do_menu'] == 1 && $l['rodic'] == 0 && !in_array($l['typ'],array('404'))/* && 
            (
            in_array($l['typ'], array('uvodni','stranka')) ||
            ($l['typ'] == 'novinky' && MODULE_NEWS) || 
            (($l['typ'] == 'clanky' || $l['typ'] == 'archiv-clanku') && MODULE_ARTICLES) || 
            ($l['typ'] == 'stranka-galerie' && MODULE_GALLERY)
            )
            */
            )
                $result[] = array(
                    'nazev' => $l['menu_nazev'], 
                    'url' => $l['url'], 
                    'aktivni' => in_array($l['id_stranky'],$this->id_stranek_v_url),
                    'aktivni_klikaci' => in_array($l['id_stranky'],$this->id_stranek_v_url) && $l['id_stranky'] != $this->aktivni_stranka
                    );
        
        return $result;
    }
    
    //zobrazi menu 1 urovne
    public function ShowMenu1nd($ul_class="", $ul_id="")
    {
        $menu = $this->GetMenu1nd();
        $html = "";
        
        if(count($menu) == 0)
            return $html;
        
        if($ul_class != "")
            $ul_class = "class='".$ul_class."'";
            
        if($ul_id != "")
            $ul_id = "id='".$ul_id."'";
        
        $html .= "<ul ".$ul_class." ".$ul_id.">";
        
        foreach($menu AS $m)
        {
            $html .= '<li>';
            
            if($m['aktivni'] && $m['aktivni_klikaci'])
                $html .= "<a href='".$m['url']."' class='aktivni'>".$m['nazev']."</a>";
            elseif($m['aktivni'] && !$m['aktivni_klikaci'])
                $html .= "<span>".$m['nazev']."</span>";
            else
                $html .= "<a href='".$m['url']."'>".$m['nazev']."</a>";
                
            $html .= '</li>';    
        }
        
        $html .= "</ul>";
        
        return $html;
        
    }
    
    //zjisti zda je aktivni odkaz v dane vetvi stromu
    private function isActiveLinkInBranch($idRodice){
        
        foreach($this->data AS $idStranky => $l){
            if(!$this->isAllowTyp($l['typ']) || $l['rodic']!=$idRodice) continue;
            if($idStranky==$this->aktivni_stranka_menu['id'])
                return true;
            elseif($this->isSubmenu($idStranky)){
                $res = $this->isActiveLinkInBranch($idStranky);
                }
            else {
                $res = false;
                }
                
            if($res) return true;
            }
        
        return false;
        
    }
    
    
    //zjisti zda typ, ktery se ma vypsat v menu je povolenym typem
    private function isAllowTyp($typ){
        if($typ=='clanek' || $typ=='novinka' || $typ=='akce' || $typ=='produkt' || $typ=='kalendar-typ' || $typ=='404' || $typ=='vyhledavani') 
            return false;
            
        return true;
        
    }
    
    //zjisti zda ma polozka jeste podmenu
    private function isSubmenu($idStranky){

        foreach($this->data AS $l){
            if($this->isAllowTyp($l['typ']) && $l['rodic']==$idStranky && $l['do_menu']==1) return true;
          
            }
        
        return false;
    }
    
    
    //zjisti pocet polozek menu
    private function CountItems($idRodice=0, $hloubka=0){
        global $private_pages;
        
        $count = 0;
        
        foreach($this->data AS $ids => $l){
            if(!$this->isAllowTyp($l['typ']) || $l['rodic']!=$idRodice || $l['do_menu']==0 || !$private_pages->has_access($ids)) continue;
            $count++;
            
            }
            
        return $count;
        
    }
    
    public function ShowNavigation($navigation_delimiter = " &raquo; "){
        global $links;
        global $page;
        
        $this->GetNavigation($this->aktivni_stranka);

        $this->navigation = array_reverse($this->navigation);
        
        //print_r($this->navigation);
        
        $prefix = "";
        
        
        $uvodni_id = 0;
        
        foreach($this->data AS $idp => $p){
            
            if($p['typ'] == 'uvodni')
            {
                $uvodni_id = $idp;
                break;
            }
        }
        
        $pocet_drobecku = count($this->navigation);
        
        $navigace = array();
        
        if($page->get_type() == 'vyhledavani'){
            
            $navigace[] = "<a href='".$links->get_url($uvodni_id)."'>".$this->data[$uvodni_id]['menu_nazev']."</a>";
            $navigace[] = "<span>".$page->get('nadpis')."</span>";
            $result = $prefix.implode($navigation_delimiter, $navigace);
            return $result; 
        }
        
        if(isset($_GET['404']) || $page->get_type() == '404'){
    
            $navigace[] = "<span>".$page->get('nadpis')."</span>";
            $result = $prefix.implode($navigation_delimiter, $navigace);
            return $result; 
            }
        
        
        $typ = $this->data[$this->aktivni_stranka]['typ'];
        
        $i=1;
        
        $nav_arr = array();
        
        foreach($this->navigation AS $n){
            if($i==$pocet_drobecku)
                $nav_arr[] = "<span>".$this->data[$n]['menu_nazev']."</span>";
                else
                $nav_arr[] = "<a href='".$links->get_url($n)."'>".$this->data[$n]['menu_nazev']."</a>";
            
            $i++;
            }       
        
        if(count($nav_arr)==0)
            $nav_begin = "<span>".$this->data[$uvodni_id]['menu_nazev']."</span>";
            else
            $nav_begin = "<a href='".$links->get_url($uvodni_id)."'>".$this->data[$uvodni_id]['menu_nazev']."</a>";
        
        
        $navigace = array_merge(array($nav_begin), $nav_arr);
        
        return $prefix.implode($navigation_delimiter, $navigace);
        
        
    }
    
    
    private function GetNavigation($idStranky){

        if(isset($this->data[$idStranky]) && $this->data[$idStranky]['typ'] == 'uvodni') return;

        if(isset($this->data[$idStranky]))
        {
            $this->navigation[] = $idStranky;
            
            if($idStranky == 0)
                return;
                else
                $this->GetNavigation($this->data[$idStranky]['rodic']);
        }

        return;                 
        
    }    
    
    //nastavi do pole idecka stranek z navigace - vyuziva generator menu k urceni ktere stranky jsou aktivni
    private function SetActiveBranch($idStranky){
        
        if(isset($this->data[$idStranky]))
        {
            $this->id_stranek_v_url[] = $idStranky;
            
            if($idStranky == 0)
                return;
                else
                $this->SetActiveBranch($this->data[$idStranky]['rodic']);
        }

        return;                 
        
    }                 
    
    
    public function BlockSitemap(){
       
        $this->menu_html = "";
        $this->GetMenu(0,"sitemap");
        return $this->menu_html;
        
    }
    
    //vraci id stranky v aktualni vetvi stromu podle urovne
    public function get_id_page_by_level($uroven = 0)
    {
        $ids = array_reverse($this->id_stranek_v_url);
        return isset($ids[$uroven]) ? $ids[$uroven] : 0;
    }
    
  
}
?>