<?php

class C_SystemMessage{
			
    private $messages = array("error" => array(), "warning" => array(), "ok" => array(), "info" => array());
    
    private $for_web = false;
    
    private $session_name = "cms";
    
    //identifikator formulare, aby nedoslo k tomu, ze se chybova hlaska zobrazi u jineho formulare
    private $form_id = "";
    
    //konstruktor
	public function __construct($session_name = "")
    {
		
        $this->session_name = $session_name == "" ? $this->session_name : $session_name;
		$this->form_id = isset($_SESSION[$this->session_name]['form_id']) ? $_SESSION[$this->session_name]['form_id'] : "";

		if(!isset($_SESSION[$this->session_name]['messages'])) 
        {
			$_SESSION[$this->session_name]['messages'] = $this->messages;
		}
        else
        {
            $this->messages = $_SESSION[$this->session_name]['messages'];
        }
		
	}
    
    public function set_for_web($val = true)
    {
        $this->for_web = $val;
        
    }

    public function set_messages($messages)
    {

        if(isset($messages['error']))
        {
            foreach($messages['error'] AS $msg)
                $this->add_error($msg);
        }

        if(isset($messages['ok']))
        {
            foreach($messages['ok'] AS $msg)
                $this->add_ok($msg);
        }


    }
    
    //nastavi identifikator formulare, aby nedoslo k tomu, ze se chybova hlaska zobrazi u jineho formulare
    //nastavi se pri odeslani formulare, pri vypisu je identifikator porovnavan s pozadovanym identifikatorem.
    public function set_form_id($id)
    {
        $this->form_id = $id;
        $_SESSION[$this->session_name]['form_id'] = $id;
        //print_r($_SESSION[$this->session_name]);
    }
   
    public function get()
    {
        $messages = array();
        $messages["error"] = $this->get_error();
        $messages["warning"] = $this->get_warning();
        $messages["ok"] = $this->get_ok();
        $messages["info"] = $this->get_info();
        
        $this->reset();
        return $messages;
    }
	
	
	
	public function add_error($message)
    {
		if($message=='') 
            return;
            
		$_SESSION[$this->session_name]['messages']['error'][] = $message;
        
        $this->messages["error"][] = $message;

    }
    
    public function add_info($message)
    {
		if($message=='') 
            return;
            
		$_SESSION[$this->session_name]['messages']['info'][] = $message;
        
        $this->messages["info"][] = $message;

    }
		
	public function add_warning($message){
	   
		if($message=='') 
            return;
            
		$_SESSION[$this->session_name]['messages']['warning'][] = $message;
        $this->messages["warning"][] = $message;
	
    }
    
    public function add_ok($message){
	   
		if($message=='') 
            return;
            
		$_SESSION[$this->session_name]['messages']['ok'][] = $message;
        $this->messages["ok"][] = $message;
	
    }
	
    public function error_exists()
    {
        return count($this->messages['error']) > 0;
    }
    
    public function ok_exists()
    {
        return count($this->messages['ok']) > 0;
    }
    
    public function warning_exists()
    {
        return count($this->messages['warning']) > 0;
    }
    
    public function info_exists()
    {
        return count($this->messages['info']) > 0;
    }
    
    	
    private function reset($message_type = "")
    {
        if($message_type == 'error' || $message_type == '')
        {
            
            $this->messages['error'] = array();
            $_SESSION[$this->session_name]['messages']['error'] = array();
        }    
        if($message_type == 'ok' || $message_type == '')
        {
            
            $this->messages['ok'] = array();
            $_SESSION[$this->session_name]['messages']['ok'] = array();
        }    
        if($message_type == 'warning' || $message_type == '')
        {
            
            $this->messages['warning'] = array();
            $_SESSION[$this->session_name]['messages']['warning'] = array();
        }
        if($message_type == 'info' || $message_type == '')
        {
            $this->messages['info'] = array();
            $_SESSION[$this->session_name]['messages']['info'] = array();
        }    
        
        unset($_SESSION[$this->session_name]['messages']);
    }
        
	public function get_error($idx = 0)
    {
	   
		if(!$this->error_exists()) 
            return false;
            
        if($idx < 0)
		  $m = $this->messages['error'];
          else
          $m = count($this->messages['error']) > 0 ? $this->messages['error'][$idx] : "";
          
		$this->reset("error");
		return $m;
	}
    
    public function to_array()
    {
        return array(
            "error" => $this->get_error(),
            "warning" => $this->get_warning(),
            "ok" => $this->get_ok()
            );
    }
    
    public function get_ok($idx = 0)
    {
	   
		if(!$this->ok_exists()) 
            return false;
            
        if($idx < 0)
		  $m =  $this->messages['ok'];
          else
          $m =  count($this->messages['ok']) > 0 ? $this->messages['ok'][$idx] : "";
          
		$this->reset("ok");
		return $m;
	}
    
    public function get_warning($idx = 0)
    {

		if(!$this->warning_exists()) 
            return false;

        if($idx < 0)
		  $m =  $this->messages['warning'];
          else
          $m =  count($this->messages['warning']) > 0 ? $this->messages['warning'][$idx] : "";
          
        
          
		$this->reset("warning");
		return $m;
	}
    
    public function get_info($idx = 0)
    {
	   
		if(!$this->info_exists()) 
            return false;
            
        if($idx < 0)
		  $m =  $this->messages['info'];
          else
          $m =  count($this->messages['info']) > 0 ? $this->messages['info'][$idx] : "";
          
		$this->reset("info");
		return $m;
	}
	
		
	public function get_message()
    {
        if($this->error_exists())
            return $this->get_error();
        elseif($this->warning_exists())
            return $this->get_warning();
        elseif($this->ok_exists())
            return $this->get_ok();
        elseif($this->info_exists())
            return $this->get_info();   
             
        return false;
	}
    
    public function get_first_message($typ = "error")
    {
        $this->reset();
        return isset($this->messages[$typ][0]) ? $this->messages[$typ][0] : false;
    }
    
    
    public function show_error_messages($return_string = false)
    {
        $html = "";
        $type = "error";
        $html .= $this->show($type, $return_string);
        
        if($return_string)
            return $html;
            else
            echo $html;
    }
    
    public function show_ok_messages($return_string = false)
    {
        $html = "";
        $type = "ok";
        $html .= $this->show($type,$return_string);

        if($return_string)
            return $html;
            else
            echo $html;
    }
    
    public function show_warning_messages($return_string = false)
    {
        $html = "";
        $type = "warning";
        $html .= $this->show($type,$return_string);
        
        if($return_string)
            return $html;
            else
            echo $html;
    }
    
    public function show_messages($return_string = false, $form_id = "")
    {
        
        
        
        $html = "";
        if($form_id == "" || $form_id == $this->form_id)
        {
            $html .= $this->show_error_messages($return_string);
            $html .= $this->show_warning_messages($return_string);
            $html .= $this->show_ok_messages($return_string);
        }
        
        unset($_SESSION[$this->session_name]['form_id']);
        
        if($return_string)
            return $html;
            else
            echo $html;
    }
    
    private function show($type, $return_string = false)
    {

        if(count($this->messages[$type]) == 0)
            return false;

        $html = $this->for_web ? $this->show_for_web($type) : $this->show_for_admin($type);
        
        $this->reset($type);
        
        if($return_string)
            return $html;
            else
            echo $html;
    }
    
    private function show_for_admin($type)
    {
        $html = "";
        $html .= "<div class='".$type." w45'><ul>";
        
        foreach($this->messages[$type] AS $m)
		  $html .= "<li>".$m."</li>";
          
		$html .= "</ul></div>";
        
        return $html;
    }
    
    private function show_for_web($type)
    {
        
        $html = "";
        $html .= "<p class='".$type."'>";
        
        $html .= implode('<br />', $this->messages[$type]);
          
		$html .= "</p>";
        
        return $html;
    }
	

}
?>