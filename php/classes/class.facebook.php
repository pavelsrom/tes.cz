<?php

/**
 * @author Pavel Srom
 * @copyright 2010
 */



class C_Facebook{
    
    
    public $setting;
    
    public $exists;
    
    public function __construct($idPlugin){
        global $db;
        global $domain;
        
        $this->setting = array();
        $this->exists = false;
        
        $data = $db->Query("SELECT * FROM ".TABLE_FACEBOOK." AS f 
            LEFT JOIN ".TABLE_FACEBOOK_PLUGINY." AS p ON f.idDomeny=p.idDomeny
            WHERE p.idPluginu=".$idPlugin."
                AND f.idDomeny=".$domain->getId()."
                AND f.zobrazit=1
                AND p.zobrazit=1 
            LIMIT 1");

        if($db->numRows($data)==0) return;
        
        $this->setting = $db->getAssoc($data);
        
        $this->exists = true;
                    
        
    }
    
    public function getFacebook(){
        
        if(!$this->exists) return "";
        $typ = strtolower($this->setting['typ']);
        $html = "";
        if($typ=='likebutton') $html =  $this->getLikeButton();
        elseif($typ=='likebox') $html =  $this->getLikeBox();
        elseif($typ=='recommendations') $html =  $this->getRecommendations();
        elseif($typ=='comments') $html =  $this->getComments();
        elseif($typ=='livestream') $html =  $this->getLiveStream();
        elseif($typ=='activityfeed') $html =  $this->getActivityFeed();
        elseif($typ=='facepile') $html =  $this->getFacepile();
        elseif($typ=='socialpanel') $html =  $this->getSocialPanel();
        
        return "<div class='facebook ".$typ."'>".$html."</div>";
    }
    
    
    private function getLikeBox(){
        $fb_page = $this->setting['urlFbStranky'];
        if($fb_page == '') return "";
        
        $pocetPolozek = intval($this->setting['pocetPolozek']);
        $stream = $this->setting['zobrazitStream']==1?'true':'false';
        
        $sirka = $this->setting['sirka'];
        if($sirka>0) $sirka = 'width="'.$sirka.'"'; else $sirka = "";
        
        $vyska = $this->setting['vyska'];
        if($vyska>0) $vyska = 'height="'.$vyska.'"'; else $vyska = "";
        
        return '<iframe src="http://www.facebook.com/plugins/likebox.php?href='.$fb_page.'&amp;width='.$sirka.'&amp;height='.$vyska.'&amp;colorscheme=light&amp;show_faces=true&amp;border_color=%23E0E5D4&amp;stream='.$stream.'&amp;header=false" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:301px; height:62px;" allowTransparency="true"></iframe>';
        
        //return '<fb:like-box colorscheme="light" header="false" href="'.$fb_page.'" '.$sirka.' '.$vyska.' connections="'.$pocetPolozek.'"></fb:like-box>';
        
    }
    
    
    private function getFacepile(){
        
        $sirka = $this->setting['sirka'];
        if($sirka>0) $sirka = 'width="'.$sirka.'"'; else $sirka = "";
       
        $vyska = $this->setting['vyska'];
        if($vyska>0) $vyska = 'height="'.$vyska.'"'; else $vyska = "";
       
        $pocetPolozek = intval($this->setting['pocetPolozek']);
        
        return '<fb:facepile max-rows="'.$pocetPolozek.'" '.$sirka.' '.$sirka.'></fb:facepile>';
        
        
    }
    
    private function getRecommendations(){
        global $domain;
        
        $site = $domain->data['domena'];
        if($domain->data['adresar']!='')
            $site = $site."/".$domain->data['adresar'];
        
        $vyska = $this->setting['vyska'];
       if($vyska>0) $vyska = 'height="'.$vyska.'"'; else $vyska = "";
       $sirka = $this->setting['sirka'];
       if($sirka>0) $sirka = 'width="'.$sirka.'"'; else $sirka = "";
        
        $color = $this->setting['barva'];
       $font = $this->setting['font'];
       
        return '<fb:recommendations site="'.$site.'" '.$vyska.' '.$sirka.' colorscheme="'.$color.'" font="'.$font.'"></fb:recommendations>';
    }
    
    private function getLiveStream(){
        
        $vyska = $this->setting['vyska'];
       if($vyska>0) $vyska = 'height="'.$vyska.'"'; else $vyska = "";
       $sirka = $this->setting['sirka'];
       if($sirka>0) $sirka = 'width="'.$sirka.'"'; else $sirka = ""; 
        
        return '<fb:live-stream event_app_id="'.FACEBOOK_APP_ID.'" '.$sirka.' '.$vyska.' via_url="" always_post_to_friends="true"></fb:live-stream>';
    }
    
    
    private function getComments(){
        global $page;
        global $domain;
        global $links;
        
        $url = $domain->getUrl().$links->get_url($page->get_id());
        $url = urlencode($url);
        
        $sirka = $this->setting['sirka'];
        if($sirka>0) $sirka = 'width="'.$sirka.'"'; else $sirka = ""; 
        
        $vyska = $this->setting['vyska'];
        if($vyska>0) $vyska = 'height="'.$vyska.'"'; else $vyska = "";
        
        $pocetPolozek = intval($this->setting['pocetPolozek']);
        
        return '<fb:comments xid="'.$url.'" numposts="'.$pocetPolozek.'" '.$sirka.' '.$vyska.'></fb:comments>';
    }
    
    private function getActivityFeed(){
        global $domain;
        
        $site = $domain->data['domena']; 
        $font = $this->setting['font'];
        $vyska = $this->setting['vyska'];
       if($vyska>0) $vyska = 'height="'.$vyska.'"'; else $vyska = "";
       $sirka = $this->setting['sirka'];
       if($sirka>0) $sirka = 'width="'.$sirka.'"'; else $sirka = ""; 
       
        return "<fb:activity site='".$site."' font='".$font."' recommendations='true' ".$vyska." ".$sirka."></fb:activity>";
    }
    
    private function getLikeButton(){
	   
       $layout = $this->setting['struktura'];
       $color = $this->setting['barva'];
       $font = $this->setting['font'];
       $fotky = $this->setting['zobrazitFotky']==1?'true':'false';
       $vyska = $this->setting['vyska'];
       if($vyska>0) $vyska = 'height="'.$vyska.'"'; else $vyska = "";
       $sirka = $this->setting['sirka'];
       if($sirka>0) $sirka = 'width="'.$sirka.'"'; else $sirka = ""; 
        
       return '<br /><fb:like layout="'.$layout.'" show_faces="'.$fotky.'" action="like" colorscheme="'.$color.'" font="'.$font.'" '.$vyska.' '.$sirka.'></fb:like>';
       		
	}
    
    private function getSocialPanel(){
        
        
        $html = "";
  
        $li = "";
        
        if($this->setting['zobrazitRssSdileni']==1)
            $li .= "<li><img src='img/social/icon-rss.gif' alt='rss'/><a class='rss' href='".$this->GetShareLink('rss')."' title='Rss' target='_blank'>Rss</a></li>";
            
         
        
        if($this->setting['zobrazitFbSdileni']==1)
            $li .= "<li><img src='img/social/icon-facebook.gif' alt='facebook'/><a class='facebook' href='".$this->GetShareLink('facebook')."' target='_blank' title='Facebook'>Facebook</a></li>";
        
        if($this->setting['zobrazitTwSdileni']==1)
            $li .= "<li><img src='img/social/icon-twitter.gif' alt='twitter'/><a class='twitter' href='".$this->GetShareLink('twitter')."' target='_blank' title='Twitter'>Twitter</a></li>";
        
        if($this->setting['zobrazitGoogleBookmarksSdileni']==1)
            $li .= "<li><img src='img/social/icon-google.gif' alt='google bookmarks'/><a class='google_bookmarks' href='".$this->GetShareLink('google_bookmarks')."' target='_blank' title='Google záložky'>".TGOOGLE_ZALOZKY."</a></li>";
            
        if($this->setting['zobrazitGoogleReaderSdileni']==1)
            $li .= "<li><img src='img/social/icon-buzz.gif' alt='google reader'/><a class='google_reader' href='".$this->GetShareLink('google_reader')."' target='_blank' title='Google Reader'>Google Reader</a></li>";
        
        if($this->setting['zobrazitLinkujSdileni']==1)
            $li .= "<li><img src='img/social/icon-linkuj.gif' alt='linkuj'/><a class='linkuj' href='".$this->GetShareLink('linkuj')."' target='_blank' title='Linkuj'>Linkuj</a></li>";
        
        if($this->setting['zobrazitDeliciousSdileni']==1)
            $li .= "<li><img src='img/social/icon-delicious.gif' alt='delicious'/><a class='delicious' href='".$this->GetShareLink('delicious')."' title='Delicious' target='_blank'>Delicious</a></li>";
            
        if($this->setting['zobrazitPrintSdileni']==1)
            $li .= "<li><img src='img/social/icon-print.gif' alt='tisk'/><a class='print' href='#' onclick='".$this->GetShareLink('print')."' title='Tisk'>".TTISK."</a></li>";
        
        if($this->setting['zobrazitEmailSdileni']==1){
            
            $li .= "<li><img src='img/social/icon-email.gif' alt='email'/><a class='email' href='".$this->GetShareLink('email')."'>".TEMAIL."</a></li>";
            
            }
        
        
        
        $html = "<ul class='social-share'>";
        
        $html .= $li;
        
        $html .= "</ul>";
        
        return $html;
    }
    
    
    public function GetShareLink($typ){
        global $links;
        global $page;
        global $domain;
        
        $url = $domain->getUrl().$links->get_url($page->get_id());
        $domena = $domain->getUrl(); 
        $domena_nazev = NAZEV_WEBU;
        $nazev = $page->get('nadpis');
        
        if($typ=='facebook')
            return "http://www.facebook.com/share.php?u=".$url;
        elseif($typ=='twitter')
            return "http://twitter.com/home?status=".$url;
        elseif($typ=='rss')
            return $domena."rss.xml";
        elseif($typ=='google_bookmarks')
            return "http://www.google.com/bookmarks/mark?op=edit&amp;output=popup&amp;bkmk=".$url."&amp;title=".$nazev;
        elseif($typ=='google_reader')
            return "http://www.google.com/reader/link?url=".$url."&amp;title=".$nazev."&amp;snippet=&amp;srcURL=".$domena."&amp;srcTitle=".$domena_nazev;
        elseif($typ=='linkuj')
            return "http://linkuj.cz/?id=linkuj&amp;url=".$url."&amp;title=".$nazev;
        elseif($typ=='delicious')
            return "http://del.icio.us/post?url=".$url."&amp;title=".$nazev;
        elseif($typ=='print')
            return "window.print();return false;";
        elseif($typ=='email'){
            $body = $this->setting['obsahEmailu'];
            $subject = $this->setting['predmetEmailu'];
            
            $body = str_replace(PHP_EOL, '%0A',$body);
            return "mailto:?body=".$body."&subject=".$subject;
        }
               
        
        
        
    } 
    
   
  
    
    
}

?>