<?php

/**
 * @author Pavel Srom
 * @copyright 2010
 */


DEFINE('SESSION_FORM_OK_MESSAGE','form-ok-msg');
DEFINE('SESSION_FORM_ERROR_MESSAGE','form-error-msg');


class C_Form{
	
	//indikuje zda formular existuje
	public $exists;
	
	//obsahuje data formulare
	public $form;
	
	//obsahuje data polozek formulare
	private $items;
	
	//prefix nazvu inputu
	private $itemNamePrefix;
	
	//nazev submit tlacitka 
	private $btnName;
	
	//oznaceni pro povinnou polozku
	private $req;
	
	//zprava ktera se bude posilat emailem
	private $message;
	
	//indikuje zda bylo stisknuto tlacitko na tomto formulari
	private $active;
	
	
	public function __construct($idForm){
		global $db;
		global $domain;
		
		$this->form = array();
		$this->items = array();
	
		$this->itemNamePrefix = "item";
		$this->btnName = "btnSendForm";
		$this->req = "<span class='req'>*</span>";
		$this->message = "";
		
		$idForm = intval($idForm);
		
		$this->form['id'] = $idForm; 
		
		$this->itemNamePrefix .= $idForm;
		
		$data_form = $db->Query("SELECT f.ukladatZpravy AS ukladat, f.potvrzeniPredmet AS predmet, f.nazev AS nazev_formulare, f.email, f.tlacitko, f.idSkupinEmailu AS skupiny, f.antispam, f.html,f.textPoOdeslani,f.posilatPotvrzeni,
				IF(f.posilatPotvrzeni=1 AND f.potvrzeni!='',f.potvrzeni,'') AS potvrzeni, f.ukladatZpravy AS ukladat, 
				fp.idPolozky, fp.nazev AS nazev_polozky, fp.typ, fp.format, fp.hodnoty, fp.povinna  
            FROM ".TABLE_FORMULARE." AS f 
			LEFT JOIN ".TABLE_FORMULARE_POLOZKY." AS fp ON f.idFormulare=fp.idFormulare
			WHERE f.idDomeny=".$domain->getId()." 
				AND f.idFormulare=".$idForm."
				AND f.zobrazit=1
				AND fp.idPolozky IS NOT NULL
			ORDER BY fp.priorita 
			");
		
		
		if($db->numRows($data_form)==0) return;

		$this->exists = true;

		while($f = $db->getAssoc($data_form)){
			$this->form['nazev'] = $f['nazev_formulare'];
			$this->form['email'] = $f['email'];
			$this->form['html'] = trim($f['html']);
			$this->form['tlacitko'] = $f['tlacitko'];
			$this->form['potvrzeni'] = $f['potvrzeni'];
			$this->form['ukladat'] = $f['ukladat'];
			$this->form['predmet'] = $f['predmet'];
			$this->form['antispam'] = $f['antispam'];
            $this->form['textPoOdeslani'] = $f['textPoOdeslani'];
            $this->form['posilatPotvrzeni'] = $f['posilatPotvrzeni'];
			$this->form['skupiny'] = explode('#',$f['skupiny']);
					
            $hodnoty = array();
            $nazev_pol = $f['nazev_polozky'];
                    
            if($f['typ']=='label' || $f['typ']=='description'){
                $nazev_pol = $f['hodnoty'];
                }
                    
            if($f['typ']=='selectbox' || $f['typ']=='checkbox' || $f['typ']=='radio'){
                $dp = explode('#', $f['hodnoty']);
                if(count($db)>0){
                    foreach($dp AS $dpp){
                        $dpp = trim($dpp);
                        if($dpp=='') continue;
                        $dpp_data = explode(':',$dpp);
                        
                        //print_r($dpp_data);
                        
                        if(count($dpp_data)==1)
                            $hodnoty[] = array('val'=>$dpp_data[0], 'req'=> false);
                        elseif(count($dpp_data)>1){
                            $req = intval(end($dpp_data));
                            if($req==1) $req=true; else $req = false; 
                            //print_r($dpp_data);
                            
                            array_pop($dpp_data);
                            $hodnoty[] = array('val'=>implode(':',$dpp_data), 'req'=>$req);
                            }
                        else continue;
                        }
                            
                            
                    }
                    
                //print_r($hodnoty);
                //echo "<br><br>";
                }
                
                //print_r($this->items);    
                    
		      $this->items[$f['idPolozky']] = array(
			     'nazev'		=>	$nazev_pol, 
			     'typ'		=>	$f['typ'],
			     'format'	=>	$f['format'],
			     'hodnoty'	=>	$hodnoty,
			     'povinna'	=>	$f['povinna']
			     );
			}
			
		if(!isset($_SESSION[SESSION_FORM_ERROR_MESSAGE.$idForm]))
			$_SESSION[SESSION_FORM_ERROR_MESSAGE.$idForm] = "";
				
		if(!isset($_SESSION[SESSION_FORM_OK_MESSAGE.$idForm]))	
			$_SESSION[SESSION_FORM_OK_MESSAGE.$idForm] = "";
			
		mysql_free_result($data_form);
        
       
		if(isset($_POST[$this->btnName]) && isset($_POST['idFormulare']) && intval($_POST['idFormulare'])==$this->form['id']){
			if(isset($_POST[$this->itemNamePrefix])) 
				$_SESSION[$this->itemNamePrefix] = $_POST[$this->itemNamePrefix];
			$this->SendForm();
			$this->active = true;
			}
			else{
			$this->active = false;
			}
			
		
		
		
	}	

	//vrati formular umisteny v poli, vhodne pro to, aby se vysledek dal formatovat dle libosti
	public function GetFormToTable(){
		global $domain;

				
		$arr = array();
		
		$html = "<span id='f".$this->form['id']."'></span>";
		
		if($this->form['html']!=''){ 
			$html .= "<div class='form-message'>".$this->GetMessage()."</div>";
			
			$pattern = "|post\x5b[0-9]+\x5d|";
			$this->form['html'] = preg_replace_callback($pattern, array('self', 'GetPostItem'), $this->form['html']);
			
			$html .= $this->form['html'];
			return $html;
			}
		
		$html .= "<div class='block-form'>";
		
		$html .= "<h3>".$this->form['nazev']."</h3>";
		$html .= "<form method='post' action=''>\n";
		
		$html .= "<table>";
		$html .= "<tr>";
		
		$html .= "<td colspan='2' class='form-message'>".$this->GetMessage()."</td>";
		
		$html .= "</td></tr>";
        
        //print_r($this->items);
        
		foreach($this->items AS $id => $pol){
			if($pol['typ']=='text') $html .= $this->GetText($id, true);
			if($pol['typ']=='textarea') $html .= $this->GetTextarea($id, true);
			if($pol['typ']=='selectbox') $html .= $this->GetSelectbox($id, true);
			if($pol['typ']=='checkbox') $html .= $this->GetCheckbox($id, true);
			if($pol['typ']=='radio') $html .= $this->GetRadio($id, true);
			if($pol['typ']=='label') $html .= $this->GetLabel($id, true);
            if($pol['typ']=='description') $html .= $this->GetDesc($id, true);
			}
            
        if($this->form['antispam']==1)
			$html .= $this->GetAntispam();
            
		$html .= "<tr><td colspan='2' class='td-button'>";
		
		$html .= "<input type='hidden' value='".$this->form['id']."' name='idFormulare' />";
		$html .= "<input type='submit' value='".$this->form['tlacitko']."' name='".$this->btnName."' class='button' />";
		
		$html .= "</td></tr>";
		
			
		$html .= "</table>";
        
		if($this->isReq())
            $html .= "<div>".$this->req."<span class='req-alert'>".TPOVINNE_UDAJE."</span></div>";
            
		$html .= "</form>";
		
		$html .= "</div>";
		return $html;
	}


	//vrati html kod formulare v tabulce 
	public function GetForm(){
		global $admin;
			
		$res = "<span id='f".$this->form['id']."'></span>";
			
		
		if($this->form['html']!=''){ 
			$res .= "<div class='form-message'>".$this->GetMessage()."</div>";
			
			$pattern = "|post\x5b[0-9]+\x5d|";
			$this->form['html'] = preg_replace_callback($pattern, array('self', 'GetPostItem'), $this->form['html']);
			
			$res .= $this->form['html'];
			return $res;
			}
			
		$res .= "<div class='form-message'>".$this->GetMessage()."</div>";
		
        $res .= "<form method='post' action=''>\n";
		
		
		$res .= "<div>";
		foreach($this->items AS $id => $pol){
			if($pol['typ']=='text') 
				$res .= $this->GetText($id);
				
			if($pol['typ']=='textarea') 
				$res .= $this->GetTextarea($id);
				
			if($pol['typ']=='selectbox') 
				$res .= $this->GetSelectbox($id);
			
			if($pol['typ']=='checkbox') 
				$res .= $this->GetCheckbox($id);
				
			if($pol['typ']=='radio') 
				$res .= $this->GetRadio($id);
				
			if($pol['typ']=='label') 
				$res .= $this->GetLabel($id);
			}
		if($this->form['antispam']==1)
			$res .= $this->GetAntispam();
		
		$res .= "</div>";
        
        if($this->isReq())
            $res .= "<div>".$this->req."<span class='req-alert'>".TPOVINNE_UDAJE."</span></div>";
            	
		$res .= "<div class='center'><input type='hidden' value='".$this->form['id']."' name='idFormulare' />";
		$res .= "<input type='submit' value='".$this->form['tlacitko']."' name='".$this->btnName."' class='button' /></div>";
		$res .= "</form>\n";
		
        
            
		return $res;
		
	}
	
    private function isReq(){
        
        if(count($this->items)==0) return false;
        
        foreach($this->items AS $i)
            if($i['povinna']==1) return true;
            
        return false;
        
    }
    

	private function GetAntispam(){


		$html = "";
		
		$dny = array(10 => '-- '.TVYBERTE_DNESNI_DEN.' --', 1 => TPONDELI, 2=> TUTERY, 3=> TSTREDA, 4=> TCTVRTEK, 5=>TPATEK, 6=>TSOBOTA, 0=>TNEDELE);

		$html .= "<tr><th><label for='fi".$this->form['id']."_antispam'>".TANTISPAM."</label></th><td>";
		
		$html .= "<select id='fi".$this->form['id']."_antispam' name='antispam' size='1' class='selectbox' >\n";
		
		$sel = true;
		
		foreach($dny as $i => $v){
			if($i == 10 && $sel){ 
				$html .=  "<option value='".$i."' selected='selected'>".$v."</option>\n";
				$sel = false;
				} 
				else 
				$html .=  "<option value='".$i."'>".$v."</option>\n";		
			}
		$html .=  "</select>";
	
		$html .= "</td></tr>";
			
		return $html;
		
	}
	
	
	private function GetRadio($id, $toTable=false){
		$val = $this->items[$id]['hodnoty'];
		
		$req = ($this->items[$id]['povinna']==1?$this->req:'');
		
		$res = "";
		
		if($toTable) $res .= "<tr>";
		
		if($toTable)
			$res .= "<th><label for='fi".$this->form['id']."".$id."'>".$this->items[$id]['nazev'].$req."</label></th>";
			else
			$res .= "<label for='fi".$this->form['id']."".$id."'>".$this->items[$id]['nazev'].$req."</label>";
		
		
		$i=0;
		
		
		if($toTable) $res .= "<td>";
		
		foreach($val AS $v){
            $selected = "";
            if($v['req']) $selected = "checked='checked'";
			$res .= "<span class='radio-item'><input type='radio' value='".$v['val']."' name='".$this->itemNamePrefix."[".$id."]' class='radio' ".$selected."/>".$v['val']."</span>";
			}
		
		if($toTable) $res .= "</td>";
		
		if($toTable) $res .= "</tr>";	
		return $res;
		}
	
	private function GetText($id, $toTable=false){
		$req = ($this->items[$id]['povinna']==1?$this->req:'');
		
		$text = $this->GetPostItem($id);

		$res = "";
		
		if($toTable) $res .= "<tr>";
		
        $maxlength = "";
        if($this->items[$id]['format']=='email') $maxlength = 70;
        if($this->items[$id]['format']=='integer' || $this->items[$id]['format']=='integer') $maxlength = 10;
        if($this->items[$id]['format']=='datum' || $this->items[$id]['format']=='cas') $maxlength = 10;
        if($this->items[$id]['format']=='icq') $maxlength = 11;
        if($this->items[$id]['format']=='datumcas') $maxlength = 20;
        if($this->items[$id]['format']=='ico') $maxlength = 8;
        if($this->items[$id]['format']=='dic') $maxlength = 12;
        
        if($maxlength!="") $maxlength = "maxlength='".$maxlength."'";

    
		if($toTable)
			$res .= "<th><label for='fi".$this->form['id']."".$id."'>".$this->items[$id]['nazev']."</label></th>";
			else
			$res .= "<label for='fi".$this->form['id']."".$id."'>".$this->items[$id]['nazev']."</label>";
		
	
		if($toTable) 
			$res .= "<td><input id='fi".$this->form['id']."".$id."' type='text' value='".$text."' name='".$this->itemNamePrefix."[".$id."]' class='txt' ".$maxlength."/>".$req."</td>";
			else
			$res .= "<input id='fi".$this->form['id']."".$id."' type='text' value='".$text."' name='".$this->itemNamePrefix."[".$id."]' class='txt' ".$maxlength."/>".$req;
		
		if($toTable) $res .= "</tr>";
		
		return $res;
	}
	
	private function GetTextarea($id, $toTable=false){
		$req = ($this->items[$id]['povinna']==1?$this->req:'');
		$text = $this->GetPostItem($id);
		
		$res = "";
		
		if($toTable) $res .= "<tr>";
		
		if($toTable)
			$res .= "<th><label for='fi".$this->form['id']."".$id."'>".$this->items[$id]['nazev'].$req."</label></th>";
			else
			$res .= "<label for='fi".$this->form['id']."".$id."'>".$this->items[$id]['nazev'].$req."</label>";
		
		if($toTable) $res .= "<td>";
	
		$res .= "<textarea id='fi".$this->form['id']."".$id."' name='".$this->itemNamePrefix."[".$id."]' class='textarea' rows='' cols=''>".$text."</textarea>";
		
		if($toTable) $res .= "</td></tr>";
		return $res;
	}
	
	private function GetSelectbox($id, $toTable=false){
		$val = $this->items[$id]['hodnoty'];
		$req = ($this->items[$id]['povinna']==1?$this->req:'');
        
		$res = "";
		
		if($toTable) $res .= "<tr>";
		
		if($toTable)
			$res .= "<th><label for='fi".$this->form['id']."".$id."'>".$this->items[$id]['nazev'].$req."</label></th>";
			else
			$res .= "<label for='fi".$this->form['id']."".$id."'>".$this->items[$id]['nazev'].$req."</label>";
		
		if($toTable) $res .= "<td>";
		
		$res .= "<select id='fi".$this->form['id']."".$id."' name='".$this->itemNamePrefix."[".$id."]' class='selectbox' size='1' >";
		
		foreach($val AS $v){
            $selected = "";
            if($v['req']) $selected = "selected='selected'";
			$res .= "<option value='".$v['val']."' ".$selected.">".$v['val']."</option>";
            }
			
		$res .= "</select>";
		
		if($toTable) $res .= "</td></tr>";
		
		return $res;
	}
	
	private function GetCheckbox($id, $toTable=false){
		$val = $this->items[$id]['hodnoty'];
		$req = ($this->items[$id]['povinna']==1?$this->req:'');
		
		$res = "";
		
		if($toTable) $res .= "<tr>";
		
		if($toTable)
			$res .= "<th><label for='fi".$this->form['id']."".$id."'>".$this->items[$id]['nazev'].$req."</label></th>";
			else
			$res .= "<label for='fi".$this->form['id']."".$id."'>".$this->items[$id]['nazev'].$req."</label>";
		
		if($toTable) $res .= "<td>";
		
		foreach($val AS $v){
            $selected = "";
            if($v['req']) $selected = "checked='checked'";
			$res .= "<span class='check-item'><input type='checkbox' name='".$this->itemNamePrefix."[".$id."][]' value='".$v['val']."' class='checkbox' ".$selected.">".$v['val']."</span>";
            }

		if($toTable) $res .= "</td></tr>";

		return $res;
	}
	
	private function GetLabel($id, $toTable=false){
		
		if($toTable)
			return "<tr><td colspan='2' class='head-label'>".$this->items[$id]['nazev']."</td></tr>";
			else
			return "<span class='label'>".$this->items[$id]['nazev']."</span>";
		}
	
	
    private function GetDesc($id, $toTable=false){
		
		if($toTable)
			return "<tr><td colspan='2' class='head-desc'>".$this->items[$id]['nazev']."</td></tr>";
			else
			return "<span class='desc'>".$this->items[$id]['nazev']."</span>";
		}
    
	//zpracuje zpravu z formulare
	private function SendForm(){
		global $db;
		global $domain;
		
		
		$rekapitulace = "<br />";
		$rekapitulace .= TZADANE_UDAJE."<br />";
		
		$pocet_textovych_inputu = 0;
		$pocet_prazdnych_textovych_inputu = 0;
		
		foreach($_POST[$this->itemNamePrefix] AS $i => $v){
			if(!is_array($v)){
				$value = secureString($v);
				if($this->items[$i]['typ']=='text' || $this->items[$i]['typ']=='textarea') {
					$pocet_textovych_inputu++;
					if(trim($v)=='') $pocet_prazdnych_textovych_inputu++;
					}
				}
				else
				{
				
					$rekapitulace .= $this->items[$i]['nazev'].": <b>";
					$i=0;
					foreach($v AS $pol){
						if($i>0) 
							$rekapitulace .= ", ".$pol; 
							else 
							$rekapitulace .= $pol;
						$i++;
						}
					$rekapitulace .= "</b><br />";
					continue;
				}
			//print_r($v);
			//echo "<br><br>";
			if($this->ValidateItem($i, $value)){
				if(trim($value)=='') $value = "---";
				$rekapitulace .= $this->items[$i]['nazev'].": <b>".$value."</b><br />";
				}
			}

		if($this->form['antispam']==1 && intval($_POST['antispam'])<>date('w') )
			$this->AddError(TANTISPAM_VYBERTE_DNESNI_DEN);
			
		if($pocet_prazdnych_textovych_inputu == $pocet_textovych_inputu) 
			$this->AddError(TNELZE_ODESLAT_PRAZDNY_FORMULAR);

		//print_r($_SESSION[SESSION_FORM_ERROR_MESSAGE.$this->form['id']]);
			
		if($_SESSION[SESSION_FORM_ERROR_MESSAGE.$this->form['id']]!=''){
			Redirect($_SERVER['HTTP_REFERER']."#f".$this->form['id']);

			}

		
		//potvrzeni pro odesilatele
		$predmet = TZPRAVA_Z_FORMULARE.": ".$this->form['nazev'];
		$zprava = $rekapitulace;
		$formular_vyplnil = $this->GetEmailSender();
		
		if($formular_vyplnil!='' && $this->form['email']!=''){
			$result = SendMail($formular_vyplnil,'',$this->form['email'],$predmet,$zprava, $zprava);
				
			}

		//if($result) $this->AddOk("odeslano"); else $this->AddError("nelze odeslat");
		
		
		if($this->form['ukladat']==1)	
			$result = $db->Query("INSERT INTO ".TABLE_FORMULARE_ZPRAVY." (idFormulare, email, zprava, stav, datum) VALUES (".$this->form['id'].",'".$db->secureString($formular_vyplnil)."', '".$db->secureString($zprava)."', 'nova', NOW())");
		
		//print_r($this->form['skupiny']);
		
		if(count($this->form['skupiny'])>0 && $formular_vyplnil!=''){
			foreach($this->form['skupiny'] AS $idSkupiny){
				if(intval($idSkupiny)==0) continue;
				
				//overi jestli je zadana emailova adresa v databazi
				$data1 = $db->Query("SELECT idPrijemce FROM ".TABLE_EMAILY_PRIJEMCI." WHERE idDomeny=".$domain->getId()." AND email='".$formular_vyplnil."'");
				
				
				if($db->numRows($data1)==0){
					//pokud neni v databazi tak provede novy zaznam
					$db->Query("INSERT INTO ".TABLE_EMAILY_PRIJEMCI."(idDomeny, email, hash, registrace, poznamka) VALUES(".$domain->getId().",'".$formular_vyplnil."', MD5('".time().$formular_vyplnil."'),NOW(),'Kontakt automaticky převeden z formuláře ".$this->form['nazev']."')");
					
					$idPrijemce = mysql_insert_id();
					
					//provede novy zaznam i do skupiny prijemcu
					$db->Query("INSERT INTO ".TABLE_EMAILY_SKUPINY_PRIJEMCI." (idPrijemce, idSkupiny) VALUES (".$idPrijemce.", ".$idSkupiny.")");
					
					}
					else
					{
						
					$data_pr = $db->getAssoc($data1);
					$idPrijemce = $data_pr['idPrijemce'];
					
					//pokud jiz je v email v tabulce emaily prijemci, overi zda je zaznam v relaci SP
					$verify = $db->Query("SELECT idRelace FROM ".TABLE_EMAILY_SKUPINY_PRIJEMCI." WHERE idSkupiny=".$idSkupiny." AND idPrijemce=".$idPrijemce);
					if($db->numRows($verify)==0)
						//pokud neni zaznam v relaci SP, vytvori novy zaznam
						$db->Query("INSERT INTO ".TABLE_EMAILY_SKUPINY_PRIJEMCI." (idPrijemce, idSkupiny) VALUES (".$idPrijemce.", ".$idSkupiny.")");
					
					}
					
				
				
			}
			
			
		}

		$predmet = $this->form['predmet'];
		$zprava = $this->form['potvrzeni']."<br /><br />".$rekapitulace;
		$from = $this->form['email'];
			
		if($from!='' && $this->form['email']!='' && $this->form['textPoOdeslani']!='' && $this->form['potvrzeni']!='' && $this->form['posilatPotvrzeni']==1 && $formular_vyplnil!=''){
			$result = SendMail($from,'',$formular_vyplnil,$predmet,$zprava,$zprava);
    		}
            
        $this->AddOk(strip_tags($this->form['textPoOdeslani']));
    	
		if(isset($_SESSION[$this->itemNamePrefix]))
			unset($_SESSION[$this->itemNamePrefix]);
				
		Redirect($_SERVER['HTTP_REFERER']."#f".$this->form['id']);

		
		}
	
	//overi formaty hodnot a povinne polozky formulare
	private function ValidateItem($id, $value){
		
		
		if(is_array($value)) return true;
		
		if($value=='' && $this->items[$id]['povinna']==0) return true;
		if($value=='' && $this->items[$id]['povinna']==1) {
			$this->addError(sprintf(TPOLOZKA_X_JE_POVINNA,$this->items[$id]['nazev']));
			return false;
			}
		
		$format = $this->items[$id]['format'];	
		if($format == 'text') return true;
		if($format == 'email') 
			if(TestEmail($value,50,false)) return true;
				else{
					$this->AddError(sprintf(TPOLOZKA_EMAIL_CHYBA, $this->items[$id]['nazev']));
					return false;
				}
		
		
		if($format == 'integer')
			if(TestInt($value, 10, false))
				return true;
				else{
					$this->AddError(sprintf(TPOLOZKA_CELE_CISLO_CHYBA, $this->items[$id]['nazev']));
					return false;
				}
			
		if($format == 'float')
			if(TestFloat($value, 10, false))
				return true;
				else{
					$this->AddError(sprintf(TPOLOZKA_DESETINNE_CISLO_CHYBA, $this->items[$id]['nazev']));
					return false;
				}	
		
		if($format == 'ico')
			if(TestIco($value, false))
				return true;
				else{
					$this->AddError(sprintf(TPOLOZKA_ICO_CHYBA, $this->items[$id]['nazev']));
					return false;
				}
			
		if($format == 'dic')
			if(TestDic($value, 15, false))
				return true;
				else{
					$this->AddError(sprintf(TPOLOZKA_DIC_CHYBA, $this->items[$id]['nazev']));
					return false;
				}
			
		if($format == 'telefon'){
			if(TestPhone($value, false)){
				
				return true;
				}
				else{
					$this->AddError(sprintf(TPOLOZKA_TEL_CHYBA, $this->items[$id]['nazev']));
					return false;
				}
			}
		if($format == 'url'){
			if(TestHttpUrl($value)){
				
				return true;
				}
				else{
					$this->AddError(sprintf(TPOLOZKA_URL_CHYBA, $this->items[$id]['nazev']));
					return false;
				}
			}	
        if($format == 'icq'){
			if(TestICQFormat($value, 11, false)){
				
				return true;
				}
				else{
					$this->AddError(sprintf(TPOLOZKA_ICQ_CHYBA, $this->items[$id]['nazev']));
					return false;
				}
			}
		if($format == 'datum')
			if(TestDate($value, false))
				return true;
				else{
					$this->AddError(sprintf(TPOLOZKA_DATUM_CHYBA, $this->items[$id]['nazev']));
					return false;
				}
		
		if($format == 'datumcas')
			if(TestDateTime($value))
				return true;
				else{
					$this->AddError(sprintf(TPOLOZKA_DATUM_CAS_CHYBA, $this->items[$id]['nazev']));
					return false;
				}
		
		return true;
		
	}
	
	private function AddError($message){
		//echo $message;
		$_SESSION[SESSION_FORM_ERROR_MESSAGE.$this->form['id']] .= $message."<br/>";
		//echo $_SESSION[SESSION_FORM_ERROR_MESSAGE.$this->form['id']];
	}
	
	private function AddOk($message){
		$_SESSION[SESSION_FORM_OK_MESSAGE.$this->form['id']] .= $message."<br/>";
	}
	
	public function GetOk(){
		$ok = $_SESSION[SESSION_FORM_OK_MESSAGE.$this->form['id']];
		if($ok!='') return "<div class='ok'>".$ok."</div>";
		return;
	}
	
	public function GetError(){
		$error = $_SESSION[SESSION_FORM_ERROR_MESSAGE.$this->form['id']];
		if($error!='') return "<div class='error'>".$error."</div>";
		return;
	}
	
	public function GetMessage(){
		$ok = $_SESSION[SESSION_FORM_OK_MESSAGE.$this->form['id']];
		$error = $_SESSION[SESSION_FORM_ERROR_MESSAGE.$this->form['id']];
		
		//echo "okmessage".$ok;
		
		
		$res = "";
		
		if($error!='') {
			$res = $this->GetError();
			}
		elseif($ok!='') {
			$res = $this->GetOk();
			}
			else 
			return;
			
		unset($_SESSION[SESSION_FORM_ERROR_MESSAGE.$this->form['id']]);
		unset($_SESSION[SESSION_FORM_OK_MESSAGE.$this->form['id']]);
		return $res;
	}
	
	//prohleda polozky formulare a urci polozku, ktera pravdepodobne obsahuje email odesilatele formulare. Vraci id polozky
	private function GetEmailSender(){
		foreach($_POST[$this->itemNamePrefix] AS $i => $v)
			if($this->items[$i]['format']=='email') return trim($_POST[$this->itemNamePrefix][$i]);
		return '';
	}
	
	private function GetPostItem($id){

        $result = "";
        
        
        
		if(!is_int($id)){
			$id = intval(ltrim(rtrim($id[0],"]"),"post["));
			}
  
		if(isset($_SESSION[$this->itemNamePrefix][$id]) && !is_array($_SESSION[$this->itemNamePrefix][$id])){
			return $_SESSION[$this->itemNamePrefix][$id];
			}
			else{
			 if(!isset($this->items[$id])) return $result;
             if($this->items[$id]['format']=='url') $result = PROTOKOL;
             elseif($this->items[$id]['format']=='email') $result = "@";
			return $result;
            }
		
	}
	
	
	
	
	
}

?>