<?php

/**
 * @author Pavel Srom
 * @copyright 2011
 */



class C_EmailContent{
    
    private $content_html;
    private $content_text;
    private $content_subject;
    private $templates;
    private $idCampaign;
    private $names;
    
    private $result_text;
    private $result_html;
    private $result_subject;
    
    public function __construct($idCampaign, $content_html = "", $content_text = "", $content_subject = ""){

        $this->setHtmlContent($content_html);
        $this->setTextContent($content_text);
        $this->setSubjectContent($content_subject);
        $this->idCampaign = $idCampaign;
        $this->setTemplates();
        $this->setNames();
               
        
    }

    
    private function setNames(){
        global $db;
        
        $data = $db->Query("SELECT pad1, pad2, pad3, pad4, pad5, pad6, pad7 FROM ".TABLE_EMAILY_JMENA);
        
        while($n = $db->getAssoc($data)){
            for($i = 1; $i <= 7; $i++){
                $this->names[mb_strtolower($n['pad1'], 'utf-8')][$i] = $n['pad'.$i];
                }
            }
        
        //print_r($this->names);
        
    }
    
    public function setTemplates($templates = null){
        global $db;
        
        $this->templates = array();
        
        if(!$templates){
            $data = $data = $db->Query("SELECT * FROM ".TABLE_EMAILY_SABLONY_NASTAVENI." WHERE idKampane=".$this->idCampaign." LIMIT 1");
    
            
        
            if($db->numRows($data)>0)
                $this->templates = $db->getAssoc($data);
            }
            else
            $this->templates = $templates;
            
    }
    
    
    public function setHtmlContent($content_html){
        
        $this->content_html = $content_html;
        $this->result_html = $content_html;
        
    }
    
    public function setTextContent($content_text){
        
        $this->content_text = $content_text;
        $this->result_text = $content_text;
        
    }
    
    public function setSubjectContent($content_subject){
        $this->content_subject = $content_subject;
        $this->result_subject = $content_subject;
    }
    
    public function setLogoutLink($link=""){
        
        $link_text = $link;
        $link = "<a href='".$link."' title='".$this->templates['urlOdhlasit']."'>".$this->templates['urlOdhlasit']."</a>";
                    
        $this->replace("{odhlasit}", $link, "", $link_text);
    }
    
    
    public function setViewLink($link_html="", $link_text=""){
        
        $link_html = "<a href='".$link_html."' title='".$this->templates['urlZobrazit']."'>".$this->templates['urlZobrazit']."</a>";
        
            
        $this->replace("{zobrazit}", $link_html, "", $link_text);    
    }
    
    
    public function setAll($name, $surname, $company, $email, $urlLogout="", $urlView_html="", $urlView_text=""){
        $this->setName($name);
        $this->setSurname($surname);
        $this->setCompany($company);
        $this->setEmail($email);
        $this->setLogoutLink($urlLogout);
        $this->setViewLink($urlView_html, $urlView_text);   
    }
    
    public function setSurname($surname){
        $surname = ($surname != '' ? $surname : $this->templates['prijmeni']);
        $this->replace("{prijmeni}", slcfirst($surname));
        $this->replace("{Prijmeni}", sucfirst($surname));
    }
    
    public function setEmail($email){
        $email = ($email != '' ? $email : '');
        $this->replace("{email}", slcfirst($email));
        $this->replace("{Email}", sucfirst($email));
    }
    
    public function setCompany($company_name){
        $company_name = ($company_name != '' ? $company_name : $this->templates['firma']);
        $this->replace("{spolecnost}", slcfirst($company_name));
        $this->replace("{Spolecnost}", sucfirst($company_name));
    }
    
    public function setName($name){
        $pady = $pady_alt = array();
        
        $name = mb_strtolower($name, 'utf-8');
        
        if(isset($this->names[$name])){
            for($i = 1; $i <= 7; $i++){
                //$j = "pad".$i;
                $pady[$i] = $this->names[$name][$i];
                }
            }
            
        for($i = 1; $i <= 7; $i++){
            $j = "jmeno".$i;
            $pady_alt[$i] = $this->templates[$j];
            }
            
        for($i = 1; $i <= 7; $i++){
            $this->replace('{jmeno'.$i.'}',slcfirst(isset($pady[$i]) ? $pady[$i] : $pady_alt[$i]), slcfirst($pady_alt[$i]));
            }
        for($i = 1; $i <= 7; $i++){
            $this->replace('{Jmeno'.$i.'}',sucfirst(isset($pady[$i]) ? $pady[$i] : $pady_alt[$i]), sucfirst($pady_alt[$i]));
            }
  
        
    }
    
    
    private function replace($resource, $dest, $dest_alt = "", $dest_text=""){
        
        if($dest_text == "")
            $dest_text = $dest;
               
        $this->result_html = str_replace($resource, $dest == "" ? $dest_alt : $dest, $this->result_html);
        $this->result_text = str_replace($resource, $dest_text == "" ? $dest_alt : $dest_text, $this->result_text);
        $this->result_subject = str_replace($resource, $dest_text == "" ? $dest_alt : $dest_text, $this->result_subject);
    
    }
    
    
    public function resetContent(){
        $this->result_html = $this->content_html;
        $this->result_text = $this->content_text;
        $this->result_subject = $this->content_subject;
    }
    
    
    public function getResultHtml(){
        return $this->result_html;
    }
    
    
    public function getResultText(){
        return $this->result_text;
    }
    
    public function getResultSubject(){
        return $this->result_subject;
    }
}


?>