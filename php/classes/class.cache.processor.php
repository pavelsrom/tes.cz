<?php

/**
 * @author Pavel Srom
 * @copyright 2015
 */



class CacheProcessor{
    
    
    //adresa, pro ukladani cache
    private $dir = "cache/";
    
    //obsahuje nacachovana data
    private $data = "";
    
    //pocet milisekund, kdy dojde k obnove cache, defaultne nastaveno na 24h
    private $expires = 0;
    
    //nazev souboru s cacheovanym html kodem
    private $name = "";
    
    //indikuje zda je cachovani zapnute v administraci
    private $caching_active = false;
    
    //konstruktor
    public function __construct()
    {
                
        $this->set_expires_default();
        
        //pokud je uzivatel prihlasen k privatnim strankam, pak kesovani neprobiha. Protoze by se musely generovat systemove boxy na miru prihlasenemu uzivateli.
        $this->caching_active = CACHE_ACTIVE;
    }
    
    public function set_expires_default()
    {
        $this->set_expires(CACHE_INTERVAL);
    }
    
    public function set_dir($dir)
    {
        $this->dir = $dir;
    }
    
    //nastavi dobu expirace v minutach   
    public function set_expires($m)
    {
        $this->expires = /*60 * */60 * intval($m);
        return $this;
    }
    
    //nastavi nazev cache - identifikator cache neboli nazev souboru cache
    public function set_name($name)
    {
        $this->name = $name;
        return $this;
    }
    
    
    //zjisti zda vyprsela cache
    //vraci true, pokud je cache stara, jinak vraci false
    public function expired()
    {
        //pokud je predan parametr recache v url, dojde k prekesovani vsech casti stranek
        if(isset($_GET['recache']))
            return true;
        
        if(!$this->caching_active)
            return true;
        
        $file = $this->get_file_name();
        
        if(file_exists($file))
        {
            
            $m = filemtime($file) + $this->expires;
            $now = time();
                        
            return $m < $now;
            
        }
        
        return true;
        
        
    }
    
    private function get_file_name()
    {
        return $this->dir."cache.".$this->name.".html";
    }
    
    //start caching
    public function start()
    {
        $this->data = "";
        
        //pokud neni cachovani aktivni, cachovani prerusi
        if(!$this->caching_active)
            return;
            
        
        ob_start();
    }
    
    //end caching
    public function end($save = true)
    {
        //pokud neni cachovani aktivni, cachovani prerusi
        if(!$this->caching_active)
            return;
        
        $this->data = ob_get_contents();
        ob_clean();
        
        if($save)
            $this->save();
                
    }
    
    //nastavit vygenerovane html do souboru cache
    public function set_content_cache($content)
    {
        
        //pokud neni cachovani aktivni, cachovani prerusi
        if(!$this->caching_active)
            return;
            
        $this->data = $content;
        $this->save();    
    }
    
    
    //vraci obsah pameti
    public function get_data()
    {
        return $this->data;
    }
    
    //ulozi obsah pameti do cache
    public function save()
    {
        if(!$this->caching_active)
            return;
            
        $file = $this->get_file_name();
        file_put_contents($file, $this->data);
    }
    
    //nacte obsah cache ze souboru
    public function load()
    {
        if(!$this->caching_active)
            return false;
            
        $file = $this->get_file_name();
        
        if(file_exists($file))
            return /*"NACTENO Z CACHE ".*/file_get_contents($file);
            
        return false;
    }
    
    //zavola url adresu stranky s parametrem recache, aby doslo k aktualizaci cache
    //funkce se pouziva v administraci ibisu k resetu cache pri ulozeni clanku/novinky/akce/
    public function reset($url = "")
    {
        $url = PROTOKOL.$_SERVER['HTTP_HOST']."/".$url."?recache";
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 15);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        curl_setopt($ch, CURLOPT_NOBODY, false);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $resp = curl_exec($ch);
        curl_close($ch);
        
    }
    
    public function delete($name)
    {
        $this->set_name($name);
        $file = $this->get_file_name();
        //echo $file;
        if(file_exists($file))
        {
            unlink($file);
              
        }
    }
    
    
    public function delete_all($pattern = "*")
    {
        $adr = glob($this->dir."cache.".$pattern);
        if($adr === false)
            return false;
        if(count($adr) > 0)
        {
            foreach ($adr as $filename) {
                unlink($filename);
            }
        }

    }
    

    
}


?>