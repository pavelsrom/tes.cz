<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */


class C_WebLangs
{

    //asociativni pole s jazyky a informacemi o nich
    private $langs = array();
    
    //id aktivniho jazyka, ktery je zrovna spravovan
    private $id_active_lang = 0;
    
    //konstruktor, vstupni parametr je pole id jazyku, ktere ma uzivatel pravo spravovat
    public function __construct( $active_lang , $idDomeny)
    {
        global $db;
                
        $data = $db->query("SELECT j.*, 
            IF(j.jazyk='".$active_lang."',1,0) AS aktivni 
            FROM ".TABLE_JAZYKY." AS j
            LEFT JOIN ".TABLE_DOMENY_JAZYKY." AS dj ON j.idJazyka = dj.idJazyka
            WHERE j.aktivni=1
                AND dj.idJazyka IS NOT NULL
                AND dj.idDomeny = '".$idDomeny."'
            GROUP BY j.idJazyka
                ");
        
        while($j = $db->getAssoc($data))
        {
            $this->langs[$j['jazyk']] = $j;
            
            if($j['aktivni'] == 1)
                $this->id_active_lang = $j['idJazyka'];
        }
        
        
            
        
    }
         
       
    
    //vraci pole se vsemi jazyky
    public function get_langs()
    {
        return $this->langs;
    }
          
   
    //vraci id aktivniho jazyka
    public function get_id()
    {
        return $this->id_active_lang;
    }
    
    //vraci pocet aktivnich jazyku pro danou domenu
    public function get_langs_count()
    {
        return count($this->langs);
    }
    
}

?>