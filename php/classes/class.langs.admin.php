<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */


class C_WebLangs
{

    //asociativni pole s jazyky a informacemi o nich
    private $langs = array();
    
    //id aktivniho jazyka, ktery je zrovna spravovan
    private $id_active_lang = 0;
    
    //konstruktor, vstupni parametr je pole id jazyku, ktere ma uzivatel pravo spravovat
    public function __construct( $userLangs , $idDomeny, $id_active_lang)
    {
        global $db;
        
        if(count($userLangs) == 0) return false;
        
        $data = $db->query("SELECT j.*, IF(j.idJazyka IN (".implode(",", $userLangs)."), 1, 0) pristup
            FROM ".TABLE_JAZYKY." AS j
            LEFT JOIN ".TABLE_DOMENY_JAZYKY." AS dj ON j.idJazyka = dj.idJazyka
            WHERE  
                j.aktivni=1
                AND dj.idJazyka IS NOT NULL
                AND dj.idDomeny = '".$idDomeny."'
            ORDER BY dj.poradi, j.idJazyka
                ");
        
        while($j = $db->getAssoc($data))
        {
            $j['nazev'] = constant($j['nazev']);
            $this->langs[$j['idJazyka']] = $j;
        }
                
        if($id_active_lang > 0 && $this->is_allowed($id_active_lang))
            $this->id_active_lang = $id_active_lang;
            else
            $this->set_first_lang();
            
        
    }
    
    public function get_all_in_array()
    {
        global $db;
        if(count($this->langs) == 0) 
            return false;
            
        $result = array();
        foreach($this->langs AS $idj => $j)
            $result[$idj] = $j["nazev"]." (".$j["jazyk"].")";
            
        return $result;
    }
    
    //pouziva se pri importu, vstupem je napr. "cz" a vystupem je id 1, pokud neni nalezeno, vraci se id aktivniho jazyka
    public function get_id_by_name($name)
    {
        
        foreach($this->langs AS $idj => $j)
        {
            if($j['jazyk'] == $name)
                return $idj;
            
            
        }
        
        return WEB_LANG_ID;
        
        
        
    }
    
    //vraci pozadovanou hodnotu podle key, pokud neni uvedena vraci asociativni pole s informacemi o jazyku s danym id
    public function get($id, $key='')
    {
        if(count($this->langs) == 0) 
            return false;
            
        foreach($this->langs AS $idl => $l)
            if($idl == $id && $l["pristup"] == 1)
                if($key == '')
                    return $l;
                    else
                    return isset($l[$key]) ? $l[$key] : "";
          
        return false; 
    }
    
    
    //vraci pozadovanou hodnotu podle key, pokud neni uvedena vraci asociativni pole s informacemi o aktualnim jazyku
    public function get_active_lang($key='')
    {
        if(count($this->langs) == 0) 
            return false;
            
        foreach($this->langs AS $idl => $l)
            if($idl == $this->id_active_lang && $l["pristup"] == 1)
                if($key == '')
                    return $l;
                    else
                    return isset($l[$key]) ? $l[$key] : "";
          
        return false;      
            
    }
    
    
    //vraci true pokud je jazyk s danym id povolen pro prihlaseneho uzivatele
    public function is_allowed($id = WEB_LANG_ID)
    {
        return isset($this->langs[$id]) && $this->langs[$id]["pristup"] == 1;
    }
    
    
    //nastavi aktualni jazyk podle id, vraci true pokud se podarilo nastavit aktualni jazyk, v opacnem pripade vraci false
    public function set_active_lang($id)
    {
        if(!$this->is_allowed($id)) 
            return false;
        
        $this->id_active_lang = $id;
        return true;
            
    }
    
    
    //vraci pole se vsemi jazyky
    public function get_langs()
    {
        return $this->langs;
    }
    
    
    //vraci id prvniho povoleneho jazyka, pokud se takovy nepodari najit vraci false
    public function get_first_lang()
    {
        if(count($this->langs) == 0) 
            return false;
            
        foreach($this->langs AS $idl => $l)
            if($l["pristup"] == 1)
                return $idl;
    }
    
    
    //nastavi jako aktivni jazyk prvni povoleny
    public function set_first_lang()
    {
        $id = $this->get_first_lang();
        
        if($id)
            $this->id_active_lang = $id;
            else
            return false;
    }
    
   
    //vraci id aktivniho jazyka
    public function get_id()
    {
        return $this->id_active_lang;
    }
    
    //vraci pocet aktivnich jazyku pro danou domenu
    public function get_langs_count()
    {
        return count($this->langs);
    }
    
}

?>