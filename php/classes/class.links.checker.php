<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */


//trida na kontrolu odkazu v obsahu stranek
class LinksChecker{
    
    
    //obsahuje kontrolovany obsah 
    private $content = "";
    
    //id jazyka
    private $lang_id = "";
    
    //id domeny
    private $domain_id = 0;
    
    //obsahuje id objektu (stranky, novinky, editboxu, clanku, akce)
    private $object_id = 0;
        
        
    //obsahuje typ objektu - novinka, stranka, editbox, clanek, akce
    private $object_type = "";
        
        
    //povolene typy objektu
    private $allow_object_type = array("stranka", "newsletter", "editbox");
    
    
    //kontrolovat odkazy
    private $bad_links_control = true;
    
    
    //obsahuje vsechny separovane odkazy, texty odkazu, navratove kody http, ulozeno v poli
    private $all_links = array();
    
    //indikuje zda je analyzovan perex
    private $perex = false;
      
    //overene domeny - domeny, ktere byly overene pomoci vyhledavani dns zaznamu
    private $verified_domain = array();
    
    //obsahuje zakladni url adresu od ktere se pak odvozuji absolutni adresy z internich relativnich adres 
    private $base_url = "";
      
    //id db sloupecku, ktery obsahuje alternativni obsah = popis u produktu
    private $alternative_content_id = 0;
      
    //mime hlavi�ky
    private $content_types = array(
        "html" => "text/html",
        "htm" => "text/html",
        "txt"  => "text/plain",
        "doc"  => "application/msword",
        "docx" => "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
        "xls"  => "application/excel",
        "xlsx" => "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
        "pdf"  => "application/pdf",
        "xml"  => "text/xml",
        "jpg"  => "image/jpeg",
        "jpeg" => "image/jpeg",
        "jpe" => "image/jpeg",
        "gif"  => "image/gif",
        "png"  => "image/png",
        "zip" => "application/zip",
        "avi" => "video/avi",
        "bmp" => "image/bmp",
        "mid" => "audio/midi",
        "midi" => "audio/midi",
        "mov" => "video/quicktime",
        "mp2" => "audio/mpeg",
        "mp3" => "audio/mpeg3",
        "mpeg"=> "video/mpeg",
        "mpg" => "video/mpeg",
        "ppt" => "application/mspowerpoint",
        "pps" => "application/mspowerpoint",
        "pptx"=> "application/vnd.openxmlformats-officedocument.presentationml.presentation",
        "ppsx"=> "application/vnd.openxmlformats-officedocument.presentationml.slideshow",
        "psd" => "application/octet-stream",
        "tif" => "image/tiff",
        "tiff"=> "image/tiff",
        "wav" => "audio/wav"
        );  
        
    //konstruktor
    public function __construct($domain_id, $object_id = 0, $object_type = "stranka", $content = "", $perex = false, $bad_links_control = true, $base_url = "", $alternative_content_id = 0)
    {
        $this->set_options($domain_id,$object_id, $object_type, $content, $perex, $bad_links_control, $base_url,$alternative_content_id);
        $this->lang_id = defined('WEB_LANG_ID') ? WEB_LANG_ID : 1;
    }
    
    
    //nacte obsah, ktery se bude kontrolovat
    public function set_content($content)
    {
        $this->content = $content;
    }
    
    //nastavi jazyk odkazu
    public function set_lang($lang_id)
    {
        $this->lang_id = $lang_id;
    }
    
    
    
    //nastavi vsechny dulezite udaje pro analyzu a ulozeni vysledku
    //v pripade uspesneho nastaveni vraci true, jinak false
    public function set_options($domain_id, $object_id, $object_type="", $content="", $perex = false, $bad_links_control = true, $base_url = "", $alternative_content_id = 0)
    {
        $object_id = intval($object_id);
        
        if($object_id <= 0) 
            return false;
        
        if(!in_array($object_type, $this->allow_object_type)) 
            return false;
        
        $this->object_id = $object_id;
        $this->object_type = $object_type;
        $this->perex = intval($perex);
        $this->bad_links_control = $bad_links_control;
        $this->content = $content;
        $this->base_url = $base_url;
        $this->domain_id = $domain_id;
        $this->alternative_content_id = $alternative_content_id;
        return true;
    }
    
    
    
    //otestuje zda se jedna o link a v pripade ze ano, vrati true. V opacnem pripade vraci false
    private function test_link_struct($url)
    {
        $url = trim($url);
		$maska = "/^(http|https|ftp):\/\/[[:alnum:]]+([-_\.]?[[:alnum:]])*\.[[:alpha:]]{2,4}/";
		$maska2 = "/^(http|https|ftp):\/\/localhost/";
		if(preg_match($maska ,$url)) return true;
		if(preg_match($maska2 ,$url)) return true;
		return false;
    }
     
     
    public function check_url($url){
      
      //pro pripad ze jsou v url mezery
      $url = str_replace(" ","%20", trim($url));  
        
      $parts=parse_url($url);
      if(!$parts) return false; /* the URL was seriously wrong */
     
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $url);
     
      /* set the user agent - might help, doesn't hurt */
      curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)');
      curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
     
      /* try to follow redirects */
      //curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
     
      /* timeout after the specified number of seconds. assuming that this script runs
        on a server, 20 seconds should be plenty of time to verify a valid URL.  */
      curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 15);
      curl_setopt($ch, CURLOPT_TIMEOUT, 20);
     
      /* don't download the page, just the header (much faster in this case) */
      if(isset($parts['scheme']) && ($parts['scheme']=='http' || $parts['scheme']=='https')){
        curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_setopt($ch, CURLOPT_HEADER, true);
        }
     
      /* handle HTTPS links */
      if(isset($parts['scheme']) && $parts['scheme']=='https'){
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,  2);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      }
     
      $response = curl_exec($ch);

      if($parts['scheme'] == 'ftp' || $parts['scheme'] == 'ftps')
      {
        $r = curl_getinfo($ch);
        $code = intval($r['http_code']);
      }  
      else
      {
     
          /*  get the status code from HTTP headers */
          if(preg_match('/HTTP\/1\.\d+\s+(\d+)/', $response, $matches)){
            $code=intval($matches[1]);
          } else {
            return array("code" => "404", "type" => "");
          };
      }
     
      curl_close($ch);
     
      /* see if code indicates success */
      if($code == 0) $code = "404";
      return array("code" => $code, "type" => ""); 
      //return (($code>=200) && ($code<400));
    }  

     
    private function check_url1($url)
    {

        $agent = "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)";
        $ch=curl_init();
    
        curl_setopt ($ch, CURLOPT_URL,$url );
        //curl_setopt($ch, CURLOPT_USERAGENT, $agent);
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt ($ch,CURLOPT_VERBOSE,false);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_NOBODY, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

        $page=curl_exec($ch);
        //echo curl_error($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if($httpcode == 0)
            $httpcode = "404";
            
        curl_close($ch);
    
        return array("code" => $httpcode, "type" => ""); 


    }  
    
    
    
    //vraci zakladni absolutni url z relativni url adresy
    function set_base_url($url)
    {
        $data = explode("/", $url);
        $pocet = count($data);
        
        if(isset($data[$pocet - 1]))
            unset($data[$pocet - 1]);
        
        $result_url = implode("/", $data);
        
        if($result_url == '/')
        {
            $this->base_url = "";
            return;
        }    
            
        
        //pokud je adresa absolutni
        if(strstr($result_url, $_SERVER['SERVER_NAME'])) 
        {
            $this->base_url = $result_url."/";
            return;
        }
            
            
        if(substr($result_url, 0, 1) == "/")
            $this->base_url = PROTOKOL.$_SERVER['SERVER_NAME'].$result_url."/";
            else
            $this->base_url = PROTOKOL.$_SERVER['SERVER_NAME'].$result_url."/";
        
    }
    
    
    
    //vytvori z relativni url adresy, absolutni url s domenou aktualniho http hostu na protokolu http
    //vstupem je relativni url adresa
    private function get_absolute_url( $url )
    {
        
        if(substr($url, 0 , 1) != "/")
            if($this->base_url == "") 
                $url = "/".$url;
                else
                return $this->base_url.$url;

            
        return PROTOKOL.$_SERVER['HTTP_HOST'].$url;
    }
    
    
    
    private function find_all_a()
    {
        
        //$maska = '/<a ([^<>])*>([^<>])*<\/a>/';
        //$maska = "<a\s[^>]*href=(\"??)([^\" >]*?)\\1[^>]*>(.*)<\/a>";
        $maska = "<a\s[^>]*href\s*=\s*([\"\']??)([^\">]*?)\\1[^>]*>(.*)<\/a>";
        preg_match_all("/".$maska."/simU", $this->content, $matches);

        //preg_match_all("/".$maska."/siU", $this->content, $matches);
        
        //print_r($matches);
        
        if(isset($matches[0]) && count($matches[0]) > 0)
        {
            foreach($matches[0] AS $id => $m)
            {
                
                $url = isset($matches[2][$id]) ? $matches[2][$id] : "";
                $text = isset($matches[3][$id]) ? $matches[3][$id] : "";
                
                //pokud je zachycen odkaz na email nebo javascriptovy kod nebo kotva pak odkaz se ingnoruje
                if(stristr($url, "mailto:") || strstr($url, "javascript:") || strstr($url, "data:") || $url[0] == "#") continue;
             
 
                //zjisteni url adresy
                /*
                preg_match("/href=[\'|\"]{1}([^\'\"]+)[\'|\"]{1}/", $m, $mdata1);
                $url = isset($mdata1[1]) ? $mdata1[1] : "";
                */
                
                
                
                //zjisteni anchor textu
                /*
                preg_match("/>([^<]*)<\/a>/", $m, $mdata2);
                $text = isset($mdata2[1]) ? $mdata2[1] : "";
                */      
                $text = strip_tags($text);

                //zjisteni zda jde o relativni ci absolutni adresu
                if(!stristr($url, "http://") && !strstr($url, "https://") && !strstr($url, "ftp://"))
                {
                    $absolute_url = $this->get_absolute_url( $url ); 
                }
                else
                    $absolute_url = $url;
                
                if(stristr($url, $_SERVER['HTTP_HOST']) || (!stristr($url, "http://") && !strstr($url, "https://") && !strstr($url, "ftp://")))
                    $link_type = "internal";
                    else
                    $link_type = "external";                
                
                $file_type = "html";
                
                $this->all_links[] = array(
                    "url" => $url, 
                    "text" => $text, 
                    "code" => "", 
                    "absolute_url" => $absolute_url, 
                    "link_type" => $link_type, 
                    "content_type" => "", 
                    "file_type" => $file_type
                    );
                
            }
                
        }
        
    }
    
    
    
    private function find_all_img()
    {
        $maska = '/<img[^<>]*[\/]{0,1}>/';
        
        
        preg_match_all($maska, $this->content, $matches_img);
        
        if(isset($matches_img[0]) && count($matches_img[0]) > 0)
        {
           
            foreach($matches_img[0] AS $id => $m)
            {
                //zjisteni url adresy
                
                
                
                preg_match("/src=[\'|\"]{1}([^\'\"]+)[\'|\"]{1}/", $m, $mdata1);
                $url = isset($mdata1[1]) ? $mdata1[1] : "";
                
                
                
                $text = "";

                //pokud je zachycen odkaz na email nebo javascriptovy kod nebo kotva pak odkaz se ingnoruje
                if(stristr($url, "mailto:") || strstr($url, "javascript:") || strstr($url, "data:") || $url[0] == "#") continue;
                
                

                //zjisteni zda jde o relativni ci absolutni adresu
                if(!stristr($url, "http://") && !strstr($url, "https://") && !strstr($url, "ftp://"))
                {
                    $absolute_url = $this->get_absolute_url( $url ); 
                }
                else
                    $absolute_url = $url;
                
                if(stristr($url, $_SERVER['HTTP_HOST']) || (!stristr($url, "http://") && !strstr($url, "https://") && !strstr($url, "ftp://")))
                    $link_type = "internal";
                    else
                    $link_type = "external";                
                
                $file_type = "image";
                
                $this->all_links[] = array(
                    "url" => $url, 
                    "text" => $text, 
                    "code" => "", 
                    "absolute_url" => $absolute_url, 
                    "link_type" => $link_type, 
                    "content_type" => "", 
                    "file_type" => $file_type
                    );
                
            }
                
        }
    }
    
    
    
    //najde vsechny odkazy v obsahu
    //vraci: pole odkazu s anchor textem, url adresou
    private function find_all_links()
    {

        //prevedeni html entit na text v kodovani utf-8
        
        $this->content = html_entity_decode($this->content, ENT_COMPAT, "UTF-8");
        $this->content = str_replace("\\\"", "\"", $this->content);
        $this->content = str_replace("\\'", "'", $this->content);

        //nalezeni odkazu v parametru href tagu A  - kompletni obsahy tagu A
        $this->find_all_img();
        
        //nalezeni odkazu v parametru src u tagu IMG
        $this->find_all_a();
        

    } 
    
    
    
    //testuje vsechny odkazy a kazdemu odkazu priradi stav - http odpoved serveru
    //pokud je testovani linku zakazano vraci false, jinak vraci true
    private function all_links_test()
    {
        if(!$this->bad_links_control) return false;
        
        $all_links = $this->all_links;

        if(count($all_links) > 0)
            foreach($this->all_links AS $id => $l)
            {
                $r = $this->check_url( $l['absolute_url'] );
                $all_links[$id]['code'] = substr($l['url'],0,1) == '#' ? "200" : $r["code"];
                $all_links[$id]['content_type'] = $r['type'];
            }
          
        $this->all_links = $all_links;

        return true;
        
    }
    
    
    
    //ulozi vysledky analyzy do databaze
    private function save()
    {
        global $db;
           
        //smazani starych zaznamu
        $db->delete(TABLE_ODKAZY, "WHERE idObjektu=".$this->object_id." AND typ='".$this->object_type."' AND perex='".$this->perex."' AND idPopisu = ".$this->alternative_content_id);
           
        //zjisteni zda je odkaz jiz v databazi, pokud ne, tak je zalozen novy zaznam
        foreach($this->all_links AS $id => $l)
        {
            $url = strtolower($l['absolute_url']);
            
            
            $values = array(
                "text" => $l['text'],
                "url" => str_replace(array("'","\""),array('',''),$l['url']),
                "absolutniUrl" => str_replace(array("'","\""),array('',''),$l['absolute_url']),
                "idObjektu" => $this->object_id,
                "typ" => $this->object_type,
                "perex" => $this->perex,
                "idPopisu" => $this->alternative_content_id,
                "idJazyka"=>$this->lang_id,
                "idDomeny"=>$this->domain_id
                ); 
            
            $values["typSouboru"] = $l['file_type'];
            $values["typOdkazu"] = $l['link_type'];    
            if($this->bad_links_control)
            {
                $values['datumKontroly'] = "now";
                $values["kod"] = $l['code'];
                $values["typObsahu"] = $l['content_type'];   
                 
                  
            } 
            
            $db->insert(TABLE_ODKAZY, $values);
           
            
        }
        
        
                   
        
        
    }
    
   
   
    //najde v obsahu chybne odkazy, otestuje je a ulozi je do db
    public function process()
    {
        //najde vsechny odkazy
        $this->find_all_links();
        
        //zkontroluje vsechny odkazy
        if($this->bad_links_control)
            $this->all_links_test();
     
        
        //ulozi vysledky do databaze
        $this->save(); 

        
        
    }
    
    
    
    private function all_links_init($limit, $cron=true)
    {
        global $db;
        
        
        //nacte odkazy z db
        if($cron)
            $where_pro_cron = "DATE_FORMAT(datumKontroly, '%Y.%m.%d')!=DATE_FORMAT(NOW(), '%Y.%m.%d') OR";
            else
            $where_pro_cron = "1=1 OR ";
        
        $data = $db->query("SELECT * 
            FROM ".TABLE_ODKAZY." 
            WHERE ".$where_pro_cron." datumKontroly IS NULL OR 
                datumKontroly='' OR 
                datumKontroly='0000-00-00 00:00:00' 
                OR kod=0 
            ORDER BY datumKontroly 
            LIMIT ".intval($limit));
        
              
        while($l = $db->getAssoc($data))
            $this->all_links[] = array(
                    "id" => $l['idOdkazu'],
                    "url" => $l['url'], 
                    "text" => $l['text'], 
                    "code" => $l['kod'], 
                    "absolute_url" => $l["absolutniUrl"], 
                    "link_type" => $l['typOdkazu'], 
                    "content_type" => $l['typObsahu'], 
                    "file_type" => $l['typSouboru']
                    ); 
                    
        //print_r($this->all_links);
                    
    }
    
    
    
    //nacte z databaze vsechny odkazy, otestuje je a ulozi je do db
    //vraci true pokud dojde k ulozeni alespon jednoho odkazu, false pokud nedojde ke zpracovani z duvodu prazdne fronty odkazu
    public function process_cron($limit = 20, $spustil_cron = true)
    {
        global $db;
        
        $this->all_links_init($limit, $spustil_cron);           
             
        if(count($this->all_links) == 0) return false;
                    
         //zkontroluje vsechny odkazy
        if($this->bad_links_control)
            $this->all_links_test();
            
        
            
        //ulozeni vysledku analyzy
        foreach($this->all_links AS $id => $l)
        {
            $url = $l['absolute_url'];
            $update = array(
                "kod" => $l['code'],
                "typOdkazu" => $l['link_type'],
                "typObsahu" => $l['content_type'],
                "typSouboru" => $l['file_type']
                );
                
            if($this->bad_links_control)
                $update["datumKontroly"] = "now";
                
            $db->update(TABLE_ODKAZY, $update, "idOdkazu='".$l['id']."'");
        } 
        
        return true;    
        
    }
    
    
    
    //vraci pocet nalezenych chybnych odkazu v tagu A
    public function get_sum_error_links()
    {
        
        $error_links_count = 0;
        foreach($this->all_links AS $l){
            $code = intval($l['code']);
            if($code >= 400 && $l['file_type']=='html')
                $error_links_count++;
            }
            
        return $error_links_count;
    } 
    
    
    
    //vraci pocet nalezenych chybnych odkazu v tagu IMG
    public function get_sum_error_images()
    {
        
        $error_links_count = 0;
        foreach($this->all_links AS $l){
            $code = intval($l['code']);
            if($code >= 400 && $l['file_type']=='image')
                $error_links_count++;
            }
            
        return $error_links_count;
    }
    
    
    
}



?>
