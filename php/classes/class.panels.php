<?php

/**
 * @author Pavel Srom
 * @copyright 2010
 */


class C_Panel{
	
	
	//polozky panelu
	private $items;
	
    //zobrazi nazvy editboxu
    private $show_name = false;
    
    //tag, ktery obaluje nazev editboxu
    private $name_tag = "h2";
    
    //povoli vypsani javascriptu pro slider
    private $allow_javascript = true;
    
    //typ panelu - napr. dolni, horni, top, paticka atd.
    private $type = "";
    
	//konstruktor tridy, typ je pravy, levy, horni, dolni, sponzor
	public function __construct($typ = "", $idProfilu=0){
		global $db;
        global $page;

        

		$this->items = array();
        
        if($typ == "")
            return;
        
        if($idProfilu==0){
            $idProfilu = intval($page->get('idProfiluPanelu'));
            $idProfilu = $db->get(TABLE_PANELY_SABLONY, "idProfilu", "idDomeny=".DOMAIN_ID." AND (idProfilu=".$idProfilu." OR vychozi=1) AND idJazyka='".WEB_LANG_ID."' ORDER BY vychozi");
            if($idProfilu == 0) return;
  
            }
        
        
		$data = $db->Query("SELECT pp.idObjektu, pt.systemovyNazev, m.konstanta AS modul
            FROM ".TABLE_PANELY_SABLONY." AS ps
            LEFT JOIN ".TABLE_PANELY_POLOZKY." AS pp ON ps.idProfilu=pp.idProfilu 
            LEFT JOIN ".TABLE_PANELY." AS p ON pp.idPanelu=p.idPanelu
            LEFT JOIN ".TABLE_PANELY_POLOZKY_TYPY." AS pt ON pt.idTypu=pp.idTypu 
            LEFT JOIN ".TABLE_CMS_MODULY." AS m ON m.idModulu=pt.idModulu
			WHERE ps.idDomeny=".DOMAIN_ID."
                AND ps.idJazyka='".WEB_LANG_ID."' 
				AND p.systemovyNazev='".$typ."'
                AND ps.zobrazit=1
                AND ps.idProfilu='".$idProfilu."'
			ORDER BY ps.vychozi=0, pp.priorita, pp.idObjektu
            
            ");
        
        $this->type == $typ;
        
		while($panel = $db->getAssoc($data))
            if(defined($panel['modul']) && constant($panel['modul']))
                $this->items[] = array('id'=>$panel['idObjektu'], 'typ'=>$panel['systemovyNazev']);
        
	}
    
    //nastavi priznak zda se ma k editboxu generovat i jeho nazev ci nikoliv
    public function set_show_name($v = true)
    {
        $this->show_name = $v;
    }
    
    //nastavi obalujici tag pro nazev
    public function set_wrap_tag_for_name($v = "h2")
    {
        $this->name_tag = $v;
    }
    
    //povoli/zakaze vypsani javascriptu k editboxu
    public function allow_javascript($allow = true)
    {
        $this->allow_javascript = $allow;
    }
    
    //blok je pole, obsahujici id a typ
    public function GetBox($blok, $id = "")
    {
        $html = "";
        
        if(!is_array($blok))
        {
            $blok = array("typ" => $blok, "id" => $id);
        }
        
        
        
        if($blok['typ']=='novinky')
        {
            if(!class_exists('C_News'))
                include_once(PRE_PATH.'php/classes/class.news.php');
	   
            $html .= $this->GenerateNews();
	    }
		elseif($blok['typ']=='anketa'){
         
		     if(!class_exists('C_Inquiry'))
			    include_once(PRE_PATH.'php/classes/class.inquiry.php');
			$html .= $this->GenerateInquiry($blok['id']);
			}	
		
        elseif($blok['typ']=='kalendar-akci'){
		     if(!class_exists('C_EventCalendar'))
			    include_once(PRE_PATH.'php/classes/class.event.calendar.php');
                
            include_once(PRE_PATH.'php/classes/class.calendar.php');
			$html .= $this->GenerateEventCalendar();
			}
		
		elseif($blok['typ']=='formular'){
		     if(!class_exists('C_Form'))
			    include_once(PRE_PATH.'php/classes/class.form.php');
			$html .= $this->GenerateForm($blok['id']);
			}
		
		elseif($blok['typ']=='galerie'){

		     if(!class_exists('C_Gallery'))
			    include_once(PRE_PATH.'php/classes/class.gallery.php');
			$html .= $this->GenerateGallery();
			}
		
		elseif($blok['typ']=='editbox'){
		     if(!class_exists('C_Editbox'))
			    include_once(PRE_PATH.'php/classes/class.editbox.php');
			$html .= $this->GenerateEditbox($blok['id']);
			}
        
		elseif($blok['typ']=='vyhledavani'){
		     if(!class_exists('C_Finder'))
			    include_once(PRE_PATH.'php/classes/class.finder.php');
			$html .= $this->GenerateFinder();
			}
	    elseif($blok['typ']=='facebook'){
	        if(!class_exists('C_Facebook'))
			    include_once(PRE_PATH.'php/classes/class.facebook.php');
			$html .= $this->GenerateFacebook($blok['id']);
			}
        elseif($blok['typ']=='archiv-clanku'){
	          if(!class_exists('C_ArticlesArchive'))
			    include_once(PRE_PATH.'php/classes/class.articles.archive.php');
			$html .= $this->GenerateArticlesArchive();
			}
        elseif(in_array($blok['typ'], array('nahodne-clanky','nejnovejsi-clanky','nejlepsi-clanky','top-clanky','nejdiskutovanejsi-clanky'))){
	          if(!class_exists('C_Articles'))
			    include_once(PRE_PATH.'php/classes/class.articles.php');
			$html .= $this->GenerateArticles($blok['typ']);
			}
        elseif($blok['typ']=='diskuze'){
			$html .= $this->GenerateDiskuzniPrispevky();
			}
        elseif($blok['typ']=='login'){
			$html .= $this->GenerateLoginForm();
			}
        elseif($blok['typ']=='newsletter'){
			$html .= $this->GenerateNewsletterForm();
			}
        elseif($blok['typ']=='rss'){
            if(!class_exists('C_Rss'))
			    include_once(PRE_PATH.'php/classes/class.rss.php');
			$html .= $this->GenerateRss();
			}
            
        elseif($blok['typ'] == 'facebook-likebox'){
            $html .= "<div class='box likebox'>";
            $html .= "<h3>Najdete nás na facebooku</h3>";
            $html .= '<div class="fb-like-box" data-href="https://www.facebook.com/FacebookDevelopers" data-width="253" data-colorscheme="dark" data-show-faces="true" data-header="false" data-stream="false" data-show-border="false"></div>';
            $html .= '</div>';
        }
        
        return $html;
        
    }
    
    
	public function GetPanel(){
		
		$html = "";

		foreach($this->items AS $blok){
				
			//$html .= '<div class="editbox">';
			$html .= $this->GetBox($blok);
            //$html .= '</div>';
        }

        
		return $html;	
	
	}
    
    public function getItems()
    {
        return $this->items;
    }
    
	
    
    private function GenerateRss()
    {
        $html = "";
		
		$rss = new C_Rss('panel');
		if(!$rss->exists) return $html;
		$box = $rss->GetBox();
		
		if($box=='') return $html;
		
		$html .= $this->GenerateBlock(TSLEDUJEME_PRO_VAS_SIT, $box, 'rss');
		
		return $html;

    }
    
    
    private function GenerateDiskuzniPrispevky()
    {
        global $domain;
        global $links;
        global $db;
        
        $data = $db->query("SELECT p.jmeno, DATE_FORMAT(p.datum, '%d.%m.%Y %H:%i') AS datum, p.idStranky, p.text, s.nazevProMenu AS nazev 
            FROM ".TABLE_DISKUZE_POLOZKY." AS p
            LEFT JOIN ".TABLE_STRANKY." AS s ON p.idStranky=s.idStranky
            WHERE s.zobrazit=1
                AND p.zobrazit=1
                AND s.idDomeny=".DOMAIN_ID."
                AND s.idJazyka=".WEB_LANG_ID."
            ORDER BY p.datum DESC
            LIMIT ".POCET_POSLEDNICH_KOMENTARU_V_BOXU."
            ");
        
        $html = "<div class='box'>";
        $html .= "<h3>".TPOSLEDNI_KOMENTARE."</h3>";
        $html .= "<div class='posledni_komentare'>";
        
        $html .= "<table>";
        while($d = $db->getAssoc($data))
        {
            $html .= "<tr>";
            $html .= '<th><a class="icon_chat" href="'.$links->get_url($d['idStranky']).'#diskuze"></a></th>';
            $html .= '<td><span>'.$d['datum'].'&nbsp; '.$d['jmeno'].' '.TOKOMENTOVAL_CLANEK.'</span> <a href="'.$links->get_url($d['idStranky']).'#diskuze">'.secureString($d['nazev']).'</a></td>';

        }
        
        $html .= "</table>";
        $html .= "</div>";
        $html .= "</div>";
        
        return $html;
    }
    
    
    private function GenerateLoginForm()
    {
        global $links;
        global $private_pages;
        
        $url_login = $links->get_url('login');
        $url_registrace = $links->get_url(ID_STRANKA_REGISTRACE);
        
        $html = "<span id='formLogin'></span>";

        if($private_pages->is_logged())
            $html .= '<h4>'.TPRIHLASENY_UZIVATEL.'</h4>
            
                <strong>'.$private_pages->get_user_name().'</strong>
                <a href="'.$url_login.'?change_password#formLogin" class="tlacitko">'.TZMENIT_HESLO.'</a>
                <a href="'.$url_login.'?logout#formLogin" class="tlacitko">'.TODHLASIT_SE.'</a>
                
                
                ';
            else
            {
            $html .= '<h4>'.TPRIHLASENI_UZIVATELE.'</h4>
                            <form method="post" action="'.$url_login.'#formLogin">
                                <div>
                                <input type="text" value="" placeholder="'.TPRIHLASOVACI_JMENO.'" class="input" name="username" />
                                <input type="password" value="" placeholder="'.THESLO.'" class="input" name="password" />
                                </div>
                                <div class="fr">';
            //$html .= '<a href="'.$url_login.'?forgot_password">'.TZAPOMNEL_JSEM_HESLO.'</a><br />';'
            
            $html .= '<a href="'.$url_registrace.'" class="tlacitko fr">'.TREGISTRACE_UZIVATELE.'</a>
                                </div>
                                <div class="fl">
                                    <input type="submit" name="btnLogin" value="'.TPRIHLASIT_SE.'" class="tlacitko" />
                                </div>
                                <div class="cleaner"></div>
                            </form>';
             }               
            /*         
            $html .= '<form method="post" action="'.$url_login.'">
                <label for="box_username">'.TPRIHLASOVACI_JMENO.'</label>
                <input type="text" value="" name="username" id="box_username"/>
                <label for="box_password">'.THESLO.'</label>
                <input type="password" value="" name="password" id="box_password"/>
                <input type="submit" name="btnLogin" value="'.TPRIHLASIT_SE.'"/>
                <br />
                <a href="'.$url_login.'?forgot_password">'.TZAPOMNEL_JSEM_HESLO.'</a>
                </form>';
            */            
        
        $html = $this->GenerateBlock(TLOGIN, $html, 'login');
        
        return $html;
    }
    
    private function GenerateNewsletterForm()
    {
        global $links;
        global $userSystemMessage;
        global $db;
        global $domain;
        
        //$url_email = $links->getUrlByType('email');
        $url_email = "#newsletter";
        $html = "";
        //$html .= '<h4>'.TODBER_NOVINEK.'</h4>';
        //$html .= TEXT_PRO_BOX_ODBER_NOVINEK;
        
        
        $value = is_post('btnOdberNovinek') ? get_secure_post('kontakt') : "";
        /*
        $html .= '<form method="post" action="'.$url_email.'">
            <div>
            <input type="text" name="kontakt" value="'.$value.'" placeholder="'.TVAS_EMAIL.'" class="input fl"/><input type="submit" value="'.TPRIHLASIT_K_ODBERU_NOVINEK.'" class="tlacitko fr" name="btnOdberNovinek" id="btnOdberNovinek"/>
            <input type="text" name="email" class="nodisplay" value="@"/>
            </div>
            <div class="cleaner"></div>
            </form>';
        */
        
        //$html .= '<h4>'.TODBER_NOVINEK.'</h4>';
        $html .= TEXT_PRO_BOX_ODBER_NOVINEK;
        $html .= $userSystemMessage->show_messages(true, "odber-novinek");
        
        $html .= '<form action="'.$url_email.'" method="post">
        <input type="text" name="kontakt" id="newsletterMail" class="cleaningText" placeholder="'.TVAS_EMAIL.'" value="'.$value.'" /><input type="text" name="email" class="nodisplay" value="@"/><input type="submit" class="btn" value="'.TODESLAT.'" name="btnOdberNovinek" title="'.TPRIHLASIT_K_ODBERU_NOVINEK.'"/>
        <p>
          <input type="checkbox" id="souhlasim" name="souhlas" value="1">
          <label for="souhlasim">'.TSOUHLASIM_SE_ZPRACOVANIM_OSOBNICH_UDAJU.'</label>
          (<a href="'.$links->get_url(ID_ZPRACOVANI_OSOBNICH_UDAJU).'" target="_blank">'.TPRECIST.'</a>)
        </p>
      </form>';
        
        
        /*    
        if(ZOBRAZIT_RSS == 1)
            $html .= "<a href='".$domain->getUrl()."rss-".LANG.".xml' class='rss'>".TODEBIRAT_NOVINKY_PRES_RSS."</a>";
          */
            
        /*
        $data = $db->query("SELECT idSkupiny, nazev FROM ".TABLE_EMAILY_SKUPINY." WHERE verejna=1 AND idDomeny=".$domain->getId());
        
        while($s = $db->getObject($data))
        {
            $html .= '<input type="checkbox" name="kategorie[]" value="'.$s->idSkupiny.'" id="sk'.$s->idSkupiny.'"/>&nbsp;&nbsp;<label for="sk'.$s->idSkupiny.'" checked="checked">'.$s->nazev.'</label><br />';
        }    
        
        */
        /*
        $html .= '<div class="textright">
        <input type="text" name="email" class="nodisplay" value="@"/>
        <input type="submit" name="btnOdberNovinek" value="" id="btnSubscribeNewsletter" title="'.TPRIHLASIT_K_ODBERU_NOVINEK.'" />
        </div>
    </form>';
        */
        
        $html = $this->GenerateBlock("", $html, 'odberNovinek', 'odberNovinek');
        
        return $html;
        
        
        
        
        
    }
    
    private function GenerateEventCalendar()
    {
        $html = "";
		
		$eventCalendar = new C_EventCalendar('box');
		//if(!$eventCalendar->hasEvents()) return $html;

        $html = $eventCalendar->get_html();
        
		$html = $this->GenerateBlock(TKALENDAR_AKCI, $html, 'kalendar-akci');
		
		return $html;
    }
    
    private function GenerateArticles($typ){
        $html = "";
		
		$articles = new C_Articles(0,$typ);
		if(!$articles->exists) return $html;

        $clanky = $articles->getArticlesBlock();  
		if($clanky=='') return $html;
		
		$html .= $this->GenerateBlock($articles->get_name(), $clanky, 'nejblok');
		
		return $html;
    }
    
    private function GenerateArticlesArchive(){
        $html = "";
		
		$articleArchive = new C_ArticlesArchive('panel');
		if(!$articleArchive->exists) return $html;

		$archive = $articleArchive->getArticlesArchiveBlock();
		
		if($archive=='') return $html;
		
		$html .= $this->GenerateBlock($articleArchive->setting['nazev'], $archive, 'articles-archive');
		
		return $html;
    }
    
    private function GenerateFacebook($idPluginu){
        $html = "";
        $fb = new C_Facebook($idPluginu);
        if(!$fb->exists) return $html;
        
        $fb_data = $fb->getFacebook();
        $html .= "<p id='facebook'>";
        $html .= $this->GenerateBlock($fb->setting['nazev'],$fb_data,'facebook');
        $html .= "</p>";
        return $html;
        }

	
	private function GenerateFinder(){
		$html = "";
		
		$finder = new C_Finder();
		if(!$finder->exists) return $html;
		$findform = $finder->GetFindForm();
		
		if($findform=='') return $html;
		
		$html .= $this->GenerateBlock($finder->name, $findform, 'finder');
		
		return $html;
		
		
	}
	
	private function GenerateEditbox($idBloku){
		$html = "";
		
		$box = new C_Editbox($idBloku);
        
        $box->allow_javascript($this->allow_javascript);
            
        
		if(!$box->exists) return $html;
		$editbox = $box->GetEditbox();
		
		if($editbox=='') return $html;
		
		$html .= $this->GenerateBlock($box->editbox['nazev'], $editbox, 'editbox');
		
		return $html;
		
		
		
	}
	
	private function GenerateInquiry($idBloku){
		$html = "";
		
		$inq = new C_Inquiry($idBloku);
		if(!$inq->exists) 
            return $html;
            
		$inquiry = $inq->get_inquiry();
		
		if($inquiry=='') 
            return $html;
		
		$html .= $this->GenerateBlock($inq->get_name(), $inquiry, 'inquiry');
		
		return $html;
			
	}
	
	private function GenerateGallery(){
		$html = "";
		$gal = new C_Gallery(0,'panel');
       
		if(!$gal->exists) return $html;
		
		$gallery = $gal->GetGalleryBlock();
		if($gallery=='') return $html;
		
		$html .= $this->GenerateBlock(TGALERIE, $gallery, 'gallery');

		return $html;
	
	}
	
	//generuje blok s odkazy
	private function GenerateLinks($idBloku){
		$html = "";		
		$idBloku = intval($idBloku);
		if($idBloku==0) return $html;
		
		$m = new C_Menu($idBloku,0, '',true, true);
		if(!$m->exists) return $html;

		$html .= $this->GenerateBlock($m->nazev, $m->GetMenu(), 'links');
		
		return $html;
	}
	
	private function GenerateForm($idBloku){
		$html = "";
		
		$f = new C_Form($idBloku);
		if(!$f->exists) return $html;
				
		$html .= $this->GenerateBlock($f->form['nazev'], $f->GetForm(), 'form');
		
		return $html;
	
	}
	
	private function GenerateNews(){
		$html = "";
		
		$news = new C_News('panel');
		if(!$news->exists) return $html;
			
		$novinky = $news->GetNewsBlock();
		if($novinky=='') return $html;
	
		$html .= $this->GenerateBlock(TNOVINKY, $novinky, 'news');
	
		return $html;
		}
	
	
    //vygeneruje box
	private function GenerateBlock($name, $content, $className, $id = "")
    {
	    $id = $id != '' ? $id = 'id="'.$id.'"' : "";   
       
        $html = "";
		//$html = '<div class="box-obal '.$className.'" '.$id.'>';
        
        /*
        if($this->show_name)
            $html .=  "<div class='nazev'>".$name."</div>";
        */
            //$html .=  "<".$this->name_tag.">".$name."</".$this->name_tag.">";
            
        $html .= '<div class="box">';       
        $html .= '<div class="box-in">'; 
        
        
		$html .= $content;
        $html .= "</div>";
        $html .= "</div>";

		return $html;
		
	}
    
    
    
	
	
	
	
	
	
	
	
	
	
	
}


?>