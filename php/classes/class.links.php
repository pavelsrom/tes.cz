<?php

/**
 * @author Pavel Srom
 * @copyright 2010
 */

class C_Links{
	
	//pole obsahujici informace pro tvorbu odkazu - id, url name, name 
	public $data;
    
	
	public function __construct($primary_key = "idStranky"){
		global $db;
		global $domain;	
        
        if(!isset($_SESSION['idUzivatele']))
            $cond = "AND zobrazit=1";
            else
            $cond = "";
        
		$data_pages = $db->Query("SELECT idStranky, idRodice, hloubka, s.nazev, nadpis,nazevProMenu,url,typ,doMenu, s.idJazyka, obrazek, j.jazyk, zobrazit, autentizace
            FROM ".TABLE_STRANKY." AS s
            LEFT JOIN ".TABLE_JAZYKY." AS j ON s.idJazyka = s.idJazyka
            WHERE idDomeny=".$domain->getId()."
                ".$cond."  
            ORDER BY priorita");
		
		while($p = $db->getAssoc($data_pages)){
			$this->data[$p[$primary_key]] = array(
                'id_stranky'=>$p['idStranky'],
                'nazev'=>$p['nazev'],
                'jazyk'=>$p['jazyk'], 
                'url_nazev'=>$p['url'],
                'url'=>($p['typ']=='uvodni'?$domain->getRelativeUrl():UrlPage($p['url'],$p['idStranky'],$p['jazyk'])),
                'menu_nazev'=>$p['nazevProMenu'],
                'do_menu'=>$p['doMenu'],
                'rodic'=>$p['idRodice'],
                'hloubka'=>$p['hloubka'],
                'nadpis'=>$p['nadpis'],
                'typ'=>$p['typ'],
                'idJazyka'=>$p['idJazyka'],
                'foto'  => $p['obrazek'],
                'aktivni' => $p['zobrazit'],
                'autentizace' => $p['autentizace']
                );
            
           
            }
            
        
            
	}
    
    
    //zjisti zda stranka s danym id ma potomky
    public function haveChildren($idPage, $povoleni_potomci = null){
        
        global $db;
        
        $povoleni_potomci = (array)$povoleni_potomci;
        
        if($povoleni_potomci != null && is_array($povoleni_potomci) && count($povoleni_potomci)>0)
            foreach($this->data AS $s)
            {
                if(!in_array($s['typ'], $povoleni_potomci)) 
                    continue;
                   
                //echo $s['rodic']."-".$idPage."<br>"; 
                if($s['rodic']==$idPage) 
                    return true;    
            }
            
        return false;
  
    }
    
    //zjisti zda bude stranka s danym id zobrazena v menu
    public function isShowInMenu($idPage){
        
        global $db;
                
        foreach($this->data AS $id => $s)
        {
            if($s['id_stranky']==$idPage && $s['do_menu'] == 0) 
                return false;
        }
        
        if($this->data[$idPage]['rodic'] > 0)
            return $this->isShowInMenu( $this->data[$idPage]['rodic'] );
            else
            return (bool) $this->data[$idPage]['do_menu'];
    
        
    }
    
    
    public function getIdByType($type, $id_jazyk=0)
    {
        foreach($this->data AS $id => $s)
            if($s['typ'] == $type && ($id_jazyk == 0 || $id_jazyk == $s['jazyk']))
                return $id;
        
        return 0;
    }
    
    public function getUrlByType($type, $id_jazyk=0)
    {
        foreach($this->data AS $id => $s)
            if($s['typ'] == $type && ($id_jazyk == LANG || $id_jazyk == $s['jazyk']))
                return $s['url'];
        
        return 0;
    }
    
    public function getIdHomePage()
    {
        foreach($this->data AS $id => $s)
            if($s['typ'] == 'uvodni')
                return $id;
        
        return 0;
    }
    
    
    public function getUrlHomePage()
    {
        foreach($this->data AS $id => $s)
            if($s['typ'] == 'uvodni')
                return $s['url_nazev'];
        
        return 0;
    }
    
    //vraci url stranky podle id
    public function getUrl($id)
    {
        return $this->get($id,'url');
    }
    
    //vraci url stranky podle id
    public function getType($id)
    {
        return $this->get($id,'typ');
    }
    
    //vraci parametr stranky podle klice
    public function get($id,$key)
    {
        return isset($this->data[$id][$key]) ? $this->data[$id][$key] : false;
    }
    
    //zjisti zda stranka s danym id ma nejake podstranky
    public function hasSubpage($id, $disallow_page_type)
    {
        foreach($this->data AS $s)
            if($s['rodic'] == $id && !in_array($s['typ'], $disallow_page_type))
                return true;
                
        return false;
    }
    
    //vraci id chybove stranky
    public function getId404()
    {
        foreach($this->data AS $s)
            if($s['typ'] == '404')
                return $s['id_stranky'];
                
        return 0;
    }
	
	
	
	
	
}

?>