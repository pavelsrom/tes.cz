<?php

/**
 * @author Pavel �rom
 * @copyright 2018
 */

class C_Provize
{
    //id uzivatele, ktery provedl platbu platebni branou
    //private $id_uzivatel;
    
    //id uzivatele, kteremu se budou pripisovat nejvetsi provize
    private $id_uzivatel_ref = 0; 
    
    //pocet cenovych urovni provizniho systemu
    private $pocet_urovni_provizi = POCET_UROVNI_PROVIZNIHO_SYSTEMU;
    
    private $urovne = array(
    
        //mesicni predplatne
        0 => array(
            1 => PROVIZE_MESICNI_1,
            2 => PROVIZE_MESICNI_2,
            3 => PROVIZE_MESICNI_3
            ),
            
        //rocni predplatne
        1 => array(
            1 => PROVIZE_ROCNI_1,
            2 => PROVIZE_ROCNI_2,
            3 => PROVIZE_ROCNI_3
            )
        );
    
    //id prihlaseneho nebo nove registrovaneho uzivatele
    private $user_id = 0;
    
    
    public function __construct($user_id, $nastav_referencni_kod = true, $pocet_urovni_provizi = POCET_UROVNI_PROVIZNIHO_SYSTEMU)
    {
        $this->user_id = $user_id;
        $this->pocet_urovni_provizi = $pocet_urovni_provizi;
        
        if($nastav_referencni_kod)
            $this->nastav_referencni_kod();
        
    }
    
    private function nastav_referencni_kod()
    {
        global $db;
        global $private_pages;
        
        
        if($private_pages->get_user_id() > 0)
        {
            $this->id_uzivatel_ref = $private_pages->get_user_id_ref();
        }
        elseif(isset($_COOKIE['ref']) && $_COOKIE['ref'] != '')
        {
            $this->id_uzivatel_ref = $db->get(TABLE_UZIVATELE,"idUzivatele","refkod='".$db->secureString($_COOKIE['ref'])."' AND aktivni=1");
            
        }
        elseif(is_get("ref"))
        {
            
            $this->id_uzivatel_ref = $db->get(TABLE_UZIVATELE,"idUzivatele","refkod='".$db->secureString(get_request("ref"))."' AND aktivni=1");
            
            if($this->id_uzivatel_ref > 0) 
                setcookie('ref', get_request("ref"),  time()+60*60*24*30); //platnost cookies nastavena na 30 dni
        }
        
        
        
        
        
    }
    
    public function get_user_id_ref()
    {
        return $this->id_uzivatel_ref;
    }
    
    
    //pokud provizni vazba jeste neexistuje, pak vytvori provizni vazbu - ale jen v pripade ze je znamy ref id a id uzivatele
    /*
    public function vytvor_vazbu($id_uzivatel)
    {
        global $db;
        
        $id_vazby = false;
        if($id_uzivatel > 0 && $this->id_uzivatel_ref > 0)
        {
            $id_vazby = $db->get(TABLE_UZIVATELE,"id","id_uzivatel=".$id_uzivatel." AND id_uzivatel_ref=".$this->id_uzivatel_ref);
            
            if($id_vazby == 0)
            {
                $db->insert(TABLE_UZIVATELE, array("id_uzivatel" => $id_uzivatel, "id_uzivatel_ref" => $this->id_uzivatel_ref));
                $id_vazby = $db->lastId();
            }
        }
        
        return $id_vazby;
    }
    */
    
    
    //na zaklade platby uzivatele s id uzivatel, pripise provize vsem refererum 
    public function pripis_provize($id_uzivatel, $id_objednavka)
    {
        
        global $db;
        
        //pokud je pocet urovni provizi nastaven na 0, provice se nepocitaji
        if($this->pocet_urovni_provizi == 0)
        {
            return false;
        }
        
        
        //overeni zda uz nebyla provize pripsana - podle existence id_objednavka
        $id = $db->get(TABLE_PROVIZE,'id','id_objednavka='.$id_objednavka);
        if($id > 0)
        {
            return false;
        }
        
        $typ_predplatneho = $db->get(TABLE_UZIVATELE,"predplatne_typ","idUzivatele=".$id_uzivatel);
        
        
        //pripisovani provizi
        for($i = 1; $i <= $this->pocet_urovni_provizi; $i++)
        {
        
            if($id_uzivatel == 0)
                break;
        
            $data = $db->query("SELECT u.predplatne_typ, u.id_uzivatel_ref, r.aktivni, r.email
                FROM ".TABLE_UZIVATELE." AS u
                LEFT JOIN ".TABLE_UZIVATELE." AS r ON u.id_uzivatel_ref = r.idUzivatele
                WHERE u.idUzivatele=".$id_uzivatel."
                GROUP BY u.idUzivatele
                LIMIT 1
                ");
            
            $u = $db->getObject($data);
        
            if($u->id_uzivatel_ref == 0)
                break;
        
            //pokud existuje id referera a navic je referen aktivni, tak mu je pripsana provize
            $aktivni  = $u->aktivni == 1;
            if($u->id_uzivatel_ref > 0 && $aktivni)
            {
                //zjisteni castky provize
                //$vyse_provize = isset($this->urovne[$u->predplatne_typ][$i]) ? $this->urovne[$u->predplatne_typ][$i] : 0;
                $vyse_provize = isset($this->urovne[$typ_predplatneho][$i]) ? $this->urovne[$typ_predplatneho][$i] : 0;
        
        
                //zapise provizi danemu uzivateli
                //$this->zapis_provizi($u->id_uzivatel_ref, $vyse_provize, 0, $u->predplatne_typ, $i, $id_objednavka);
                $this->zapis_provizi($u->id_uzivatel_ref, $vyse_provize, 0, $typ_predplatneho, $i, $id_objednavka);
                
                //odesle email s informaci o nove provizi na proviznim uctu
                $this->odeslat_upozorneni($u->email, $vyse_provize);
                
                
            }
            
            $id_uzivatel = $u->id_uzivatel_ref;
  
        }
        
        return true;
  
    }
    
    
    private function je_uzivatel_aktivni($id_uzivatel)
    {
        
    }
    
    //zapise konkretni provizi danemu uzivateli
    public function zapis_provizi($id_uzivatel_ref, $vyse_provize, $aktivni = 0, $typ_predplatneho = null, $uroven_provize = 0, $id_objednavka = "")
    {
        global $db;
        
        
        if($vyse_provize == 0)
            return false;
        
        $insert = array(
            "id_uzivatel" => $id_uzivatel_ref,
            "castka" => $vyse_provize,
            "stupen" => $uroven_provize,
            "id_objednavka" => $id_objednavka,
            "datum" => "now",
            "aktivni" => $aktivni,
            "predplatne_typ" => $typ_predplatneho === null ? "null" : $typ_predplatneho
            );
            
        $db->insert(TABLE_PROVIZE, $insert);
            
        return $db->lastId();
            
    }
    
    
    //odesle email s upozorneni o pripsani nove provize
    public function odeslat_upozorneni($email, $castka)
    {
        global $domain;
        
        $predmet = PREDMET_EMAILU_PROVIZE_PRIPSANA;
        $zprava = TEXT_EMAILU_PROVIZE_PRIPSANA;   
        $zprava = str_replace("[castka]",price($castka), $zprava);           
    
        //nacteni html sablony
        $sablona = file_get_contents(PRE_PATH.$domain->getDir()."email.html");
        $zprava = str_replace("{obsah}",$zprava,$sablona);

        //odeslani emailu
        $odeslano = SendMail(SMTP_LOGIN_3, NAZEV_WEBU, $email, $predmet, $zprava,'',null, false, 3);
    }
    
    
    
    //zalozi zadost o provizi a vrati id zadosti
    //vstupni parametr je cislo bankovniho uctu uzivatele
    public function vytvor_zadost($ucet_uzivatele)
    {
        global $db;
        global $private_pages;
        
        $celkem = $this->celkova_vyse_neproplacene_provize();
        
        if($celkem > 0 && MIN_CASTKA_VYPLACENI_PROVIZE <= $celkem)
        {
            $insert = array(
                "id_uzivatel" => $this->user_id,
                "jmeno_uzivatele" => $private_pages->get_user_name(),
                "celkem" => $celkem,
                "datum" => "now",
                "id_stav" => 1, //ceka na vyrizeni,
                "ucet_uzivatele" => $ucet_uzivatele
                );
                
            $db->insert(TABLE_PROVIZE_ZADOSTI, $insert);
         
            $id_zadosti = $db->lastId();
            
            //id zadosti se priradi vsem polozkam jednotlivych provizi
            $db->update(TABLE_PROVIZE, array("id_zadosti" => $id_zadosti), "id_uzivatel = ".$this->user_id." AND (id_zadosti = 0 OR id_zadosti IS NULL) AND aktivni=1");
               
            return $id_zadosti;
        }
        
        
        return false;
        
        
    }
    
    //zjisti celkovou vysi aktualnich provizi
    public function celkova_vyse_neproplacene_provize()
    {
        global $db;
        
        $celkem = $db->get(TABLE_PROVIZE,"SUM(castka)","id_uzivatel=".$this->user_id." AND (id_zadosti = 0 OR id_zadosti IS NULL) AND aktivni=1");
        
        return $celkem;
        
    }
    
    public function celkova_vyse_proplacenych_zadosti()
    {
        global $db;
        
        $celkem = $db->get(TABLE_PROVIZE_ZADOSTI,"SUM(celkem)","id_uzivatel=".$this->user_id." AND id_stav=2");
        
        return $celkem;
    }
    
    public function celkova_vyse_neproplacenych_zadosti()
    {
        global $db;
        
        $celkem = $db->get(TABLE_PROVIZE_ZADOSTI,"SUM(celkem)","id_uzivatel=".$this->user_id." AND (id_stav=1 OR id_stav=3)");
        
        return $celkem;
    }
    
    
    //vraci data provizi k dane zadosti
    public function get_data($id_zadost = 0)
    {
        global $db;
        
        $where = $id_zadost == 0 ? "(id_zadosti = 0 OR id_zadosti IS NULL)" : "id_zadosti = ".$id_zadost;
        
        $data = $db->query("SELECT id,DATE_FORMAT(p.datum, '%d.%m.%Y') AS datum, castka, aktivni
            FROM ".TABLE_PROVIZE." AS p
            WHERE id_uzivatel=".$this->user_id."
                AND ".$where."
            ORDER BY p.datum DESC
            ");
        
        return $data;
        
        
    }
    
    
    
    
}




?>