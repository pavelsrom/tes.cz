<?php

/**
 * @author Pavel Srom
 * @copyright 2010
 */



class C_News{

	
	//obsahuje polozky bloku novinky
	public $items;
	
	//indikuje existenci galerie s danym id
	public $exists;
	
	public function __construct($typ = 'stranka', $idNovinky=0){

		global $domain;	
		global $db;
		global $links;
        global $private_pages;
        global $pagination;
        
		$this->items = array();
		$this->exists = false;
       
		
        
        if($typ == 'stranka')
            $limit = ($pagination->get_current_page()-1) * intval(POCET_NOVINEK_NA_STRANKU).",".intval(POCET_NOVINEK_NA_STRANKU);
        elseif($typ == 'panel')
            $limit = intval(POCET_NOVINEK_V_BOXU);
        else return;
        
        
        if($typ=='novinka' && $idNovinky>0)
        {
            $data = $db->Query("SELECT idStranky, nazev AS titulek, perex, idStranky AS odkaz_novinky, obrazek, 
                DATE_FORMAT(datum,'%e. %c. %Y') AS datum, DATE_FORMAT(datum, '%h:%i') AS cas
    			FROM ".TABLE_STRANKY." 
    			WHERE idDomeny=".$domain->getId()."
                    AND idJazyka=".WEB_LANG_ID."
                    AND typ='novinka'
    				AND zobrazit=1
    				AND (od <= CURDATE() OR od IS NULL)
    				AND (do >= CURDATE() OR do IS NULL)
                    AND idStranky=".$idNovinky."
                LIMIT 1
    		  ");
              
              $pagination->set_number_items( $db->numRows($data) );
              
        }
        else
        {
                
            //zjisteni privatnich stranek
            $where = "";
            if(MODULE_PRIVATE_PAGES)
            {

                $d = $db->query("SELECT idStranky AS id
                    FROM ".TABLE_STRANKY." AS s
                    WHERE zobrazit=1 
                        AND typ='novinka' 
                        AND s.idDomeny=".$domain->getId()."
                        AND s.idJazyka=".WEB_LANG_ID."
                        AND (s.od <= CURDATE() OR s.od IS NULL)
    				    AND (s.do >= CURDATE() OR s.do IS NULL)
                        ");
                        
                $zakazane_stranky_id = array();
                while($s = $db->getObject($d))
                    if(!$private_pages->has_access($s->id))
                        $zakazane_stranky_id[] = $s->id; 
                
                $db->free($d);
                        
                $where = count($zakazane_stranky_id) > 0 ? " AND idStranky NOT IN (".implode(",",$zakazane_stranky_id).")" : ""; 
                
                        
            }    
            
                
                
    		$data = $db->Query("SELECT SQL_CALC_FOUND_ROWS idStranky, nazev AS titulek, perex, idStranky AS odkaz_novinky, obrazek, IF(TRIM(s.obsah) = '', 0, 1) AS zobrazit_odkaz,
                DATE_FORMAT(datum,'%e. %c. %Y') AS datum, DATE_FORMAT(datum, '%h:%i') AS cas, obsah
    			FROM ".TABLE_STRANKY." AS s
    			WHERE idDomeny=".$domain->getId()."
                    AND idJazyka=".WEB_LANG_ID."
                    AND typ='novinka'
    				AND zobrazit=1
    				AND (od <= CURDATE() OR od IS NULL)
    				AND (do >= CURDATE() OR do IS NULL)
                    ".$where."
    			ORDER BY s.datum DESC
                LIMIT ".$limit."
    		  ");
             $celkem_data = $db->Query("SELECT FOUND_ROWS()");
            $celkem_arr = $db->getRow($celkem_data);

            $pagination->set_number_items($celkem_arr[0]);
             }	
		
        
		
		
		if($db->numRows($data)==0) return;

		$this->exists = true;

        $i=0;
		while($news = $db->getAssoc($data)){
			if($news['obrazek']!= '' && file_exists(PRE_PATH.$domain->getDir().USER_DIRNAME_NEWS.$news['obrazek'] )){
				$img = RELATIVE_PATH.$domain->getDir().USER_DIRNAME_NEWS.$news['obrazek'];
				}
				else 
                $img = '';
		
					
						
			$link = $links->get_url($news['odkaz_novinky']);
								
			$linkname = $db->get(TABLE_STRANKY,'nazev','idStranky='.$news['odkaz_novinky']);
		
						
				$this->items[$news['idStranky']] = array(
                    'obsah' => $news['obsah'],
					'poradi' => $i,
					'titulek' => $news['titulek'],
					'perex' => $news['perex'],
					'obrazek' => $img,
					'odkaz' => $link,
					'nazev_odkazu'=> $linkname,
					'datum' => $news['datum'],
					'cas' => $news['cas'],
                    'zobrazit_odkaz' => $news['zobrazit_odkaz']
					);
                $i++;
				}

		$db->free($data);
		
	}
	
	public function GetNewsOnPage(){
	   
       global $domain;
       global $pagination;
       
		$html = "";
		
		$pocet = intval(POCET_NOVINEK_NA_STRANKU);
        $pagination->set_number_items_on_page($pocet);
        
		$stranka = $pagination->get_current_page();
		$od = ($stranka-1) * $pocet;
		
		
		$html .= "<span id='news'></span>";

		$i = 0;
        $html .= '<div class="produktList">';
        
		foreach($this->items AS $id => $pol)
        {

			//if($pol['poradi']<$od) continue;
			//if($pocet==$i) break;
			
			$html .= ' <div class="produkt" id="i'.$id.'">';
            /*
            if($pol['obrazek']!='')
            {
                $html .= '<div class="obrazek">';
                if($pol['odkaz']!='')
					$html .= "<a href='".$pol['odkaz']."' title='".TCIST_VICE."'><img src='".$pol['obrazek']."' alt='".$pol['titulek']."'></a>";
					else
					$html .= "<img src='".$pol['obrazek']."' alt='".$pol['titulek']."'>";
                $html .= '</div>';
			}
            */
            
            $html .= '<div class="ingredienceObr">
                            <p>
                                <img src="'.$pol['obrazek'].'" alt="'.$pol['titulek'].'">
                            </p>
                        </div>';
            
            
            /*
            $html .= "<span class='datum'>".$pol['datum']."</span>";
            if($pol['odkaz']!='' && $pol['zobrazit_odkaz'])
				$html .= "<h2><a href='".$pol['odkaz']."' title='".TCIST_VICE."'>".$pol['titulek']."</a></h2>";
				else
				$html .= "<h2>".$pol['titulek']."</h2>";
            */
            
			$html .= '<div class="produktText">';
            $html .= "<h2>".$pol['titulek']."</h2>";
			$html .= $pol['obsah'];
            $html .= '</div>';
            
            /*
            if($pol['odkaz']!='' && $pol['zobrazit_odkaz'])
			    $html .= '<a class="button" href="'.$pol['odkaz'].'">'.TCIST_VICE.'</a>';
            */
			//$html .= "<div class='cleaner'></div>";
			$html .= "</div>";
			$i++;
        }
        
                        
                   
                                    

        
        
		
		//strankovani
		$html .= '</div>';
		
		$html .= $pagination->set_url_tail("#news")->get_html();
		
                
        if(ZOBRAZIT_RSS == 1)
        {
		  //$html .= "<div class='rssBox'>";
		  //$html .= "<a href='".$domain->getUrl()."rss-".LANG.".xml'>".TODEBIRAT_NOVINKY_PRES_RSS."</a>";
		  //$html .= "</div>";
        }
        
        
        
		return $html;
		
	}
    
    public function GetNewsOnStartPage(){
        global $domain;
        global $links;

		
		$html = '';
		$html .= "<div class='novinky'>";
        $html .= "<h2>".TNOVINKY."</h2>";
        
		$i = 0;
        $pocetUvod = intval(POCET_NA_UVODNI_STRANKU);
        
		foreach($this->items AS $id => $pol)
        {
			if($pocetUvod==$i) break;
            
			$html .= "<div class='novinka'>";
            
			/*
            if($pol['obrazek']!='')
            {
                $html .= '<div class="obrazek">';
				if($pol['odkaz']!='' && $pol['zobrazit_odkaz'])
					$html .= "<a href='".$pol['odkaz']."'><img src='".$pol['obrazek']."' alt='".$pol['titulek']."' /></a>";
					else
					$html .= "<img src='".$pol['obrazek']."' alt='".$pol['titulek']."' />";
                $html .= '</div>';
			}
            */
            
            $html .= '<span class="datum">'.$pol['datum'].'</span>';
            
            if($pol['odkaz']!='' && $pol['zobrazit_odkaz'])
				$html .= "<h3><a href='".$pol['odkaz']."' title='".TCIST_VICE."'>".$pol['titulek']."</a></h3>";
				else
				$html .= "<h3>".$pol['titulek']."</h3>";
			
			
			$html .= $pol['perex'];
				
			/*
			if($pol['odkaz']!='' && $pol['zobrazit_odkaz'])
	           $html .= '<a class="cist_vice" href="'.$pol['odkaz'].'">'.TCIST_VICE.'</a>';
            */
			//$html .= "<div class='cleaner'></div>";
            
			$html .= "</div>";
            
            $i++;
        }
		
        $odkaz_na_vsechny_novinky = $links->get_url('novinky');
        if($odkaz_na_vsechny_novinky!='' && $pocetUvod>0)
        {
            
            $html .= '<p><a class="button" href="'.$odkaz_na_vsechny_novinky.'"><span class="bg"></span>'.TZOBRAZIT_VSECHNY_NOVINKY.'</a></p>';
            //$html .= '<div class="cleaner"></div>';
        }
        
		$html .= "</div>";
        
        return $html;
        
    }
	
	
		
	public function GetNewsBlock(){
		global $domain;	
		global $links;
        
		$html = "";
		
		$pocet = intval(POCET_NOVINEK_V_BOXU);
		
		if(count($this->items) == 0) return '';
		
		if($pocet > count($this->items)) $pocet = count($this->items);
		
		
		
		if($pocet == 0) return '';
		
		$pom = $this->items;
		//ksort($pom);
		$s=0;
		$rand = array();
		foreach($pom AS $id => $p){
			$rand[] = $id;
			$s++;
			if($s>=$pocet) break;
			}
		

		
		if(!is_array($rand)) $rand = array($rand);
		
		//print_r($rand);
		$html .= '<h3>'.TNOVINKY.'</h3>';
        $html .= '<div class="novinky">';
        
		foreach($rand AS $id){
			$html .= '<div class="novinka">';
            if($this->items[$id]['odkaz']!='')
				$html .= "<h4><a href='".$this->items[$id]['odkaz']."' title='".TCIST_VICE."'>".secureString($this->items[$id]['titulek'])."</a></h4>";
				else
				$html .= "<h4>".secureString($this->items[$id]['titulek'])."</h4>";
            
            $html .= "<span class='datum'>".$this->items[$id]['datum']."</span>";
            $html .= '<p>'.strip_tags($this->items[$id]['perex']).'</p>';
            
            if($this->items[$id]['odkaz']!='')
                $html .= '<a href="'.$this->items[$id]['odkaz'].'" class="vice">'.TCIST_VICE.'</a>';
                
            /*
            $img = "";

            if($this->items[$id]['obrazek']!='')
            {
                
              if(strip_tags($this->items[$id]['perex']) == "") 
			     $html .= '<div class="novinka_stin">
                    <div class="novinka_foto">
                        <span class="image" style="background: url('.RELATIVE_PATH.$this->items[$id]['obrazek'].') center center no-repeat"></span>
                    </div>
                </div>';
                else
                $html .= '<div class="novinka_stin">
                    <div class="novinka_foto">
                        <a href="'.$this->items[$id]['odkaz'].'" class="image" style="background: url('.$this->items[$id]['obrazek'].') center center no-repeat" title="'.TCIST_VICE.'"></a>
                    </div>
                </div>';
                
		    }
            */

            $html .= '<div class="cleaner"></div>';
            $html .= '</div>';
			
            }

            $odkaz_na_vsechny_novinky = $links->get_url('novinky');
			if($odkaz_na_vsechny_novinky!='')
				$html .= "<a href='".$odkaz_na_vsechny_novinky."' class='tlacitko fr'>".TZOBRAZIT_VSECHNY_NOVINKY."</a>";
			
            $html .= '<div class="cleaner"></div>';
            $html .= '</div>';	

		return $html;
		
	}
    
    public function GetNews(){
        
        $html = "";
        $html .= "<div class='block-news'>";

		foreach($this->items AS $id => $pol){
		
			$html .= "<div class='news'>";
			
			$html .= "<div class='news-header'>";
			
			if($pol['datum']!='')
				$datum = $pol['datum'];
				else
				$datum = "";
            
			$html .= "<span class='news-date'>".$datum."</span>";
			
			$html .= "<span class='news-name'>";
			if($pol['odkaz']!='')
				$html .= "<a href='".$pol['odkaz']."'>".$pol['titulek']."</a>";
				else
				$html .= $pol['titulek'];
				
			$html .= "</span>";
			
			$html .= "</div>";
			
			$html .= "<div class='news-text'>";
			
			if($pol['obrazek']!='')
				if($pol['odkaz']!='')
					$html .= "<div class='news-image'><a href='".$pol['odkaz']."'><img src='".$pol['obrazek_stranka']."' alt='".$pol['titulek']."' /></a></div>";
					else
					$html .= "<div class='news-image'><img src='".$pol['obrazek']."' alt='".$pol['titulek']."' /></div>";
					
			$html .= $pol['perex'];
			$html .= "</div>";
			
			$html .= "<div>";
			$html .= "</div>";
			$html .= "<div class='cleaner'></div>";
			$html .= "</div>";
			$i++;
			}
		
		$html .= "</div>";
        
        return $html;
    }



}
?>