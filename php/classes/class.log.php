<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */


//trida na logovani akci
class LogItem
{
    
    //id uzivatele
    private $module_id;
    
    //id objektu
    private $object_id;
    
    //typ objektu stranka, clanek, novinka, ... 
    private $object_type;
    
    //nazev objektu - nazev stranky, nazev novinky, jmeno a prijmeni uzivatele atd.
    private $object_name;
    
    //id uzivatele
    private $user_id;
    
    //jmeno uzivatele - pro jistotu se uklada jak id tak nazev
    private $user_name;
    
    //id domeny
    private $domain_id;
    
    //typ akce - create, update, delete, import, export, send, login, logout 
    private $action_type;
    
    //jazyk spravovane domeny
    private $lang_id;
    
    //maximalni pocet uchovavanych logu
    private $max_logs;
    
    //interpretovany text
    private $text;    
    
    //datum logu
    private $date;
    
    //konstruktor tridy, vstupy id domeny, id jazyka a id a jmeno prihlaseneho uzivatele
    public function __construct($module_id, $domain_id, $lang_id, $user_id, $user_name)
    {
        $this->module_id = $module_id;
        $this->domain_id = $domain_id;
        $this->lang_id = $lang_id;
        $this->user_id = $user_id;
        $this->user_name = $user_name;
        $this->lang_id = WEB_LANG_ID;
        $this->max_logs = POCET_UCHOVAVANYCH_LOGU;
    }
    
    public function set($action_type, $object_type, $object_id=0, $object_name="",$text = "",$date="")
    {
        $this->action_type = $action_type;
        $this->object_type = $object_type;
        $this->object_id = $object_id;
        $this->object_name = $object_name;
        $this->text = $text;
        $this->date = $date;
    }
    
    public function set_module_id($module_id)
    {
        $this->module_id = $module_id;
    }
    
    
    //vunkce vycisti tabulku s logy, zachova povoleny pocet logu
    private function clean()
    {
        global $db;
        
        
        //otestovani zda v databazi neni prekrocen maximalni pocet logu
        $data = $db->query("SELECT idLogu AS id FROM ".TABLE_LOG." WHERE idDomeny=".$this->domain_id." ORDER BY idLogu DESC");
        
        //smazani prebytecnych zaloh
        $pocet_zaznamu = $db->numRows($data);
        if(($pocet_zaznamu+1) > $this->max_logs){
            $i=1;
            while($row = $db->getObject($data)){
                if($i < $this->max_logs) {
                    $i++;
                    continue;
                    }
                $db->query("DELETE FROM ".TABLE_LOG." WHERE idLogu=".$row->id." AND idDomeny=".$this->domain_id." LIMIT 1");
                $i++;
                }
            }
        
    }
    
    
    
    //prida zaznam do logu, vstupy typ akce, typ objektu, id a nazev objektu
    public function add_log($action_type, $object_type = "", $object_id = 0, $object_name = "")
    {
        global $db;              
        
        $this->set($action_type, $object_type, $object_id, $object_name);
        
        //if(!MODULE_LOG) return false;
        
        $this->clean();
        
        $insert = array(
            'idModulu'      => $this->module_id,
            'idDomeny'      => $this->domain_id,
            'idJazyka'      => $this->lang_id,
            'idUzivatele'   => $this->user_id,
            'jmenoUzivatele'=> $this->user_name,
            'idObjektu'     => $this->object_id,
            'typObjektu'    => $this->object_type,
            'nazevObjektu'  => $this->object_name,
            'akce'          => $this->action_type,
            'datum'         => 'now'  
            );


        $db->insert(TABLE_LOG, $insert);

    }
    

    
    public function get_action_type()
    {
        return $this->action_type;
    }
    
    public function get_object_type()
    {
        return $this->object_type;
    }
    
    public function get_object_id()
    {
        return $this->object_id;
    }
    
    public function get_object_name()
    {
        return $this->object_name;
    }
    
    public function get_user_name()
    {
        return $this->user_name;
    }
    
    public function get_text()
    {
        return $this->text;
    }
    
    public function get_date()
    {
        return $this->date;
    }
    
    
    
   
}


//trida pro generovani seznamu logu
class LogList
{
    
    private $domain_id;
    
    private $lang_id;
    
    private $user_id;
    
    private $object_id;
    
    private $object_type;
    
    private $logs = array(); 
    
    private $date_od = "";
    
    private $date_do = "";
    
    private $action_type = "";
    
    //povolene typy objektu
    private $allowed_object_types = array(
        'stranka'                   => TL_STRANKY,
        'clanek'                    => TL_CLANKU,
        'produkt'                   => TL_PRODUKTU,
        'novinka'                   => TL_NOVINKY,
        'galerie'                   => TL_GALERIE,
        'akce'                      => TL_AKCE,
        'kalendar-akci'             => TL_KALENDAR_AKCI,
        'kalendar-typ'              => TL_KALENDAR_AKCI_TYPY,
        'galerie-slozka'            => TL_TSLOZKY_GALERIE,
        'galerie-fotka'             => TL_TFOTKY_V_GALERII,
        'zakladni-nastaveni'        => TL_TUZIVATELE,
        'sablona-stranky'           => TL_SABLONY_STRANKY,
        'sablona-panelu'            => TL_SABLONY_PANELU,
        'osobni-udaje'              => TL_OSOBNICH_UDAJU_UZIVATELE,
        'clanky-nastaveni'          => TL_VSECH_CLANKU,
        'stranky-nastaveni'         => TL_VSECH_STRANEK,
        'galerie-nastaveni'         => TL_VSECH_GALERII,
        'novinky-nastaveni'         => TL_VSECH_NOVINEK,
        'ankety-nastaveni'          => TL_VSECH_ANKET,
        'formulare-nastaveni'       => TL_VSECH_FORMULARU,
        'mapa-stranek'              => TL_RAZENI_STRANEK,
        'anketa'                    => TL_ANKETY,
        'ankety-nastaveni'          => TL_ANKETY_NASTAVENI,
        'formular'                  => TL_FORMULARE,
        'formular-html'             => TL_HTML_KODU_FORMULARE,
        'formular-zprava'           => TL_ZPRAVY_FORMULARE, 
        'diskuze-prispevek'         => TL_DISKUZNIHO_PRISPEVKU,
        'diskuze-nastaveni'         => TL_DISKUZE,
        'diskuze-blacklist'         => TL_IP_ADRESY_V_BLACKLISTU,
        'diskuze-filtr'             => TL_FILTRU_DISKUZE,
        'editbox'                   => TL_EDITBOXU,
        'editbox-kolotoc'           => TL_KOLOTOCE_EDITBOXU,
        'editboxy-nastaveni'        => TL_VSECH_EDITBOXU,
        'newsletter-kampane'        => TL_EMAILOVE_KAMPANE,
        'newsletter-prijemci'       => TL_PRIJEMCE,
        'newsletter-skupiny'        => TL_EMAILOVE_SKUPINY,
        'newsletter-nastaveni'      => TL_MODULU_NEWSLETTERY,
        'newsletter-nastaveni-import'=>TL_IMPORTU_KONTAKTU,
        'statistiky-nastaveni'      => TL_STATISTIK,
        'domeny'                    => TL_DOMENY,
        'uzivatele'                 => TL_UZIVATELE,
        'uzivatele-skupiny'         => TL_SKUPINU,
        'administrator-email'       => TL_ADMINISTRATOROVI,
        //'udaje-webu'                => TL_WEBU,
        'clanek-fotka'              => TL_FOTKY_CLANKU,
        'stranka-fotka'              => TL_FOTKY_STRANKY,
        'produkt-fotka'             => TL_FOTKY_PRODUKTU,
        'akce-fotka'                => TL_FOTKY_AKCE,
        'novinka-fotka'             => TL_FOTKY_NOVINKY,
        'editbox-fotka'             => TL_FOTKY_EDITBOXU,
        'newsletter-priloha'        => TL_PRILOHY_NEWSLETTERU,
        'preklady-admin'            => TL_PREKLADY_ADMIN,
        'preklady-web'              => TL_PREKLADY_WEB,
        'napoveda'                  => TL_NAPOVEDA,
        'zaloha-db'                 => TL_ZALOHA_DB,
        'nastaveni'                 => TL_WEBU,
        'panel'                     => TL_PANEL,
        'tag'                       => TL_TAG,
        'presmerovani'              => TL_PRESMEROVANI,
        'provize'                   => "žádost o provizi",
        'objednavka'                => "objednávka",
        );
        
    private $allowed_action_type = array(
        'create'        => TVYTVORENI,
        'edit-settings' => TEDITACE_NASTAVENI,
        'edit-content'  => TEDITACE_OBSAHU,
        'delete'        => TSMAZANI,
        'login'         => TPRIHLASENI_DO_SYSTEMU,
        'logout'        => TODHLASENI_ZE_SYSTEMU,
        'export'        => TEXPORT,
        'import'        => TIMPORT,
        'send'          => TZASLANI_EMAILU,
        'upload'        => TNAHRANI_FOTKY_SOUBORU
        );
        
        
        
    //konstruktor seznamu logu
    public function __construct($domain_id, $lang_id = 0, $user_id = 0, $object_id = 0, $object_type='', $action_type="", $date_od="", $date_do="", $limit = "")
    {
        global $db;
        
        $this->domain_id = $domain_id;
        $this->lang_id = $lang_id;
        $this->user_id = $user_id;
        $this->object_id = $object_id;
        $this->object_type = $object_type;
        $this->date_do = $date_do;
        $this->date_od = $date_od;
        $this->action_type = $action_type;
        
        $where = "";
        if($this->lang_id > 0)
            $where .= " AND idJazyka=".$this->lang_id; 
        if($this->object_id > 0)
            $where .= " AND idObjektu=".$this->object_id; 
        if(is_array($this->object_type) && count($this->object_type) > 0)
            $where .= " AND typObjektu IN (".implode(',',$this->object_type).")";
        if($this->date_do != "")
            $where .= " AND datum <= '".GetGADate($this->date_do)." 23:59:59'";
        if($this->date_od != "")
            $where .= " AND datum >= '".GetGADate($this->date_od)." 00:00:00'";
        if($this->user_id > 0)
            $where .= " AND idUzivatele=".$this->user_id;
        if(is_array($this->action_type) && count($this->action_type) > 0)
            $where .= " AND akce IN (".implode(',',$this->action_type).")";
        
            
        if($limit == "")
            $limit = POCET_ZAZNAMU_NA_JEDNU_STRANKU;
                         
        $data = $db->query("SELECT SQL_CALC_FOUND_ROWS *, DATE_FORMAT(datum,'%d.%m.%Y %H:%i:%s') AS datum_komplet 
            FROM ".TABLE_LOG." 
            WHERE idDomeny=".$this->domain_id." ".$where."
            GROUP BY CONCAT(idObjektu,typObjektu,datum) 
            ORDER BY datum DESC
            LIMIT ".$limit."
            ");
        
        while($log = $db->getAssoc($data))
        {
            $id = $log['idLogu'];
            $text = $this->interpret_log($log['idObjektu'],$log['nazevObjektu'],$log['typObjektu'],$log['akce']);
            
            $this->logs[$id] = new LogItem($log['idModulu'],$log['idDomeny'],$log['idJazyka'],$log['idUzivatele'],$log['jmenoUzivatele']);
            $this->logs[$id]->set($log['akce'],$log['typObjektu'],$log['idObjektu'],$log['nazevObjektu'],$text,$log['datum_komplet']);
        }
        
        
    }
    
    
    //funkce vraci pole objektu s logy
    public function get_list()
    {
        return $this->logs;
    }
    
    
    //interpretuje log do citelne textove podoby
    private function interpret_log($objekt_id,$object_name,$object_type,$action)
    {
        $text = $this->allowed_action_type[$action]." ";
        
        if(isset($this->allowed_object_types[$object_type]))
            $text .= $this->allowed_object_types[$object_type]." ";
            
        $text .= $this->get_url_admin_object($objekt_id,$object_name,$object_type,$action);
        return $text; 
    }
    
    
    //vraci url adresu pro správu objektu v administraci cms
    private function get_url_admin_object($objekt_id,$object_name,$object_type,$action)
    {
        $url = "";
        $povolene_akce = array('delete','upload','login','logout','export','import','send'); 
        $povolene_typy = array();
        /*
        if($objekt_id > 0 && $object_name != "" && in_array($action,$povolene_akce))
        {
                
        }
          */  
            
        if($object_name != "")
            return "<b>".$object_name."</b>";
            else
            return $url;
            
        
            
    }
    
    public function get_logs_count()
    {
        return count($this->logs);
    }
    
    
}

?>