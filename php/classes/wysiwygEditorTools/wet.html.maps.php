<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

?>

<!--
<p>Pomocí tohoto nástroje můžete do wysiwyg editoru vložit mapku z Google Maps.</p>
-->

<table class='we-table'>
<tr>
            <th><?php echo TZADEJTE_ADRESU_MESTO_ULICE; ?></th>
            <td class='we-input'><input type='text' value='' name='maps-adresa' class='maps-adresa'/></td>
            <td></td></tr>
        
        
        <!--
<tr>
            <th>Vyberte typ mapky</th>
            <td class='we-input'>
            <input type='radio' value='seznam' name='maps-typ' id='mt1' class='radio' checked='checked'/>Mapy.cz
            <input type='radio' value='google' name='maps-typ' id='mt2' class='radio'/>Google Maps
            </td>
-->
        
<tr>
            <th><?php echo TPRIBLIZENI;?></th>
            <td class='we-input'>
            <select class='maps-zoom' name='zoom'>
            <option value='1'>1</option>
            <option value='2'>2</option>
            <option value='3'>3</option>
            <option value='4'>4</option>
            <option value='5'>5</option>
            <option value='6'>6</option>
            <option value='7'>7</option>
            <option value='8'>8</option>
            <option value='9'>9</option>
            <option value='10'>10</option>
            <option value='11'>11</option>
            <option value='12'>12</option>
            <option value='13'>13</option>
            <option value='14'>14</option>
            <option value='15'>15</option>
            <option value='16' selected='selected'>16</option>
            <option value='17'>17</option>
            <option value='18'>18</option>
            <option value='19'>19</option>
            <option value='20'>20</option>
            </select>
            </td>   
<td></td></tr>
        
        <tr><th><?php echo TNAHLED_MAPY;?></th><td><img src='' class='maps-nahled' width='200' height='200' alt='' title='<?php echo TVLOZIT_DO_EDITORU;?>' /></td><td><a class='getMaps' href='#'><span class="fa fa-check"></span> <?php echo TVLOZIT_DO_EDITORU;?></a></td></tr>
</table>


<script type="text/javascript">
<!--

$(function(){
    
    var wet_id = "#wet<?php echo $this->getWetId();?> ";
    var editor = CKEDITOR.instances.<?php echo $this->wysiwyg_editor_id;?>;
    
    $(document).on('click',wet_id + ".getMaps",function(){
                var adresa = $(wet_id + '.maps-adresa').val();
                var zoom = $(wet_id + '.maps-zoom').val();
                if($.trim(adresa)=='') {
                    alert("<?php echo TNENI_ZADANA_ADRESA_PRO_MAPU;?>");
                    return false;
                    }
                $.post('<?php echo AJAX_GATEWAY_WET;?>google.maps',{adresa: adresa,zoom: zoom},function(data){
                    editor.insertHtml( data );
                    $.scrollTo($("#we<?php echo $this->getWetId();?>"), 500);
                    
                    });
                return false;
                });
                
            $(document).on('change',wet_id + '.maps-adresa, ' + wet_id + '.maps-zoom', function(){
                var adresa = $(wet_id + '.maps-adresa').val();
                var zoom = $(wet_id + '.maps-zoom').val();
                $.post('<?php echo AJAX_GATEWAY_WET;?>google.maps&nahled',{adresa: adresa,zoom: zoom},function(data){
                    $(wet_id + '.maps-nahled').attr('src',data);
                    });
                })
    
})

// -->
</script>