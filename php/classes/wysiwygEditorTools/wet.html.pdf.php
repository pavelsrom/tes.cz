<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

?>

<p><?php echo TPOZOR_PDF_MUZE_OBSAHOVAT_NEPRESNE_FORMATOVANI;?></p>
<table class='we-table'>
<tr>
            <th><?php echo TNAZEV_PDF_SOUBORU; ?></th>
            <td class='we-input'><input type='text' value='document.pdf' name='pdf-name' class='pdf-name'/></td>
            <td><a class='toPdf' href='#'><span class=" fa fa-download"></span> <?php echo TSTAHNOUT_PDF_SOUBOR;?></a></td></tr>
        </table>



<script type="text/javascript">

$(function(){
<!--    
var wet_id = "#wet<?php echo $this->getWetId();?> ";
var editor = CKEDITOR.instances.<?php echo $this->wysiwyg_editor_id;?>;


$.download = function(url, data, nazev_souboru){
    //url and data options required
    if( url && data ){
        //data can be string of parameters or array/object
        data = typeof data == 'string' ? data : $.param(data);
        var form = $('<form action="'+ url +'" method="post"></form>');
        //split params into form inputs
        form.append('<input type="hidden" name="nazev_souboru" id="nazev_souboru" value=""/>');
        form.append('<input type="hidden" name="html" id="html" value=""/>');
        form.find('#html').val(data);
        form.find('#nazev_souboru').val(nazev_souboru);
        
        /*
        $.each(data.split('&'), function(){
            var pair = this.split('=');
            form.append('<input type="hidden" name="'+ pair[0] +'" id="'+ pair[0] +'" />');
            form.find('#' + pair[0]).val(escape(pair[1]));
            });
        */
        
        
    //submit the form
    form.appendTo('body').submit().remove();
    };
}; 


$(document).on('click',wet_id + ".toPdf",function(){
    var nazev_souboru = $(wet_id + '.pdf-name').val();
    var html = editor.getData();
    var data = nazev_souboru + html;
    $.download('<?php echo AJAX_GATEWAY_WET;?>html.to.pdf',data, nazev_souboru);
    return false;
    });
            
})
// -->            
</script>
                
       