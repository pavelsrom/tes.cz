<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */


global $db;
global $modules;
global $domain;
global $links;

$m = $modules->GetActiveModule();
$modul = $m->moduleSystemName;
$idObjektu = $m->get_id_action();

if($modul == 'editboxes')
    $typ = 'editbox';
elseif($modul == 'newsletter')
    $typ = 'newsletter';
else
    $typ = 'stranka';
    

$data = $db->query("SELECT z.idZalohy AS id, z.typ, DATE_FORMAT(z.datum,'%d.%m.%Y %H:%i') AS datum, 
        TRIM(CONCAT(u.jmeno,' ',u.prijmeni)) AS jmeno 
        FROM ".TABLE_ZALOHY." AS z 
        LEFT JOIN ".TABLE_UZIVATELE." AS u ON z.idUzivatele=u.idUzivatele
        WHERE idDomeny=".$domain->getId()."
            AND idObjektu=".$idObjektu."
            AND typ='".$typ."'
        ORDER BY z.datum DESC
        ");

$form = new FormFilter();
$form->add_text(THLEDAT_V_PEREXU_A_OBSAHU, 'vyraz', "", 0, false, "", "vyraz");

echo $form->get_html_code();


$table = new Table("tZalohy".$this->getWetId(),"table tZalohy");

$table->tr_head()->add(TDATUM)->add(TJMENO_A_PRIJMENI)->add(TAKCE,'w150');

echo $table->get_html();



?>




<div class="cleaner"></div>

<!-- overlayed element -->
<div class="apple_overlay overlay_zaloha" id="overlay-zaloha">
  <!-- the external content is loaded inside this tag -->
  <div class="contentWrap"></div>
</div>


<script type="text/javascript">
<!--
$(function(){
    
    var editor = CKEDITOR.instances.<?php echo $this->wysiwyg_editor_id;?>;
    var editor_perex = CKEDITOR.instances.wysiwygEditorPerex;
    
    var oTable = $("#tZalohy" + <?php echo $this->getWetId();?>).dataTable({
        "oLanguage": {
            "sLengthMenu": "<?php echo TZOBRAZENO_X_ZAZNAMU_NA_STRANCE;?>",
      		"sZeroRecords": "<?php echo TNENALEZEN_ZADNY_ZAZNAM;?>",
            "sInfo": "<?php echo TZOBRAZENO_X_AZ_Y_ZAZNAMU_Z_X_ZAZNAMU;?>",
            "sInfoEmpty": "<?php echo TZOBRAZENO_0_AZ_0_ZAZNAMU_Z_0_ZAZNAMU;?>",
            "sInfoFiltered": "(<?php echo TFILTROVANO_Z_X_ZAZNAMU;?>)",
            "sProcessing": "<?php echo TVAS_POZADAVEK_SE_ZPRACOVAVA;?>",
            "sLoadingRecords": "<?php echo TCEKEJTE_PROSIM;?>...",
            "oPaginate": {
                "sFirst": "<?php echo TPRVNI;?>",
                "sLast": "<?php echo TPOSLEDNI;?>",
                "sNext": "<?php echo TNASLEDUJICI;?>",
                "sPrevious": "<?php echo TPREDCHOZI;?>"
                }
            },
        "bProcessing": true,
        "bServerSide": true,
        "bSortClasses": false,
        "bStateSave": <?php echo UKLADAT_STAVY_FILTRU_SEZNAMU; ?>,
        "iDisplayLength": 10,
        "sPaginationType": "full_numbers",
        "sAjaxSource": "<?php echo AJAX_GATEWAY_WET;?>content.backup.list&object_id=<?php echo $idObjektu;?>&object_type=<?php echo $typ; ?>",
        "aoColumns": [
            {"sClass": "sl1","bSortable": false},{"sClass": "sl2","bSortable": false},{"sClass": "sl3 akce","bSortable": false}
            ],
        "fnServerData": function ( sSource, aoData, fnCallback ) {
                aoData.push({ "name": "vyraz", "value": $("#vyraz").val() });
                $.getJSON( sSource, aoData, function (json) { fnCallback(json); } );
                }
     
    })
    
    $("input#vyraz").bind('keyup change', function() {
            oTable.fnDraw(); 
            });
    
    //prida k bunce th span
    $("tZalohy .dataTable th").each(function(){
        $(this).html("<span>" + $(this).html() + "</span>");
    })
    
    $(document).on("click","a.vlozit_perex_do_editoru",function(){
        $("#confirm").dialog("close");
        var id = $(this).attr("id").substring(7);
        var perex = $("#perex_" + id).html();
        editor_perex.setData( perex );  
        $.scrollTo($("#wysiwygEditorPerex"), 300);
        return false;
    })
    
    $(document).on("click","a.vlozit_obsah_do_editoru",function(){
        $("#confirm").dialog("close");
        var id = $(this).attr("id").substring(7);
        var obsah = $("#obsah_" + id).html();
        editor.setData( obsah );
        $.scrollTo($("#we<?php echo $this->getWetId();?>"), 300);
        
        return false;
    })
    
    $(document).on("click","a.vlozit_popis1_do_editoru",function(){
        $("#confirm").dialog("close");
        var id = $(this).attr("id").substring(7);
        var obsah = $("#popis1_" + id).html();
        e1 = CKEDITOR.instances.w1;
        e1.setData( obsah );
        $.scrollTo($("#wet2"), 300);
        
        return false;
    })
    
    
    $(document).on("click","a.vlozit_popis2_do_editoru",function(){
        $("#confirm").dialog("close");
        var id = $(this).attr("id").substring(7);
        var obsah = $("#popis2_" + id).html();
        e1 = CKEDITOR.instances.w2;
        e1.setData( obsah );
        $.scrollTo($("#wet3"), 300);
        
        return false;
    })
    
    $(document).on("click","a.vlozit_popis3_do_editoru",function(){
        $("#confirm").dialog("close");
        var id = $(this).attr("id").substring(7);
        var obsah = $("#popis3_" + id).html();
        e1 = CKEDITOR.instances.w3;
        e1.setData( obsah );
        $.scrollTo($("#wet4"), 300);
        
        return false;
    })
    
    $(document).on("click","a.vlozit_popis4_do_editoru",function(){
        $("#confirm").dialog("close");
        var id = $(this).attr("id").substring(7);
        var obsah = $("#popis4_" + id).html();
        e1 = CKEDITOR.instances.w4;
        e1.setData( obsah );
        $.scrollTo($("#wet5"), 300);
        
        return false;
    })
    
    $(document).on("click","a.vlozit_popis5_do_editoru",function(){
        $("#confirm").dialog("close");
        var id = $(this).attr("id").substring(7);
        var obsah = $("#popis5_" + id).html();
        e1 = CKEDITOR.instances.w5;
        e1.setData( obsah );
        $.scrollTo($("#wet6"), 300);
        
        return false;
    })
    
    $(document).on("click",".we-tools a.nahled",function(e){
        
        $("#confirm").dialog({
              modal: true,  
              title: "<?php echo TNAHLED_ZALOHOVANEHO_OBSAHU;?>",
              dialogClass: "simplemodal okno_zalohy",
              width: "60%",
              height: 600
            });
        id = $(this).attr("id").substring(1);  
        jQuery('#confirm').load("<?php echo AJAX_GATEWAY_WET;?>content.backup&id=" + id).dialog('open'); 
        return false;

        })
    

})
// -->
</script>
