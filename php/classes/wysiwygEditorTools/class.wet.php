<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

class C_WysiwygEditorTools
{
    
    //obsahuje seznam pluginu
    private $allowed_plugins_list = array();
    
    //obsahuje vsechny mozne pluginy
    private $all_plugins_list = array();
    
    //obsahuje vsechny pluginy, ktere se budou renderovat
    private $render_plugins_list = array();
    
    //prefix id pluginu
    private $id_prefix = "wet-";
    
    //pocet instanci objektu wysiwyg editor tools
    private static $wetId = 0;
    
    //wysiwyg editor id
    private $wysiwyg_editor_id = "";

    
    //kontruktor
    public function C_WysiwygEditorTools($all_plugins_list = null, $allowed_plugins_list = null, $wysiwyg_editor_id="wysiwygEditor")
    {
        $this->allowed_plugins_list = $allowed_plugins_list;
        $this->all_plugins_list = $all_plugins_list;
        self::$wetId++;
        
        $this->wysiwyg_editor_id = $wysiwyg_editor_id;
        
        //zjisti, ktere pluginy se budou renderovat
        foreach($this->allowed_plugins_list AS $ap)
        {
            
            foreach($this->all_plugins_list AS $p)
            {
                if($ap == $p['id'] && $p['enabled'])
                {
                    $this->render_plugins_list[] = $p;
                    break;
                }
                    
            }
            
        }
                
        
    }
    
    //vraci cislo instance objektu
    public function getWetId()
    {
        return self::$wetId;
    }
    
    //renderuje html kod pro wysiwyg editor tools
    public function render()
    {
        global $db;
        global $domain;

        echo "<div class='we-tools' id='wet".$this->getWetId()."'>";
        echo "<div class='we-tools-header'>";
        
        foreach($this->render_plugins_list AS $p)
        {
            echo "<div class='we-tools-icon icon_".$p['id']."' id='".$this->id_prefix.$p['id'].$this->getWetId()."'>

                <div><span class='fa fa-".$p['icon']."'></span>".$p['name']."</div>
            </div>";
        }
            
        echo "</div>";
        echo "<div class='cleaner'></div>";
        
        foreach($this->render_plugins_list AS $p)
        {
            echo "<div class='we-tools-content' id='content-".$this->id_prefix.$p['id'].$this->getWetId()."'>";
            include($p['html']);
            echo "</div>";
        }
        
        
        
        echo "</div>";
        echo "<span id='we".$this->getWetId()."'></span>";
        $this->javascript_for_layout();
        
    }
    
    
    private function javascript_for_layout()
    {
        
        ?>
        
        <script type="text/javascript">
        
        
        
        $(function(){
            
            var wet_id = "#wet<?php echo $this->getWetId();?> ";
            
            $(wet_id + " .we-tools-content").hide();
                 
                $(document).on('click',wet_id + ' .we-tools-icon',function(){
                   
                    $(wet_id + '.we-tools-content').slideUp('fast');
                    $(wet_id + '.we-tools-icon').removeClass('we-tools-active');
                    
                    
                    
                    var id = $(this).attr('id');
                    $(this).addClass('we-tools-active');
                    
                    var d = $(wet_id + '#content-'+id).css('display');
                    if(d == 'none')
                        $(wet_id + '#content-'+id).slideDown('fast');
                        else
                        $(wet_id + '#content-'+id).slideUp('fast');
                        
                    return false;
                    });
                    
                $(document).on('mouseover',wet_id + ' .we-tools-icon',function(){
                    $(this).addClass('active');
                })
                
                $(document).on('mouseout',wet_id + ' .we-tools-icon',function(){
                    $(this).removeClass('active');
                })
                    
            })
             </script>
                
        <?php
    }
    
    
    
    
   
    
}



?>