<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */


$wet_plugins = array();

$wet_plugins[] = array(
    "id"        =>  "template", 
    "icon"      =>  "template_icon.png",
    "name"      =>  TNACIST_SABLONU,
    "html"      =>  "wet.html.template.php",
    "enabled"   =>  true
    );
    
$wet_plugins[] = array(
    "id"        =>  "pdf",
    "name"      =>  TEXPORT_DO_PDF,
    "icon"      =>  "file-pdf-o",
    "html"      =>  "wet.html.pdf.php",
    "enabled"   =>  true
    );

$wet_plugins[] = array(
    "id"        =>  "url",
    "name"      =>  TVLOZIT_Z_URL,
    "icon"      =>  "url_icon.png",
    "html"      =>  "wet.html.url.php",
    "enabled"   =>  false
    );

$wet_plugins[] = array(
    "id"        =>  "maps",
    "name"      =>  TVLOZIT_MAPKU,
    "icon"      =>  "map-marker",
    "html"      =>  "wet.html.maps.php",
    "enabled"   =>  true
    );
    
$wet_plugins[] = array(
    "id"        =>  "lang",
    "name"      =>  TPRELOZIT_TEXT,
    "icon"      =>  "flag",
    "html"      =>  "wet.html.lang.php",
    "enabled"   =>  true
    );
    
$wet_plugins[] = array(
    "id"        =>  "graph1",
    "name"      =>  TVLOZIT_KRUHOVY_GRAF,
    "icon"      =>  "graph1_icon.png",
    "html"      =>  "wet.html.graph1.php",
    "enabled"   =>  true
    );  
    
$wet_plugins[] = array(
    "id"        =>  "graph2",
    "name"      =>  TVLOZIT_SLOUPCOVY_GRAF,
    "icon"      =>  "graph2_icon.png",
    "html"      =>  "wet.html.graph2.php",
    "enabled"   =>  false
    );    
    
$wet_plugins[] = array(
    "id"        =>  "graph3",
    "name"      =>  TVLOZIT_SPOJNICOVY_GRAF,
    "icon"      =>  "graph3_icon.png",
    "html"      =>  "wet.html.graph3.php",
    "enabled"   =>  false
    );    
    
$wet_plugins[] = array(
    "id"        =>  "rescure",
    "name"      =>  TOBNOVIT_ZE_ZALOHY,
    "icon"      =>  "life-ring",
    "html"      =>  "wet.html.rescure.php",
    "enabled"   =>  true
    );    
    
$wet_plugins[] = array(
    "id"        =>  "help",
    "name"      =>  TNAPOVEDA,
    "icon"      =>  "help_icon.png",
    "html"      =>  "wet.html.help.php",
    "enabled"   =>  true
    );    
    
    
    
?>