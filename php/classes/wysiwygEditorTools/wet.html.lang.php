<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

//echo "<p>Vyberte zdrojový jazyk a cílový jazyk. Pokud nevyberete zdrojový jazyk, systém se bude snažit rozpoznat jazyk sám.</p>";

        echo "<table class='we-table' style='width: 100%'>";
        
        $lang = array(''=>'-- '.TVYBERTE.' --','cs'=>TCESTINA,'sk'=>TSLOVENSTINA,'en'=>TANGLICTINA,'de'=>TNEMCINA,'pl'=>TPOLSTINA,'it'=>TITALSTINA,'fr'=>TFRANCOUSTINA,'es'=>TSPANELSTINA);
        $options = "";
        foreach($lang AS $l => $p)
            $options .= "<option value='".$l."'>".$p."</option>";
        
        
        echo "<tr>
            <th>".TZDROJOVY_JAZYK."</th>
            <td class='we-input'>
            <select name='zdrojovy_jazyk' class='zdrojovy_jazyk' size='1'>
            ".$options."
            </select>
            </td>
            
            <th>".TCILOVY_JAZYK."</th>
            <td class='we-input'>
            <select name='cilovy_jazyk' class='cilovy_jazyk' size='1'>
            ".$options."
            </select>
            </td>
            </tr>";
            
        echo "<tr>
            <th>".TZDROJOVY_TEXT."</th><td></td>
            <th>".TPRELOZENY_TEXT."</th><td></td>
            </tr>";
            
            
        

        echo "<tr>
            <td colspan='2' class='w50'><textarea name='zdrojovy_text' cols='' rows='' class='zdrojovy_text h100 w100'></textarea></td>
            <td colspan='2' class='w50'><textarea name='cilovy_text' cols='' rows='' class='cilovy_text h100 w100'></textarea></td>
            </tr>";
        
        echo "<tr><th></th><td></td><td><a href='#' class='prelozit'><span class='fa fa-sign-in'></span> ".TPRELOZIT."</a></td><td><a class='prekladDoEditoru' href='#'><span class='fa fa-check'></span> ".TVLOZIT_TEXT_DO_EDITORU."</a></td></tr>";
  
        echo "</table>";

?>





<script type="text/javascript">

$(function(){
<!--
    
var wet_id = "#wet<?php echo $this->getWetId();?> ";
var editor = CKEDITOR.instances.<?php echo $this->wysiwyg_editor_id;?>;

$(document).on('click',wet_id + '.prelozit', function(){
                var cilovy_jazyk = $(wet_id + '.cilovy_jazyk').val();
                var zdrojovy_jazyk = $(wet_id + '.zdrojovy_jazyk').val();
                var zdrojovy_text = $.trim($(wet_id + '.zdrojovy_text').val());
                
                if(cilovy_jazyk==''){
                    alert('<?php echo TMUSITE_ZVOLIT_CILOVY_JAZYK;?>');
                    return false;
                    }
                
                if(zdrojovy_jazyk==''){
                    alert('<?php echo TMUSITE_ZVOLIT_ZDROJOVY_JAZYK;?>');
                    return false;
                    }
                
                if(zdrojovy_text==''){
                    alert('<?php echo TZADEJTE_ZDROJOVY_TEXT_PRO_PREKLAD;?>');
                    return false;
                    }    
                
                $.post('<?php echo AJAX_GATEWAY_WET;?>text.translate',{jazyk1: zdrojovy_jazyk, jazyk2: cilovy_jazyk, text: zdrojovy_text},function(data){
                    $(wet_id + '.cilovy_text').html(data);
                    })
                    
                return false;
                })
            
            /*
            $('#oznacenyTextEditoru').click(function(){
                var text_na_preklad;
                //var text_na_preklad = $('#wysiwygEditor, .wysiwygEditor').tinymce().selection.getContent({format : 'text'}))
                $('#zdrojovy_text').val(text_na_preklad);
                })
                
            $('#celyTextEditoru').click(function(){
                //var text_na_preklad;
                var text_na_preklad = $('#wysiwygEditor, .wysiwygEditor').text());// tinymce().getContent({format : 'text'}))
                $('#zdrojovy_text').val(text_na_preklad);
                })
            */    
            $(document).on('click',wet_id + '.prekladDoEditoru',function(){
                var prelozeny_text = $(wet_id + '.cilovy_text').val();
                
                if(prelozeny_text==''){
                    alert('<?php echo TNEBYL_PRELOZEN_ZADNY_TEXT;?>');
                    return false;
                    }
                
                editor.insertHtml( prelozeny_text );    

                $.scrollTo($("#we<?php echo $this->getWetId();?>"), 500);
                return false;
                })
            
})
// -->
</script>