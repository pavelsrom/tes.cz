<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */


include_once("class.wet.php");
include("config.plugins.php");

$plugins = array("pdf", "maps", "lang", "rescure");

$wet = new C_WysiwygEditorTools( $wet_plugins, $plugins, $wysiwygEditorId);

$wet->render();

?>