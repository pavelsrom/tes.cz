<?php

/**
 * @author Pavel Srom
 * @copyright 2013
 */

class C_Domains{
	
    //id aktivni domeny
	private $id;
    
    //jmeno aktivni domeny
	private $name;	
    
    //absolutni url domeny i s adresarem
    private $url;
    
    //relativni url domeny
    private $relative_path;
    
    //data aktivni domeny	
	public $data;

    //adresar domeny
    private $dir;

		
	//konstruktor
	//pokud je zadan hash, bude nactena domena podle hashe

	public function __construct( $hash = "" , $domain_id=0){
		global $db;
			
        $where = "";
        
        if($hash == "" && $domain_id==0)
        {
            //autodetekce aktivni domeny
            $name_data = explode('.', $_SERVER['HTTP_HOST']);
            if($name_data[0] == 'www') 
                unset($name_data[0]);
        
            $where = "domena='".implode('.', $name_data)."'";
        }
        elseif($hash != "")
        {
            $where = "hash='".$hash."'";
        }
        elseif($domain_id > 0)
        {
            $where = "idDomeny='".$domain_id."'";
        }
 
        //inicializace promennych
        $this->id = 0; 
        $this->name = "";
        $this->url = "";
        $this->relative_path = "";
        $this->data = null;    
            
		$data = $db->Query("SELECT idDomeny, domena, hash, adresar, www, urlStruktura, urlKoncovka,
                    IF(adresar='',CONCAT('".PROTOKOL."',IF(www = 1, 'www.',''),domena,'/'),CONCAT('".PROTOKOL."',IF(www = 1, 'www.',''),domena,'/',adresar,'/')) AS url,
                    IF(adresar='','/',CONCAT('/',adresar,'/')) AS relative_path
					FROM ".TABLE_DOMENY."
					WHERE ".$where." 
                        AND zobrazit=1 
                    LIMIT 1");


					
		if($db->numRows($data)==1){
		  $this->data = $db->getAssoc($data);
          $this->id = $this->data['idDomeny'];
          $this->name = $this->data['domena'];
          $this->url = $this->data['url'];
                    
          $this->relative_path = $this->data['relative_path'];
          
          $d = $db->query("SELECT idModulu FROM ".TABLE_DOMENY_MODULY." WHERE idDomeny='".$this->id."'");
          $this->data['moduly'] = array();
          while($m = $db->getAssoc($d))
            $this->data['moduly'][] = $m['idModulu']; 
            
          $d = $db->query("SELECT idJazyka FROM ".TABLE_DOMENY_JAZYKY." WHERE idDomeny='".$this->id."'");
          $this->data['jazyky'] = array();
          while($m = $db->getAssoc($d))
            $this->data['jazyky'][] = $m['idJazyka']; 
          
          
            $this->dir = $this->getDir();

          }  
          
          
          
		}
	
    //overi zda domena existuje
    public function exists()
    {
        return (bool) $this->id;
    }
    
    //vraci id aktivni domeny
    public function getId()
    {
        return $this->id;
    }
    
    
    //vraci jmeno aktivni domeny
    public function getName()
    {
        return $this->name;
    }
    
    //vraci url rootu aktivni domeny
    public function getUrl()
    {
        return $this->url;
    }
    
    //vraci relativni url rootu aktivni domeny
    public function getRelativeUrl()
    {
        return $this->relative_path;
    }
    
    //vraci relativni url rootu aktivni domeny
    public function getRelativePath()
    {
        return $this->relative_path;
    }
    
    
    
    //vraci hodnotu z pole data, pokud hodnota neexistuje vraci false
    public function get($key = "")
    {
        return isset($this->data[$key]) ? $this->data[$key] : false;
    }
	
	
	public function getDir(){

        if($this->dir != "")
            return $this->dir;

		$id = get_sysint($this->id);

		$name = mb_eregi_replace("\.","_",$this->name);
		//$dir = $id."-".$name;
        $dir = $id;

        return DIRNAME_DOMAINS.$dir."/";

	}

    public function setDir($dir)
    {
        $this->dir = $dir;
    }
	

	//vraci true pokud se ma provest presmerovani domeny na verzi www, jinak vraci false
	public function redirectWww(){
					
		$data = explode('.', $_SERVER['HTTP_HOST']);
		return $data[0]!='www' && $this->data['www']==1; 

		}
		
		
	public function get_absolute_dir_path()
    {
        $url = rtrim($this->url,"/").RELATIVE_URL_USER_DOMAIN;
        return $url;
    }



	
	 
	
	
}


?>