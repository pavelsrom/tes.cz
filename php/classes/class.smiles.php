<?php

/**
 * @author Pavel Srom
 * @copyright 2010
 */


class C_Smiles{
	
	//pole souboru smajliku
	private $smiles;
	
	//pole zkratek smajliku
	private $shortcuts; 
	
    //indikuje zda paleta smajliku existuje
	public $exists;
	
    //id tematu
	private $idThemes;
	
	private $dirName; 
	
	public function __construct($idThemes){
		
		global $db;
		global $domain;
		
		
		if($idThemes==0) return;
			
		$this->exists = false;
		
		$this->smiles = array();
		$this->shortcuts = array();
		$this->idThemes = $idThemes;
		
		
		$data = $db->Query("SELECT s.adresar, sp.idSmajlika, sp.nazev, sp.zkratka,sp.soubor FROM ".TABLE_SMAJLICI." AS s 
			LEFT JOIN ".TABLE_SMAJLICI_POLOZKY." AS sp ON s.idTematu=sp.idTematu
			WHERE s.zobrazit=1
				AND sp.zobrazit=1
				AND sp.soubor!=''
				AND s.idTematu=".$idThemes."
			ORDER BY sp.priorita, sp.idSmajlika
				
			");
			
		if($db->numRows($data)==0){
			return;
			}	
			
		$i=0;
		while($s = $db->getAssoc($data)){
			if($i==0) $this->dirName = $s['adresar'];
			$id = $s['idSmajlika'];
			$file = DIRNAME_SMILES.$this->dirName."/".$s['soubor'];

			if(!file_exists(PRE_PATH.$file)) continue;
			$file = PRE_PATH.$file;
				
			$this->smiles[$id] = "<img class='smile' src='".$file."' alt='".$s['nazev']."' title='".$s['zkratka']."' />";
			$this->shortcuts[$id] = $s['zkratka'];
			$i++;
			}
		
		if(count($this->smiles)==0) return;
			
		$this->exists = true;	
		
		mysql_free_result($data);
	}
	
	//nahradi zkratky smajliku v textu obrazkama
	public function GetSmilesToText($text){
		$res = "";
		
		if(!$this->exists) return $res;

		$res = str_replace($this->shortcuts, $this->smiles, $text);
		
		return $res;
		
	}
	
	
	//vraci smajliky pro nahled, bez odkazu
	public function GetSmiles(){
		$res = "";
		if(!$this->exists) return $res;
		
		$res .= "<div class='smiles'>";
		foreach($this->smiles AS $s)
			$res .= $s."\n";
			
		$res .= "</div>";
		return $res;
		
	}
	
	//vraci smajliky v podobe odkazu s javascriptovym kodem pro vkladani smajliku do textarea
	public function GetSmilesClickable($textareaId){
		$res = "";
		if(!$this->exists) return $res;
		
		$res = "<script type='text/javascript'>";
		$res .= "function get_smile(smile){
			document.getElementById('".$textareaId."').value += ' '+smile+' '; 
			document.getElementById('".$textareaId."').focus();
			}	
			
			";
		$res .= "</script>";
		
		$res .= "<div class='smiles'>";
		foreach($this->smiles AS $id => $s){
			$res .= "<a href=\"javascript:get_smile('".$this->shortcuts[$id]."')\">";
			$res .= $s;
			$res .= "</a>\n";
			}
		$res .= "</div>";
		return $res;
	}
	
	//vrati text bez smajliku
	public function DeleteSmilesFromText($text){
		$res = "";
		if(!$this->exists) return $res;
		
		$res = str_replace($this->shortcuts, '', $text);
		
		return $res;
		
	}
	
	
	
	
	
}

?>