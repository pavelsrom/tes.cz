<?php

/**
 * @author Pavel �rom
 * @copyright 2018
 */


class C_Basket{
    
    private $items = array();
    
    //obsahuje data polozek posledni objednavky
    private $order_items = array();
    
    //obsahuje data posledni objednavky
    private $order_data = array();
        
    //id prihlaseneho nebo nove registrovaneho uzivatele
    private $user_id;
        
    public function __construct()
    {
        global $db;
        global $private_pages;
        
        $user_id = $private_pages->is_logged() ? $private_pages->get_user_id() : session_id();

        $this->user_id = $user_id;
        
        //nacte polozky kosiku z databaze      
        $this->initialize_items();
            
        
        
        
        
    }

    private function initialize_items()
    {
        global $db;

        //nulovani/inicializace polozek
        $this->items = array();
                
        $data_kosik = $db->query("SELECT k.*, s.obrazek 
            FROM ".TABLE_ESHOP_KOSIK." AS k
            LEFT JOIN ".TABLE_STRANKY." AS s ON k.id_produkt = s.idStranky
            WHERE id_uzivatel='".$this->user_id."' 
            ORDER BY id");

        while($k = $db->getObject($data_kosik))
        {
            $this->items[] = $k;
        }
    }
    
    
    //vraci true, pokud je kosik prazdny
    public function is_empty()
    {
        return $this->get_items_count() == 0;
    }
    
           
    
    //vraci polozky kosiku v poli
    public function get_items()
    {
        return $this->items;
    }
    
    
    //vraci pocet polozek v kosiku
    public function get_items_count()
    {
        return count($this->items);
    }
    
    
    //vraci hodnotu kosiku vcetne dph
    public function get_total_price($formated = false, $s_dph = true)
    {
        global $private_pages;

        $celkem = 0;
        
        if(!$s_dph)
            $sl_cena = $private_pages->is_vo() ? 'cena_bez_dph_vo' : 'cena_bez_dph';
            else
            $sl_cena = $private_pages->is_vo() ? 'cena_s_dph_vo' : 'cena_s_dph';

        //$sl_cena_bez_dph = $private_pages->is_vo() ? 'cena_bez_dph_vo' : 'cena_bez_dph';

        foreach($this->items AS $k)
        {
            $celkem += $k->{$sl_cena} * $k->pocet; 
        }
        
        if($formated)
            return $celkem == 0 ? TKOSIK_JE_PRAZDNY : price($celkem);
            else
            return $celkem;
        
    }

    //klicova funkce, ktera validuje kroky objednavky
    public function check_steps()
    {
        global $page;
        global $private_pages;
        global $links;

        //kontrola, jestli je neco v kosiku
        if($this->is_empty())
        {
            Redirect($links->get_url(ID_KOSIK));
        }

        //pro vstup do vsech kroku je nutne se prihlasit
        if(!$private_pages->is_logged())
        {
            //Redirect($links->get_url('login'));
            $private_pages->redirect(true, false);
        }


        $user = $private_pages->get();

        if($page->get_id() == ID_STRANKY_DOPRAVA_A_PLATBA || $page->get_id() == ID_STRANKY_SOUHRN_OBJEDNAVKY)
        {
            //kontrola, zda ma uzivatel vyplneny fakturacni a dodaci udaje
            

            if(($user['dodaci_adresa'] && !TestAdress($user['ulice_dod'], $user['cislo_dod'], $user['mesto_dod'], $user['psc_dod']))
                || !TestAdress($user['ulice'], $user['cislo'], $user['mesto'], $user['psc'])
            )
                Redirect($links->get_url(ID_STRANKY_DODACI_UDAJE));

        }

        if($page->get_id() == ID_STRANKY_SOUHRN_OBJEDNAVKY)
        {

            if($user['preferovana_doprava'] == 0 || $user['preferovana_platba'] == 0 || ($user['preferovana_doprava'] == 1 && $user['preferovana_prodejna'] == 0) || (($user['preferovana_doprava'] == 2 && !test_mapa_rozvozu($this->get_delivery_psc()))))
                Redirect($links->get_url(ID_STRANKY_DOPRAVA_A_PLATBA));

        }

        

    }

    //vraci psc pro doruceni
    public function get_delivery_psc()
    {
        global $private_pages;

        $user = $private_pages->get();

        $psc = $user['dodaci_adresa'] ? $user['psc_dod'] : $user['psc'];

        return $psc;

    }



    //funkce, ktera pri udalosti prihlaseni uzivatele prehodi session_id na id prihlaseneho uzivatele
    public function user_logged()
    {
        global $private_pages;
        global $db;

        $user_id = $private_pages->get_user_id();

        $db->update(TABLE_ESHOP_KOSIK, array("id_uzivatel" => $user_id), "id_uzivatel='".session_id()."'");

        //osetreni, aby se nenacetl stejny produkt na vice radku, kdyz bude mit napr. prihlaseny uzivatel v kosiku jeden makovec, pak se odhlasi do prazdneho kosiku da stejny makovec a pak se prihlasi a dojde ke slouceni kosiku, tak se musi ten stejny makovec ulozi jako jedna polozka a ne do dvou polozek
        $data = $db->query("SELECT *, SUM(pocet) AS pocet FROM ".TABLE_ESHOP_KOSIK." WHERE id_uzivatel = ".$user_id." GROUP BY id_produkt");

        //smazani vsech polozek kosiku
        $db->delete(TABLE_ESHOP_KOSIK, "id_uzivatel = ".$user_id);

        //nasypani spravnych polozek zpet s upravenymi pocty
        while($p = $db->getObject($data))
        {
            $insert = array(
                "id_uzivatel" => $user_id,
                "id_produkt" => $p->id_produkt,
                "nazev" => $p->nazev,
                "pocet" => $p->pocet,
                "cena_bez_dph" => $p->cena_bez_dph,
                "cena_bez_dph_vo" => $p->cena_bez_dph_vo,
                "cena_s_dph" => $p->cena_s_dph,
                "cena_s_dph_vo" => $p->cena_s_dph_vo,
                "dph" => $p->dph,
                "datum" => $p->datum
            );

            $db->insert(TABLE_ESHOP_KOSIK, $insert);
        }

        $this->initialize_items();

    }

    
    //prida polozku do kosiku
    public function add_item()
    {
        global $db;
        global $page;
        global $private_pages;

        $id_produkt = get_int_post("id_produkt", $page->get_id());
        $pocet = get_int_post("pocet", 1);  

        $produkt = $db->get(TABLE_STRANKY,array("nazev", 'cena_s_dph_vo', "cena_s_dph",'cena_bez_dph_vo','cena_bez_dph', "dph"),"idStranky=".$id_produkt." AND typ='produkt'");
        
        if(!$produkt)
            return false;

        //overeni zda je zbozi uz v kosiku
        $id_kosik = $db->get(TABLE_ESHOP_KOSIK,"id","id_produkt=".$id_produkt." AND id_uzivatel='".$this->user_id."'");
     
        
        if($id_kosik == 0)
        {

            $insert = array(
                "id_uzivatel" => $this->user_id,
                "id_produkt" => $id_produkt,
                "nazev" => $produkt->nazev,
                "pocet" => $pocet,
                "cena_s_dph" => $produkt->cena_s_dph,
                "cena_bez_dph" => $produkt->cena_bez_dph,
                "cena_s_dph_vo" => $produkt->cena_s_dph_vo,
                "cena_bez_dph_vo" => $produkt->cena_bez_dph_vo,
                "dph" => $produkt->dph,
                "datum" => "now"
                );

            $db->insert(TABLE_ESHOP_KOSIK, $insert);

        }
        else
        {
            
            $db->increment(TABLE_ESHOP_KOSIK, "pocet", $pocet, "id = ".$id_kosik);

        }

        $this->initialize_items();

        
        return true;
    }

    //přepočítá košík
    public function recalculate()
    {
        global $db;

        $polozky = get_array_post("pocet");
        
        foreach($polozky AS $id => $pocet)
        {
            if($pocet <= 0)
                $db->delete(TABLE_ESHOP_KOSIK, "id=".$id);
                else
                $db->update(TABLE_ESHOP_KOSIK, array("pocet" => $pocet), "id=".$id);

        }


    }
            
    
    //vymaze polozku z kosiku
    public function remove_item($id_item)
    {
        global $db;      

        $db->delete(TABLE_ESHOP_KOSIK, "id=".intval($id_item)." AND id_uzivatel='".$this->user_id."'");

        $this->initialize_items();
    }
    
    //vymaze vsechny polozky kosiku
    public function remove_all_items()
    {
        global $db;
        
        
        $db->delete(TABLE_ESHOP_KOSIK, "id_uzivatel='".$this->user_id."' OR id_uzivatel='".session_id()."'");

        $this->initialize_items();
    }
    
    
    //vraci udaje o vytvorene objednavce na zaklade udaju z db
    public function get_order_data($db_sloupecek = '')
    {
        if($db_sloupecek != '')
            $result = isset($this->order_data[$db_sloupecek]) ? $this->order_data[$db_sloupecek] : false;
            else
            $result = $this->order_data;
        
        return $result;
        
        
    }
    
    //vytvori objednavku
    public function create_order($remove_all_items = true)
    {
        global $db;
        global $private_pages;
        
        $kosik_polozky = (array) $this->get_items();

        //ulozeni objednavky
        $cislo_faktury = $this->generate_invoice_number();
        $user = $private_pages->get();

        $platba_data = $db->query("SELECT * FROM ".TABLE_ESHOP_TYPY_PLATEB." WHERE aktivni=1 AND id=".$private_pages->get("preferovana_platba"));
        $platba = $db->getObject($platba_data);

        $doprava_data = $db->query("SELECT * FROM ".TABLE_ESHOP_TYPY_DOPRAVY." WHERE aktivni=1 AND id=".$private_pages->get("preferovana_doprava"));
        $doprava = $db->getObject($doprava_data);

        $zbozi_s_dph = $this->get_total_price(false);
        $zbozi_bez_dph = $this->get_total_price(false, false);
       

        $doprava_dph = $doprava->dph;
        $doprava_s_dph = $doprava->cena_s_dph;
        $doprava_bez_dph = $doprava->cena_bez_dph;

        $balne_dph = BALNE_DPH;
        $balne_s_dph = BALNE_CENA_S_DPH;
        $balne_bez_dph = BALNE_CENA_BEZ_DPH;

        if($this->get_delivery_price($zbozi_s_dph, $doprava_s_dph, false) == 0)
        {
            $doprava_dph = $doprava_s_dph = $doprava_bez_dph = 0;
        }


        $platba_dph = $platba->dph;
        $platba_s_dph = $platba->cena_s_dph;
        $platba_bez_dph = $platba->cena_bez_dph;


        $insert = array(
            "id_uzivatel" => $this->user_id,
            "cislo_faktury" => $cislo_faktury,
            "jmeno" => $user['jmeno'],
            "prijmeni" => $user['prijmeni'],
            "firma" => $user['firma'],
            "ic" => $user['ico'],
            "dic" => $user['dic'],
            "email" => $user['email'],
            "telefon" => $user['telefon'],
            "ulice" => $user['ulice'],
            "cislo" => $user['cislo'],
            "mesto" => $user['mesto'],
            "psc" => $user['psc'],
            "ulice_dod" => $user['ulice_dod'],
            "cislo_dod" => $user['cislo_dod'],
            "mesto_dod" => $user['mesto_dod'],
            "psc_dod" => $user['psc_dod'],
            "id_platba" => $platba->id,
            "id_doprava" => $doprava->id,
            "id_prodejna" => $user['preferovana_prodejna'],
            "prodejna" => $db->get(TABLE_STRANKY, "CONCAT(nazev, ' - ', perexBezHtml)", "idStranky=".$user['preferovana_prodejna']),
            "doprava" => $doprava->nazev,
            "platba" => $platba->nazev,
            "firma_info" => $user['firma_info'],
            "dodaci_adresa" => $user['dodaci_adresa'],
            "velkoobchod" => $user['velkoobchod'],
            "id_stav" => 1,
            "doprava_s_dph" => $doprava_s_dph,
            "doprava_bez_dph" => $doprava_bez_dph,
            "doprava_dph" => $doprava_dph,
            "balne_s_dph" => $balne_s_dph,
            "balne_bez_dph" => $balne_bez_dph,
            "balne_dph" => $balne_dph,
            "platba_s_dph" => $platba_s_dph,
            "platba_bez_dph" => $platba_bez_dph,
            "platba_dph" => $platba_dph,
            "zbozi_s_dph" => $zbozi_s_dph,
            "zbozi_bez_dph" => $zbozi_bez_dph,
            "celkem_s_dph" => $zbozi_s_dph + $doprava_s_dph + $platba_s_dph + $balne_s_dph,
            "celkem_bez_dph" => $zbozi_bez_dph + $doprava_bez_dph + $platba_bez_dph + $balne_bez_dph,
            "datum" => "now"

            );
        
        $this->order_data = $insert;
        
        $db->insert(TABLE_ESHOP_OBJEDNAVKY, $insert);
        $id_objednavky = $db->lastId();


        //ulozeni polozek objednavky 
        $sl_cena = $private_pages->is_vo() ? 'cena_s_dph_vo' : 'cena_s_dph';
        $sl_cena_bez_dph = $private_pages->is_vo() ? 'cena_bez_dph_vo' : 'cena_bez_dph';

        foreach($kosik_polozky AS $k)
        {
            $k = (array)$k;
            $insert = array(
                "id_objednavka" => $id_objednavky, 
                "id_produkt" => $k["id_produkt"],
                "nazev" => $k["nazev"],
                "pocet" => $k["pocet"],
                "cena_s_dph" => $k[$sl_cena],
                "cena_bez_dph" => $k[$sl_cena_bez_dph],
                "dph" => $k['dph']
                );
                
            $this->order_items[] = $insert;
            
            $db->insert(TABLE_ESHOP_OBJEDNAVKY_POLOZKY, $insert);
        }
        
        //vymazani kosiku
        if($remove_all_items)
            $this->remove_all_items();
        
        return $id_objednavky;
        
    }

    //vymaze objednavku
    public function delete_order($order_id)
    {
        global $db;

        $db->delete(TABLE_ESHOP_OBJEDNAVKY, "id=".$order_id);
        $db->delete(TABLE_ESHOP_OBJEDNAVKY_POLOZKY, "id_objednavka=".$order_id);
    }


    
    //vraci polozky posledni vytvorene objednavky
    public function get_order_items_data()
    {
        return $this->order_items;
    }
    
    
    //generuje cislo faktury
    private function generate_invoice_number()
    {
        global $db;
        
        $rok = date("y");
        $mesic = date("m");
        
        $max = $db->get(TABLE_ESHOP_OBJEDNAVKY, "MAX(cislo_faktury)", "cislo_faktury LIKE '".$rok.$mesic."%'");
        
        //nove cislo faktury
        if($max == "")
        {
            $cislo_faktury = $rok.$mesic."0001";
        }
        else
        {
            $max = substr($max, -4);
            $max = intval($max) + 1;
            $cislo_faktury = $rok.$mesic.substr('0000'.$max,-4);
        }
        
        return $cislo_faktury;
        
        
    }
    

    
    //nastavi u dane objednavky datum, kdy byla objednavka zaplacena
    public function set_payment_date($id_objednavky)
    {
        global $db;
        
        $db->update(TABLE_ESHOP_OBJEDNAVKY, array("zaplaceno" => "now"), "id = ".$id_objednavky);
    }
    
    
    //nastavi id stavu objednavky
    public function set_order_state($id_objednavky, $id_stav)
    {
        global $db;
        
        $db->update(TABLE_ESHOP_OBJEDNAVKY, array("id_stav" => $id_stav), "id=".$id_objednavky);
    }

    public function load_order($order_id)
    {
        global $db;

        $data_obj = $db->query("SELECT *, DATE_FORMAT(datum, '%d.%m.%Y %H:%i') AS datum, DATE_FORMAT(zaplaceno, '%d.%m.%Y %H:%i') AS zaplaceno FROM ".TABLE_ESHOP_OBJEDNAVKY." WHERE id = ".intval($order_id));

        if($db->numRows($data_obj) == 1)
            return $db->getObject($data_obj);

        return false;



    }

    public function load_order_items($order_id)
    {
        global $db;

        $data_obj = $db->query("SELECT * FROM ".TABLE_ESHOP_OBJEDNAVKY_POLOZKY." WHERE id_objednavka = ".intval($order_id));

        $items = array();
        while($item = $db->getObject($data_obj))
            $items[] = $item;

        return $items;


    }
    
    
    private function get_id_state($gopay_state)
    {
        
        if($gopay_state == "CREATED")
            $id_stav = 1;//nova objednavka
        elseif($gopay_state == 'PAID')
            $id_stav = 2;//zaplaceno
        elseif($gopay_state == 'CANCELED' || $gopay_state == 'TIMEOUTED' || $gopay_state == 'REFUNDED')
            $id_stav = 3;//zruseno
        else
            $id_stav = 1;//nova objednavka
            
        return $id_stav;
        
        
        
    }
    
    //upravi u vsech polozek kosiku prislusnost k registrovanemu uzivateli, 
    //dokud neni uzivatel registrovan, tak je u nej prirazena session jako jediny identifikator
    public function set_to_login_user()
    {
        global $db;
        global $private_pages;

        if($private_pages->is_logged())
            $db->update(TABLE_ESHOP_KOSIK, ['id_uzivatel' => $private_pages->get_user_id()], "id_uzivatel = '".session_id()."'");


    }
    
    //vraci cenu dopravy
    public function get_delivery_price($basket_total_price, $delivery_total_price, $formated = true)
    {

        $delivery_price = $delivery_total_price;

        if($delivery_total_price == 0 || $basket_total_price >= DOPRAVA_ZDARMA_OD)
            $delivery_price = 0;

        if($formated)
        {
            $delivery_price = $delivery_price == 0 ? TZDARMA : price($delivery_price);
        }

        return $delivery_price;

    }

    //odesle fakturu pro danou objednavku
    public function send_order($id_objednavky, $with_invoice = false)
    {
        
        global $db;
        global $domain;


        //odeslani emailu s upozornenim a odkazem na potvrzeni a zaplaceni registrace
        $zprava = TEXT_EMAILU_FAKTURA;
        $predmet = PREDMET_EMAILU_FAKTURA;
               
        
        $obj = $this->load_order($id_objednavky);
        $items = $this->load_order_items($id_objednavky);

        /*
        if($with_invoice)
        {
            $url_faktury = get_url_invoice($hash, $obj->cislo_faktury);
            $prvni = strpos($zprava,'{');
            $posledni = strpos($zprava,'}');
            $nazev_odkazu = substr($zprava,$prvni+1,$posledni-$prvni-1);
            $zprava = str_replace('{'.$nazev_odkazu.'}','<a href="'.$url_faktury.'" class="button">'.$nazev_odkazu.'</a>',$zprava);
        }
        */

        

        $obsah = "";

        $table = new Table();
        $table->set_style("font-size: 16px; width: 100%; border: none; border-collapse:collapse; border-spacing: 0;");

        $table->tr_head()
                ->add_th(TNAZEV_PRODUKTU, "t-left", 0, "", 0, "background-color: #eeeeee; border-bottom: 1px solid #aaaaaa; padding: 8px;border-top: 1px solid #aaaaaa;")
                ->add_th(TPOCET, "t-right", 0, "", 0, "background-color: #eeeeee; border-bottom: 1px solid #aaaaaa; padding: 8px;border-top: 1px solid #aaaaaa;")
                ->add_th(TCENA_ZA_KS, "t-right", 0, "", 0, "background-color: #eeeeee; border-bottom: 1px solid #aaaaaa; padding: 8px;border-top: 1px solid #aaaaaa;")
                ->add_th(TCENA_CELKEM, "t-right", 0, "", 0, "background-color: #eeeeee; border-bottom: 1px solid #aaaaaa; padding: 8px;border-top: 1px solid #aaaaaa;");


        foreach($items AS $p)
        {
            
            $table->tr()
                ->add_td($p->nazev, "t-left", 0, "", 0, "text-align: left; padding: 8px; border-bottom: 1px solid #eeeeee")
                ->add_td($p->pocet, "t-right", 0, "", 0, "text-align: right; padding: 8px; border-bottom: 1px solid #eeeeee")
                ->add_td(price($p->cena_s_dph), "t-right", 0, "", 0, "text-align: right; padding: 8px; border-bottom: 1px solid #eeeeee")
                ->add_td(price($p->cena_s_dph * $p->pocet), "t-right", 0, "", 0, "text-align: right; padding: 8px; border-bottom: 1px solid #eeeeee");

        }

        $table->tr("doprava")
            ->add_td(TDOPRAVA.": ".$obj->doprava,"t-left", 0, "", 3, "text-align: left; padding: 8px; border-bottom: 1px solid #eeeeee; background: #f4f4f4;")
            ->add_td(price($obj->doprava_s_dph), "t-right", 0, "", 0, "text-align: right; padding: 8px; border-bottom: 1px solid #eeeeee; background: #f4f4f4;");

        if($obj->id_prodejna > 0)
        {
            $table->tr()->add_td(TPRODEJNA.": <strong>".$obj->prodejna."</strong>", "t-left", 0, "", 4, "text-align: left; padding: 8px; border-bottom: 1px solid #eeeeee; background: #f4f4f4;");
        }

        $table->tr("platba")
            ->add_td(TZPUSOB_PLATBY.": ".$obj->platba,"t-left", 0, "", 3, "text-align: left; padding: 8px; border-bottom: 1px solid #eeeeee; background: #f4f4f4;")
            ->add_td(price($obj->platba_s_dph), "t-right", 0 , "", 0, "text-align: right; padding: 8px; border-bottom: 1px solid #eeeeee;background: #f4f4f4;");

        
        
        $table->tr("balne")
            ->add_td(TBALNE,"t-left", 0, "", 3, "text-align: left; padding: 8px; border-bottom: 1px solid #eeeeee; background: #f4f4f4;")
            ->add_td(price($obj->balne_s_dph), "t-right", 0 , "", 0, "text-align: right; padding: 8px; border-bottom: 1px solid #eeeeee;background: #f4f4f4;");

        $table->tr("celkem")
            ->add_td(TCELKEM_S_DPH,"t-left", 0, "", 3, "text-align: left; padding: 8px; border-top: 1px solid #aaaaaa; border-bottom: 1px solid #aaaaaa; background: #ececec; font-weight: bold;")
            ->add_td(price($obj->celkem_s_dph), "t-right", 0, "", 0, "text-align: right; padding: 8px; border-top: 1px solid #aaaaaa; border-bottom: 1px solid #aaaaaa; background: #ececec; font-weight: bold;");


        $obsah .= '<h3>'.TCISLO_ODJEDNAVKY.'</h3>';
        $obsah .= '<p><strong>'.$obj->cislo_faktury.'</strong></p>';

        $obsah .= '<h3>'.TDATUM_ODJEDNAVKY.'</h3>';
        $obsah .= '<p>'.$obj->datum.'</p>';

        $obsah .= $table->get_html();

        $obsah .= '<h3>'.TFAKTURACNI_ADRESA.'</h3>';
        $obsah .= '<p>
            '.TJMENO_A_PRIJMENI.': '.trim($obj->jmeno." ".$obj->prijmeni).'<br />
            '.TULICE.', '.TCP.': '.trim($obj->ulice." ".$obj->cislo).'<br />
            '.TMESTO.': '.$obj->mesto.'<br />
            '.TPSC.': '.$obj->psc.'
            </p>';

        $obsah .= '<h3>'.TDODACI_ADRESA.'</h3>';

        if($obj->dodaci_adresa == 0)
            $obsah .= '<p>'.TDODACI_ADRESA_JE_STEJNA_JAKO_FAKTURACNI.'</p>';
        else
            $obsah .= '<p>
                '.TJMENO_A_PRIJMENI.': '.trim($obj->jmeno." ".$obj->prijmeni).'<br />
                '.TULICE.', '.TCP.': '.trim($obj->ulice_dod." ".$obj->cislo_dod).'<br />
                '.TMESTO.': '.$obj->mesto_dod.'<br />
                '.TPSC.': '.$obj->psc_dod.'
                </p>';

        if($obj->firma_info == 1)
        {
            $obsah .= '<h3>'.TINFORMACE_O_FIRME.'</h3>';
            $obsah .= '<p>
                '.TFIRMA.': '.$obj->firma.'<br />
                '.TIC.': '.$obj->ic.'<br />
                '.TDIC.': '.$obj->dic.'
            </p>';
        }




        //nacteni html sablony
        $zprava = str_replace("[rekapitulace]", $obsah, $zprava);
        $sablona = file_get_contents($domain->getDir()."email.html");
        $zprava = str_replace("{obsah}",$zprava,$sablona);
        
        $odeslano = SendMail(EMAIL_WEBU, NAZEV_WEBU, $obj->email, $predmet, $zprava);

        //kopie objednavky
        $odeslano = SendMail(EMAIL_WEBU, NAZEV_WEBU, KONTAKTNI_FORMULAR_EMAIL, $predmet, $zprava);
        $odeslano = SendMail(EMAIL_WEBU, NAZEV_WEBU, "pavelsrom@volny.cz", $predmet, $zprava);
        $odeslano = SendMail(EMAIL_WEBU, NAZEV_WEBU, "info@eracek.cz", $predmet, $zprava);
        
    }

    
 
    
}

?>