<?php


class C_Privileges{
    
    //pole opravneni
    private $privileges = array();
    
    //id opravneni prihlaseneho uzivatele
    private $privilege_id = 0;
    
    //priorita profilu prav prihlaseneho uzivatele
    private $privilege_priorite=0;
      
    
    //konstruktor
    public function __construct()
    {
        global $db;
        
        $data = $db->query("SELECT 
                nazev AS konst_nazev,
                systemovyNazev AS nazev, 
                priorita,
                idPrava,
				content_add, 
				content_view, 
				content_edit, 
				content_delete,
				settings_add, 
				settings_view, 
				settings_edit, 
				settings_delete, 
				set_delete,
				set_privilege,  
                seo,
                translations,
                only_my,
				send_email,
				export,
				import,
				statistics_view  
                FROM ".TABLE_CMS_PRAVA."
                ");
        
        while($p = $db->getAssoc($data))
        {
            $this->privileges[$p['idPrava']] = $p;           
        } 
        
        
        $db->free($data);
    }
    
    //nastavi id opravneni
    public function set_privilege_id($privilege_id)
    {
        $this->privilege_id = $privilege_id;
        
        //zjisteni hodnoty priority prihlaseneho uzivatele
        foreach($this->privileges AS $p)
            if($p['idPrava'] == $privilege_id)
            {
                $this->privilege_priorite = $p['priorita'];
            }
        
    }
    
    
    public function get_all_privileges()
    {
        return $this->privileges;
    }
    
    //vraci mixed, pokud je zadano privilege, pak vraci hodnotu 1 nebo 0. Pokud neni zadana hodnota privilege, pak vraci pole prav pro prihlaseneho uzivatele
    public function get($privilege_name = "")
    {
        if($privilege_name == "")
            return $this->privileges[$this->privilege_id];
            
        foreach($this->privileges[$this->privilege_id] AS $p => $h)
        {
            if($p == $privilege_name)
                return $h;
        }
            
        return false;
            
    }
    
    //vraci true, pokud ma prihlaseny uzivatel prioritu prava alespon takovou jakou ma zadany parametr $privilege_profile_name
    public function has_min_privilege($privilege_profile_name)
    {
        //zjisteni hodnoty priority pro vstupni parametr neboli hranicni priorita
        $hranicni_priorita = 0;
        foreach($this->privileges AS $idp => $p)
        {
            if($p['nazev'] == $privilege_profile_name)
                $hranicni_priorita = $p['priorita'];
        }

        return $hranicni_priorita <= $this->privilege_priorite;   
        
    }
    
    //vraci true, pokud ma prihlaseny uzivatel prioritu prava maximalne takovou jakou ma zadany parametr $privilege_profile_name
    public function has_max_privilege($privilege_profile_name)
    {
        //zjisteni hodnoty priority pro vstupni parametr neboli hranicni priorita
        $hranicni_priorita = 0;
        foreach($this->privileges AS $idp => $p)
        {
            if($p['nazev'] == $privilege_profile_name)
                $hranicni_priorita = $p['priorita'];
        }

        return $hranicni_priorita >= $this->privilege_priorite;   
        
    }
    
    
    
    
}



class C_User{
	
    //id prihlaseneho uzivatele
    private $id;
    
    //jmeno uzivatele
    private $first_name;
    
    //prijmeni prihlaseneho uzivatele
    private $surname;
    
    //email uzivatele
    private $email;
    
    //povolene domeny
    private $domains;
    
    //povolene moduly
    private $modules;
    
    //povolene jazykove mutace
    private $languages;
        
    //opravneni
    private $privileges;
    
    //detekuje zda se nekdo prihlasuje automaticky pres generovany link
    public $autologin = false;
    
	//konstruktor
	public function __construct(){
		
       $this->autologin = isset($_GET['login']) && isset($_GET['uid']) && isset($_GET['p']);
             
       $this->privileges = new C_Privileges();
       
		if(isset($_POST['btnLogin']) || $this->autologin) 
            $this->login();
		
		if(isset($_SESSION['idUzivatele'])) 
        {
          
			$this->id = $_SESSION['idUzivatele'];
			$this->first_name = $_SESSION['jmeno'];
			$this->surname = $_SESSION['prijmeni'];
			$this->domains = $_SESSION['domeny'];
			$this->modules = $_SESSION['moduly'];
            $this->languages = $_SESSION['jazyky'];
            $this->email = $_SESSION['email'];
            $this->privileges->set_privilege_id($_SESSION['prava']);
		}
		else 
        {
			$this->Reset();
		}


	}
	
    public function getPrivilegeName()
    {
        $p = $this->privileges->get_all_privileges();
        return constant($p[$_SESSION['prava']]['konst_nazev']);
    }
    
    //vraci jmeno a prijmeni uzivatele
    public function getName()
    {
        return trim($this->first_name." ".$this->surname);
    }
    
    public function getEmail()
    {
        return $this->email;
    }
    
    public function getId()
    {
        return $this->id;
    }
    
    public function getPrivileges()
    {
        return $this->privileges->get();
    }
    
    public function getDomains()
    {
        return $this->domains;
    }
    
    public function getModules()
    {
        return $this->modules;
    }
    
    public function getLanguages()
    {
        return $this->languages;
    }
    
	//vynuluje data uzivatele
	public function Reset(){
		$this->id = "";
		$this->first_name = "";
		$this->surname = "";
        $this->email = "";
		$this->privileges = null;
		$this->domains = "";
		$this->modules = array();
        $this->languages = array();
        $this->domains = array();
	}
	
	//prihlasi uzivatele
	private function Login(){
		global $db;
		global $systemMessage;
		global $domain;

        if(!$this->autologin)
        {
    		if(!TestStringLoginEN($_POST['login'], 200, false)) {
    			$systemMessage->add_error(TCHYBNE_HESLO_NEBO_LOGIN);
    			return;
    			}
    		if(!TestStringEN($_POST['pass'], 50, false)) {
    			$systemMessage->add_error(TCHYBNE_HESLO_NEBO_LOGIN);
    			return;
    			}
    
    	
    		//overeni zda se v databazi nachazi zadane heslo a login
    		$record = $db->Query("SELECT * FROM ".TABLE_UZIVATELE." WHERE login='".$db->secureString($_POST['login'])."' /*AND heslo='".sha1($_POST['pass'])."'*/ AND idPrava!=".ID_PRAVA_PRIVATNI." LIMIT 1");
            
            $pass = $_POST['pass'];
                      
            
            
        }
        else
        {
            
             //overeni zda se v databazi nachazi zadane heslo a login
    		$record = $db->Query("SELECT * FROM ".TABLE_UZIVATELE." WHERE idUzivatele='".intval($_GET['uid'])."' AND heslo='".$db->secureString($_GET['p'])."' AND idPrava!=".ID_PRAVA_PRIVATNI); 
            
            $pass = $_GET['p'];
        }
        
        
		$user = $db->getAssoc($record);
		
        //var_dump(password_verify($pass, $user['heslo']));
        
		if(password_verify($pass, $user['heslo']) && $db->numRows($record)==1){
            $this->privileges->set_privilege_id($user['idPrava']);
            
			$prava = $this->privileges->get();

			//pokud je nastaveno v administraci blokovani prihlasovani, prihlaseni neprobehne
			if(BLOCKED_LOGIN && $prava['superadmin']==0) {
				$systemMessage->add_error(TPRIHLASOVANI_UZIVATELU_BYLO_POZASTAVENO_ADMINISTRATOREM);
				return;
				}
			
			//pokud je zaznam v databazi nalazen, overi se zda neni ucet blokovan
			if($user['aktivni']==0)
            {
				$systemMessage->add_error(TTENTO_UCET_BYL_ZABLOKOVAN);
				return;
			}
				
		
			//overeni zda ma uzivatel pridelenu nejakou domenu
			//if($domain->HaveAnyDomain($user['idUzivatele']))echo "ma domenu"; else echo "nema domenu";
			if(!$domain->HaveAnyDomain($user['idUzivatele']) && $prava['admin']==0 && $prava['superadmin']==0)
            {
				$systemMessage->add_error(TUCET_EXISTUJE_NENI_DOMENA_PRO_SPRAVU);
				return;
			}
			                       
			
			$this->id = $_SESSION['idUzivatele'] = $user['idUzivatele'];
			$this->first_name = $_SESSION['jmeno'] = $user['jmeno'];
			$this->surname = $_SESSION['prijmeni'] = $user['prijmeni'];
            $this->email = $_SESSION['email'] = $user['email'];
            $_SESSION['prava'] = $user['idPrava'];
            
			$this->setModules();
            $this->setLanguages();
            $this->setDomains();
            
			//overeni zda ma uzivatel prirazen nejaky modul
            if(count($this->modules) == 0)
            {
                $systemMessage->add_error(TUCET_EXISTUJE_NENI_MODUL_PRO_SPRAVU);
                $this->Logout(false);
				return;
            }
            
            
            if(count($this->languages) == 0)
            {
                
                $systemMessage->add_error(TUCET_EXISTUJE_NENI_JAZYK_PRO_SPRAVU);
                $this->Logout(false);
				return;
            }
            
            if(count($this->domains) == 0)
            {
                $systemMessage->add_error(TUCET_EXISTUJE_NENI_DOMENA_PRO_SPRAVU);
                $this->Logout(false);
				return;
            }
            
			if(!$domain->UserDomains()){
				$domain->refresh($this->domains);
			}
			
			if(!$domain->UserDomains() && !$this->UserPrivilege('admin') && !$domain->IsCentralDomain()) {
				$systemMessage->add_error(TNEMATE_OPRAVNENI_KE_SPRAVE);
				$this->Logout(false);
				return;
				}
			
            
			if($domain->getId() == 0 && $domain->getName() == ''){
				$domain->refresh($this->domains);
				$systemMessage->add_error(TCHYBA_SYSTEMU_CMS);
				return;
				} 

			$db->Query("UPDATE ".TABLE_UZIVATELE." SET posledniPristup=NOW(), spatnePrihlaseni=0 WHERE idUzivatele = ".$_SESSION['idUzivatele']);
		
            $_SESSION['inline_editace'] = $this->autologin;
			
			}
			else
			{
			//pokud neni v databazi login se zadanym heslem, overi se zda se v databazi nachazi alespon login
			$data = $db->Query("SELECT idUzivatele, spatnePrihlaseni, aktivni FROM ".TABLE_UZIVATELE." WHERE login='".$_POST['login']."'");
			$u = $db->getAssoc($data);
			
			//pokud se v databazi naleza login, nastavi se jeden spatny pokus pri prihlaseni
			if($u['aktivni']==0){
				$systemMessage->add_error(TTENTO_UCET_BYL_ZABLOKOVAN);
				return;
				}
				
			if($db->numRows($data)==1)
				$db->Query("UPDATE ".TABLE_UZIVATELE." SET spatnePrihlaseni = spatnePrihlaseni + 1 WHERE idUzivatele=".$u['idUzivatele']);
		
			//pokud jsou vycerpany vsechny pokusy pro prihlaseni je ucet zablokovan
			if(($u['spatnePrihlaseni']+1)==MAX_BAD_LOGIN){
				$db->Query("UPDATE ".TABLE_UZIVATELE." SET aktivni=0 WHERE idUzivatele=".$u['idUzivatele']);
				$systemMessage->add_error(TTENTO_UCET_BYL_ZABLOKOVAN);
				return;
				}
				
			$systemMessage->add_error(TCHYBNE_HESLO_NEBO_LOGIN." ".sprintf(TZBYVA_POKUSU, (MAX_BAD_LOGIN - ($u['spatnePrihlaseni']+1))));	
			} 
	}
	

	//odhlasi prihlaseneho uzivatele
	public function Logout($redirect = true){

        
		unset($_SESSION['idUzivatele']);
		unset($_SESSION['jmeno']);
		unset($_SESSION['prijmeni']);
        unset($_SESSION['email']);
		unset($_SESSION['prava']);
		unset($_SESSION['domeny']);
		unset($_SESSION['aktivniDomena']);
        unset($_SESSION['jazyky']);
        unset($_SESSION['moduly']);
		unset($_SESSION['inline_editace']);
		$this->Reset();

		if($redirect) 
            Redirect(RELATIVE_PATH.ADMIN_SCRIPT."/");
	}

	//overuje zda je nekdo prihlasen
	public function logged(){
		return (bool) $this->id;
	}
	
	//zjisti zda ma uzivatel pridelenu nejakou domenu pro spravu obsahu
	public function hasDomain(){
		return count($this->domains) > 0;
	}
	
	//overi zda ma uzivatel dane pravo
    //vstupem je seznam poli nebo retezcu - nazvu opravneni
    //pokud jsou splneny vsechna prava, pak vraci true, jinak false (pokud neni alespon jedno pravo povoleno)
	public function UserPrivilege()
    {
	   $args = func_get_args();
       $privilege = array();
       foreach($args as $arg)
       {
            $privilege += (array) $arg; 
       }
       
    
        $privilege = (array) $privilege;
        foreach($privilege AS $p)
        {
    		if($this->privileges->get($p)==1 || $this->privileges->get('nazev')==$p) 
    			continue; 
    		
            return false;
        }
        
        return true;
	}
	
    //aktualizuje data prihlaseneho uzivatele
    //pokud uzivatel v administraci uzivatelu ulozi nastaveni sebe sama musi dojit k refreshi objektu user.
	public function reload(){
		global $db;
		global $domain;
		
		
		$data = $db->Query("SELECT * FROM ".TABLE_UZIVATELE." WHERE idUzivatele=".$this->getId());
		if($db->numRows($data)==1){
			$user = $db->getAssoc($data);
			$this->first_name = $_SESSION['jmeno'] = $user['jmeno'];
			$this->surname = $_SESSION['prijmeni'] = $user['prijmeni'];
            $this->email = $_SESSION['email'] = $user['email'];
			$this->setDomains();
            $this->setModules();
            $this->setLanguages();
            
			}

	}
    
    //nastavi pole id modulu, se kterymi ma uzivatel pravo disponovat
    private function setModules()
    {
        global $db;
        $d = $db->query("SELECT idModulu FROM ".TABLE_UZIVATELE_MODULY." WHERE idUzivatele='".$this->getId()."'");
        $result = array();
        while($m = $db->getAssoc($d))
            $result[] = $m['idModulu'];
            
        $this->modules = $_SESSION['moduly'] = $result;
        
    }
    
    //nastavi pole id jazyku, se kterymi ma uzivatel pravo disponovat
    private function setLanguages()
    {
        global $db;
        $d = $db->query("SELECT idJazyka FROM ".TABLE_UZIVATELE_JAZYKY." WHERE idUzivatele='".$this->getId()."'");
        $result = array();
        while($j = $db->getAssoc($d))
            $result[] = $j['idJazyka'];

        $this->languages = $_SESSION['jazyky'] =  $result;
    }
    
    
    //zjisti id domen, ktere ma uzivatel pravo spravovat a vrati retezec id domen ve formatu napr. 1#2#3
	//pokud je parametr idUser 0, pak overi prihlaseneho uzivatele, 
	//pokud je parametr idUser vetsi nez 0 overi v databazi uzivatele s id idUser.
	public function setDomains()
    {
		global $db;
		$data = $db->Query("SELECT idDomeny FROM ".TABLE_UZIVATELE_DOMENY." WHERE idUzivatele='".$this->id."'");
		$result = array();
		if($db->numRows($data)!=0){
			while($d = $db->getAssoc($data)){
				$result[] = $d['idDomeny']; 
				}
			}
		$this->domains = $_SESSION['domeny'] =  $result;
	}
	
	//zjisti zda ma uzivatel alespon zadane pravo podle priority
    public function minPrivilege($privilege_name)
    {
        return $this->privileges->has_min_privilege($privilege_name);
    }
    
    //zjisti zda ma uzivatel maximalne zadane pravo podle priority
    public function maxPrivilege($privilege_name)
    {
        return $this->privileges->has_max_privilege($privilege_name);
    }

	
}
?>