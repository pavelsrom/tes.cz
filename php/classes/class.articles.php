<?php

/**
 * @author Pavel Srom
 * @copyright 2010
 */


class C_Articles{
    
    public $items;
    public $exists;
        
    private $link;
    private $name;
    private $article_id = 0;
    private $rubric_id = 0;
    
    //mozne typy stranka, nahodne-clanky, nejnovejsi-clanky, nejlepsi-clanky, clanek, souvisejici, rubriky
    public function __construct($idBloku=0, $typ='stranka', $idClanku=0){
        global $db;
        global $domain;
        global $links;
        global $private_pages;
        global $pagination;
        
        $this->exists = false;
        
        $this->article_id = intval($idClanku);
        $this->rubric_id = intval($idBloku);
        $this->link = $links->get_url($idBloku);
        
                    
        if($typ == 'clanek' && $idClanku>0){
            $data = $db->Query("SELECT idStranky, nazev AS titulek, perex, idStranky AS odkaz_novinky, obrazek, datumAktualizoval AS aktualizace,
                DATE_FORMAT(datum,'%d.%m.%Y') AS datum, DATE_FORMAT(datum, '%h:%i') AS cas, autor, idRodice AS rodic, obsah
    			FROM ".TABLE_STRANKY." 
    			WHERE idDomeny=".$domain->getId()."
                    AND idJazyka=".WEB_LANG_ID."
                    AND typ='clanek'
    				AND zobrazit=1
    				AND (od <= CURDATE() OR od IS NULL)
    				AND (do >= CURDATE() OR do IS NULL)
                    AND archivovat = 0
                    AND idStranky=".$idClanku."
                LIMIT 1
    		  ");
            if($db->numRows($data)>0)
                $this->exists = true;
            }
        elseif($typ == 'souvisejici' && $idClanku>0){
            $data = $db->Query("SELECT idStranky, nazev AS titulek, perex, idStranky AS odkaz_novinky, obrazek, autor, 
                DATE_FORMAT(datum,'%d.%m.%Y') AS datum, DATE_FORMAT(datum, '%h:%i') AS cas, c.datumAktualizoval AS aktualizace
                FROM ".TABLE_SOUVISEJICI_CLANKY." AS sc 
    			LEFT JOIN ".TABLE_STRANKY." AS c ON c.idStranky=sc.idSouvisejicihoClanku
    			WHERE c.idDomeny=".$domain->getId()."
                    AND c.idJazyka=".WEB_LANG_ID."
                    AND c.typ='clanek'
    				AND c.zobrazit=1
    				AND (c.od <= CURDATE() OR c.od IS NULL)
    				AND (c.do >= CURDATE() OR c.do IS NULL)
                    AND sc.idClanku=".$idClanku."
                GROUP BY sc.idSouvisejicihoClanku
    		  ");
            if($db->numRows($data)>0)
                $this->exists = true;
            $this->name = TSOUVISEJICI_CLANKY;
            }
        elseif($typ == 'rubriky'){
            $data = $db->Query("SELECT idStranky, nazev AS titulek, perex, idStranky AS odkaz_novinky, obrazek, autor, 
                DATE_FORMAT(datum,'%d.%m.%Y') AS datum, DATE_FORMAT(datum, '%h:%i') AS cas, c.datumAktualizoval AS aktualizace
                FROM ".TABLE_STRANKY." AS c
    			WHERE c.idDomeny=".$domain->getId()."
                    AND c.idJazyka=".WEB_LANG_ID."
                    AND c.typ='clanky'
    				AND c.zobrazit=1
    				AND (c.od <= CURDATE() OR c.od IS NULL)
    				AND (c.do >= CURDATE() OR c.do IS NULL)
                ORDER BY idStranky
    		  ");
            if($db->numRows($data)>0)
                $this->exists = true;
            $this->name = TRUBRIKY;
            }
        elseif($typ == 'tagy'){
            $data = $db->Query("SELECT idStranky, nazev AS titulek, perex, obrazek, autor, datum, datumAktualizoval AS aktualizace
                FROM ".TABLE_STRANKY." AS c
    			WHERE c.idDomeny=".$domain->getId()."
                    AND c.idJazyka=".WEB_LANG_ID."
                    AND c.typ='tag'
    				AND c.zobrazit=1
                ORDER BY c.nazev
    		  ");
            if($db->numRows($data)>0)
                $this->exists = true;
            }
        elseif($typ == 'tag'){
            
            $limit = ($pagination->get_current_page() - 1) * intval(POCET_CLANKU_NA_STRANKU).",".intval( 	POCET_CLANKU_NA_STRANKU);

            $data = $db->Query("SELECT SQL_CALC_FOUND_ROWS s.idStranky, s.nazev AS titulek, s.perex, s.idStranky AS odkaz_novinky, s.obrazek, s.autor, COUNT(d.idPolozky) AS pocet_komentaru, r.idStranky AS idRubriky, r.url AS urlRubriky, r.nazev AS nazevRubriky, j.jazyk, s.obsah, s.datumAktualizoval AS aktualizace,
                DATE_FORMAT(s.datum,'%d.%m.%Y') AS datum, DATE_FORMAT(s.datum, '%h:%i') AS cas
                FROM ".TABLE_STRANKY_TAGY." AS t
    			LEFT JOIN ".TABLE_STRANKY." AS s ON s.idStranky = t.idObjektu
                LEFT JOIN ".TABLE_JAZYKY." AS j ON s.idJazyka = j.idJazyka
                LEFT JOIN ".TABLE_STRANKY." AS r ON s.idRodice = r.idStranky
                LEFT JOIN ".TABLE_DISKUZE_POLOZKY." AS d ON s.idStranky=d.idStranky AND d.zobrazit = 1
    			WHERE s.idDomeny=".$domain->getId()."
                    AND s.idJazyka=".WEB_LANG_ID."
    				AND s.zobrazit=1
                    AND t.idTagu=".$idBloku."
                    AND s.typ='clanek'
    				AND (s.od <= CURDATE() OR s.od IS NULL)
    				AND (s.do >= CURDATE() OR s.do IS NULL)
                    AND s.archivovat = 0
                GROUP BY s.idStranky
    			ORDER BY s.datum DESC
                
                LIMIT ".$limit);
                
            if($db->numRows($data)>0)
                $this->exists = true;
            }
        elseif(in_array($typ, array('stranka','nahodne-clanky','nejnovejsi-clanky','nejlepsi-clanky','top-clanky','nejdiskutovanejsi-clanky')))
            {
            
            $sort = "";
            $limit = $where = $having = "";
            
            if($typ=='stranka'){
                $sort = "s.datum DESC, s.idStranky";//"s.datum DESC, s.idStranky";
                $limit = ($pagination->get_current_page() - 1) * intval(POCET_CLANKU_NA_STRANKU).",".intval( 	POCET_CLANKU_NA_STRANKU);

                /*
                vyjimka pekarny racek 

                if($idBloku > 0)
                    $where = "AND s.idRodice=".$idBloku;
                */
                }
            elseif($typ=='nejnovejsi-clanky')
                {
                $sort = "s.datum DESC";
                $limit = intval(POCET_NEJNOVEJSICH_CLANKU_V_BOXU);
                $this->name = TNEJNOVEJSI_CLANKY;
                }
            elseif($typ == 'nejlepsi-clanky')
                {
                $sort = "IF(s.pocetHlasovani > 0,s.pocetHlasu/s.pocetHlasovani,0) DESC, s.idStranky DESC";
                $limit = intval(POCET_NEJLEPSICH_CLANKU_V_BOXU);
                $this->name = TNEJLEPSI_CLANKY;
                }
            elseif($typ == 'top-clanky')
                {
                $sort = "s.datum DESC";
                $where .= "AND s.doMenu = 1";
                $limit = intval(POCET_TOP_CLANKU_V_BOXU);
                $this->name = TTOP_CLANKY;
                }
            elseif($typ == 'nejdiskutovanejsi-clanky')
                {
                $sort = "pocet_komentaru DESC";
                //$having .= "pocet_komentaru > 0";
                $having = "HAVING pocet_komentaru > 0";
                $limit = intval(POCET_NEJDISKUTOVANEJSICH_CLANKU_V_BOXU);
                $this->name = TNEJDISKUTOVANEJSI_CLANKY;
                }
            else{
                $sort = "RAND()";
                $limit = intval(POCET_NAHODNYCH_CLANKU_V_BOXU);
                $this->name = TNAHODNE_CLANKY;
                }
            
            //zjisteni privatnich stranek
            if(MODULE_PRIVATE_PAGES)
            {
                
                
                $d = $db->query("SELECT idStranky AS id
                    FROM ".TABLE_STRANKY." AS s
                    WHERE zobrazit=1 
                        AND typ='clanek' 
                        AND s.idDomeny=".$domain->getId()."
                        AND s.idJazyka=".WEB_LANG_ID."
                        AND (s.od <= CURDATE() OR s.od IS NULL)
    				    AND (s.do >= CURDATE() OR s.do IS NULL)
                        AND s.archivovat = 0
                        ".$where."
                        ");
                        
                $zakazane_stranky_id = array();
                while($s = $db->getObject($d))
                    if(!$private_pages->has_access($s->id))
                        $zakazane_stranky_id[] = $s->id; 
                
                $db->free($d);
                        
                $where .= count($zakazane_stranky_id) > 0 ? " AND s.idStranky NOT IN (".implode(",",$zakazane_stranky_id).")" : ""; 
                
                        
            }
            
            
            $data = $db->Query("SELECT SQL_CALC_FOUND_ROWS s.idStranky, s.nazev AS titulek, s.perex, s.idStranky AS odkaz_novinky, s.obrazek, s.autor, COUNT(d.idPolozky) AS pocet_komentaru, r.idStranky AS idRubriky, r.url AS urlRubriky, r.nazev AS nazevRubriky, j.jazyk, s.obsah, IF(TRIM(s.obsah) = '', 0, 1) zobrazit_odkaz, s.datumAktualizoval AS aktualizace, s.doMenu,
                DATE_FORMAT(s.datum,'%d.%m.%Y') AS datum, DATE_FORMAT(s.datum, '%h:%i') AS cas
    			FROM ".TABLE_STRANKY." AS s
                LEFT JOIN ".TABLE_JAZYKY." AS j ON s.idJazyka = j.idJazyka
                LEFT JOIN ".TABLE_STRANKY." AS r ON s.idRodice = r.idStranky
                LEFT JOIN ".TABLE_DISKUZE_POLOZKY." AS d ON s.idStranky=d.idStranky AND d.zobrazit = 1
    			WHERE s.idDomeny=".$domain->getId()."
                    AND s.idJazyka=".WEB_LANG_ID."
                    AND s.typ='clanek'
                    AND r.typ='clanky'
    				AND s.zobrazit=1
    				AND (s.od <= CURDATE() OR s.od IS NULL)
    				AND (s.do >= CURDATE() OR s.do IS NULL)
                    ".$where."
                    AND s.archivovat = 0
                GROUP BY s.idStranky
                ".$having."
    			ORDER BY ".$sort."
                LIMIT ".$limit."
    		  ");

            if($db->numRows($data)>0)
                $this->exists = true;
            };
                        
        $celkem_data = $db->Query("SELECT FOUND_ROWS()");
        $celkem_arr = $db->getRow($celkem_data);
        $pagination->set_number_items( intval($celkem_arr[0]) );
        
        $i = 0;
        while($a = $db->getAssoc($data)){
            
            $aktualizace = "?".str_replace(array(" ",":", "-"), "", $a['aktualizace']);

            if($a['obrazek']!= '' && file_exists(PRE_PATH.$domain->getDir().USER_DIRNAME_ARTICLES_MINI.$a['obrazek'] )){
				$img = $domain->getDir().USER_DIRNAME_ARTICLES_MINI.$a['obrazek'];				
				}
				else 
                $img = '';
		
            if($a['obrazek']!= '' && file_exists(PRE_PATH.$domain->getDir().USER_DIRNAME_ARTICLES_MAXI.$a['obrazek'] )){
				$img_maxi = $domain->getDir().USER_DIRNAME_ARTICLES_MAXI.$a['obrazek'];				
				}
				else 
                $img_maxi = '';
			
            if($a['obrazek']!= '' && file_exists(PRE_PATH.$domain->getDir().USER_DIRNAME_ARTICLES_BOX.$a['obrazek'] )){
				$img_box = $domain->getDir().USER_DIRNAME_ARTICLES_BOX.$a['obrazek'];				
				}
				else 
                $img_box = '';		
						
			$link = $links->get_url($a['idStranky']);
								
			$linkname = $a['titulek'];
            
            $this->items[$i] = $a;
            $this->items[$i]['odkaz'] = $link;
			$this->items[$i]['nazev_odkazu'] = $linkname;
            $this->items[$i]['obrazek'] = $a['obrazek'];
            $this->items[$i]['obrazek_path'] = $img.$aktualizace;
            $this->items[$i]['obrazek_path_maxi'] = $img_maxi.$aktualizace;
            $this->items[$i]['obrazek_path_box'] = $img_box.$aktualizace;
            
            $this->items[$i]['id'] = $a['idStranky'];
            if(isset($a['nazevRubriky']))
                $this->items[$i]['nazev_rubriky'] = $a['nazevRubriky'];
            if(isset($a['urlRubriky']))
                $this->items[$i]['url_rubriky'] = $links->get_url($a['idRubriky']);
            if(isset($a['pocet_komentaru']))
                $this->items[$i]['pocet_komentaru'] = intval($a['pocet_komentaru']);
            $i++;
            }
            
        $db->free($data);
            
        
        
    }
    
    public function GetAllTags()
    {
        global $links;
        $html = "";
        
        if(count($this->items)==0) return $html; 
        
        foreach($this->items AS $pol)
        {
            echo "<a href='".$links->get_url($pol['idStranky'])."'>".$pol['titulek']."</a>";
        }
        
        return $html;
        
    }
    
    public function GetArticlesOnPage(){
        global $domain;
        global $pagination;
        
		$html = "";
		
        
        if(count($this->items)==0) return $html; 
        
		$pocet = intval(POCET_CLANKU_NA_STRANKU);
		$pagination->set_number_items_on_page($pocet);
        
        
        $session = 'clanky';
		$stranka = $pagination->get_current_page();
		$od = ($stranka-1) * $pocet;
		
		
		$html .= "<span id='".$session."'></span>";

        $i = 0;
        
        $html .= '<ul class="news-list articles-list">';
        
        

        foreach($this->items AS $id => $pol){

                   
            $html .= '<li class="news-item">
						<a href="'.$pol['odkaz'].'" title="'.TCIST_VICE.'">
							<div class="news-img">
								<img src="'.$pol['obrazek_path'].'" alt="">
							</div>
							<h2>'.$pol['titulek'].'</h2>
							<p>'.strip_tags($pol['perex']).'</p>
						</a>
					</li>';

            


               /* 
            $html .= '<div class="blog-prispevek">
    		<a href="'.$pol['odkaz'].'" title="'.TCIST_VICE.'"><img src="'.$pol['obrazek_path'].'" alt="'.$pol['titulek'].'"></a>
    		<div class="blog-text">
    			<h2>
    				<a href="'.$pol['odkaz'].'">'.$pol['titulek'].'</a>
    			</h2>
    			<p class="datum">'.$pol['datum'].'</p>
    			<p>
    				'.strip_tags($pol['perex']).'
    			</p>
    		</div>
    		<div class="cist-vice">
    			<a href="'.$pol['odkaz'].'" class="btn">'.TCIST_VICE.'</a>
    		</div>
    	</div>
';
                
*/
            
                        
			/*
			if($pol['obrazek']!='')
                $html .= '
                    <a href="'.$pol['odkaz'].'" title="'.$pol['titulek'].'" title="'.TCIST_VICE.'">
                        <img alt="'.$pol['titulek'].'" src="'.$pol['obrazek_path'].'"/>
                    </a>
                ';
            
            if($pol['zobrazit_odkaz'])
			     $html .= '<h3><a href="'.$pol['odkaz'].'" title="'.TCIST_VICE.'">'.secureString($pol['titulek']).'</a></h3>';
                 else
                 $html .= '<h3>'.secureString($pol['titulek']).'</h3>';
            
            //$html .= "<span class='datum'>".$pol['datum']./*$pol['nazev_rubriky'].*/"</span>";
            /*	
            $html .= '<div class="produktText">';
            $html .= '<h2>'.$pol['titulek'].'</h2>';
            $html .= editable($pol['id'], $pol['obsah'], 'content');
            $html .= '</div>';    		
            */
            /*
            if($pol['odkaz']!='' && $pol['zobrazit_odkaz'])
			  $html .= '<a class="button" href="'.$pol['odkaz'].'" title="'.TCIST_VICE.'">'.TCIST_VICE.'</a>';
            */
			//$html .= "<div class='cleaner'></div>";
            
            //$html .= "</div>";
            

			$i++;
			}
		
        
        $html .= '</ul>';
       
		//strankovani
		$html .= $pagination->set_url_tail("#clanky")->get_html();
		
        
        /*
        if(ZOBRAZIT_RSS_CLANKY==1 && file_exists(PRE_PATH.$domain->getDir()."rss.xml")){
				$html .= "<div class='rss'>";
                
				$html .= "<a href='".$domain->getUrl()."/rss.xml'><img src='img/rss.png' alt='RSS'/></a>";
				$html .= "<a href='".$domain->getUrl()."/rss.xml'>".TODBER_CLANKU_PRES_RSS."</a>";
				$html .= "</div>";
			     }
        
        */
        
		return $html;
        
        
        
        
    }
    
    public function GetArticlesBlock(){
        global $domain;	
		
        $html = "";
        
        if(count($this->items) == 0)
            return $html;
        
        $html .= '<h4>'.$this->name.'</h4>';
        $html .= '<ul>';
        foreach($this->items AS $idc => $r)
        {
            $html .= '<li><a href="'.$r['odkaz'].'" class="odkaz" title="'.TCIST_VICE.'">'.$r['titulek'].'</a></li>';
        }
        $html .= '</ul>';
        
        //$html .=  '<a class="vsechny_reference" href="'.$this->get_url_rubric().'" title="Zobrazit více referencí"></a>';
        

        return $html;
        
        
		
		$html = "";
        $html .= "<h3>".$this->name."</h3>";
		$html .= "<ul>";
        	
		foreach($this->items AS $i){	
			/*
			if($i['obrazek']!='')
				$img = "<img src='".$i['obrazek']."' title='".$i['titulek']."' alt=''/>";
				else
				$img = "";
		
			if($i['odkaz']!='')
				if($img!='') $img = "<a href='".$i['odkaz']."'>".$img."</a>";

			if($img!='')
				$html .= "<div class='news-image'>".$img."</div>";
			*/
	
			
			if($i['odkaz']!='')
				$html .= "<li><a href='".$i['odkaz']."'>".secureString($i['titulek'])."</a></li>";
				else
				$html .= "<li>".secureString($i['titulek'])."</li>";
			
		    /*
			if($i['odkaz']!=''){
				$odkaz = "<span class='news-detail'><a href='".$i['odkaz']."'>Číst více...</a></span>";
				}
				else
				$odkaz = '';
            
		
		  if(trim($datum)!=''){
				$html .= "<span class='news-date'>";
				$html .= $datum;
				$html .= "</span> ";
				}
			$html .= strip_tags($i['perex'])." ".$odkaz;
            */
			}

        $html .= "</ul>";

			if($this->link!=''){
				$html .= "<div class='all-news-link'>";
				$html .= "<a href='".$this->link."'>".TZOBRAZIT_VSECHNY_CLANKY."</a>";
				$html .= "</div>";
				}

		return $html;
        
        
        
        
    }

    private function get_photos()
    {
        global $db;
        global $domain;
        
        $data = $db->query("SELECT id, soubor, popis FROM ".TABLE_STRANKY_FOTKY." WHERE idStranky=".$this->article_id." ORDER BY priorita");
        
        $fotky = array();
        
        while($f = $db->getAssoc($data))
        {
            $fotky[$f['id']] = $f;
            $fotky[$f['id']]['obrazek_path'] = $domain->getDir().USER_DIRNAME_STRANKY_MINI.$f['soubor'];
            $fotky[$f['id']]['obrazek_path_maxi'] = $domain->getDir().USER_DIRNAME_STRANKY_MAXI.$f['soubor'];
        }    
         
        return $fotky; 
        
        
    }
    
    public function GetArticle(){
        global $links;

        $html = "";
        
        
        

        if(count($this->items)==0 || !$this->exists) return $html;
        
        $clanek = $this->items[0];
        $fotky = $this->get_photos();

        $html .= editable($clanek['id'], $clanek['obsah'], "content");

        if(count($fotky) > 0)
        {
            $html .= '<ul class="gallery-list">';
            foreach($fotky AS $f)
            {

                $html .= '<li>
                <a href="'.$f['obrazek_path_maxi'].'" class="gallery-item" data-title="'.$clanek['titulek'].'">
                    <img src="'.$f['obrazek_path'].'" alt="" class="gallery-img">
                </a>
            </li>';

            }

            $html .= '</ul>';

        }
        else 
        {
            
        }

        
        $html .= '
            <p>
                <a href="'.$links->get_url($clanek['rodic']).'" class="btn btn-big">'.TZPET_NA_PREHLED.'</a>
            </p>
        ';

        /*
        $html .= "<div class='block-news'>";

		$i = 0;

		foreach($this->items AS $id => $pol){

			//if($pol['poradi']<$od) continue;
			//if($pocet==$i) break;
			
			$html .= "<div class='news'>";
			
			$html .= "<div class='news-header'>";
			
			if($pol['datum']!='')
				$datum = $pol['datum'];
				else
				$datum = "";
            
			$html .= "<span class='news-date'>".$datum."</span>";
			
			$html .= "<span class='news-name'>";
			if($pol['odkaz']!='')
				$html .= "<a href='".$pol['odkaz']."'>".$pol['titulek']."</a>";
				else
				$html .= $pol['titulek'];
				
			$html .= "</span>";
			
			$html .= "</div>";
			
			$html .= "<div class='news-text'>";
			
			if($pol['obrazek']!='')
				if($pol['odkaz']!='')
					$html .= "<div class='news-image'><a href='".$pol['odkaz']."'><img src='".$pol['obrazek']."' alt='".$pol['titulek']."' /></a></div>";
					else
					$html .= "<div class='news-image'><img src='".$pol['obrazek']."' alt='".$pol['titulek']."' /></div>";
					
			$html .= $pol['perex'];
			$html .= "</div>";
			
			$html .= "<div>";
			$html .= "</div>";
			$html .= "<div class='cleaner'></div>";
			$html .= "</div>";
			$i++;
			}
		
		$html .= "</div>";
        */


        return $html;
        
    }
    
    public function GetArticleRate(){
        global $page;
        global $domain;
        
        $html = "";

        if(ZOBRAZIT_HODNOCENI_CLANKU==0) return $html;
        
        if($page->get('hlasovani') > 0)
            $avg = $page->get('hlasy') / $page->get('hlasovani');
            else 
            $avg = 0;
        
        $html .= TOHODNOTTE_CLANEK;
        $html .= "<div id='container'>";
        $html .= "<form action='' method='post' id='hodnoceni-clanku'>";
        $html .= "<p>";
        $pocet_polozek = ROZSIRENA_STUPNICE_HODNOCENI_CLANKU==0?5:10;
        
        for($i=1; $i<=$pocet_polozek; $i++){
            if($avg >= $i && $avg < ($i + 1)) $ch = "checked='checked'"; else $ch = "";
            $html .= "<input type='radio' name='rate' value='".$i."' title='".$i."' ".$ch." />";
            }
        
        $html .= "<input type='submit' value='Hlasuj' />";
        $html .= "</p>";
        $html .= "</form>";

        $html .= "<div id='loader'><div style='padding-top: 5px;'>".TPROSIM_CEKEJTE."</div></div>";
        $html .= "</div>";
        $cookie = 'hlasovani-clanek-'.$page->get_id();
        
        
        if(isset($_COOKIE[$cookie])) 
            $disabled_votting = "disabled: true,"; 
            else 
            $disabled_votting = "";
        
        $html .= '
        <script type="text/javascript">
        $(function(){
			$("#hodnoceni-clanku").children().not(":radio").hide();
			
			// Create stars
			$("#hodnoceni-clanku").stars({
				cancelShow: false,
                oneVoteOnly: true,
                <?php echo $disabled_votting; ?>
                //split: 2,
				callback: function(ui, type, value)
				{
					var arr = ui.$stars.find("a");

					arr.slowEach(100, 
						function(){ $(this).animate({top: "28px"}, 300) }, 
						function()
						{
							$("#loader").fadeIn(function()
							{
							    var id_clanku = "'.$pages->get_id().'"; 
								$.post("'.AJAX_GATEWAY.'articles_voting", {rate: value, idClanku: id_clanku}, function(data)
								{
									//ui.select(Math.round(db.avg));
									$("#prumer-hlasovani").text(data.prumer);
									//$("#votes").text(data.pocet_hlasu);
                                    $("#hodnoceni-clanku").stars("select", data.skupina);
                                    
                                    //ulozeni cookies, aby nedoslo k vicenasobnemu hlasovani
                                    var datum = new Date();
                                    datum.setTime(datum.getTime() + 30*24*60*60*1000);
                                    var c = "hlasovani-clanek-" + id_clanku + "=1; expires=" + datum.toGMTString();
                                    document.cookie = c;
                                    
									$("#loader").fadeOut(function(){ 
										arr.slowEach(100, function(){ $(this).animate({top: 0}, 300) });
									});

								}, "json");
							});
						});
				}
			});
		});
        
        </script>';
        
        
        
        //$html .= "<br /><br />";
        $html .= THODNOCENI_CLANKU." <span id='prumer-hlasovani'>".Round($avg,1)."</span> / ".$pocet_polozek;
       
        return $html;
    }
    
        
    
    public function GetRelatedArticles(){
        global $links;
        global $private_pages;
        
        $html = "";
        
        
        $vypis_sc = array();
        
        //souvisejici clanky zadane rucne
        if(isset($this->items))
        {
            foreach($this->items AS $id => $pol)
            {
                $vypis_sc[$pol['idStranky']] = $pol['idStranky']; 
            }
        }
        
        
        //souvisejici clanky automaticky generovane pres tagy
        $tagy = new C_Tags($this->article_id,'stranka',true,true);
        $tagy->set_lang(WEB_LANG_ID);
        $id_souvisejicich_clanku = $tagy->get_related_objects();
        
        
        foreach($id_souvisejicich_clanku AS $id => $pol)
        {
            
                
            $vypis_sc[$pol['id']] = $pol['nazev'];
            
        }
        
        
        if(count($vypis_sc)==0 || ZOBRAZOVAT_SOUVISEJICI_CLANKY == 0) 
            return $html;
        
        
        $html .= "<h3>".TSOUVISEJICI_CLANKY."</h3>";
        $html .= "<div class='related-articles'>";
        
        $souvisejici = array();
        $i = 0;
        foreach($vypis_sc AS $id => $nazev)
        {
            if($i >= POCET_SOUVISEJICICH_CLANKU) 
                break;
            
            if($private_pages->has_access($id))
            {    
                $souvisejici[] = "<a href='".$links->get_url($id)."?tag=related'>".$nazev."</a>";    			  
                $i++;
            }
            
		}
        
        $html .= implode("<br />", $souvisejici);
        
        $html .= "</div>";
        
        return $html;
    }
    
    
    //vraci data clanku
    public function get_data()
    {
        return $this->items;
    }
    
    //vraci nazev bloku
    public function get_name()
    {
        return $this->name;
    }
    
    //vraci url rubriky 
    public function get_url_rubric()
    {
        return $this->link;
    }
    
    //vraci id rubriky
    public function get_rubric_id()
    {
        return $this->rubric_id;
    }
    
    
}




?>