<?php

/**
 * @author Pavel Srom
 * @copyright 2014
 */



class C_PrivatePages{
    
    private $is_logged = false;
    
    private $user_id = 0;
    
    private $user_id_ref = 0;
    
    private $user_hash = "";
    
    private $user_privilege = 0;
    
    private $user_name = "";
    
    private $user_groups = array();
    
    private $user_langs = array();
    
    private $user_domains = array();
    
    private $session_name = 'private_user';

    private $page_groups = array();
    
    private $lang_id = 0;
    
    private $domain_id = 0;

    private $velkoobchod = 0;

    //private $page_id = 0;
    
    private $all_pages = array();
    
    //indikuje zda je uzivatel prihlasen nebo se jedna o uzivatele nove registrovaneho, ktery musi nejdrive predplatit a az pak mu bude zpristupnen profil
    private $user_type = "";
    
    //uzivatelska data z registrace
    private $user_data = array();

    public function __construct($domain_id)
    {
        
        
        
        $this->is_logged = isset($_SESSION[$this->session_name]);
        
     
        
        $this->lang_id = WEB_LANG_ID;
        $this->domain_id = $domain_id;
        //$this->set_page_id($page_id);
        $this->set_pages();
        $this->set_user();
        
    }
    
    public function set_page_groups($page_groups)
    {
        $this->page_groups = $page_groups;
    }

    //vraci informaci o prihlasenem uzivateli nebo vsechna data o nem
    public function get($db_cols = "")
    {
        if(!$this->is_logged())
            return false;

        if(count($this->user_data) == 0)
            $this->load_user_data();

        if($db_cols == "")
            return $this->user_data;

        if(isset($this->user_data[$db_cols]))
            return $this->user_data[$db_cols];

        return false;
        
    }

    //nacte veskera data o prihlasenem uzivateli
    private function load_user_data()
    {
        global $db;
        $data = $db->query("SELECT * FROM ".TABLE_UZIVATELE." WHERE idUzivatele = ".$this->user_id." LIMIT 1");

        $this->user_data = $db->getAssoc($data);


    }

    //updatuje informace prihlaseneho uzivatele
    public function update($insert)
    {
        global $db;

        if($this->is_logged())
            $db->update(TABLE_UZIVATELE, $insert, "idUzivatele=".$this->user_id);

        return $this->is_logged();

    }
     
    
    private function set_user()
    {
        global $db;
        global $links;
        
        
        if($this->is_logged)
        {
            
            
            $this->user_id = $_SESSION[$this->session_name]["id"];
            $this->user_name = $_SESSION[$this->session_name]["name"];
            $this->user_hash = $_SESSION[$this->session_name]["hash"];
            $this->user_privilege = $_SESSION[$this->session_name]["privilege"];
            $this->user_groups = $_SESSION[$this->session_name]["groups"];
            $this->user_langs = $_SESSION[$this->session_name]["langs"];
            $this->user_domains = $_SESSION[$this->session_name]["domains"];           
            $this->velkoobchod = isset($_SESSION[$this->session_name]["velkoobchod"]) ? $_SESSION[$this->session_name]["velkoobchod"] : 0; 
            
        }
        
    } 

    //indikuje zda je o velkoobchodniho zakaznika
    public function is_vo()
    {
        return $this->velkoobchod;
    }

    //indikuje zda je o maloobchodniho zakaznika
    public function is_mo()
    {
        return !$this->is_vo();
    }
    
    
    //aktivuje ucet uzivatele
    public function user_activate($user_id = 0)
    {
        global $db;
        
        $user_id = $user_id > 0 ? $user_id : $this->get_user_id();
        
        //pokud se uzivatel nove registruje a zaplatil za sluzbu, pak se mu ucet aktivuje
        $db->update(TABLE_UZIVATELE, array("aktivni" => 1), "idUzivatele=".$user_id);
            
    }
    
    //aktivuje ucet uzivatele
    public function user_deactivate($user_id = 0)
    {
        global $db;
        
        $user_id = $user_id > 0 ? $user_id : $this->get_user_id();
        
        //pokud se uzivatel nove registruje a zaplatil za sluzbu, pak se mu ucet aktivuje
        $db->update(TABLE_UZIVATELE, array("aktivni" => 0), "idUzivatele=".$this->get_user_id());
            
    }
    
    
    public function get_user_type()
    {
        return $this->user_type;
    }
    
    public function get_user_name()
    {
        return $this->user_name;
    }
    
    public function get_user_id()
    {
        return $this->user_id;
    }
    
    public function get_user_hash()
    {
        return $this->user_hash;
    }
    
    
    public function is_logged()
    {
        return $this->is_logged && $this->user_id > 0;
    }
    
    public function login($user_name, $password, $is_admin = false)
    {
        global $db;
        global $userSystemMessage;
        
        $userSystemMessage->set_form_id("login");
        $lang_id = $this->lang_id;
        $domain_id = $this->domain_id;
        
        //pokud se admin prihlasuje k nejakemu uctu pres redakcni system, pak nezalezi jestli je dany uzivatel aktivni nebo ne
        $where_aktivni = $is_admin ? "" : " AND u.aktivni = 1"; 
        
        
        $d = $db->query("SELECT *, u.idUzivatele AS id, TRIM(CONCAT(u.jmeno,' ', u.prijmeni)) AS jmeno, idPrava AS opravneni, heslo, hash
            FROM ".TABLE_UZIVATELE." AS u
            WHERE 
                u.login = '".$db->secureString($user_name)."'
                AND idPrava IN (2,3,4,5,10)
                ".$where_aktivni."
            GROUP BY u.idUzivatele
            LIMIT 1
            ");
        
                            
        
        if($db->numRows($d) > 0)
        {
            $user = $db->getObject($d);
            
            //overeni hesla
                       
            if(!$is_admin)
            {
                if(!password_verify($password, $user->heslo))
                {
                    $userSystemMessage->add_error(TCHYBNE_PRIHLASOVACI_UDAJE);   
                    return false;
                }
            }
            else
            {
                //prihlaseni z administrace
                if($password != $user->heslo)
                {
                    $userSystemMessage->add_error(TCHYBNE_PRIHLASOVACI_UDAJE);   
                    return false;
                } 
            }
            
            $dj = $db->query("SELECT idJazyka AS id FROM ".TABLE_UZIVATELE_JAZYKY." WHERE idUzivatele=".$user->id);
            $langs = array();
            while($j = $db->getObject($dj))
                $langs[] = $j->id;
                
            $dd = $db->query("SELECT idDomeny AS id FROM ".TABLE_UZIVATELE_DOMENY." WHERE idUzivatele=".$user->id);
            $domains = array();
            while($j = $db->getObject($dd))
                $domains[] = $j->id;
            
            $dd = $db->query("SELECT idSkupiny AS id FROM ".TABLE_UZIVATELE_SKUPINY_RELACE." WHERE idUzivatele=".$user->id);
            $groups = array();
            while($j = $db->getObject($dd))
                $groups[] = $j->id;
            
            
            
            
            if(in_array($lang_id, $langs) && in_array($domain_id, $domains))
            {
                
                $db->Query("UPDATE ".TABLE_UZIVATELE." SET posledniPristupWeb=NOW(), spatnePrihlaseni=0 WHERE idUzivatele = ".$user->id);
		
                
                $_SESSION[$this->session_name]["id"] = $user->id;
                $_SESSION[$this->session_name]["hash"] = $user->hash;
                $_SESSION[$this->session_name]["name"] = $user->jmeno;
                $_SESSION[$this->session_name]["privilege"] = $user->opravneni;
                $_SESSION[$this->session_name]["groups"] = $groups;
                $_SESSION[$this->session_name]["langs"] = $langs;
                $_SESSION[$this->session_name]["domains"] = $domains;
                $_SESSION[$this->session_name]["velkoobchod"] = $user->velkoobchod;
                $this->is_logged = true;
                $this->set_user();
                
                
                return true;
            }
            else
            return false;

            
        }
        else
        {
            $userSystemMessage->add_error(TCHYBNE_PRIHLASOVACI_UDAJE);   
            return false;
        }
        
    }
    
    
    public function set_pages()
    {
        global $db;    
        global $domain;
        
        $this->all_pages = array();
        
        $data = $db->query("SELECT idStranky AS id FROM ".TABLE_STRANKY." WHERE idDomeny=".$domain->getId()." AND idJazyka=".WEB_LANG_ID." AND zobrazit=1");
        
        while($p = $db->getObject($data))
        { 
            $page_id = $p->id;
            $this->set_all_pages($page_id,$page_id);
            $this->all_pages[$page_id]["tree"] = array_reverse($this->all_pages[$page_id]["tree"], true);
        }
    

        $this->set_private_groups();
        //print_r($this->all_pages);
        
        
    }
    
    private function set_all_pages($page_id, $parent_id = 0)
    {
        global $db;

        if($parent_id == 0)
                return;

        $p = $db->get(TABLE_STRANKY,array("autentizace","idRodice"),"idStranky=".$parent_id);

        if($p !== false)
        {
            if($parent_id == $page_id)
                $this->all_pages[$page_id]["tree"] = array();
                       
            $this->all_pages[$page_id]["tree"][$parent_id] = array(
                "id" => $parent_id, 
                "private" => $p->autentizace, 
                "groups" => array()
                );
            
            if($parent_id == 0)
                return;
                else
                $this->set_all_pages($page_id, $p->idRodice);
        }

        
        return;                 
        
    } 
    
    
    private function set_private_groups()
    {
        global $db;
        
        $d = $db->query("SELECT idStranky, idSkupiny FROM ".TABLE_STRANKY_SKUPINY."");
        
        $skupiny = array();
        while($s = $db->getObject($d))
        {
            $ids = $s->idStranky;
            $idsk= $s->idSkupiny;
            
            if(!isset($skupiny[$ids]))
                $skupiny[$ids] = array();
            
            $skupiny[$ids][$idsk] = $idsk;
            
            
        };

        
        foreach($this->all_pages AS $page_id => $p)
        {
            
            foreach($p["tree"] AS $parent_id => $parent)
            {
                
                $this->all_pages[$page_id]["tree"][$parent_id]["groups"] = isset($skupiny[$parent_id]) ? $skupiny[$parent_id] : array();
                
                
            }
            
            
            
        }
        
        
    }
    
    public function is_private($page_id)
    {
        $is_private = false;
        
        if(!MODULE_PRIVATE_PAGES || !isset($this->all_pages[$page_id]))
            return $is_private;
        
        foreach($this->all_pages[$page_id]["tree"] AS $ids => $p)
        {
            if($p['private'] == 1)
                return true;
        }
                
    }
    
    //vraci zdedene privatni skupiny
    public function get_page_groups($page_id)
    {
        $groups = array();
        
        if(!isset($this->all_pages[$page_id]))
            return $groups;
        
        foreach($this->all_pages[$page_id]["tree"] AS $ids => $p)
        {
            if($p['private'] == 1)
                foreach($p['groups'] AS $idg)
                    $groups[$idg] = $idg;
        }
        
        return $groups;
    }
    
    
    public function has_private_page_access($page_id)
    {
                   
   
        if(!$this->is_logged)
            return false;

        $groups = $this->get_page_groups($page_id);
        
        foreach($this->user_groups AS $ids)
        {
            if(in_array($ids, $groups))
                return true;
        }
        
        return false;
        
    }
    
    //vraci true, pokud ma uzivatel pristup k dane skupine
    public function has_access_user_to_group($group_id)
    {
        
        return in_array($group_id, $this->user_groups);
        
    }
    
    public function has_access($page_id)
    {
        
        //test aktivniho modulu - pokud modul neni aktivni, pak ke strance je pristup vzdy
        if(!MODULE_PRIVATE_PAGES)
            return true;
        
        if(!$this->is_private($page_id))
            return true;
            
        //test opravneni uzivatele - superadmin, admin, editor se dostanou vsude
        /*
        if(in_array($this->user_privilege, array(2,3,4,5)))
            return true;
        */
            
        //test - overeni pristupu k privatni strance
        if(!$this->has_private_page_access($page_id))
            return false;
        
        
        
        //test - overeni pristupu k domene
        if(!in_array($this->domain_id, $this->user_domains) && $this->user_privilege == ID_PRAVA_PRIVATNI)
            return false;
        
        
        //test - overeni pristupu k jazykove mutaci
        if(!in_array($this->lang_id, $this->user_langs) && $this->user_privilege == ID_PRAVA_PRIVATNI)
            return false;
        
        
        return true;
        
        
        
    }
    
    
    public function send_forgot_password($username)
    {
        global $userSystemMessage;
        global $db;


    	$d = $db->query("SELECT idUzivatele AS id, email, login FROM ".TABLE_UZIVATELE." WHERE login='".$db->secureString($username)."' AND aktivni = 1 and email != '' LIMIT 1");
    
        /*
        $text_lang = unserialize(TEXT_EMAILU_PRO_NOVE_HESLO);
        $text = $text_lang[WEB_LANG_ID];
        */
        $text = TEXT_EMAILU_PRO_NOVE_HESLO;

        $odeslano = false;
        $userSystemMessage->set_form_id("login");
        
		if ($db->numRows($d)==1)
		{
			$row=$db->getObject($d);
			if ($row->email<>'')
			{
				$email=$row->email;
				$id_admin=$row->id;
            
                //generovani noveho hesla
                $heslo = password_gen(8);
				$update['heslo']= password_hash($heslo, PASSWORD_DEFAULT);
		
				$db->update(TABLE_UZIVATELE, $update, "idUzivatele='".$id_admin."'");
				$predmet = TNOVE_HESLO_PRO_UZIVATELE." ".$username;
                $text = str_replace(array("[heslo]","[login]"),array($heslo,$row->login),$text);
                $odeslano = SendMail(EMAIL_WEBU,NAZEV_WEBU,$email,$predmet,$text);
                
                $userSystemMessage->add_ok(TNOVE_HESLO_VAM_BYLO_ZASLANO_NA_EMAIL." ".$email);
                
			}
		}
        else
        $userSystemMessage->add_error(TUZIVATELSKE_JMENO_NEEXISTUJE);
        
        
        return $odeslano;
        
        
    }
    
    public function change_password($old_pass, $new_pass1, $new_pass2)
    {
        global $db;
        global $userSystemMessage;
        
        
        $odeslano = false;
        
        $old = ($old_pass);
        $new1= ($new_pass1);
        $new2= ($new_pass2);
        $heslo = $new1;
        
        /*
        $text_lang = unserialize(TEXT_EMAILU_PRO_ZMENU_HESLA);
        $text = $text_lang[WEB_LANG_ID];
        */

        //dd(TEXT_EMAILU_PRO_ZMENU_HESLA);
        $text = TEXT_EMAILU_PRO_ZMENU_HESLA;

        
        $user = $db->get(TABLE_UZIVATELE,array("heslo","email","login"),"idUzivatele=".$this->user_id." AND aktivni = 1 ");
        
        /*
        $d = $db->query("SELECT idUzivatele AS id, email, login 
            FROM ".TABLE_UZIVATELE." 
            WHERE heslo='".password_hash($old, PASSWORD_DEFAULT)."' 
                AND idUzivatele='".$this->user_id."'
                AND aktivni = 1 
            LIMIT 1");
        */
        
        if(trim($old) == "")
            $error = TVYPLNTE_DOSAVADNI_HESLO;
        elseif($new1 != $new2)
            $error = TNOVE_HESLO_NESOUHLASI;
        elseif($new1 == "" || $new2 == "")
            $error = TNOVE_HESLO_NESMI_BYT_PRAZDNE;
        elseif(!password_verify($old,$user->heslo))
    	   $error = TDOSAVADNI_HESLO_NESOUHLASI;
        
        $userSystemMessage->set_form_id("login");
            
        if($error == "")
        {
            /*
            $row=$db->getObject($user);
            $email = $row->email;
            $jmeno = $row->login;
            */
            $email = $user->email;
            $jmeno = $user->login;
            
            $db->update(TABLE_UZIVATELE, array("heslo" => password_hash($heslo, PASSWORD_DEFAULT)),"idUzivatele=".$this->user_id);
            
            $predmet = TNOVE_HESLO_PRO_UZIVATELE." ".$jmeno;
            $text = str_replace(array("[heslo]","[login]"),array($heslo,$jmeno),$text);

            $odeslano = SendMail(EMAIL_WEBU,NAZEV_WEBU,$email,$predmet,$text);
            $userSystemMessage->add_ok(TNOVE_HESLO_VAM_BYLO_ZASLANO_NA_EMAIL." ".$email);
            
            
        }
        else
        $userSystemMessage->add_error($error);
        
        return $odeslano;
        
        
    }
    
    

    
    
    
    
    
    public function logout()
    {
        unset($_SESSION[$this->session_name]);
        unset($_SESSION["id_uzivatel_new"]);
    }
    
    public function redirect($save_referer = false, $noaccess_show = true)
    {
        global $links;
        
        
        
        $noaccess = $this->is_logged && $noaccess_show ? "?no_access" : "";
        
        if($save_referer)
        {
            $ref = PROTOKOL.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
            $_SESSION['referer'] = str_replace("?logout","",$ref);
        }
        
        $url = $links->get_url("login");
        
        header("Location: ".$links->get_url("login").$noaccess);
        exit();
    }
    
    
    
}

?>