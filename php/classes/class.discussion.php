<?php

/**
 * @author Pavel Srom
 * @copyright 2010
 */
 

class C_Discussion{
	
	//obsahuje polozky diskuze
	public $items;
	
	//id stranky na ktere se diskuze nachazi
	private $idPage;
	
	//indikuje existenci diskuze s danym id
	public $exists;
	
    //obsahuje vyrazy filtru
    private $filter_terms;
        
	public function __construct($idPage){
		global $domain;
		global $db;
        global $pagination;

		$this->items = array();
		$this->exists = false;
        $this->filter_terms = array();

		if($idPage==0) return;
		
		$this->idPage = $idPage;
		
		
			
    //nacteni vyrazu filtru
    if(POVOLIT_CENZURU_PRISPEVKU && (AUTOMATICKA_CENZURA_PRISPEVKU || POVOLIT_FILTR_PRISPEVKU))
        {
        $data = $db->query("SELECT filtr AS vyraz FROM ".TABLE_DISKUZE_FILTR." WHERE zobrazit=1 AND idDomeny=".$domain->getId());
        
		while($d = $db->getAssoc($data))
            $this->filter_terms[] = $d['vyraz'];     
        }
        
        
	//reakce na kliknuti na tlacitko pro pridani prispevku
	if(is_post("btnVlozitPrispevek")){

		$ids = get_int_post('idStranky');
		
		if($ids!=$idPage) return;
		
		//zpracovani prispevku
		$this->SaveContribution();
		}
    
	$strankovat_od = ($pagination->get_current_page() - 1) * intval(POCET_PRISPEVKU_NA_STRANKU);	
	$data_polozky = $db->Query("SELECT dp.idPolozky, dp.text, dp.jmeno, dp.email, dp.ip, dp.admin,
			DATE_FORMAT(dp.datum,'%d.%m.%Y') AS datum, 
			DATE_FORMAT(dp.datum, '%H:%i') AS cas
			FROM ".TABLE_DISKUZE_POLOZKY." AS dp
			WHERE dp.idStranky=".$idPage."
                AND dp.zobrazit=1
			ORDER BY dp.datum DESC, dp.idPolozky DESC
            
			");
		
		
	$i = 0;
	while($d = $db->getAssoc($data_polozky)){
		$this->items[$d['idPolozky']] = array(
			'poradi'=>  $i,
			'jmeno'	=>	$d['jmeno'],
			'email'	=>	$d['email'],
			'text'	=>	$d['text'],
			'datum'	=>	$d['datum'],
			'cas'	=>	$d['cas'],
            'ip'    =>  $d['ip'],
            'admin' =>  $d['admin']
			);	
		$i++;
		}
	
	$db->free($data_polozky);
		
	}
	
	public function GetDiscussion()
    {
		global $domain;
		global $db;
        global $userSystemMessage;
        global $pagination;
        
		$pocet = intval(POCET_PRISPEVKU_NA_STRANKU);
        $pagination->set_number_items_on_page($pocet);
        
        $c1 = rand(0,9);
        $c2 = rand(0,9);

		$session = 'diskuze';
		$stranka = $pagination->get_current_page();
		$od = ($stranka-1) * $pocet;
        
		$html = '';
        	
		$html .= "<span id='diskuze'></span>"; 
        $html .= '<h3>'.TPRIDAT_KOMENTAR.'</h3>';
        	
        $html .= $userSystemMessage->show_messages(true,"diskuze");	

        $html .= '
			
			<form method="post" action="#diskuze" class="fDiskuze">
				<input type="text" name="jmeno" id="jmeno" value="'.$this->GetPostItem('jmeno').'" placeholder="'.TJMENO.'" class="cleaningText req"/><br />
				<input type="text" name="kontakt" value="'.$this->GetPostItem('kontakt').'" placeholder="'.TEMAIL.'" class="cleaningText req"/><br  />
				
				<textarea name="text" rows="8" cols="20" placeholder="'.TTEXT.'" class="cleaningText req">'.$this->GetPostItem('text').'</textarea><br />
                <input type="text" class="cleaningText antispam req" name="antispam" placeholder="'.sprintf(TANTISPAM, $c1, $c2).'"/><br />
                <span class="req">'.TPOVINNE_UDAJE.'</span>
                <input type="hidden" name="idStranky" value="'.$this->idPage.'" />
                <input type="hidden" name="c1" value="'.$c1.'"/>
                <input type="hidden" name="c2"  value="'.$c2.'"/>
                <input type="text" name="email" value="@" class="nodisplay" />
                <br />
				<input type="submit" name="btnVlozitPrispevek" value="'.TODESLAT.'" class="submit-button" />
                
                <div class="cleaner"></div>
			</form>			
            <br />
            ';
        
        
			
		$smiles = null;
		if(SMAJLICI > 0){
			include_once('class.smiles.php');
			$smiles = new C_Smiles(SMAJLICI);
			if($smiles->exists){
				$html .= "<div class='smiles'>";
				$html .= $smiles->GetSmilesClickable('textPrispevku');
				$html .= "</div>";

				}	
			}

        /*
		$dny = array(10 => '-- vyberte dnešní den --', 1 => "pondělí", 2=> "úterý", 3=> "středa", 4=> "čtvrtek", 5=>"pátek", 6=>"sobota", 0=>"neděle");
		
			$html .= "<tr><th>Antispam</th><td>";
			$html .= ShowSelectBoxToString('antispam', $dny, 10, false);
			$html .= "</td></tr>";
		*/
		
		
		$i = 0;
		
        if(count($this->items) > 0)
        {
        $html .= '<div id="vypisPrispevku">';
        $html .= '<h3>'.TKOMENTARE.'</h3>';
        
		foreach($this->items AS $p){
			if($p['poradi']<$od) continue;
			if($pocet==$i) break;
			
            $p['text'] = strip_tags($p['text']);
            	
             if(POVOLIT_FILTR_PRISPEVKU){
                $filtrovany_text = $this->filter($p['text']);
                $p['text'] = $filtrovany_text['text'];
                }
    
            $text = "";
    		if($smiles==null)
    			$text = secureString($p['text']);
    			else
    			$text = $smiles->GetSmilesToText(secureString($p['text']));

            $html .= '<div class="komentar">';
            $html .= '<strong>'.secureString($p['jmeno']).'</strong><br />';
            $html .= '<em>'.$p['datum']." ".$p['cas'].'</em>';
            $html .= '<div class="text">'.$text.'</div>';
            $html .= '</div>';  
          
          
            
            
            /*
			$html .= "<div class='discussion-item'>";
			$html .= "<div class='header'>
				<span class='name'>".secureString($p['jmeno'])."</span>";
				
			if($p['email']!='')
				$html .= ", <span class='email'>".secureString(str_replace('@','(at)',$p['email']))."</span>";
				
			$html .= "</div>";
			$html .= "<div class='datetime'>
				<span class='date'>".$p['datum']."</span>
				<span class='time'>".$p['cas']."</span>
				</div>";
			*/

			$i++;
			}
		
        $html .= "</div>";
		}
        
        		
		$html .= $pagination->set_url_tail("#diskuze")->get_html();
		
		return $html;
		
	}
	
	
	private function SaveContribution()
    {
		global $db;
		global $domain;
		global $links;
		global $userSystemMessage;
        
		$result = $this->ValidateItem();
        
        
        
		if($result == null){
		    return;  
			//Redirect($_SERVER['HTTP_REFERER']."#diskuze");
			}

        
        $vysledek_blacklistu = $this->blacklist();
        $vysledek_cenzury = 0; 
        
        if(AUTOMATICKA_CENZURA_PRISPEVKU || POVOLIT_FILTR_PRISPEVKU)
            $vysledek_cenzury = $this->filter($result['text']); 
        
        
        //print_r($vysledek_cenzury);
        
		if(POVOLIT_CENZURU_PRISPEVKU && !AUTOMATICKA_CENZURA_PRISPEVKU)
            $res1 = $db->Query("INSERT INTO ".TABLE_DISKUZE_POLOZKY." (idStranky,jmeno,email,datum,text,zobrazit, blacklist)
			     VALUES (".$this->idPage.",'".$result['jmeno']."','".$result['email']."',NOW(),'".$result['text']."',0,".$vysledek_blacklistu.")");
        elseif(POVOLIT_CENZURU_PRISPEVKU && AUTOMATICKA_CENZURA_PRISPEVKU)
            {
                  
                $res1 = $db->Query("INSERT INTO ".TABLE_DISKUZE_POLOZKY." (idStranky,jmeno,email,datum,text,zobrazit, blacklist) 
			     VALUES (".$this->idPage.",'".$result['jmeno']."','".$result['email']."',NOW(),'".$result['text']."',".intval($vysledek_cenzury['bad']).",".$vysledek_blacklistu.")");
            }
            else
            $res1 = $db->Query("INSERT INTO ".TABLE_DISKUZE_POLOZKY." (idStranky,jmeno,email,datum,text,blacklist)
			     VALUES (".$this->idPage.",'".$result['jmeno']."','".$result['email']."',NOW(),'".$result['text']."',".$vysledek_blacklistu.")");
            
			
		if(ODESILAT_PRISPEVKY_NA_EMAIL!="" && EMAIL_WEBU!=''){
			if($result['email']!='') $from=$result['email']; else $from=FROM_ADMIN_EMAIL;
			
        $nazev = $db->get(TABLE_STRANKY,"nazev","idStranky=".$this->idPage);
        $link = "<a href='".PROTOKOL.$domain->getUrl().$links->get_url($this->idPage)."#diskuze'>".$nazev."</a>";
				
        $zprava = "Přidán nový diskuzní příspěvek do diskuzního fora na stránce ".$link." (web: ".$domain->getName()."). <br/>Příspěvek vložil <b>".$result['jmeno']."</b> ".(($result['email']=='')?'':'('.$result['email'].')')." s textem:<br /><b>".$result['text']."</b><br/>.";

				
			$res2 = SendMail($from,$result['jmeno'],EMAIL_WEBU,"Diskuzní příspěvek", $zprava, $zprava);
			}
		
		if($res1)
        {
		      if(AUTOMATICKA_CENZURA_PRISPEVKU && $vysledek_cenzury['bad'])
                    $userSystemMessage->add_ok(TPRISPEVEK_BYL_ODESLAN_KE_SCHVALENI);
                    else
			        $userSystemMessage->add_ok(TPRISPEVEK_BYL_VLOZEN);
                    
              Redirect($_SERVER['HTTP_REFERER']."#diskuze");  
        }
		else
		$this->AddError(TPRISPEVEK_SE_NEPODARILO_VLOZIT);
			
		
	
		}
	
	private function ValidateItem(){
		
		global $db;
        global $userSystemMessage;
		
        $userSystemMessage->set_form_id("diskuze");
        
		$res1 = $res2 = $res3 = $res4 = true;
		
		
		$jmeno = get_post('jmeno');
        $jmeno = $db->secureString($jmeno);
            
        if(!is_post("email") || get_post("email")!="@")
            return false;
            
		if(!TestSentenceCZ($jmeno,80,false)){
			$userSystemMessage->add_error(TJMENO_MUSI_BYT_VYPLNENO);
			$res1 = false; 
			}
			else
			$result['jmeno'] = $jmeno;

		
		$email = get_post('kontakt');
        $email = $db->secureString($email);
        
		if(!TestEmail($email,200,false)){
			$userSystemMessage->add_error(sprintf(TPOLOZKA_EMAIL_CHYBA,TEMAIL));
			$res2 = false; 
			}
			else
			$result['email'] = $email;

			
		$text = get_post('text');
        $text = $db->secureString($text);
		if($text==''){
			$userSystemMessage->add_error(TNENI_VYPLNEN_TEXT_PRISPEVKU);
			$res3 = false;
			
			}
			else
			$result['text'] = $text;
			
		$c1 = get_int_post("c1");
        $c2 = get_int_post("c2");
		$antispam = get_int_post("antispam");
		if($antispam != $c1 + $c2){
			$userSystemMessage->add_error(TANTISPAM_VYBERTE_DNESNI_DEN);
			$res4 = false;
			
			}
			else
			$result['antispam'] = "";
			
		if($res1 && $res2 && $res3 && $res4) 
			return $result; 
			else 
			return NULL;
		
	}
	
	

	
	private function GetPostItem($name)
    {
        return secureString(get_post($name));
		
	}
	
    
    private function blacklist()
    {
        global $db;
        global $domain;
        
        if(!POVOLIT_BLACKLIST) return 0;
        
        $ip = $_SERVER['REMOTE_ADDR'];
        
        $ip_data = explode('.',$ip);
        $ip1 = $ip_data[0].".*";
        $ip2 = $ip_data[0].".".$ip_data[1].".*";
        $ip3 = $ip_data[0].".".$ip_data[1].".".$ip_data[2].".*";
        
        $data = $db->Query("SELECT * FROM ".TABLE_DISKUZE_BLACKLIST." WHERE (ip LIKE '".$ip."%' OR ip LIKE '".$ip1."%' OR ip LIKE '".$ip2."%' OR ip LIKE '".$ip3."%') AND idDomeny = ".$domain->getId()." LIMIT 1");
        
        if($db->numRows($data) == 1)
            return 1;
            else
            return 0;
    }
    
    //vstupni parametry: text, ktery se bude filtrovat
    private function filter($text)
    {
        global $db;
        global $domain;  
        
        $bool = false;
        
        $interpunkce = array(',','.','!','?','\"','\'','%','$','@',':');
        $text = strip_tags($text);
        
        if(!function_exists('mb_split')) return array('bad' => $bool, 'text' => $text);
        
        
        mb_internal_encoding("UTF-8");
        if(count($this->filter_terms) == 0) return array('bad' => $bool, 'text' => $text);
        
       
        $slova = explode(' ', $text);
       
        if(count($slova)==0) return array('bad' => false, 'text' => $text);

        

        foreach($slova AS $ids => $s)
            foreach($this->filter_terms AS $idv => $v){
                $testovane_slovo = implode('', mb_split('/[\\W]/u', trim($s)));
                
                $testovane_slovo = str_replace(
                    array('.',',','?','!','\'','\"','[',']','{','}','(',')',':','@','#','$','%','^','&','*','+','-','_' ),
                    array('','','','','','','','','','','','','','','','','','','','','','',''),
                    $testovane_slovo
                    );
                
                $testovane_slovo = str_replace(array('Ě','Š','Č','Ř','Ž','Ý','Á','Í','É','Ú','Ů','Ď','Ť','Ň','Ó'),
                    array('ě',"š","č","ř","ž","ý","á","í","é","ú","ů","ď","ť","ň"), $testovane_slovo); 
                     
                //print_r($testovane_slovo);
                //echo $testovane_slovo."<br>";
                $vyraz_filtru = "/^[\\W\\d]*".str_replace('*','[\\wĚŠČŘŽÝÁÍÉÚŮĎŤŇÓěščřžýáíéúůďťňó]*',$v)."[\\W\\d]*$/iu";
                //echo $vyraz_filtru."<br>";
                
                
                //zachovani interpunkci na konci a na zacatku separovaneho slova
                $i2 = mb_substr($s,-1);
                if(!in_array($i2, $interpunkce))
                    $i2 = "";
                    
                $i1 = mb_substr($s,0,1);
                if(!in_array($i1, $interpunkce))
                    $i1 = "";
                
                
                
                
                if(preg_match($vyraz_filtru,$testovane_slovo)){
                    //echo "Slovo (".mb_strlen($s).")".$s." vyhovuje vyrazu".$vyraz_filtru."<br /><br />";
                    $length = mb_strlen($s);
                    if($i1!='') $length--;
                    if($i2!='') $length--;
                    
                    $slova[$ids] = $i1.str_repeat('*',$length).$i2;
                    $bool = true;
                    }
                
                }
        
        return array(
            'text' => implode(' ', $slova), 
            'bad' => $bool //indikuje zda nejake slovo v prispevku vyhovuje nekteremu vyrazu filtru
            );
        
        
    }
    
	
	
}



?>