<?php

/**
 * @author Pavel Srom
 * @copyright 2010
 */


class C_Editbox{
	
	//obsahuje data o editboxech
	public $editbox;
	
	//obsahuje polozky editboxu, vetsinou obsahuje jen jednu polozku, jen u rotujicich editboxu jich obsahuje vice
	public $items;
	
	//indikuje existenci galerie s danym id
	public $exists;
	
    //indikuje zda pridat javascript ke kolotoci ci nikoliv
    private $javascript = true;

    //indikuje, zda bude box editovatelny
    private $editable = true;
    
    //id boxu
    private $id = 0;
	
	public function __construct($idEditbox){
		global $domain;
		global $db;
        
        $this->id = $idEditbox;
		$this->editbox = array();
		$this->items = array();
		$this->exists = false;
		
		$idEditbox = intval($idEditbox);
		if($idEditbox==0) return;
        
		$data = $db->Query("SELECT *, idEditboxu AS idPolozky
			FROM ".TABLE_EDITBOXY." AS e
			WHERE idDomeny=".$domain->getId()."
				AND idEditboxu=".$idEditbox."
				AND e.zobrazit=1
			LIMIT 1
			");
		
        if($db->numRows($data)==0){
			$this->exists = false;
			return;
			}
        
        $e = $db->getAssoc($data);
        
        $this->editbox['id'] = $e['idEditboxu'];
		$this->editbox['nazev'] = $e['nazev'];
        $this->editbox['stridani']=$e['stridani'];
        $this->editbox['obrazek']=$e['obrazek'];
        $this->editbox['obrazek_path']= RELATIVE_URL_USER_DOMAIN.USER_DIRNAME_EDITBOXES.$e['obrazek'];
        $this->editbox['efekt']=$e['samorotujiciEfekt'];
        $this->editbox['rychlost_efektu']=$e['samorotujiciRychlostEfektu'];
        $this->editbox['prodleva']=$e['samorotujiciProdleva'];
        $this->editbox['sledovatProkliky']=$e['sledovatProkliky'];
        
        if($e['stridani']=='nahodne')	
            $data_polozky = $db->Query("SELECT e.idEditboxu AS idPolozky, nazev, obsah, obrazek
                FROM ".TABLE_EDITBOXY_POLOZKY." AS ep
                LEFT JOIN ".TABLE_EDITBOXY." AS e ON ep.idEditboxu=e.idEditboxu 
                WHERE ep.idKolotoce=".$idEditbox."
                    AND e.idDomeny=".$domain->getId()."
    				AND zobrazit=1
    				AND (od <= CURDATE() OR od IS NULL OR od = '0000-00-00')
    				AND (do >= CURDATE() OR do IS NULL OR do = '0000-00-00')
    			ORDER BY RAND()
                LIMIT 1
    			");    
        elseif($e['stridani']=='kolotoc'){
            $session = 'poradi-editbox'.$idEditbox;
			$akt_poradi = isset($_SESSION[$session])?intval($_SESSION[$session]):0;
            
            $data_polozky = $db->Query("SELECT e.idEditboxu AS idPolozky, nazev, obsah, obrazek 
                FROM ".TABLE_EDITBOXY_POLOZKY."  AS ep
                LEFT JOIN ".TABLE_EDITBOXY." AS e ON ep.idEditboxu=e.idEditboxu
                WHERE ep.idKolotoce=".$idEditbox."
                    AND e.idDomeny=".$domain->getId()."
    				AND zobrazit=1
    				AND (od <= CURDATE() OR od IS NULL OR od = '0000-00-00')
    				AND (do >= CURDATE() OR do IS NULL OR do = '0000-00-00')
    			ORDER BY ep.poradi, ep.idRelace
                LIMIT ".$akt_poradi.",1
    			"); 
                
            if($db->numRows($data_polozky)==0){ 
                $akt_poradi = 0;
                $data_polozky = $db->Query("SELECT e.idEditboxu AS idPolozky, nazev, obsah, obrazek
                    FROM ".TABLE_EDITBOXY_POLOZKY." AS ep
                    LEFT JOIN ".TABLE_EDITBOXY." AS e ON ep.idEditboxu=e.idEditboxu
                    WHERE ep.idKolotoce=".$idEditbox."
                        AND e.idDomeny=".$domain->getId()."
        				AND zobrazit=1
        				AND (od <= CURDATE() OR od IS NULL OR od = '0000-00-00')
        				AND (do >= CURDATE() OR do IS NULL OR do = '0000-00-00')
        			ORDER BY ep.poradi, ep.idRelace
                    LIMIT 0,1
        			"); 

                }

            $akt_poradi++;
                 
            $_SESSION[$session] = $akt_poradi;
            
                    
            }
        elseif($e['stridani']=='samorotujici'){
            $data_polozky = $db->Query("SELECT e.idEditboxu AS idPolozky, nazev, obsah, obrazek
                    FROM ".TABLE_EDITBOXY_POLOZKY." AS ep
                    LEFT JOIN ".TABLE_EDITBOXY." AS e ON ep.idEditboxu=e.idEditboxu 
                    WHERE ep.idKolotoce=".$idEditbox."
                        AND e.idDomeny=".$domain->getId()."
        				AND zobrazit=1
        				AND (od <= CURDATE() OR od IS NULL OR od = '0000-00-00')
        				AND (do >= CURDATE() OR do IS NULL OR do = '0000-00-00')
        			ORDER BY ep.poradi, ep.idRelace
        			");
            
        }    
        
	
		

		$this->exists = true;
		
        if($e['stridani'] == 'zadne'){
            $this->items[$idEditbox] = array(
    				'id'=> $e['idPolozky'], 
    				'nazev'=>$e['nazev'], 
                    'obsah'=>$e['obsah'],
                    'obrazek'=>$e['obrazek'],
                    'obrazek_path'=> RELATIVE_URL_USER_DOMAIN.USER_DIRNAME_EDITBOXES_MAXI.$e['obrazek'],
                    'obrazek_path_mini'=> RELATIVE_URL_USER_DOMAIN.USER_DIRNAME_EDITBOXES_MINI.$e['obrazek']
    				);
                    
            
                    
            }
            else
            {
    		$i = 0;

    		while($editbox = $db->getAssoc($data_polozky)){
    			$this->items[$i] = array(
    				'id'=> $editbox['idPolozky'], 
    				'nazev'=>$editbox['nazev'], 
    				'obsah'=>$editbox['obsah'],
                    'obrazek'=>$editbox['obrazek'],
                    'obrazek_path'=> RELATIVE_URL_USER_DOMAIN.USER_DIRNAME_EDITBOXES_MAXI.$editbox['obrazek'],
                    'obrazek_path_mini'=> RELATIVE_URL_USER_DOMAIN.USER_DIRNAME_EDITBOXES_MINI.$editbox['obrazek']
    				);
			
			
			    $i++;
                }

                
			}
			
		$db->free($data);
        if(isset($data_polozky))
            $db->free($data_polozky);
		
	}
	
    //vraci polozky editboxu
    public function get_items()
    {
        return $this->items;
    }
    
    //vraci hodnotu nastaveni editboxu
    public function get_setting($setting)
    {
        return isset($this->editbox[$setting]) ? $this->editbox[$setting] : "";
    }

    //nastavi editovatelnost boxu
    public function editable($editable = true)
    {
        $this->editable = $editable;
        return $this;
    }
    
    //vraci pocet editboxu
    public function get_boxes_count()
    {
        return count($this->items);
    }
    
    public function get_html($id = 0)
    {
        
        $id = $id == 0 ? $this->id : $id;
        
        $html = $this->editable ? editable($id, $this->items[$id]['obsah'], "editbox",$this->editbox['stridani']!='samorotujici') : $this->items[$id]['obsah']; 
        
        return $html;
    }
    
    //nastavi zda se zobrazi javascript u kolotoce ci nikoliv
    public function allow_javascript($allow = true)
    {
        $this->javascript = $allow;
    }
        
	
	public function GetEditbox(){
		global $db;
        
		$html = "";
		if(!$this->exists) return $html;
		
        $style = "";
           
       
		$html .= "<div class='block-editbox' id='editbox".$this->editbox['id']."' ".$style.">";
        
        $class = "";
        if($this->editbox['sledovatProkliky']==1){
            $class = "class='sledovat_prokliky'";
        
        
            
            //pricteni bodu pro zobrazeni
            if($this->editbox['stridani']=='samorotujici')
                $db->Query("UPDATE ".TABLE_EDITBOXY." SET zobrazeno=zobrazeno+1 WHERE idEditboxu=".intval($this->editbox['id']));
                else
                $db->Query("UPDATE ".TABLE_EDITBOXY." SET zobrazeno=zobrazeno+1 WHERE idEditboxu=".intval($this->items[0]['id']));
            }
        
           
        
        if($this->editbox['stridani']=='samorotujici')
        {
            $style="style='display: none;'";
        }
            
        foreach($this->items AS $pol) 
        {
            $html .= "<div ".$class." ".$style.">";

            if($this->editable)
                $html .= editable($this->editbox['id'], $pol['obsah'],"editbox",$this->editbox['stridani']!='samorotujici');
                else
                $html .= $pol['obsah'];

            $html .= "</div>";
        }
          
		$html .= "</div>";
		return $html;
		}
	
}


?>