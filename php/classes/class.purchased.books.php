<?php

/**
 * @author Pavel �rom
 * @copyright 2018
 */


class C_PurchasedBooks
{
    
    //nactene zakoupene knihy
    private $purchased_books = array();
    
    
    public function __construct($nacist_zakoupene_knihy = false)
    {
        global $db;
        global $private_pages;
        
        //nacte zakoupene knihy, na tomto zaklade se pak urcuje zda si uzivatel knihu koupil a pak se zobrazuji hlasky a tlacitko Koupit v detailu produktu
        if($nacist_zakoupene_knihy)
        {
            
            $data = $db->query("SELECT id, hash, mesic, rok FROM ".TABLE_ESHOP_ZAKOUPENE_KNIHY." WHERE id_uzivatel=".$private_pages->get_user_id());
            
            while($k = $db->getObject($data))
                $this->purchased_books[$k->hash] = $k->rok."-".$k->mesic;
            
        }
        
        
    }
    
    
    //prida polozky objednavky mezi zakoupene knihy
    public function add($id_objednavka)
    {
        global $db;
        
        //nacteni polozek zaplacene objednavky
        $data = $db->query("SELECT op.id_produkt, op.od, op.do, op.id_objednavka, o.id_uzivatel
            FROM ".TABLE_ESHOP_OBJEDNAVKY_POLOZKY." AS op
            LEFT JOIN ".TABLE_ESHOP_OBJEDNAVKY." AS o ON o.id = op.id_objednavka
            WHERE o.id_stav = 2
                AND op.id_objednavka = ".$id_objednavka."
            ");
            
        //pro kazdou polozku zaplacene objednavky, zadej do fronty emailu
        while($p = $db->getObject($data))
        {

            //nacte vsechny mesice v intervalu od - do
            $mesice = $this->get_all_months($p->od, $p->do);
            
            foreach($mesice AS $m)
            {
                $this->save($m['mesic'], $m['rok'], $id_objednavka, $p->id_uzivatel);
                
            }
            
            
        }
        
        
        
            
    }
    
    //ulozi zaznam pokud jeste neexistuje do databaze
    private function save($mesic, $rok, $id_objednavka, $id_uzivatel)
    {
        global $db;
        
        $rok = intval($rok);
        $mesic = intval($mesic);
        
        
        //overeni zda uz zaznam existuje
        $id = $db->get(TABLE_ESHOP_ZAKOUPENE_KNIHY, "id", "id_uzivatel=".$id_uzivatel." AND mesic=".intval($mesic)." AND rok=".$rok."");
        
        //pokud zaznam jeste neexistuje, pak se vytvori
        if($id == 0)
        {
            $insert = array(
                "id_objednavka" => $id_objednavka,
                "id_uzivatel" => $id_uzivatel,
                "mesic" => $mesic,
                "rok" => $rok,
                "vlozeno" => "now",
                "hash" => uniqid(rand(10000,99999).$id_uzivatel.$id_objednavka.$id)
                );
                
            $db->insert(TABLE_ESHOP_ZAKOUPENE_KNIHY, $insert);
                
        }
        
        
    }
    
    
    
    //vraci v poli vsechny mesice v danem intervalu
    public function get_all_months($od, $do)
    {
        $start    = (new DateTime($od))->modify('first day of this month');
        $end      = (new DateTime($do))->modify('first day of next month');
        $interval = DateInterval::createFromDateString('1 month');
        $period   = new DatePeriod($start, $interval, $end);
        
        $result = array();
        foreach ($period as $dt) {
            $result[] = array(
                "mesic" => $dt->format("m"), 
                "rok" => $dt->format("Y")
                );
        }
        
        return $result;
        
        
    }
    
    //vraci true, pokud ma uzivatel knihu zakoupenou/predplacenou, jinak vraci false
    public function has_purchased($mesic, $rok)
    {
        $mesic = intval($mesic);
        $rok = intval($rok);
        
        $k = $rok."-".$mesic;
        
        $d1 = date("Yn");
        $d2 = $rok.intval($mesic);
    
        
    
        //za zakoupenou je povazovana v danou chvili kniha, ktera je vydana drive nebo aktualni rok a mesic. Takze pokud ma nekdo predplaceno do budoucna, tak tyto knihy, ktere se budou teprve vydavat neuvidi
        if($d2 > $d1)
            return false;
        
                
        //kniha je povazovana za zakoupenou pokud ma zaznam v tabulce eshop_zakoupene_knihy
        $ma_koupeno = in_array($k, $this->purchased_books);
        
        return $ma_koupeno;
        
    }
    
    
    //vraci odkaz ke stazeni
    public function get_download_url($mesic, $rok)
    {
        global $private_pages;
        
        $k = $rok."-".$mesic;
        
        $key = array_search($k, $this->purchased_books);
        
        if($key === false)
            return false;
            
        return get_url_download($private_pages->get_user_hash(), $key); 
        
    }
    
    
    
    
    
    
}

?>