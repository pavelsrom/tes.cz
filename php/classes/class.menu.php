<?php

/**
 * @author Pavel Srom
 * @copyright 2010
 */

class C_Menu{
	
	//pole s polozkami menu
	public $menu;
	
	//urcuje zda je menu nastaveno
	public $exists;
	
	
	//pocet odkazu levelu 1
	private $count_links_level1;
	
	//nazev bloku menu
	public $nazev;
	
	
	//indikuje zobrazeni kompletniho rozbaleneho stromu menu
	private $allItems;
	
	//indikuje zda ma byt kazda polozka v menu odkazem, vcetne aktivnich polozek
	private $allLinks;
	
	//id aktualni stranky
	private $idAktualniStranky;
	
	
	//pokud je idMenu 0, pak se nacte do objektu hlavni horni menu, allItems urcuje zda se maji rozbalit vsechny polozky menu nebo jen aktivni polozky nebo aktivni podmenu, allLinks urcuje zda i aktivni odkazy maji byt odkazy
	
	public function __construct($idMenu, $idAktualniStranky=0, $typ='',$allItems=true, $allLinks=false){
		global $domain;
		global $db;
		
		global $links;

		$this->code = '';
		$this->exists = false;
		
		$this->allLinks = $allLinks;
		$this->allItem = $allItems;
		
		$this->idAktualniStranky = $idAktualniStranky;
		
		$this->menu = array();
		
		//vybere polozky z databaze
		if($idMenu>0){
			$data = $db->Query("SELECT m.nazev AS nazev_menu, mp.idPolozky, mp.idRodic, mp.nazev, mp.odkaz
				FROM ".TABLE_MENU." AS m
				LEFT JOIN ".TABLE_MENU_POLOZKY." AS mp ON m.idMenu = mp.idMenu
				WHERE m.idDomeny = ".$domain->getId()." 
					AND m.idMenu =".$idMenu."
					AND m.zobrazit = 1
				ORDER BY mp.idRodic, mp.priorita,mp.idPolozky");
                
            if($db->numRows($data)==0) 
    			$this->exists = false; 
    			else 
    			$this->exists=true;
            }
        else{
            $this->exists=false;
            return;
            }
			
		

			
		//inicializuje pole menu
			
		$ind = true;	//indikuje jestli uz byla nejaka polozka oznacena jako aktivni

		if($db->numRows($data)>0)
			while($item = $db->getAssoc($data)){
				$this->nazev = $item['nazev_menu'];
				$nazev = $item['nazev'];
				$url = "";
				$link = $item['odkaz'];
				if(ctype_digit($link) && isset($links->data[$link])){
					$url = $links->data[$link]['url'];

					if($link==$this->idAktualniStranky) 
						$aktivni=1; 
						else 
						$aktivni=0;
					}
					
					elseif(TestHttpUrl($link)){ 
						$url = $link;
						$aktivni = 0;
						}
						else
						{
						$aktivni = 0;
						$url = "";
						}

			 	
				if(!$ind && $aktivni==1){
			 		$aktivni = 0;
			 		}
			 	
							 	
				$this->menu[$item['idPolozky']] = array('idRodice'=>$item['idRodic'], 'nazev'=>$nazev, 'idUrl'=>intval($item['odkaz']) , 'url'=>$url, 'aktivni'=>$aktivni, 'i'=>0);
  
				}
		$this->count_links_level1 = count($this->menu);
		if($this->count_links_level1 > 0) $this->exists = true;
		//print_r($this->menu);
		
		mysql_free_result($data);
	}
	
	public function GetMenu($width=''){
		if(count($this->menu)==0) return '';
        
        //vynulovani indikatoru, ktery urcuje zda uz byla polozka vypsana
        //toto je nutne pro rekurzivni funkci Menu, pri vypisovani menu se kazda polozka oznaci indikatorem i=1
        //pokud by doslo k opetovnemu vypisovani menu, menu se nevypise, jen pokud probehne nasledujici inicializace
        foreach($this->menu AS $id=>$v)
            $this->menu[$id]['i'] = 0;
            
		$this->Menu(0, $width);
		return $this->code;
		
		
		//$this->GetNavigation();	
		
		
	}
	
	private function Menu($idRodic=0, $width='', $firstULclass=true, $li_obal = false){
		
		$style='';
		if($width!='') $style = "style='width:".intval($width)."px'";
		
		
		if($firstULclass) $class = "class='links'"; else $class="";
		
		
        
        //echo $idRodic;      
          
        $podmenu = array();
        foreach($this->menu AS $id => $m){
            if($m['idRodice']!=$idRodic || $m['i']==1) continue;
            $m['i']=1;
            $podmenu[$id] = $m;
            }
        
        $pocet = count($podmenu);
        if($pocet==0) return;
        
        if($li_obal) $this->code .= "<li>\n";
        
        $this->code .= "<ul ".$style." ".$class.">\n";
        
        $i=0;
		foreach($podmenu AS $id => $m){
			if($i==0) $prvni = 'first'; else $prvni = '';
			if($i==$pocet-1) $posledni = 'last'; else $posledni = '';
	
			$url = $m['url'];
			
			if($m['aktivni']==1 || $this->IsActiveItem($id))
				{
				if($this->allLinks)
					$this->code .= "<li class='aktivni ".$prvni." ".$posledni."'><a href='".$url."'>".$m['nazev']."</a></li>\n";
					else
					$this->code .= "<li class='aktivni ".$prvni." ".$posledni."'><span>".$m['nazev']."</span></li>\n";
				}
				else
				$this->code .= "<li class=' ".$prvni." ".$posledni."'><a href='".$url."'>".$m['nazev']."</a></li>\n";
				
				
				
			if($this->allItem){
				$this->Menu($id,$width, false, true);
				}
				else
				if($m['aktivni']==1 || $this->IsActiveItem($id)){
					$this->Menu($id,$width, false, true);
					}
					
			$i++;
			}
		$this->code .= "</ul>\n\n";
		if($li_obal) $this->code .= "</li>\n";
		
	}
	
	
	//zjisti zdali je v podmenu aktivni stranka
	private function IsActiveItem($idRodice){

		foreach($this->menu AS $m)
			if($m['idUrl']==$this->idAktualniStranky && $m['idRodice']==$idRodice) return true;
			
		return false;
	}
	
	public function GetLinksToArray(){
		$result = array();
		foreach($this->menu AS $m){
			$result[] = array('nazev'=>$m['nazev'], 'url'=>$m['url']);
    		}
			
		return $result;
		
		
	}
	
	public function GetLinks($delimiter='',$div_obal=false){
		$arr = $this->GetLinksToArray();
		
		$html = "";
		
		$i=0;
		if(count($arr)>0){
			if($div_obal) $html .= "<ul>";
			foreach($arr AS $a){
				if($i != 0) $html .= $delimiter."\n";
				$html .= "<li><a href='".$a['url']."'>".$a['nazev']."</a></li>";
				$i++;
				}
			if($div_obal) $html .= "</ul>";
			}
		return $html;
		
	}
	
	
	
}

?>