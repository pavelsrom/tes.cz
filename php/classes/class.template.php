<?php

/**
 * @author Pavel �rom
 * @copyright 2017
 */



class C_Template{
    
    //obsahuje data pro sablonu
    private $template = array();
    
    //cesta k adresari sablon
    private $template_root = "";
    
    //cesta k sablone
    private $template_path;
    
    //konstruktor
    public function __construct($template_root = "")
    {
        if($template_root == '')
            $template_root = RELATIVE_URL_USER_DOMAIN.USER_DIRNAME_SABLONY;
        
        
    }
    
    //prida parametr pro sablonu
    public function add_param($key, $values)
    {
        $this->data[$key] = $values;
        return $this;
    }
    
    //nastavi sablonu
    public function set_template($path)
    {
        $this->template_path = $path.".php";
        
        
    }
    
    //vygeneruje html kod
    public function render_to_string()
    {
        
        ob_start();
        
        include($this->template_root.$this->template_path);
        $html = ob_get_contents();
        
        ob_clean();
        
        
        return $html;
        
    }
    
    
    
    
    
    
}


?>