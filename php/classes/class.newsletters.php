<?php

/**
 * @author Pavel Srom
 * @copyright 2014
 */

class C_EmailQueue{
    
    //id jazyka
    private $lang_id = 0;
    
    //id kampane
    private $campaign_id = 0;
    
    //maximalni pocet pokusu
    private $max_attempt = 4;
    
    //limit pro jednorazove odeslani emailu
    private $limit = 800;
    
    //konstantni velikost emailu bez prilohy
    private $const_email_size = 0.1;
    
    
    public function __construct($campaign_id, $lang_id)
    {
        $this->campaign_id = $campaign_id;
        $this->lang_id = $lang_id;    
        
    }
    
    
    public function set_limit($limit = 0)
    {
        $this->limit = $limit == 0 ? $this->allowed_number_of_emails() : $limit;
    }
    
    
    //pokud je vstupem email, pak bude vybran z databaze pouze tento email, jinak budou vybrany vsechny emaily
    public function get_recipients($email = '', $osloveni_muz = null, $osloveni_zena = null, $osloveni_ostatni = null)
    {
        global $db;
        
        if($email == '')
            $data = $db->query("SELECT u.jmeno, u.prijmeni, TRIM(CONCAT(u.jmeno,' ',u.prijmeni)) AS cele_jmeno, u.email, u.hash, u.pohlavi, u.osloveni, k.osloveniMuz AS osloveni_muz, k.osloveniZena AS osloveni_zena, k.osloveniOstatni AS osloveni_ostatni, u.idPrijemce AS id, u.spolecnost 
                FROM ".TABLE_EMAILY_FRONTA." AS f 
                LEFT JOIN ".TABLE_EMAILY_PRIJEMCI." AS u ON f.idPrijemce = u.idPrijemce
                LEFT JOIN ".TABLE_EMAILY_KAMPANE." AS k ON f.idKampane = k.idKampane
                WHERE f.idKampane=".$this->campaign_id."
                    AND f.odeslano = 0
                    AND f.pokus <= ".$this->max_attempt."
                GROUP BY f.id
                ORDER BY f.id
                LIMIT ".$this->limit
            );
            else
            $data = $db->query("SELECT u.jmeno, u.prijmeni, TRIM(CONCAT(u.jmeno,' ',u.prijmeni)) AS cele_jmeno, u.email, u.hash, u.pohlavi, u.osloveni, k.osloveniMuz AS osloveni_muz, k.osloveniZena AS osloveni_zena, k.osloveniOstatni AS osloveni_ostatni, u.idPrijemce AS id, u.spolecnost  
                FROM ".TABLE_EMAILY_PRIJEMCI." AS u
                LEFT JOIN ".TABLE_EMAILY_KAMPANE." AS k ON k.idKampane = ".$this->campaign_id."
                WHERE u.email = '".$db->secureString($email)."'
                GROUP BY u.idPrijemce
                "
            );
        
        $emaily = array();
        while($e = $db->getObject($data))
        {
            $pohlavi = $e->pohlavi;
            $osloveni = $e->osloveni;
            
            $osloveni_final = "";
            if($pohlavi == 1)
                $sqlp = "muz";
            elseif($pohlavi == 2)
                $sqlp = "zena";
            else
                $sqlp = "ostatni";
                
            if(trim($osloveni) == "")
                $sqlp = "ostatni";
            
            $osloveni_final = (is_null(${"osloveni_".$sqlp}) ? $e->{"osloveni_".$sqlp} : ${"osloveni_".$sqlp}).($sqlp == 'ostatni' ? "" : " ".$osloveni);
                
                
                
    		$osloveni_final=trim($osloveni_final);
                
    		$emaily[$e->email]=array(
                "hash" => $e->hash, 
                "cele-osloveni" => $osloveni_final,
                "osloveni" => $osloveni,
                "organizace" => $e->spolecnost,
                "cele_jmeno" => $e->cele_jmeno,
                "jmeno" => $e->jmeno,
                "prijmeni" => $e->prijmeni,  
                "id" => $e->id,
                "email" => $e->email,
                );
                

        }
        
        //pokud se zasila testovaci email a pritom email neni v databazi prijemcu, tak budou predvyplnene hodnoty jine
        if(count($emaily) == 0 && $email != '')
        {

            $osloveni_final = $db->get(TABLE_EMAILY_KAMPANE,"osloveniOstatni","idKampane=".$this->campaign_id);
            
            $emaily[$email]=array(
                "hash" => "", 
                "organizace" => "Obuv Novák s.r.o.",
                "osloveni" => "Nováku",
                "cele-osloveni" => $osloveni_final,
                "cele_jmeno" => "Jan Novák",
                "jmeno" => "Jan",
                "prijmeni" => "Novák",  
                "id" => 0,
                "email" => $email,
                );
                
            
        }
        
        
        $db->free($data);
        
        return $emaily;
        
    }
    
    
    private function allowed_number_of_emails()
    {
        
        $pocetni_limit = intval(EMM_CRON_LIMIT);
        $datovy_limit = intval(EMM_CRON_LIMIT_MB);
        
        $velikost_emailu = $this->get_email_size($this->campaign_id);

        $velikost = $this->get_sent_email_size();
        $velikost_odeslanych_emailu = $velikost["velikost"];
        $pocet_odeslanych_emailu = $velikost["pocet"];
        
        
        $pocetni_limit1 = $pocetni_limit - $pocet_odeslanych_emailu;
        $datovy_limit = $datovy_limit - $velikost_odeslanych_emailu;
        
        $pocetni_limit2 = $datovy_limit > 0 && $velikost_emailu > 0 ? floor($datovy_limit / $velikost_emailu) : 0;
        
        
        $pocetni_limit1 = $pocetni_limit1 < 0 ? 0 : $pocetni_limit1;
        $pocetni_limit2 = $pocetni_limit2 < 0 ? 0 : $pocetni_limit2;
        
        if($pocetni_limit2 > 0)
            return min($pocetni_limit1, $pocetni_limit2);
            else
            return $pocetni_limit1;
        
    }
    
    //vraci velikost jednoho emailu v mb
    private function get_email_size($campaign_id)
    {
        global $db;
    
        $d = $db->query("SELECT (SUM(velikost) / 1024 / 1024) AS velikost
            FROM ".TABLE_EMAILY_PRILOHY." 
            WHERE idKampane = ".$campaign_id);
        
        $p = $db->getObject($d);
        
        return $p->velikost + $this->const_email_size;
    }
    
    //vraci velikost vsech odeslanych emailu (v posledni hodine) v MB
    private function get_sent_email_size()
    {
        global $db;
    
        $d = $db->query("SELECT idKampane, COUNT(id) AS pocet 
            FROM ".TABLE_EMAILY_FRONTA." 
            WHERE pokus > 0
                AND datum > DATE_SUB(NOW(), INTERVAL 1 HOUR) 
            GROUP BY idKampane
            ");
            
        $celkova_velikost = 0;
        $celkovy_pocet = 0;
        while($k = $db->getObject($d))
        {
            $celkovy_pocet += $k->pocet;
            $celkova_velikost += $k->pocet * $this->get_email_size($k->idKampane); 
                    
        }
        
        return array("velikost" => $celkova_velikost, "pocet" => $celkovy_pocet);
    }
    
    //zjisti pocet zbyvajicich emailu k odeslani
    public function get_count_remaining_emails()
    {
        global $db;
        
        $d = $db->query("SELECT COUNT(id) AS pocet 
            FROM ".TABLE_EMAILY_FRONTA." 
            WHERE idKampane=".$this->campaign_id." 
                AND odeslano=0
                ");
                
        $p = $db->getObject($d);
        return $p->pocet;
        
    }
    
    //zjisti pocet emailu, ze kterych se ctenar proklikl na web
    public function get_count_clicked_emails()
    {
        global $db;
        
        $d = $db->query("SELECT COUNT(id) AS pocet 
            FROM ".TABLE_EMAILY_FRONTA." 
            WHERE idKampane=".$this->campaign_id." 
                AND kliknuto=1
                ");
                
        $p = $db->getObject($d);
        return $p->pocet;
        
    }
    
    //zjisti pocet emailu ktere byly otevreny/precteny
    public function get_count_opened_emails()
    {
        global $db;
        
        $d = $db->query("SELECT COUNT(id) AS pocet 
            FROM ".TABLE_EMAILY_FRONTA." 
            WHERE idKampane=".$this->campaign_id." 
                AND otevreno=1
                ");
                
        $p = $db->getObject($d);
        return $p->pocet;
        
    }
    
    //zjisti pocet emailu ktere byly otevreny/precteny
    public function get_count_unsubscribed_emails()
    {
        global $db;
        
        $d = $db->query("SELECT COUNT(id) AS pocet 
            FROM ".TABLE_EMAILY_FRONTA." 
            WHERE idKampane=".$this->campaign_id." 
                AND odhlaseno=1
                ");
                
        $p = $db->getObject($d);
        return $p->pocet;
        
    }
    
    
    //vstupem je id emailu v tabulce emm_fronta a stav true/false. Pokud je true, pak se email smaze z fronty, pokud false, tak se oznaci zvysi pokus o 1 a ulozi datum posledniho pokusu
    public function add_attempt($user_id, $odeslano)
    {
        global $db;
        
        $odeslano = intval($odeslano);
                
        $db->query("UPDATE ".TABLE_EMAILY_FRONTA." SET pokus = pokus + 1, datum=NOW(), odeslano=".$odeslano." WHERE idPrijemce=".intval($user_id)." AND idKampane=".$this->campaign_id);
        
        if($odeslano)
            $db->query("UPDATE ".TABLE_EMAILY_KAMPANE." SET odeslano = odeslano + 1 WHERE idKampane=".$this->campaign_id);
        
    }
    
    
    //naplni frontu emaily dane kampane
    public function fill()
    {
        global $db;
        
        if($this->is_filled_queue())
            return false;
            
        $emaily = $this->get_all_recipients();
        $i = 0;
        foreach($emaily AS $e => $id)
        {
            $insert = array(
                "idKampane" => $this->campaign_id,
                "idPrijemce" => $id,
                "email" => $e
                );
            $db->insert(TABLE_EMAILY_FRONTA, $insert);
            $i++;
        }
        
        $db->update(TABLE_EMAILY_KAMPANE,array("celkem" => $i),"idKampane=".$this->campaign_id);
        
        return true;
        
    }
    
    
    //vraci true, pokud je fronta pro danou kampan naplnena, jinak false
    public function is_filled_queue()
    {
        global $db;
        
        $d1 = $db->query("SELECT id 
            FROM ".TABLE_EMAILY_FRONTA." 
            WHERE idKampane = ".$this->campaign_id." 
            LIMIT 1");
            
        $d2 = $db->query("SELECT idKampane 
            FROM ".TABLE_EMAILY_KAMPANE." 
            WHERE idKampane = ".$this->campaign_id." 
                AND stav='ukonceno' 
            LIMIT 1");
        
        return $db->numRows($d1) > 0 || $db->numRows($d2) > 0;
    }
    
    //vraci pocet neodeslanych emailu
    public function get_count_unsent_emails()
    {
        global $db;
        
        $d = $db->query("SELECT COUNT(id) AS pocet 
            FROM ".TABLE_EMAILY_FRONTA." 
            WHERE idKampane=".$this->campaign_id." 
                AND pokus <= ".$this->max_attempt." 
                AND odeslano = 0
                ");
        
        $pocet = 0;
        if($db->numRows($d) > 0)
        {
            $p = $db->getObject($d);
            $pocet = $p->pocet;
        }
        else
        {
            $pocet = 0;
        }
        
        return $pocet;
        
        
    }
    
    //vraci pocet dorucenych emailu
    public function get_count_sent_emails()
    {
        global $db;
        
        $d = $db->query("SELECT COUNT(id) AS pocet FROM ".TABLE_EMAILY_FRONTA." WHERE idKampane=".$this->campaign_id." AND odeslano = 1");
        
        $pocet = 0;
        if($db->numRows($d) > 0)
        {
            $p = $db->getObject($d);
            $pocet = $p->pocet;
        }
        else
        {
            $pocet = 0;
        }
        
        return $pocet;
        
        
    }

    //vraci v poli nedorucitelne emaily, tj. emaily, ktere se nepodarilo odeslat na maximalni pocet povolenych pokusu
    public function get_undeliverable_emails()
    {
        global $db;
        
        $d = $db->query("SELECT email 
            FROM ".TABLE_EMAILY_FRONTA." 
            WHERE idKampane = ".$this->campaign_id." 
                AND odeslano = 0 
                AND pokus > ".$this->max_attempt."
            ORDER BY id
                ");
        $emaily = array();
        while($e = $db->getObject($d))
            $emaily[] = $e->email;
                
        return $emaily;
        
    }
    
    //vraci vsechny emaily prijemcu Z FRONTY - dorucitelne i nedorucitelne
    public function get_all_emails()
    {
        global $db;
        
        $data = $db->query("SELECT idPrijemce AS id, email FROM ".TABLE_EMAILY_FRONTA." WHERE idKampane=".$this->campaign_id);
        $emaily = array();
        while($e = $db->getObject($data))
            $emaily[$e->email] = $e->id;
            
        return $emaily; 
        
    }
    
    
    public function get_all_recipients()
    {
        global $db;
        global $domain;
        
    	$emaily=array();
    	//skupiny k odběru
    	$query=$db->query("SELECT DISTINCT u.email, u.idPrijemce AS id
    					   FROM ".TABLE_EMAILY_PRIJEMCI." u
                           LEFT JOIN ".TABLE_EMAILY_SKUPINY_PRIJEMCI." AS sp ON u.idPrijemce = sp.idPrijemce
                           LEFT JOIN ".TABLE_EMAILY_SKUPINY_KAMPANE." AS sk ON sp.idSkupiny = sk.idSkupiny
    					   LEFT JOIN ".TABLE_EMAILY_KAMPANE." k ON k.idKampane=sk.idKampane
    					   WHERE k.idKampane='".$this->campaign_id."'
                                AND u.idDomeny = ".$domain->getId()."
                                AND u.idJazyka = ".$this->lang_id." 
                                AND u.stav=1 
                           GROUP BY u.email
     					   ORDER BY u.email
                           
    	                   ");
                           
    	while ($row=$db->getObject($query))
            {
    		$emaily[$row->email]=$row->id;
            }
            
    	return $emaily;

        
    }
    
    
    
    
}








class C_Campaign{
    
    //singleton instance objektu kampane
    private static $instance;

    //html obsah
    private $content_html = "";
    
    //textovy obsah - plain text
    private $content_text = "";
    
    //predmet
    private $subject = "";
    
    //objekt fronty
    private $queue = null;

    //indikuje zda kampan pro odeslani existuje
    private $exists = false; 
    
    //id jazyka kampane
    private $lang_id = 0;
    
    //id kampane
    private $id = 0;
    
    //nazev kampane
    private $name = "";
    
    //GA kod kampane
    private $code = "";
    
    //stav kampane
    private $state = "";
    
    //defaultni nastaveni pro osloveni uzivatelu - slouzi pro testovaci odeslani emailu pomoci ajaxu
    private $custom_salutation = array("muz" => null, "zena" => null, "ostatni" => null);
    
    
    //celkem emailu
    private $sum_emails = 0;
    
    private $end_date = "";
    
    //konstruktor
    private function __construct($campaign_id = 0)
    {
        global $db;
        global $domain;
        
        $where = $campaign_id == 0 ? "stav='odesilano'" : "idKampane=".$campaign_id;
            
        //cron
        $data=$db->query("SELECT idKampane AS id, obsah, textovyObsah AS text, idJazyka AS jazyk, predmet, nazev, kod, stav, celkem, IF(datumOdeslano IS null OR datumOdeslano = '','',DATE_FORMAT(datumOdeslano,'%d.%m.%Y %H:%i')) AS konec
            FROM ".TABLE_EMAILY_KAMPANE."
            WHERE ".$where." AND (start <= NOW() OR start IS NULL) 
                AND idDomeny = ".$domain->getId()."
            ORDER BY start
            LIMIT 1
            ");
     
        if($db->numRows($data) > 0)
        {

            $this->exists = true;
            $row = $db->getObject($data);
            
            $this->content_html = $row->obsah;
            $this->content_text = $row->text;
            $this->subject = $row->predmet;
            $this->id = $row->id;
            $this->lang_id = $row->jazyk;
            $this->name = $row->nazev;
            $this->code = $row->kod;
            $this->state = $row->stav;
            $this->end_date = $row->konec;
            
            $this->sum_emails = $row->celkem;
            
            $this->queue = new C_EmailQueue($this->id, $row->jazyk);
            
        }
        
    }

    public static function get_instance($campaign_id = 0)
    {
        if (self::$instance === NULL) {
            self::$instance = new self($campaign_id);
        }
        
        return self::$instance;
    }
    
    public function get_end_date()
    {
        return $this->end_date;
    }
    
    //nastavi defaultni hodnoty pro osloveni uzivatelu v kampani - slouzi pro odeslani testovaciho emailu ajaxem
    public function set_custom_salutation($osloveni_muz = null, $osloveni_zena = null, $osloveni_ostatni = null)
    {
        $this->custom_salutation = array(
            "muz" => $osloveni_muz,
            "zena" => $osloveni_zena,
            "ostatni" => $osloveni_ostatni
            );
    }
    
    public function set_content_html($text)
    {
        $this->content_html = $text;
    }
    
    public function set_content_text($text)
    {
        $this->content_text = $text;
    }
    
    public function set_subject($subject)
    {
        $this->subject = $subject;
    }
    
    public function get_state()
    {
        return $this->state;
    }
    
    public function get_sum_emails()
    {
        return $this->sum_emails;
    }
    
    public function get_content_html()
    {
        return $this->content_html;
    }
    
    public function get_content_text()
    {
        return $this->content_text;
    }
    
    public function get_subject()
    {
        return $this->subject;
    }
    
    public function get_lang()
    {
        return $this->lang_id;
    }
    
    
    //odesila kampan podle vypocitaneho limitu
    //vstupem muze byt email - treba testovaci - na ktery bude dany email odeslan. Pokud neni email uveden, bude to odeslano na automaticky vyber emailu - celou kampan
    public function send($email_testovaci = "")
    {
       
        if(!$this->exists)
            return false;
            
        $emaily = $this->queue->get_recipients($email_testovaci, $this->custom_salutation['muz'], $this->custom_salutation['zena'],$this->custom_salutation['ostatni']);
        
           
        
        foreach($emaily AS $email => $e)
        {
            
            $template = $this->get_template($e);
            $n = new C_Newsletter($this->content_html, $this->content_text, $this->subject, $this->lang_id);
            $n->set_test($email_testovaci != '');
            $n->prepare_content($template);
            $html = $n->get_content_html();
            $text = $n->get_content_text();
            $subject = $n->get_subject();
            
            //$odeslano = (bool)rand(0,1);
            $odeslano = SendMail(EMAIL_WEBU, NAZEV_WEBU, $email, $subject,$html,$text, $this->get_attachments());
            
            if(!$n->is_test())
                $this->queue->add_attempt($e['id'], $odeslano);
                else
                return $odeslano;
                
                
        }
        
        
    }
    
    
    private function get_template($template)
    {
        global $domain;
        
        $dnes = date("d.m.Y"); 
        
        $doplneni = array(
                "dnes" => $dnes, 
                "hash_domeny" => $domain->get("hash"), 
                "id_kampane" => $this->id, 
                "nazev_kampane" => $this->name,
                "kod_kampane" => $this->code
                );
        
        $t = $template + $doplneni;
        
        return $t;
        
    }
    
    
    public function get_template_for_user($email)
    {
        global $db;
        
        $e = $this->queue->get_recipients($email);
        $t = false;
        if(count($e) == 1)
        {
            $tu = end($e);
            $t = $this->get_template($tu);
        }    
        
        return $t;
    }
    
    
    
    
    
    //pokusi se uzavrit kampan
    public function close()
    {
        global $db;
        
        if(is_null($this->queue))
            return false;
        
        if($this->queue->get_count_unsent_emails() > 0)
        {
            //aktualizace informaci o kampani a neuzavreni
            $update=array(
                'odeslano' => $this->queue->get_count_sent_emails()	
                );
            
        }
        else
        {
            //uzavreni kampane
            $emaily_data = $this->queue->get_all_emails();
            $emaily = array_keys($emaily_data);
            $nedorucitelne_emaily = $this->queue->get_undeliverable_emails();
            
            $update=array(
                'datumOdeslano'=>'now',
                'prijemci'=>implode(PHP_EOL, $emaily),
                'stav'=>'ukonceno',
                //'odeslano'=>count($emaily),
                'celkem' =>count($emaily),
                'nedoruceno' => count($nedorucitelne_emaily),
                'nedorucitelne' => count($nedorucitelne_emaily) > 0 ? implode(PHP_EOL, $nedorucitelne_emaily ) : ""
                );
            
        }
        
        
        $db->update(TABLE_EMAILY_KAMPANE,$update,'idKampane='.$this->id);
    
    
    }
    
    //vraci seznam priloh v poli
    private function get_attachments()
    {
        global $db;
        global $domain;
        
        $data = $db->Query("SELECT * FROM ".TABLE_EMAILY_PRILOHY." WHERE idKampane=".$this->id);
        $prilohy = array();
        
        while($attach = $db->getAssoc($data))
       	    $prilohy[] = PRE_PATH.$domain->getDir().USER_DIRNAME_ATTACHMENT.$attach['soubor'];
           	
        return $prilohy;
    }
 
    //vraci objekt fronty
    public function queue()
    {
        return $this->queue;
    }
    
    //zmeni stav kampane
    public function change_state($state)
    {
        global $db;
        
        $db->update(TABLE_EMAILY_KAMPANE,array("stav" => $state),"idKampane=".$this->id);
    }
 
}



class C_Newsletter{
    
    private $content_html = "";
    
    private $content_text = "";
    
    private $subject = "";
    
    //indikuje zda jde o testovaci email
    private $is_test = false;
    
    private $lang_id = 0;
    
    public function __construct($content_html, $content_text, $subject, $lang_id)
    {
        
        $this->content_html = $content_html;
        $this->content_text = $content_text;
        $this->subject = $subject;
        $this->lang_id = $lang_id;
    }
    
    public function set_test($val = true)
    {
        $this->is_test = $val;
    }
    
    public function is_test()
    {
        return $this->is_test;
    }
    
    public function prepare_content($template, $for_show_html_version = false)
    {
        global $domain;
        
        $url_odhlaseni = $domain->getUrl()."php/scripts/newsletter.logout.php?d=".$template['hash_domeny']."&p=".$template['hash']."&k=".$template['id_kampane'];
        
        $odhlaseni_lang = unserialize(stripslashes(TEXT_ODHLASENI_ODBERU_NEWSLETTERU));
        $odhlaseni_text = $odhlaseni_lang[$this->lang_id];
        $prvni = strpos($odhlaseni_text,'{');
		$posledni = strpos($odhlaseni_text,'}');
        $nazev_odkazu = substr($odhlaseni_text,$prvni+1,$posledni-$prvni-1);
        $odhlaseni_text = str_replace('{'.$nazev_odkazu.'}','<a href="'.$url_odhlaseni.'">'.$nazev_odkazu.'</a>',$odhlaseni_text);
		
        
        $url_html_verze = $template['id'] > 0 ? $domain->getUrl()."php/scripts/newsletter.show.php?d=".$template['hash_domeny']."&k=".$template['id_kampane']."&p=".$template['hash']."&html=1" : "";
        
        $html_verze_lang = unserialize(stripslashes(TEXT_HTML_VERZE_EMAILU));
        $html_verze_text = $html_verze_lang[$this->lang_id];
        $prvni = strpos($html_verze_text,'{');
		$posledni = strpos($html_verze_text,'}');
        $nazev_odkazu = substr($html_verze_text,$prvni+1,$posledni-$prvni-1);
        $html_verze_text = str_replace('{'.$nazev_odkazu.'}','<a href="'.$url_html_verze.'">'.$nazev_odkazu.'</a>',$html_verze_text);
        
        $url_test_otevreni = $template['id'] > 0 ? $domain->getUrl()."php/scripts/newsletter.open.email.php?d=".$template['hash_domeny']."&p=".$template['hash']."&k=".$template['id_kampane'] : "";
        $test_otevreni = !$for_show_html_version && $template['id'] > 0 ? "<img width='1' height='1' src='".$url_test_otevreni."' alt='' />" : "";
        
        $template['odhlaseni'] = $odhlaseni_text;
        $template['odhlaseni-url'] = $url_odhlaseni;
        $template['html-verze'] = $for_show_html_version ? "" : $html_verze_text;
        $template['html-verze-url'] = $for_show_html_version ? "" : $url_html_verze;
        
        //nahrazeni znacek v sablonach
        $this->content_html = $this->replacement($template, $this->content_html, true);
        $this->content_html .= $test_otevreni;
        
        
        
        
        
        
        $this->content_text = $this->replacement($template, $this->content_text);
        $this->subject = $this->replacement($template, $this->subject);
        
        //obaleni do html znacek
        $this->content_html = '
            <html lang="cs">
            <head>
                <title>'.$this->subject.'</title>
                <meta charset="utf-8">
            </head>
            <body>
            '.$this->content_html.'
            </body>
            </html>
            ';
        
        
    }
    
    private function replacement($template, $string, $is_html = false)
    {
        global $domain;
        
        //oznackovani odkazu google analytics parametry
        if($is_html && !$this->is_test)
            if($template['kod_kampane'] == "")
                $string = preg_replace(
                "/(<a href=\"[[:alpha:]]+:\/\/(www)?[.]?(".$domain->getName().")[\.]?[a-zA-Z]*[-_~&amp;#=\?\ .a-z0-9\/]*)/i",
                "\\1?click", 
                $string
                );
                else
                $string = preg_replace(
                    "/(<a href=\"[[:alpha:]]+:\/\/(www)?[.]?(".$domain->getName().")[\.]?[a-zA-Z]*[-_~&amp;#=\?\ .a-z0-9\/]*)/i",
                    "\\1?click&p=".$template['hash']."&k=".$template['id_kampane']."&utm_source=email&utm_medium=email&utm_campaign=".urlencode($template['nazev_kampane'])."&utm_content=".urlencode($template['kod_kampane']), 
                    $string
                    );
                /*
            $string = eregi_replace(
                "(<a href=\"[[:alpha:]]+:\/\/(www)?[.]?(".$domain->getName().")[\.]?[a-zA-Z]*[-_~&amp;#=\?\ .a-z0-9\/]*)",
                "\\1?proklik&prijemce=".$template['hash']."&kampan=".$template['id_kampane']."&utm_source=email&utm_medium=email&utm_campaign=".urlencode($template['nazev_kampane'])."&utm_content=".urlencode($template['kod_kampane']), 
                $string
                );
                */
        
        if($is_html)
            $string = str_replace("{odhlaseni}", $template['odhlaseni'], $string);
            else
            $string = str_replace("{odhlaseni}", $template['odhlaseni-url'], $string);
            
        if($is_html)
            $string = str_replace("{html-verze}", $template['html-verze'], $string);
            else
            $string = str_replace("{html-verze}", $template['html-verze-url'], $string);
            
        $string = str_replace("{dnes}", $template['dnes'], $string);
        $string = str_replace("{organizace}", $template['organizace'], $string);
        $string = str_replace("{osloveni}", $template['osloveni'], $string);
        $string = str_replace("{cele-osloveni}", $template['cele-osloveni'], $string);
        $string = str_replace("{cele-jmeno}", $template['cele_jmeno'], $string);
        $string = str_replace("{jmeno}", $template['jmeno'], $string);
        $string = str_replace("{prijmeni}", $template['prijmeni'], $string);
        $string = str_replace("{email-odesilatele}", EMAIL_WEBU, $string);
        $string = str_replace("{nazev-odesilatele}", NAZEV_WEBU, $string);
        $string = str_replace("{email-prijemce}", $template['email'], $string);
        
        
        return $string;
        
    }
    
    public function get_content_html()
    {
        return $this->content_html;
    }
    
    
    public function get_content_text()
    {
        return $this->content_text;
    }
    
    public function get_subject()
    {
        return $this->subject;
    }
    
    
    
    
    
    
    
    
}

?>