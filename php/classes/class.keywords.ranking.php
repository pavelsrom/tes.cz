<?php

/**
 * @author Pavel Srom
 * @copyright 2014
 */


class KeywordsRanking{
    
    //vyhledavaci a vyhledavaci url    
    private $search_engines = array(
        "google" => "http://ajax.googleapis.com/ajax/services/search/web?v=1.0&q=%s&rsz=8&hl=cs&userip=192.168.0.1",
        //"google" => "http://www.google.cz/search?q=%s&num=50",
        "seznam" => "http://search.seznam.cz/?q=%s&count=20"
        );
    
    //klicove slovo 
    private $keyword = "";
     
    //nazev webu, ktery se bude vyhledavat
    private $host = ""; 
    
    //pole pro ulozeni do db
    private $insert = array();
     
     
    //konstruktor
    public function __construct($keyword, $host="")
    {

        $this->host = $host == "" ? $_SERVER['HTTP_HOST'] : $host;
                
        if(!strstr($this->host,"www."))
            $this->host = "www.".$this->host;
                
        $this->modify_keyword($keyword);
    }
   
    
    //nastavi klicove slovo pro vyhledavani
    public function set_keyword($keyword)
    {
        $this->modify_keyword($keyword);
    }
    
    
    //upravi klicove slovo pro zarazeni do url
    private function modify_keyword($keyword = "")
    {
        $keyword = $keyword == "" ? $this->keyword : $keyword;
        $keyword = trim($keyword);
        $this->keyword = str_replace(" ","+", $keyword);
        
    }
    
    //vraci pozici v google.cz
    /*
    public function get_position_in_google($content="")
    {
        $url = sprintf($this->search_engines["google"],$this->keyword);
        //prochazime maximalne 1 stranku po 50 vysledcich
        for($i = 1; $i <= 1; $i++)
        {
        
            $od = ($i - 1) * 50;
            $from = "&start=".$od;

            $content = $content!='' ? $content : $this->get_content($url.$from);           
            $vsechny_vysledky = htmlqp($content,"body > p a");

            $odkazy = array();          
            foreach($vsechny_vysledky AS $a)
            {
                $href = $a->attr("href");
                $h = explode("url?q=",$href);
                $odkaz = "";
                if(count($h) == 2)
                {
                    $h1 = explode("&sa=", $h[1]);
                    if(count($h1) == 2)
                    {
                        $odkaz = $h1[0];
                    }
                }

                if($odkaz == "")
                    continue;

                $odkazy[] = trim($odkaz);
            }
            
            $pozice = $this->get_position($odkazy, $i);
            
            if($pozice === false)
                continue;
            
            $pozice = $pozice + $od;
            
            if($pozice > 0)
            {
                return $pozice;
            }
            
            
        
        }
        
        return 0;
        
    }
    */
    
    
    public function get_position_in_google()
    {
        
        $url = sprintf($this->search_engines["google"],urlencode($this->keyword));
        //prochazime maximalne 7 stranek po 8 vysledcich
        for($i = 1; $i <= 7; $i++)
        {
        
            $od = ($i - 1) * 8;
            $from = "&start=".$od;

            $content = $this->get_content($url.$from, false);

            $result = json_decode($content);
            //print_r($result);
            $odkazy = array();
            
            if(!$result->responseData)
                return false;
                      
            foreach($result->responseData->results AS $r)
            {
                $odkazy[] = $r->unescapedUrl;// $r->visibleUrl;
                //print_r($r);
            }
            
            //print_r($odkazy);
            
            $pozice = $this->get_position($odkazy, $i);
            
            if($pozice === false)
                continue;
            
            $pozice = $pozice + $od;
            
            if($pozice > 0)
            {
                return $pozice;
            }
            
            
        
        }
        
        return 0;
        
    }
    
    //vraci pozici v seznam.cz
    public function get_position_in_seznam()
    {
        $url = sprintf($this->search_engines["seznam"],$this->keyword);
        //prochazime maximalne 3 stranky po 20 vysledcich
        for($i = 1; $i <= 3; $i++)
        {
        
            $od = ($i - 1) * 20;
            $from = "&from=".$od;

            $content = $this->get_content($url.$from);
            $qp = htmlqp($content,".results .modCont");
            
            $vsechny_vysledky = $qp->not(".skliktop")->find(".text h3 a");
            
            if($vsechny_vysledky->length == 0)
                return -1;
            
            $odkazy = array();
            foreach($vsechny_vysledky AS $a)
            {
                $odkazy[] = $a->attr("href");
            }

            $pozice = $this->get_position($odkazy, $i);           
            
            if($pozice === false)
                continue;
            
            $pozice = $pozice + $od;
            
            if($pozice > 0)
            {
                return $pozice;
            }
        
        }
        
        return 0;
        

    }
    
    
    //vraci pozici z nalezenych vysledku
    private function get_position($links)
    {
        $position = 1;
        foreach($links AS $href)
        {
            $d = parse_url($href);
            $host = $d['host'];
            
            if($host == $this->host)
            {
                return $position;
            }
            
            $path = $d['path'];
            $position++;
            
        }
        
        return false;
        
        
        
    }
    
    
    //vraci odezvu od vyhledavacu
    private function get_content($url, $delay = true)
    {
                   
        $s = rand(2, 6); //cekani 2 - 5 sekund, simulovani lidskeho vyhledavani
        if($delay)
            sleep($s);
               
        $referer = "http://".$this->host."/dt=".time();
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 5.1; rv:28.0) Gecko/20100101 Firefox/28.0');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 15);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        curl_setopt($ch, CURLOPT_NOBODY, false);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_REFERER, $referer);
        //curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $response = curl_exec($ch);
        //echo curl_error($ch);
        curl_close($ch);
        
        
        
        return $response;
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}



?>