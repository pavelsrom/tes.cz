<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */


class TableCell
{
    //obsah
    private $content = "";
    
    //class
    private $class = "";
    
    //id
    private $id = "";
    
    //typ bunky td nebo th
    private $type = "";
    
    //id napovedy
    private $id_help = 0;
    
    //colspan
    private $colspan = 0;
    
    //inline style
    private $style = "";
    
    //konstruktor
    public function __construct($type='td',$content = "", $class = "", $id = "", $id_help=0, $colspan=0,$style="") 
    {
        $this->content = $content;
        $this->class = $class;
        $this->id = $id;
        $this->type = $type;
        $this->id_help = $id_help;
        $this->colspan = $colspan;
        $this->style = $style;
    } 
    
    
    //vraci html kod td bunky
    public function get_html()
    {
        $id = $this->id == "" ? "" : " id='".$this->id."'";
        $class = $this->class == "" ? "" : " class='".$this->class."'";
        $help = $this->id_help > 0 ? " ".help($this->id_help) : "";
        $colspan = $this->colspan > 0 ? " colspan='".$this->colspan."'" : "";
        $style = $this->style != "" ? " style='".$this->style."'" : "";
        
        return "<".$this->type.$id.$class.$colspan.$style.">".$this->content.$help."</".$this->type.">";
    }
    
}


class TableRow
{
    //id
    private $id = "";
    
    //class
    private $class = "";

    //style
    private $style = "";
    
    //polozky td
    private $cells = array();
    
    //citac polozek
    private $cells_count = 0;
    
    //typ radku head, body, foot
    private $type = "";
    
    //konstruktor
    public function __construct($class = "", $id = "", $type="", $style = "")
    {
        $this->class = $class;
        $this->id = $id;
        $this->type = $type;
        $this->style = $style;
    }
    
    //vytvori bunku td
    public function add($content = "", $class = "", $id_help=0, $id = "", $colspan=0,$style="")
    {
        $this->cells_count++;
        $idc = $this->cells_count;
        $this->cells[ $idc ] = new TableCell($this->type == 'head' ? "th" : "td",$content, $class, $id, $id_help,$colspan,$style);       
        return $this;
    }
    
    //vytvori bunku td
    public function add_td($content = "", $class = "", $id_help=0, $id = "", $colspan=0)
    {
        $this->cells_count++;
        $idc = $this->cells_count;
        $this->cells[ $idc ] = new TableCell("td",$content, $class, $id, $id_help,$colspan);       
        return $this;
    }
    
    //vytvori bunku th
    public function add_th($content = "", $class = "", $id_help=0, $id = "", $colspan=0)
    {
        $this->cells_count++;
        $idc = $this->cells_count;
        $this->cells[ $idc ] = new TableCell("th",$content, $class, $id, $id_help, $colspan);       
        return $this;
    }
    
    //nastavi hodnoty bunek td
    public function add_values()
    {
        $val = func_get_args();
        foreach($val AS $v)
            $this->add($v);
    }
    
    
    //vraci html kod radku tr
    public function get_html()
    {
        
        $id = $this->id == "" ? "" : " id='".$this->id."'";
        $class = $this->class == "" ? "" : " class='".$this->class."'";
        $style = $this->style == "" ? "" : " style='".$this->style."'";
        
        $td = array("");
        foreach($this->cells AS $c)
            $td[] = $c->get_html();
        
        //print_r($td);
        
        return "<tr".$id.$class.$style.">".implode("\n\t\t",$td)."</tr>";
    }
    
    public function get_cell_count()
    {
        return $this->cells_count;
    }
    
    
    
}



class Table
{
    
    //id
    private $id;
    
    //class
    private $class = "";
    
    //polozky td
    private $rows = array();
    
    //polozky radku v headeru tabulky
    private $rows_head = array();
    
    //citac polozek
    private $rows_count = 0;
    
    //citac polozek pro head
    private $rows_head_count = 0;
    
    //hlaska pokud je tabulka prazdna
    private $empty_table_message = "";
    
    //inline style
    private $style = "";
    
    //konstruktor
    public function __construct($id = "", $class = "table", $empty_table_message="" )
    {
        $this->class = $class;
        $this->id = $id;
        $empty_table_message = $empty_table_message == "" ? TNENALEZEN_ZADNY_ZAZNAM : $empty_table_message;
        $this->set_empty_table_message($empty_table_message);
    }
    
    //nastavi hlasku, ktera se zobrazi kdyz bude tabulka prazdna
    public function set_empty_table_message( $message = "")
    {
        $this->empty_table_message = $message;
    }
    
    //nastavuje inline styl tabulce
    public function set_style($inline_style)
    {
        $this->style = $inline_style;
    }
    
    //vraci html kod tabulky
    public function get_html()
    {
        $id = $this->id == "" ? "" : " id='".$this->id."'";
        $class = $this->class == "" ? "" : " class='".$this->class."'";
        $style = $this->style == "" ? "" : " style='".$this->style."'";
        $tr_html = [];
        $tr_head_html = "";

        if(count($this->rows_head) > 0)
        {
            foreach($this->rows_head AS $r)
                $tr_html[] = $r->get_html();
                
            $tr_head_html = implode("\n\t", $tr_html);    
        }
        
        if(count($this->rows) > 0)
        {
            foreach($this->rows AS $r)
                $tr[] = $r->get_html();
                
            $tr_html = implode("\n\t", $tr);    
        }
        else
            $tr_html = "<tr><td colspan='".$this->get_colls_count()."'>".$this->empty_table_message."</td></tr>";
        
        return "<table".$id.$class.$style."><thead>".$tr_head_html."</thead><tbody>".$tr_html."</tbody></table>\n";
    }
    
    //vraci pocet sloupcu tabulky
    private function get_colls_count()
    {
        $count = isset($this->rows[1]) ? $this->rows[1]->get_cell_count() : 0;
        $count = $count == 0 && isset($this->rows_head[1]) ? $this->rows_head[1]->get_cell_count() : $count;
        return $count;
    }
    
    //vytvori tr radek
    public function tr($class = "", $id = "", $style = "")
    {
        $this->rows_count++;
        $idr = $this->rows_count;
        $this->rows[ $idr ] = new TableRow($class, $id, "", $style);
        return $this->rows[ $idr ];
    }
    
    
    
    public function tr_head($class = "", $id = "")
    {
        $this->rows_head_count++;
        $idr = $this->rows_head_count;
        $this->rows_head[ $idr ] = new TableRow($class, $id,"head");
        return $this->rows_head[ $idr ];
    }
    
    
}

?>