<?php

/**
 * @author Pavel Srom
 * @copyright 2013
 */



class C_ObjectAccess{
    
    
    //indikuje zda bude ke strance pristup vzdy
    private $access_always = false;
    
    //obsahuje v poli id povolenych stranek daneho uzivatele
    private $allowed_objects = array();
    
    //id prihlaseneho uzivatele
    private $user_id = 0;
    
    
    
    //konstruktor
    public function __construct()
    {
        global $login_obj;
        
        $this->access_always = !$login_obj->UserPrivilege('only_my');
        $this->user_id = $login_obj->getId();
        
        if($this->access_always) return;
        
        $this->set_allowed_objects();

    }
    
    
    
    //nastavi id povolenych stranek
    private function set_allowed_objects()
    {
        global $db;
        
        $data = $db->query("SELECT idObjektu AS id, typ 
            FROM ".TABLE_STRANKY_EDITORI." 
            WHERE idUzivatele=".$this->user_id);
        
        while($s = $db->getObject($data))
        {
            
            $this->allowed_objects[$s->typ][] = $s->id;
        }
        
        
        
        
    }
    
    
    //prida stranku nebo stranky v poli mezi povolene stranky
    //pokud je nastaven parametr rewrite na true, pak budou vsechny zaznamy smazany a nahrazeny novymi
    public function add_allowed_object($object_id, $object_type='stranka', $rewrite = false)
    {
        global $db;
        
        $object_id = (array) $object_id;
        
        $this->allowed_objects[] = $object_id;
        
        if($rewrite)
            $db->delete(TABLE_STRANKY_EDITORI,"WHERE idUzivatele=".$this->user_id);
        
        foreach($object_id AS $id)
        {
            if(!$rewrite)
                $existuje = $db->get(TABLE_STRANKY_EDITORI,'idUzivatele',"idObjektu=".$id." AND idUzivatele=".$this->user_id);
                else
                $existuje = 0;
        
            $existuje = intval($existuje);
            if($existuje == 0)
            {
                $db->insert(TABLE_STRANKY_EDITORI,array('idUzivatele' => $this->user_id, 'idObjektu' => $id, 'typ' => $object_type));
            }
        }
        
    }
    
    
    //overi zda ma prihlaseny uzivatel pristup ke strance s danym id 
    public function has_access($object_id, $object_type = 'stranka')
    {
        $object_id = intval($object_id);
        $is_allowed = isset($this->allowed_objects[$object_type]) ? in_array($object_id, $this->allowed_objects[$object_type]) : false;
        return $this->access_always || $object_id == 0 || $is_allowed;
        
    }
    

    
    
}


?>