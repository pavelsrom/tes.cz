<?php

/**
 * @author Pavel Srom
 * @copyright 2010
 */


class C_Rss{
    
    public $items;
    public $exists;
    
    private $aktualni_stranka;
    private $celkovy_pocet_polozek;
    
    //mozne typy stranka, nahodne-clanky, nejnovejsi-clanky, nejlepsi-clanky, clanek, souvisejici, rubriky
    public function __construct($typ='stranka'){
        global $db;
        global $domain;
        global $links;
        
        $this->exists = false;
        $this->aktualni_stranka = isset($_GET['stranka'])?intval($_GET['stranka']):1;
        $this->aktualni_stranka = $this->aktualni_stranka <= 0 ? 1 : $this->aktualni_stranka;
            
        $this->celkovy_pocet_polozek = 0;
                
                
        if($typ == 'stranka'){
            $limit = ($this->aktualni_stranka - 1) * POCET_CLANKU_NA_STRANKU.",".POCET_CLANKU_NA_STRANKU;
            }
            else{
                
            $limit = POCET_NEJLEPSICH_CLANKU_V_BOXU;
            //$where = " AND CURDATE()>=c.datum";
            }
            
        $data = $db->Query("SELECT c.titulek, c.perex, c.url, IF(c.datum IS NOT NULL, DATE_FORMAT(c.datum,'%d.%m.%Y %H:%i'),'') AS datum, IF(c.datum IS NOT NULL, DATE_FORMAT(c.datum,'%H:%i'),'') AS cas, adresa AS web, DATE_FORMAT(c.datum,'%d') AS den, DATE_FORMAT(c.datum,'%m') AS mesic, DATE_FORMAT(c.datum,'%Y') AS rok 
                FROM ".TABLE_RSS." AS c
                ORDER BY c.datum DESC
                LIMIT ".$limit."
    		  ");
              
        if($db->numRows($data)>0)
            $this->exists = true;


        
        $celkem_data = $db->Query("SELECT c.idPolozky
                FROM ".TABLE_RSS." AS c
                GROUP BY c.idPolozky
    		  ");
        $this->celkovy_pocet_polozek = $db->numRows($celkem_data);      
        
        $i = 0;
        while($a = $db->getAssoc($data)){
							
			$this->items[$i] = $a;
            $i++;
            }
            
        
        
    }
    
    
    public function GetArticlesOnPage(){
        global $domain;
        global $admin;
       
		$html = "";
		
        
        if(count($this->items)==0) return $html; 
        
		$pocet = POCET_CLANKU_NA_STRANKU;
		
        $session = 'clanky';
		$stranka = $this->aktualni_stranka;
		$od = ($stranka-1) * $pocet;
		
		$html .= "<span id='clanky'></span>";
        
        $pocet_stranek = ceil($this->celkovy_pocet_polozek/$pocet);

        $html .= GetPaging($pocet_stranek, $stranka, "#clanky",4,"nahore");
        //$html .= "<br />";
		$i = 0;

		foreach($this->items AS $id => $pol){

            
            $url = parse_url($pol['url']);
            
            $html .= "<div class='clanek'>";
            $html .= '<h2 class="titulek"><a href="'.$pol['url'].'" target="_blank" title="Přečíst celý článek">'.secureString($pol['titulek']).'</a></h2>';
            
            $html .= '<p class="clanek_info">
                    <span class="icon_datum icon">'.$pol['datum'].'</span>&nbsp;&nbsp;|&nbsp;&nbsp;
                    <span class="icon_rubrika icon"><a href="'.PROTOKOL.$url['host'].'">'.$pol['web'].'</a></span>
                </p>';
                                        						
			$html .= "<p>".strip_tags($pol['perex'],'<b><strong><i><em><a>')."... <a href='".$pol['url']."' target='_blank' > Číst více</a></p>";
           
            $html .= "</div>";
			$i++;
			}
		
		//strankovani
	
		

		$html .= GetPaging($pocet_stranek, $stranka, "#clanky",4,"dole");   
        
        
		return $html;
        
        
    }
    
    public function GetBox(){
        global $domain;	
		global $admin;	
		global $links;
		
		$html = "";
        $html .= "<h3>".TSLEDUJEME_PRO_VAS_SIT."</h3>";
        $html .= "<div class='posledni_komentare nej_clanky nahodne-clanky'>";
        
		$html .= "<table class='tRss'>";
        	
		foreach($this->items AS $i){	
	
			$html .= "<tr>";
            $html .= "<td><em>".$i['cas']."</em>&nbsp;&nbsp;</td>";
			if($i['url']!='')
				$html .= "<td><a href='".$i['url']."' target='_blank'>".secureString($i['titulek'])."</a>, <b>".$i['web']."</b></td>";
				else
				$html .= "<td>".secureString($i['titulek'])."</td>";
			
            $html .= "</tr>";

			}

        $html .= "</table>";
        
        //$html .= "<p ><br /><a class='viceInformaci' style='float: right;' href='".$links->getUrl(ID_STRANKY_RSS)."'>Zobrazit více článků z internetu</a><br class='cleaner'/></p>";
        
        $html .= "</div>";        

		return $html;
        
        
        
        
    }
    
    private function getCeskyMesic($c)
    {
        switch($c){
            case 1: return "leden";
            case 2: return "únor";
            case 3: return "březen";
            case 4: return "duben";
            case 5: return "květen";
            case 6: return "červen";
            case 7: return "červenec";
            case 8: return "srpen";
            case 9: return "září";
            case 10: return "říjen";
            case 11: return "listopad";
            case 12: return "prosinec";
        }
        
        return "";
    }
    
    
    
    
    
}




?>