<?php

/**
 * @author pavel šrom
 * @copyright 2018
 */

use GoPay\Definition\Language;
use GoPay\Definition\Payment\Currency;
use GoPay\Definition\Payment\PaymentInstrument;
use GoPay\Definition\Payment\BankSwiftCode;
use GoPay\Definition\Payment\Recurrence;


class C_Gopay{
    
    //vytvorene spojeni s GoPay
    private $gopay;
    
    //vysledek volani api
    private $response;
    
    public function __construct()
    {
              
        
        $this->gopay = GoPay\payments([
            'goid' => GOPAY_GOID,
            'clientId' => GOPAY_CLIENT_ID,
            'clientSecret' => GOPAY_CLIENT_SECRET,
            'isProductionMode' => GOPAY_OSTRY_PROVOZ,
            'language' => Language::CZECH,
            'scope' => 'payment-all'
        ]); 
        
        
        
    }
    
    
    
    public function create($id_objednavka, $objednavka, $objednavka_polozky, $opakovana_platba = false)
    {
        global $links;
        //global $predplatne;
        global $domain;
        
        $items = array();
        foreach($objednavka_polozky AS $p)
        {
            $items[] = array(
                "name" => $p['nazev'],
                "amount" => ($p['cena_s_dph'] * $p['pocet'])."00", //castka v halerich
                "count" => $p['pocet']
                );
        }
        
        
        
        $platba = array(
            'amount' => $objednavka['celkem_s_dph']."00", //castka v halerich
            'currency' => Currency::CZECH_CROWNS,
            'order_number' => $objednavka['cislo_faktury'],
            'order_description' => '',
            'items' => $items,
            'callback' => array(
                'return_url' => $domain->getUrl()."gopay-return",
                'notification_url' => $domain->getUrl()."gopay-notify"
            ),
            'lang' => Language::CZECH, // if lang is not specified, then default lang is used
        );
        
        
        
        if($opakovana_platba)
        {
            /*
            $platba['recurrence'] = array(
                        'recurrence_cycle' => 'MONTH',//Recurrence::MONTH,
                        'recurrence_period' => $predplatne->get_type() == 1 ? "12" : "1",
                        'recurrence_date_to' => '2030-12-31'
                    );
            */
            
            $platba['recurrence'] = array(
                        'recurrence_cycle' => 'ON_DEMAND',//Recurrence::MONTH,
                        'recurrence_period' => 1,
                        'recurrence_date_to' => '2030-12-31'
                    );        
                    
                    
                    
        }
        
       
        $this->response = $this->gopay->createPayment($platba);
        
        return $this->response;
        
    }
    
    //provede automatickou platbu
    public function automatic_payment($id_rodicovske_platby, $id_objednavka, $objednavka, $objednavka_polozky)
    {
        
        global $domain;
        
        $items = array();
        foreach($objednavka_polozky AS $p)
        {
            $items[] = array(
                "name" => $p['nazev'],
                "amount" => ($p['cena_s_dph'] * $p['pocet'])."00", //castka v halerich
                "count" => $p['pocet']
                );
        }
        
        $platba = array(
            'amount' => $objednavka['cena_s_dph']."00", //castka v halerich
            'currency' => Currency::CZECH_CROWNS,
            'order_number' => $objednavka['cislo_faktury'],
            'order_description' => '',
            'items' => $items
            //'lang' => Language::CZECH, // if lang is not specified, then default lang is used
        );
        
        
        $this->response = $this->gopay->createRecurrence(
            $id_rodicovske_platby,
            $platba
        );
        
        return $this->response;
        
        
    }
    
    
    //zrusi automaticke strhavani plateb
    public function cancel_recurrence_payments($id_gopay_platby)
    {
        $this->response = $this->gopay->voidRecurrence($id_gopay_platby);
        
        return $this->response;
    }
    
    
    
    public function get_status($id_gopay_platby)
    {
        
        $this->response = $this->gopay->getStatus($id_gopay_platby);
        
        return $this->response;
    }
    
    

    
    
    
    
    
}

?>