<?php

/**
 * @author Pavel Srom
 * @copyright 2014
 */


//trida zpracovava stavy panelu pro inline editaci
class InlineEditPanel{
    
    //indikuje zda je panel zmenseny nebo v plne velikosti
    private $hidden = true;
    
    //urcuje vychozi pozici panelu
    private $position = "top";
    
    //nazev session ve ktere jsou ulozeny informace o stavu panelu
    private $session_name = "ine_settings";
    
    
    //konstruktor
    public function __construct()
    {
        if(isset($_SESSION[$this->session_name]))
        {
            $s = $_SESSION[$this->session_name];
            $this->hidden = isset($s['hidden']) ? $s['hidden'] : $this->hidden;
            $this->position = isset($s['position']) ? $s['position'] : $this->position;
        }
        else
        $_SESSION[$this->session_name] = array(
            'hidden' => $this->hidden,
            'position' => $this->position
            );
        
    }
    
    //nastavi panelu priznak skryty
    public function set_hidden($value = true)
    {
        $this->hidden = $value;
        $_SESSION[$this->session_name]['hidden'] = $value;
    }
    
    //vraci true, pokud je panel skryty. V opacnem pripade vraci false
    public function is_hidden()
    {
        return $this->hidden;        
    }
    
    //vraci aktualni pozici panelu
    public function get_default_position()
    {
        return $this->position;
    }
    
    //nastavi aktualni pozici
    public function set_default_position($value = "top")
    {
        $this->hidden = $value;
        $_SESSION[$this->session_name]['position'] = $value;
    }
    
    
    
    
    
    
    
}

?>