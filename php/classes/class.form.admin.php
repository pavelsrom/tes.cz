<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */


class FormItem
{
    
    //jmeno polozky
    private $item_name;
    
    //atribut type
    private $type = "";
    
    //atribut name
    private $name = "";
    
    //atribut value
    private $default_value = "";
    
    //values - hodnoty pro selectboxy, skupinu radiobuttonu nebo checkboxu
    private $values = array();
    
    //html kod formularoveho prvku
    private $code = "";
    
    //trida prvku
    private $class = "";
    
    //id prvku
    private $id = "";
   
    //poznamka
    private $note = "";
    
    //indikuje zda je polozka povinna
    private $req = false;
    
    //id helpu
    private $help_id = 0;
    
    //indikuje zda je prvek disablovan
    private $disabled = false;
    
    private $disabled_values = array();

    
    //class bunky td obsahujici input
    public $td_class = "";
    
    //class bunky th obsahujici nazev
    public $th_class = "";
    
    //class radku tr
    public $tr_class = "";
    
    //selectbox obsahuje optgroups
    private $has_optgroups = false;
    
    //onclick udalost
    private $onclick = "";
    
    //onselect udalost
    private $onchange = "";
    
    //text pred prvkem
    private $text_before;
    
    //text za prvkem
    private $text_after;
    
    //urcuje parametr maxlength
    private $maxlength = 0;
    
    //urcuje zda se zobrazi wysiwyg editor tools, nastavuje se pro editor
    private $wet = false;
    
    //urcuje zda se budou vstupy osetrovat ci nikoliv
    private $secureString = true;
    
    //obsahuje javascriptovy kod pro ckeditor a ckfinder
    public $wysiwyg_js = "";
    
    //vypise checkboxy pod sebou, jinak je vypise ve trech sloupcich;
    private $list_below = false; 
    
    //pokud je true, pak oznaci prvek tridami jquery ui
    private $jquery_ui = false;
        
    //promenna form item id, obsahuje poradi inputu
    private static $sfid = 0;
        
    //promenna obsahuje aktualni poradi inputu
    private $fid = 0;
        
    //multiple pole - selectbox, file
    private $multiple = false;
        
    //indikuje zda se pouzije ckeditor nastaveny pro emailove kampane
    private $editor_for_mailing = false;
        
    private $readonly = false;
        
    //html kod pred a za prvkem
    private $after = "";
    private $before = "";
    
    
        
    //konstruktor
    public function __construct($item_name="", $type="", $name="", $default_value="", $values="", $id="", $class="", $note="", $req = false, $help_id=0, $show_optgroups=false, $list_below = false)
    {
        $this->item_name = $item_name;
        $this->type = $type;
        $this->name = $name;
        $this->default_value = $default_value;
        $this->values = $values;
        $this->id = $id;
        $this->class = $class;
        $this->note = $note;
        $this->req = $req;
        $this->help_id = $help_id;
        $this->has_optgroups = $show_optgroups;
        $this->list_below = $list_below;
        $this->fid = self::$sfid++;
        
    }
    
    public function get_fid()
    {
        return $this->fid;
    }
    
    public function set_before($html)
    {
        $this->before = $html;
        return $this;
    }
    
    public function set_after($html)
    {
        $this->after = $html;
        return $this;
    }
    
    public function get_before()
    {
        return $this->before;
    }
    
    public function get_after()
    {
        return $this->after;
    }
    
    
    
    
    
    
    public function allow_jquery_ui($val = true)
    {
        $this->jquery_ui = (bool)$val;
        return $this;
    }
    
    //nastavi text, ktery se vypise pred prvkem
    public function allow_wet()
    {
        $this->wet = true;
        return $this;
    }
    
    public function set_multiple($val = true)
    {
        $this->multiple = $val;
        return $this;
    }
    
    public function set_readonly($val = true)
    {
        $this->readonly = $val;
        return $this;
    }
    
    public function for_mailing($val = true)
    {
        $this->editor_for_mailing = $val;
        return $this;
    }
    
    public function setSecureString($val = true)
    {
        $this->secureString = (bool)$val;
        return $this;
    }
    
    //povoli / zakaze vypis checkboxu pod sebou
    public function setListBelow($val = true)
    {
        $this->list_below = $val;
        return $this;
    }
   
    //nastavi text, ktery se vypise pred prvkem
    public function add_text_before($text)
    {
        $this->text_before = $text;
        return $this;
    }
    
    //nastavi text, ktery se vypise po prvku
    public function add_text_after($text)
    {
        $this->text_after = $text;
        return $this;
    }
   
    //nastavi class inputu
    public function add_class($class)
    {
        $this->class .= " ".$class;
        return $this;
    }
    
    //nastavi atribut maxlength
    public function set_maxlength($maxlength)
    {
        $this->maxlength = $maxlength;
        return $this;
    }
    
    //nastavi class pro bunku td obsahujici input
    public function set_class_td($class)
    {
        $this->td_class = $class;
        return $this;
    }
    
    //nastavi class pro bunku td obsahujici input
    public function set_class_th($class)
    {
        $this->th_class = $class;
        return $this;
    }
    
    //nastavi class pro bunku td obsahujici input
    public function set_class_tr($class)
    {
        $this->tr_class = $class;
        return $this;
    }
   
    //nastavi polozku jako disablovanou
    public function set_disabled($val = true)
    {
        $this->disabled = $val;
        return $this;
    }
    
        //nastavi polozky jako disablovanou
    public function set_disabled_values($values)
    {
        $this->disabled_values = $values;
        return $this;
    }

    
    //nastavi js udalost onclick
    public function set_onclick_event($code)
    {
        $this->onclick = $code;
        return $this;
    }
    
    //nastavi js udalost onselect
    public function set_onchange_event($code)
    {
        $this->onchange = $code;
        return $this;
    }
   
    //nastavi polozku jako povinnou
    public function set_req($val = true)
    {
        $this->req = $val;
        return $this;
    }
    
    //nastavi class
    public function set_class($class = "")
    {
        $this->class = $class;
        return $this;
    }
    
    //nastavi id polozky
    public function set_id($id = "")
    {
        $this->id = $id;
        return $this;
    }
    
    //nastavi poznamku
    public function set_note($note = "")
    {
        $this->note = $note;
        return $this;
    }
    
    //nastavi napovedu polozky
    public function set_help($help_id = 0)
    {
        $this->help_id = $help_id;
        return $this;
    }
    
    //vraci nazev tr class
    public function get_class_tr()
    {
        return $this->tr_class;
    }
    
    //nastavi html kod prvku formulre
    private function set_html_code()
    {
        $id = $this->id == "" ? "" : "id='".$this->id."'";
        $disabled = $this->disabled ? "disabled='disabled'" : "";
        $id_multi = str_replace("]", "", $this->name);
        $id_multi = str_replace("[", "", $id_multi);
        $onclick = $this->onclick != "" ? " onclick=\"".$this->onclick."\" " : ""; 
        $onchange = $this->onchange != "" ? " onchange=\"".$this->onchange."\" " : "";
        $tr_class = $this->tr_class != "" ? "class='".$this->tr_class."'" : "";
        $maxlength = $this->maxlength != "" && $this->maxlength > 0 ? "maxlength='".$this->maxlength."'" : "";
        $this->code = $this->text_before;
        $multiple = $this->multiple ? "multiple" : "";
        $readonly = $this->readonly ? "readonly" : "";
        //osetreni vstupu
        if(is_array($this->values) && $this->secureString)
        {
            foreach($this->values AS $idv => $v)
                if(!is_array($v))
                    $this->values[$idv] = secureString($v);
        }
        elseif(in_array($this->type, array('text','password','textarea','submit','button')) && $this->secureString)
            $this->default_value = secureString($this->default_value);
           
        if($this->is_req() && $this->jquery_ui)
            $this->class .= " req ";
        
        switch($this->type)
        {
            case "text": 
                {
                    if($this->jquery_ui)
                        $this->class .= " ui-widget-content ui-corner-all";
                        
                    $this->code .= "<input type='text' name='".$this->name."' value='".$this->default_value."' class='input w100 ".$this->class."' ".$id." ".$disabled." ".$maxlength." />";
                    break;
                }
            
            case "file": 
                {
                    if($this->jquery_ui)
                        $this->class .= "ui-widget-content ui-corner-all";
                        
                    
                        
                    $this->code .= "<input type='file' name='".$this->name."' value='".$this->default_value."' class='input w100 ".$this->class."' ".$id." ".$disabled." ".$maxlength." ".$multiple."/>";
                    break;
                }
                
            case "password": 
                {
                    if($this->jquery_ui)
                        $this->class .= "ui-widget-content ui-corner-all";
                        
                    $this->code .= "<input type='password' name='".$this->name."' value='".$this->default_value."' class='input w100 ".$this->class."' ".$id." ".$disabled." ".$maxlength." />";
                    break;
                }
            case "textarea": 
                {
                    if($this->jquery_ui)
                        $this->class .= "ui-widget-content ui-corner-all";
                        
                    $this->code .= "<textarea cols='' rows='' name='".$this->name."' class='w100 ".$this->class."' ".$id." ".$disabled." ".$readonly.">".$this->default_value."</textarea>";
                    break;
                }
            case "editor": 
                {
                    if($this->wet)
                    {
                        ob_start();
                        $wysiwygEditorId = $this->id;
                        include("php/classes/wysiwygEditorTools/wet.php");
                        $this->code .= ob_get_clean();
                    } 
                    $this->code .= "<textarea cols='' rows='' name='".$this->name."' class='w100 ".$this->class."' ".$id." ".$disabled.">".htmlspecialchars($this->default_value)."</textarea>";
                    
                    //prida js kod pro ckeditor
                    
                    $this->add_wysiwyg_editor_code($this->name);
                    
                    break;
                }
            case "radio": {
                    $i = 1;
                    foreach($this->values AS $value => $name_item)
                    {
                        $checked = $value == $this->default_value ? "checked='checked'" : "";
                        $this->code .= "<input type='radio' value='".$value."' name='".$this->name."' class='radio ".$this->class."' id='".$id_multi.$i."' ".$checked." ".$disabled."/> <label for='".$id_multi.$i."'>".$name_item."</label> ";
                        $i++;
                    }
                    
                break;
                }  
            case "checkbox": {
                    $i = 0;
                    $pocet_checkboxu = count($this->values);
                    $pocet_sloupcu = $this->list_below ? 1 : 3;
                    $pocet_radku = ceil($pocet_checkboxu / $pocet_sloupcu);
                    $sirka = round(100 / $pocet_sloupcu,2); 
                    $style = $this->list_below ? "" : "style='width:".$sirka."%; float:left'";
                    
                    foreach($this->values AS $value => $name_item)
                    {
                        $checked = in_array($value, (array) $this->default_value) ? "checked='checked'" : "";
                        
                        $dis = $disabled;
                        if($dis == '')
                            $dis = in_array($value, $this->disabled_values) ? 'disabled="disabled"' : '';

                        
                        /*
                        if($i%$pocet_radku == 0)
                            $this->code .= "<div class='f-left'><div class='checkbox_list'>";
                        */
                        
                        $this->code .= "<div ".$style." class='checkbox-div ".(is_array($this->class) ? $this->class[$value]:$this->class)."'>";
                        $this->code .= "<div class='checkbox-div-in'>";
                        
                        //$this->code .= "<input type='checkbox' value='".$value."' name='".$this->name."[]' class='checkbox' id='".(is_array($this->id)?$this->id[$value] : $id_multi.$i.$value)."' ".$checked." ".$disabled."/> <label for='".(is_array($this->id)?$this->id[$value] : $id_multi.$i.$value)."'>".$name_item."</label>";
                        $this->code .= "<input type='checkbox' value='".$value."' name='".$this->name."[]' class='checkbox' id='".(is_array($this->id)?$this->id[$value] : $id_multi.$i.$value)."' ".$checked." ".$dis."/> <label for='".(is_array($this->id)?$this->id[$value] : $id_multi.$i.$value)."'>".$name_item."</label>";

                        $this->code .= "</div>";
                        $this->code .= "</div>";
                        
                        $i++;
                        
                        /*
                        if($i%$pocet_radku == 0)
                            $this->code .= "</div></div>";
                        */
                        
                    }
                    $this->code .= "<div class='cleaner'></div>";
                    break;
                }
            case "selectbox": {
                    $i = 1;
                    
                    if($this->jquery_ui)
                        $this->class .= "ui-widget-content ui-corner-all";
                        
                    $this->code .= "<select name='".$this->name."' size='1' class='select ".$this->class."' ".$id." ".$disabled." ".$onchange." ".$onclick.">";

                    if($this->has_optgroups)
                    {
                        foreach($this->values AS $optgroups_name => $val)
                        {
                            if($optgroups_name != '')
                                $this->code .= "<optgroup label='".$optgroups_name."'>";
                            
                            foreach($val AS $value => $name_item)
                            {
                                $selected = $value == $this->default_value ? "selected='selected'" : "";
                                $this->code .= "<option value='".$value."' ".$selected." >".$name_item."</option>";
                                $i++;
                            }
                            
                            if($optgroups_name != '')
                                $this->code .= "</optgroup>";
                        }
                    }
                    else
                    {
                        foreach($this->values AS $value => $name_item)
                        {
                            $selected = $value == $this->default_value ? "selected='selected'" : "";
                            $this->code .= "<option value='".$value."' ".$selected.">".$name_item."</option>";
                            $i++;
                        }
                    }
                    
                    
                    $this->code .= "</select>";
                    break;
                }
            case "multiple_selectbox": {
                    $i = 0;
                    
                    $this->code .= "<select name='".$this->name."[]' multiple='multiple' class='select ".$this->class."' ".$id." ".$disabled." ".$onchange." ".$onclick.">";
                    foreach($this->values AS $value => $name_item)
                    {
                        $selected = in_array($value, (array) $this->default_value) ? "selected='selected'" : "";
                        $this->code .= "<option value='".$value."' ".$selected.">".$name_item."</option>";
                        
                        
                        $i++;

                    }
                    $this->code .= "</select>";
                    break;
                }
            case "submit": {
                $title = $disabled != "" ? "title='".ERROR_NEDOSTATECNA_PRAVA."'" : "";
                
                if($title != "")
                    $this->class .= " disabled";
                
                if($this->jquery_ui)
                    $this->class .= " ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only";
                
                $this->code .= "<input type='submit' value='".$this->default_value."' name='".$this->name."' class='tool_button ".$this->class."' ".$id." ".$disabled." ".$onclick." ".$title."/>";
                break;
                } 
            case "button": {
                if($this->jquery_ui)
                    $this->class .= " ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only";
                
                $this->code .= "<input type='button' value='".$this->default_value."' name='".$this->name."' class='tool_button ".$this->class."' ".$id." ".$disabled." ".$onclick."  />";
                break;
                } 
            case "section": {
                if($this->jquery_ui)
                    //$this->code .= "<div class='ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix'>".$this->item_name."</div>";
                    $this->code .= '<li><a href="#ine_tabs_'.$this->fid.'">'.$this->item_name."</a></li>";
                    else
                    $this->code .= ShowH3($this->item_name, $this->name, $this->note, $this->tr_class);
                break;
                }   
            case "plain_text": {
                $this->code .= $this->default_value;
                break;
                }
            case "hidden": {
                $this->code .= "<input type='hidden' value='".$this->default_value."' name='".$this->name."' ".$id." />";
                break;
                } 
            case "code": {
                $this->code .= $this->default_value;
                break;
                } 
            case "note": {
                $this->code .= "<tr ".$tr_class."><td colspan='2'>".$this->default_value."</td></tr>";
                break;
                }
            default: {break;}

        }
        
        $this->code .= $this->text_after;
    } 
    
    private function add_wysiwyg_editor_code($name)
    {
        global $domain;
        
        $jazyk = CMS_LANG == 'cz' ? 'cs' : CMS_LANG;      

        if($this->editor_for_mailing)
        {
        $_SESSION['useruploads'] = ABSOLUTE_URL_USER_DOMAIN.USER_DIRNAME_USERUPLOADS;
        $this->wysiwyg_js .= "
        CKFinder.setupCKEditor( null, '".RELATIVE_PATH."ckfinder/' );
        CKEDITOR.replace( '".$name."' ,{
            language: '".$jazyk."',
            //toolbar: 'Basic',
            entities: false,
            htmlEncodeOutput: false,
            entities_greek: false,
            entities_latin: false,
            allowedContent: true,
            extraPlugins: 'magicline,docprops,colordialog',
            toolbarGroups: [
            { name: 'document',    groups: [ 'print','preview','doctools','templates' ] },
            { name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
            { name: 'editing',     groups: [ 'find', 'div' ] },
            { name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align' ] },
            { name: 'tools', groups: ['mode','maximize'] },
            '/',
            { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
            { name: 'links' },
            { name: 'insert' },
            { name: 'styles' },
            { name: 'showblocks'}
           
            ],
            removePlugins: 'autosave,save,font,newpage,placeholder,bidi,symbol,youtube',
            baseUrl: '".$domain->getUrl().$domain->getDir()."userfiles/',
            templates_files: ['".AJAX_GATEWAY."templates.list'],
            fullPage: true,
            readOnly: ".($this->disabled ? 'true' : 'false')."
        });
                
        ";
        }
        else
        {
            $_SESSION['useruploads'] = RELATIVE_URL_USER_DOMAIN.USER_DIRNAME_USERUPLOADS;
            $this->wysiwyg_js .= "
        CKFinder.setupCKEditor( null, '".RELATIVE_PATH."ckfinder/' );
        CKEDITOR.replace( '".$name."' ,{
            language: '".$jazyk."',
            //toolbar: 'Basic',
            entities: false,
            htmlEncodeOutput: false,
            entities_greek: false,
            entities_latin: false,
            allowedContent: true,
            extraPlugins: 'magicline',
            toolbarGroups: [
            { name: 'document',    groups: [ 'print','preview','doctools','templates' ] },
            { name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
            { name: 'editing',     groups: [ 'find', 'div' ] },
            { name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align' ] },
            { name: 'tools', groups: ['mode','maximize'] },
            '/',
            { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
            { name: 'links' },
            { name: 'insert' },
            { name: 'styles' },
            { name: 'showblocks'}
           
            ],
            removePlugins: 'autosave,save,font,newpage,placeholder,bidi,youtube',
            baseUrl: '".RELATIVE_PATH.$domain->getDir()."userfiles/',
            templates_files: ['".AJAX_GATEWAY."templates.list'],
            readOnly: ".($this->disabled ? 'true' : 'false')."
        });
                
        ";
        }
       
    }
    
    //vraci bool hodnotu jestli se maji checkboxy radit pod sebou nebo vedle sebe
    public function get_list_below()
    {
        return $this->list_below;
    }
    
    //vraci text napovedy
    public function get_help()
    {
        return $this->help_id == 0 ? "" : help($this->help_id);
    }
    
    //vraci nazev formularove polozky
    public function get_name()
    {
        return $this->item_name;
    }
    
    //vraci typ formularove polozky
    public function get_type()
    {
        return $this->type;
    }
    
    
    //vraci html kod prvku formulare
    public function get_html_code()
    {
        $this->set_html_code();
        return $this->code;
    }
    
    
    //vraci true pokud je polozka povinna ci nikoliv
    public function is_req()
    {
        return $this->req;
    }
        
}



class Form
{

    //action parametr
    private $action = "";
    
    //method parametr
    private $method = "post";
    
    //enctype parametr
    private $enctype = "";
    
    //polozky formulare
    private $items = array();
    
    //citac polozek formulare
    private $items_count = 0;
    
    //name formulare
    private $name="";
    
    //id formulare
    private $id="";
    
    //class tabulky formulare
    private $table_class="";
    
    //automaticky detekuje zda je nastavena post hodnota a pokud ano, tak ji vlozi misto defaultni hodnoty do inputu ci dalsiho prvku formulare
    private $autodetection_action = true;
    
    //pridava prvku tridy jquery ui pro snadne stylovani
    private $jquery_ui_css = false;
    
    private $next_actions = false;
    
    
    //konstruktor
    public function __construct()
    {
        
    }
    
    public function add_next_actions()
    {
        
        $next_actions = array(
            0 => TULOZIT_A_VRATIT_NA_VYPIS, 
            1 => TULOZIT_A_VRATIT_K_EDITACI, 
            2 => TULOZIT_A_ZALOZIT_NOVOU
        );
        $this->next_actions = new FormItem("", "selectbox", "dalsi_akce", get_next_action_code(), $next_actions);

        
        return $this;
    }
    
    
    //nastavi znackovani prvku tridami pro css jquery UI
    public function set_jquery_ui_css($val = true)
    {
        $this->jquery_ui_css = $val;
    }
    
    //nastavi autodetekci rozpoznavani hodnot v postu
    public function set_autodetection_action($val = true)
    {
        $this->autodetection_action = $val;
    }
    
    //nastavi id formulare
    public function set_id($id)
    {
        $this->id = $id;
    }
    
    //nastavi class table formulare
    public function set_class_table($table_class)
    {
        $this->table_class = $table_class;
    }
    
    //vraci polozky formulare v poli
    public function get_items()
    {
        return $this->items;
    }
    
    //vraci enctype formulare
    public function get_enctype()
    {
        return $this->enctype;
    }
    
    //vraci method formulare
    public function get_method()
    {
        return $this->method;
    }
    
    
    //vraci action formulare
    public function get_action()
    {
        return $this->action;
    }
    
    //nastavi parametr action formulare
    public function set_action($action)
    {
        $this->action = $action;
    }
    
    
    //nastavi parametr enctype formulare
    public function set_enctype($enctype)
    {
        $this->enctype = $enctype;
    }
    
    
    //nastavi parametr method formulare
    public function set_method($method = "post")
    {
        $this->method = $method;
    }
    
    //nastavi parametr name formulare
    public function set_name($name = "post")
    {
        $this->method = $name;
    }
    
    
    //povoli ve formulari upload souboru nebo zakaze
    public function allow_upload($val = true)
    {       
        $this->enctype = $val ? "multipart/form-data" : ""; 
    }
    
    
    //prida polozku do formulare
    //type: text, radio, textarea, editor, select, checkbox
    //name atribut
    //values
    public function add_item($item_name, $type, $name, $default_value="", $values = null, $class = "", $id = "", $req=false, $note="", $help_id=0)
    {
        if($this->autodetection_action)
            $default_value = isset($_POST[$name]) ? $_POST[$name] : "";
        
        $idi = $this->items_count;
        $this->items[$idi] = new FormItem($item_name, $type, $name, $default_value, $values, $id, $class, $note, $req, $help_id);
        $this->items[$idi]->allow_jquery_ui( $this->jquery_ui_css );
        $this->items_count++;
        
        return $this->items[$idi];
        
    }

    //prida textovy input
    public function add_text($item_name, $name, $default_value = "", $help_id=0, $req=false, $class = "", $id = "")
    {
        if($this->autodetection_action)
            $default_value = get_secure_post($name,true,$default_value);
        
        $idi = $this->items_count;
        $this->items[$idi] = new FormItem($item_name, "text", $name, $default_value, null,$id, $class,"",$req, $help_id);
        $this->items[$idi]->allow_jquery_ui( $this->jquery_ui_css );       
        $this->items_count++;
        return $this->items[$idi]; 
    }
    
    //prida textovy input pro heslo
    public function add_password($item_name, $name, $default_value = "", $help_id=0, $req=false, $class = "", $id = "")
    {
        $idi = $this->items_count;
        $this->items[$idi] = new FormItem($item_name, "password", $name, $default_value, null,$id, $class,"",$req,$help_id);  
        $this->items[$idi]->allow_jquery_ui( $this->jquery_ui_css );
        $this->items_count++;
        return $this->items[$idi];
    }

    
    //prida textarea
    public function add_textarea($item_name, $name, $default_value = "", $help_id=0, $req=false, $class = "", $id = "")
    {
        if($this->autodetection_action)
            $default_value = get_secure_post($name,true,$default_value);
            
        $idi = $this->items_count;
        $this->items[$idi] = new FormItem($item_name, "textarea", $name, $default_value, null, $id, $class,"",$req, $help_id); 
        $this->items[$idi]->allow_jquery_ui( $this->jquery_ui_css );
        $this->items_count++;
        return $this->items[$idi]; 
    }
    
    
    //prida radiobuttony
    public function add_radiobuttons($item_name, $name, $default_value = "", $values=array(),$help_id=0,$class = "", $id = "")
    {
        if($this->autodetection_action)
            $default_value = get_secure_post($name,true,$default_value);
            
        if($values == null || count($values) == 0)
            $values = array(1=>TANO,0=>TNE);
            
        $idi = $this->items_count;
        $this->items[$idi] = new FormItem($item_name, "radio", $name, $default_value, $values, $id, $class,"",false, $help_id);
        $this->items[$idi]->allow_jquery_ui( $this->jquery_ui_css );
        $this->items_count++;
        return $this->items[$idi];
    }
    
    
    //prida checkboxy
    public function add_checkboxes($item_name, $name, $default_value = array(), $values=array(),$help_id=0,$req=false,$class = "", $id = "", $list_below = false)
    {
        if($this->autodetection_action)
            $default_value = get_array_post($name,$default_value);
            
        $idi = $this->items_count;
        $this->items[$idi] = new FormItem($item_name, "checkbox", $name, $default_value, $values, $id, $class,"",$req, $help_id,false,$list_below);
        $this->items_count++;
        $this->items[$idi]->allow_jquery_ui( $this->jquery_ui_css );
        return $this->items[$idi];
    }
    
    
    //prida selectbox
    public function add_selectbox($item_name, $name, $default_value = "", $values=array(),$help_id=0,$req=false,$class = "", $id = "",$show_optgroups=false)
    {
        if($this->autodetection_action)
            $default_value = get_secure_post($name,true,$default_value);          
            
        $idi = $this->items_count;
        $this->items[$idi] = new FormItem($item_name, "selectbox", $name, $default_value, $values, $id, $class,"",$req, $help_id, $show_optgroups);
        $this->items[$idi]->allow_jquery_ui( $this->jquery_ui_css );
        $this->items_count++;
        return $this->items[$idi];
    }
    
    //prida vicevyberovy selectbox
    public function add_multiple_selectbox($item_name, $name, $default_value = array(), $values=array(),$help_id=0,$req=false,$class = "", $id = "", $list_below = false)
    {
        if($this->autodetection_action)
            $default_value = get_array_post($name,$default_value);

        $idi = $this->items_count;
        $this->items[$idi] = new FormItem($item_name, "multiple_selectbox", $name, $default_value, $values, $id, $class,"",$req, $help_id,false,$list_below);
        $this->items[$idi]->allow_jquery_ui( $this->jquery_ui_css );
        $this->items_count++;
        return $this->items[$idi];
    }
    
    
    //prida wysiwyg editor
    public function add_editor($item_name, $name, $default_value = "", $help_id=0, $req=false,$class = "wysiwygEditor h500 w100", $id = "wysiwygEditor")
    {
        if($this->autodetection_action)
            $default_value = get_post($name,$default_value);
            
        $idi = $this->items_count;
        $this->items[$idi] = new FormItem($item_name, "editor", $name, $default_value, null,$id, $class,"",$req,$help_id);
        $this->items[$idi]->allow_jquery_ui( $this->jquery_ui_css );
        $this->items_count++;
        return $this->items[$idi];
    }
    
    
    //prida sekci
    public function add_section($item_name, $name, $note="", $help_id=0,$class = "", $id = "")
    {
        $idi = $this->items_count;
        $this->items[$idi] = new FormItem($item_name, "section", $name, "", "", $id, $class, $note, false, $help_id);
        $this->items[$idi]->allow_jquery_ui( $this->jquery_ui_css );
        $this->items_count++;
        return $this->items[$idi];
    }
    
    
    //prida tlacitko pro odeslani
    public function add_submit($default_value = TULOZIT, $name="btnAction", $class = "save_button", $id = "")
    {
        $idi = $this->items_count;
        $this->items[$idi] = new FormItem("","submit", $name, $default_value, null,$id, $class);
        $this->items[$idi]->allow_jquery_ui( $this->jquery_ui_css );
        $this->items_count++;
        return $this->items[$idi];
    }
    
    //prida tlacitko
    public function add_button($default_value = "", $name = "", $class = "save_button", $id = "")
    {
        $idi = $this->items_count;
        $this->items[$idi] = new FormItem("","button", $name, $default_value, null,$id, $class);
        $this->items[$idi]->allow_jquery_ui( $this->jquery_ui_css );
        $this->items_count++;
        return $this->items[$idi];
    }
    
    
    //prida plain text
    public function add_plain_text($item_name="",$default_value="", $help_id=0, $class = "", $id = "")
    {
        $idi = $this->items_count;
        $this->items[$idi] = new FormItem($item_name,"plain_text", "", $default_value,"",$id, $class,"",false,$help_id);
        $this->items[$idi]->allow_jquery_ui( $this->jquery_ui_css );
        $this->items_count++;
        
        return $this->items[$idi];
    }
    
    //prida skryty input
    public function add_hidden($name,$default_value="",$id = "")
    {
        $idi = $this->items_count;
        $this->items[$idi] = new FormItem("","hidden", $name, $default_value, "", $id);
        $this->items_count++;
        return $this->items[$idi];
    }
    
    //prida skryty input
    public function add_file($item_name, $name,$req=false,$help_id=0,$class="", $id="")
    {
        $idi = $this->items_count;
        $this->items[$idi] = new FormItem($item_name,"file", $name, "","",$id, $class,"",$req,$help_id);
        $this->items[$idi]->allow_jquery_ui( $this->jquery_ui_css );
        $this->items_count++;
        return $this->items[$idi];
    }
    
    
    //prida html kod do formulare
    public function add_code($code)
    {
        $idi = $this->items_count;
        $this->items[$idi] = new FormItem("","code", "", $code);
        
        $this->items_count++;
        return $this;// $this->items[$idi];
    }
    
    //prida text do formulare na radek s colspan=2
    public function add_note($text='')
    {
        $idi = $this->items_count;
        $this->items[$idi] = new FormItem("","note", "", $text);
        $this->items[$idi]->allow_jquery_ui( $this->jquery_ui_css );
        $this->items_count++;
        return $this->items[$idi];
    }
    
    
    //vraci kod formulare
    public function get_html_code()
    {
        $enctype = $this->enctype != "" ?  "enctype='".$this->enctype."'" : "";
        $id = $this->id != "" ?  "id='".$this->id."'" : "";
        $table_class = "table2 ";
        $table_class .= $this->table_class != "" ?  $this->table_class : "";
        
        $form = "\n\n";
        $form .= "<form method='".$this->method."' action='".$this->action."' ".$enctype." ".$id.">\n";
        $i = 1;
        $count = count($this->items);
        
        $je_nejaka_sekce = false;
        $zalozky = "";
        if($this->jquery_ui_css)
            foreach($this->items AS $item)
                if($item->get_type() == 'section')
                {
                    $zalozky .= $item->get_html_code();
                    $je_nejaka_sekce = true;
                }
                
        if($zalozky != "")
            $form .= "<ul>".$zalozky."</ul>";
                
        foreach($this->items AS $item)
        {
            
            if($item->get_type() == 'submit' || $item->get_type() == 'hidden')
            {
                $i++; 
                continue;   
            }
            
            if($item->get_type() == 'section')
            {
                if($i > 1)
                {
                                        
                    $form .= "</table>";
                    
                    if($this->jquery_ui_css)
                        $form .= "</div>";
                        
                }
                
                if(!$this->jquery_ui_css)
                    $form .= $item->get_html_code();
                
                
                if($count != $i)
                {
                    /*
                    if($this->jquery_ui_css)
                        $form .= "<div class='ui-dialog-content ui-widget-content'>";
                      */ 
                      
                    if($this->jquery_ui_css)
                        $form .= "<div id='ine_tabs_".$item->get_fid()."'>";
                         
                    $form .= "<table class='".$table_class."'>";
                }
            }
            elseif($item->get_type() == 'code')
            {
                $form .= $item->get_html_code();
            }
            else
            {
                $help = $item->get_help();
                $help = $help == "" ? "" : " ".$help;
                $tr_class = $item->tr_class != "" ? "class='".$item->tr_class."'" : "";
                
                if($this->jquery_ui_css)
                {
                    //$form .= "<div class='input_box'>".$item->get_name().$help.$item->get_html_code()."</div>";
                    $form .= "<tr ".$tr_class."><th class='".$item->th_class."'>".$item->get_name().$help."</th>";
                    $form .= "<td class='".$item->td_class."'>".$item->get_before().$item->get_html_code().$item->get_after()." ".($item->is_req() && !$this->jquery_ui_css ? req() : "")."</td>";
                    $form .= "</tr>";
                }
                else
                {
                    $form .= "<tr ".$tr_class."><th class='".$item->th_class."'>".$item->get_name().$help."</th>";
                    $form .= "<td class='".$item->td_class."'>".$item->get_before().$item->get_html_code().$item->get_after()." ".($item->is_req() && !$this->jquery_ui_css ? req() : "")."</td>";
                    $form .= "</tr>";
                }
            }
            
            $i++;
        }
            
          /*  
        if($this->jquery_ui_css)
            $form .= "</div>";
            else
            */
            $form .= "</table>";
        
        $hidden = "";
        foreach($this->items AS $item)
        {
            
            if($item->get_type() == 'hidden')
                $hidden .= $item->get_html_code();
            
        }
        
        $form .= "<div class='cleaner'>".$hidden."</div>";
        
        
        //vygenerovani tlacitek
        $form .= "<div class='dolni-tlacitka'>";
        foreach($this->items AS $item)
        {
            if($item->get_type() != 'submit') continue;               
                    
            
            if($this->jquery_ui_css)
                $form .= "<div class='ui-dialog-buttonpane ui-widget-content ui-helper-clearfix'>
                            <div class='ui-dialog-buttonset'>
                                ".$item->get_html_code()."
                            </div>
                        </div>";
                else       
                { 
                //$form .= "<div class='cleaner'></div>";
                $form .= $item->get_before().$item->get_html_code().$item->get_after();
                
                if($this->next_actions !== false)
                    $form .= $this->next_actions->get_html_code();
                
                }
        }
        $form .= "</div>";
        
        
        $form .= "</form>\n\n";
        
        
        
        $form .= $this->wysiwyg_js_code();
        $form .= "<div class='cleaner'></div>";
        return $form;
    }
    
    //vraci js kod pro wysiwyg editor
    private function wysiwyg_js_code()
    {
        
        $code = "";
        foreach($this->items AS $item)
        {
            $code .= $item->wysiwyg_js;
        }    
        
        
        //nacteni polozek pro selectbox stranek v ckeditoru
/*
        $links = array( array('Text of Link to Google','http://google.com') , array('Test of link to bing','http://www.bing.com') );
        $json_links = json_encode($links);
        //$json_links = "null";
  */     
        
        if($code != '')
        {
           
            $code = '<script type="text/javascript">
                var pages_items = '.get_pages_for_editor().'; 
                
                '.$code.'    
   
                </script>';
            
            
        }
        
        
        return $code;
        
        
        
    }
    
    
    


    
}

//trida pro vykresleni formulare filtru
class FormFilter extends Form
{
    
    //trida obalu formulare
    private $class = "filter";
    
    //nastavi class obalu formulare filtru
    public function set_class($class)
    {
        $this->class = $class;
    }
    
    //funkce pro vykresleni formulare
    public function get_html_code( $main_tools = "" )
    {
        $enctype = $this->get_enctype() != "" ?  "enctype='".$this->get_enctype()."'" : "";
        $form = "\n\n";
        $form .= "<div class='tools-panel'>";
        $form .= "<div class='".$this->class."'>";
        
        $form .= "<form method='".$this->get_method()."' action='".$this->get_action()."' ".$enctype.">\n";
        $form .= "<table class='tFilter'>";
        $i = 1;
        
        $items = $this->get_items();
        $count = count($items);
        foreach($items AS $item)
        {
            if($item->get_type() == 'submit')
            {
                continue;   
            }
            
            if($item->get_type() == 'section')
            {
                continue;
            }
            elseif($item->get_type() == 'code')
            {
                $form .= $item->get_html_code();
            }
            else
            {
                $help = $item->get_help();
                $help = $help == "" ? "" : " ".$help;
                $form .= "<tr><th>".$item->get_name().$help."</th>";
                $form .= "<td>".$item->get_html_code()." ".($item->is_req() && !$this->jquery_ui_css ? req() : "")."</td>";
                $form .= "</tr>";
            }
            
            $i++;
        }
            
        $form .= "</table>";     
        $form .= "</form>\n\n";
        $form .= "</div>";
        $form .=  $main_tools;
        $form .=  "</div>";
        $form .= "<div class='cleaner'></div>";
        
        return $form;
    }
    
    
    
}


?>