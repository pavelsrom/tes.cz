<?php

/**
 * @author Pavel Srom
 * @copyright 2013
 */


class C_Page{
    
    //upravena request uri, tedy adresa stranky, ktera je obsazena v GET parametru "html"
    private $uri = "";
    
    //id stranky
    private $id = 0;
        
    //typ stranky
    private $type = "404";
    
    //pole s informacemi o strance
    private $data = array();
    
    private $find_word = false;
    
    private $h1_tag = "h1"; 
    
    //id rodice
    private $parent_id = 0;
    
    
    //konstruktor
    public function __construct()
    {
        global $domain;
        
        $this->uri = isset($_GET['html']) ? $_GET['html'] : "";
        
        $adresar_dom = $domain->data['adresar']=='' ? "" : $domain->data['adresar'];
        
        $this->find_word = isset($_GET['hledat']);
        
        //pokud je nastaven get parametr 404, zobrazi se chybova stranka
        if(isset($_GET['404']))
        {
            $this->id = 0;
        }
        else
        {
            $data_url = $this->check_url();  
            
            $idPage = $data_url['id'];
            $urlNamePage = $data_url['nazev'];
            $koncovka = $data_url['koncovka'];
            $presmerovat = $data_url['presmerovat'];

            //pokud je zadana url adresa index.php, pak dojde k presmerovani na korenovou url

            if($urlNamePage=='index' && $koncovka=='php' && $_SERVER['REQUEST_URI']=='/index.php')
                Redirect($domain->getUrl());
                            
            //pokud neni zadana url adresa index.php a je nastaveno presmerovani na jinou url, pak se presmeruje
            if($urlNamePage!='index' && $koncovka!='php' && $presmerovat!='')
                Redirect($presmerovat);      
            
            $this->id = $idPage;
            
        }
        
        		
        
        //pokud je uzivatel prihlasen v administraci, pak uvidi na webu stranky, ktere maji zakazano zobrazovani
		if(!isset($_SESSION['idUzivatele']))
            $cond = "AND zobrazit=1
                AND (((od <= CURDATE() OR od IS NULL) AND (do >= CURDATE() OR do IS NULL)) OR (archivovat=1)) 
				";
            else
            $cond = "";
        
        $types = array();
        $request_uri = explode('?',$_SERVER['REQUEST_URI']);

        
        if($_SERVER['REQUEST_URI'] == $domain->getRelativeUrl() ||  
            $request_uri[0] == $domain->getRelativeUrl() ||
            $_SERVER['REQUEST_URI'] == $domain->getRelativeUrl().LANG."/" || 
            ($_SERVER['REQUEST_URI'] == $domain->getRelativeUrl() && LANG == DEFAULT_LANG)
            )
        {
            $types = array("'uvodni'");
        }
        
        
        //nacte data stranky
        $this->set_data($cond, $types);
                        
        //presmeruje prazdnou stranku na prvniho potomka
        $this->empty_page_process();
        
        //v pripade vyhledavani oznaci vyhledavana slova v obsahu   
        $this->content_prepare();     
        
        
        
        
        
        //presmerovani uvodni stranky na zakladni url, rovnez presmerovani index.php na zakladni url
		if($this->type == 'uvodni' && 
            $_SERVER['REQUEST_URI'] != $domain->getRelativeUrl() && 
            $_SERVER['REQUEST_URI'] != $domain->getRelativeUrl().LANG."/" && 
            !is_get('hledat') && !is_get('login') && !is_get('user') && !is_get('stranka') && !is_get('click') && !is_get('no_access') && !is_get('vote') && !is_get("mesic") && !is_get("rok") && !is_get("ref") && !is_get("confirm_registrace"))
            {        
    			$url = $domain->getUrl();
    			Redirect($url, 301);
			}

        //pokud nebyla stranka nalezena vraci kod 404
        if($this->type == "404")
        {
            redirect('',404);
        }
        
    }
    
    //presmeruje prazdnou stranku na prvni stranku potomka
    private function empty_page_process()
    {
        global $db;
        global $domain;
        
        if($this->type != 'prazdna-stranka') 
            return;
        
        $data = $db->Query("SELECT idStranky AS id, url AS url, j.jazyk
    			FROM ".TABLE_STRANKY." AS s
                LEFT JOIN ".TABLE_JAZYKY." AS j ON s.idJazyka = j.idJazyka 
    			WHERE idDomeny=".$domain->getId()." 
                    AND s.idJazyka=".WEB_LANG_ID."
                    AND (typ='404' OR idRodice=".$this->id.")
                ORDER BY priorita, typ='404'
                LIMIT 1           
                ");
                
       $ps = $db->getAssoc($data);
       $presmerovaniPrazdneStranky = UrlPage($ps['url'],$ps['id'],$ps['jazyk']);
       Redirect($presmerovaniPrazdneStranky);
    }
    
    
    //nacte informace o aktualni strance
    //vstupni parametr urcuje podminku nacteni dat a popr typy stranky
    private function set_data($cond, $types)
    {
        global $db;
        global $domain;
        
        $typy = array("'404'");
        if($types && count($types) > 0)
            $t = array_merge($typy, $types);
            else
            $t = $typy;
                
        $order = count($types) == 0 ? "ORDER BY typ='404'" : "ORDER BY typ='uvodni' DESC";
        
        $where = "";
        if(!POVOLIT_DETAIL_FOTKY)
        {
            $where = " AND typ != 'galerie-fotka'"; 
        }
        
        
        //nacte data stranky
		$data = $db->Query("SELECT *, idStranky AS id, typ, obsah, perex, obrazek,
            IF(nadpis='', nazev, nadpis) AS nadpis,
            IF(title='', IF(nadpis='', nazev, nadpis), title) AS title,
            IF(description='', IF(title='', IF(nadpis='', nazev, nadpis), title), description) AS description,
            DATE_FORMAT(datum,'%e. %c. %Y') AS datum
			FROM ".TABLE_STRANKY." 
			WHERE idDomeny=".$domain->getId()."
                AND idJazyka=".WEB_LANG_ID." 
                ".$cond."
                ".$where."
                AND (typ IN (".implode(",",$t).") OR idStranky=".$this->id.")
            ".$order."
            LIMIT 1
            ");

        if($db->numRows($data) == 0) return false;
        
        $this->data = $db->getAssoc($data);
        $this->type = $this->data['typ'];  
        $this->id = $this->data['id']; 
        $this->parent_id = $this->data['idRodice'];
                             
    }

    public function get_parent_id()
    {
        return $this->parent_id;
    }

    public function is_home()
    {
        return $this->type == 'uvodni';
    }
    
    
    //funkce overi url adresu a vraci pole s informacemi o id, nazvu, koncovce a url pro presmerovani
    private function check_url()
    {
        global $domain;
        global $db;
        
        $koncovka_data = explode('.',$this->uri);
        $koncovka = "";
        if(count($koncovka_data)>1){
            $koncovka = end($koncovka_data);
            
            array_pop($koncovka_data);
            }
        
        $url_struktura_data = implode('',$koncovka_data);
        $url_struktura_data1 = explode('_',$url_struktura_data);
        
        $nazev = "";
        $id = 0;
        
        $result = array();
        
        $pozadovana_struktura = $domain->data['urlStruktura'];
        $pozadovana_koncovka = $domain->data['urlKoncovka'];
        $pozadovana_url = "";
        
        if(count($url_struktura_data1)==1) {
            
            $nazev = $url_struktura_data1[0];
            
            $cond = "";
            if($nazev != "")
                $cond .= " AND url='".$db->secureString($nazev)."'";
            else
                $cond .= " AND typ='uvodni'";
            
            if(!isset($_SESSION['idUzivatele']))
                $cond .= "AND zobrazit=1
                    AND (((od <= CURDATE() OR od IS NULL) AND (do >= CURDATE() OR do IS NULL)) OR (archivovat=1)) 
    				";
                else
                $cond .= "";
            
            $data = $db->Query("SELECT idStranky FROM ".TABLE_STRANKY." 
                WHERE idDomeny=".$domain->getId()." 
                    AND idJazyka='".WEB_LANG_ID."'
                    ".$cond."
                    LIMIT 1");
            if($db->numRows($data)>0){
                $ids = $db->getAssoc($data);
                $id = intval($ids['idStranky']);
                }
            }
        elseif(count($url_struktura_data1)>1){
                 
                if(ctype_digit($url_struktura_data1[0])){
                    $id = intval($url_struktura_data1[0]);
                    $nazev = end($url_struktura_data1);
                    }
                elseif(ctype_digit(end($url_struktura_data1))){
                    $id = intval(end($url_struktura_data1));    
                    $nazev = $url_struktura_data1[0];
                    }
            }
        
        $presmerovat = "";

        if($pozadovana_koncovka!='') $pozadovana_koncovka = ".".$pozadovana_koncovka;
        
        if($pozadovana_struktura=='id-nazev' && $id>0 && $nazev!='')
            $pozadovana_url = $id."_".$nazev.$pozadovana_koncovka;
        elseif($pozadovana_struktura=='nazev-id' && $id>0 && $nazev!='')
            $pozadovana_url = $nazev."_".$id.$pozadovana_koncovka;
        elseif($pozadovana_struktura='nazev' && $nazev!='')
            $pozadovana_url = $nazev.$pozadovana_koncovka;
        
        
        
        
        if($this->uri!=$pozadovana_url)
            $presmerovat = $pozadovana_url;
        
        $result = array(
            'id'=>$id,
            'nazev'=>$nazev,
            'koncovka'=>$koncovka,
            'presmerovat'=>$presmerovat
            ); 

        return $result;    
        
    }
   
    
    //vraci id stranky
    public function get_id()
    {
        return $this->id;
    }
    
    
    //vraci typ stranky
    public function get_type()
    {
        return $this->type;
    }
    
    
    //vraci udaj z pole dat stranky
    public function get($cols_name)
    {
        return isset($this->data[$cols_name]) ? $this->data[$cols_name] : "";
    }
    
    
    //vraci nadpis
    public function get_h1()
    {
        return editable($this->id, $this->get("nadpis"), "h1",true, $this->h1_tag);
    }
    
    public function set_h1_tag($tag)
    {
        $this->h1_tag = $tag;
    }
    
    
    //vraci hlavni obsahovou cast stranky
    public function get_content($class = "")
    {
        global $links;
        
        $obsah = $this->get('obsah');
        $obsah = trim($obsah) == "" ? "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" : $obsah;    
          
        $obsah = editable($this->id, $obsah, "content",!$this->find_word)."<div class='cleaner'></div>";     
        
        
        if(($this->type == 'novinka' && MODULE_NEWS) || ($this->type == 'clanek' && MODULE_ARTICLES) || ($this->type == 'produkt' && MODULE_ESHOP))
        {
            $obsah.= '<div class="text-center"><a href="'.$links->get_url($this->parent_id).'" class="btn">'.TZPET_NA_VYPIS.'</a></div>';  
        }
        
        
                
        return $obsah;
        
    }
    
    //vypise stitky pod nadpis stranky/novinky/clanku
    public function get_tags()
    {
        
        if(!MODULE_RELATED_ARTICLES)
            return false;
    
        /* Tagy - stitky */
        $tagy = new C_Tags();
        $tagy->set_lang(WEB_LANG_ID);
        $tagy->set_tags(true);
        $tagy->set_data();
        $stitky = $tagy->get_tags_by_object($this->id);
           
        
        if(count($stitky))
        {
            $pole_stitku = array();
            foreach($stitky AS $s)
            {
                if($s['count'] == 0)
                    continue;
                    
                $pole_stitku[] = "<a href='".$s['url']."' class='tag'>".$s['name']."</a>";
            }
            
            return "<h3>".TTAGY."</h3>".implode(" ",$pole_stitku);
        }
        
        return "";
        
        
    }
    
    
    //v pripade vyhledavani oznaci vyhledavana slova v obsahu a v perexu
    private function content_prepare()
    {
        if($this->find_word && $this->type != 'vyhledavani')
        {
            if(!class_exists('C_Finder'))
			    include_once(PRE_PATH.'php/classes/class.finder.php');
                
            $vyraz = isset($_GET['hledat']) ? $_GET['hledat'] : '';
            $finder = new C_Finder($vyraz);
            $this->data['obsah'] = $finder->MarkWord($this->get('obsah'), $finder->term);
            $this->data['perex'] = $finder->MarkWord($this->get('perex'), $finder->term);  
        }
                
    }
    
    
    //vraci perex clanku, novinky ...
    public function get_perex()
    {
        $perex = $this->get('perex');       
                 
        $perex = editable($this->get_id(),$perex,"perex",!$this->find_word); 
        return $perex;
    }
    
    
    //vraci kompletni obsah stranky vcetne vsech pridruzenych modulu
    public function get_complete_contents($showH1 = true)
    {
        global $links;
        
        $obsah = "";
        
        if($showH1)
            $obsah .= $this->get_h1();
        
        
        $obsah .= $this->get_tags();
        
        
        if(($this->type == 'novinka' && MODULE_NEWS) || ($this->type == 'clanek' && MODULE_ARTICLES))
        {
            
            $obsah.= "<div class='datum'>
                    <em>".$this->get("datum")."</em>
                </div>";
            $obsah.= $this->get_perex();
            /*
            if($this->type == 'clanek' && $this->get("obrazek") != '')
            {
                $obsah .= '<div class="hlavni_obrazek"><img src="'.RELATIVE_URL_USER_DOMAIN.USER_DIRNAME_ARTICLES_MAXI.$this->get("obrazek").'" alt="'.$this->get("nadpis").'"></div>';
            }
            */
            
        }    
        
        
        
        if($this->type == 'produkt' && MODULE_ESHOP)
        {
            
        }
        else
        {
            $obsah.= $this->get_content(); 
        }
        
        
        
        //v detailu produktu se vypisuje specialni obsah
        /*
        if($this->type == 'produkt' && MODULE_ESHOP)
        {
            if(!class_exists('C_Products'))
                include_once(PRE_PATH.'php/classes/class.products.php');
			$products = new C_Products(0,'detail',$this->get_id());
			$obsah .= $products->get_html();    
        }
        */
        
        /*
        if(($this->type == 'novinka' && MODULE_NEWS) || ($this->type == 'clanek' && MODULE_ARTICLES) || ($this->type == 'produkt' && MODULE_ESHOP))
        {
            $obsah.= '<div class="cist-vice"><a href="'.$links->get_url($this->parent_id).'" class="button fr">'.TZPET_NA_VYPIS.'</a></div>';  
            $obsah .= '<div class="cleaner"></div>';
        }
        */
        
        /*
        ob_start();
        include("php/includes/social.php");
        $obsah .= ob_get_contents();
        ob_clean();
        */
        $obsah.= $this->get_special_content();
        
        return $obsah;
    }
    
    
    //vypise zbytek obsahu
    public function get_special_content()
    {
        $html = "";
        $typ = $this->type;
        
       
        //vysledky vyhledavanni
        if(MODULE_FINDER && $typ == 'vyhledavani')
        {
            if(!class_exists('C_Finder'))
                include_once(PRE_PATH.'php/classes/class.finder.php');
                
			$vyraz = isset($_GET['vyraz'])?$_GET['vyraz']:'';
			$finder = new C_Finder($vyraz);
			$finder->Find();
			$html .= $finder->GetResult();
			
        }
        //mapa webu
        if($typ == 'sitemap')
        {
            if(!class_exists('C_WebTree'))
			    include_once(PRE_PATH.'php/classes/class.web.tree.php');
			$sitemap = new C_WebTree();
            
			$html .= $sitemap->BlockSitemap();
				
		}
        //vypis akci kalendare akci
        if(MODULE_EVENT_CALENDAR && $typ == 'kalendar-akci')
        {
            if(!class_exists('C_EventCalendar'))
			    include_once(PRE_PATH.'php/classes/class.event.calendar.php');
			$eventCalendar = new C_EventCalendar();
			$html .= $eventCalendar->get_html();
        }
        
        //vypis akci daneho typu kalendare akci
        if(MODULE_EVENT_CALENDAR && $typ == 'kalendar-typ')
        {
            if(!class_exists('C_EventCalendar'))
			    include_once(PRE_PATH.'php/classes/class.event.calendar.php');
			$eventCalendar = new C_EventCalendar($typ, $this->id);
			$html .= $eventCalendar->get_html();
        }
        
        //vypis akci daneho typu kalendare akci
        if(MODULE_EVENT_CALENDAR && $typ == 'akce')
        {
            if(!class_exists('C_EventCalendar'))
			    include_once(PRE_PATH.'php/classes/class.event.calendar.php');
			$eventCalendar = new C_EventCalendar($typ, $this->id);
			$html .= $eventCalendar->get_html();
        }
        /*
        if(MODULE_ARTICLES && $this->get_id() == ID_STRANKY_RSS)
        {
            if(!class_exists('C_Rss'))
                include_once(PRE_PATH.'php/classes/class.rss.php');
			$rss = new C_Rss("stranka");
			$html .= $rss->GetArticlesOnPage();
		}
        */
        
        //vypis novinek
        
        if(MODULE_NEWS && ($typ == 'novinky' /*|| $this->data['typ']=='uvodni'*/))
        {
            if(!class_exists('C_News'))
                include_once(PRE_PATH.'php/classes/class.news.php');
            $news = new C_News();
			if($news->exists)
                if($this->data['typ'] == 'uvodni')
                    $html .= $news->GetNewsOnStartPage();
                    else
                    $html .= $news->GetNewsOnPage();
                
		}
        
        //vypis clanku
        if(MODULE_ARTICLES && $typ=='clanky')
        {
            if(!class_exists('C_Articles'))
                include_once(PRE_PATH.'php/classes/class.articles.php');
			$articles = new C_Articles($this->get_id());
			$html .= $articles->GetArticlesOnPage();
        }

        //vypis clanku
        if(MODULE_ARTICLES && $typ=='clanek')
        {
            if(!class_exists('C_Articles'))
                include_once(PRE_PATH.'php/classes/class.articles.php');
			$articles = new C_Articles($this->get_id(), 'clanek', $this->get_id());
			$html .= $articles->GetArticle();
        }
        


        //vypis clanku daneho stitku
        if(MODULE_ARTICLES && $typ=='tag')
        {
            if(!class_exists('C_Articles'))
                include_once(PRE_PATH.'php/classes/class.articles.php');
			$articles = new C_Articles($this->get_id(),'tag');
			$html .= $articles->GetArticlesOnPage();
		}
        //vypis stitku
        if(MODULE_ARTICLES && $typ=='tagy')
        {
            if(!class_exists('C_Articles'))
                include_once(PRE_PATH.'php/classes/class.articles.php');
			$articles = new C_Articles($this->get_id(),'tagy');
			$html .= $articles->GetAllTags();
		}
        
        //vypis archivu clanku
        if(MODULE_ARTICLES && $typ=='archiv-clanku')
        {
            if(!class_exists('C_ArticlesArchive'))
                include_once(PRE_PATH.'php/classes/class.articles.archive.php');
		    $articlesArchive = new C_ArticlesArchive();
			$html .= $articlesArchive->getArticlesArchiveOnPage();
		}
        //vypis galerie - vypis slozek, galerii, fotek, detail fotky
        /*
        if(MODULE_GALLERY && in_array($typ, array('galerie','galerie-fotka','galerie-slozka','stranka-galerie')) )
        {
            if(!class_exists('C_Gallery'))
                include_once(PRE_PATH.'php/classes/class.gallery.php');
		  
          $gal = new C_Gallery();
		  if($gal->exists)
			$html .= $gal->GetGalleryPage();
			
        }
        */
        //vypis clanku
        if(MODULE_ESHOP && $typ=='produkty')
        {
            if(!class_exists('C_Products'))
                include_once(PRE_PATH.'php/classes/class.products.php');
			$products = new C_Products($this->get_id());
			$html .= $products->get_html();
		}
        
        /*
        if(MODULE_ESHOP && $typ=='produkt')
        {
            if(!class_exists('C_Products'))
                include_once(PRE_PATH.'php/classes/class.products.php');
			$products = new C_Products(0,'detail',$this->get_id());
			$html .= $products->get_html();
		}
        */
        
        //vypis galerie - vypis slozek, galerii, fotek, detail fotky
        if(MODULE_PRIVATE_PAGES && $typ == 'login')
        {
          
		  include(PRE_PATH.'php/includes/page.login.php');
			
        }
        
        //vypis galerie - vypis slozek, galerii, fotek, detail fotky
        if(MODULE_NEWSLETTER && $typ == 'email')
        {
          
		  include(PRE_PATH.'php/includes/page.email.php');
			
        }
        
                //vypis galerie - vypis slozek, galerii, fotek, detail fotky
        if(MODULE_HELPDESK && $typ == 'helpdesk')
        {
          
		  include(PRE_PATH.'php/includes/page.helpdesk.php');
			
        }


        if($this->get_type() != 'login')
        {
            
            //vypis konkretni galerie pod obsahem stranky 
            /*
            if(MODULE_GALLERY && $this->get('idGalerie') > 0)
            {
    
                if(!class_exists('C_Gallery'))
                    include_once(PRE_PATH.'php/classes/class.gallery.php');
    			
                $gal = new C_Gallery($this->get('idGalerie'));	
                if($gal->exists)
                { 
    			     $html .= $gal->GetGalleryPage();
                }
    					
            }
            */
            
            //vypis ankety pod obsahem stranky
            if(MODULE_INQUIRY && $this->get('idAnkety'))
            {
                if(!class_exists('C_Inquiry'))
    	           include_once(PRE_PATH.'php/classes/class.inquiry.php');
    			$anketa = new C_Inquiry($this->get('idAnkety'));
    			if($anketa->exists)
    				$html .= $anketa->GetInquiryOnPage();
    		}
            //vypis formulare
            if(MODULE_FORMS && $this->get('idFormulare'))
            {
                if(!class_exists('C_Form'))
    			    include_once(PRE_PATH.'php/classes/class.form.php');
    			$formular = new C_Form($this->get('idFormulare'));
    			if($formular->exists)
    				$html .= $formular->GetFormToTable();
    		}
            //vypis editboxu
            if(MODULE_EDITBOX && $this->get('idEditboxu'))
            {
                if(!class_exists('C_Editbox'))
                    include_once(PRE_PATH.'php/classes/class.editbox.php');
                $editbox = new C_Editbox($this->aktualni['idEditboxu']);
    			if($editbox->exists)
    				$html .= $editbox->GetEditbox();
    		}
            //vypis souvisejicich clanku a hodnoceni clanku
            if($this->get_type() == 'clanek')
            {
                if(!class_exists('C_Articles'))
    				    include_once(PRE_PATH.'php/classes/class.articles.php');
    			$articles = new C_Articles(0,'souvisejici',$this->get_id());
                $html .= $this->get_tags();
                $html .= $articles->GetArticleRate();
    			$html .= $articles->GetRelatedArticles();
            }
            //vypis diskuze
            if(MODULE_DISCUSSION && $this->get('diskuze'))
            {
                if(!class_exists('C_Discussion'))
    			    include_once(PRE_PATH.'php/classes/class.discussion.php');
    			$diskuze = new C_Discussion($this->get_id());
    			$html .= $diskuze->GetDiscussion();
    		}
        
        }
        
        return $html;
  
            
    }

    public function get_photos()
    {
        global $db;
        global $domain;
        
        $data = $db->query("SELECT id, soubor, popis FROM ".TABLE_STRANKY_FOTKY." WHERE idStranky=".$this->id." ORDER BY priorita");
        
        $fotky = array();
        
        while($f = $db->getAssoc($data))
        {
            $fotky[$f['id']] = $f;
            $fotky[$f['id']]['obrazek_path'] = $domain->getDir().USER_DIRNAME_STRANKY_MINI.$f['soubor'];
            $fotky[$f['id']]['obrazek_path_maxi'] = $domain->getDir().USER_DIRNAME_STRANKY_MAXI.$f['soubor'];
        }    
         
        return $fotky; 
        
        
    }

    public function get_links()
    {
        global $db;
        global $domain;
        
        $data = $db->query("SELECT id, url, popis FROM ".TABLE_STRANKY_ODKAZY." WHERE idStranky=".$this->id." ORDER BY priorita");
        
        $odkazy = array();
        
        while($f = $db->getAssoc($data))
        {
            $odkazy[$f['id']] = $f;
        }    
         
        return $odkazy; 
        
        
    }
    
    
    
    
    
    
}





?>