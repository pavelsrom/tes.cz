<?php

/**
 * @author Pavel Srom
 * @copyright 2009
 */


class C_Inquiry{
	

	//obsahuje data o ankete
	private $items;
	
	//obsahuje polozky ankety
	private $inquiry;
    
    //indikuje zda uz uzivatel hlasoval
    private $was_voted;
	
    //ip uzivatele
    private $user_ip;
    
    //id ankety
    private $id;
    
    //nazev ankety
    private $name;
    
    //otazka
    private $question;
    
    //odpovedi
    private $answers = array();
    
    //celkovy pocet hlasu v ankete
    private $total_votes = 0;
    
    //systemovy nazev ankety pro nazvy session a cookies
    private $system_name = "";
    
    //indikuje zda anketa existuje
    public $exists = false;
    
    
    //konstruktor
	public function __construct($idInquiry)
    {
		global $domain;
		global $db;
		
		$this->items = array();
		$this->exists = false;
        $this->was_voted = false;
		
		$this->id = intval($idInquiry);
        
        $this->user_ip = get_ip();
        
		if($this->id <= 0) 
            return;
		
        $data = $db->query("SELECT a.nazev, IF(ip.ip IS NULL OR ip.datum != CURDATE(), 0,1) hlasoval, o.otazka 
            FROM ".TABLE_ANKETY." AS a
            LEFT JOIN ".TABLE_ANKETY_IP." AS ip ON a.idAnkety = ip.idAnkety AND ip = '".$this->user_ip."' 
            LEFT JOIN ".TABLE_ANKETY_OTAZKY." AS o ON a.idAnkety = o.idAnkety
            WHERE a.idAnkety = ".$this->id."
                AND (a.od IS NULL OR a.od = '0000-00-00' OR a.od <= CURDATE())
                AND (a.do IS NULL OR a.do = '0000-00-00' OR a.do >= CURDATE())
                AND a.zobrazit = 1
                AND o.idJazyka = ".WEB_LANG_ID."
                AND a.idDomeny = ".$domain->getId()."
            GROUP BY a.idAnkety
            LIMIT 1
            ");
        
			
		if($db->numRows($data) == 0)
        {
            return;
        }
             
        
		
			
        $a = $db->getObject($data);
            
        $this->name = $a->nazev;
        $this->question = $a->otazka;
        $this->was_voted = $a->hlasoval == 1;
        
        
            
        //nacteni otazek
        $data = $db->query("SELECT o.idOdpovedi AS id, o.hlasy, o.barva, n.nazev
            FROM ".TABLE_ANKETY_ODPOVEDI." AS o
            LEFT JOIN ".TABLE_ANKETY_ODPOVEDI_NAZVY." AS n ON o.idOdpovedi = n.idOdpovedi
            WHERE o.idAnkety = ".$this->id."
                AND o.idOdpovedi
                AND n.idJazyka = ".WEB_LANG_ID."
            GROUP BY o.idOdpovedi
            ORDER BY o.priorita
            ");
        
        if($db->numRows($data) == 0)
            return;
        
        $this->exists = true;
            
		$i = 0;
		while($o = $db->getObject($data))
        {
			$this->answers[$o->id] = array(
                'nazev'=>$o->nazev, 
                'barva'=>$o->barva, 
                'hlasy'=>$o->hlasy
                );
			
			$i++;

		}
		
		//zjisteni celkoveho poctu hlasu
		$celkem = 0;
		foreach($this->answers AS $item)
			$celkem += $item['hlasy'];
		
		$this->total_votes = $celkem;
		      
		$this->system_name = 'inquiry_'.$this->id;
	    $this->was_voted = $this->was_voted();   
    
		$db->free($data);
	}
	
    //vraci nazev ankety
    public function get_name()
    {
        return $this->name;
    }
    
    
    //zjisti zda uz uzivatel hlasoval
    private function was_voted()
    {
        $t1 = $this->was_voted;
        $t2 = isset($_COOKIE[$this->system_name]); 
        $t3 = isset($_SESSION[$this->system_name]);
        
        return $t1 || $t2 || $t3;
        
    }
   
	
    
    //vraci html kod pro anketu
	public function get_inquiry()
    {
		
		global $domain;
        global $userSystemMessage;

        $html = "";
        
        if(!$this->exists)
            return $html;
        
		$html .= "<span id='a".$this->id."'></span>";
        
        $html .= "<h4>".TANKETA."</h4>";
		$html .= $userSystemMessage->show_messages(true, $this->system_name);
		
		$html .= "<p><strong>".$this->question."</strong></p>";
        
        //print_r($_COOKIE);
        
		foreach($this->answers AS $id => $item){
			if($this->total_votes == 0)
				$proc = 0;
				else
				$proc = (round($item['hlasy'] / $this->total_votes * 100));
			
			$score = ZOBRAZOVAT_POCET_HLASU ? $item['hlasy'] : $proc."%";
			
			$proc = $proc < 2 ? 1 : $proc;
		
            $html .= "<em>".$item['nazev']."</em>";
            
            
        
            $html .= '<div class="graf-obal">
                            <div class="graf">
                                <div class="value" style="width: '.$proc.'%;"></div>
                            </div>';
            $html .= '<span>'.$score.'</span>';
            
            $povoleno_cookies = isset($_COOKIE['voting_allow']) || (isset($_SERVER['HTTP_REFERER']) && !strstr($_SERVER['HTTP_REFERER'],$domain->getUrl()));
            
            if(!$this->was_voted && $povoleno_cookies)
                $html .= "<a href='".$domain->getRelativeUrl()."?vote=".$this->id."|".$id."' rel='nofollow' class='tlacitko hlasuj'>".THLASOVAT."</a>";
                 
            $html .= '</div>';
        

			}
		
		return $html;
		
	}
	
	
	
	
	
			
}




?>