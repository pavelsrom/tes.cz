<?php
//namespace trialog;

/**
 * Calendar helper: constants
 *
 * @author Olivier Maridat (Trialog)
 * @link http://www.trialog.com
 */
final class SimpleCalendarConst
{

	const Sunday = 0;

	const Monday = 1;

	const Tuesday = 2;

	const Wednesday = 3;

	const Thursday = 4;

	const Friday = 5;

	const Saturday = 6;

	const Today = 'today';

	const Tomorrow = 'tomorrow';

	const Yesterday = 'yesterday';
}
