<?php

/**
 * @author Pavel Srom
 * @copyright 2010
 */


class C_Products{
    
    public $items;
    public $exists;
    
    private $link;
    private $name;
    private $product_id = 0;
    private $category_id = 0;
    
    private $type = "";
    
    //celkovy pocet prooduktu
    private $count_all_products = 0;

    //mozne typy stranka, produkt, rubriky
    public function __construct($idBloku=0, $typ='stranka', $idProduktu=0){
        global $db;
        global $domain;
        global $links;
        global $pagination;
        global $private_pages;
        
        $this->exists = false;
        $this->type = $typ;
        
        $this->product_id = intval($idProduktu);
        $this->category_id = intval($idBloku);
                       
        $this->link = $links->get_url($idBloku);
                  
        if($typ == 'detail' && $idProduktu>0){
            $data = $db->Query("SELECT idStranky, nazev, obrazek, popis1, popis2, popis3, popis4, popis5, dph, zobrazit_cenu, 
                s.cena_s_dph, perex, obsah, s.datumAktualizoval AS aktualizace, 
                s.cena_bez_dph, s.delka, s.sirka, s.hloubka_bazenu AS hloubka, s.objem, s.celkovy_rozmer, s.vnitrni_rozmer,
                s.vaha AS vaha, s.status,
                DATE_FORMAT(datum,'%d.%m.%Y') AS datum, DATE_FORMAT(datum, '%h:%i') AS cas, autor
    			FROM ".TABLE_STRANKY." AS s
    			WHERE idDomeny=".$domain->getId()."
                    AND idJazyka=".WEB_LANG_ID."
                    AND typ='produkt'
    				AND zobrazit=1
    				AND (od <= CURDATE() OR od IS NULL)
    				AND (do >= CURDATE() OR do IS NULL)
                    AND archivovat = 0
                    AND idStranky=".$idProduktu."
                ORDER BY priorita, idStranky
                LIMIT 1
    		  ");
            if($db->numRows($data)>0)
                $this->exists = true;
            }
        
        elseif($typ == 'stranka' || $typ == 'novinky' || $typ == 'hlavni_kategorie')
            {
            
            if($typ == 'novinky')
                $limit = (($pagination->get_current_page() - 1) * 6).",6";
                else
                $limit = ($pagination->get_current_page() - 1) * intval(POCET_PRODUKTU_NA_STRANKU).",".intval( 	POCET_PRODUKTU_NA_STRANKU);
            
            if($typ == 'novinky')
            {
                if($this->product_id > 0)
                    $where = "AND s.status = ".$this->product_id;
                    else
                    $where = "AND s.status IN (1, 2)";

            }
            elseif($typ == 'hlavni_kategorie')
            {
                //zjisteni klasickych produktovych kategorii z nadrazene hlavni kategorie
                $data_r = $db->query("SELECT idStranky AS id FROM ".TABLE_STRANKY." WHERE idRodice=".$idBloku);

                $pages = array(0);
                while($p = $db->getObject($data_r))
                    $pages[] = $p->id;

                $where = "AND s.idRodice IN (".implode(", ", $pages).")";

            }
            else
                $where = "AND s.idRodice=".$idBloku."";
            
            $sort = get_request("sort", get_session("sort", "a-z"));
            set_session("sort", $sort);

            if($sort == 'a-z')
                $order = "s.nazev";
            elseif($sort == 'cena-vzestupne')
                $order = "s.".$sl_cena_s_dph;
            elseif($sort == 'cena-sestupne')
                $order = "s.".$sl_cena_s_dph." DESC";
            else
                $order = "s.datum DESC, s.idStranky DESC";

            $data = $db->Query("SELECT SQL_CALC_FOUND_ROWS s.idStranky, s.nazev, s.perex, s.obrazek, s.autor, COUNT(d.idPolozky) AS pocet_komentaru, r.idStranky AS idKategorie, r.url AS urlKategorie, r.nazev AS nazevKategorie, j.jazyk, s.obsah, 
            s.cena_s_dph, s.status, s.datumAktualizoval AS aktualizace,
            s.cena_bez_dph, s.dph, s.vaha AS hmotnost, 
            s.delka, s.sirka, s.hloubka_bazenu AS hloubka, s.objem, s.celkovy_rozmer, s.vnitrni_rozmer,
            s.zobrazit_cenu,
                DATE_FORMAT(s.datum,'%d.%m.%Y') AS datum, DATE_FORMAT(s.datum, '%h:%i') AS cas, s.popis2
    			FROM ".TABLE_STRANKY." AS s
                LEFT JOIN ".TABLE_JAZYKY." AS j ON s.idJazyka = j.idJazyka
                LEFT JOIN ".TABLE_STRANKY." AS r ON s.idRodice = r.idStranky
                LEFT JOIN ".TABLE_DISKUZE_POLOZKY." AS d ON s.idStranky=d.idStranky AND d.zobrazit = 1
    			WHERE s.idDomeny=".$domain->getId()."
                    AND s.idJazyka=".WEB_LANG_ID."
                    AND s.typ='produkt'
                    AND r.typ='produkty'
    				AND s.zobrazit=1
    				AND (s.od <= CURDATE() OR s.od IS NULL)
    				AND (s.do >= CURDATE() OR s.do IS NULL)
                    ".$where." 
                GROUP BY s.idStranky
    			ORDER BY ".$order."
                LIMIT ".$limit."
    		  ");

            $this->exists = $db->numRows($data)>0;
            
            };
                        
        $celkem_data = $db->Query("SELECT FOUND_ROWS()");
        

        $celkem_arr = $db->getRow($celkem_data);
        
        $pagination->set_number_items($celkem_arr[0]);
        $this->count_all_products = $celkem_arr[0];
        
        $i = 0;
        while($a = $db->getAssoc($data)){
            
            if($a['obrazek']!= '' && file_exists(PRE_PATH.$domain->getDir().USER_DIRNAME_ESHOP_MINI.$a['obrazek'] )){
				$img = RELATIVE_URL_USER_DOMAIN.USER_DIRNAME_ESHOP_MINI.$a['obrazek'];				
				}
				else 
                $img = RELATIVE_URL_USER_DOMAIN."img/product-default.jpg";
		
            if($a['obrazek']!= '' && file_exists(PRE_PATH.$domain->getDir().USER_DIRNAME_ESHOP_MAXI.$a['obrazek'] )){
				$img_maxi = RELATIVE_URL_USER_DOMAIN.USER_DIRNAME_ESHOP_MAXI.$a['obrazek'];				
				}
				else 
                $img_maxi = RELATIVE_URL_USER_DOMAIN."img/product-default.jpg";
					
			if($a['obrazek']!= '' && file_exists(PRE_PATH.$domain->getDir().USER_DIRNAME_ESHOP_STREDNI.$a['obrazek'] )){
				$img_stredni = RELATIVE_URL_USER_DOMAIN.USER_DIRNAME_ESHOP_STREDNI.$a['obrazek'];				
				}
				else 
                $img_stredni = RELATIVE_URL_USER_DOMAIN."img/product-default.jpg";
                
                			
			$link = $links->get_url($a['idStranky']);
								
			$linkname = $a['nazev'];
            $aktualizace = "?".str_replace(array(" ",":", "-"), "", $a['aktualizace']);

            $this->items[$i] = $a;
            $this->items[$i]['odkaz'] = $link;
			$this->items[$i]['nazev_odkazu'] = $linkname;
            $this->items[$i]['obrazek'] = $a['obrazek'];
            $this->items[$i]['obrazek_path'] = $img.$aktualizace;
            $this->items[$i]['obrazek_path_maxi'] = $img_maxi.$aktualizace;
            $this->items[$i]['obrazek_path_stredni'] = $img_stredni.$aktualizace;
            $this->items[$i]['id'] = $a['idStranky'];
            if(isset($a['nazevKategorie']))
                $this->items[$i]['kategorie'] = $a['nazevKategorie'];
            if(isset($a['urlKategorie']))
                $this->items[$i]['url_kategorie'] = UrlPage($a['urlKategorie'],$a['idKategorie'],$a['jazyk']);
            if(isset($a['pocet_komentaru']))
                $this->items[$i]['pocet_komentaru'] = intval($a['pocet_komentaru']);
            $i++;

            }
            
        
        
    }
        
    //vraci pocet vsech produktu v databazi
    public function get_count_all_products()
    {
        return $this->count_all_products;
    }

    //vraci pocet nactenych produktu
    public function get_count_products()
    {
        return count($this->items);
    }
        
    public function get_html()
    {
        $html = "";
        if($this->type == 'stranka' || $this->type == 'novinky' || $this->type == 'hlavni_kategorie')
            $html = $this->GetProductsOnPage();
        elseif($this->type == 'detail')
            $html .= $this->getDetail();
        return $html;
        
    }
    
    private function get_photos()
    {
        global $db;
        global $domain;
        
        $data = $db->query("SELECT id, soubor, popis FROM ".TABLE_STRANKY_FOTKY." WHERE idStranky=".$this->product_id." ORDER BY priorita");
        
        $fotky = array();
        
        while($f = $db->getAssoc($data))
        {
            $fotky[$f['id']] = $f;
            $fotky[$f['id']]['obrazek_path'] = $domain->getDir().USER_DIRNAME_STRANKY_MINI.$f['soubor'];
            $fotky[$f['id']]['obrazek_path_maxi'] = $domain->getDir().USER_DIRNAME_STRANKY_MAXI.$f['soubor'];
        }    
         
        return $fotky; 
        
        
    }
    
    private function getDetail()
    {
        global $db;
        global $page;
        
        $produkt = $this->items[0];
        
        $html = "";
        
        $fotky = $this->get_photos();
        

        $html .= '<div class="detail">';
        $html .= '<h2>'.$produkt['nazev'].'</h2>';
        $html .= '<div class="detail-info">
            <div class="detail-slider">
                <div class="tags">';

                if($produkt['status'] == 1)
                    $html .= '<span class="new">
                        <img src="'.RELATIVE_URL_USER_DOMAIN.'img/tag-news.png" alt="">
                    </span>';

                if($produkt['status'] == 2)
                    $html .= '<span class="action">
                        <img src="'.RELATIVE_URL_USER_DOMAIN.'img/tag-action.png" alt="">
                    </span>';
                
                if($produkt['status'] == 3)
                    $html .= '<span class="recomended">
                        <img src="'.RELATIVE_URL_USER_DOMAIN.'img/tag-recomended.png" alt="">
                    </span>';

                    
                $html .= '</div>
                <div class="detail-slider-inner">';
      
              
        //if($produkt['obrazek'] != '')
            $html .= '<div class="detail-slide">
                        <a href="'.$produkt['obrazek_path_maxi'].'"  data-lightbox="image"  data-title="'.$produkt['nazev'].'">
                            <img src="'.$produkt['obrazek_path_stredni'].'" alt="'.$produkt['nazev'].'">
                        </a>
                    </div>';


        foreach($fotky AS $f)
        {
            $html .= '<div class="detail-slide">
                        <a href="'.$f['obrazek_path_maxi'].'"  data-lightbox="image"  data-title="'.$produkt['nazev'].'">
                            <img src="'.$f['obrazek_path'].'" alt="'.strip_tags($f['popis']).'">
                        </a>
                    </div>';

        }
            

                $html .= '</div>
            </div>
            <div class="detail-text" id="info">
                <p>'.get_weight($produkt['hmotnost'], $produkt['hmotnost_jednotka']).'</p>
                <p class="price">'.TCENA_S_DPH.': <strong>'.price($produkt['cena_s_dph']).'</strong> <span>s DPH</span></p>
                <form action="#info" class="add-to-basket" method="POST">
                    <div class="in">
                        <div class="message"></div>
                        <input type="number" name="pocet" value="1" min="1" max="999">
                        <input type="hidden" name="id_produkt" value="'.$produkt['id'].'">
                        <input type="hidden" name="btnKoupit" value="1">
                        <button type="submit" class="btn"><span>'.TKOUPIT_NA_ESHOPU.'</span></button>
                        
                    </div>
                </form>
                <p>
                    '.nl2br(strip_tags($produkt['perex'])).'
                <p>
                    <a href="#zalozky">'.TDALSI_INFORMACE.' &raquo;</a>
                </p>
            </div>
        </div>
        '.$this->getBookmarks().'
    </div>';



/*
        if($produkt['obrazek'] != '')
            $html .= '<div class="produktFoto fr">
                
                <a href="'.$produkt['odkaz'].'"  title="'.$produkt['nazev'].'">
                    <img src="'.$produkt['obrazek_path_stredni'].'" alt="'.$produkt['nazev'].'">
                </a>
                <a href="#" class="galerie">'.TGALERIE.' ('.(count($fotky) + 1).')</a>
                </div>';
        
        foreach($fotky AS $f)
        {
            $html .= '<a href="'.$f['obrazek_path_maxi'].'" data-lightbox="roadtrip" class="nodisplay" title="'.strip_tags($f['popis']).'"></a>';
        }
        
        
        $html .= '<div class="produktDetail">';
        $html .= $page->get_content();
        if($produkt['cena'] > 0 && $produkt['zobrazit_cenu'] == 1)
        {
                
            $html .= '<p class="cena">'.TCENA_BEZ_DPH.": <strong>".$this->price($produkt['cena']).'</strong>';
                
            if($produkt['dph'] > 0)
                $html .= '<br />'.TCENA_S_DPH.': '.$this->price($produkt['cena'],$produkt['dph']);
            
            $html .= '</p>';
                    
        }
                
        $html .= '</div>';
        $html .= '<div class="cleaner"></div>';
        
        $html .= $this->getBookmarks();
        $html .= '<div class="cleaner"></div>';
        */
        return $html;
        
    }
    
    private function getBookmarks($idx = 0)
    {
        
        $produkt = $this->items[$idx];
        $zalozky = array();
        $zalozky[1] = array("nazev" => TPRODUKT_ZALOZKA1, "obsah" => $produkt['obsah']);
        $zalozky[2] = array("nazev" => TPRODUKT_ZALOZKA2, "obsah" => $produkt['popis1']);
        /*
        $zalozky[3] = array("nazev" => TPRODUKT_ZALOZKA3, "obsah" => $produkt['popis3']);
        $zalozky[4] = array("nazev" => TPRODUKT_ZALOZKA4, "obsah" => $produkt['popis4']);
        $zalozky[5] = array("nazev" => TPRODUKT_ZALOZKA5, "obsah" => $produkt['popis5']);
        */  


        $html = '<div class="detail-tabs" x-data="{ tab: window.location.hash ? window.location.hash.substring(1) : \'popis\' }" id="zalozky">
            <ul>
                <li>
                    <a href="#popis" :class="[ tab === \'popis\' ? \'active-tab\' : \'\' ]"
                        @click.prevent="tab = \'popis\'; window.location.hash = \'popis\'" >'.TPRODUKT_ZALOZKA1.'</a>
                </li>
                <li>
                    <a href="#slozeni" :class="[ tab === \'slozeni\' ? \'active-tab\' : \'\' ]"
                        @click.prevent="tab = \'slozeni\'; window.location.hash = \'slozeni\'" >'.TPRODUKT_ZALOZKA2.'</a>
                </li>
            </ul>
        <div class="tabs-content">
            <div x-show="tab === \'popis\'">
                
                    <h3>'.TPRODUKT_ZALOZKA1.'</h3>
                    '.$produkt['obsah'].'
                
            </div>
            <div x-show="tab === \'slozeni\'">
                
                <h3>'.TPRODUKT_ZALOZKA2.'</h3>
                '.$produkt['popis1'].'
                
            </div>
            </div>
        </div>';
        
        /*
        $html = "";
        
        //zalozky
        $html .= '<div id="bookmarksObal">';
        $html .= '<div id="bookmarks">';
        
        foreach($zalozky AS $idz => $z)
            $html .= '<a href="#" id="bm'.$idz.'">'.$z['nazev'].'</a>';
            
        $html .= '<div class="cleaner"></div>';
        $html .= '</div>';
        $html .= '</div>';
        
        foreach($zalozky AS $idz => $z)
            $html .= '<div class="bm'.$idz.' bmObsah">
                <h2>'.$z['nazev'].'</h2>
                '.editable($this->product_id,$z['obsah'],'popis'.$idz).
                '</div>';
        
        */
        
        
        return $html;
    }
        
    public function GetProductsOnPage(){
        global $domain;
        global $pagination;
        
		$html = "";
		
        /*
        if(count($this->items)==0) 
            return $html; 
        */
		$pocet = intval(POCET_PRODUKTU_NA_STRANKU);
		$pagination->set_number_items_on_page($pocet);
        
		$stranka = $pagination->get_current_page();
		$od = ($stranka-1) * $pocet;
		
		
		//$html .= "<span id='produkty'></span>";

		$i = 0;
        
        //$html .= "<h2>".TNEJPRODAVANEJSI_PRODUKTY."</h2>";
        
        //$html .= $this->filter();
        //$html .= '<div class="flex">';

        if(count($this->items) > 0)
        {
            $html .= '<ul class="pools">';

            foreach($this->items AS $id => $pol)
            {

                $html .= '<li class="pool-item">
                <h3>
                  <a href="'.$pol['odkaz'].'">'.colorize($pol['nazev']).'</a>
                </h3>
                <div class="pool-img">
                  <a href="'.$pol['odkaz'].'">
                    <img src="'.$pol['obrazek_path_stredni'].'" alt="'.$pol['nazev'].'">
                  </a>
                </div>
                <ul class="pool-atributes">
                  <li>
                    <p class="atribures-headline">Délka</p>
                    <p>'.$pol['delka'].' m</p>
                  </li>
                  <li>
                    <p class="atribures-headline">Šířka</p>
                    <p>'.$pol['sirka'].' m</p>
                  </li>
                  <li>
                    <p class="atribures-headline">Hloubka</p>
                    <p>'.$pol['hloubka'].' m</p>
                  </li>
                  <li>
                    <p class="atribures-headline">Barvy</p>
                    <span class="pool-color pool-white"></span>
                    <span class="pool-color pool-blue"></span>
                    <span class="pool-color pool-gray"></span>
                  </li>
                  <li class="more-pool">
                    <a href="'.$pol['odkaz'].'">Více info</a>
                  </li>
                </ul>
              </li>';

                $i++;
            }

            $html .= '</ul>';
        }
        else
        {
            $html .= '<p>'.TFILTR_ZADNY_ZAZNAM.'</p>';
        }


        //$html .= '</div>';
        
		//strankovani
		//$html .= $pagination->set_url_tail("#produkty")->get_html();
       
        
		return $html;
        
        
    }
    
    private function filter()
    {
        global $db;
        global $domain;
        
        $typ_bydleni = array(0 => "-- ".TFILTR_TYP_BYDLENI." --");
        $data_ciselnik= $db->query("SELECT id, nazev_cz AS nazev FROM ".TABLE_CISELNIK_TYP_BYDLENI." ORDER BY nazev");
        while($r = $db->getAssoc($data_ciselnik))
            $typ_bydleni[$r['id']] = $r['nazev']; 
        
        $html_sel1 = frm_selectbox("typ", $typ_bydleni, "custom-select apartmany","sources", TFILTR_TYP_BYDLENI);
             
        $mesta = array(0 => "-- ".TFILTR_MESTO." --");
        $data_ciselnik= $db->query("SELECT id, nazev FROM ".TABLE_CISELNIK_MESTA." WHERE id_rodic = 0 ORDER BY nazev");
        while($r = $db->getAssoc($data_ciselnik))
        {
            $mesta[$r['id']] = $r['nazev'];
            
            $d = $db->query("SELECT id, nazev FROM ".TABLE_CISELNIK_MESTA." WHERE id_rodic = ".$r['id']." ORDER BY nazev");
            while($rr = $db->getAssoc($d))
                $mesta[$rr['id']] = " - ".$rr['nazev']; 
         
        }
        $html_sel2 = frm_selectbox("mesto", $mesta, "custom-select poloha","poloha", TFILTR_MESTO);
        
        $min = $db->get(TABLE_STRANKY,"MIN(cena)",'zobrazit=1 AND idJazyka='.WEB_LANG_ID.' AND cena > 0 AND typ="produkt" AND idDomeny='.$domain->getId());
        $max = $db->get(TABLE_STRANKY,"MAX(cena)",'zobrazit=1 AND idJazyka='.WEB_LANG_ID.' AND cena > 0 AND typ="produkt" AND idDomeny='.$domain->getId());
        
        
        $html = "";
        $html .= '<div class="filtrovani">';
		$html .= '<form action="#content" method="get">
					<div class="selekty">
                    
						'.$html_sel1.'
                        '.$html_sel2.'
						
					</div>
					<div class="range">
						<span>'.TCENA_DO.' <span id="cenicka">'.number_format(get_int_get("cena", $max), 0, "", ".").'</span> '.MENA.'</span>
						<input type="range" min="'.$min.'" max="'.$max.'" step="10000" value="'.get_int_get("cena", $max).'" data-orientation="horizontal" name="cena">
					</div>
					<input type="submit" value="'.THLEDAT.'">';
                    
		$html .= '</form>
			</div>';
        
        return $html;
        
        
    }
    
    private function price($cena, $dph = 0)
    {
        $res = "";
        
        if($cena <= 0)
            return $res;
            
        if($dph == 0)
            $res = number_format($cena, 0, ',', ' ')." ".MENA;
            else
            $res = number_format(round($cena + $cena * $dph / 100), 0, ',', ' ')." ".MENA;
            
        return $res;    
        
        
    }
    
    
        
    
    public function GetRelatedProducts(){
        global $links;
        
        $html = "";
        
        
        $vypis_sc = array();
        
        //souvisejici clanky zadane rucne
        if(isset($this->items))
        {
            foreach($this->items AS $id => $pol)
            {
                $vypis_sc[$pol['idStranky']] = $pol['idStranky']; 
            }
        }
        
        
        //souvisejici clanky automaticky generovane pres tagy
        $tagy = new C_Tags($this->product_id,'stranka',true,true);
        $tagy->set_lang(WEB_LANG_ID);
        $id_souvisejicich_produktu = $tagy->get_related_objects();
        
        
        foreach($id_souvisejicich_produktu AS $id => $pol)
        {
            
                
            $vypis_sc[$pol['id']] = $pol['id'];
            
        }
        
        
        if(count($vypis_sc)==0 || ZOBRAZOVAT_SOUVISEJICI_PRODUKTY == 0) return $html;
        
        
        $html .= "<h3>".TSOUVISEJICI_CLANKY."</h3>";
        $html .= "<div class='related-articles'>";
        
        $souvisejici = array();
        $i = 0;
        foreach($vypis_sc AS $id => $nazev)
        {
            if($i >= POCET_SOUVISEJICICH_PRODUKTU) 
                break;
                
            $souvisejici[] = "<a href='".$links->get_url($id)."?tag=related'>".$nazev."</a>";    			         
            $i++;
		}
        
        $html .= implode("<br />", $souvisejici);
        
        $html .= "</div>";
        
        return $html;
    }
    
    
    //vraci data clanku
    public function get_data($idx = null)
    {
        if(is_null($idx))
            return $this->items;
            else
            return $this->items[$idx];
    }
    
    //vraci nazev bloku
    public function get_name()
    {
        return $this->name;
    }
    
    //vraci url rubriky 
    public function get_url_category()
    {
        return $this->link;
    }
    
    //vraci id rubriky
    public function get_category_id()
    {
        return $this->category_id;
    }
    
    
}




?>