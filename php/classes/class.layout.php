<?php

/**
 * @author lolkittens
 * @copyright 2013
 */



class C_Layout{
    
    //javascriptove soubory - po nacteni webu ve footeru
    private $js = array();
    
    //javascript v head
    private $js_head = array();
    private $css_head = array();
    
    //klasicke css
    private $css = array();
    
    //tiskove css
    private $css_print = array();
    
    //cesta k css souborum
    private $css_path = "";
    
    private $link_preload = array();
    
    //cesta k js souborum
    private $js_path = "";
    
    //urcuje zda bude mit html dokument hlavicku podle normy html5
    private $is_html5 = false;
    
    //js kod, ktery se vklada do headeru
    private $js_inline = "";
    
    //cesta k favicon
    private $favicon = "";
    
    //link rel preconnect
    private $preconnect = array();
    
    //konstruktor
    public function __construct()
    {
         $this->set_default_path();
    }
    
    
    public function set_preconnect($preconnected)
    {
        $this->preconnect = $preconnected;
    }
    
    
    public function set_default_path()
    {
        $this->css_path = RELATIVE_URL_USER_DOMAIN.USER_DIRNAME_CSS;
        $this->js_path = RELATIVE_URL_USER_DOMAIN.USER_DIRNAME_JS;
        
    }
    
    public function set_js_path($path)
    {
        $this->js_path = $path;
    }
    
    public function set_css_path($path)
    {
        $this->css_path = $path;
    }
    
    //prida do headeru odkaz na css soubor
    public function add_css($filename, $params = array(), $preload = false, $print = false)
    {
        $path = strstr($filename,"http") || strstr($filename,"https") ? "" : $this->css_path;
        
        if($preload)
            $this->link_preload[] = array($path.$filename, "style");
        
        if($print)
            $this->css_print[$filename] = $path.$filename;
            else
            $this->css[$filename] = array($path.$filename, "", 0, $params);
            
        
    }

    public function add_css_for_print($filename)
    {
        $path = strstr($filename,"http") || strstr($filename,"https") ? "" : $this->css_path;
        $this->css_print[$filename] = $path.$filename;
    }
    
    public function add_preload_link($filename, $type, $crossorigin = false)
    {
        $crossorigin = $type == 'font' || $crossorigin ? 'crossorigin' : '';
        $this->link_preload[] = array($filename, $type, $crossorigin);
    }
    

    
    //prida do headeru odkaz na css soubor
    public function add_css_for_mobile($filename, $max_width = 480)
    {
        $path = strstr($filename,"http") || strstr($filename,"https") ? "" : $this->css_path;
        $this->css[$filename] = array($path.$filename, 'mobile', $max_width);
  
  
    }
    
    //prida do headeru odkaz na css soubor
    public function add_css_for_tablet($filename, $max_width = 670)
    {
        $path = strstr($filename,"http") || strstr($filename,"https") ? "" : $this->css_path;
        $this->css[$filename] = array($path.$filename, "tablet", $max_width);
  
    }
    
    //prida do headeru odkaz na css soubor
    public function add_css_for_pc($filename, $max_width = 768)
    {
        $path = strstr($filename,"http") || strstr($filename,"https") ? "" : $this->css_path;
        $this->css[$filename] = array($path.$filename, "pc", $max_width);
  
    }
    
    //prida do headeru odkaz na css soubor
    public function add_js($filename, $params = array(), $preload = false)
    {
        
            
        $path = strstr($filename,"http") || strstr($filename,"https") || strstr($filename,"//") ? "" : $this->js_path;
        
        if($preload)
            $this->link_preload[] = array($path.$filename, "script");
        
        
        $this->js[$filename] = array($path.$filename, $params);
    }
    
    public function add_css_to_head($filename, $params = array(), $preload = false)
    {
        $path = strstr($filename,"http") || strstr($filename,"https") || strstr($filename,"//") ? "" : $this->css_path;
        
        
        if($preload)
            $this->link_preload[] = array($path.$filename, "style");
        
        $this->css_head[$filename] = array($path.$filename, $params);
    }
    
    public function add_js_to_head($filename, $params = array(), $preload = false)
    {
        
        
        $path = strstr($filename,"http") || strstr($filename,"https") || strstr($filename,"//") ? "" : $this->js_path;
        
        if($preload)
            $this->link_preload[] = array($path.$filename, "script");
        
        $this->js_head[$filename] = array($path.$filename, $params);
    }
    
    public function add_inline_js($js)
    {
        
        $this->js_inline .= $js." \n";
        
    }
    
    //nastavi obrazek favicon do headeru
    public function set_favicon($favicon)
    {
        $this->favicon = $favicon; 
    }
        
    //vraci hlavicku html dokumentu 
    public function get_header()
    {
        global $page;
        global $domain;
        global $pagination;
        
        $html = "";

        $html .= "<!DOCTYPE html>\n";

        $lang = LANG;
        if($lang == 'at')
            $lang = 'de';
        elseif($lang == 'cz')
            $lang = 'cs';

        if(MODULE_FACEBOOK && FACEBOOK_APP_ID)
            $html .= '<html xmlns:fb="http://ogp.me/ns/fb#">';
            else
            $html .= "<html lang='".$lang."'>\n";

        
        
        $html .= "<head>\n";
        
        
        $titulek = $page->get("title");
        
        if(TITULEK_PREFIX != '' && $titulek!='')
            $titulek = TITULEK_PREFIX." ".$titulek;
        
        if(TITULEK_POSTFIX != '')
            $titulek = $titulek." ".TITULEK_POSTFIX;
        
		$html .= "<title>".$titulek."</title>\n";

        $html .= '<meta charset="utf-8" />'."\n";
        $html .= '<meta http-equiv="X-UA-Compatible" content="IE=edge">'."\n";
        $html .= '<meta name="viewport" content="minimum-scale=1.0, width=device-width, initial-scale=1.0, shrink-to-fit=no">'."\n";

        
        $url_stranky = PROTOKOL.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
        
        if(MODULE_FACEBOOK/* && FACEBOOK_APP_ID!='' && FACEBOOK_ADMIN_ID!=''*/)
            {
            $html .= "<meta property='og:title' content='".$page->get('title')."'/>\n";
            $html .= "<meta property='og:type' content='article'/>\n";
            $html .= "<meta property='og:url' content='".$url_stranky."'/>\n";
            
            //print_r($pages->aktualni);
            
            if($page->get_type() =='novinka'){
                $img = $domain->getDir().USER_DIRNAME_NEWS.$page->get('obrazek');
                if(file_exists($img))
                    $html .= "<meta property='og:image' content='".$domain->getUrl().$img."'/>\n";
                }
            if($page->get_type() =='clanek'){
                $img = $domain->getDir().USER_DIRNAME_ARTICLES_MAXI.$page->get('obrazek');
                if(file_exists($img))
                    $html .= "<meta property='og:image' content='".$domain->getUrl().$img."'/>\n";
                }
            
            $html .= '<meta property="og:image:type" content="image/jpeg">';
            $html .= "<meta property='og:site_name' content='".NAZEV_WEBU."'/>\n";
            //$html .= "<meta property='fb:app_id' content='".FACEBOOK_APP_ID."'/>\n";
            //$html .= "<meta property='fb:admins' content='".FACEBOOK_ADMIN_ID."'/>\n";
            $html .= "<meta property='og:description' content='".$page->get('description')."'/>\n";
            $html .= "<meta property='og:email' content='".EMAIL_WEBU."'/>\n";
            }
        
        if(POVOLIT_INDEXACI_WEBU)
		      $html .= "<meta name='robots' content='index, follow' />\n";
              else
              $html .= "<meta name='robots' content='noindex, nofollow' />\n";
            
        //$html .= "<title>".$page->get('title')."</title>\n";
        
        if($page->get_type() =='novinka'){
            $img = $domain->getDir().USER_DIRNAME_NEWS.$page->get('obrazek');
            if(file_exists($img))
            {
                $html .=  "<link rel='image_src' href='".$domain->getUrl().$img."' />\n";
                $html .=  "<link rel='previewimage' href='".$domain->getUrl().$img."' />\n";
                    
            }   
        }
        
        if($page->get_type()=='clanek'){
            $img = $domain->getDir().USER_DIRNAME_ARTICLES_MAXI.$page->get('obrazek');
            if(file_exists($img))
            {
                $html .=  "<link rel='image_src' href='".$domain->getUrl().$img."?1' />\n";
                $html .=  "<link rel='previewimage' href='".$domain->getUrl().$img."?1' />\n";
            }
        }
        
        
        if(count($this->link_preload) > 0)
        {
            foreach($this->link_preload AS $preload)
            {
                $crossorigin = isset($preload[2]) ? $preload[2] : "";
                $html .= '<link rel="preload" href="'.$preload[0].'" as="'.$preload[1].'" '.$crossorigin.'>';
                $html .= "\n";
            }
        }
        
        
        foreach($this->preconnect AS $link)
        {
            $html .= "<link rel='preconnect' href='".$link."' />\n";
        }
                
        if(GOOGLE_WEBMASTER_TOOLS_KOD != '')
            $html .= '<meta name="google-site-verification" content="'.GOOGLE_WEBMASTER_TOOLS_KOD.'" />';
        
        //canonical
        $url_canonical = explode("?",$url_stranky);
        $url_canonical = $url_canonical[0];        
        $html .=  "<link rel='canonical' href='".$url_canonical."' />\n";
        
        
        //pagination
        $url_prev = $pagination->get_prev_url();
        $url_next = $pagination->get_next_url();
        
        if($url_prev != '')
            $html .= '<link rel="prev" href="'.$url_prev.'" />';
            
        if($url_next != '')
            $html .= '<link rel="next" href="'.$url_next.'" />';
        
        //description
		$html .=  "<meta name='description' content='".$page->get('description')."' />\n";
        
        //autor
        $html .=  "<meta name='author' content='".META_AUTOR."'/>\n";
        
        
        //favicon
        if($this->favicon != "")
        {
            $html .= '<link rel="shortcut icon" href="'.$this->favicon.'" type="image/x-icon" />
                        <link rel="icon" href="'.$this->favicon.'" type="image/x-icon" />'."\n";
        }
        
        
        $html .= '<!--[if lt IE 9]><script src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->';
        
        
        
        /*
        if($this->is_html5)
            $html .= '<!--[if IE]><script src="js/html5.js"></script><![endif]-->';
        */
        

            
        foreach($this->css_print AS $css_print)
			$html .= "<link rel='stylesheet' type='text/css' media='print' href='".htmlspecialchars($css_print)."' />\n";
		
        
			

        foreach($this->css_head AS $css)	
			$html .= "<link rel='stylesheet' type='text/css' media='print,screen' href='".htmlspecialchars($css[0])."' />\n";
        
        
		foreach($this->js_head AS $js)	
			$html .= "<script type='text/javascript' src='".htmlspecialchars($js[0])."'></script>\n";
        
        
        
        //systemove js soubory
        //$html .= "<script type='text/javascript' src='".$domain->getUrl()."js/fe_ajax.js'></script>\n";
        
        
        $html .= '<!--[if lt IE 9]>';
        
        foreach($this->css AS $css)
        {
            $cesta = $css[0];
            $typ = isset($css[1]) ? $css[1] : "";
            $max_width = isset($css[2]) ? $css[2] : 0;
            $params = $this->get_params_string($css[3]);
            
            if($typ == "mobile" || $typ == "tablet" || $typ == "pc")
                $html .= "<link rel='stylesheet' type='text/css' href='".$cesta."'  ".$params."/>\n";
            
                
        }
        
        //$html .= "<link rel='stylesheet' type='text/css' href='".RELATIVE_URL_USER_DOMAIN."css/ie.css' />\n";
        $html .= '<![endif]-->';
        
        
        
        
        
        if(GA_KOD!='')
        {
              
            /*
            $html .= '                
                
                <!-- Global site tag (gtag.js) - Google Analytics -->
                <script async src="https://www.googletagmanager.com/gtag/js?id='.GA_KOD.'"></script>
                <script>
                  window.dataLayer = window.dataLayer || [];
                  function gtag(){dataLayer.push(arguments);}
                  gtag("js", new Date());
                
                  gtag("config", "'.GA_KOD.'");
                </script>

            ';
            */

            $html .= "<!-- Google Tag Manager -->
            <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','".GA_KOD."');</script>
            <!-- End Google Tag Manager -->";

            /*
            $html .= '<!-- Cookie Consent by https://www.CookieConsent.com -->
            <script type="text/javascript" src="//www.cookieconsent.com/releases/4.0.0/cookie-consent.js" charset="UTF-8"></script>
            <script type="text/javascript" charset="UTF-8">
            document.addEventListener(\'DOMContentLoaded\', function () {
            cookieconsent.run({"notice_banner_type":"headline","consent_type":"express","palette":"light","language":"cs","page_load_consent_levels":["strictly-necessary"],"notice_banner_reject_button_hide":false,"preferences_center_close_button_hide":false,"website_name":"Tesmotors.cz"});
            });
            </script>
            
            <!-- Unnamed script -->
            <script type="text/plain" cookie-consent="functionality">
            var dataLayer = window.dataLayer || [];
            dataLayer.push({
              event:"addConsent",
              consentType:"personalizace"
            });
            </script>
            <!-- end of Unnamed script-->
            
            <!-- Unnamed script -->
            <script type="text/plain" cookie-consent="tracking">
            var dataLayer = window.dataLayer || [];
            dataLayer.push({
              event:"addConsent",
              consentType:"tracking"
            });
            </script>
            <!-- end of Unnamed script-->
            
            <!-- Unnamed script -->
            <script type="text/plain" cookie-consent="targeting">
            var dataLayer = window.dataLayer || [];
            dataLayer.push({
              event:"addConsent",
              consentType:"ads"
            });
            </script>
            <!-- end of Unnamed script-->
            
            <noscript>ePrivacy and GPDR Cookie Consent by <a href="https://www.CookieConsent.com/" rel="nofollow noopener">Cookie Consent</a></noscript>
            <!-- End Cookie Consent by https://www.CookieConsent.com -->';
            */

        }
        
        
        $html .= '<!-- Podpora html5 v IE starsim nez IE9 -->
    <!--[if lt IE 9]>
    <script type="text/javascript" src="'.RELATIVE_PATH.'js/html5shiv.min.js" />
    </script>
    <![endif]-->';
           
        $html .= "</head>\n";
                   
        return $html; 
                    
        
    }
    
    
    private function get_params_string($params = array())
    {
        $str = "";
        
        
        foreach($params AS $p => $v)
        {
            $str .= ' '.$p.'="'.$v.'"';
        }
        
        return $str;
        
    }
    
    public function get_footer()
    {
        
        $html = "";
        
        
        foreach($this->css AS $css)
        {
            $cesta = $css[0];
            $typ = isset($css[1]) ? $css[1] : "";
            $min_width = isset($css[2]) ? $css[2] : 0;
            $params = $this->get_params_string($css[3]);
            
            if($typ == "")
                $html .= "<link rel='stylesheet' type='text/css' media='print,screen' href='".htmlspecialchars($cesta)."' />\n";
            elseif($typ == "mobile" || $typ == "tablet" || $typ == "pc")
                $html .= "<link rel='stylesheet' type='text/css' media='(min-width: ".$min_width."px)' href='".$cesta."' ".$params."/>\n";
            
                
        }
        
        
        foreach($this->js AS $js)
        {
            $url = $js[0];
            $params = $this->get_params_string($js[1]);
			$html .= "<script type='text/javascript' src='".htmlspecialchars($url)."' ".$params."></script>\n";
        }
           
        if($this->js_inline != '')
            $html .= "<script type='text/javascript'>
                ".$this->js_inline."
                </script>";
            
        $html .= PATICKA_KOD; 
            
        return $html;
            
    }
    
    
    
}


?>