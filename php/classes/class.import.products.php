<?php

class ImportProducts{


    //obsahuje nactena data
    private $data = array();

    //obsahuje info o importu
    private $log = array();

    //pocet vlozenych, aktualizovanych a smazanych produktu
    private $inserted = 0;
    private $updated = 0;
    private $deleted = 0;

    //pripravena data pro vlozeni do db 
    private $insert_data = array();
    private $update_data = array();
    private $delete_data = array();

    //stavajici produkty v databazi - jen id_serd
    private $products = array();

    //indikuje zda jde o debugovaci mod ci produkcni mod. V tomto modu nedochazi k operacim insert/update/delete v databazi
    private $debug_mode = false;

    public function __construct($debug = false)
    {
        global $db;

        //nastavuje debugovaci mod
        $this->debug_mode = $debug;

        //vycisteni logu
        $this->clear_log();

        //nacteni vsech produktu v databazi podle id_serd
        $data_produkty = $db->query("SELECT id_serd FROM ".TABLE_STRANKY." WHERE typ='produkt'");
        while($p = $db->getObject($data_produkty))
        {
            $this->products[$p->id_serd] = $p->id_serd;
        }

        
    }


    private function clear_log()
    {
        $this->log = array("ok" => array(), "error" => array());
    }

    private function float($val)
    {
        $val = (string) $val;
        $val = str_replace(",",".",$val);
        return floatval($val);
    }

    //nastaveni zdroje xml
    public function load($xmlSource)
    {
        
        //vycisteni logu
        $this->clear_log();

        //nacteni xml dokumentu
        $xml = new SimpleXMLElement($xmlSource);

        if(!$xml)
        {
            $this->log("Nelze načíst xml");
            return false;
        }

        foreach($xml->PRODUCT_DOCUMENTS->PRODUCT AS $row)
        {
            $data = array(
            "id_serd" => (int) $row->ID_CODE,
                "nazev" => (string) $row->NAME,
                "nazevProMenu" => (string) $row->NAME,
                "nadpis" => (string) $row->NAME,
                "idRodice" => (int) $row->ID_SUBCATEGORY,
                "vaha" => (string) $row->UNIT,
                "vaha_jednotka" => (string) $row->UNITCODE,
                "perex" => (string) $row->SHORT_DESCRIPTION,
                "perexBezHtml" => strip_tags((string) $row->SHORT_DESCRIPTION),
                "obsah" => (string) $row->LONG_DESCRIPTION,
                "obsahBezHtml" => strip_tags((string) $row->LONG_DESCRIPTION),
                "popis1" => (string) $row->LONG_CONTENT,
                "popis1BezHtml" => strip_tags((string) $row->LONG_CONTENT),
                "status" => (int) $row->ID_STATUS,
                "cena_s_dph" => $this->float($row->PRICEMO_BRUTTO),
                "cena_bez_dph" => $this->float($row->PRICEMO_NETTO),
                "cena_s_dph_vo" => $this->float($row->PRICEVO_BRUTTO),
                "cena_bez_dph_vo" => $this->float($row->PRICEVO_NETTO),
                "typ" => "produkt",
                "idDomeny" => 1,
                "idJazyka" => 1,
                "doMenu" => 0,
                "url" => get_url_nazev((int) $row->ID_CODE, ModifyUrl((string) $row->NAME)),
                "title" => (string) $row->NAME,
                "description" => "",
                "dph" => $this->float($row->TAX_RATE),
                "datum" => (string) $row->SORTING_DATE != '' ? date("Y-m-d H-i", strtotime((string) $row->SORTING_DATE)) : '',
                "mo" => intval( (int) $row->ID_BELONGS == 1 || (int) $row->ID_BELONGS == 2),
                "vo" => intval( (int) $row->ID_BELONGS == 1 || (int) $row->ID_BELONGS == 3)
            );

            //dd($data);

            //validuje data
            $ok = $this->validate($data);
            if(!$ok)
                continue;
            
            //pripravi data pro databazi - urci, ktera se budou mazat, vytvaret, aktualizovat
            $this->prepare($data);

        }


    }

    //validace hodnot z xml
    private function validate($insert)
    {
        global $db;

        if($insert['id_serd'] <= 0)
            $this->log("Chybná hodnota ID_CODE");
        
        if($insert['idRodice'] <= 0)
            $this->log("Produkt ID ".$insert['id_serd'].": Produkt nemá podkategorii - chybná hodnota ID_SUBCATEGORY");
        else
        {
            if($insert['idRodice'] != $db->get(TABLE_STRANKY, "idStranky", "typ='produkty' AND idStranky = ".$insert['idRodice']))
                $this->log("Produkt ID ".$insert['id_serd'].": Podkategorie s ID ".$insert['idRodice']." neexistuje -  chybná hodnota ID_SUBCATEGORY");
        }

        if($insert['nazev'] == '')
            $this->log("Produkt ID ".$insert['id_serd'].": Chybná hodnota NAME");

        if($insert['cena_s_dph'] <= 0)
            $this->log("Produkt ID ".$insert['id_serd'].": Chybná hodnota PRICEMO_BRUTTO");    

        if($insert['cena_bez_dph'] <= 0)
            $this->log("Produkt ID ".$insert['id_serd'].": Chybná hodnota PRICEMO_NETTO"); 

        if($insert['cena_s_dph_vo'] <= 0)
            $this->log("Produkt ID ".$insert['id_serd'].": Chybná hodnota PRICEVO_BRUTTO"); 

        if($insert['cena_bez_dph_vo'] <= 0)
            $this->log("Produkt ID ".$insert['id_serd'].": Chybná hodnota PRICEVO_NETTO"); 

        if($insert['dph'] <= 0)
            $this->log("Produkt ID ".$insert['id_serd'].": Chybná hodnota TAX_RATE"); 

        return count($this->log['error']) == 0;

    }

    private function prepare($data)
    {
        global $db;


        if(in_array($data['id_serd'], $this->products))
        {
            unset($data['url']);
            $this->update_data[] = $data;
            $this->updated++;
        }
        else 
        {
            $this->insert_data[] = $data;
            $this->inserted++;
            
        }

        unset($this->products[$data['id_serd']]);

    }


    private function log($text, $typ = "error")
    {
        $this->log[$typ][] = $text;
    }

    //ulozi, aktualizuje, maze produkty
    public function save()
    {

        global $db;

        //insert
        foreach($this->insert_data AS $insert)
        {
            $insert['jmenoVytvoril'] = "system";
            $insert['datumVytvoril'] = "now";

            if(!$this->debug_mode)
                $db->insert(TABLE_STRANKY, $insert);
        }

        //update
        foreach($this->update_data AS $insert)
        {
            
            $insert['jmenoAktualizoval'] = "system";
            $insert['datumAktualizoval'] = "now";

            if(!$this->debug_mode)
                $db->update(TABLE_STRANKY, $insert, "id_serd=".$insert['id_serd']);
        }

        //delete
        foreach($this->products AS $id)
        {
            $this->deleted++;

            if(!$this->debug_mode)
                $db->delete(TABLE_STRANKY, "id_serd=".$id);
            
        }

        //doplneni logu
        $this->log("Úspěšně založeno ".$this->inserted." produktů", "ok");
        $this->log("Úspěšně aktualizováno ".$this->updated." produktů", "ok");
        $this->log("Úspěšně smazáno ".$this->deleted." produktů", "ok");


    }


    public function get_log_list($str = false)
    {

        /*
        if(count($this->log['error']) == 0)
            return false;
        */


        if($str)
        {
            $msg = array();
            foreach($this->log['error'] AS $m)
            {
                $msg[] = "[error] ".$m;
            }

            foreach($this->log['ok'] AS $m)
            {
                $msg[] = "[ok] ".$m;
            }

            return implode("\n", $msg);
            
        }
            else
            return $this->log;


    }




}