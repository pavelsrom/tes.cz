<?php


/**
 * Simple Calendar
 *
 * @author Jesse G. Donat <donatj@gmail.com>
 * @link http://donatstudios.com
 * @license http://opensource.org/licenses/mit-license.php
 *
 */
class SimpleCalendar {

	/**
	 * Array of Week Day Names
	 *
	 * @var array
	 */
	public $wday_names = false;
    
    //nazvy mesicu
    //public $months_names = false;
    
	private $now;
	private $daily_html = array();
	private $offset = 0;

	/**
	 * Constructor - Calls the setDate function
	 *
	 * @see setDate
	 * @param null|string $date_string
	 */
	function __construct( $date_string = null ) {
		$this->setDate($date_string);
	}

	/**
	 * Sets the date for the calendar
	 *
	 * @param null|string $date_string Date string parsed by strtotime for the calendar date. If null set to current timestamp.
	 */
	public function setDate( $date_string = null ) {
		if( $date_string ) {
			$this->now = getdate(strtotime($date_string));
		} else {
			$this->now = getdate();
		}
	}

	/**
	 * Add a daily event to the calendar
	 *
	 * @param string      $html The raw HTML to place on the calendar for this event
	 * @param string      $start_date_string Date string for when the event starts
	 * @param null|string $end_date_string Date string for when the event ends. Defaults to start date
	 */
	public function addDailyHtml( $html, $start_date_string, $end_date_string = null ) {
		static $htmlCount = 0;
		$start_date = strtotime($start_date_string);
		if( $end_date_string ) {
			$end_date = strtotime($end_date_string);
		} else {
			$end_date = $start_date;
		}

		$working_date = $start_date;
		do {
			$tDate = getdate($working_date);
			$working_date += 86400;
			$this->daily_html[$tDate['year']][$tDate['mon']][$tDate['mday']][$htmlCount] = $html;
		} while( $working_date < $end_date + 1 );

		$htmlCount++;

	}

	/**
	 * Clear all daily events for the calendar
	 */
	public function clearDailyHtml() { $this->daily_html = array(); }

	/**
	 * Sets the first day of the week
	 *
	 * @param int|string $offset Day to start on, ex: "Monday" or 0-6 where 0 is Sunday
	 */
	public function setStartOfWeek( $offset ) {
		if( is_int($offset) ) {
			$this->offset = $offset % 7;
		} else {
			$this->offset = date('N', strtotime($offset)) % 7;
		}
	}

	/**
	 * Returns/Outputs the Calendar
	 *
	 * @param bool $echo Whether to echo resulting calendar
	 * @return string HTML of the Calendar
	 */
	public function show( $mesic, $rok, $url_kalendar_akci, $echo = true ) {
		if( $this->wday_names ) {
			$wdays = $this->wday_names;
		} else {
			$today = (86400 * (date("N")));
			$wdays = array();
			for( $i = 0; $i < 7; $i++ ) {
				$wdays[] = strftime('%a', time() - $today + ($i * 86400));
			}
		}

        

		$this->array_rotate($wdays, $this->offset);
		$wday    = date('N', mktime(0, 0, 1, $this->now['mon'], 1, $this->now['year'])) - $this->offset;
		$no_days = cal_days_in_month(CAL_GREGORIAN, $this->now['mon'], $this->now['year']);

        
        $mesic_prev = $mesic == 1 ? 12 : $mesic - 1;
        $mesic_next = $mesic == 12? 1 : $mesic + 1;
        
        $rok_prev = $mesic == 1 ? $rok - 1 : $rok;
        $rok_next = $mesic == 12? $rok + 1 : $rok; 
        
        $base_url = $_SERVER['REQUEST_URI'];
        $url_next = $url_prev = "";
        if(strstr($base_url,"?"))
        {
            $bu = explode("?",$base_url);
            $base_url = $bu[0];
            
            $url_next = $base_url."?mesic=".$mesic_next."&rok=".$rok_next;
            $url_prev = $base_url."?mesic=".$mesic_prev."&rok=".$rok_prev;
        }
        else
        {
            $url_next = $base_url."?mesic=".$mesic_next."&rok=".$rok_next;
            $url_prev = $base_url."?mesic=".$mesic_prev."&rok=".$rok_prev;
        }
        
        $url_prev .= "#cal";
        $url_next .= "#cal";
        
        
        $out = "";
        $out .= '<div class="kalendar-box">';
        $out .= '<div class="kalendar-sipky">
            <a href="'.$url_prev.'" class="prev">&lsaquo;</a>
            <span>'.GetMonthName($mesic).' '.$rok.'</span>
            <a href="'.$url_next.'" class="next">&rsaquo;</a>
            </div>';
            
		$out .= '<table class="tKalendar"><thead>';
        
        $out .= '<tr>';

		for( $i = 0; $i < 7; $i++ ) {
			$out .= '<th>' . $wdays[$i] . '</th>';
		}

		$out .= "</tr></thead>\n<tbody>\n<tr>";

		$wday = ($wday + 7) % 7;

		if( $wday == 7 ) {
			$wday = 0;
		} else {
			$out .= str_repeat('<td class="nic">&nbsp;</td>', $wday);
		}

		$count = $wday + 1;
        $legenda = array();
		for( $i = 1; $i <= $no_days; $i++ ) {
			

            /*
			$datetime = mktime(0, 0, 1, $this->now['mon'], $i, $this->now['year']);
			$out .= '<time datetime="' . date('Y-m-d', $datetime) . '">' . $i . '</time>';
            */
            
            $popisek = array();
            
			$dHtml_arr = false;
            $pocet_akci = 0;
			if( isset($this->daily_html[$this->now['year']][$this->now['mon']][$i]) ) {
				$dHtml_arr = $this->daily_html[$this->now['year']][$this->now['mon']][$i];
            
                //spocita kolik je v danem dni akci a odecte akci s nazvem DNES, protoze to neni akce
                if(is_array($dHtml_arr))
                {
                    foreach($dHtml_arr AS $e)
                    {
                        //pokud by to nebylo pole, pak by se jednalo o pseudo akci DNES
                        if(is_array($e))
                            $pocet_akci++;
                    }
                }
                
			}            
            
            
            
            //print_r($dHtml_arr);
            
            $class = "";
            
            $je_vice_akci = $pocet_akci > 1;
            
            if($i == date('j')/*$this->now['mday']*/ && $this->now['mon'] == date('n') && $this->now['year'] == date('Y'))
            {
                $class .= 'dnes';
                $legenda[-1] = array("barva" => "green", "nazev" => TDNES, "url" => "");
            }
            
             if($je_vice_akci)
             {
               $class .= " vice-akci";
               $legenda[0] = array("barva" => "yellow", "nazev" => TVICE_AKCI_V_JEDEN_DEN, "url" => "");
             }
             
             
            $style = "";  
            
            
            $je_obsah = false;                      
			if( is_array($dHtml_arr) ) {
			     
				foreach( $dHtml_arr as $e ) {
				    if(is_array($e))
                    {
					   $popisek[] = $e['format_datum']." - ".$e['nazev'];
                       $je_obsah = $e['je_obsah'];

                       
                       if(!$je_vice_akci)
                       {
                            $legenda[$e['id_kategorie']] = array("barva" => "#".$e['barva'], "nazev" => $e['kategorie'], "url" => $e['kategorie_url']);
                            $style = "style='background: #".$e['barva']."'";
                       }
                       
                        
                       
                       
                    }
                       else
                       $popisek[] = $e;
				}
			}
            
            
            
            
            $class = $class != "" ? "class='".$class."'" : "";
            
            $out .= '<td ' . $class . '>';
            
            if(count($popisek) > 0)
            {
                if($je_obsah)
                {
                    $d = $i.".".$this->now['mon'].".".$this->now['year'];
                    $url_akce = $je_vice_akci ? $url_kalendar_akci."?od=".$d."&do=".$d."#vypis_akci" : $e['odkaz'];
                    $out .= '<a href="'.$url_akce.'" title="'.implode("\n",$popisek).'" '.$style.'>' . $i . '</a>';
                }
                    else
                    $out .= '<span title="'.implode("\n",$popisek).'" '.$style.'>' . $i . '</span>';
            }
            else
                $out .= '<span>' . $i . '</span>';
            

			$out .= "</td>";

			if( $count > 6 ) {
				$out .= "</tr>\n" . ($i != $count ? '<tr>' : '');
				$count = 0;
			}
			$count++;
		}
		$out .= ($count != 1 ? str_repeat('<td class="nic">&nbsp;</td>', 8 - $count) : '') . "</tr>\n</tbody></table></div>\n";
        
        //vypis legendy
        if(count($legenda) > 0)
        {
            ksort($legenda);
            
            //$out .= '<strong class="fr">'.TLEGENDA.'</strong>';
            //$out .= '<div class="cleaner"></div>';
            $out .= '<table class="tLegenda fr">';
            foreach($legenda AS $l)
            {
                
                if($l['url'] != '')
                    $a = '<a href="'.$l['url'].'">'.$l['nazev'].'</a>';
                    else
                    $a = '<span>'.$l['nazev'].'</span>';
                
                $out .= '<tr><td>'.$a.'</td><th><em style="background: '.$l['barva'].'"></em></th></tr>';
            } 
            $out .= '</table>';
            $out .= '<div class="cleaner"></div>';
        }
        
        
        
		if( $echo ) {
			echo $out;
		}

		return $out;
	}

	private function array_rotate( &$data, $steps ) {
		$count = count($data);
		if( $steps < 0 ) {
			$steps = $count + $steps;
		}
		$steps = $steps % $count;
		for( $i = 0; $i < $steps; $i++ ) {
			array_push($data, array_shift($data));
		}
	}

}
