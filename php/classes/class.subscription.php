<?php

/**
 * @author Pavel �rom
 * @copyright 2018
 */


class C_Subscription{
    
    //typ predplatneho, 0=mesicni, 1=rocni
    private $type = false;
    
    //cena predplatneho
    private $price = false;
    
    //id prihlaseneho/nove registrovaneho uzivatele
    private $user_id = 0;
    
    public function __construct($user_id)
    {

        global $db;
    
        $this->user_id = $user_id;
        
        //zjisteni typu predplatneho daneho uzivatele
        if($this->user_id > 0)
        {
            
            $predplatne = $db->get(TABLE_UZIVATELE,array("predplatne_typ","predplatne_cena"),"idUzivatele=".$this->user_id);
            $this->type = $predplatne->predplatne_typ;
            $this->price = $predplatne->predplatne_cena;

            
        }
        
    }
    
    //nastavi typ predplatneho a cenu
    public function set($predplatne_typ, $predplatne_cena)
    {
        $this->type = $predplatne_typ;
        $this->price = $predplatne_cena;
    }
    
    //vraci informace o dalsim predplatnem pro daneho uzivatele
    public function get_next($id_polozky_obj = 0)
    {
        global $db;

        //zjisteni zda k aktualnimu datu uz existuje predplatne
        if($id_polozky_obj == 0)
            $data = $db->query("SELECT od, do, datum_dalsi_platby 
                FROM ".TABLE_ESHOP_OBJEDNAVKY_POLOZKY." AS op
                LEFT JOIN ".TABLE_ESHOP_OBJEDNAVKY." AS o ON op.id_objednavka = o.id 
                WHERE op.id_produkt = 0
                    AND o.id_uzivatel = ".$this->user_id." 
                    AND op.od <= CURDATE() 
                    AND op.do >= CURDATE()
                    AND o.id_stav = 2
                ORDER BY o.id DESC
                LIMIT 1
                ");
            else
            //vyuziva skript pro cron - strhavani automatickych plateb
            $data = $db->query("SELECT od, do, datum_dalsi_platby 
                FROM ".TABLE_ESHOP_OBJEDNAVKY_POLOZKY."
                WHERE id = ".$id_polozky_obj."
                LIMIT 1
                ");
        
        //pokud k dnesnimu datu existuje aktivni predplatne, pak nastavi predplatne od nasledujiciho mesice
        if($db->numRows($data) > 0)
        {
            
            $p = $db->getObject($data);
            
            
            $od_time = strtotime("first day of next month", strtotime($p->do));
            
            
            if($this->type == 0)
                //mesicni predplatne
                $do_time = strtotime("last day of next month", strtotime($p->do));
                else
                //rocni predplatne
                $do_time = strtotime("last day of next month + 11 month", strtotime($p->do));
                
            $datum_dalsi_platby = $this->get_next_payment_date($p->datum_dalsi_platby);
            
            
        }
        else
        {
            $od_time = strtotime("first day of this month");
            
            
            if($this->type == 0)
                //mesicni predplatne
                $do_time = strtotime("last day of this month");
                else
                //rocni predplatne
                $do_time = strtotime("last day of this month + 11 month");
                
            $datum_dalsi_platby = $this->get_next_payment_date( date("Y-m-d"));
        }
        
        $od = date("Y-m-d", $od_time);    
        $do = date("Y-m-d", $do_time); 
        
        $result = array(
            "od" => $od,
            "do" => $do,
            "text_od" => date("m/Y", $od_time),
            "text_do" => date("m/Y", $do_time),
            "datum_dalsi_platby" => $datum_dalsi_platby
            );
        
        return $result;
        
        
        
    }
    
    
    //vraci datum nasledujici platby, na zaklade data posledni platby
    public function get_next_payment_date($last_payment_date)
    {
        $date = $last_payment_date;//date($last_payment_date);
        $currentMonth = date("m",strtotime($date));
                
        $jednotka = $this->type == 0 ? "month" : "year"; 
        
        //pro mesicni predplatne
        if($this->type == 0)    
        {  
            $nextMonth = date("m",strtotime($date." + 1 month"));

            //pokud je napr 31. ledna pricten jeden mesic, pak se dostaneme na brezen a to nechceme, protoze nesmime vynechat zadny mesic, tak musime osetrit unor        
            if($currentMonth==$nextMonth-1)
            {
                $nextDate = date('Y-m-d',strtotime($date." +1 month"));
            }
            else
            {
                $nextDate = date('Y-m-d', strtotime("last day of next ".$jednotka,strtotime($date)));
            }
        }
        //pro rocni predplatne
        else
        {

            $nextMonth = date("m",strtotime($date." + 1 year"));
            //echo $currentMonth."-".$nextMonth;

            //pokud je napr 31. ledna pricten jeden mesic, pak se dostaneme na brezen a to nechceme, protoze nesmime vynechat zadny mesic, tak musime osetrit unor        
            if($currentMonth==$nextMonth)
            {
                //vzdy k prvnimu dni v mesici se bude provadet platba, protoze se muze stat, ze si nekdo objedna rocni predplatne 15. ledna 2018, a tak bude mit predplatne do prosince 2018 a pak by nebylo vhodne, aby cekal zakaznik na knihu nez probehne platba 15. ledna 2019!
                $nextDate = date('Y-m-01',strtotime($date." +1 year"));
            }
            else
            {
                $nextDate = date('Y-m-d', strtotime("first day of next month",strtotime($date)));
            }
            
            
        }
        
        return $nextDate;
    }
    
    //vraci text pro oznaceni polozky v kosiku
    public function get_text_next_subscription()
    {
    
        
        $next = $this->get_next();
        
        if($this->type == 0)
            $text = $next["text_od"];
            else
            $text = $next["text_od"]." - ".$next["text_do"]; 
        
        $text = TPREDPLATNE." (".$text.")";
        
        return $text;
    }
    
    public function get_price()
    {
        return $this->price;
        
        
    }
    
    public function get_type()
    {
        return $this->type;
    }
    
    
    
    
}

?>