<?php

class C_SystemMessage{
		
	private $ERROR;
	private $OK;
	
	public $ERROR_EXISTS;
	public $OK_EXISTS;
	
	public $LOG_FILE;
	
	private $ERROR_MESSAGE_SESSION;
	private $OK_MESSAGE_SESSION;
	private $WARNING_MESSAGE_SESSION;
	
    private $messages = array("error" => array(), "warning" => array(), "ok" => array());
    
	public function C_SystemMessage($sessionPrefix=""){
		
        $sessionPrefix = $sessionPrefix == "" ? "admin" : "";
        
		$this->ERROR_MESSAGE_SESSION = $sessionPrefix."_error_msg";
		$this->OK_MESSAGE_SESSION = $sessionPrefix."_ok_msg";
		$this->WARNING_MESSAGE_SESSION = $sessionPrefix."_warning_msg";
		
		if(!isset($_SESSION[$this->OK_MESSAGE_SESSION])) {
			$_SESSION[$this->OK_MESSAGE_SESSION]='';
			}
		if(!isset($_SESSION[$this->ERROR_MESSAGE_SESSION])) {
			$_SESSION[$this->ERROR_MESSAGE_SESSION]='';
			}
		if(!isset($_SESSION[$this->WARNING_MESSAGE_SESSION])) {
			$_SESSION[$this->WARNING_MESSAGE_SESSION]='';
			}
		if($_SESSION[$this->OK_MESSAGE_SESSION]=='') $this->OK_EXISTS = false; else $this->OK_EXISTS = true;
		if($_SESSION[$this->ERROR_MESSAGE_SESSION]=='') $this->ERROR_EXISTS = false; else $this->ERROR_EXISTS = true;
		if($_SESSION[$this->WARNING_MESSAGE_SESSION]=='') $this->WARNING_EXISTS = false; else $this->WARNING_EXISTS = true;
		
		}


		
	public function showErrorMessage(){
		if(!$this->ERROR_EXISTS) return;
		echo "<div class='error w45'><ul>";
		echo $_SESSION[$this->ERROR_MESSAGE_SESSION];
		echo "</ul></div>";
		$this->ResetError();
		
	}
	
	public function showWarningMessage(){
		if(!$this->WARNING_EXISTS) return;
		echo "<div class='warning w45'><ul>";
		echo $_SESSION[$this->WARNING_MESSAGE_SESSION];
		echo "</ul></div>";
		$this->ResetWarning();
		
	}
	

	public function showOKMessage(){
		if(!$this->OK_EXISTS) return;
		echo "<div class='ok w45'><ul>";
		echo $_SESSION[$this->OK_MESSAGE_SESSION];
		echo "</ul></div>";	
		$this->ResetOk();
		
	}

	
	public function showMessages(){
		$this->ShowErrorMessage();
		$this->ShowWarningMessage();
		$this->ShowOKMessage();
	}
    
    
    

    
	

}
?>