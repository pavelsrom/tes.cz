<?php

if (!function_exists('curl_init')) {
  throw new Exception('Facebook needs the CURL PHP extension.');
}
if (!function_exists('json_decode')) {
  throw new Exception('Facebook needs the JSON PHP extension.');
}

/**
 * Thrown when an API call returns an exception.
 *
 * @author Naitik Shah <naitik@facebook.com>
 */
class FacebookApiException extends Exception
{
  /**
   * The result from the API server that represents the exception information.
   */
  protected $result;

  /**
   * Make a new API Exception with the given result.
   *
   * @param Array $result the result from the API server
   */
  public function __construct($result) {
    $this->result = $result;

    $code = isset($result['error_code']) ? $result['error_code'] : 0;

    if (isset($result['error_description'])) {
      // OAuth 2.0 Draft 10 style
      $msg = $result['error_description'];
    } else if (isset($result['error']) && is_array($result['error'])) {
      // OAuth 2.0 Draft 00 style
      $msg = $result['error']['message'];
    } else if (isset($result['error_msg'])) {
      // Rest server style
      $msg = $result['error_msg'];
    } else {
      $msg = 'Unknown Error. Check getResult()';
    }

    parent::__construct($msg, $code);
  }

  /**
   * Return the associated result object returned by the API server.
   *
   * @returns Array the result from the API server
   */
  public function getResult() {
    return $this->result;
  }

  /**
   * Returns the associated type for the error. This will default to
   * 'Exception' when a type is not available.
   *
   * @return String
   */
  public function getType() {
    if (isset($this->result['error'])) {
      $error = $this->result['error'];
      if (is_string($error)) {
        // OAuth 2.0 Draft 10 style
        return $error;
      } else if (is_array($error)) {
        // OAuth 2.0 Draft 00 style
        if (isset($error['type'])) {
          return $error['type'];
        }
      }
    }
    return 'Exception';
  }

  /**
   * To make debugging easier.
   *
   * @returns String the string representation of the error
   */
  public function __toString() {
    $str = $this->getType() . ': ';
    if ($this->code != 0) {
      $str .= $this->code . ': ';
    }
    return $str . $this->message;
  }
}

/**
 * Provides access to the Facebook Platform.
 *
 * @author Naitik Shah <naitik@facebook.com>
 */
class Facebook
{
  /**
   * Version.
   */
  const VERSION = '2.1.2';

  /**
   * Default options for curl.
   */
  public static $CURL_OPTS = array(
    CURLOPT_CONNECTTIMEOUT => 10,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_TIMEOUT        => 60,
    CURLOPT_SSL_VERIFYPEER => false, 
    CURLOPT_SSL_VERIFYHOST => 2, 
    CURLOPT_USERAGENT      => 'facebook-php-2.0',
  );

  /**
   * List of query parameters that get automatically dropped when rebuilding
   * the current URL.
   */
  protected static $DROP_QUERY_PARAMS = array(
    'session',
    'signed_request',
  );

  /**
   * Maps aliases to Facebook domains.
   */
  public static $DOMAIN_MAP = array(
    'api'      => 'https://api.facebook.com/',
    'api_read' => 'https://api-read.facebook.com/',
    'graph'    => 'https://graph.facebook.com/',
    'www'      => 'https://www.facebook.com/',
  );

  /**
   * The Application ID.
   */
  protected $appId;

  /**
   * The Application API Secret.
   */
  protected $apiSecret;

  /**
   * The active user session, if one is available.
   */
  protected $session;

  /**
   * The data from the signed_request token.
   */
  protected $signedRequest;

  /**
   * Indicates that we already loaded the session as best as we could.
   */
  protected $sessionLoaded = false;

  /**
   * Indicates if Cookie support should be enabled.
   */
  protected $cookieSupport = false;

  /**
   * Base domain for the Cookie.
   */
  protected $baseDomain = '';

  /**
   * Indicates if the CURL based @ syntax for file uploads is enabled.
   */
  protected $fileUploadSupport = false;

  /**
   * Initialize a Facebook Application.
   *
   * The configuration:
   * - appId: the application ID
   * - secret: the application secret
   * - cookie: (optional) boolean true to enable cookie support
   * - domain: (optional) domain for the cookie
   * - fileUpload: (optional) boolean indicating if file uploads are enabled
   *
   * @param Array $config the application configuration
   */
  public function __construct($config) {
    $this->setAppId($config['appId']);
    $this->setApiSecret($config['secret']);
    if (isset($config['cookie'])) {
      $this->setCookieSupport($config['cookie']);
    }
    if (isset($config['domain'])) {
      $this->setBaseDomain($config['domain']);
    }
    if (isset($config['fileUpload'])) {
      $this->setFileUploadSupport($config['fileUpload']);
    }
  }

  /**
   * Set the Application ID.
   *
   * @param String $appId the Application ID
   */
  public function setAppId($appId) {
    $this->appId = $appId;
    return $this;
  }

  /**
   * Get the Application ID.
   *
   * @return String the Application ID
   */
  public function getAppId() {
    return $this->appId;
  }

  /**
   * Set the API Secret.
   *
   * @param String $appId the API Secret
   */
  public function setApiSecret($apiSecret) {
    $this->apiSecret = $apiSecret;
    return $this;
  }

  /**
   * Get the API Secret.
   *
   * @return String the API Secret
   */
  public function getApiSecret() {
    return $this->apiSecret;
  }

  /**
   * Set the Cookie Support status.
   *
   * @param Boolean $cookieSupport the Cookie Support status
   */
  public function setCookieSupport($cookieSupport) {
    $this->cookieSupport = $cookieSupport;
    return $this;
  }

  /**
   * Get the Cookie Support status.
   *
   * @return Boolean the Cookie Support status
   */
  public function useCookieSupport() {
    return $this->cookieSupport;
  }

  /**
   * Set the base domain for the Cookie.
   *
   * @param String $domain the base domain
   */
  public function setBaseDomain($domain) {
    $this->baseDomain = $domain;
    return $this;
  }

  /**
   * Get the base domain for the Cookie.
   *
   * @return String the base domain
   */
  public function getBaseDomain() {
    return $this->baseDomain;
  }

  /**
   * Set the file upload support status.
   *
   * @param String $domain the base domain
   */
  public function setFileUploadSupport($fileUploadSupport) {
    $this->fileUploadSupport = $fileUploadSupport;
    return $this;
  }

  /**
   * Get the file upload support status.
   *
   * @return String the base domain
   */
  public function useFileUploadSupport() {
    return $this->fileUploadSupport;
  }

  /**
   * Get the data from a signed_request token
   *
   * @return String the base domain
   */
  public function getSignedRequest() {
    if (!$this->signedRequest) {
      if (isset($_REQUEST['signed_request'])) {
        $this->signedRequest = $this->parseSignedRequest(
          $_REQUEST['signed_request']);
      }
    }
    return $this->signedRequest;
  }

  /**
   * Set the Session.
   *
   * @param Array $session the session
   * @param Boolean $write_cookie indicate if a cookie should be written. this
   * value is ignored if cookie support has been disabled.
   */
  public function setSession($session=null, $write_cookie=true) {
    $session = $this->validateSessionObject($session);
    $this->sessionLoaded = true;
    $this->session = $session;
    if ($write_cookie) {
      $this->setCookieFromSession($session);
    }
    return $this;
  }

  /**
   * Get the session object. This will automatically look for a signed session
   * sent via the signed_request, Cookie or Query Parameters if needed.
   *
   * @return Array the session
   */
  public function getSession() {
    if (!$this->sessionLoaded) {
      $session = null;
      $write_cookie = true;

      // try loading session from signed_request in $_REQUEST
      $signedRequest = $this->getSignedRequest();
      if ($signedRequest) {
        // sig is good, use the signedRequest
        $session = $this->createSessionFromSignedRequest($signedRequest);
      }

      // try loading session from $_REQUEST
      if (!$session && isset($_REQUEST['session'])) {
        $session = json_decode(
          get_magic_quotes_gpc()
            ? stripslashes($_REQUEST['session'])
            : $_REQUEST['session'],
          true
        );
        $session = $this->validateSessionObject($session);
      }

      // try loading session from cookie if necessary
      if (!$session && $this->useCookieSupport()) {
        $cookieName = $this->getSessionCookieName();
        if (isset($_COOKIE[$cookieName])) {
          $session = array();
          parse_str(trim(
            get_magic_quotes_gpc()
              ? stripslashes($_COOKIE[$cookieName])
              : $_COOKIE[$cookieName],
            '"'
          ), $session);
          $session = $this->validateSessionObject($session);
          // write only if we need to delete a invalid session cookie
          $write_cookie = empty($session);
        }
      }

      $this->setSession($session, $write_cookie);
    }

    return $this->session;
  }

  /**
   * Get the UID from the session.
   *
   * @return String the UID if available
   */
  public function getUser() {
    $session = $this->getSession();
    return $session ? $session['uid'] : null;
  }

  /**
   * Gets a OAuth access token.
   *
   * @return String the access token
   */
  public function getAccessToken() {
    $session = $this->getSession();
    // either user session signed, or app signed
    if ($session) {
      return $session['access_token'];
    } else {
      return $this->getAppId() .'|'. $this->getApiSecret();
    }
  }

  /**
   * Get a Login URL for use with redirects. By default, full page redirect is
   * assumed. If you are using the generated URL with a window.open() call in
   * JavaScript, you can pass in display=popup as part of the $params.
   *
   * The parameters:
   * - next: the url to go to after a successful login
   * - cancel_url: the url to go to after the user cancels
   * - req_perms: comma separated list of requested extended perms
   * - display: can be "page" (default, full page) or "popup"
   *
   * @param Array $params provide custom parameters
   * @return String the URL for the login flow
   */
  public function getLoginUrl($params=array()) {
    $currentUrl = $this->getCurrentUrl();
    return $this->getUrl(
      'www',
      'class.login.php',
      array_merge(array(
        'api_key'         => $this->getAppId(),
        'cancel_url'      => $currentUrl,
        'display'         => 'page',
        'fbconnect'       => 1,
        'next'            => $currentUrl,
        'return_session'  => 1,
        'session_version' => 3,
        'v'               => '1.0',
      ), $params)
    );
  }

  /**
   * Get a Logout URL suitable for use with redirects.
   *
   * The parameters:
   * - next: the url to go to after a successful logout
   *
   * @param Array $params provide custom parameters
   * @return String the URL for the logout flow
   */
  public function getLogoutUrl($params=array()) {
    return $this->getUrl(
      'www',
      'logout.php',
      array_merge(array(
        'next'         => $this->getCurrentUrl(),
        'access_token' => $this->getAccessToken(),
      ), $params)
    );
  }

  /**
   * Get a login status URL to fetch the status from facebook.
   *
   * The parameters:
   * - ok_session: the URL to go to if a session is found
   * - no_session: the URL to go to if the user is not connected
   * - no_user: the URL to go to if the user is not signed into facebook
   *
   * @param Array $params provide custom parameters
   * @return String the URL for the logout flow
   */
  public function getLoginStatusUrl($params=array()) {
    return $this->getUrl(
      'www',
      'extern/login_status.php',
      array_merge(array(
        'api_key'         => $this->getAppId(),
        'no_session'      => $this->getCurrentUrl(),
        'no_user'         => $this->getCurrentUrl(),
        'ok_session'      => $this->getCurrentUrl(),
        'session_version' => 3,
      ), $params)
    );
  }

  /**
   * Make an API call.
   *
   * @param Array $params the API call parameters
   * @return the decoded response
   */
  public function api(/* polymorphic */) {
    $args = func_get_args();
    if (is_array($args[0])) {
      return $this->_restserver($args[0]);
    } else {
      return call_user_func_array(array($this, '_graph'), $args);
    }
  }

  /**
   * Invoke the old restserver.php endpoint.
   *
   * @param Array $params method call object
   * @return the decoded response object
   * @throws FacebookApiException
   */
  protected function _restserver($params) {
    // generic application level parameters
    $params['api_key'] = $this->getAppId();
    $params['format'] = 'json-strings';

    $result = json_decode($this->_oauthRequest(
      $this->getApiUrl($params['method']),
      $params
    ), true);

    // results are returned, errors are thrown
    if (is_array($result) && isset($result['error_code'])) {
      throw new FacebookApiException($result);
    }
    return $result;
  }

  /**
   * Invoke the Graph API.
   *
   * @param String $path the path (required)
   * @param String $method the http method (default 'GET')
   * @param Array $params the query/post data
   * @return the decoded response object
   * @throws FacebookApiException
   */
  protected function _graph($path, $method='GET', $params=array()) {
    if (is_array($method) && empty($params)) {
      $params = $method;
      $method = 'GET';
    }
    $params['method'] = $method; // method override as we always do a POST

    $result = json_decode($this->_oauthRequest(
      $this->getUrl('graph', $path),
      $params
    ), true);

    // results are returned, errors are thrown
    if (is_array($result) && isset($result['error'])) {
      $e = new FacebookApiException($result);
      switch ($e->getType()) {
        // OAuth 2.0 Draft 00 style
        case 'OAuthException':
        // OAuth 2.0 Draft 10 style
        case 'invalid_token':
          $this->setSession(null);
      }
      throw $e;
    }
    return $result;
  }

  /**
   * Make a OAuth Request
   *
   * @param String $path the path (required)
   * @param Array $params the query/post data
   * @return the decoded response object
   * @throws FacebookApiException
   */
  protected function _oauthRequest($url, $params) {
    if (!isset($params['access_token'])) {
      $params['access_token'] = $this->getAccessToken();
    }

    // json_encode all params values that are not strings
    foreach ($params as $key => $value) {
      if (!is_string($value)) {
        $params[$key] = json_encode($value);
      }
    }
    return $this->makeRequest($url, $params);
  }

  /**
   * Makes an HTTP request. This method can be overriden by subclasses if
   * developers want to do fancier things or use something other than curl to
   * make the request.
   *
   * @param String $url the URL to make the request to
   * @param Array $params the parameters to use for the POST body
   * @param CurlHandler $ch optional initialized curl handle
   * @return String the response text
   */
  protected function makeRequest($url, $params, $ch=null) {
    if (!$ch) {
      $ch = curl_init();
    }

    $opts = self::$CURL_OPTS;
    if ($this->useFileUploadSupport()) {
      $opts[CURLOPT_POSTFIELDS] = $params;
    } else {
      $opts[CURLOPT_POSTFIELDS] = http_build_query($params, null, '&');
    }
    $opts[CURLOPT_URL] = $url;

    // disable the 'Expect: 100-continue' behaviour. This causes CURL to wait
    // for 2 seconds if the server does not support this header.
    if (isset($opts[CURLOPT_HTTPHEADER])) {
      $existing_headers = $opts[CURLOPT_HTTPHEADER];
      $existing_headers[] = 'Expect:';
      $opts[CURLOPT_HTTPHEADER] = $existing_headers;
    } else {
      $opts[CURLOPT_HTTPHEADER] = array('Expect:');
    }

    curl_setopt_array($ch, $opts);
    $result = curl_exec($ch);
    if ($result === false) {
      $e = new FacebookApiException(array(
        'error_code' => curl_errno($ch),
        'error'      => array(
          'message' => curl_error($ch),
          'type'    => 'CurlException',
        ),
      ));
      curl_close($ch);
      throw $e;
    }
    curl_close($ch);
    return $result;
  }

  /**
   * The name of the Cookie that contains the session.
   *
   * @return String the cookie name
   */
  protected function getSessionCookieName() {
    return 'fbs_' . $this->getAppId();
  }

  /**
   * Set a JS Cookie based on the _passed in_ session. It does not use the
   * currently stored session -- you need to explicitly pass it in.
   *
   * @param Array $session the session to use for setting the cookie
   */
  protected function setCookieFromSession($session=null) {
    if (!$this->useCookieSupport()) {
      return;
    }

    $cookieName = $this->getSessionCookieName();
    $value = 'deleted';
    $expires = time() - 3600;
    $domain = $this->getBaseDomain();
    if ($session) {
      $value = '"' . http_build_query($session, null, '&') . '"';
      if (isset($session['base_domain'])) {
        $domain = $session['base_domain'];
      }
      $expires = $session['expires'];
    }

    // prepend dot if a domain is found
    if ($domain) {
      $domain = '.' . $domain;
    }

    // if an existing cookie is not set, we dont need to delete it
    if ($value == 'deleted' && empty($_COOKIE[$cookieName])) {
      return;
    }

    if (headers_sent()) {
      self::errorLog('Could not set cookie. Headers already sent.');

    // ignore for code coverage as we will never be able to setcookie in a CLI
    // environment
    // @codeCoverageIgnoreStart
    } else {
      setcookie($cookieName, $value, $expires, '/', $domain);
    }
    // @codeCoverageIgnoreEnd
  }

  /**
   * Validates a session_version=3 style session object.
   *
   * @param Array $session the session object
   * @return Array the session object if it validates, null otherwise
   */
  protected function validateSessionObject($session) {
    // make sure some essential fields exist
    if (is_array($session) &&
        isset($session['uid']) &&
        isset($session['access_token']) &&
        isset($session['sig'])) {
      // validate the signature
      $session_without_sig = $session;
      unset($session_without_sig['sig']);
      $expected_sig = self::generateSignature(
        $session_without_sig,
        $this->getApiSecret()
      );
      if ($session['sig'] != $expected_sig) {
        self::errorLog('Got invalid session signature in cookie.');
        $session = null;
      }
      // check expiry time
    } else {
      $session = null;
    }
    return $session;
  }

  /**
   * Returns something that looks like our JS session object from the
   * signed token's data
   *
   * TODO: Nuke this once the login flow uses OAuth2
   *
   * @param Array the output of getSignedRequest
   * @return Array Something that will work as a session
   */
  protected function createSessionFromSignedRequest($data) {
    if (!isset($data['oauth_token'])) {
      return null;
    }

    $session = array(
      'uid'          => $data['user_id'],
      'access_token' => $data['oauth_token'],
      'expires'      => $data['expires'],
    );

    // put a real sig, so that validateSignature works
    $session['sig'] = self::generateSignature(
      $session,
      $this->getApiSecret()
    );

    return $session;
  }

  /**
   * Parses a signed_request and validates the signature.
   * Then saves it in $this->signed_data
   *
   * @param String A signed token
   * @param Boolean Should we remove the parts of the payload that
   *                are used by the algorithm?
   * @return Array the payload inside it or null if the sig is wrong
   */
  protected function parseSignedRequest($signed_request) {
    list($encoded_sig, $payload) = explode('.', $signed_request, 2);

    // decode the data
    $sig = self::base64UrlDecode($encoded_sig);
    $data = json_decode(self::base64UrlDecode($payload), true);

    if (strtoupper($data['algorithm']) !== 'HMAC-SHA256') {
      self::errorLog('Unknown algorithm. Expected HMAC-SHA256');
      return null;
    }

    // check sig
    $expected_sig = hash_hmac('sha256', $payload,
                              $this->getApiSecret(), $raw = true);
    if ($sig !== $expected_sig) {
      self::errorLog('Bad Signed JSON signature!');
      return null;
    }

    return $data;
  }

  /**
   * Build the URL for api given parameters.
   *
   * @param $method String the method name.
   * @return String the URL for the given parameters
   */
  protected function getApiUrl($method) {
    static $READ_ONLY_CALLS =
      array('admin.getallocation' => 1,
            'admin.getappproperties' => 1,
            'admin.getbannedusers' => 1,
            'admin.getlivestreamvialink' => 1,
            'admin.getmetrics' => 1,
            'admin.getrestrictioninfo' => 1,
            'application.getpublicinfo' => 1,
            'auth.getapppublickey' => 1,
            'auth.getsession' => 1,
            'auth.getsignedpublicsessiondata' => 1,
            'comments.get' => 1,
            'connect.getunconnectedfriendscount' => 1,
            'dashboard.getactivity' => 1,
            'dashboard.getcount' => 1,
            'dashboard.getglobalnews' => 1,
            'dashboard.getnews' => 1,
            'dashboard.multigetcount' => 1,
            'dashboard.multigetnews' => 1,
            'data.getcookies' => 1,
            'events.get' => 1,
            'events.getmembers' => 1,
            'fbml.getcustomtags' => 1,
            'feed.getappfriendstories' => 1,
            'feed.getregisteredtemplatebundlebyid' => 1,
            'feed.getregisteredtemplatebundles' => 1,
            'fql.multiquery' => 1,
            'fql.query' => 1,
            'friends.arefriends' => 1,
            'friends.get' => 1,
            'friends.getappusers' => 1,
            'friends.getlists' => 1,
            'friends.getmutualfriends' => 1,
            'gifts.get' => 1,
            'groups.get' => 1,
            'groups.getmembers' => 1,
            'intl.gettranslations' => 1,
            'links.get' => 1,
            'notes.get' => 1,
            'notifications.get' => 1,
            'pages.getinfo' => 1,
            'pages.isadmin' => 1,
            'pages.isappadded' => 1,
            'pages.isfan' => 1,
            'permissions.checkavailableapiaccess' => 1,
            'permissions.checkgrantedapiaccess' => 1,
            'photos.get' => 1,
            'photos.getalbums' => 1,
            'photos.gettags' => 1,
            'profile.getinfo' => 1,
            'profile.getinfooptions' => 1,
            'stream.get' => 1,
            'stream.getcomments' => 1,
            'stream.getfilters' => 1,
            'users.getinfo' => 1,
            'users.getloggedinuser' => 1,
            'users.getstandardinfo' => 1,
            'users.hasapppermission' => 1,
            'users.isappuser' => 1,
            'users.isverified' => 1,
            'video.getuploadlimits' => 1);
    $name = 'api';
    if (isset($READ_ONLY_CALLS[strtolower($method)])) {
      $name = 'api_read';
    }
    return self::getUrl($name, 'restserver.php');
  }

  /**
   * Build the URL for given domain alias, path and parameters.
   *
   * @param $name String the name of the domain
   * @param $path String optional path (without a leading slash)
   * @param $params Array optional query parameters
   * @return String the URL for the given parameters
   */
  protected function getUrl($name, $path='', $params=array()) {
    $url = self::$DOMAIN_MAP[$name];
    if ($path) {
      if ($path[0] === '/') {
        $path = substr($path, 1);
      }
      $url .= $path;
    }
    if ($params) {
      $url .= '?' . http_build_query($params, null, '&');
    }
    return $url;
  }

  /**
   * Returns the Current URL, stripping it of known FB parameters that should
   * not persist.
   *
   * @return String the current URL
   */
  protected function getCurrentUrl() {
    $protocol = PROTOKOL;
    $currentUrl = $protocol . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    $parts = parse_url($currentUrl);

    // drop known fb params
    $query = '';
    if (!empty($parts['query'])) {
      $params = array();
      parse_str($parts['query'], $params);
      foreach(self::$DROP_QUERY_PARAMS as $key) {
        unset($params[$key]);
      }
      if (!empty($params)) {
        $query = '?' . http_build_query($params, null, '&');
      }
    }

    // use port if non default
    $port =
      isset($parts['port']) &&
      (($protocol === 'http://' && $parts['port'] !== 80) ||
       ($protocol === 'https://' && $parts['port'] !== 443))
      ? ':' . $parts['port'] : '';

    // rebuild
    return $protocol . $parts['host'] . $port . $parts['path'] . $query;
  }

  /**
   * Generate a signature for the given params and secret.
   *
   * @param Array $params the parameters to sign
   * @param String $secret the secret to sign with
   * @return String the generated signature
   */
  protected static function generateSignature($params, $secret) {
    // work with sorted data
    ksort($params);

    // generate the base string
    $base_string = '';
    foreach($params as $key => $value) {
      $base_string .= $key . '=' . $value;
    }
    $base_string .= $secret;

    return md5($base_string);
  }

  /**
   * Prints to the error log if you aren't in command line mode.
   *
   * @param String log message
   */
  protected static function errorLog($msg) {
    // disable error log if we are running in a CLI environment
    // @codeCoverageIgnoreStart
    if (php_sapi_name() != 'cli') {
      error_log($msg);
    }
    // uncomment this if you want to see the errors on the page
    // print 'error_log: '.$msg."\n";
    // @codeCoverageIgnoreEnd
  }

  /**
   * Base64 encoding that doesn't need to be urlencode()ed.
   * Exactly the same as base64_encode except it uses
   *   - instead of +
   *   _ instead of /
   *
   * @param String base64UrlEncodeded string
   */
  protected static function base64UrlDecode($input) {
    return base64_decode(strtr($input, '-_', '+/'));
  }
}


/**
 * Facebook wrapper library for facebook new graph api calls and social plugins
 *
 * @author Vadim Gabriel <vadimg88[at]gmail[dot]com>
 * @link http://www.vadimg.com/
 * @copyright Vadim Gabriel
 * @license MIT
 */

/**
 	The MIT License

	Copyright (c) 2010 Vadim Gabriel

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.
 */

/**
 * Provides *Simple* access and a wrapper for various facebook calls
 *
 * @package Facebook Graph API
 * @version 0.1 Alpha
 * @author Vadim Gabriel <vadimg88@gmail.com>
 */
class facebookLib extends Facebook
{
	/**
	 * Library version
	 * @var string
	 */
	const VERSION = '1.2a';
	/**
	   * Default options for curl.
	   */
       
	  public static $CURL_OPTS = array(
	    CURLOPT_CONNECTTIMEOUT => 10,
	    CURLOPT_RETURNTRANSFER => true,
	    CURLOPT_TIMEOUT        => 60,
	    CURLOPT_FRESH_CONNECT  => 1,
		CURLOPT_PORT		   => 443,
	    CURLOPT_USERAGENT      => 'facebook-php-2.0',
	    CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_RETURNTRANSFER => true,
	  );
      
         
      
	/**
	 * whether to disable the SSL check when performing the CURL calls
	 * By default facebook uses SSL verifications, This causes the calls to fails on sites that do not use an SSL
	 * certified domain or include a certification file.
	 * It's recommended not to set this to true but if the following fixes does not work you can use this a a last resort.
	 *
	 *  1. ) Download: http://www.gknw.net/php/phpscripts/mk-ca-bundle.phps to c:\
	 *  2. ) Open cmd and run: c:>php mk-ca-bundle.php
	 *  3. ) Output should read: Downloading 'certdata.txt' ...Done (140 CA certs processed).
	 *  4. ) This file should be created: c:\ca-bundle.crt
	 *  5. ) Move c:\ca-bundle.crt to your preferred path
	 *  6. ) After initiating this library add the following line right below that:
	 *  facebookLib::$CURL_OPTS[CURLOPT_CAINFO] = 'path\to\cert\ca-bundle.crt';
	 * 
	 * You'll end up with something looking like this:
	 * include_once "facebookLib.php";
	 * $facebook = new facebookLib($config);
	 * facebookLib::$CURL_OPTS[CURLOPT_CAINFO] = 'path\to\cert\ca-bundle.crt';
	 *
	 * @var boolean 
	 */
	public $disableSSLCheck = false;
	/**
	 *  the error code if one exists
	 * @var integer
	 */
	protected $errorCode = 0;
	/**
	 * the error message if one exists
	 * @var string 
	 */
	protected $errorMessage = '';
	/**
	 * whether to throw exceptions on error or not
	 * @var boolean 
	 */
	protected $throwExceptions = true;
	/**
	 *  the response message
	 * @var string
	 */
	protected $response = '';
	/**
	 *  the headers returned from the call made
	 * @var array
	 */
	protected $headers = '';
	/**
	 * By default facebook returns most of the time a json object
	 * You can enable this to use json_decode to decode the json object into a php object
	 * @var boolean 
	 */
	protected $decodeJson = false;
	/**
	 * By default facebook returns most of the time a json object
	 * You can enable this to convert an object to an array
	 * @var boolean 
	 */
	protected $returnAsArray = false;
	
	/**
	   * Initialize a Facebook Application.
	   *
	   * The configuration:
	   * - appId: the application API key
	   * - secret: the application secret
	   * - cookie: (optional) boolean true to enable cookie support
	   * - domain: (optional) domain for the cookie
	   *
	   * @param Array the application configuration
	   */
	public function __construct($config)
	{
		parent::__construct($config);
	}
	
	/**
	 * Get users information (based on the permission you have) 
	 *
	 * 
	 * @param string the member profile id, If null will get the current authenticated user
	 * @return array List of information about the user
	 */
	public function getInfo($id=null)
	{
		return $this->callApi('me', $id);
	}
	
	/**
	 * Get information about a certain object
	 * All objects in Facebook can be accessed in the same way
	 *
	 *   - Users: https://graph.facebook.com/btaylor (Bret Taylor)
	 *   - Pages: https://graph.facebook.com/cocacola (Coca-Cola page)
	 *   - Events: https://graph.facebook.com/251906384206 (Facebook Developer Garage Austin)
	 *   - Groups: https://graph.facebook.com/2204501798 (Emacs users group)
	 *   - Applications: https://graph.facebook.com/2439131959 (the Graffiti app)
	 *   - Status messages: https://graph.facebook.com/367501354973 (A status message from Bret)
	 *   - Photos: https://graph.facebook.com/98423808305 (A photo from the Coca-Cola page)
	 *   - Photo albums: https://graph.facebook.com/99394368305 (Coca-Cola's wall photos)
	 *   - Videos: https://graph.facebook.com/614004947048 (A Facebook tech talk on Tornado)
	 *   - Notes: https://graph.facebook.com/122788341354 (Note announcing Facebook for iPhone 3.0)
	 * 
	 * @param mixed the name or id of the object who's information we would like to retrieve 
	 * 				If the param is null then it will fetch the current users information
	 * @return array information related to that object
	 *
	 */
	public function get($id=null)
	{
		return $this->callApi(null, $id);
	}
	
	/**
	 * Get an object picture 
	 * The same URL pattern works for all objects in the graph:
     *
	 *   - People: http://graph.facebook.com/vadim.v.gabriel/picture
	 *   - Events: http://graph.facebook.com/331218348435/picture
	 *   - Groups: http://graph.facebook.com/335845912900/picture
	 *   - Pages: http://graph.facebook.com/DoloresPark/picture
	 *   - Applications: http://graph.facebook.com/2318966938/picture
	 *   - Photo Albums: http://graph.facebook.com/platform/picture
     *	
	 * @param mixed the name or id of the object who's information we would like to retrieve 
	 * 				If the param is null then it will fetch the current users information
	 * @return string the link that will display the objects picture
	 *
	 */
	public function getPicture($id=null)
	{
		return $this->callApi('picture', $id, true);
	}
	
	/**
	 * Get a users links
	 *
	 * @param mixed the name or id of the object who's information we would like to retrieve 
	 * 				If the param is null then it will fetch the current users information
	 * @return array information regarding the user links
	 */
	public function getLinks($userid=null)
	{
		return $this->callApi('links', $userid);
	}
	
	/**
	 * Get a users friends list
	 *
	 * @param mixed the name or id of the object who's information we would like to retrieve 
	 * 				If the param is null then it will fetch the current users information
	 * @return array information regarding the user friends
	 */
	public function getFriends($userid=null)
	{
		return $this->callApi('friends', $userid);
	}
	
	/**
	 * Get a users news feed (the news appearing on his wall)
	 *
	 * @param mixed the name or id of the object who's information we would like to retrieve 
	 * 				If the param is null then it will fetch the current users information
	 * @return array information regarding the users news feed wall
	 */
	public function getNewsFeed($userid=null)
	{
		return $this->callApi('home', $userid);
	}
	
	/**
	 * Get a users profile feed (news from his profile only)
	 *
	 * @param mixed the name or id of the object who's information we would like to retrieve 
	 * 				If the param is null then it will fetch the current users information
	 * @return array information regarding the users profile feed
	 */
	public function getProfileFeed($userid=null)
	{
		return $this->callApi('feed', $userid);
	}
	/**
	 * Get all the likes a user made
	 *
	 * @param mixed the name or id of the object who's information we would like to retrieve 
	 * 				If the param is null then it will fetch the current users information
	 * @return array information regarding the user likes
	 */
	public function getLikes($userid=null)
	{
		return $this->callApi('likes', $userid);
	}
	
	/**
	 * Get users movies
	 *
	 * @param mixed the name or id of the object who's information we would like to retrieve 
	 * 				If the param is null then it will fetch the current users information
	 * @return array information regarding the users favorite movies
	 */
	public function getMovies($userid=null)
	{
		return $this->callApi('movies', $userid);
	}
	
	/**
	 * Get the users books
	 *
	 * @param mixed the name or id of the object who's information we would like to retrieve 
	 * 				If the param is null then it will fetch the current users information
	 * @return array information regarding the users books
	 */
	public function getBooks($userid=null)
	{
		return $this->callApi('books', $userid);
	}
	
	/**
	 * Get a users notes
	 *
	 * @param mixed the name or id of the object who's information we would like to retrieve 
	 * 				If the param is null then it will fetch the current users information
	 * @return array information regarding the users notes
	 */
	public function getNotes($userid=null)
	{
		return $this->callApi('notes', $userid);
	}
	
	/**
	 * Get the users photos
	 *
	 * @param mixed the name or id of the object who's information we would like to retrieve 
	 * 				If the param is null then it will fetch the current users information
	 * @return array information regarding the users photos
	 */
	public function getPhotos($userid=null)
	{
		return $this->callApi('photos', $userid);
	}
	
	/**
	 * Get the users videos
	 *
	 * @param mixed the name or id of the object who's information we would like to retrieve 
	 * 				If the param is null then it will fetch the current users information
	 * @return array information regarding the users videos
	 */
	public function getVideos($userid=null)
	{
		return $this->callApi('videos', $userid);
	}
	
	/**
	 * Get the users events
	 *
	 * @param mixed the name or id of the object who's information we would like to retrieve 
	 * 				If the param is null then it will fetch the current users information
	 * @return array information regarding the users event his attending
	 */
	public function getEvents($userid=null)
	{
		return $this->callApi('events', $userid);
	}
	
	/**
	 * Get the users groups
	 *
	 * @param mixed the name or id of the object who's information we would like to retrieve 
	 * 				If the param is null then it will fetch the current users information
	 * @return array information regarding the users groups his a member of
	 */
	public function getGroups($userid=null)
	{
		return $this->callApi('groups', $userid);
	}
	
	/**
	 * Get the users that are attending to an event
	 *
	 * @param mixed the id of the event we would like to see the attenders
	 * @return array information regarding the users who are attending to the event
	 */
	public function getEventAttenders($eventid)
	{
		return $this->callApi('attending', $eventid);
	}
	
	/**
	 * Post a status update
	 *
	 * @param mixed the name or id of the object we would like to post a update his status
	 * 				If the param is null then it will fetch the current users information
	 * @param mixed access_token of the currently logged in user
	 * @param string the status message
	 * @return string the id of the newly created status
	 */
	public function postStatus( $profileid=null, $access_token, $message )
	{
		return $this->doCall($this->callApi('feed', $profileid, true), array('access_token'=>$access_token,'message'=>$message));
	}

	/**
	 * Post a feed to a users wall
	 *
	 * @param mixed the name or id of the object we would like to post a feed to it's wall
	 * 				If the param is null then it will fetch the current users information
	 * @param mixed access_token of the currently logged in user
	 * @param array list of additional parameters to pass
	 * 		        This method supports the following parameters:
	 * 				- message, picture, link, name, description
	 * @return string the id of the newly created feed
	 */
	public function postFeed( $profileid=null, $access_token, $params=array() )
	{
		return $this->doCall($this->callApi('feed', $profileid, true), array_merge(array('access_token'=>$access_token), $params));
	}
	
	/**
	 * Post a comment on for a certain feed
	 *
	 * @param mixed the id of the feed we would like to post a comment to
	 * @param mixed access_token of the currently logged in user
	 * @param string the comment
	 * @return string the id of the newly created comment
	 */
	public function postComment( $postid, $access_token, $comment )
	{
		return $this->doCall($this->callApi('comments', $postid, true), array('access_token'=>$access_token, 'comment'=>$comment));
	}
	
	/**
	 * Like a certain post feed
	 *
	 * @param mixed the name or id of the object we would like to like (feed)
	 * @param mixed access_token of the currently logged in user
	 * @return boolean true/false on success
	 */
	public function postLike( $postid, $access_token )
	{
		return $this->doCall($this->callApi('likes', $postid, true), array('access_token'=>$access_token));
	}

	/**
	 * Post a note to a users wall
	 *
	 * @param mixed the name or id of the object we would like to post a note to it's wall
	 * 				If the param is null then it will fetch the current users information
	 * @param mixed access_token of the currently logged in user
	 * @param array list of additional parameters to pass
	 * 		        This method supports the following parameters:
	 * 				- message, subject
	 * @return string the id of the newly created note
	 */
	public function postNote($profileid=null, $access_token, $params=array())
	{
		return $this->doCall($this->callApi('notes', $profileid, true), array_merge(array('access_token'=>$access_token), $params));
	}

	/**
	 * Post a link to a users wall
	 *
	 * @param mixed the name or id of the object we would like to post a link to it's wall
	 * 				If the param is null then it will fetch the current users information
	 * @param mixed access_token of the currently logged in user
	 * @param array list of additional parameters to pass
	 * 		        This method supports the following parameters:
	 * 				- message, link
	 * @return string the id of the newly created link
	 */
	public function postLink($profileid=null, $access_token, $params=array())
	{
		return $this->doCall($this->callApi('links', $profileid, true), array_merge(array('access_token'=>$access_token), $params));
	}

	/**
	 * Post an event by a certain user
	 *
	 * @param mixed the name or id of the object we would like to post an event
	 * 				If the param is null then it will fetch the current users information
	 * @param mixed access_token of the currently logged in user
	 * @param array list of additional parameters to pass
	 * 		        This method supports the following parameters:
	 * 				- name, start_time, end_time, description, owner, location
	 * @return string id of the newly created event
	 */
	public function postEvent($profileid=null, $access_token, $params=array())
	{
		return $this->doCall($this->callApi('events', $profileid, true), array_merge(array('access_token'=>$access_token), $params));
	}

	/**
	 * Attend an event
	 *
	 * @param mixed the event id we would like to attend to
	 * @param mixed access_token of the currently logged in user
	 * @return boolean true/false on success
	 */
	public function postAttending($eventid, $access_token)
	{
		return $this->doCall($this->callApi('attending', $eventid, true), array('access_token'=>$access_token));
	}
	
	/**
	 * Decline an event
	 *
	 * @param mixed the event id we would not like to attend
	 * @param mixed access_token of the currently logged in user
	 * @return boolean true/false on success
	 */
	public function postNotAttending($eventid, $access_token)
	{
		return $this->doCall($this->callApi('declined', $eventid, true), array('access_token'=>$access_token));
	}
	
	/**
	 * Maybe Attend an event
	 *
	 * @param mixed the event id we might attend
	 * @param mixed access_token of the currently logged in user
	 * @return boolean true/false on success
	 */
	public function postMaybeAttending($eventid, $access_token)
	{
		return $this->doCall($this->callApi('maybe', $eventid, true), array('access_token'=>$access_token));
	}

	/**
	 * Post a new album to a users profile
	 *
	 * @param mixed the id of the object we would like to post an album
	 * 				If the param is null then it will fetch the current users information
	 * @param mixed access_token of the currently logged in user
	 * @param array list of additional parameters to pass
	 * 		        This method supports the following parameters:
	 * 				- name, message
	 * @return string id of the newly created album
	 */
	public function postAlbum($profileid=null, $access_token, $params=array())
	{
		return $this->doCall($this->callApi('albums', $profileid, true), array_merge(array('access_token'=>$access_token), $params));
	}

	/**
	 * Upload a photo to an album
	 *
	 * @param mixed the id of the object we would like to upload a photo to
	 * 				If the param is null then it will fetch the current users information
	 * @param mixed access_token of the currently logged in user
	 * @param string the message describing the photo
	 * @return string id of the newly created photo
	 */
	public function postPhoto( $albumid, $access_token, $message )
	{
		return $this->doCall($this->callApi('photos', $albumid, true), array('access_token'=>$access_token, 'message'=>$message));
	}

	/**
	 * Search for a certain object
	 *
	 * All public posts: 
	 * - https://graph.facebook.com/search?q=watermelon&type=post
     * - People: https://graph.facebook.com/search?q=mark&type=user
     * - Pages: https://graph.facebook.com/search?q=platform&type=page
     * - Events: https://graph.facebook.com/search?q=conference&type=event
     * - Groups: https://graph.facebook.com/search?q=programming&type=group
 	 *
	 * @param mixed type of the search
	 * @param mixed query the query to search for
	 * @param array list of additional parameters to pass
	 * @return object search results
	 */
	public function search( $type, $query, $params=array() )
	{
		return $this->doCall($this->callApi('search', 0, true, true, array_merge(array('q'=>$query,'type'=>$type), $params)), array(), false );
	}
	
	/**
	 * Display the facebook social plugin activity box 
	 * 
	 * @param string the site url we would like to get the activity from
	 * @param integer the width of the activity box
	 * @param integer the height of the activity box
	 * @param string 'true'/'false' display the activity box header or not
	 * @param string 'light'/'dark' the activity box color scheme
	 * @param boolean whether we would like to use the fb:activity fbml tag or an iframe
	 * @return string the generated iframe
	 */
	public function showActivity( $site, $width=300, $height=300, $header='true', $color='light', $fbml=true )
	{
		if( $fbml )
		{
			return "<fb:activity site='{$site}' width='{$width}' height='{$height}' colorscheme='{$color}' header='{$header}'></fb:activity>";
		}
		return "<iframe src='http://www.facebook.com/plugins/activity.php?site={$site}width={$width}&amp;height={$height}&amp;header={$header}&amp;colorscheme={$colorScheme}' scrolling='no' frameborder='0' allowTransparency='true' style='border:none; overflow:hidden; width:{$width}px; height:{$height}px'></iframe>";
	}
	
	/**
	 * Display the facebook social plugin comments box
	 * 
	 * @param integer unique id of this widget
	 * @param integer the width of the comments box
	 * @param integer number of posts to show in the comments box
	 * @return string the generated fb:comments tag
	 */
	public function showComments( $id='widget', $width=500, $numposts=10 )
	{
		return "<fb:comments xid='{$id}' numposts='{$numposts}' width='{$width}'></fb:comments>";
	}
	
	/**
	 * Display the facebook social plugin face pile
	 * 
	 * @param integer the width of the face pile box
	 * @param integer number of rows to display
	 * @return string the generated fb:facepile 
	 */
	public function showFacePile( $width=200, $numrows=10 )
	{
		return "<fb:facepile max-rows='{$numrows}' width='{$width}'></fb:facepile>";
	}
	
	/**
	 * Display the facebook social plugin like button
	 * 
	 * @param string the url we would like to like
	 * @param string the layout of the like button
	 * @param string 'true'/'false' whether to show faces under the like button
	 * @param integer 
	 * @param string 'like'/'recommended' the action name
	 * @param string 'light'/'dark' the activity box color scheme
	 * @param boolean whether we would like to use the fb:activity fbml tag or an iframe
	 * @return string the generated iframe
	 */
	public function showLike( $url, $layout='standard', $showfaces='true', $width=450, $action='like', $color='light', $fbml=true )
	{
		if( $fbml )
		{
			return "<fb:like href='{$url}' layout='{$layout}' show_faces='{$showfaces}' width='{$width}' action='{$action}' colorscheme='{$color}'></fb:like>";
		}
		return "<iframe src='http://www.facebook.com/plugins/like.php?href={$url}&layout={$layout}&amp;show_faces={$showfaces}&amp;width={$width}&amp;action={$action}&amp;colorscheme={$color}' scrolling='no' frameborder='0' allowTransparency='true' style='border:none; overflow:hidden; width:{$width}px; height:px'></iframe>";
	}
	
	/**
	 * The Like Box is a social plugin that enables Facebook Page owners to attract and gain Likes from their own website. 
	 * The Like Box enables users to:
     *
	 * - See how many users already like this page, and which of their friends like it too
	 * - Read recent posts from the page
	 * - Like the page with one click, without needing to visit the page
	 *
	 * @param integer the profile id of the page we would like to show likes from
	 * @param integer the width of the like box widget
	 * @param integer the number of connections to show
	 * @param string 'true'/'false' whether to show the page public stream or not
	 * @param string 'true'/'false' whether to show the header in the box or not
	 * @return string the generated iframe	
	 *
	 */
	public function showLikeBox( $profileid, $width=292, $connections=10, $stream='true', $header='true' )
	{
		return "<fb:like-box profile_id='{$profileid}' width='{$width}' connections='{$connections}' stream='{$stream}' header='{$header}'></fb:like-box>";
	}
	
	/**
	 * The Live Stream plugin lets users visiting your site or application share activity and comments in real time. 
	 * The Live Stream Box works best when you are running a real-time event, like live streaming video for concerts, speeches, or webcasts, live Web chats, webinars, massively multiplayer games.
	 * 
	 * @param integer the application id to show the live stream from
	 * @param integer the width of the live stream box
	 * @param integer the height of the live stream box
	 * @param mixed the live stream unique id (if you have more then one live stream on the page then enter a unique name for each one)
	 * @return string the live stream widget
	 */
	public function showLiveStream( $appid, $width=400, $height=500, $xid='livestream' )
	{
		return "<fb:live-stream app_id='{$appid}' width='{$width}' height='{$height}' xid='{$xid}'></fb:live-stream>";
	}
	
	/**
	 * The Login with Faces plugin shows profile pictures of the user's friends who have already signed up for your site in addition to a login button.
	 * 
	 * @param string 'true'/'false' whether to show the faces of the facebook users who already use this application
	 * @param integer the width of the facebook login button
	 * @param integer number of rows to display the faces in
	 * @return string the login button fbml tag
	 */
	public function showLoginButton( $showfaces='true', $width=200, $maxrows=1 )
	{
		return "<fb:login-button show-faces='{$showfaces}' width='{$width}' max-rows='{$maxrows}'></fb:login-button>";
	}
	
	/**
	 * The Recommendations plugin shows personalized recommendations to your users. 
	 * Since the content is hosted by Facebook, the plugin can display personalized recommendations whether or not the user has logged into your site. 
	 * To generate the recommendations, the plugin considers all the social interactions with URLs from your site. 
	 * For a logged in Facebook user, the plugin will give preference to and highlight objects her friends have interacted with.
	 * 
	 * @param string the url we would like to show the recommendations from
	 * @param integer the width of the recommendations box
	 * @param integer the height of the recommendations box
	 * @param string 'true'/'false' whether to include the header of the recommendations box
	 * @param string 'light'/'dark' display the recommendations box in a certain color scheme
	 * @param boolean whether to show the recommendation box using facebook fbml tag or an iframe
	 * @return string the recommendations fbml tag or iframe
	 */
	public function showRecommendations( $url, $width=300, $height=300, $header='true', $color='light', $fbml=true )
	{
		if( $fbml )
		{
			return "<fb:recommendations site='{$url}' width='{$width}' height='{$height}' header='{$header}' colorscheme='{$color}'></fb:recommendations>";
		}
		return "<iframe src='http://www.facebook.com/plugins/recommendations.php?site={$url}&amp;width={$width}&amp;height={$height}&amp;header={$header}&amp;colorscheme={$color}' scrolling='no' frameborder='0' allowTransparency='true' style='border:none; overflow:hidden; width:{$width}px; height:{$height}px'></iframe>";
	}
	
	/**
	 * 
	 * 
	 * @param integer the application id
	 * @param string 'true'/'false' whether to check the login status or not
	 * @param string 'true'/'false' whether to enable cookies to allow the server to access the session
	 * @param string 'true'/'false' whether to enable parsing XFBML
	 * @param string the language of the JS file loaded canonical ID for example 'en_US', 'he_IL' etc..
	 * @return string the generated script tag and div
	 */
	public function includeScript( $appid, $status='true', $cookie='true', $xfbml='true', $language='en_US' )
	{
		$code = <<<EOF
			<div id="fb-root"></div>
			<script>
			  window.fbAsyncInit = function() {
			    FB.init({appId: '{$appid}', status: {$status}, cookie: {$cookie}, xfbml: {$xfbml} });
			  };
			  (function() {
			    var e = document.createElement('script'); e.async = true;
			    e.src = document.location.protocol +
			      '//connect.facebook.net/{$language}/all.js';
			    document.getElementById('fb-root').appendChild(e);
			  }());
			</script>
EOF;
	
		return $code;
		
	}
	
	/**
	 * Set the SSL Check status
	 *
	 * @param boolean the SSL check status
	 * @return facebookLib object
	 */
	public function setSSLStatus($status=false)
	{
		$this->disableSSLCheck = $status;
		return $this;
	}
	/**
	 * Get the SSL check status
	 * 
	 * @return boolean the SSL Check status
	 */
	public function getSSLStatus()
	{
		return $this->disableSSLCheck;
	}
	/**
	 * Set the headers
	 *
	 * @param array the headers array
	 * @return facebookLib object
	 */
	public function setHeaders( $headers='' )
	{
		$this->headers = $headers;
		return $this;
	}
	/**
	 * Get the headers
	 * 
	 * @return array the headers returned from the call
	 */
	public function getHeaders()
	{
		return $this->headers;
	}
	/**
	 * Set the error code number
	 *
	 * @param integer the error code number
	 * @return facebookLib object
	 */
	public function setErrorCode($code=0)
	{
		$this->errorCode = $code;
		return $this;
	}
	/**
	 * Get the error code number
	 * 
	 * @return integer error code number
	 */
	public function getErrorCode()
	{
		return $this->errorCode;
	}
	/**
	 * Set the error message
	 *
	 * @param string the error message
	 * @return facebookLib object
	 */
	public function setErrorMessage($message='')
	{
		$this->errorMessage = $message;
		return $this;
	}
	/**
	 * Get the error code message
	 * 
	 * @return string error code message
	 */
	public function add_error()
	{
		return $this->errorMessage;
	}
	/**
	 * Set the throwExceptions status
	 *
	 * @param boolean the throwExceptions status
	 * @return facebookLib object
	 */
	public function setThrowExceptions($status=true)
	{
		$this->throwExceptions = $status;
		return $this;
	}
	/**
	 * Get the throwExceptions
	 * 
	 * @return boolean the throwExceptions
	 */
	public function getThrowExceptions()
	{
		return $this->throwExceptions;
	}
	/**
	 * Set the response
	 *
	 * @param mixed the response returned from the call
	 * @return facebookLib object
	 */
	public function setResponse( $response='' )
	{
		$this->response = $response;
		return $this;
	}
	/**
	 * Get the response data
	 * 
	 * @return mixed the response data
	 */
	public function getResponse()
	{
		return $this->response;
	}
	/**
	 * Set the Json Decode property
	 *
	 * @param boolean the json decode status
	 * @return facebookLib object
	 */
	public function setDecodeJson($status=false)
	{
		$this->decodeJson = $status;
		return $this;
	}
	/**
	 * Get the DecodeJson property status
	 *
	 * @return boolean the decodeJson status
	 */
	public function getDecodeJson()
	{
		return $this->decodeJson;
	}
	/**
	 * Set the return as an array property
	 *
	 * @param boolean
	 * @return facebookLib object
	 */
	public function setReturnArray($status=false)
	{
		$this->returnAsArray = $status;
		return $this;
	}
	/**
	 * Get the DecodeJson property status
	 *
	 * @return boolean the decodeJson status
	 */
	public function getReturnArray()
	{
		return $this->returnAsArray;
	}
	
	/**
	 * This function is used to 
	 *
	 * @param string the url we would like to parse
	 * @param array array of post parameters to include in the call
	 * @param boolean whether to use a post request or a get request
	 * @throws Exception
	 * @return string error message on failure mixed on success (based on the certain call can be an array or information, integer representing an id or a string)
	 */
	protected function doCall($url, $params=array(), $usepost=true)
	{	
		$var = '';
		// rebuild url if we don't use post
		if(count( $params ))
		{
			// rebuild parameters
			foreach($params as $key => $value) 
			{
				$var .= '&'. $key .'='. urlencode($value);
			}	
		}
		
		// If disabled is on we need to convert the https to http for post calls
		// Since it won't allow use to post stuff to facebook
		if( $this->disableSSLCheck )
		{
			$url = str_replace('https://', 'http://', $url);
		}
		
		// set options
		$options = self::$CURL_OPTS;
		$options[CURLOPT_URL] = $url;
		// set extra options
		if( $usepost )
		{
			$options[CURLOPT_POST] = true;
			$options[CURLOPT_POSTFIELDS] = trim($var, '&');
		}
		
		// Disable SSL check if we do not use SSL here
		if( $this->disableSSLCheck )
		{
			$options[CURLOPT_SSL_VERIFYPEER] = false;
		}

		// init
		$curl = curl_init();

		// set options
		curl_setopt_array($curl, $options);

		// execute
		$this->setResponse( curl_exec($curl) );
		$this->setHeaders( curl_getinfo($curl) );

		// fetch errors
		$this->setErrorCode( curl_errno($curl) );
		$this->setErrorMessage( curl_error($curl) );
		
		// If error then return the error
		if( $this->add_error() )
		{
			if( $this->getThrowExceptions() )
			{
				throw new Exception( $this->add_error() );
			}
			else
			{
				return $this->add_error();
			}
		}

		// close
		curl_close($curl);
		
		// Do we need to convert Json?
		if( substr($this->getResponse(), 0, 2) == '{"' )
		{
			if( $this->getDecodeJson() )
			{
                $return = json_decode( $this->getResponse() );
			    // Convert to array
                if($this->getReturnArray()) {
                    $return = $this->objectToArray($return);
                }
				$this->setResponse( $return );
			}
		}
		
		return $this->getResponse();
	}
	
   	/**
   	* Makes an HTTP request. This method can be overriden by subclasses if
   	* developers want to do fancier things or use something other than curl to
   	* make the request.
   	*
   	* @param String the URL to make the request to
   	* @param Array the parameters to use for the POST body
   	* @param CurlHandler optional initialized curl handle
   	* @return String the response text
   	*/
  	protected function makeRequest($url, $params, $ch=null) 
	{
    	if (!$ch) {
      	$ch = curl_init();
    	}
		
		// If disabled is on we need to convert the https to http for post calls
		// Since it won't allow use to post stuff to facebook
		if( $this->disableSSLCheck )
		{
			$url = str_replace('https://', 'http://', $url);
		}
		
    	$opts = self::$CURL_OPTS;
    	$opts[CURLOPT_POSTFIELDS] = http_build_query($params, null, '&');
    	$opts[CURLOPT_URL] = $url;

		// Disable SSL check if we do not use SSL here
		if( $this->disableSSLCheck )
		{
			$opts[CURLOPT_SSL_VERIFYPEER] = false;
		}

		// init
		$ch = curl_init();

		// set options
		curl_setopt_array($ch, $opts);

		// execute
		$this->setResponse( curl_exec($ch) );
		$this->setHeaders( curl_getinfo($ch) );

		// fetch errors
		$this->setErrorCode( curl_errno($ch) );
		$this->setErrorMessage( curl_error($ch) );
		
		// If error then return the error
		if( $this->add_error() )
		{
			if( $this->getThrowExceptions() )
			{
				throw new Exception( $this->add_error() );
			}
			else
			{
				return $this->add_error();
			}
		}

		// close
		curl_close($ch);
		
		// Do we need to convert JSON?
		if( substr($this->getResponse(), 0, 2) == '{"' )
		{
			if( $this->getDecodeJson() )
			{   
			    $return = json_decode( $this->getResponse() );
			    // Convert to array
                if($this->getReturnArray()) {
                    $return = $this->objectToArray($return);
                }
				$this->setResponse( $return );
			}
		}
		
		return $this->getResponse();
    }
	
	/**
	 * Internal Call to facebook graph API 
	 *
	 *
	 * @param string the API method we would like to call
	 * @param integer/string the user id or name we use to make the call
	 * @param boolean if to perform a call or just return a link of that call instead
	 * @param boolean if we would like to perform the call without the user id in the link
	 * @param array any additional parameters to pass to the call method
	 * @return mixed response
	 */
	protected function callApi($api='', $userid=null, $returnLink=false, $nouid=false, $params=array())
	{
		$uid = $userid !== null ? $userid : $this->getUser();
		$uid = $nouid === true ? '' : $uid . '/';
		$apimethod = $api == 'me' ? '' : $api;
		$query = !empty($params) ? '?' . http_build_query($params) : '';
		if( $returnLink )
		{
			return Facebook::$DOMAIN_MAP['graph'] . $uid . $apimethod . $query;
		}
        return $this->api($uid . $apimethod . $query);
	}
	
	/**
	 * Convert an object to an array
	 *
	 */
	protected function objectToArray( $object )
    {
        if( !is_object( $object ) && !is_array( $object ) )
        {
            return $object;
        }
        if( is_object( $object ) )
        {
            $object = get_object_vars( $object );
        }
        return array_map( array($this, 'objectToArray'), $object );
    }
	
}

?>