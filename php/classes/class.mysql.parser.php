<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

class MySQL_Restore
{
   private $file;
   private $gz_compress;
   private $error;
   private $content;
   
   function MySQL_Restore($file, $gz_compress=false)
   {     
      
      $this->gz_compress = $gz_compress;
      
      $this->setFile($file);
      $this->setContent();
                
      $this->error = "";     
   }
   
   public function getError()
   {
    return $this->error;
   }
   
   /**
   * @purpose : Sets filename to be parsed
   * @params $file
   * @return none
   */
   public function setFile($file)
   {
      $this->file = $file;
   }

   /**
   * @purpose : Gets filename to be parsed
   * @params none
   * @return filename
   */
   
   public function getFile()
   {
      return $this->file;
   }
   
   /**
   * @purpose : Parses SQL file
   * @params none
   * @return none
   */

   private function setContent()
   {
    
    $file = $this->getFile(); 
    // Getting the SQL file content  
        if($this->gz_compress)
            $this->content = $this->gzfile_get_contents($file);
            else     
            $this->content = file_get_contents($file);
            
        if($this->content == "")
            $this->error = ERROR_NEPODARILO_SE_NACIST_SOUBOR_SE_ZALOHOU;
   }

   public function startParsing()
   {
        global $db; 
               
        // Processing the SQL file content             
        $file_content = explode("\n",$this->content);           
     
     
        $query = "";
       
          // Parsing the SQL file content             
          foreach($file_content as $idr => $sql_line)
          {       
             if(trim($sql_line) != "" && strpos($sql_line, "--") === false)
             {             
                $query .= $sql_line;
                  // Checking whether the line is a valid statement
                  if(preg_match("/(.*);/", $sql_line) && substr(trim($query),-1,1) == ";")
                  {
                    //echo substr(trim($query),-1,1);
                    //test jestli na dalsim radku je obsazeno nejake klicove slovo
                    $res = !isset($file_content[$idr+1]);
                    $kl = array("DROP","INSERT","CREATE");
                    if(!$res)
                    {
                        $l = count($file_content);
                        foreach($kl AS $sl)
                        {
                            for($i = $idr+1; $i < $l; $i++)
                            {
                                //pokud radek neexistuje je radek se strednikem 
                                //povazovan za konec dotazu a je spusten
                                
                                if(!isset($file_content[$i]))
                                {
                                    $res = true;
                                    break;
                                }
                                
                                //pokud je radek prazdny pokracuje na dalsi radek
                                if(trim($file_content[$i]) == "") continue;
                                
                                //pokud je nalezeno na nepraznem radku klicove slovo mysql, 
                                //pak se cyklus prerusi a radek se strednikem je povazovan 
                                //za konec dotazu, dotaz se muze skompletovat a spustit
                                $res = (bool) strstr(substr($file_content[$i],0,20),strtoupper($sl));
                                break;
                                
                                    
                            }
                        if($res) break;
                        
                        }
                    }
                    
                    if($res)
                       {
                       $query = substr($query, 0, -1);                       
                       //Executing the parsed string, returns the error code in failure
                       $result = $db->query($query);
                       $query = "";
                       }
                  }
             }
          } //End of foreach
       
        return true;
   } //End of function
   
   
  private function gzfile_get_contents($filename, $use_include_path = 0) {
    $file = @gzopen($filename, 'rb', $use_include_path);
    $data = "";
    if ($file) {
        while (!gzeof($file)) {
            $data .= gzread($file, 1024);
        }
        gzclose($file);
    }
    return $data;
  }
  
  // remove_comments will strip the sql comment lines out of an uploaded sql file
// specifically for mssql and postgres type files in the install.... 
    private function remove_comments()
{
   $lines = explode("\n", $this->content);
   $output = "";

   // try to keep mem. use down
   $linecount = count($lines);

   $in_comment = false;
   for($i = 0; $i < $linecount; $i++)
   {
      if( preg_match("/^\/\*/", preg_quote($lines[$i])) )
      {
         $in_comment = true;
      }

      if( !$in_comment )
      {
         $output .= $lines[$i] . "\n";
      }

      if( preg_match("/\*\/$/", preg_quote($lines[$i])) )
      {
         $in_comment = false;
      }
   }

   unset($lines);
   $this->content = $output;
} 
  

// remove_remarks will strip the sql comment lines out of an uploaded sql file
  private function remove_remarks()
{
   $lines = explode("\n", $this->content);
   
   // try to keep mem. use down
   $sql = "";
   
   $linecount = count($lines);
   $output = "";

   for ($i = 0; $i < $linecount; $i++)
   {
      if (($i != ($linecount - 1)) || (strlen($lines[$i]) > 0))
      {
         if (isset($lines[$i][0]) && $lines[$i][0] != "#")
         {
            $output .= $lines[$i] . "\n";
         }
         else
         {
            $output .= "\n";
         }
         // Trading a bit of speed for lower mem. use here.
         $lines[$i] = "";
      }
   }
   
   $this->content = $output;
   
} 
  

  public function split_sql_file($delimiter = ";")
{
    
   $this->remove_comments();
   $this->remove_remarks(); 
    
   // Split up our string into "possible" SQL statements.
   $tokens = explode($delimiter, $this->content);

   // try to save mem.
   $sql = "";
   $output = array();
   
   // we don't actually care about the matches preg gives us.
   $matches = array();
   
   // this is faster than calling count($oktens) every time thru the loop.
   $token_count = count($tokens);
   for ($i = 0; $i < $token_count; $i++)
   {
      // Don't wanna add an empty string as the last thing in the array.
      if (($i != ($token_count - 1)) || (strlen($tokens[$i] > 0)))
      {
         // This is the total number of single quotes in the token.
         $total_quotes = preg_match_all("/'/", $tokens[$i], $matches);
         // Counts single quotes that are preceded by an odd number of backslashes,
         // which means they're escaped quotes.
         $escaped_quotes = preg_match_all("/(?<!\\\\)(\\\\\\\\)*\\\\'/", $tokens[$i], $matches);
         
         $unescaped_quotes = $total_quotes - $escaped_quotes;
         
         // If the number of unescaped quotes is even, then the delimiter did NOT occur inside a string literal.
         if (($unescaped_quotes % 2) == 0)
         {
            // It's a complete sql statement.
            $output[] = trim($tokens[$i]);
            // save memory.
            $tokens[$i] = "";
         }
         else
         {
            // incomplete sql statement. keep adding tokens until we have a complete one.
            // $temp will hold what we have so far.
            $temp = $tokens[$i] . $delimiter;
            // save memory..
            $tokens[$i] = "";
            
            // Do we have a complete statement yet?
            $complete_stmt = false;
            
            for ($j = $i + 1; (!$complete_stmt && ($j < $token_count)); $j++)
            {
               // This is the total number of single quotes in the token.
               $total_quotes = preg_match_all("/'/", $tokens[$j], $matches);
               // Counts single quotes that are preceded by an odd number of backslashes,
               // which means they're escaped quotes.
               $escaped_quotes = preg_match_all("/(?<!\\\\)(\\\\\\\\)*\\\\'/", $tokens[$j], $matches);
         
               $unescaped_quotes = $total_quotes - $escaped_quotes;
               
               if (($unescaped_quotes % 2) == 1)
               {
                  // odd number of unescaped quotes. In combination with the previous incomplete
                  // statement(s), we now have a complete statement. (2 odds always make an even)
                  $output[] = trim($temp . $tokens[$j]);

                  // save memory.
                  $tokens[$j] = "";
                  $temp = "";
                  
                  // exit the loop.
                  $complete_stmt = true;
                  // make sure the outer loop continues at the right point.
                  $i = $j;
               }
               else
               {
                  // even number of unescaped quotes. We still don't have a complete statement.
                  // (1 odd and 1 even always make an odd)
                  $temp .= $tokens[$j] . $delimiter;
                  // save memory.
                  $tokens[$j] = "";
               }
               
            } // for..
         } // else
      }
   }

   return $output;
}   



   
} //End of class


 

?>