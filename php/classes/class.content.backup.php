<?php

/**
 * @author Pavel Srom
 * @copyright 2010
 */

class ContentBackup{


    //maximalni pocet zaloh pro jednu stranku
    private $max_store;

    //id domeny
    private $domain_id;
    
    //id objektu
    private $object_id;
    
    //typ objektu
    private $object_type;
    
    //id uzivatele
    private $user_id;


    //konstruktor tridy
    public function __construct($domain_id, $object_id, $user_id, $object_type = 'stranka'){
        
        $this->max_store = POCET_UCHOVAVANYCH_ZALOH;
        
        if($this->max_store<0) 
            $this->max_store = 0;
            
        $this->domain_id = $domain_id;
        $this->user_id = $user_id;
        $this->object_id = $object_id;
        $this->object_type = $object_type;
        
            
        }

    private function Clean()
    {
        global $db;
        
        if($this->max_store == 0){
            $db->query('TRUNCATE TABLE '.TABLE_ZALOHY);
            return;
            }
        
        //otestovani zda v databazi neni prekrocen maximalni pocet zaloh
        $data = $db->query("SELECT idZalohy AS id
            FROM ".TABLE_ZALOHY." 
            WHERE idObjektu=".$this->object_id." 
                AND typ='".$this->object_type."' 
                AND idDomeny=".$this->domain_id." 
            ORDER BY datum DESC ");
        
        //smazani prebytecnych zaloh
        $pocet_zaznamu = $db->numRows($data);
        if(($pocet_zaznamu+1) > $this->max_store){
            $i=1;
            while($row = $db->getObject($data)){
                if($i < $this->max_store) {
                    $i++;
                    continue;
                    }
                $db->query("DELETE FROM ".TABLE_ZALOHY." WHERE idZalohy=".$row->id." AND idDomeny=".$this->domain_id." LIMIT 1");
                $i++;
                }
            }
        
    }


    //prida zalohu do skladu zaloh
    //id typu je id stranky nebo id editboxu, typ je stranka nebo editbox, idAutora je id uzivatele
     public function AddStore($content="", $perex="",$popis1 = "", $popis2 = "", $popis3 = "", $popis4 = "", $popis5 = ""){
        global $db;
    
        $this->Clean();
        
        $insert = array(
            "idObjektu" => $this->object_id,
            "typ" => $this->object_type,
            "obsah" => $content,
            "obsahBezHtml" => strip_tags($content),
            "perex" => $perex,
            "perexBezHtml" => strip_tags($perex),
            "popis1" => $popis1,
            "popis1BezHtml" => strip_tags($popis1),
            "popis2" => $popis2,
            "popis2BezHtml" => strip_tags($popis2),
            "popis3" => $popis3,
            "popis3BezHtml" => strip_tags($popis3),
            "popis4" => $popis4,
            "popis4BezHtml" => strip_tags($popis4),
            "popis5" => $popis5,
            "popis5BezHtml" => strip_tags($popis5),
            "datum" => "now",
            "idUzivatele" => $this->user_id,
            "idDomeny" => $this->domain_id
            );

        $db->insert(TABLE_ZALOHY, $insert);

    
    }


    //vraci obsah stranky nebo editboxu pro ulozeni do zalohy
     private function GetOldContent(){
        global $db;
        
        if($typ=='stranka')
            $data = $db->query("SELECT obsah,perex,popis1, popis2, popis3, popis4, popis5 FROM ".TABLE_STRANKY." WHERE idStranky=".$this->object_id." AND idDomeny=".$this->domain_id." LIMIT 1");
            
        elseif($typ=='editbox')
            $data = $db->query("SELECT obsah,'' AS perex FROM ".TABLE_EDITBOXY." WHERE idEditboxu=".$this->object_id." AND idDomeny=".$this->domain_id." LIMIT 1");
        
        elseif($typ=='newsletter')
            $data = $db->query("SELECT obsah,'' AS perex FROM ".TABLE_EMAILY_KAMPANE." WHERE idKampane=".$this->object_id." AND idDomeny=".$this->domain_id." LIMIT 1");
            
        else return "";
        
        if($db->numRows($data)==0) return "";
        
        $row = $db->getAssoc($data);
        return $row;
        /*
        return array(
            'obsah'=>$db->secureString($row->obsah,false),
            'perex'=>$db->secureString($row->perex,false)
            );
        */
    }
    
    
    //vraci obsah zalohy
    public function GetContent($idStore){
        global $db;
        
        $data = $db->query("SELECT obsah,perex,popis1, popis2, popis3, popis4, popis5 FROM ".TABLE_ZALOHY." WHERE idZalohy=".$idStore." AND idDomeny=".$this->domain_id." AND idObjektu=".$this->object_id." AND typ='".$this->object_type."' LIMIT 1");
        
        if($db->numRows($data)==0) return "";
        
        $row = $db->getAssoc($data);
        return $row;
   
        }
        
   
   //vraci id zalohy posledni verze stranky
   private function GetIdLastVersion(){
        global $db;
        $poradi = 2;
        $data = $db->query("SELECT idZalohy AS id 
            FROM ".TABLE_ZALOHY." 
            WHERE idObjektu=".$this->object_id." 
                AND typ='".$this->object_type."' 
                AND idDomeny=".$this->domain_id." 
            ORDER BY datum_zalozeni DESC 
            LIMIT ".$poradi);
        
        if($db->numRows($data)>0){
            $i=1;
            while($row = $db->getObject($data)){
                if($i==$poradi) {return $row->id;}
                $i++;
                }
            }        
        
        return 0;
    
   }
   
    
    
    
    
}




?>