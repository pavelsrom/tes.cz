<?php

/**
 * @author Pavel Srom
 * @copyright 2013
 */

//*******************************************************************
//trida, ktera pracuje s tagy (stitky) stranek, clanku, novinek atd.
//*******************************************************************

class C_Tags{
    
    //id objektu
    private $object_id = 0;
        
    //typ objektu
    private $object_type = "";
    
    //obsahuje data tagu - id tagu a nazev
    private $tags = array();
    
    //obsahuje informace o tom, ktery objekt ma prirazeny ktery tag
    private $data = array();
    
    //url stranky, kde jsou vypsany vsechny tagy
    private $url = "tags/";
    
    //jazyk tagu, pokud neni nastaven vyberou se vsechny jazyky
    private $lang = "";
    
    
    //konstruktor
    //vstup: id objektu (stranky, clanku, novinky, obrazku, galerie...), typ objektu: (stranka,galerie,obrazek,clanek,...)
    public function __construct($object_id = 0, $object_type = "", $set_data = false, $set_tags = false)
    {
        
        global $lang;
        
        $this->lang = $lang->get_id();
        $this->object_id = $object_id;
        $this->object_type = $object_type;
        
                
        if($set_data)
            $this->set_data();
        
        if($set_tags)
            $this->set_tags();

        
    }
    
    //nastavi zakladni url adresu pro tagy
    public function set_url($url)
    {
        $this->url = $url;
    }
    
    //vraci udaj o tagu 
    //vstupem je co se ma vracet a podle ceho se to ma vyhledat a zda se ma vratit jen prvni nalezeny vysledek nebo cele pole vysledku
    //ve zkratce: funkce vraci hodnotu tagu "what" pokud hodnota tagu "by" == by_value
    //pokud neni nic nalezeno vraci se false
    public function get($what, $by, $by_value, $return_first = true)
    {
        $result = array();
        foreach($this->tags AS $idt => $t)
        {
            if(isset($t->{$by}) && $t->{$by} == $by_value && isset($t->{$what}))
                $result[] = $t->{$what};
        }
        
        if($return_first && count($result) > 0)
            return $result[0];
        elseif(!$return_first && count($result) > 0)
            return $result;
        else
            return false;

    }
    
    //vraci url adresu stranky pro seznam tagu a detail tagu (vypis novinek/clanku/stranek)
    public function get_url($id_tag = 0)
    {
        $url = $this->url;
        if(isset($this->tags[$id_tag]))
            $url .= $this->tags[$id_tag]->url;
        
        return $this->url;
    }
    
    //nastavi informace o tom, ktere objekty maji prirazeny ktere tagy
    public function set_data()
    {
        global $db;
        global $domain;
        
        $where = "";
        if($this->lang != "")
            $where .= " AND t.idJazyka='".$this->lang."'";
            
        if($this->object_type != '')
            $where .= " AND ts.typObjektu='".$this->object_type."'";
            
            
        $data = $db->query("SELECT ts.idTagu AS id_tag, ts.idObjektu AS id_objekt, ts.typObjektu AS typ_objektu 
            FROM ".TABLE_STRANKY_TAGY." AS ts 
            LEFT JOIN ".TABLE_STRANKY." AS t ON ts.idTagu = t.idStranky 
            WHERE t.idDomeny='".$domain->getId()."' 
                AND t.typ = 'tag' 
                $where
            GROUP BY CONCAT(ts.idTagu,ts.idObjektu,ts.typObjektu)
            ORDER BY typObjektu DESC
            ");
                
        while($d = $db->getObject($data))
            $this->data[] = $d;

    }
    
    //nastavi informace o vsech stitcich - nazev a id
    public function set_tags($set_count = false)
    {
        global $db;
        global $domain;


        $where = "";
        if($this->lang != "")
            $where = " AND tg.idJazyka='".$this->lang."'";
        
        $data = $db->query("SELECT tg.idStranky AS id_tag, tg.nazev, tg.url AS url, 0 AS count 
            FROM ".TABLE_STRANKY." AS tg 
            WHERE tg.typ = 'tag'
                AND tg.idDomeny = '".$domain->getId()."' 
                ".$where." 
            GROUP BY CONCAT(tg.idStranky,tg.nazev) 
            ORDER BY tg.nazev, tg.idStranky
            ");          
                
        while($d = $db->getObject($data))
            $this->tags[$d->id_tag] = $d;
        
        
        if(!$set_count) return;
        
        //global $privatni_stranky;
        
        //-------------------------------------------
        //zjisteni kolik objektu dany tag obsahuje
        $data = $db->query("SELECT t.idTagu AS id_tag, t.idObjektu AS id_objekt, t.typObjektu AS typ_objektu
            FROM ".TABLE_STRANKY_TAGY." AS t
            LEFT JOIN ".TABLE_STRANKY." AS tg ON t.idObjektu = tg.idStranky
            LEFT JOIN ".TABLE_STRANKY." AS s ON t.idObjektu = s.idStranky
            ".$where."
            ");
            
        $tagy_pocty = array();
        
        while($o = $db->getObject($data))
        {
            //$typ = $o->typ_objektu;
            //$ids = $o->id_objekt;
            $idt = $o->id_tag;
                                               
            $this->tags[$idt]->count++;
                    
        }

        
        
    }
    

    //nastavi jazyk
    public function set_lang($lang = "cz")
    {
        $this->lang = $lang;
    }
    
    //vraci pole tagu, ktere prislusi danemu objektu
    public function get_tags()
    {
        $result = array();
        
        $tags_data = isset($_POST['as_values_tagy']) ? $_POST['as_values_tagy'] : false;
        $tags = explode(",",$tags_data);
        if($tags_data !== false)
        {
            foreach($tags AS $idt)
            {
                $id_tag = intval($idt);
                if($id_tag<=0) continue;
                 
                $result[$id_tag] = $this->tags[$id_tag];
            }
            
            return $result;
        }
        
        foreach($this->data AS $d)
        {
            if($d->id_objekt == $this->object_id && $d->typ_objektu == $this->object_type)
                $result[$d->id_tag] = $this->tags[$d->id_tag];
        }
        
        //asort($result);
        return $result;
    }
    
    //vraci pole vsech tagu
    public function get_all_tags()
    {
        return $this->tags;
    }
    
    //ulozi tagy do db
    public function save_tags($object_id=0, $object_type="")
    {
        global $db;
        global $domain;
        global $log;
        global $links;
        
        
        $object_id = $object_id == 0 ? $this->object_id : $object_id;
        $object_type = $object_type == "" ? $this->object_type : $object_type;
                
        $tags_data = isset($_POST['as_values_tagy']) ? $_POST['as_values_tagy'].",".$_POST['tagy'] : false;
        $tags = explode(",",$tags_data);
        
        if($tags === false)
            return false;
        
        
        $db->query("DELETE FROM ".TABLE_STRANKY_TAGY." WHERE idObjektu = ".$object_id." AND typObjektu='".$object_type."'");
        foreach($tags AS $t)
        {
            if(is_numeric($t))
            {
                $id_tag = intval($t);
                if($id_tag <= 0) continue;
                $insert = array("idTagu" => $id_tag, "idObjektu" => $object_id, "typObjektu" => $object_type);
                $db->insert(TABLE_STRANKY_TAGY, $insert);    
            }
            else
            {
                $s = secureString(trim($t));
                if($s == "") continue;
                
                //overi jestli tag existuje
                $d = $db->query("SELECT idStranky AS id_tag 
                    FROM ".TABLE_STRANKY." 
                    WHERE nazev = '".$t."'
                        AND idDomeny = ".$domain->getId()." 
                        AND typ = 'tag'
                    LIMIT 1");
                if($db->numRows($d) > 0)
                {
                    $t = $db->getObject($d);
                    $id_tag = $t->id_tag;
                    $insert = array("idTagu" => $id_tag, "idObjektu" => $object_id, "typObjektu" => $object_type);
                    $db->insert(TABLE_STRANKY_TAGY, $insert); 
                }
                else
                {
                    $url=ModifyUrl($s);
                    for($dupl=1;$dupl<=50;$dupl++)
                    {
                        $sql=$db->query(" SELECT url 
                            FROM ".TABLE_STRANKY." 
                            WHERE idDomeny=".$domain->getId()."  
                                AND url='$url' 
                            LIMIT 1
                            ");
                            
                        if ($db->numRows($sql)>0)
                            $url=ModifyUrl($s.$dupl);
                            else
                            break;
                    }
                    
                    $insert = array(
                        "nazev" => $s, 
                        "nadpis" => $s,
                        "nazevProMenu" => $s,
                        "url" => $url, 
                        "idRodice" => $links->get_url('tagy'),
                        "idJazyka" => $this->lang, 
                        "typ" => "tag", 
                        "idDomeny" => $domain->getId(),
                        "doMenu" => 0,
                        "title" => $s
                        );
                    $db->insert(TABLE_STRANKY, $insert);
                    
                    $id_tag = $db->lastID();
                    $insert = array("idTagu" => $id_tag, "idObjektu" => $object_id, "typObjektu" => $object_type);
                    $db->insert(TABLE_STRANKY_TAGY, $insert);
                    $idTagu = $db->lastId();
                    $log->add_log('create','tag',$idTagu,$s);
                    
                      
                }
                
                
                
            }
            
            
            
        }
            
        
    }
    
    
    //vraci pocet objektu prirazenych k danemu tagu
    private function get_objects_count($tag_id, $object_type = "")
    {
        $count = 0;
        foreach($this->data AS $t)
        {
            if($t->id_tag == $tag_id && ($object_type == "" || $object_type == $t->typ_objektu))
                $count++;
        }
        
        return $count;
        
    }
    
    
    //vraci id souvisejicich objektu v poli podle zadaneho typu objektu
    //funkce porovnava shodu tagu vuci aktualnimu objektu
    //limit urcuje kolik objektu se ma vracet, resp. kolik souvisejicich clanku se bude zobrazovat v detailu clanku - pokud je 0, pak vraci vsechny souvisejici objekty
    public function get_related_objects($object_type = "", $limit = 0)
    {
        global $db;
        
        //oddelovac retezcu
        $odd = "---";
        
        //vrati id tagu aktualniho objektu
        $tagy_aktivniho_objektu = $this->get_tags();

        $tagy = array();
        $tagy_pocet_objektu = array();
        foreach($tagy_aktivniho_objektu AS $t)
        {
            $tagy[] = $t->id_tag;
            $tagy_pocet_objektu[$t->id_tag] = $this->get_objects_count($t->id_tag); //pres vsechny typy objektu
        }
        
        //setrideni tagu od nejmensiho po nejvetsi - co se tyce poctu pridelenych objektu
        //cim mene ma tag prirazenych objektu tim ma vetsi prednost pri vypisovani souvisejicich clanku
        ksort($tagy_pocet_objektu);
        
            
        //vytvori pole objektu, ktere vyhovuji zadanemu typu, napr. vsechny clanky, krome aktualniho objektu
        $objekty = $this->get_all_data("", $this->object_id, $this->object_type);
        
        //zjisti pocet shod tagu u daneho objektu (clanku)
        $souvisejici = array();
        $id_souvisejicich_tagu = array();
        
        foreach($tagy AS $ida)
        {

            foreach($objekty AS $s)
            {
                if($ida == $s->id_tag)
                {
                    if(isset($souvisejici[$s->id_objekt.$odd.$s->typ_objektu]))
                        $souvisejici[$s->id_objekt.$odd.$s->typ_objektu]++;
                        else
                        $souvisejici[$s->id_objekt.$odd.$s->typ_objektu] = 1;
                        
                    if(isset($id_souvisejicich_tagu[$s->id_objekt.$odd.$s->typ_objektu]))
                        $id_souvisejicich_tagu[$s->id_objekt.$odd.$s->typ_objektu][] = $ida;
                        else
                        $id_souvisejicich_tagu[$s->id_objekt.$odd.$s->typ_objektu] = array($ida);
                }  
            }
            
        }
        
        //setrideni souvisejicich clanku od clanku s nejvetsi shodou
        arsort($souvisejici);
               
        //setrideni clanku podle posledni zmeny a podle nazvu s ohledem na poradi relevance zjistene vyse
        if(count($souvisejici) > 0)
        {
            $nazvy = array();
            
            
            //rozdeleni id souvisejicich objektu do skupin
            $s1 = array(0); //id souvisejicich clanku
            $s2 = array(0); //id souvisejicich stranek
            $s3 = array(0); //id souvisejicich novinek
            
            foreach($souvisejici AS $ids => $s)
            {
                $data = explode($odd,$ids);
                if($data[1] == 'clanek')
                    $s1[] = intval($data[0]);
                elseif($data[1] == 'stranka')
                    $s2[] = intval($data[0]);
                elseif($data[1] == 'novinka')
                    $s3[] = intval($data[0]);
                    
            }
            
            $query = "
            
                SELECT s.idStranky AS id, s.nazev, l.datum, 'clanek' AS typ
                FROM ".TABLE_STRANKY." AS s  
                LEFT JOIN ".TABLE_LOG." AS l ON s.idStranky = l.idObjektu AND typObjektu='stranka' AND akce='create'
                WHERE s.id_stranka IN (".implode(",",$s1).")
                    and s.zobrazit=1 
                    and s.typ = 'clanek'
                    and (s.od is null or s.od<NOW()) 
                    and (s.do is null or s.do>=NOW())                    
                    ";
             
            $data = $db->query($query);
                    
            $souvisejici_pom = array();
            $nazvy = array();
            
            //echo $query;
            
            while($c = $db->getObject($data))
            {
                //pokud souvisejici objekt ma zakazano zobrazeni, nebude ulozen
                if(isset($souvisejici[$c->id.$odd.$c->typ]))
                {
                    //ulozeni zretezeni pocet shod tagu, poradi pokud je vice objektu se stejnou shodou, data a nazvu kvuli trideni
                    
                    //vytvori pole tagu objektu s id id_objektu
                    $objekty_tagy = array();
                    foreach($this->data AS $d)
                    {
                        if($d->id_objekt == $c->id && $d->typ_objektu == $c->typ)
                            $objekty_tagy[] = $d->id_tag;
                    }
                        
                        
                    //ohodnoti - secte pocet clanku, ktere obsahuji spolecne tagy objektu s id id_objektu
                    $h = 0;
                    foreach($objekty_tagy AS $id_tagu)
                    {
                        if(isset($tagy_pocet_objektu[$id_tagu]) && $tagy_pocet_objektu[$id_tagu] > 0)
                        {
                            $h += $tagy_pocet_objektu[$id_tagu];
                        }
                    }
                        
                        
                    //vypocita prumer ze vsech hodnot tagu a udela se inverze odectenim od 1000000, protoze pole se tridi od nejvetsiho po nejmensi - cim vice clanku tag obsahuje tim mensi vahu dany clanek ma pri trideni
                    $delitel = count($objekty_tagy);
                    $h = $delitel == 0 ? 0 : 1000000 - round($h / $delitel);
 
                    $kod_tagu = "x".implode("x",$id_souvisejicich_tagu[$c->id.$odd.$c->typ])."x";
                    $souvisejici_pom[$c->id.$odd.$c->typ] = $souvisejici[$c->id.$odd.$c->typ].$odd.$c->datum.$odd.$c->nazev.$odd.$kod_tagu.$odd.$h.$odd.$c->typ;
                    
                    $nazvy[$c->id.$odd.$c->typ] = $c->nazev;
                }
            }    
            
            //print_r($souvisejici_pom);
            
            arsort($souvisejici_pom);
            
            //pretrideni pole
            $souvisejici_pom = $this->related_objects_resorting($souvisejici_pom, $tagy_pocet_objektu, $odd);
            
            //print_r($souvisejici_pom);
            
            //prirazeni nazvu k souvisejicim clankum
            $souvisejici = array();
            foreach($souvisejici_pom AS $ids => $s)
            {
                $data = explode($odd,$s);
                $d = explode($odd,$ids);
                
                $souvisejici[] = array("id" => $d[0], "nazev" => $nazvy[$ids], "typ" => end($data));
            }
            
           
        }
     
        //print_r($souvisejici);
        
        //aplikace limitu, pokud je limit 0, pak jsou vraceny vsechny souvisejici objekty
        /*
        if($limit > 0)
        {
            $i = 0;
            $result = array();
            
            foreach($souvisejici AS $ids => $s)
            {
                if($i == $limit)
                    break;

                $result[] = $s; 
                $i++;
            }
        }
        else
        $result = $souvisejici;
        */
        
        return $souvisejici;
        
    }
    
    
    //nastavi sekundarni prioritu pro razeni souvisejicich clanku
    //vstupnim parametrem je specialne upravene pole pro trideni, ktere je definovano ve funkci get_related_objects
    //druhym vstupnim parametrem je pole poctu objektu jednotlivych tagu aktualniho objektu (clanku)
    private function related_objects_resorting($related, $tags_objects_count, $odd = "---")
    {
               
        //print_r($related);
               
        //zalohovani obsahu pole
        $zal2 = $related;       
        
        $tagy_kody = array();
        $max_h = 0;
        
        foreach($zal2 AS $id_objektu => $s)
        {           
            //rozdeleni retezce na pet slozek - pocet shod, datum, nazev, kod tagu (zakodovane id tagu), hodnota tagu
            $data = explode($odd, $s);
            
            $pocet_shod_tagu = intval($data[0]);
            $kod_tagu = $data[3];
            $h = $data[4];   
        
            $max_h = $max_h < $pocet_shod_tagu ? $pocet_shod_tagu : $max_h;    
            $tagy_kody[$kod_tagu] = $h;
            
        }
        
       
        //setrideni kodu tagu podle relevance, ktera je urcena poctem obsazenych objektu v tagu. Cim mensi pocet objektu v tagu, tim relevantnejsi
        arsort($tagy_kody);
        
       
        
        //print_r($tagy_kody);
        //print_r($related);

        //upravy souboru zal - seznam souvisejicich clanku
                
        for($i = 1; $i <= $max_h; $i++)
        {
            $poradi = 1000000;
            //vycleneni kodu tagu pozadovane hloubky
            $tk = array();
            foreach($tagy_kody AS $kod => $hodnota)
            {
                $kod_data = explode("x",trim($kod,"x"));
                $hloubka = count($kod_data);
                
                if($hloubka != $i)
                    continue;
                   
                   
                $tk[] = $kod; 
            }
            
            //pokud retezec jeste neobsahuje informaci o relevanci, pak je doplnena
            $je_zaznam = true;
            while($je_zaznam)
            {
                
                $je_zaznam = false;

                foreach($tk AS $kod)
                {   
                                        
                    $priradit = true;
                    foreach($zal2 AS $ido => $s2)
                    {
                        //parsovani retezce
                        $data = explode($odd, $related[$ido]);

                        //pokud uz je do retezce pridana hodnota o relevanci, pak pokracuj dal
                        if(count($data) != 6)
                        {
                            continue;
                        }                        
                        
                        $kod_tagu = $data[3];
                        $h = $data[4];
                        
                        if($kod_tagu != $kod)
                            continue;
                        
                        $je_zaznam = true;
                        
                        if($kod == $kod_tagu && $priradit)
                        {
                            
                            $related[$ido] = $data[0].$odd.$this->string_rank($poradi).$odd.$data[1].$odd.$data[2].$odd.$data[5];//.$odd.$data[3].$odd.$data[4];
                            $priradit = false;
                            $poradi--;
                        } 
   
                    }
 
                }
                
            }
     
        }
        
        //echo count($zal1)."*".count($tagy_kody)."*".count($zal2);
        
        
        //echo $poradi;
        arsort($related);
        
        //print_r($related);
        
        return $related;
        
        
        
        
        
    }
    
    //upravi poradi do pozadovaneho formatu pro razeni
    private function string_rank($rank)
    {
        $max_delka_kodu = 7;
        $s = str_repeat("0", $max_delka_kodu - strlen($rank)).$rank;
        return $s;
    }
    
    
    //vraci data o prirazeni objektu k tagum
    public function get_data()
    {
        return $this->data;
    }
    
    //vraci vsechna data - pole vsech objektu vcetne aktualniho
    //parametr vynechat objekt id urcuje, ktere id objektu se nema nacitat
    private function get_all_data($object_type = "", $vynechat_object_id = 0, $vynechat_object_type="")
    {
        global $db;
        
        $where = "";
        if($this->lang != "")
            $where .= " AND t.idJazyka='".$this->lang."'";
            
        if($object_type != '')
            $where .= " AND ts.typ_objektu='".$object_type."'";
        
        if($object_type != '')
            $where .= " AND ts.typ_objektu!='".$vynechat_object_type."'";
        
        if($vynechat_object_id > 0)
            $where .= " AND ts.id_objekt!='".$vynechat_object_id."'";
                
        $data = $db->query("SELECT ts.idTagu AS id_tag, ts.idObjektu AS id_objekt, ts.typObjektu AS typ_objektu 
            FROM ".TABLE_STRANKY_TAGY." AS ts 
            LEFT JOIN ".TABLE_STRANKY." AS t ON ts.id_tag = t.id_tag 
            WHERE t.typ = 'tag' 
                $where
            GROUP BY CONCAT(ts.idTagu,ts.idObjektu,ts.typObjektu)
            ORDER BY typObjektu DESC
            ");
        
        $objekty = array();        
        while($d = $db->getObject($data))
            $objekty[] = $d;
            
        return $objekty;
            
    }
    
    
    //vraci tagy podle parametru objektu
    //pouziva se pro vypisovani stitku v detailu stranky/clanku/novinky
    public function get_tags_by_object($object_id, $object_type)
    {
        $result = array();
        
        foreach($this->data AS $t)
        {
            if($t->typ_objektu == $object_type && $t->id_objekt == $object_id)
                $result[$t->id_tag] = array(
                    "name" => $this->tags[$t->id_tag]->nazev, 
                    "url" => $this->url.$this->tags[$t->id_tag]->url.".html",
                    "count" => $this->tags[$t->id_tag]->count
                    ); 
            
            
        }
        
        return $result;
        
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
}




?>