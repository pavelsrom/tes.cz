<?php

/**
 * @author Pavel Srom
 * @copyright 2010
 */


class C_ModuleInfo
{
	
	//indikuje zda je modul aktivni
	public $active;
	
	//systemovy nazev, ktery se prenasi jako get promenna
	public $moduleSystemName;
	
	//oficialni nazev modulu
	public $moduleName;
	
	//soubor ikony modulu
	public $icon;
	
	//identifikacni cislo modulu, na jehoz zaklade se overuje zda muze byt modul pouzit ke sprave dane domeny
	public $id;
	
	//identifikuje zda se jedna o modul, ktery vyuzivaji administratori
	public $admin_mod; 
	
	//indikuje zda jde o modul zakladni
	public $basic;
	
    //indikuje zda jde o funkci a ne o modul
    public $function;
    
    //indikuje zda je modul povolen pro danou domenu
    public $allowed;
	
    //nazev souboru
    public $file;
    
    //parametry url adresy
    public $url = array();
    
    //parametry pro nadpis h1
    private $h1 = array();
    
    //menu
    private $menu = array();
    
    //prvni stranka - hlavni stranka modulu
    public $first_page = ""; 
    
    
    //konstruktor modulu
	public function __construct($inicialize = null)
    {
        global $domain;
		global $db;
		global $systemMessage;
		global $login_obj;
        global $links;
        global $log;
        global $lang;
        global $object_access;
                       
		if(!$this->Inicialize($inicialize)) return;

        $maska_souboru = DIRNAME_MODULES."module.".$this->moduleSystemName."/action.*";
        $files = glob($maska_souboru);
        foreach($files AS $f)
            include($f);
        
        
	}
		
        
    //funkce inicializuje informace o modulu a stara se o bezpecnost modulu
	private function Inicialize($inicialize=NULL)
    {
        global $login_obj;
        global $db;
        
        
        
		//inicializace promennych modulu
		if(!$inicialize) return false;
		
		$this->moduleName = $inicialize['name']; 
		$this->moduleSystemName = $inicialize['systemName'];
		$this->id = $inicialize['moduleId'];
		$this->icon = $inicialize['icon'];
		$this->admin_mod = $inicialize['adminModule'];
		$this->active = $inicialize['active'];
		$this->basic = $inicialize['basic'];
		$this->function = $inicialize['function'];
        $this->allowed = $inicialize['allowed'];
        $this->first_page = $inicialize['firstPage'];
        
        if(!$this->active || !$this->allowed || $this->function) return false;
        
        if(!$this->construct_security()) return false;
        
        $this->url['module'] = isset($_GET['module']) ? strtolower($_GET['module']) : "";
        $this->url['page'] = isset($_GET['page']) ? strtolower($_GET['page']) : "";
        $this->url['action'] = isset($_GET['action']) ? strtolower($_GET['action']) : "";
        $this->url['id_action'] = isset($_GET['id_action']) ? intval($_GET['id_action']) : 0;
        $this->url['id_page'] = isset($_GET['id_page']) ? intval($_GET['id_page']) : 0;
        $this->url['value'] = isset($_GET['value']) ? $_GET['value'] : "";
        $this->url['weblang'] = isset($_GET['weblang']) ? intval($_GET['weblang']) : 0;
        
        $functions_script = DIRNAME_MODULES."module.".$this->moduleSystemName."/functions.module.".$this->moduleSystemName.".php";
        if(file_exists($functions_script))
            include($functions_script);
        
        $config_script = DIRNAME_MODULES."module.".$this->moduleSystemName."/config.module.".$this->moduleSystemName.".php";
        if(file_exists($config_script))
            include($config_script);
        
        //inicializace nadpisu
        $this->h1 = $module_h1;
        foreach($module_h1 AS $idh => $h1)
        {
            $this->h1[$idh]['page'] = isset($h1['page']) ? strtolower($h1['page']) : "";
            $this->h1[$idh]['action'] = isset($h1['action']) ? strtolower($h1['action']) : "";
            $this->h1[$idh]['verify_id_page'] = isset($h1['verify_id_page']) ? intval($h1['verify_id_page']) : 0;
            $this->h1[$idh]['verify_id_action'] = isset($h1['verify_id_action']) ? intval($h1['verify_id_action']) : 0;
            $this->h1[$idh]['text'] = isset($h1['text']) ? help($h1['text'],true) : "";   
        }
        
        
        //inicializace menu
        $this->menu = $module_menu;
        foreach($module_menu AS $idm => $m)
        {
            $this->menu[$idm]['page'] = isset($m['page']) ? strtolower($m['page']) : "";
            $this->menu[$idm]['action'] = isset($m['action']) ? strtolower($m['action']) : "";
            $this->menu[$idm]['active'] = intval($this->url['page'] == $this->menu[$idm]['page']);
            $this->menu[$idm]['url'] = $this->get_link($this->menu[$idm]['page'], 0, $this->menu[$idm]['action'], 0);   
        }
        
        return true;
	}  
    
    //vraci nadpis h1
    public function getH1()
    {
        $h1 = "";
        $text = "";

        foreach($this->h1 AS $h1)
        {
            if($this->url['page'] == $h1['page'] && 
                $this->url['action'] == $h1['action'] &&
                (($h1['verify_id_page'] && $this->get_id_page() > 0) || (!$h1['verify_id_page'] && $this->get_id_page() == 0)) &&
                (($h1['verify_id_action'] && $this->get_id_action() > 0) || (!$h1['verify_id_action'] && $this->get_id_action() == 0))
                )
                return array("h1" => $h1['name'], "text" => $h1['text']);   
        }
            
        return false;
    }
    
    
        
        
    //funkce generuje html kod modulu    
    public function Create()
    {
        global $login_obj;
		global $domain;
		global $db;
		global $systemMessage;
        global $links;
        global $modules;
        global $lang;
        global $log;
        global $object_access;
          
        
        if(!$this->create_security()) return false;
         
        
        $maska_souboru = DIRNAME_MODULES."module.".$this->moduleSystemName."/show.*";
        $files = glob($maska_souboru);
        $indik = false;
        foreach($files AS $f)
            //if(!$indik)
                $indik = include($f);
            
        //if(!$indik)
        //    include("layout/cms_404.php");
            
        return true;
        
    }  
    
    //bezpecnostni funkce pro konstruktor
    private function construct_security()
    {
        global $login_obj;
		global $domain;
        
        if($this->admin_mod)
        {
            if(!$login_obj->UserPrivilege("admin") && !$login_obj->UserPrivilege("superadmin")) return false;
        }
        else
        {
            if(!$login_obj->logged()) return false;
            if(!$domain->exists()) return false;
            if(!$domain->UserDomains()) return false;
        }
        

        return true;
    }
    
    //bezpecnostni funkce pro funkci create
    private function create_security()
    {
        global $login_obj;
		global $domain;
        
        if($this->admin_mod)
        {
            if(!$login_obj->UserPrivilege("admin") && !$login_obj->UserPrivilege('superadmin')) return false;
        }
        else
        {
            if(!$this->active || !$this->allowed) return false;	
    		if(!$login_obj->logged()) return false;
    		if(!$domain->UserDomains() && (!$login_obj->UserPrivilege('admin') && !$login_obj->UserPrivilege('superadmin'))) return false;
        }
        
        return true;
    } 
    
    
    //vraci url adresu korespondujici s aktivnim modulem a aktivni strankou modulu
    public function get_link($page = "", $id_page = 0, $action = "", $id_action = 0,$value="",$weblang=0)
    {
        return get_link($this->moduleSystemName, $page, $id_page, $action, $id_action, $value,$weblang);
        
    }   
    
    //vraci menu v asociativnim poli
    public function get_menu()
    {
        return $this->menu;
    }
    
    
    //overuje zda se muze zobrazit stranka modulu podle dodanych parametru
    //id_page_verify udava zda se bude overovat hodnota vetsi nez 0 u id stranky modulu, podobne u id_action_verify
    public function is_access($page, $action = "", $id_page_verify = false, $id_action_verify = false, $privileges = "", $value_verify=false)
    {
        global $login_obj;
        
        $privileges = (array) $privileges;
        
        if($page != $this->url['page'])
            return false;
        if($action != "" && $action != $this->url['action'])
            return false;
        if($action == "" && $this->url['action'] != "")
            return false;
        if(($id_page_verify == 0 && $this->url['id_page'] > 0) || ($id_page_verify > 0 && $this->url['id_page'] == 0))
            return false;
        if(($id_action_verify == 0 && $this->url['id_action'] > 0) || ($id_action_verify > 0 && $this->url['id_action'] == 0))
            return false;
        if($value_verify && $this->get_value()=='')
            return false;
         
        if(count($privileges) > 0)
        {
            foreach($privileges AS $p)
                if($p != "" && !$login_obj->UserPrivilege($p)) 
                    return false;
        }
        
               
        return true;
            
    } 
    
    //funkce vraci post hodnotu predanou modulu
    public function get_post( $key , $default_value = "")
    {
        return $this->is_post($key) ? trim($_POST[$key]) : $default_value;
    }
    
    //funkce vraci post hodnotu predanou modulu osetrenou
    public function get_secure_post( $key , $strip_tags = false, $default_value = "", $we = false)
    {
        $string = $this->is_post($key) ? htmlspecialchars($_POST[$key], ENT_QUOTES) : $default_value;
        $string = $strip_tags ? strip_tags($string) : $string;
        return trim($string);
    }
    
    //funkce vraci post hodnotu konvertovanou na int
    public function get_int_post($key, $default_value = 0)
    {
        return $this->is_post($key) ? intval($_POST[$key]) : $default_value;
    }
    
    //overi zda existuje post promenna s danym klicem
    public function is_post($key)
    {
        return isset($_POST[$key]);
    }
    
   
    //vraci zakladni url modulu
    public function get_root_url()
    {
        return RELATIVE_PATH.ADMIN_SCRIPT."/".$this->moduleSystemName;
    }
    
    //vraci hodnotu url adresy modulu nebo asociativni pole s informacemi o url adrese
    public function get_url($key = '')
    {
        return $key == '' ? $this->url : (isset($this->url[$key]) ? $this->url[$key] : "");
    }
    
    //vraci id akce z url adresy modulu
    public function get_id_action()
    {
        return $this->url['id_action'];
    }
    
    //vraci id akce z url adresy modulu
    public function get_action()
    {
        return $this->url['action'];
    }
    
    //vraci id page z url adresy modulu
    public function get_id_page()
    {
        return $this->url['id_page'];
    }
    
    //vraci id akce z url adresy modulu
    public function get_page()
    {
        return isset($this->url['page']) ? $this->url['page'] : "";
    }
    
    //vraci hodnotu predanou v url get
    public function get_value()
    {
        return $this->url['value'];
    }
    
    //vraci url hlavni stranky modulu
    public function get_first_page()
    {
        return $this->get_root_url()."?weblang=".WEB_LANG_ID."&page=".$this->first_page;
    }
    
    //vraci id jazyka domeny
    public function get_web_lang()
    {
        return $this->url['weblang'];
    }
    
    //vraci id modulu
    public function get_id()
    {
        return $this->id;
    }
    
    public function get_module_name()
    {
        return $this->moduleName;
    }
    
    public function get_module_system_name()
    {
        return $this->moduleSystemName;
    }
    
    

}
	
	

class C_Modules{
	
	//pole objektu modulu
	public $modul;
	
	//vsechny moduly
	public $allModules;
	
    //povolene moduly pro uzivatele
    private $allowed_modules_for_user = array();
    
    //povolene moduly pro domenu
    private $allowed_modules_for_domain = array();
    
    
	//konstruktor tridy, vstupnimi parametry je retezec id hodnot modulu, ktere budou zapnuty, hodnota adm zapina ci vypina zobrazeni administracnich modulu. Pokud je hodnota adm true, pak se zobrazi i administracni moduly i kdyz dana domena nema zapnuty tyto moduly
	 
	public function __construct($ids, $adm=false){
		global $db;
		global $login_obj;
        global $log;
		
		$this->modul = array();
		$this->allModules = array();
		$set = "";
		$ids_dom = $ids;
		$ids_user = $login_obj->getModules();

		
		foreach($ids_user AS $mu)
			if(in_array($mu, $ids_dom)) 
				if($set=='') $set = $mu; else $set .= ",".$mu;
		
        $this->allowed_modules_for_domain = $ids_dom;
        $this->allowed_modules_for_user = $ids_user;
		
		if($set == '') $set = 0;
		
        $set_dom = count($ids_dom) == 0 ? 0 : implode(',',$ids_dom);
       
		if($adm)
			$data = $db->Query("SELECT * , 
				IF(zobrazit=1 AND ((idModulu IN (".$set.") OR (idModulu IN (".$set_dom.") AND podmodul=1)) OR admin=1), '1', '0' ) AS aktivni
				FROM ".TABLE_CMS_MODULY."
				ORDER BY admin, priorita");
			else
			$data = $db->Query("SELECT * , 
				IF(zobrazit=1 AND (idModulu IN (".$set.") OR (idModulu IN (".$set_dom.") AND podmodul=1)) , '1', '0' ) AS aktivni
				FROM ".TABLE_CMS_MODULY."
				ORDER BY admin, priorita");
		
		
		//nacteni modulu z databaze, includovani souboru modulu a vytvoreni instance modulu
           
        //definovani konstant   
        while($mod = $db->getAssoc($data))
        {
            if($mod['aktivni']==1)
				define($mod['konstanta'], true);
				else
				define($mod['konstanta'], false);
        }
        
        
  
        $db->dataSeek($data);
           
        
		while($mod = $db->getAssoc($data)){		
            
			$this->allModules[$mod['idModulu']] = defined($mod['nazev']) ? constant($mod['nazev']) : "";
			
			
				
				$id = $mod['idModulu'];		
				$ini = array();
				$ini['name'] = defined($mod['nazev']) ? constant($mod['nazev']) : "";
				$ini['systemName'] = $mod['systemovyNazev'];
				$ini['icon'] = $mod['ikona'];
				$ini['basic'] = $mod['zakladni'];
                $ini['allowed'] = $mod['aktivni'];
                $ini['function'] = $mod['podmodul'];
				$ini['moduleId'] = $mod['idModulu'];
				$ini['adminModule'] = $mod['admin'];
                $ini['firstPage'] = $mod['hlavniStranka'];
				
				if(isset($_GET['module']) && $_GET['module'] == $ini['systemName'])
                {
					$ini['active'] = 1;
                    $log->set_module_id($mod['idModulu']);
                }
					else
					$ini['active'] = 0;	
					
				
            $this->modul[$mod['idModulu']] = new C_ModuleInfo($ini);
                
            
			}
			
		$db->free($data);

		}
		
	//vrati systemovy nazev aktivniho modulu
	public function GetSystemNameOfActiveModule(){
		$systemName = "";
		foreach($this->modul AS $m){
			if($m->active) 
				$systemName = $m->moduleSystemName;
			}
            
        return $systemName;

		}
	
	//vraci referenci na aktivni modul	
	public function GetActiveModule()
    {
		
		foreach($this->modul AS $m){
			if($m->active) 
				return $m;
			}
            
        return null;
	}
        
    //vraci cestu k ikone modulu podle zadaneho systemoveho jmena
	public function GetIconModuleBySystemName($systemName){
		$module = null;
		foreach($this->modul AS $m){
			if($m->moduleSystemName==$systemName || stristr($m->moduleSystemName, $systemName)) 
				return DIRNAME_MODULES_ICON.$m->icon;
			}
        return '';
		}
		
	//vrati pocet dostupnych modulu, ktere byly prirazeni danne domene
	public function GetCountModules(){
		return count($this->modul);
	}
	
	//vrati systemovy nazev prvniho nalezeneho uzivatelskeho modulu
	public function GetFirstUserModuleSystemName(){
		if($this->GetCountModules()==0) return '';
		foreach($this->modul AS $m){
			if($m->admin_mod==0 && $m->allowed == 1) 
				return $m->moduleSystemName;
			}
		return '';
	}
	
	//vrati systemovy nazev prvniho nalezeneho administracniho modulu
	public function GetFirstAdminModuleSystemName(){
		if($this->GetCountModules()==0) return '';
		foreach($this->modul AS $m){
			if($m->admin_mod==1 && $m->allowed == 1) 
				return $m->moduleSystemName;
		}
		return '';
		
	}
    
    //vraci url prvniho modulu po prihlaseni
    public function GetFirstUrl()
    {

        if($this->GetCountModules()==0) return false;
		foreach($this->modul AS $m){
			if($m->allowed == 1 && $m->function == 0) 
				return $m->get_first_page();
		}
		return '';
    }
	
	//vraci true pokud je dany modul prirazen k domene
	public function isModule($moduleSystemName){
		
		foreach($this->modul AS $m)
			if($m['systemName']==$moduleSystemName) return true;
		return false;		
		}
		
	public function IsBasic($id){
		if($this->GetCountModules()==0) return 0;
		foreach($this->modul AS $m){
			if($m->id == $id) 
				return $m->basic;
			}
		return 0;
		}
        
    //vraci objekt modulu podle id
    public function get_module($id)
    {
        return isset($this->modul[$id]) ? $this->modul[$id] : null;
    }
    
    //vraci pole vsech modulu
    public function get_all_modules()
    {
        return $this->modul;
    }
    
    //vraci pole vsech povolenych modulu
    public function get_allowed_modules()
    {
        $allowed_modules = array();
        foreach($this->modul AS $m)
            if($m->allowed)
                $allowed_modules[] = $m;
                
        return $allowed_modules;
    }
    
    //zjisti zda je modul pro danou domenu ci uzivatele povolen
    public function is_allowed($id, $for = '')
    {
        if($for == 'domain')
            return in_array($id, $this->allowed_modules_for_domain);
        elseif($for == 'user')
            return in_array($id, $this->allowed_modules_for_user);
        else
            return in_array($id, $this->allowed_modules_for_user) && in_array($id, $this->allowed_modules_for_domain);
    }
    
    //zjisti zda modul s danym id je funkce nebo ne
    public function is_function($id)
    {
        return (isset($this->modul[$id]) && $this->modul[$id]->function == 1); 
    }
	
}


?>