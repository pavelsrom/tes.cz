<?php

/**
 * @author Pavel �rom
 * @copyright 2017
 */

class C_Patination{
    
    //aktualni stranka
    private $current_page = 1;
    
    //pocet polozek na jedne strance
    private $number_of_items_on_page;
    
    //pocet stranek
    private $number_of_pages = 1;
    
    //pocet vsech polozek
    private $number_of_items = 0;
    
    //okoli stranky, ktere bude nahrazeno teckami
    private $around_page = 4;
    
    //kotva na konci url
    private $url_tail = "";
    
    //nazev parametru v GET
    private $get_param_name = "stranka";
    
    
    //konstruktor
    public function __construct()
    {
        $this->current_page = get_int_request( $this->get_param_name, 1);
        $this->current_page = $this->current_page < 1 ? 1 : $this->current_page;
    }
    
    
    //vraci cislo aktualni stranky
    public function get_current_page()
    {
        return $this->current_page;
    }
    
    //nastavi pocet vsech polozek
    public function set_around_page($around_page)
    {
        $this->around_page = $around_page;
        $this->set_number_of_pages();
        
        return $this;
    }
    
    //nastavi pocet vsech polozek
    public function set_number_items($number_items)
    {
        $this->number_of_items = intval($number_items);
        $this->set_number_of_pages();
        
        return $this;
    }
    
    //vraci pocet vsech polozek
    public function get_number_items()
    {
        return $this->number_of_items;
        
    }        
    
    //nastavi pocet polozek na stranku
    public function set_number_items_on_page($number_items)
    {
        $this->number_of_items_on_page = $number_items;
        $this->set_number_of_pages();
            
        return $this;
    }
    
    private function set_number_of_pages()
    {
        if($this->number_of_items_on_page > 0)
            $this->number_of_pages = ceil($this->number_of_items / $this->number_of_items_on_page);
        
    }
    
    //nastavi konec url, kotvu
    public function set_url_tail($url_tail = "")
    {
        $this->url_tail = $url_tail;
            
        $this->set_number_of_pages();
            
        return $this;
    }
    
    //vraci url nasledujici stranky
    public function get_next_url()
    {
        
        if($this->number_of_pages <= $this->current_page || $this->number_of_pages == 1)
            return false;
        
                
        $url = PROTOKOL.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
        $url = explode("?",$url);
        $url = $url[0];
        
        $url .= "?".$this->get_param_name."=".($this->current_page+1);
        return $url;
        
    }
    
    //vraci url predchozi stranky
    public function get_prev_url()
    {
        if($this->current_page <= 1 || $this->number_of_pages == 1)
            return false;
        
                
        $url = PROTOKOL.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
        $url = explode("?",$url);
        $url = $url[0];
        
        if($this->current_page > 2)
            $url .= "?".$this->get_param_name."=".($this->current_page-1);
        
        return $url;
    }
    
    
    
    public function get_html($class = "")
    {
        
        global $page;
        global $links;
    	
    	$stranka = $this->current_page;
        $pocet_stranek = $this->number_of_pages;
        
   
                                
    	$tecky_leve = false;
    	$tecky_prave = false;
    	
    	$html = "";
    	
        $konec_url = $this->url_tail;
        $url = $links->get_url($page->get_id());
        
    	if($pocet_stranek>1)
        {
            $html .= '
            <div class="paginator">
                    <ul>';
            
            if($pocet_stranek > 3)
                if($stranka == 1)
    		        $html .= "<li class='prev'><span>".TPREDCHOZI."</span></li>";
    			    else		
    			    $html .= "<li class='prev'><a href='".$url."?".$this->get_param_name."=".($stranka-1).$konec_url."'>".TPREDCHOZI."</a></li>";
            
            
    		for($j=1; $j<=$pocet_stranek; $j++){
                $html .= '<li class="pagination-item">';
    			if($j==1){
    				if($j==$stranka)
    					$html .= "<span>".$j."</span>";
    					else
    					{
    					$html .= "<a href='".$url.$konec_url."'>".$j."</a>";
    					
    					}
    				continue;
    				}
    				
    			if($j==$pocet_stranek){
    				if($j==$stranka)
    					$html .= "<span>".$j."</span>";
    					else
    					$html .= "<a href='".$url."?".$this->get_param_name."=".$j.$konec_url."'>".$j."</a>";
    					
    				continue;
    				}
    				
    			if($stranka>($j + $this->around_page)) {
    				if(!$tecky_leve) $html .= "...";
    				$tecky_leve = true;
    				continue;
    				}
    				
    			if($stranka<($j - $this->around_page)){
    				if(!$tecky_prave) $html .= "...";
    				$tecky_prave = true;
    				continue;
    				}
    
    			if($j==$stranka)
    				 $html .= "<span>".$j."</span>";
    				 else
                     $html .= "<a  href='".$url."?".$this->get_param_name."=".$j.$konec_url."'>".$j."</a>";
                    
                     
                $html .= '</li>';
    			}
    		
            if($pocet_stranek > 3)
                if($stranka == $pocet_stranek)
    		        $html .= "<li class='next'><span>".TNASLEDUJICI."</span></li>";
    			    else		
    			    $html .= "<li class='next'><a href='".$url."?".$this->get_param_name."=".($stranka+1).$konec_url."'>".TNASLEDUJICI."</a></li>";
            

            $html .= "</ul>
            </div>";

        }
	
	   return $html;
    
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}

?>