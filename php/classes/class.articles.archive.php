<?php

/**
 * @author Pavel Srom
 * @copyright 2010
 */



class C_ArticlesArchive{
    
    public $items;
    
    public $exists;
    private $aktualni_stranka;
    private $celkovy_pocet_polozek;
    
    public function __construct($typ='stranka'){
        global $db;
        global $domain;
        global $links;
        
        
        $this->exists = false;
        $this->aktualni_stranka = isset($_GET['stranka'])?intval($_GET['stranka']):1;
        $this->celkovy_pocet_polozek = 0;
        $this->items = array();
        
        if($typ=='stranka'){
            $data = $db->Query("SELECT * FROM ".TABLE_STRANKY." 
                WHERE typ='clanek' 
                    AND zobrazit=1
                    AND idDomeny=".$domain->getId()."
       				AND (do < CURDATE() AND do IS NOT NULL)
                ORDER BY datum DESC
                LIMIT ".(($this->aktualni_stranka - 1) * intval(POCET_CLANKU_NA_STRANKU)).",".intval(POCET_CLANKU_NA_STRANKU)."");
            }
       elseif($typ=='panel'){
            
            $data = $db->Query("SELECT DATE_FORMAT(datum,'%m') AS mesic, DATE_FORMAT(datum,'%Y') AS rok, COUNT(idStranky) AS pocet 
                FROM ".TABLE_STRANKY." 
                WHERE typ='clanek'
                    AND idDomeny=".$domain->getId()."
                    AND zobrazit=1
       				AND (do < CURDATE() AND do IS NOT NULL) 
                GROUP BY DATE_FORMAT(datum,'%y%m')
                ORDER BY DATE_FORMAT(datum,'%y%m') DESC
                ");
           
           while($d = $db->getAssoc($data))
                $this->items[] = $d;     
    
           } 
        
       $this->exists = true; 
        
        
        
    }
    
    private function getQueryArchive(){
        global $db;
        global $domain;
        
        $vyraz = isset($_POST['vyraz'])?$db->secureString(strip_tags($_POST['vyraz'])):'';
        $od = isset($_POST['datum_od'])?GetUniDate($_POST['datum_od']):'';
        $do = isset($_POST['datum_do'])?GetUniDate($_POST['datum_do']):'';
        $rubrika = isset($_POST['rubrika'])?abs(intval($_POST['rubrika'])):0;
        
        //vyhledavani retezce
        $like = "";
        if($vyraz !=''){
            $like = "AND (c.nadpis LIKE '%".$vyraz."%' OR c.perexBezHtml LIKE '%".$vyraz."%' OR c.obsahBezHtml LIKE '%".$vyraz."%')";
            
        }
        
        //filtrovani podle data
        $date_range = "";
        if($od != '' || $do != ''){
            
            if($od != '')
                $date_range .= " AND c.datum >= '".$od."' ";
            if($do != '')
                $date_range .= " AND c.datum <= '".$do."' ";
        }
        
        //filtrovani podle rubriky
        $like_rubrika = "";
        if($rubrika > 0){
            $like_rubrika = " AND r.idStranky=".$rubrika;
        }
        
        $query = "SELECT r.nadpis AS rubrika, c.nadpis AS titulek, c.url AS url,c.idStranky AS idClanku, 
            DATE_FORMAT(c.datum, '%d.%m.%Y') AS datum, r.autor,j.jazyk
            IF(c.obrazek is null,'',c.obrazek) AS obrazek  
            FROM ".TABLE_STRANKY." AS r 
            LEFT JOIN ".TABLE_JAZYKY." AS j ON r.idJazyka = j.idJazyka
            RIGHT JOIN ".TABLE_STRANKY." AS c ON r.idStranky=c.idRodice
            WHERE r.zobrazit=1
                AND c.zobrazit=1 
                AND c.idDomeny=".$domain->getId()."
                AND r.idDomeny=".$domain->getId()."
                AND c.archivovat=1
                ".$like."
                ".$date_range."
                ".$like_rubrika."
            GROUP BY c.idStranky
            ORDER BY c.datum
            LIMIT 150
            ";
        
        return $query;
    }
    
    public function getArticlesArchiveOnPage(){
        
        global $links;
        global $db;
        global $domain;
        
        $html = "";
        
        $rubriky = array();
        $rubriky[0] = "--- ".TVSECHNY_RUBRIKY." ---";
        foreach($links->data AS $id => $r)
            if($r['typ']=='clanky')
                $rubriky[$id] = $r['nadpis'];
             
        
        
        
        $html .= '<script type="text/javascript">
		
        
        	function date_validate() {
        	   var reg = "^(\\d){1,2}\\.(\\d){1,2}\\.(\\d){2,4}$";
                var re = new RegExp(reg);
               
        		var dod = document.getElementById("datum_od").value.toString();
        		var ddo = document.getElementById("datum_do").value.toString();
                
        		if ( $.trim(dod) == "" && $.trim(ddo) == "" )
        		{
        			return true;
        		}
        		else if ( $.trim(dod) == "" && $.trim(ddo) != "" )
        		{

                  if(re.test(ddo))
                    return true;
                    else
                    return false;

        		}
        		else if ( $.trim(ddo) == "" && $.trim(dod) != "" )
        		{
                  if(re.test(dod))
        			 return true;
                     else
                     return false;
        		}
        		else if ( $.trim(ddo) != "" && $.trim(dod) != "" )
        		{
        			
                  if(re.test(ddo) && re.test(dod))
        			 return true;
                     else
                     return false;
        		}
        		return false;
        	}
   
        
        
        
		$(function(){
                        
            var nCloneTh = document.createElement( "th" );
        	var nCloneTd = document.createElement( "td" );
        	nCloneTd.innerHTML = "<img src=\'img/details_open.png\' alt=\'detail\'/>";
        	nCloneTd.className = "center";
        	
        	$("#directory-archiv-clanku thead tr").each( function () {
        		this.insertBefore( nCloneTh, this.childNodes[0] );
        	} );
        	
        	$("#directory-archiv-clanku tbody tr").each( function () {
        		this.insertBefore( nCloneTd.cloneNode( true ), this.childNodes[0] );
        	} );

            $(document).on("click", "#directory-archiv-clanku tbody td img.ikona-detail",function () {
        		var nTr = this.parentNode.parentNode;
        		if ( this.src.match("icon_minus") )
        		{

        			this.src = "img/icon_plus.gif";
        			oTable.fnClose( nTr );
        		}
        		else
        		{
        		    var pol_id = $(this).attr(\'id\');
                    var id_polozky = parseInt(pol_id.substr(1,pol_id.length));
                    if(id_polozky <= 0) {
                        return;
                        }
        			
                    
                    var aData = oTable.fnGetData( nTr );
                this.src = "img/icon_minus.gif";    
                $.post("'.AJAX_GATEWAY.'articles.archive.perex",{idPolozky: id_polozky},function(data){
                    if(data==\'\') data = "Bez detailních informací";
                	var sOut = "<table cellpadding=\'5\' cellspacing=\'0\' border=\'0\' style=\'padding-left:0px;\'>";
                	sOut += "<tr><td>" + data + "</td></tr>";
                	sOut += "</table>";

        			oTable.fnOpen( nTr, sOut, "details" );
               
                    })
        		}
        	});
            
			var oTable = $("#directory-archiv-clanku").dataTable({
				"bProcessing": true,
				"bServerSide": true,
				"bStateSave": true,
				"sPaginationType": "full_numbers",
				"sAjaxSource": "'.AJAX_GATEWAY.'articles.archive",
                "aoColumns": [null, null, null, null, null],
                "fnServerData": function ( sSource, aoData, fnCallback ) {
                       aoData.push( { "name": "vyraz", "value": $("#vyraz").val()} );
                       aoData.push( { "name": "datum_od", "value": $("#datum_od").val()} );
                       aoData.push( { "name": "datum_do", "value": $("#datum_do").val()} );
                       aoData.push( { "name": "rubrika", "value": $("#rubrika option:selected").val()} );
                       $.getJSON( sSource, aoData, function (json) { fnCallback(json); } );
                       }
				});
            
            $("#vyraz").bind(\'keyup\', function() {
                oTable.fnDraw(); 
                });    
            $("#datum_od").bind(\'keyup change\', function() {
                if(date_validate())
                    oTable.fnDraw(); 
                });
        	$("#datum_do").bind(\'keyup change\', function() {
        	   if(date_validate())
                    oTable.fnDraw();
        	   });
            $("#rubrika").change( function() {
                if(date_validate())
                    oTable.fnDraw();
                });

       
       
			})
		
		</script>';
        
        
        
        $html .= "<form method='post' action=''>";
        $html .= '<table border="0" cellspacing="5" cellpadding="5">
                <tr>
					<td>'.THLEDANY_VYRAZ.':</td>

					<td><input type="text" id="vyraz" name="vyraz" class="input"  value="'.(isset($_POST['vyraz'])?secureString($_POST['vyraz']):'').'"/></td>
				</tr>
				<tr>
					<td>'.TZOBRAZIT_CLANKY_OD.':</td>

					<td><input type="text" id="datum_od" name="datum_od" class="datepicker input" value="'.(isset($_POST['datum_od'])?secureString($_POST['datum_od']):'').'" /></td>
				</tr>
				<tr>
					<td>'.TZOBRAZIT_CLANKY_DO.':</td>
					<td><input type="text" id="datum_do" name="datum_do" class="datepicker input" value="'.(isset($_POST['datum_do'])?secureString($_POST['datum_do']):'').'" /></td>
				</tr>
                <tr>
					<td>'.TRUBRIKA.':</td>
					<td>'.ShowSelectBoxToString('rubrika',$rubriky,(isset($_POST['rubrika'])?intval($_POST['rubrika']):0),false,"",'rubrika').'</td>
				</tr>
			</table>';
        $html .= "<noscript><input type='submit' name='btnFiltrovat' value='".THLEDAT."' class='button submit'/></noscript>";
        $html .= "</form>";

        $data = $db->Query($this->getQueryArchive());
        

        $html .= "<table class='table directory' id='directory-archiv-clanku' style='width: 100%'>";
		$html .= "<thead><tr>";
        $html .= "<th>".TDATUM."</th>";
        $html .= "<th>".TAUTOR."</th>";
        $html .= "<th>".TRUBRIKA."</th>";
        $html .= "<th>".TCLANEK."</th>";

		$html .= "</tr></thead>";
		$html .= "<tbody>";
        while($c = $db->getAssoc($data))
            $html .= "<tr><td>".$c['datum']."</td><td>".$c['autor']."</td><td>".$c['rubrika']."</td><td><a href='".urlPage($c['url'],$c['idClanku'],$c['jazyk'])."' title='".TCIST_VICE."'>".$c['titulek']."</a></td></tr>";
            
		$html .= "</tbody>";
		$html .= "<tfoot></tfoot>";
		$html .= "</table>";
        
        
        return $html;
        
        
        
    }
    
    public function getArticlesArchiveBlock(){
        
        
        
        
        
        
    }
    
    
    
    
    
    
    
}


?>