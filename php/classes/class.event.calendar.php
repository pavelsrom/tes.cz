<?php

/**
 * @author Pavel Srom
 * @copyright 2013
 */

class C_EventCalendar
{
    
    //typ vypisu
    private $typ = ""; 
    
    //id typu akce nebo id akce, v pripade vypisu typu
    private $id = 0;
    
    //terminy
    private $items = array();   
    
    //typy akci
    private $typy_akci = array();
        
    //obsahuje data pro detail akce
    private $event_detail = array();
    
    //aktualni mesic kalendare akci
    private $month = false;
    
    private $year = false;
    
    private $filter_settings = array();
    
    //kontruktor kalendare akci, nacita data o akcich
    public function __construct($typ = 'kalendar-akci', $id = 0, $datum_od="", $datum_do="")
    {
        global $db;
        global $domain;
        global $links;
        global $pagination;
        
        $this->id = $id;
        $this->typ = $typ;
        
        $where = "";
        
        if($typ != 'box')
        {
            $datum_od = get_request("od");
            $datum_do = get_request("do");
                        
            $this->filter_settings["od"] = $datum_od;
            $this->filter_settings["do"] = $datum_do;
            
            $datum_od = get_sql_date($datum_od);
            $datum_do = get_sql_date($datum_do);
            
            
            $datum_od .= $datum_od != '' ? " 00:00:00" : "";
            $datum_do .= $datum_do != '' ? " 23:59:59" : "";    
            /*
            if($datum_od != '')
                $where .= " AND k.od <= '".$datum_od."'";
            
            if($datum_do != '')
                $where .= " AND (k.do IS NOT NULL AND k.do != '0000-00-00 00:00:00' AND k.do <= '".$datum_do."' OR ";
            */
            
            if($datum_od != '' && $datum_do != '')    
                $where .= " AND (
                    (k.od >= '".$datum_od."' AND k.od <= '".$datum_do."') OR 
                    (k.od < '".$datum_od."' AND k.do IS NOT NULL AND k.do != '0000-00-00 00:00:00' AND k.do >= '".$datum_od."'))";
            elseif($datum_od != '')
            {
                $where .= " AND (
                    (k.od >= '".$datum_od."') OR 
                    (k.od < '".$datum_od."' AND k.do IS NOT NULL AND k.do != '0000-00-00 00:00:00' AND k.do >= '".$datum_od."'))";
            }
            elseif($datum_do != '')
                $where .= " AND (k.od <= '".$datum_do."')";
                    
            $this->filter_settings["where"] = $where;        
                    
                    
                
        }
        
        //pro nacteni kalendare akci potrebujeme ziskat data o vsech akcich poradanych v poslednich dvou mesicich 
        //a vsech naslednych akci
        if($typ == 'kalendar-akci')
        {
            $limit = "LIMIT ".(($pagination->get_current_page() - 1) * intval(POCET_AKCI_NA_STRANKU).",".intval(POCET_AKCI_NA_STRANKU));
            $where .= "";
        }
        elseif($typ == "kalendar-typ" && $id > 0)
            {
            $limit = "LIMIT ".(($pagination->get_current_page() - 1) * intval(POCET_AKCI_NA_STRANKU).",".intval(POCET_AKCI_NA_STRANKU));            
            $where .= " AND s.typ='akce' AND r.idStranky=".$id;
            }
        elseif($typ == "akce" && $id > 0)
        {
            $where .= " AND s.typ='akce' AND k.idAkce=".$id;
            $limit = "";
        }
        elseif($typ == "box")
        {
            $rok = get_int_get("rok", date('Y'));
            $mesic = get_int_get("mesic", date('m'));
            $den = 1;
            
            $this->month = $mesic;
            $this->year = $rok;
            
            $datum_od = $rok."-".$mesic."-".$den;
            $datum_do = date("Y-m-d 23:59:59", strtotime("last day of this month", mktime(0,0,0,$mesic,$den,$rok)));        
            
            $where .= " AND (
                (k.od >= '".$datum_od."' AND k.od <= '".$datum_do."') OR 
                (k.od < '".$datum_od."' AND k.do IS NOT NULL AND k.do != '0000-00-00 00:00:00' AND k.do >= '".$datum_od."'))";
            $limit = "";
        }
        
                
            $data = $db->query("SELECT SQL_CALC_FOUND_ROWS s.nazev, s.perex, s.obrazek, s.misto, r.barva,
                    s.idStranky, k.idTerminu, r.idStranky AS idKategorie, r.nazev AS kategorie, k.od, k.do,
                    IF(k.od IS NOT NULL AND k.od != '', DATE_FORMAT(k.od,'%d.%m.%Y'),'') AS datum_od,
                    IF(k.od IS NOT NULL AND k.od != '', DATE_FORMAT(k.od,'%H:%i'),'') AS cas_od,
                    IF(k.do IS NOT NULL AND k.do != '', DATE_FORMAT(k.do,'%d.%m.%Y'),'') AS datum_do,
                    IF(k.do IS NOT NULL AND k.do != '', DATE_FORMAT(k.do,'%H:%i'),'') AS cas_do,
                    IF(TRIM(s.obsah)='',0,1) AS je_obsah,
                    IF(k.do IS NOT NULL AND k.do != '', DATE_FORMAT(k.do,'%Y-%m-%d'),'') AS sql_do,
                    IF(k.od IS NOT NULL AND k.od != '', DATE_FORMAT(k.od,'%Y-%m-%d'),'') AS sql_od
                    
                FROM ".TABLE_KALENDAR_TERMINY." AS k
                LEFT JOIN ".TABLE_STRANKY." AS s ON k.idAkce = s.idStranky
                LEFT JOIN ".TABLE_STRANKY." AS r ON s.idRodice = r.idStranky
                WHERE s.zobrazit=1
                    AND r.zobrazit=1
                    AND s.typ='akce'
                    AND r.typ='kalendar-typ'
                    AND s.idJazyka=".WEB_LANG_ID."
                    AND s.idDomeny=".$domain->getId()."
                    AND r.idDomeny=".$domain->getId()."
                    ".$where."
                GROUP BY k.idTerminu
                ORDER BY k.od DESC, k.do DESC
                ".$limit."
                 ");
                 
            $celkem_data = $db->Query("SELECT FOUND_ROWS()");
            $celkem_arr = $db->getRow($celkem_data);
            
            $pagination->set_number_items($celkem_arr[0]);
            
            $i = 0;
            while($a = $db->getAssoc($data)){
                
                if($a['obrazek']!= '' && file_exists(PRE_PATH.$domain->getDir().USER_DIRNAME_CALENDAR.$a['obrazek'] )){
    				$img = $domain->getDir().USER_DIRNAME_CALENDAR.$a['obrazek'];				
    				}
    				else 
                    $img = '';					
    						
    			$link = $links->get_url($a['idStranky']);
    								
    			$linkname = $a['nazev'];
                
                $this->items[$i]['odkaz'] = $link;
    			$this->items[$i]['nazev_odkazu'] = $linkname;
                $this->items[$i]['obrazek'] = $a['obrazek'];
                $this->items[$i]['obrazek_path'] = $img;
                $this->items[$i]['id_akce'] = $a['idStranky'];
                $this->items[$i]['id_terminu'] = $a['idTerminu'];
                $this->items[$i]['misto'] = $a['misto'];
                $this->items[$i]['kategorie'] = $a['kategorie']; //typ akce
                $this->items[$i]['id_kategorie'] = intval($a['idKategorie']); //id typu akce
                $this->items[$i]['kategorie_url'] = $links->get_url($a['idKategorie']);
                
                $this->items[$i]['datum_od'] = $a['datum_od'];
                $this->items[$i]['datum_do'] = $a['datum_do'];
                $this->items[$i]['cas_od'] = str_replace("00:00","",$a['cas_od']);
                $this->items[$i]['cas_do'] = str_replace("00:00","",$a['cas_do']);
                $this->items[$i]['format_datum'] = $this->format_date_range($a['datum_od'],$a['cas_od'],$a['datum_do'],$a['cas_do']);
                $this->items[$i]['nazev'] = $a['nazev'];
                $this->items[$i]['perex'] = $a['perex'];
                $this->items[$i]['barva'] = $a['barva'];
                $this->items[$i]['od'] = $a['od']; //neformatovane datum
                $this->items[$i]['do'] = $a['do']; //neformatovane datum
                $this->items[$i]['sql_od'] = $a['od']; //neformatovane datum
                $this->items[$i]['sql_do'] = $a['do']; //neformatovane datum
                $this->items[$i]['je_obsah'] = $a['je_obsah'];
                $i++;
                }
            
        //nastavi typy = kategorie akci 
        if($typ != 'box' && $typ!='akce')
            $this->set_events_types();
        elseif($typ == 'akce')
            $this->set_event_detail();
        
        
        $db->free($data);
    }
    
    
    private function set_event_detail()
    {
        global $db;
        global $domain;
        
        if($this->typ != 'akce')
            return false;
            
        $data = $db->query("SELECT a.misto, k.nazev AS kategorie 
            FROM ".TABLE_STRANKY." AS a
            LEFT JOIN ".TABLE_STRANKY." AS k ON a.idRodice = k.idStranky
            WHERE a.idStranky=".$this->id."
                AND a.idDomeny=".$domain->getId()."
                AND a.idJazyka=".WEB_LANG_ID."
            GROUP BY a.idStranky
            LIMIT 1
            ");
            
        $this->event_detail = $db->getAssoc($data);
            
    }
    
    //nastavi typy akci
    private function set_events_types()
    {
        global $db;
        global $domain;
        global $links;
        
        $data = $db->query("SELECT s.nazev, s.idStranky AS id, s.barva, COUNT(k.idStranky) AS pocet_akci
            FROM ".TABLE_STRANKY." AS s
            LEFT JOIN ".TABLE_STRANKY." AS k ON s.idStranky = k.idRodice
            LEFT JOIN ".TABLE_KALENDAR_TERMINY." AS kt ON k.idStranky = kt.idAkce
            WHERE s.typ='kalendar-typ'
                AND s.zobrazit=1
                AND s.autentizace=0
                AND s.idDomeny=".$domain->getId()."
                AND s.idJazyka=".WEB_LANG_ID."
            GROUP BY s.idStranky
            ORDER BY s.nazev
        ");
        
        while($k = $db->getAssoc($data))
        {
            $this->typy_akci[$k['id']] = $k;
        }
        
        
    }
    
    //formatuje datovy interval
    private function format_date_range($datum_od, $cas_od, $datum_do, $cas_do)
    {
        $format = "";
        if($datum_od == $datum_do || $datum_do == "")
        {
            if($cas_od == "" || $cas_od == '00:00')
                $format .= $datum_od;
                else
                $format .= $datum_od." ".$cas_od;
                
            if($cas_od != "" && $cas_do != '' && $cas_od != $cas_do)
                $format .= " - ".$cas_do;
                
        }
        elseif($datum_od != $datum_do)
        {
            $format .= $datum_od." ".$cas_od." - ".$datum_do." ".$cas_do;
        }    
        
        return $format;
    }
    
    //vraci html kod pro vypis
    public function get_html()
    {
        $html = "";
        if($this->typ == 'kalendar-akci')
            $html = $this->get_html_event_calendar();
        elseif($this->typ == "kalendar-typ")
            $html = $this->get_html_event_type();
        elseif($this->typ == "akce")
            $html = $this->get_html_event_detail();
        elseif($this->typ == "box")
            $html = $this->get_html_box();
            
        return $html;
    }
    
    
    //vypise stranku kalendare akci
    private function get_html_event_calendar()
    {
        $html = "";
        //$html .= $this->get_html_filter();
        $html .= $this->get_html_events_types();
        $html .= $this->get_html_events();
        
        return $html;
    }
    
    //vypise stranku typu akci
    private function get_html_event_type()
    {
        return $this->get_html_event_calendar();
    }
    
    //vraci vsechny terminy
    public function get_items()
    {
        return $this->items;
    }
    
    
    //vypise stranku konkretni akce
    private function get_html_event_detail()
    {
        $html = "";
        
        $terminy = $this->get_items();
        
        $html .= "<table class='tTerminy'>";

        if($this->event_detail['misto']!='')
            $html .= "<tr><th>".TKDE.":</th><td>".$this->event_detail['misto']."</td></tr>";
            
        if($this->event_detail['kategorie']!='')
            $html .= "<tr><th>".TTYP_AKCE.":</th><td>".$this->event_detail['kategorie']."</td></tr>";
        
        if(count($terminy) > 0)
        {
            $html .= "<tr><th>".TKDY.":</th><td>";
            foreach($terminy AS $t)
            {
                $html .= $t['format_datum']."<br />";
            }
            $html .= "</td></tr>";
        }
        
        $html .= "</table>";
        
        return $html;
        
    }
    
    
    //vypise akce
    private function get_html_events()
    {
        global $links;
        global $pagination;
        
        $html = "";

        if(!$this->hasEvents()) 
            return $html; 
        
        $pocet = intval(POCET_AKCI_NA_STRANKU);
		$pagination->set_number_items_on_page($pocet);
        
        $session = 'vypis_akci';
		$stranka = $pagination->get_current_page();
		$od = ($stranka-1) * $pocet;
				
		$html .= "<span id='".$session."'></span>";

		$i = 0;
        
        $filtr_text = array();
        
        if($this->filter_settings["od"] != '' && $this->filter_settings["od"] == $this->filter_settings["do"])
            $filtr_text[] = TDEN." ".$this->filter_settings["od"];
        else
        {
            if($this->filter_settings["od"] != '')
                $filtr_text[] = TOD." ".$this->filter_settings["od"];
                
            if($this->filter_settings["do"] != '')
                $filtr_text[] = TDO." ".$this->filter_settings["do"];
        }
        
        $filtr_text = implode(" - ", $filtr_text);
        
        $filtr_text = $filtr_text == "" ? "" : '<br /><span>'.$filtr_text.'</span>';
        
        $html .= '<h2>'.TVYPIS_AKCI.$filtr_text.'</h2>';
        
        foreach($this->items AS $pol)
        {
                
            $html .= "<div class='novinka akce'>";

			if($pol['obrazek']!='')
                $html .= '<div class="obrazek"><a href="'.$links->get_url($pol['id_akce']).'" title="'.TCIST_VICE.'"><img alt="'.$pol['nazev'].'" src="'.$pol['obrazek_path'].'"></a></div>';
				
            if($pol['je_obsah'])
                $html .= '<h3><span style="border-left: 10px solid #'.$pol['barva'].'">&nbsp;</span><a href="'.$links->get_url($pol['id_akce']).'" title="'.TCIST_VICE.'">'.secureString($pol['nazev']).'</a></h3>';
                else
                $html .= '<h3><span style="border-left: 10px solid #'.$pol['barva'].'">&nbsp;</span>'.secureString($pol['nazev']).'</h3>';
            
            $html .= '<span class="datum">';
            
            if($pol['misto'] != '')
                $html .= TKDE.": ".$pol['misto']."<br />";
                
            $html .= TKDY.": ".$pol['format_datum'];
            $html .= '</span>';    
            $html .= $pol['perex'];   
             		
            
			
            $html .= "<div class='cleaner'></div>";
            $html .= "</div>";
            

			$i++;
        }
        
        
        
        $url_filtr = "&od=".$this->filter_settings["od"]."&do=".$this->filter_settings["do"]."#vypis_akci";
        
        $html .= $pagination->set_url_tail($url_filtr)->get_html();
        
        return $html;
    }
    
    //vypise stranku editboxu kalendare akci
    private function get_html_box()
    {
        global $domain;
        global $page;
        global $links;
        
        $html = "";
        $html .= "<div id='cal'></div>";
        $html .= "<h4>".TKALENDAR_AKCI."</h4>";
        
        
        $calendar = new SimpleCalendar($this->year."-".$this->month."-01");
        $calendar->setStartOfWeek(1);

        $calendar->wday_names = array(TDEN1, TDEN2, TDEN3, TDEN4, TDEN5, TDEN6, TDEN7);
        
        //oznaceni dnesniho data
        $calendar->addDailyHtml( TDNES, date("Y-m-d") );
        
        //print_r($this->items);
        $url_kalendare_akci = $links->get_url('kalendar-akci');
        foreach($this->items AS $e)
        {
            $calendar->addDailyHtml( $e, $e['sql_od'], $e['sql_do'] );
        }
        
        $html .= $calendar->show($this->month, $this->year, $url_kalendare_akci, false);
        
        return $html;
    }
    
    //vypise filtr pro vyhledavani akci
    private function get_html_filter()
    {
        global $links;
        
        $html = "";
        $html .= "<div class='vyhledavaci-filtr'>";
        $html .= "<form method='get' action='#filtr'>";
        $html .= "<label for='filtr_od'>".TOD."</label> <input type='text' name='od' value='".secureString($this->filter_settings['od'])."' placeholder='".TOD."' class='input' id='filtr_od'/> ";
        $html .= "<label for='filtr_do'>".TDO."</label> <input type='text' name='do' value='".secureString($this->filter_settings['do'])."' placeholder='".TDO."' class='input' id='filtr_do'/> ";
        $html .= "<input type='submit' value='".TZOBRAZIT."' name='btnZobrazit' class='tlacitko'/> ";
        $html .= "<a class='tlacitko' href='".$links->get_url('kalendar-akci')."#filtr' >".TZOBRAZIT_VSE."</a>";
        $html .= "</form>";
        $html .= "</div>";
        
        return $html;
    }
    
    //vraci odkazy na typy akci
    private function get_html_events_types()
    {
        global $links;
        global $db;
        global $domain;
        
        
        $html = "";
        $html .= "<div class='typy-akci' id='filtr'>";
        $html .= "<h4>".TFILTR_AKCI_PODLE_DATA_KONANI."</h4>";
        $html .= $this->get_html_filter();
        $typy_akci = array();
        
        $pocet_akci_filtr = array();
        
        //zjisti skutecny pocet akci podle filtru
        /*
        foreach($this->items AS $e)
        {
            $idk = $e['id_kategorie'];
            if(isset($pocet_akci_filtr[$idk]))
                $pocet_akci_filtr[$idk]++;
                else
                $pocet_akci_filtr[$idk] = 1;
        }
        */
        
        $data = $db->query("SELECT SQL_CALC_FOUND_ROWS s.nazev, COUNT(r.idStranky) AS pocet, s.idStranky AS id

                    
                FROM ".TABLE_STRANKY." AS s
                LEFT JOIN ".TABLE_STRANKY." AS r ON r.idRodice = s.idStranky
                LEFT JOIN ".TABLE_KALENDAR_TERMINY." AS k ON k.idAkce = r.idStranky
                WHERE s.zobrazit=1
                    AND r.zobrazit=1
                    AND s.typ='kalendar-typ'
                    AND r.typ='akce'
                    AND s.idJazyka=".WEB_LANG_ID."
                    AND s.idDomeny=".$domain->getId()."
                    AND r.idDomeny=".$domain->getId()."
                    ".$this->filter_settings['where']."
                GROUP BY s.idStranky
                ORDER BY s.priorita
                 ");
        
        while($s = $db->getObject($data))
        {
            $pocet_akci_filtr[$s->id] = intval($s->pocet);
        }
        
        
        $pocet_akci = 0;
        $url_filtr = "?od=".$this->filter_settings["od"]."&do=".$this->filter_settings["do"]."#filtr";
        
        foreach($this->typy_akci AS $id => $a)
        {
            $p = isset($pocet_akci_filtr[$id]) ? $pocet_akci_filtr[$id] : 0;
            
            $typy_akci[$id] = "<tr>
                <th><em style='background: #".$a['barva']."'></em></th>
                <td><a href='".$links->get_url($id).$url_filtr."' class=''>".$a['nazev']." (".$p.")</a></td>
                </tr>";
                
            $pocet_akci += $p;//$a['pocet_akci'];
            
        }
        
        $typy_akci[0] = "<tr><th><em></em></th><td><a href='".$links->get_url('kalendar-akci').$url_filtr."' class=''>".TVSECHNY_AKCE." (".$pocet_akci.")</a></td></tr>";
        
        
        ksort($typy_akci);
        
        $html .= "<h4>".TFILTR_AKCI_PODLE_TYPU_AKCE."</h4>";
        $html .= '<table class="tLegenda">';
        $html .= implode("",$typy_akci);
        $html .= '</table>';
        
        
        
        $html .= "<div class='cleaner'></div>";
        $html .= "</div>";
        
        return $html;
    }
    
    
    //overi zda existuji nejake akce
    public function hasEvents()
    {
        return count($this->items) > 0;
    }
    
    
    
    
}

?>