<?php

/**
 * @author Pavel Srom
 * @copyright 2010
 */

class C_XmlFile{

	//adresar kam se budou exporty ukladat
	public $type;
	
    //konstruktor
	public function __construct($type = 'rss'){
		
		$this->type = $type;

		}
        
    public function generate()
    {
        
        if($this->type == 'rss')
        {
            header('Content-Type: text/xml; charset=utf-8');
            echo $this->rss();
        }
        elseif($this->type == 'sitemap')
        {
            header('Content-Type: text/xml; charset=utf-8');
            echo $this->sitemap();
        }
        elseif($this->type == 'robots')
        {
            header("Content-type: text/plain; charset=UTF-8");
            echo $this->robots();
        }

        
    }
    
    //generuje robots.txt
    private function robots()
    {
        global $domain;
        if(POVOLIT_INDEXACI_WEBU)
            $out = "User-agent: * \nSitemap: ".$domain->getUrl()."sitemap.xml";
            else
            $out = "User-agent: *\nDisallow: /";
            
        return $out;
    }


    //generuje sitemap.xml
	private function sitemap(){
		global $db;
		global $domain;
        global $private_pages;
			
		$res = $db->Query("SELECT s.*, DATE_FORMAT(IF(s.datumAktualizoval IS NOT NULL, s.datumAktualizoval, s.datumVytvoril),'%Y-%m-%d') AS aktualizace, j.jazyk 
			FROM ".TABLE_STRANKY." AS s
            LEFT JOIN ".TABLE_JAZYKY." AS j ON s.idJazyka = j.idJazyka 
            WHERE s.idDomeny=".$domain->getId()." 
				AND s.zobrazit=1
				AND s.autentizace=0
                AND s.typ NOT IN ('404','prazdna','galerie','galerie-slozka','galerie-fotka','clanek','novinka','akce','tag','autor','login','produkt','email','objednavka')
                AND s.idStranky NOT IN (83,44)
            GROUP BY s.idStranky
                

                ");

		
		$xml = "<?xml version='1.0' encoding='UTF-8'?>
			<urlset xmlns='http://www.sitemaps.org/schemas/sitemap/0.9'
			xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'
			xsi:schemaLocation='http://www.sitemaps.org/schemas/sitemap/0.9
			    http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd'>";

   		while($page = $db->getAssoc($res))
        {
   		   
   		    if($private_pages->is_private($page['idStranky']))
                continue;   
           
			$xml .= "<url>\n";
            
            if($page['typ']!='uvodni')
                $url = UrlPage($page['url'],$page['idStranky'],$page['jazyk']);
            elseif($page['jazyk'] == 'cz')
                $url = "";
            else
                $url = $domain->getRelativeUrl().$page['jazyk']."/";
            
            
			$xml .= "<loc>".PROTOKOL.($domain->get("www") == 1 ? "www." : "").$domain->getName().$url."</loc>\n";
			$xml .= "<lastmod>".$page['aktualizace']."</lastmod>\n";
			$xml .= "<changefreq>daily</changefreq>\n";
			$xml .= "<priority>0.8</priority>\n";
			$xml .= "</url>";
        }
		$xml .= "</urlset>\n";

		return $xml;
		
    }

    //generuje rss pro novinky a clanky
	private function rss(){
		global $db;
		global $domain;
		global $private_pages;
        
        $data = $db->Query("SELECT DATE_FORMAT(MAX(datum),'%d.%m.%Y %H:%i:%s') AS lastModify  
				
			FROM ".TABLE_STRANKY."
			WHERE idDomeny = ".$domain->getId()."
				AND zobrazit=1
                AND idJazyka='".WEB_LANG_ID."'
                AND (typ='novinka' OR typ='clanek')
				AND (
					od <= CURDATE( )
					OR od IS NULL
					)
				AND (
					do >= CURDATE( )
					OR do IS NULL
					)
            LIMIT 1
			");
        
        $lm = $db->getAssoc($data);
        
        
		$data = $db->Query("SELECT s.idStranky, s.nazev, s.perex, s.obrazek, j.jazyk, r.nazev AS rubrika,
				IF( s.datum IS NOT NULL , DATE_FORMAT(s.datum,'%d.%m.%Y %H:%i:%s'), '' ) AS vytvoreno, s.url,
                DATE_FORMAT(s.datumAktualizoval,'%Y-%m-%d') AS aktualizace 
			FROM ".TABLE_STRANKY." AS s
            LEFT JOIN ".TABLE_JAZYKY." AS j ON s.idJazyka = j.idJazyka 
            LEFT JOIN ".TABLE_STRANKY." AS r ON s.idRodice = r.idStranky
			WHERE s.idDomeny = ".$domain->getId()."
				AND s.zobrazit=1
                AND s.idJazyka='".WEB_LANG_ID."'                
                AND s.autentizace=0
                AND (s.typ='novinka' OR s.typ='clanek')
				AND (
					s.od <= CURDATE( )
					OR s.od IS NULL
					)
				AND (
					s.do >= CURDATE( )
					OR s.do IS NULL
					)
            GROUP BY s.idStranky
			ORDER BY s.datum DESC
            LIMIT 15
			");
        
		/*
		if($db->numRows($data)==0) {
            $xml = "<?xml version='1.0' encoding='utf-8'?>\n";
    		$xml .= "<rss version='2.0' xmlns:atom='http://www.w3.org/2005/Atom'>\n";
            $xml .= "</rss>\n";
            return false;
          
            }
		*/
		$xml = "<?xml version='1.0' encoding='utf-8'?>\n";
		$xml .= "<rss version='2.0' xmlns:atom='http://www.w3.org/2005/Atom'>\n";
		
		$xml .= "<channel>\n";
		
		if(NAZEV_WEBU=='')
			$title = $domain->getName();
			else
			$title = NAZEV_WEBU; 
		
		$url = $domain->getUrl()."rss.xml";
		
		$xml .= "<title>".$title."</title>\n";
		
		$xml .= "<link>".$domain->getUrl()."</link>\n";
		
		$xml .= "<description>".(POPIS_WEBU==''?$domain->getName():POPIS_WEBU)."</description>\n";
		
		$xml .= "<language>cs</language>\n";
		
		$xml .= "<lastBuildDate>".date("D, d M Y H:i:s O", strtotime($lm['lastModify']))."</lastBuildDate>\n";
		
		$xml .= "<atom:link href='".$url."' rel='self' type='application/rss+xml' />\n";

		$i=0;

		while($news = $db->getAssoc($data)){
			
			if(intval($news['idStranky'])==0) continue;

            if($private_pages->is_private($news['idStranky']))
                continue;

            
			/*
			if($i==0){
				
				$src = "";
				if($news['obrazek']!='' || $news['obrazek']!=null){
				if(TestHttpUrlImage($news['obrazek']))
					$src = $news['obrazek'];
				if(file_exists($domain->getDir().USER_DIRNAME_NEWS.$news['obrazek']))
					$src = "http://".$domain->getName().$domain->getDir().USER_DIRNAME_NEWS."/".$news['obrazek'];
				}

			if($src!=''){
				$xml .= "<image>\n";
	      		$xml .= "<url>".$src."</url>\n";
	      		$xml .= "<title>".$title."</title>\n";
	      		$xml .= "<link>http://".$domain->getName()."</link>\n";
				$xml .= "</image>\n";
				}
	
				}
			*/
				
            $news['perex'] = str_replace(array("&nbsp;"),array(" "), $news['perex']);
                
			$xml .= "<item>\n";
			$xml .= "<title>".$news['nazev']."</title>\n";
			$xml .= "<description>".strip_tags($news['perex'])."</description>\n";
			$xml .= "<link>".PROTOKOL.$domain->getName().UrlPage($news['url'],$news['idStranky'],$news['jazyk'])."#utm_source=rss&amp;utm_medium=feed&amp;utm_campaign=".$news['rubrika']."&amp;utm_content=main</link>\n";
			$xml .= "<pubDate>".date("D, d M Y H:i:s O", strtotime($news['vytvoreno']))."</pubDate>\n";
			$xml .= "<guid>".PROTOKOL.$domain->getName().UrlPage($news['url'],$news['idStranky'],$news['jazyk'])."</guid>";
			$xml .= "</item>\n";
			$i++;
			}
		
		$xml .= "</channel>\n";
		$xml .= "</rss>\n";
	
        return $xml;

		}


}

?>