<?php

/**
 * @author Pavel Srom
 * @copyright 2010
 */

class C_Finder{
	
	//vyhledavany vyraz
	public $term;
	
	//obsahuje pole dotazu v poli
	private $query;
	
	//maximalni pocet vysledku
	public $limit;
	
	//vysledky vyhledavani
	public $result;
    
	//relevance stranek
	public $relevance;
    
    public $name;
	
    //indikuje zda se bude vyhledavat fulltextove nebo pomoci like
    public $fulltext = true;
    
	public function __construct($term=''){
		global $db;
		global $domain;
		
		
		$this->term = $db->secureString(strip_tags(urldecode($term)));
		
		$this->relevance = array();
		$this->result = array();
		
		$this->fulltext = true;
		$this->query = array();
		$this->result = array();
		$this->limit = 20;
        $this->name = TVYHLEDAVANI;
		}
	
	public function Find($statistics = true){
		global $db;
        global $domain;
		
		if(strlen($this->term)<3) return;	
		
		
		//ulozeni vyrazu do statistik
        if($statistics)
        {
    		$data1 = $db->Query("SELECT idVyrazu FROM ".TABLE_VYHLEDAVANI_VYRAZY." WHERE idDomeny=".$domain->getId()." AND vyraz=LOWER('".$this->term."') LIMIT 1");
    		
    		if($db->numRows($data1)==0)
    			$db->Query("INSERT INTO ".TABLE_VYHLEDAVANI_VYRAZY." (idDomeny, vyraz, cetnost) VALUES (".$domain->getId().", LOWER('".$this->term."'), 1)");
    			else
    			$db->Query("UPDATE ".TABLE_VYHLEDAVANI_VYRAZY." SET cetnost = cetnost + 1 WHERE idDomeny=".$domain->getId()." AND vyraz=LOWER('".$this->term."')");
		}
        
		
		$query = $this->GetMatchQuery();

        
		$data = $db->Query($query." ORDER BY vaha DESC LIMIT ".$this->limit);
		
		
		//zjisteni relevance

		while($r = $db->getAssoc($data)){
			$id = $r['id'];
			$vaha =$r['vaha'];
			$obsah = $r['perex']." ".$r['obsah']." ".$r['popis1']." ".$r['popis2']." ".$r['popis3']." ".$r['popis4']." ".$r['popis5'];
			$typ = $this->GetTypeName($r['typ']);
            $typ = $typ != "" ? $typ.": " : "";
            	
			if(isset($this->relevance[$id]))
				$this->relevance[$id] = $this->relevance[$id] + $vaha;
				else
				$this->relevance[$id] = $vaha;

					
			if(!isset($this->result[$id])){
				$this->result[$id]=array();
				$this->result[$id]['nadpis']= $typ.$r['nazev'];
				$this->result[$id]['url']= strstr($r['url'],PROTOKOL) ? $r['url'] : UrlPage($r['url'],$r['id'],$r['jazyk']);
				}
				
			if(isset($this->result[$id]))
				if(isset($this->result[$id]['obsah']))
					$this->result[$id]['obsah'] = $this->result[$id]['obsah']." ".$obsah;
					else
					$this->result[$id]['obsah'] = $obsah;
	
				
			}

			
		
	}
	
    public function GetTypeName($typ)
    {
        $nazev_typu = "";
        if($typ == "novinka")
            $nazev_typu = TNOVINKA;
        elseif($typ == "clanek")
            $nazev_typu = TCLANEK;
        elseif($typ == "akce")
            $nazev_typu = TAKCE;
        elseif($typ == "galerie")
            $nazev_typu = TGALERIE;
        elseif($typ == "galerie-fotka")
            $nazev_typu = TGALERIE_FOTKA;
        elseif($typ == "produkt")
            $nazev_typu = TPRODUKT;
            
        return $nazev_typu;
        
    }
    
    
	public function GetResult(){
        
        global $private_pages;
        global $links;
        
		$html = "";

		$html .= '<div class="search search-page">
		<div class="search-container">';
		$html .= $this->GetFindForm();
		$html .= '</div></div>';
		/*
        $html .= '<form class="fUni" method="get" action="'.$links->get_url('vyhledavani').'">

                    <label for="hledany-vyraz">'.THLEDANY_VYRAZ.'</label> <input id="hledany-vyraz" type="text" name="vyraz" class="input" value="'.secureString($this->term).'"/> <input type="submit" class="btn" value="'.THLEDAT.'" name="hledat"/><input type="hidden" name="hledat" value="hledat" />
                </form>';
		*/

		$html .= "<h2>".THLEDANY_VYRAZ.": \"".secureString($this->term)."\"</h2>";

        $vysledek = "";
        
		$vysledek .= "<div class='novinky'>";
			
		$pocet = 0;
		foreach($this->relevance AS $id => $r){
			if($r==0) continue;
            if(!$private_pages->has_access($id))
                continue;
                
            $pocet++;
			$obsah = $this->CropAndSaveWords($this->result[$id]['obsah'],$this->term,120);
			
            $target = strstr($this->result[$id]['url'],PROTOKOL) ? "target='_blank'" : ""; 
            $url = strstr($this->result[$id]['url'],PROTOKOL) ? $this->result[$id]['url'] : ($this->result[$id]['url']."?hledat=".urlencode($this->term));
            
            $vysledek .= "<div class='aktualita'>";
			$vysledek .= "<h3><a href='".$url."' ".$target.">".$this->MarkWord($this->result[$id]['nadpis'], $this->term)."</a></h3>";
				
			$vysledek .= "<p>".$this->MarkWord($obsah, $this->term)."</p>";
            $vysledek .= "</div>";
			}
		
        $vysledek .= '</div>';
        		
        if($pocet == 0)
            $html .= "<p>".TNEBYLO_NIC_NALEZENO."</p>";
            else
            $html .= $vysledek;
        
        
			
			
		return $html;
		}
		
	//vybere část textu, kde se nachází hledaná slova	
	//vstupni parametry / kde, co, okoli slova
	public function CropAndSaveWords($Where,$What,$WordsAround)
		{
		$Where = trim($Where);
		$What = trim($What);
		
		//$What = "Úklid";
		//echo $What;
		$i = mb_stripos($Where,$What);
		if($i==false)
			{
			$i = 0;
			}
	
		while($i>0 && isset($Where[$i-$WordsAround]) && $Where[$i-$WordsAround]!=" ")
			{
			$i--;
			}
	
		if($i-$WordsAround>=0)
			{
			$Where = substr($Where,$i-$WordsAround);
			}
		else
			{
			$WordsAround = $WordsAround ;
			}
		$i = strpos($Where,$What);
		if($i==false)
			{
			$i = 0;
			}
			while($i<strlen($Where) && isset($Where[$i+$WordsAround+strlen($What)]) && $Where[$i+$WordsAround+strlen($What)]!=" ")
				{
				$i++;
				}
	
		$Where = substr($Where,0,$i+$WordsAround+strlen($What));
        
        $Where = cut_text($Where,$WordsAround,"");
        
        if(trim($Where) == "")
            return $Where;
            else
		    return "...".$Where."...";
	}
		
	//zvyrazni nalezene slovo
	public function MarkWord($text, $slovo) {
        if(mb_strlen($slovo) < 3) return $text;
        
    	$slova=explode(' ',$slovo);
		
    	for($f=0;$f<count($slova);$f++)	{
			    $search = preg_quote(htmlspecialchars(stripslashes($slova[$f])), '~');
			    //$search = $slova[$f];
			    if ($search) {
			        $text = preg_replace("~$search~i", '<span class="search-term">\\0</span>', $text);
			        //$text = eregi_replace($search, '<span class="search-term">'.$search.'</span>', $text);
			        
			        // odstranění zvýrazňování z obsahu <option> a <textarea> a zevnitř značek a entit
			        $span = '<span class="search-term">[^<]*</span>';
			        $pattern = "~<(option|textarea)[\\s>]([^<]*$span)+|<([^>]*$span)+|&([^;]*$span)+~i";
			        $text = preg_replace_callback($pattern, 'UnmarkWord', $text);
			    }
			}
		return $text;
		}
	
	
	public function GetFindForm(){
		global $links;
        
		$html = "";
		/*
        $html .= '<form method="get" id="formVyhledavani" action="'.$links->get_url('vyhledavani').'" >
                <div>
                <input type="text" name="vyraz" class="input fl" placeholder="'.THLEDANY_VYRAZ.'..."/>
                <input type="image" src="'.RELATIVE_URL_USER_DOMAIN.'img/hledat.png" name="hledat" class="button fr" alt="'.THLEDAT.'"/>
                <input type="hidden" name="hledat" value="hledat" />
                </div>
            </form>';
        */
			
		$html .= '<form action="'.$links->get_url('vyhledavani').'"  method="get" >
		<div class="finder-wrapper">
		  <input type="text" placeholder="'.THLEDANY_VYRAZ.'" name="vyraz" id="hledany-vyraz">
		  <button type="submit" name="hledat">
            <img src="'.RELATIVE_URL_USER_DOMAIN.'img/search.svg" alt="">
          </button>
		</div>
	  </form>';
/*
        $html .= '<form method="get" action="'.$links->get_url('vyhledavani').'" class="fVyhledavani">
                    <input type="text" name="vyraz" class="text" placeholder="'.THLEDANY_VYRAZ.'..." title="'.THLEDANY_VYRAZ.'"/>
                    <input type="submit"  value="" name="hledat" title="'.THLEDAT.'" class="submit" placeholder="'.THLEDAT.'"/>
                </form>';
  */      
        
		return $html;
	}
    
    public function GetFindFormMobile(){
		global $links;
        
		$html = "";
            
        $html .= '<form method="get" action="'.$links->get_url('vyhledavani').'" class="fVyhledavani mobileSearch">
                    <input type="text" name="vyraz" class="text" placeholder="'.THLEDANY_VYRAZ.'..." title="'.THLEDANY_VYRAZ.'"/>
                    <input type="submit"  value="" name="hledat" title="'.THLEDAT.'" class="submit"/><input type="hidden" name="hledat" value="hledat" class="button"/>
                    <button><i class="fa fa-search" aria-hidden="true"></i></button>
                </form>';
        
        
		return $html;
	}
	
	
	
	
	private function GetMatchQuery(){
		global $domain;
		global $private_pages;

        $where = "";
        if(!POVOLIT_DETAIL_FOTKY)
        {
            $where = " AND s.typ != 'galerie-fotka'"; 
		}
		
		        
        if($this->fulltext)
    		return "SELECT s.idStranky AS id, s.typ,
				IF(s.nadpis='', s.nazev, s.nadpis) AS nazev,
				s.url AS url, 
                s.obsahBezHtml AS obsah,
                s.perexBezHtml AS perex,
                s.popis1BezHtml AS popis1,
                s.popis2BezHtml AS popis2,
                s.popis3BezHtml AS popis3,
                s.popis4BezHtml AS popis4,
                s.popis5BezHtml AS popis5,
                IF(s.nadpis LIKE '%".$this->term."%', 3, 0) +
                IF(s.perexBezHtml LIKE '%".$this->term."%', 2, 0) +
                IF(s.obsahBezHtml LIKE '%".$this->term."%', 1, 0) +
                IF(s.popis1BezHtml LIKE '%".$this->term."%', 0.5, 0) +
                IF(s.popis2BezHtml LIKE '%".$this->term."%', 0.5, 0) +
                IF(s.popis3BezHtml LIKE '%".$this->term."%', 0.5, 0) +
                IF(s.popis4BezHtml LIKE '%".$this->term."%', 0.5, 0) +
                IF(s.popis5BezHtml LIKE '%".$this->term."%', 0.5, 0) +
                10 * MATCH(s.nadpis) AGAINST ('".$this->term."') +
				6 * MATCH(s.perexBezHtml) AGAINST ('".$this->term."') + 
                3 * MATCH(s.obsahBezHtml) AGAINST ('".$this->term."') +
                1 * MATCH(s.popis1BezHtml) AGAINST ('".$this->term."') +
                1 * MATCH(s.popis2BezHtml) AGAINST ('".$this->term."') +
                1 * MATCH(s.popis3BezHtml) AGAINST ('".$this->term."') +
                1 * MATCH(s.popis4BezHtml) AGAINST ('".$this->term."') +
                1 * MATCH(s.popis5BezHtml) AGAINST ('".$this->term."') AS vaha,
                j.jazyk,
                0 AS priorita		
			FROM ".TABLE_STRANKY." AS s 
            LEFT JOIN ".TABLE_JAZYKY." AS j ON s.idJazyka = j.idJazyka
			WHERE s.idDomeny=".$domain->getId()."
                AND s.idJazyka=".WEB_LANG_ID."
				AND s.zobrazit=1
                AND s.typ NOT IN ('vyhledavani','404','objednavka','email')
            HAVING vaha > 0
            ";
            
            else
            
            return "SELECT s.idStranky AS id, s.typ,
				IF(s.nadpis='', s.nazev, s.nadpis) AS nazev,
				s.url AS url, 
                s.obsahBezHtml AS obsah,
                s.perexBezHtml AS perex,
                s.popis1BezHtml AS popis1,
                s.popis2BezHtml AS popis2,
                s.popis3BezHtml AS popis3,
                s.popis4BezHtml AS popis4,
                s.popis5BezHtml AS popis5,
                IF(s.nadpis LIKE '%".$this->term."%', 10, 0) +
                IF(s.perexBezHtml LIKE '%".$this->term."%', 6, 0) +
                IF(s.obsahBezHtml LIKE '%".$this->term."%', 3, 0) +
                IF(s.popis1BezHtml LIKE '%".$this->term."%', 1, 0) +
                IF(s.popis2BezHtml LIKE '%".$this->term."%', 1, 0) +
                IF(s.popis3BezHtml LIKE '%".$this->term."%', 1, 0) +
                IF(s.popis4BezHtml LIKE '%".$this->term."%', 1, 0) +
                IF(s.popis5BezHtml LIKE '%".$this->term."%', 1, 0) AS vaha,
                j.jazyk,
                0 AS priorita		
			FROM ".TABLE_STRANKY." AS s 
            LEFT JOIN ".TABLE_JAZYKY." AS j ON s.idJazyka = j.idJazyka
			WHERE s.idDomeny=".$domain->getId()."
                AND s.idJazyka=".WEB_LANG_ID."
				AND s.zobrazit=1
                AND s.typ NOT IN ('vyhledavani','404','objednavka','email')
                ".$where."
            HAVING vaha > 0
            ";
            
			
		
		
		
		
		
	}
	
	
	
}

?>