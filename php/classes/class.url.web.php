<?php

/**
 * @author Pavel Srom
 * @copyright 2013
 */



class C_Url
{
    //obsahuje URI retezec, ktery se bude parsovat
    private $uri = "";
    
    //aktivni jazyk webu
    private $lang = LANG;

    //obsahuje data stranek
    private $data = array();

    //zde jsou ulozene url stranek, na ktere uz byl pozadavek
    private $cache = array();
    
    //konstruktor
    public function __construct($uri = "")
    {
        //inicializace uri
        $uri = $uri == "" ? "" : $uri;
        $get_uri = isset($_GET['url']) ? $_GET['url'] : "";
        $uri = $uri == "" ? $get_uri : $uri;
        $this->uri = $uri;
        
        //nacte data vsech stranek
        //$this->set_data();     
        
        //parsovani uri - zjisteni jazyka, id stranky, typ stranky
        $this->parse();
    }
    
    //nacte data vsech stranek do pameti
    private function set_data()
    {
        global $db;
		global $domain;	
        
        if(!isset($_SESSION['idUzivatele']))
            $cond = "AND zobrazit=1";
            else
            $cond = "";
        
		$data_pages = $db->Query("SELECT idStranky, idRodice, hloubka, s.nazev, nadpis,nazevProMenu,url,typ,doMenu, s.idJazyka, obrazek, j.jazyk
            FROM ".TABLE_STRANKY." AS s
            LEFT JOIN ".TABLE_JAZYKY." AS j ON s.idJazyka = s.idJazyka
            WHERE idDomeny=".$domain->getId()."
                ".$cond."  
            ORDER BY priorita");
		
		while($p = $db->getAssoc($data_pages)){
			$this->data[$p['idStranky']] = array(
                'id_stranky'=>$p['idStranky'],
                'nazev'=>$p['nazev'],
                'jazyk'=>$p['jazyk'], 
                'url_nazev'=>$p['url'],
                'url'=>($p['typ']=='uvodni' ? $domain->getRelativeUrl().(LANG == DEFAULT_LANG ? "" : LANG."/"):UrlPage($p['url'],$p['idStranky'],$p['jazyk'])),
                'menu_nazev'=>$p['nazevProMenu'],
                'do_menu'=>$p['doMenu'],
                'rodic'=>$p['idRodice'],
                'hloubka'=>$p['hloubka'],
                'nadpis'=>$p['nadpis'],
                'typ'=>$p['typ'],
                'idJazyka'=>$p['idJazyka'],
                'foto'  => $p['obrazek']
                );
            
           
            }
            
            
        print_r($this->data);
            
    }
    
    
    //zjisti zda stranka s danym id ma potomky
    public function haveChildren($idPage, $povoleni_potomci = null){
        
        global $db;
        global $domain;
        
        $povoleni_potomci = (array)$povoleni_potomci;
        
        $data = $db->query("SELECT typ FROM ".TABLE_STRANKY." WHERE idRodice=".$idPage." AND idDomeny=".$domain->getId());
        
        if($db->numRows($data) > 0 && $povoleni_potomci != null && is_array($povoleni_potomci) && count($povoleni_potomci)>0)
            while($s = $db->getAssoc($data))
            {
                if(!in_array($s['typ'], $povoleni_potomci)) 
                    continue;
                   
                return true;    
            }
            
        return false;
  
    }
    
    //zjisti zda bude stranka s danym id zobrazena v menu
    public function isShowInMenu($idPage){
        
        global $db;
        global $domain;
        
        $s = $db->get(TABLE_STRANKY, array("doMenu AS do_menu","idRodice AS rodic"),"idStranky=".$idPage);
        
                
        if($s!== false && $s->do_menu == 0) 
            return false;
        
        
        if($s->rodic > 0)
            return $this->isShowInMenu( $s->rodic);
            else
            return (bool) $s->do_menu;
    
        
    }
    
    public function getIdHomePage()
    {
        foreach($this->data AS $id => $s)
            if($s['typ'] == 'uvodni')
                return $id;
        
        return 0;
    }
    
    
    public function getUrlHomePage()
    {
        foreach($this->data AS $id => $s)
            if($s['typ'] == 'uvodni')
                return $s['url_nazev'];
        
        return 0;
    }

    public function get_name($id)
    {
        return $this->get($id, 'menu_nazev');
    }
    
    //vraci url stranky podle id
    public function getUrl($id)
    {
        return $this->get($id,'url');
    }
    
    //vraci url stranky podle id
    public function getType($id)
    {
        return $this->get($id,'typ');
    }
    
    //vraci parametr stranky podle klice
    public function get($id,$key)
    {
        return isset($this->data[$id][$key]) ? $this->data[$id][$key] : false;
    }
    
    //zjisti zda stranka s danym id ma nejake podstranky
    public function hasSubpage($id, $disallow_page_type)
    {
        global $db;
        global $domain;
        
        $data = $db->query("SELECT typ FROM ".TABLE_STRANKY." WHERE idDomeny=".$domain->getId()." AND idRodice=".$id);
        
        while($s = $db->getAssoc($data))
            if(!in_array($s['typ'], $disallow_page_type))
                return true;
                
        return false;
    }
    
    //vraci id chybove stranky
    public function getId404()
    {
        foreach($this->data AS $s)
            if($s['typ'] == '404')
                return $s['id_stranky'];
                
        return 0;
    }

    
    //parsuje url adresu - zjisti udaje jako jazyk, id stranky, typ stranky, koncovku, adresu pro dalsi presmerovani
    private function parse()
    {
        global $domain;
        global $db;
        
        
        //zjisteni koncovky url adresy
        $koncovka_data = explode('.',$this->uri);
        $koncovka = "";
        if(count($koncovka_data)>1){
            $koncovka = end($koncovka_data);
            
            array_pop($koncovka_data);
            }
        
        
        $url_bez_koncovky = implode('',$koncovka_data);
        $url_nazvy_data = explode('/',$url_bez_koncovky);
        
        $url_nazev = end($url_nazvy_data);       
        $url_struktura_data = explode("_",$url_nazev);
        
        $nazev = "";
        $id = 0;
        
        $result = array();
        
        $pozadovana_struktura = $domain->data['urlStruktura'];
        $pozadovana_koncovka = $domain->data['urlKoncovka'];
        $pozadovana_url = "";
        
        if(count($url_struktura_data)==1) {
            
            $nazev = $url_struktura_data[0];
            
            if(!isset($_SESSION['idUzivatele']))
                $cond = "AND zobrazit=1
                    AND (((od <= CURDATE() OR od IS NULL) AND (do >= CURDATE() OR do IS NULL)) OR (archivovat=1)) 
    				";
                else
                $cond = "";
            
            $data = $db->Query("SELECT idStranky FROM ".TABLE_STRANKY." 
                WHERE idDomeny=".$domain->getId()." 
                    AND idJazyka='".WEB_LANG_ID."'
                    AND url='".$db->secureString($nazev)."' 
                    ".$cond."
                    LIMIT 1");
            if($db->numRows($data)>0){
                $ids = $db->getAssoc($data);
                $id = intval($ids['idStranky']);
                }
            }
        elseif(count($url_struktura_data)>1){
                 
                if(ctype_digit($url_struktura_data[0])){
                    $id = intval($url_struktura_data[0]);
                    $nazev = end($url_struktura_data);
                    }
                elseif(ctype_digit(end($url_struktura_data))){
                    $id = intval(end($url_struktura_data));    
                    $nazev = $url_struktura_data[0];
                    }
            }
        
        $presmerovat = "";

        if($pozadovana_koncovka!='') $pozadovana_koncovka = ".".$pozadovana_koncovka;
        
        if($pozadovana_struktura=='id-nazev' && $id>0 && $nazev!='')
            $pozadovana_url = $id."_".$nazev.$pozadovana_koncovka;
        elseif($pozadovana_struktura=='nazev-id' && $id>0 && $nazev!='')
            $pozadovana_url = $nazev."_".$id.$pozadovana_koncovka;
        elseif($pozadovana_struktura='nazev' && $nazev!='')
            $pozadovana_url = $nazev.$pozadovana_koncovka;
        
        if($this->uri!=$pozadovana_url)
            $presmerovat = $pozadovana_url;
        
        
        return array(
            'id'=>$id,
            'nazev'=>$nazev,
            'koncovka'=>$koncovka,
            'presmerovat'=>$presmerovat
            );
        
        
        
    }

    //rekurzivni funkce, ktera generuje url typu strom
    private function generate_tree_url($id, $url)
    {
        global $db;
        
        $url_part = $db->get(TABLE_STRANKY, array("url","idRodice AS id"), "idStranky=".$id);
        
        if($url_part->url == "")
            return "";
            
        $url = $url_part->url."/".$url;
        
        $url = $this->generate_tree_url($url_part->id, $url);        
        return $url;
        
    }



    //parametr id muze byt bude id stranky nebo typ stranky
    public function get_url($id_nebo_typ)
    {
        
        global $db;
        global $domain;
        global $langs;
        
       
        
        //pokud je url jiz nalezena v cache, pouzije se
        if(isset($this->cache[$id_nebo_typ]))
            return $this->cache[$id_nebo_typ];
        
        
        //nacteni udaju o url adrese
        $data = $db->query("SELECT idStranky AS id, urlKomplet AS url_komplet, url, jazyk, typ, idRodice AS idr 
            FROM ".TABLE_STRANKY." AS s
            LEFT JOIN ".TABLE_JAZYKY." AS j ON s.idJazyka=j.idJazyka
            WHERE (idStranky=".intval($id_nebo_typ)." OR typ='".$db->secureString($id_nebo_typ)."')
                AND idDomeny=".$domain->getId()."
                AND s.idJazyka = ".WEB_LANG_ID."
            LIMIT 1
            ");
        
        
        if($db->numRows($data) == 0)
            return '';
        
        $page = $db->getObject($data);
        $id = $page->id;
        $name = $page->url;
        $typ = $page->typ;
        $lang_name = $page->jazyk;
        $url_komplet = $page->url_komplet;
        

        //ulozeni do cache
        if($url_komplet != '')
        {
            $this->cache[$id] = $url_komplet;
            $this->cache[$typ] = $url_komplet;
        }
        
        //pokud je url jiz nalezena v cache podle id stranky, pouzije se
        if(isset($this->cache[$id]))
            return $this->cache[$id];
            
        //pokud je url jiz nalezena v cache podle typu stranky, pouzije se
        if(isset($this->cache[$typ]))
            return $this->cache[$typ];
        
        
        //samotne generovani url adresy
        $koncovka = $domain->data['urlKoncovka'];
        if($koncovka!='') 
            $koncovka = ".".$koncovka;

        $struktura = $domain->data['urlStruktura'];
        
        //$struktura='nazev-nazev';
        
        $result = "";
        if($typ == 'uvodni')
            $result = "";
        elseif($struktura=='id-nazev')
        	$result = $id."_".$name.$koncovka;
        elseif($struktura=='nazev-id')
            $result = $name."_".$id.$koncovka; 
        elseif($struktura=='nazev')
            $result = $name.$koncovka;
        elseif($struktura=='nazev-nazev')
            $result = $this->generate_tree_url($page->idr, $name.$koncovka);
            
        if(MULTILANGUAGE_WEB && $lang_name != DEFAULT_LANG)
            $result = $lang_name."/".$result;
        
		return $domain->getRelativeUrl().$result;
        
        
        
    }


    
    
    
}


?>