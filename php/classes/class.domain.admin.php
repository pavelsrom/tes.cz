<?php

/**
 * @author Pavel Srom
 * @copyright 2009
 */

class C_Domains{
	
    //id aktivni domeny
	private $id;
    
    //jmeno aktivni domeny
	private $name;
    
    //url aktivni domeny
    private $url;
    
    //relativni cesta k domene
	private $relative_url;
    
    
    //data aktivni domeny
	public $data;
    
    //nazvy session klicu, ve kterych je ulozeno id a nazev aktivni domeny
	private $sn_id_domain; 
	private $sn_domain_name;

	//tato vlastnost urcuje zda se bude nazev slozky domeny udrzovat v session, to je zapotrebi kvuli inicializaci skriptu pro tinybrowser, kde je zapotrebi menit uzivatelske slozky pro nahrana data
	public $sn_domain_dir;
	public $sn_domain_subdir;
		
	//konstruktor
	public function __construct(){
		global $db;
		
		$this->sn_id_domain = 'cmsDomainId';
		$this->sn_domain_name = 'cmsDomainName';
		$this->sn_domain_dir = 'cmsDomainDir';
		$this->sn_domain_subdir = 'cmsSubdirDocumentRoot';
		
		if(!isset($_SESSION[$this->sn_id_domain])) 
        {
			$this->id = $_SESSION[$this->sn_id_domain] = $this->GetIDAktivDomain();
			$this->name = $_SESSION[$this->sn_domain_name] = $this->GetNameDomainByID($this->id);
			$this->data = $this->setData(true);
		}
			else
            {
				$this->id = $_SESSION[$this->sn_id_domain];
				$this->name = $_SESSION[$this->sn_domain_name];
								
				if($this->id==0)
                {
					$this->id = $_SESSION[$this->sn_id_domain] = $this->GetIDAktivDomain();
					$this->name = $_SESSION[$this->sn_domain_name] = $this->GetNameDomainByID($this->id);
				}
					
				
				
			}
			
			$this->setData();	

		}
	
    //overi zda domena existuje
    public function exists()
    {
        return (bool) $this->id;
    }
    
    //vraci id aktivni domeny
    public function getId()
    {
        return $this->id;
    }
    
    
    //vraci jmeno aktivni domeny
    public function getName()
    {
        return $this->name;
    }
    
    //vraci url rootu aktivni domeny
    public function getUrl()
    {
        return $this->url;
    }
    
    //vraci relativni url rootu aktivni domeny
    public function getRelativeUrl()
    {
        return $this->relative_url;
    }
    
    
    
    //nastavi data domeny
    //parametr autodetection pokud je true dojde k automaticke detekci domeny, pokud neni true nactou se data aktualni domeny podle session
    private function setData($autodetection = false)
    {
        global $db;

        if($autodetection)
        {
            $domainName = $_SERVER['HTTP_HOST'];
    		$host1 = GetDomainWithWww($domainName);
    		$host2 = GetDomainWithoutWww($domainName);
            $where = "(domena='".$host1."' OR domena='".$host2."')";
        }
        else
        $where = "idDomeny=".$this->id;

        $data = $db->Query("SELECT idDomeny, domena, hash, adresar, www, urlStruktura, urlKoncovka,
            IF(adresar='',CONCAT('".PROTOKOL."',IF(www = 1,'www.',''),domena,'/'),CONCAT('".PROTOKOL."',domena,'/',adresar,'/')) AS url,
            IF(adresar='','/',CONCAT('/',adresar,'/')) AS relative_url
		    FROM ".TABLE_DOMENY." 
            WHERE ".$where." 
                AND zobrazit=1 
            LIMIT 1");
					
		if($db->numRows($data)==1)
        {
            $this->data = $db->getAssoc($data);
            $this->id = $this->data['idDomeny'];
		    $this->name = $this->data['domena'];
            $this->url = $this->data['url'];
            $this->relative_url = $this->data['relative_url'];
            
            $d = $db->query("SELECT idModulu FROM ".TABLE_DOMENY_MODULY." WHERE idDomeny='".$this->id."'");
            $this->data['moduly'] = array();
            while($m = $db->getAssoc($d))
                $this->data['moduly'][] = $m['idModulu']; 
            
            $d = $db->query("SELECT idJazyka FROM ".TABLE_DOMENY_JAZYKY." WHERE idDomeny='".$this->id."'");
            $this->data['jazyky'] = array();
            while($m = $db->getAssoc($d))
                $this->data['jazyky'][] = $m['idJazyka']; 
            
            
            
            return true;
        }
        else
        return false;
        
			
		
    }
    
    //vraci hodnotu z pole data, pokud hodnota neexistuje vraci false
    public function get($key = "")
    {
        return isset($this->data[$key]) ? $this->data[$key] : false;
    }
    
    
	//nastavi novou aktivni domenu
	public function setActiveDomain($id_domeny){
		global $db;
		
	
		if($id_domeny == 0) 
            return false;
	 		else
	 		{
			$this->id = $_SESSION[$this->sn_id_domain] = $id_domeny;
			$this->name = $_SESSION[$this->sn_domain_name] = $this->getNameDomainById($id_domeny);
			
			$this->setData();
			} 
			
		if($this->sn_domain_dir) 
            $_SESSION[$this->sn_domain_dir] = $this->getDir();
            
		return true;
		
	}
	
	
		
	//overi zda ma prihlaseny uzivatel pravo spravovat vybranou aktualni domenu
	public function userDomains(){
	   
		if(isset($_SESSION['domeny'])) 
            $domains = $_SESSION['domeny']; 
            else 
            return false;
     
        return in_array($this->id, $domains);

		}
		
	//overi zda ma domena pravo vyuzivat dany modul cms
	public function userModul($idModulu, $moduly = null){
		
		if($moduly == null)
			$moduly = $this->data['moduly'];
			
		if($idModulu < 1 || $moduly=='') 
            return false;
            
	    return in_array($idModulu, $moduly);
        
		}
		
	//overi zda ma prihlaseny uzivatel pravo spravovat vybranou domenu s danym ID
	public function userDomainsById($idDomain){
		if(isset($_SESSION['domeny'])) 
            $domeny = $_SESSION['domeny']; 
            else 
            return false;
            
		 return in_array($idDomain, $domeny);

		}
		
	
	//funkce vraci pole jmen domen, ktere ma uzivatel pravo spravovat	
	public function userDomainsToArray($arrayDomains){
		$pom_arr = array();
		$result = array();
		if($arrayDomains == '') 
            return $result;
            
		foreach($arrayDomains AS $id){
			 $name = $this->GetNameDomainById($id);
             if($name == "") continue;
             $result[$id] = $name;
		}
		return $result;
		
	}
	

	//vraci id aktualni domeny	
	public function getIdAktivDomain(){
		global $db;
        
        $domena = $_SERVER['HTTP_HOST'];
        $domenaSwww = GetDomainWithWww($domena);
        $domenaBezWww = GetDomainWithoutWww($domena);
		$data = $db->Query("SELECT idDomeny FROM ".TABLE_DOMENY." WHERE (domena = '".$domenaSwww."' OR domena='".$domenaBezWww."') AND zobrazit=1 LIMIT 1");
		if($db->numRows($data)==0){ 
			return 0; 
			}
			else 
			{
			$domena = $db->getAssoc($data);
			return $domena['idDomeny'];
			}
	}

		
	//vrati nazev domeny podle id domeny
	public function getNameDomainById($idDomain){
		global $db;
        
		if($idDomain<1) return false;
        
		$data = $db->Query("SELECT domena FROM ".TABLE_DOMENY." WHERE idDomeny=".$idDomain." LIMIT 1");
		if($db->numRows($data)==1){
			$d = $db->getAssoc($data);
			return $d['domena'];
		} 
        else 
        return false;
	}
	
	//vraci ID domeny podle zadaneho jmena domeny
	public function getIdDomainByName($nameDomain){
		global $db;
		if(trim($nameDomain)=='') return 0;
		$data = $db->Query("SELECT idDomeny FROM ".TABLE_DOMENY." WHERE domena='".$nameDomain."'");
		if($db->numRows($data)==0) 
            return 0;
			else 
            $result = $db->getAssoc($data);
		
		return $result['idDomeny'];
	}
	
	public function isCentralDomain(){
		return stristr($_SERVER['SERVER_NAME'], CENTRAL_DOMAIN);
		
	}
	
	//
	public function refresh($userDomains){
	   
        //zjisteni prvni domeny v poradi uzivatelskych domen
        //nastavi id prvni domeny kterou muze uzivatel spravovat

		$this->id = $_SESSION[$this->sn_id_domain] = count($userDomains)>0 ? $userDomains[0] : 0;;

		if($this->id == 0){
			return false;
			}
			else
			{
			$this->name = $_SESSION[$this->sn_domain_name] = $this->GetNameDomainByID($this->id);
            
			if($this->name == '') 
                return false;
			
			$this->setData();
			
			if($this->sn_domain_dir) 
                $_SESSION[$this->sn_domain_dir] = $this->getDir();
			}
            
        return true;
	}
	

	public function getDir($completePath=true){
		$id = get_sysint($this->id);

		$name = mb_eregi_replace("\.","_",$this->name);
		//$dir = $id."-".$name;
        
        $dir = $id;
		if($completePath) 
			return DIRNAME_DOMAINS.$dir."/";
			else
			return $dir;
	}
	
	//overi zda ma uzivatel pridelenu nejakou domenu
	public function haveAnyDomain($idUser){
		global $db;
		
		if($idUser<1) 
            return false;
            
		$data = $db->Query("SELECT * FROM ".TABLE_UZIVATELE_DOMENY." WHERE idUzivatele='".$idUser."'");
		return $db->numRows($data) > 0;
	}
    

}


?>