<?php

/**
 * @author Pavel Srom
 * @copyright 2014
 */


class C_PrivatePages{
    
    //id aktivni stranky
    private $page_id = 0;
    
    //id stranek ve vetvi stromu, kde se stranka s danym id nachazi
    private $all_pages = array();
    
    public function __construct($page_id = 0)
    {
        $this->page_id = $page_id;
        
        if(!MODULE_PRIVATE_PAGES)
            return;
        
        $this->set_all_pages($page_id);
        $this->all_pages = array_reverse($this->all_pages, true);
        
        //$this->sign_private_pages();
        //print_r($this->all_pages);
        $this->set_private_groups();
        //print_r($this->all_pages);
        
    }
    
    
    private function set_all_pages($idStranky = 0){
        global $db;
        
        $s = $db->get(TABLE_STRANKY,array("idRodice AS rodic","autentizace AS privatni"),"idStranky=".$idStranky);
        
        if($s !== false)
        {
            $this->all_pages[$idStranky] = array("id" => $idStranky, "privatni" => $s->privatni);
            
            if($idStranky == 0)
                return;
                else
                {
                
                $this->set_all_pages($s->rodic);
                }
        }

        
        return;                 
        
    } 
    
    //nastavi priznak privatni u stranek
    /*
    private function sign_private_pages()
    {
        global $links;
        
        foreach($this->all_pages AS $ids => $p)
        {
            $this->all_pages[$ids]["privatni"] = $links->get($ids, "autentizace"); 
        }
        
        
    }
    */
    
    private function set_private_groups()
    {
        global $db;
        
        foreach($this->all_pages AS $ids => $p)
        {
            if($this->all_pages[$ids]["privatni"] == 0)
                continue;
                
            $d = $db->query("SELECT r.idSkupiny, s.nazev 
                FROM ".TABLE_STRANKY_SKUPINY." AS r
                LEFT JOIN ".TABLE_UZIVATELE_SKUPINY." AS s ON r.idSkupiny = s.idSkupiny
                WHERE r.idStranky = ".$ids."
                GROUP BY r.idSkupiny 
                ");
            
            $this->all_pages[$ids]["skupiny"] = array();    
            while($s = $db->getObject($d))
                $this->all_pages[$ids]["skupiny"][] = array("id" => $s->idSkupiny, "nazev" => $s->nazev);
                 
        }
        
    }
    
    
    public function is_private()
    {
        $is_private = false;
        
        if(!MODULE_PRIVATE_PAGES)
            return $is_private;
        
        foreach($this->all_pages AS $ids => $p)
        {
            if($p['privatni'] == 1)
                return true;
        }
                
    }
    
    //vraci vsechny privatni skupiny dane stranky vcetne zdedenych
    public function get_groups()
    {
        $groups = array();
        
        foreach($this->all_pages AS $ids => $p)
        {
            if($p['privatni'] == 1)
                foreach($p['skupiny'] AS $s)
                    $groups[$s['id']] = $s['nazev'];
        }
        
        return $groups;
    }
    
    //vraci zdedene privatni skupiny
    public function get_inherited_groups()
    {
        $groups = array();
        
        foreach($this->all_pages AS $ids => $p)
        {
            if($p['privatni'] == 1 && $ids != $this->page_id)
                foreach($p['skupiny'] AS $s)
                    $groups[] = $s['id'];//$s['nazev'];
        }
        
        //print_r($groups);
        
        return $groups;
    }
    
    //vraci vsechny id
    public function get_ids()
    {
        $id = array();
        foreach($this->all_pages AS $ids => $p)
            $id[] = $ids;
            
        return $id;
    }
    
    //vraci true, pokud stranka zdedila privatnost od podrizene stranky
    function is_inherited_private()
    {
        $result = false;
        
        if(!MODULE_PRIVATE_PAGES)
            return $result;
        
        foreach($this->all_pages AS $ids => $p)
        {
            if($p['privatni'] == 1 && $ids != $this->page_id)
                return true;
        }
        
        //print_r($groups);
        
        return $result;
    }
    
    
    
    
    
    
    
    
}


?>