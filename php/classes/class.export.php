<?php

/**
 * @author Pavel Srom
 * @copyright 2010
 */

/*
class ExportXls{
	
	private $file;
	private $row;
	
	private $worksheet;
	
	public function ExportXls($nazev_listu='Kontakty'){ 
		include('excelwriter/Writer.php');
		$this->workbook = new Spreadsheet_Excel_Writer();    
	    $this->workbook->setVersion(8);         
	    $this->worksheet =& $this->workbook->addWorksheet($nazev_listu);
	    $this->worksheet->setInputEncoding("UTF-8");
	    
	    $this->row = 1;
	    
	    }
    
    public function AddRow($data){
    	foreach($data AS $col => $value)
    		$this->write($this->row, $col+1, $value);
    	
    }
    
    public function download($filename){
 		$this->workbook->send($filename);
   		$this->workbook->close();
   		
    	
    	
    }
    
    
    
}
*/	


class ExportXls {

	private $file;
	private $row;

	public function __construct() {
		$this->file = $this->__BOF();
		$this->row = 0;
		}

	private function __BOF() {
		//return pack("CCCCCCCC",0xd0,0xcf,0x11,0xe0,0xa1,0xb1,0x1a,0xe1);
		return pack("ssssss", 0x809, 0x8, 0x0, 0x10, 0x0, 0x0);
		}

	private function __EOF() {
		return pack("ss", 0x0A, 0x00);
		}

	private function __writeNum($row, $col, $value) {
		$this->file .= pack("sssss", 0x203, 14, $row, $col, 0x0);
		$this->file .= pack("d", $value);
		}

	private function __writeString($row, $col, $value ) {
		$L = strlen($value);
		$this->file .= pack("ssssss", 0x204, 8 + $L, $row, $col, 0x0, $L);
		$this->file .= $value;
		}

	private function writeCell($value,$row,$col) {
		if(is_numeric($value)) {
			$this->__writeNum($row,$col,$value);
			}
		elseif(is_string($value)) {
			$this->__writeString($row,$col,$value);
			}
		}

	public function addRow($data,$row=null) {
		//If the user doesn't specify a row, use the internal counter.

		if(!isset($row)) {
			$row = $this->row;
			$this->row++;
			}
		for($i = 0; $i<count($data); $i++) {
			$cell = $data[$i];
			$this->writeCell($cell,$row,$i);
			}
		}

	public function download($filename) {
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Content-Type: application/force-download");
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");
		header("Content-Disposition: attachment;filename=".$filename);
		header("Content-Transfer-Encoding: binary ");
		$this->write();
		}

	private function write() {
		echo $file = $this->file.$this->__EOF();
		}

	} 

class ExportCsv {
	private $file;
	private $delimiter;
	
	public function __construct(){
		$this->file = "";
		$this->delimiter = ";";
		
	}
	
	private function __EOL() {
		return "\r\n";
		}
	
	public function AddRow($data){
		$i=1;
		$c = count($data);
		foreach($data AS $p){
			if(stristr($p, $this->delimiter) === FALSE){ 
				$this->file .= $p;
				}
				else{
				$this->file .= "'".$p."'";
				}
			if($i != $c)
				$this->file .= $this->delimiter;
				
			$i++;
			}
		$this->file .= $this->__EOL();
		
	}
	
	public function download($filename){
		
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Content-Type: application/force-download");
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");
		header("Content-Disposition: attachment;filename=".$filename);
		header("Content-Transfer-Encoding: binary ");
		
		$this->write();
		exit;

	}
	
	private function write() {
		echo $this->file;
		}
		
}

class ExportXml{
	
	private $file;
	
	private $rootName;
	private $itemName;
	
	public function __construct($rootName, $itemName){
		$this->rootName = $rootName;
		$this->itemName = $itemName;
		$this->file = "<?xml version='1.0' encoding='utf-8'?>\n";
		$this->file = $this->file .= "<".$this->rootName.">\n"; 
		
		
		
	}
	
	public function addRow($row){
		$this->file .= "\t<".$this->itemName.">\n"; 
		foreach($row AS $i => $v){
			$this->file .= "\t\t<".$i.">".$v."</".$i.">\n";
		}
		$this->file .= "\t</".$this->itemName.">\n"; 
	}
	
	public function write(){
		echo $this->file."</".$this->rootName.">\n";
		
	}
	
	public function download($filename){
		
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Content-Type: application/force-download");
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");
		header("Content-Disposition: attachment;filename=".$filename);
		header("Content-Transfer-Encoding: binary ");
		
		$this->write();
		exit;

	}
	
	
}

?>