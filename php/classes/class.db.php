<?php
class C_Db{
	
	private  $host;
	private  $login;
	private  $password;
	private  $db_name;
    private  $port;
	
    //pocet provedenych sql dotazu
	private $count_queries;
    
    //posledni sql dotaz
    private $last_query;
    
    //zapne ukladani pomalych sql dotazu do db
	private $debug = false;
    
    //vraci dobu trvani posledniho sql dotazu v sekundach
    private $seconds = false;
    
    private $show_error = false;
    
    private $connection = null;
    
	public function __construct(){
	   
       $iso = isOnline();
       
       $this->host = $iso ? DB_SERVER : LOCAL_DB_SERVER;
	   $this->login = $iso ? DB_USER : LOCAL_DB_USER;
	   $this->password = $iso ? DB_PASS : LOCAL_DB_PASS;
	   $this->db_name = $iso ? DB_NAME : LOCAL_DB_NAME;
       $this->port = 3306;//$iso ? DB_PORT : LOCAL_DB_PORT;         
	   $this->count_queries = 0;
	   $this->last_query = "";
       $this->debug = DEBUG_SQL_QUERY;
       
	}
	
    public function show_error($val = true)
    {
        $this->show_error = $val;
    }
    
	//připojení k databázi
	public function connect(){
		$this->connection = mysqli_connect($this->host, $this->login, $this->password, $this->db_name, $this->port);
        
		if(!$this->connection) 
            return 0;
            
		$this->set_db($this->db_name);
		
		return $this->connection;
		
	}
    
    //nastavi db
    public function set_db($db_name)
    {
        $this->db_name = $db_name;
        $result = mysqli_select_db($this->connection, $db_name);
		if(!$result) 
            return 0;

		$this->Query("SET NAMES 'utf8'");
		$this->Query("SET CHARACTER SET utf8");
		$this->Query("SET character_set_client=utf8");
		$this->Query("SET character_set_connection=utf8");
		$this->Query("SET character_set_results=utf8");
        $this->query("SET SESSION sql_mode=''");
        
        
    }
    
    //zapne / vypne debugovani
    public function set_debug($debug = true)
    {
        $this->debug = $debug;
    }
    
    //vraci posledni sql dotaz
    public function get_last_query()
    {
        return $this->last_query;
    }
    
    //vraci pocet provedenych sql dotazu
    public function get_count_queries()
    {
        return $this->count_queries;
    }
    
    //vraci pocet sekund trvani posledniho sql dotazu
    public function get_seconds()
    {
        return $this->seconds;
    }
    
    //vraci nazev db serveru
    public function getHost()
    {
        return $this->host;
    }
    
    //vraci login
    public function getLogin()
    {
        return $this->login;
    }
    
    //vraci heslo
    public function getPassword()
    {
        return $this->password;
    }
    
    //vraci nazev databaze
    public function getDbName()
    {
        return $this->db_name;
    }
	
    //formatuje microtime
    private function microtime_float()
    {
        list($usec, $sec) = explode(" ", microtime());
        return ((float)$usec + (float)$sec);
    }
    
    //provadi sql dotaz
	public function query($query){
	   $this->last_query = $query;
       
        $start = $this->microtime_float();
        $result = @mysqli_query($this->connection, $query);
        $end = $this->microtime_float();
        
        $this->seconds = round(($end - $start) / 1, 3);
        if($this->debug)
        {
            
            $sql_debug = "INSERT INTO ".TABLE_CMS_DEBUG." (popis,doba,datum) VALUES ('".$query."',".$this->seconds.",NOW())";
            mysqli_query($this->connection, $sql_debug);
            //echo $this->seconds."<br />";
        }
        
        
		$this->count_queries++;
		if(!$result) {
			echo "<p>Omlouváme se, ale na stránce došlo k chybě. Záznam o této chybě byl zaslán administrátorovi. Uděláme maximum, aby se tato chyba přístě neopakovala.</p>";
            if($this->show_error)
                echo $query."<br />".mysqli_errno($this->connection)."<br />".mysqli_error($this->connection);
                else
			     sql_error_handler($query, mysqli_errno($this->connection), mysqli_error($this->connection));
			exit;
		}else return $result;
		
	}
	
    //odpojedni od db
	public function disconnect(){
		return @mysqli_close($this->connection);
	}
	
    //vraci vysledek sql dotazu jako pole objektu
	public function getObject($record){
		$result = @mysqli_fetch_object($record);
		return $result;
		
	}
        
	//vraci vysledek sql dotazu jako asociativni pole
    public function getAssoc($record){
		$result = @mysqli_fetch_assoc($record);
		return $result;
		
	}
    
    //vraci vysledek sql dotazu jako asociativni pole a cislovane pole
    public function getRow($record){
		$result = @mysqli_fetch_row($record);
		return $result;
		
	}

	//vraci pocet zaznamu sql dotazu
	public function numRows($record){
		return @mysqli_num_rows($record);
	}
    
    //vraci pocet zaznamu sql dotazu
    public function numFields($record){
		return @mysqli_num_fields($record);
	}
	
	//ochrana dotazu sql proti sql injection
	public function secureString($string, $strip_tags = true){
		$string = Trim($string);
        
        if($strip_tags)
            $string = strip_tags($string);
        /*
		$string = html_entity_decode($string, ENT_QUOTES, "ISO-8859-1");
		$string = preg_replace('/&#(\d+);/me',"chr(\\1)", $string);
		$string = preg_replace('/&#x([a-f0-9]+);/mei',"chr(0x\\1)", $string);
		if (get_magic_quotes_gpc()) $string = stripslashes($string);
		*/
		$string = mysqli_real_escape_string($this->connection, $string);
		return $string;	
	}
    
    //ochrana post promenne proti sql injection
	public function secure_post($key, $default_value = "", $strip_tags = true){
	   $post_key = get_string_between($key,"[","]");
       
        
        if($post_key != "" && $post_key != $key)
        {
            $post_name = rtrim($key,"[".$post_key."]");
            
            $string = isset($_POST[$post_name][$post_key]) ? trim($_POST[$post_name][$post_key]) : $default_value; 
        }
        else
		  $string = isset($_POST[$key]) ? Trim($_POST[$key]) : $default_value;
        
        if($strip_tags)
            $string = strip_tags($string);

		$string = mysqli_real_escape_string($this->connection, $string);
		return $string;	
	}
    
    //uvolni pamet
    public function free($record)
    {
        mysqli_free_result($record);
    }
    
    //vraci posledni ulozene id
    public function lastId()
    {
        return mysqli_insert_id($this->connection);
    }
    
    /**
     * vrací ukazatel dotazu na začátek
     */
    public function dataSeek($dat, $pocet=0)
    {
        return mysqli_data_seek($dat,$pocet); 
    }
    
    /**
     * Pocet radku az pomoci FOUND_ROWS
     */
    public function foundRows()
    {
      $sql = $this->getObject($this->query("SELECT FOUND_ROWS() as pocet"));
      return  $sql->pocet;
    }
    
    //vlozi zaznamy do db
    public function insert($table, $arrayValues = array())
    {
       $values='';
       foreach ($arrayValues as $key => $value)
       	{
    	if ($value==='null')
        	$values.= "null, ";
    	elseif ($value===0)
        	$values.= "0, ";
    	elseif ($value=='now')
        	$values.= "now(), ";
    	else
    		{
    		$values.="'".mysqli_real_escape_string($this->connection, $value)."', ";
    		}
    	}
    
    
      $values = substr($values, 0, -2);
      $this->query("INSERT INTO $table (" . implode(", ", array_keys($arrayValues)) . ") VALUES (".$values.")");
    
    }

    public function increment($table, $col_name, $number = 1, $where = "")
    {

        if($where != '')
            $where = "WHERE " .$where;

        $this->query("UPDATE $table SET ".$col_name." = ".$col_name." + ".intval($number)." ".$where);
    }

    public function decrement($table, $col_name, $number = 1, $where = "")
    {

        if($where != '')
            $where = "WHERE " .$where;

        $this->query("UPDATE $table SET ".$col_name." = ".$col_name." - ".intval($number)." ".$where);
    }
    
    
    /**
     * Delete
     **/
    //smaze zaznam z db
    public function delete($table, $where)
    {
        
      if(!strstr($where,"WHERE"))
        $where = "WHERE ".$where;
          
      $this->query("DELETE FROM $table $where");
    
    }
    
    /**
     * Update
     **/
     //aktualizuje zaznamy v db
    public function update($table, $arrayValues, $where)
    {
      $query  = "UPDATE $table SET ";
      foreach ($arrayValues as $column => $value)
       	{
    	if ($value==='null')
        	$query .= "$column = null, ";
    	elseif ($value===0)
        	$query .= "$column = 0, ";
    	elseif ($value=='now')
        	$query .= "$column = now(), ";
        else
        	$query .= "$column = '".mysqli_real_escape_string($this->connection, $value)."', ";
       	}
    
       $query = substr($query, 0, -2);
       $query = $query . " WHERE " . $where;
       $query = @$this->query($query);
    
       return $query;
    }
    
    
    //vraci polozku z dane tabulky
    //pokud neni zaznam nalezen vraci false
    public function get($table, $items, $where, $limit=1)
    {
        
        
        if(is_array($items))
            $sel = implode(",",$items);
            else
            $sel = $items." AS item";
        
        
        $data = $this->query("SELECT ".$sel." FROM ".$table." WHERE ".$where." LIMIT ".$limit);
        
        if($this->numRows($data)==0) return false;
        
        $item = $this->getObject($data);
        
        if(is_array($items))
            return $item;
            else
            return $item->item;
        
            
        
    }

    public function optimize($table)
    {
        $this->query("OPTIMIZE TABLE ".$table);
    }

    public function repair($table)
    {
        $this->query("REPAIR TABLE ".$table);
    }






	
}
?>