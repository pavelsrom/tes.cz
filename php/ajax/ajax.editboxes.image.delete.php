<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!$login_obj->UserPrivilege('content_delete') || !defined("SECURITY_CMS")) exit;

$id = get_int_post('id');

if($id <= 0) 
    exit;
			
$data = $db->Query("SELECT obrazek, nazev FROM ".TABLE_EDITBOXY."
				WHERE idDomeny=".$domain->getId()."
					AND idEditboxu=".$id."
                    AND idJazyka=".WEB_LANG_ID."
				LIMIT 1");	
                
if($db->numRows($data)==0 || $id == 0) 
    exit;
    
$obr = $db->getAssoc($data);
$nazev = $obr['nazev'];
if($obr['obrazek']!='')
{
    @unlink(PRE_PATH.$domain->getDir().USER_DIRNAME_EDITBOXES_MAXI.$obr['obrazek']);
    @unlink(PRE_PATH.$domain->getDir().USER_DIRNAME_EDITBOXES_MINI.$obr['obrazek']);
}				
$db->Query("UPDATE ".TABLE_EDITBOXY." SET obrazek='' WHERE idEditboxu=".$id." AND idDomeny=".$domain->getId()." LIMIT 1");
			
$log->add_log('delete','editbox-fotka',$id,$nazev);

exit;

?>