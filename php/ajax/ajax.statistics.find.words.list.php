<?php

/**
 * @author Pavel Srom
 * @copyright 2010
 */

//ajaxovy skript na odhlaseni kontaktu z odberu newsletteru

if(!isset($_GET['module']) || !defined("SECURITY_CMS")) exit;

$vyraz = trim($db->secureString($_GET['vyraz']));
$module = trim($_GET['module']);

//nastaveni limitu
$limit = "";
if(isset($_GET['iDisplayLength'])){
    $dStart = "";
    if(isset($_GET['iDisplayStart']))
        $dStart = abs(intval($_GET['iDisplayStart'])).",";
        
    $limit = " LIMIT ".$dStart.abs(intval($_GET['iDisplayLength'])); 
}


//trideni podle sloupcu
$order = "";
if(isset($_GET['iSortCol_0'])){
    $s = abs(intval($_GET['iSortCol_0']));
    if($s == 0) $order = "vyraz";
    elseif($s == 1) $order = "cetnost";
    else $order = "cetnost";
}

if($order != '') 
    $order = "ORDER BY ".$order." ".(isset($_GET['sSortDir_0'])?$_GET['sSortDir_0']:'DESC');

//sestaveni podminky pro vyhledavani
$like = array();
if($vyraz != '')
{
    $like[] = "vyraz LIKE '".$vyraz."%'";
}
    

if(count($like) > 0)
    $where = "WHERE ".implode(" OR ",$like)." AND idJazyka=".WEB_LANG_ID;
    else
    $where = "WHERE idJazyka=".WEB_LANG_ID;

$data_pocet = $db->Query("SELECT COUNT(idVyrazu) AS celkem FROM ".TABLE_VYHLEDAVANI_VYRAZY." ".$where);
$iTotal_arr = $db->getAssoc($data_pocet);
$iTotal = $iTotal_arr['celkem'];

$data = $db->Query("SELECT SQL_CALC_FOUND_ROWS vyraz, cetnost   
				FROM ".TABLE_VYHLEDAVANI_VYRAZY." 
				".$where." 
                ".$order."
                ".$limit."
                ");


$iFilteredTotal_data = $db->Query("SELECT FOUND_ROWS()");
$iFilteredTotal_arr = $db->getRow($iFilteredTotal_data);
$iFilteredTotal = intval($iFilteredTotal_arr[0]);


$tr = array();
while($c = $db->getAssoc($data))
{
    $vyraz = $c['vyraz'];
    $cetnost = $c['cetnost'];
    $pom = array();
    
    $pom[] = json_encode(secureString($vyraz));
    $pom[] = json_encode($cetnost);
        
    $tr[] = "[".implode(',', $pom)."]";
    
}

echo "{";
echo '"sEcho": '.(intval($_GET['sEcho'])).', ';
echo '"iTotalRecords": '.intval($iTotal).', ';
echo '"iTotalDisplayRecords": '.($iFilteredTotal).', ';
echo '"aaData": ['.implode(',', $tr).'] }';


?>