<?php

/**
 * @author Pavel Srom
 * @copyright 2010
 */


if(!isset($_POST['idTematu']) || !defined("SECURITY_CMS")) exit;

$idTematu = intval($_POST['idTematu']);

if(!$login_obj->UserPrivilege('settings_view') && !$login_obj->UserPrivilege('settings_add')) exit;

$data = $db->Query("SELECT s.adresar, sp.* FROM ".TABLE_SMAJLICI." AS s 
	LEFT JOIN ".TABLE_SMAJLICI_POLOZKY." AS sp ON s.idTematu=sp.idTematu  
	WHERE s.idTematu=".$idTematu." 
		AND sp.zobrazit=1 
		AND s.zobrazit=1
	ORDER BY sp.priorita,sp.idSmajlika
		");
	
$result = "";
while($smile = $db->getAssoc($data)){
	
	$src = DIRNAME_SMILES.$smile['adresar']."/".$smile['soubor'];
	if(file_exists(PRE_PATH.$src))
		$result .= "<img class='smile' src='".RELATIVE_PATH.$src."' alt='".$smile['nazev']."' title='".$smile['nazev']."'/>&nbsp;&nbsp;";
	}

echo $result;
exit;


?>