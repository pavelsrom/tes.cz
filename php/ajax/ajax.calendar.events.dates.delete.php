<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */
 
 
if(!$login_obj->UserPrivilege('content_delete') || !defined("SECURITY_CMS")) exit;


$id = get_int_post('id');
$idAkce = get_int_post('id_akce');

if($id <= 0 || $idAkce < 0 || !$object_access->has_access($idAkce)) exit;

$nazev = $db->get(TABLE_STRANKY,'nazev',"idStranky=".$idAkce." AND idDomeny=".$domain->getId()." AND idJazyka=".WEB_LANG_ID." AND typ='akce'");

if($nazev == "") exit;
  
$log->add_log('edit-content',"kalendar-akci",$idAkce,$nazev);

$db->delete(TABLE_KALENDAR_TERMINY, "WHERE idTerminu=".$id." LIMIT 1");

exit;


?>