<?php

/**
 * @author Pavel Srom
 * @copyright 2010
 */

//ajaxovy skript na odhlaseni kontaktu z odberu newsletteru

if(!isset($_GET['module']) || !defined("SECURITY_CMS")) exit;

$vyraz = trim($db->secureString($_GET['vyraz']));
$module = trim($_GET['module']);
$idRubriky = get_int_request("rubrika");;
$top_clanek = get_int_request("top");
$stav = isset($_GET['stav']) ? $_GET['stav'] : "";
$tagy = $db->secureString($_GET['tagy']);


//nastaveni limitu
$limit = "";
if(isset($_GET['iDisplayLength'])){
    $dStart = "";
    if(isset($_GET['iDisplayStart']))
        $dStart = abs(intval($_GET['iDisplayStart'])).",";
        
    $limit = " LIMIT ".$dStart.abs(intval($_GET['iDisplayLength'])); 
}



//trideni podle sloupcu
$order = "";
if(isset($_GET['iSortCol_0'])){
    $s = abs(intval($_GET['iSortCol_0']));
    if($s == 0) $order = "s.idStranky";
    elseif($s == 1) $order = "s.nazev";
    elseif($s == 2) $order = "rubrika";
    elseif($s == 4) $order = "s.datum";
    elseif($s == 5) $order = "prumerne_hodnoceni";
    elseif($s == 6) $order = "s.stav";    
    elseif($s == 7) $order = "s.doMenu";
    elseif($s == 8) $order = "s.diskuze";
    elseif($s == 9) $order = "s.zobrazit";
    elseif($s == 10 && MODULE_ARTICLES_ARCHIVE) $order = "s.archivovat";
    else $order = "s.idStranky";
}

if($order != '') 
    $order = "ORDER BY ".$order." ".(isset($_GET['sSortDir_0'])?$_GET['sSortDir_0']:'');

//echo $order;

//sestaveni podminky pro vyhledavani
$like = array();
if($idRubriky > 0)
{
    $like[] = "s.idRodice=".$idRubriky;
}
if($vyraz != '')
{
    $like[] = "(s.idStranky LIKE '".$vyraz."%' OR s.nazev LIKE '%".$vyraz."%')";
}
if($stav != '')
{
    $like[] = "s.stav = '".$stav."'";
}
if($top_clanek >= 0)
{
    $like[] = "s.doMenu = '".$top_clanek."'";
}
    


$where = "WHERE s.idJazyka = ".WEB_LANG_ID." AND s.typ='clanek' AND s.idDomeny=".$domain->getId().((count($like) > 0) ? " AND ".implode(" AND ",$like) : "");

$data_pocet = $db->Query("SELECT COUNT(idStranky) AS celkem FROM ".TABLE_STRANKY." AS s ".$where);
$iTotal_arr = $db->getAssoc($data_pocet);
$iTotal = $iTotal_arr['celkem'];


        
$data = $db->Query("SELECT SQL_CALC_FOUND_ROWS s.*, DATE_FORMAT(s.datum,'%d.%m.%Y') AS datum, r.nazev AS rubrika, s.doMenu AS top_clanek,
                IF(s.pocetHlasovani > 0, ROUND(s.pocetHlasu / s.pocetHlasovani, 1), 0) AS prumerne_hodnoceni, j.jazyk
				FROM ".TABLE_STRANKY." AS s
                LEFT JOIN ".TABLE_JAZYKY." AS j ON s.idJazyka = j.idJazyka
                LEFT JOIN ".TABLE_STRANKY." AS r ON s.idRodice=r.idStranky
				".$where."
				GROUP BY s.idStranky
				".$order."
                ".$limit."
				");


$iFilteredTotal_data = $db->Query("SELECT FOUND_ROWS()");
$iFilteredTotal_arr = $db->getRow($iFilteredTotal_data);
$iFilteredTotal = intval($iFilteredTotal_arr[0]);


$dis_content = !$login_obj->UserPrivilege('content_view');
$dis_delete = !$login_obj->UserPrivilege('content_delete');

$vsechny_stavy = array('rozpracovano' => TROZPRACOVANO, 'kontrola' => TKE_KONTROLE, 'zamitnuto' => TZAMITNUTO, 'hotovo' => THOTOVO);
$vsechny_stavy_barvy = array('rozpracovano' => 'gray', 'kontrola' => 'orange', 'zamitnuto' => 'red', 'hotovo' => 'green');

$tr = array();
while($c = $db->getAssoc($data))
{
    $ids = $c['idStranky'];
    $idr = $c['idRodice'];
           
    //nacteni tagy
    $d = $db->query("SELECT t.idStranky, t.nazev, IF(t.nazev LIKE '%".$tagy."%',1,0) AS existuje
        FROM ".TABLE_STRANKY_TAGY." AS ts  
        LEFT JOIN ".TABLE_STRANKY." AS t ON ts.idTagu = t.idStranky
        WHERE ts.typObjektu = 'stranka'
            AND ts.idObjektu = ".$ids."
        ORDER BY t.nazev
        ");
    $tagy_clanku = array();
    $splnuje_podminku = $tagy == "";
    while($t = $db->getObject($d))
    {
        $tagy_clanku[$t->idStranky] = $t->nazev;
        if($t->existuje == 1)
            $splnuje_podminku = true;
    }
    
    if(!$splnuje_podminku)
    {
        $iTotal--;
        continue;
    }

    $private_pages = new C_PrivatePages($ids);
    $privatni = ano_ne($private_pages->is_private(),implode(", ", $private_pages->get_groups())); 

    $zobrazit_diskuzi = $c['diskuze'] == 1 && MODULE_DISCUSSION ? " a href=".get_link("discussions","diskuze",$ids)." title='".TZOBRAZIT_DISKUZNI_PRISPEVKY."'>".TZOBRAZIT_DISKUZNI_PRISPEVKY."</a>" : "";
    
    $pom = array();
    $pom[] = json_encode($ids);
    $pom[] = json_encode(secureString($c['nazev']));
    $pom[] = json_encode(secureString($c['rubrika']));
    $pom[] = json_encode(secureString(implode(", ",$tagy_clanku)));
    $pom[] = json_encode(secureString($c['datum']));
    $pom[] = json_encode(intval($c['prumerne_hodnoceni'])." / ".intval($c['pocetHlasovani']));
    $pom[] = json_encode("<span style='color: ".$vsechny_stavy_barvy[$c['stav']]."'>".$vsechny_stavy[$c['stav']]."</span>");
    $pom[] = json_encode(ano_ne($c['top_clanek']));
    $pom[] = json_encode(ano_ne($c['diskuze']).$zobrazit_diskuzi);
    
    
    
    
    $pom[] = json_encode($privatni);
    $pom[] = json_encode(ano_ne($c['zobrazit']));
    
    if(MODULE_ARTICLES_ARCHIVE)
        $pom[] = json_encode(ano_ne($c['archivovat']));
    
    $ikony = "";
    
    $ikony = "<a href='".urlPage($c['url'],$ids, $c['jazyk'])."' target='_blank'>".icon('link')."</a>";
    
    
    if($dis_content)
        $ikony .= icon_disabled('content');
        else
        $ikony .= "<a href='".get_link('articles','rubriky',$idr,'edit-content',$ids)."'>".icon('content')."</a>";
    
    if($dis_delete || !$object_access->has_access($ids))
        $ikony .= icon_disabled('delete');
        else
        $ikony .= "<a href='#' id='delete".$ids."' class='delete'>".icon('delete')."</a>";
        
    $pom[] = json_encode($ikony);
        
    $tr[] = "[".implode(',', $pom)."]";
    
}

echo "{";
echo '"sEcho": '.(intval($_GET['sEcho'])).', ';
echo '"iTotalRecords": '.$iTotal.', ';
echo '"iTotalDisplayRecords": '.($iFilteredTotal).', ';
echo '"aaData": ['.implode(',', $tr).'] }';


?>