<?php

/**
 * @author Pavel Srom
 * @copyright 2014
 */


include("../classes/class.form.admin.php");
include("../functions/functions.inputs.php");


$idPage = get_int_post("id_page");

//id aktualni stranky v inline editaci, posila se kvuli tomu ze novy clanek (tj. clanek s id 0) lze vytvorit i z clankove stranky tak i z libovolneho clanku. Na zaklade teto informace se automaticky predvyplnuje pole pro rubriku
$idPageINE = get_int_post('id_ine_page'); 

if($idPage > 0)
    $typ = $links->getType($idPage);
    else
    $typ = get_post('typ');
    

if($action == 'pridat-stranku')
    $idPage = 0;

$page = array();
$page['nazev'] = "";
$page['nadpis'] = "";
$page['url'] = "";
$page['title'] = "";
$page['description'] = "";
$page['idProfiluPanelu'] = 0;
$page['typ'] = $typ;
$page['zobrazit'] = 0;
$page['idRodice'] = 0;
$page['idAnkety'] = 0;
$page['idFormulare'] = 0;
$page['diskuze'] = 0;
$page['autentizace'] = 0;





if($idPage > 0) {
    $data = $db->Query("SELECT s.*, j.jazyk,
				IF(s.nadpis='', s.nazev, s.nadpis) AS nadpis,
                IF(s.od IS NULL, '', DATE_FORMAT(s.od,'%d.%m.%Y')) AS publikovat_od,
				IF(s.do IS NULL, '', DATE_FORMAT(s.do,'%d.%m.%Y')) AS publikovat_do,
				IF(s.datum IS NULL, '', DATE_FORMAT(s.datum,'%d.%m.%Y')) AS datum
			FROM ".TABLE_STRANKY." AS s
            LEFT JOIN ".TABLE_JAZYKY." AS j ON s.idJazyka = j.idJazyka 
			WHERE idStranky=".$idPage." 
                AND (typ='galerie-slozka' OR typ='galerie')
				AND idDomeny=".$domain->getId()."
                AND s.idJazyka=".WEB_LANG_ID."
			LIMIT 1");
			
			
    $page = $db->getAssoc($data);
}

$idRoot = $links->getIdByType('stranka-galerie',WEB_LANG_ID);

$slozky_selectbox = get_gallery($idPage);
$slozky_selectbox = array($idRoot => "-- ".TKOREN_GALERIE." --") + $slozky_selectbox;

$form = new Form();
$form->allow_upload();
$form->set_jquery_ui_css();

$form->add_section(TZAKLADNI_UDAJE, "home");
$form->add_text(TNAZEV,'nazev',$page['nazev'],0,true,"",'nazev');
$form->add_text(THLAVNI_NADPIS_STRANKY,'nadpis',$page['nadpis'],3,true,"",'nadpis');
$form->add_selectbox(TZANORENI_DO_SLOZKY, 'idRodice',$page['idRodice'],$slozky_selectbox,267);

if($idPage > 0)
{       
    $form->add_hidden('typ',$page['typ']);
}
else
{
    $typy = array("galerie" => TGALERIE, "slozka-galerie" => TSLOZKA_GALERIE);
    $form->add_radiobuttons(TTYP,'typ',$page['typ'],$typy); 
}

$form->add_radiobuttons(TZOBRAZOVAT,'zobrazit',$page['zobrazit']);    
$form->add_hidden('idStranky',$idPage); 


     
            
if(MODULE_PRIVATE_PAGES)
    $form->add_radiobuttons(TPRIVATNI_STRANKA, 'autentizace', $page['autentizace'], array(1 => TANO, 0 => TNE), 5)->set_class_tr("prazdna-stranka-hidden");
    else
    $form->add_hidden('autentizace',$page['autentizace']);

        
$form->add_section(TDOPLNUJICI_OBSAH,"obsah");


if(MODULE_INQUIRY)
{
    $d = $db->Query("SELECT idAnkety AS id, nazev FROM ".TABLE_ANKETY." WHERE idDomeny=".$domain->getId());
    
    $sel = array(0 => "-- ".TVYBERTE." --");
    $pocet = $db->numRows($d);
            
    while($s = $db->getAssoc($d))
        $sel[$s['id']] = $s['nazev'];
                
    if(count($sel) == 1){
        $text_neni = "";
        if($login_obj->UserPrivilege('content_add'))
            $text_neni .= tlacitko(get_link('inquiry',"ankety",0,'new'));
        
        $form->add_plain_text(TANKETA, $text_neni)->set_class_tr("prazdna-stranka-hidden");    
        }
        else
        $form->add_selectbox(TANKETA, 'idAnkety', $page['idAnkety'], $sel)->set_class_tr("prazdna-stranka-hidden");
            
}
else
    $form->add_hidden('idAnkety', $page['idAnkety']);
    
        
if(MODULE_DISCUSSION)
    $form->add_radiobuttons(TDISKUZE, 'diskuze', $page['diskuze'], array(1 => TANO, 0=>TNE))->set_class_tr("prazdna-stranka-hidden");
    else
    $form->add_hidden('diskuze', $page['diskuze']);
   
   
if($idPage > 0 && $typ == 'galerie')
{
    $form->add_section(TFOTKY_V_GALERII, 'fotka'); 
    $form->add_file(TPRIDAT_FOTKY, 'fotky')->set_multiple();
         
    $data_fotky = $db->Query("SELECT g.*, j.jazyk, j.idJazyka
        FROM ".TABLE_STRANKY." AS g
        LEFT JOIN ".TABLE_JAZYKY." AS j ON g.idJazyka = j.idJazyka 
        WHERE idRodice=".$idPage." 
            AND typ='galerie-fotka' 
            AND g.idJazyka=".WEB_LANG_ID."
        ORDER BY priorita, idStranky DESC");  
    
    $img = "";
    if($db->numRows($data_fotky) == 0)
    {
        $img = TNENI_ULOZEN_ZADNY_OBRAZEK;
    }
    else
    {	
        $img .= "<div id='ine_sortable'>";
        while($obr = $db->getAssoc($data_fotky))
        {
            $img .= "<div class='ine_fotka' id='f".$obr['idStranky']."'>";
            $img .= "<img src='".RELATIVE_PATH.$domain->getDir().USER_DIRNAME_GALLERY_MINI.$obr['obrazek']."' alt='' />";
            $img .= "<input type='text' value='".$obr['nazev']."' class='input ui-widget-content ui-corner-all' title='".TNAZEV."' name='fotka_nazev[".$obr['idStranky']."]'/><br />";
            $img .= "<textarea class='input ui-widget-content ui-corner-all' title='".TPOPIS."' name='fotka_popis[".$obr['idStranky']."]'>".strip_tags($obr['obsah'])."</textarea>";
            $img .= "<a href='".RELATIVE_PATH.$domain->getDir().USER_DIRNAME_GALLERY_MAXI.$obr['obrazek']."' target='_blank'>".TDETAIL_FOTKY."</a> | <a href='#' class='ine_delete'>".TSMAZAT."</a>";
            $img .= "</div>";
        }
        $img .= "</div>";
        $img .= "<div class='ine_cleaner'></div>";
    }
    
    $form->add_plain_text("",'<div id="ine_progressbar"><div class="progress-label">'.TCEKEJTE_PROSIM.'</div></div><div id="ine_protokol"></div>'); 
    $form->add_plain_text(TNAHRANE_FOTKY);
    $form->add_note($img)->set_class_td("ine_fotky");

}
   
$data_panely = $db->Query("SELECT idProfilu,nazev FROM ".TABLE_PANELY_SABLONY." WHERE idDomeny=".$domain->getId()." AND zobrazit=1 AND idJazyka=".WEB_LANG_ID." ORDER BY vychozi DESC ");
                
$profily = array();
while($profil = $db->getAssoc($data_panely))
    $profily[$profil['idProfilu']] = $profil['nazev'];

$form->add_section(TSABLONA_PANELU, 'panel-setting')->set_class_tr("prazdna-stranka-hidden");
$form->add_selectbox(TSABLONA_PANELU, 'idProfiluPanelu', $page['idProfiluPanelu'], $profily, 135)->set_class_tr("prazdna-stranka-hidden");     
    
    
if($login_obj->UserPrivilege('seo'))
{
    $form->add_section(TSEO_PARAMETRY, 'seo', "", 8);
    $form->add_hidden('url', $page['url']);
    //$form->add_text(TURL_NAZEV, 'url', $page['url'], 9, $idPage > 0);
    $form->add_text(TTITLE, 'title', $page['title'], 10,false,'','title');
    $form->add_textarea(TDESCRIPTION, 'description', $page['description'], 11);	
}
else
{
    $form->add_hidden('url', $page['url']);
    $form->add_hidden('title', $page['title'], 'title');
    $form->add_hidden('description', $page['description']);	
}       


$result = array(
    "html" => $form->get_html_code(),
    "url" => array(
        "save" => get_link_ajax('gallery','gallery.save.form',$idModulu),
        "upload" => get_link_ajax('gallery','photos.upload',$idModulu),
        "photo_gallery" => get_link_ajax('gallery','photos.delete',$idModulu),
        ),
    "messages" => array()
    );

echo array2json($result);
 
    
    
    
    
    
    
    


?>