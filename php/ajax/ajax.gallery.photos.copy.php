<?php

/**
 * @author Pavel Srom
 * @copyright 2010
 */


//skript kopiruje fotky z jedne galerie do druhe. Kopiruje nazvy a url adresy

if(!defined("SECURITY_CMS")) exit;

$idGalerie = get_int_post('id_gallery_target');
$idGalerie_zdroj = get_int_post('id_gallery_source');
$fotky = get_array_post('photos');

$result = array('ok' => "",'error' => ERROR_KOPIROVANI_FOTEK);

if($idGalerie==0 || $idGalerie_zdroj == 0 || count($fotky) == 0)
{
    
    echo array2json($result);
    exit;
} 

$zkopirovano = $celkem = 0;

foreach($fotky AS $id)
{
    $celkem++;
    $d = $db->query("SELECT * FROM ".TABLE_STRANKY." WHERE idDomeny='".$domain->getId()."' AND typ='galerie-fotka' AND idRodice='".$idGalerie_zdroj."' AND idStranky='".$id."' LIMIT 1");
    
    if($db->numRows($d) == 0) continue;
    
    $fotka = $db->getAssoc($d);
    
    //overeni zda uz fotka v databazi neexistuje
    $v = $db->query("SELECT idStranky 
        FROM ".TABLE_STRANKY." 
        WHERE idRodice='".$idGalerie."' 
            AND typ='galerie-fotka' 
            AND idDomeny='".$domain->getId()."' 
            AND obrazek='".$fotka['obrazek']."'
            AND idStranky!='".$id."'
        LIMIT 1");
    
    if($db->numRows($v) > 0) continue;
    
    $insert = array(
        "idDomeny"      => $domain->getId(),
        "idJazyka"      => WEB_LANG_ID,
        "idRodice"      => $idGalerie,
        "typ"           => "galerie-fotka",
        "nazev"         => $fotka['nazev'],
        "nazevProMenu"  => $fotka['nazevProMenu'],
        "nadpis"        => $fotka['nadpis'],
        "title"         => $fotka['title'],
        "doMenu"        => 0,
        "obrazek"       => $fotka['obrazek'],
        "idVytvoril"    => $login_obj->getId(),
        "jmenoVytvoril" => $login_obj->getName(),
        "datumVytvoril" => "now",
        'idAktualizoval'=> $login_obj->getId(),
        'jmenoAktualizoval' => $login_obj->getName(),
        'datumAktualizoval' => "now"   
        );
    
    $db->insert(TABLE_STRANKY, $insert);
    $lastId = $db->lastId();
    
    $url = get_url_nazev(0, $fotka['url']);
    
    $update = array(
        "url" => $url
        );
    
    $db->update(TABLE_STRANKY, $update, "idStranky=".$lastId);
    
    $log->add_log('create','galerie-fotka',$lastId, $fotka['nazev']);
    $zkopirovano++;
}

$result['ok'] = sprintf(OK_ZKOPIROVANO,$zkopirovano, $celkem);
echo array2json($result);




?>