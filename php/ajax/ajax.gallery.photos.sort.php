<?php

/**
 * @author Pavel Srom
 * @copyright 2010
 */



if(!isset($_POST['img']) || !isset($_POST['idGallery']) || !defined("SECURITY_CMS")) exit;

$obrazky = $_POST['img'];
$idGalerie = intval($_POST['idGallery']);

if($idGalerie==0 || count($obrazky)<2  || !$object_access->has_access($idGalerie)) exit;


if(!$login_obj->UserPrivilege('content_edit')) exit;


foreach($obrazky AS $i => $obr){
	
	$db->Query("UPDATE ".TABLE_STRANKY." SET priorita=".$i." WHERE idStranky=".$obr." AND idRodice=".$idGalerie." AND typ='galerie-fotka' AND idDomeny=".$domain->getId()." LIMIT 1");

	}

$nazev = $db->get(TABLE_STRANKY,'nazev','idStranky='.$idGalerie);
$log->add_log('edit-content','galerie',$idGalerie,$nazev);
	
exit;


?>