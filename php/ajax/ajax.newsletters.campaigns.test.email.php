<?php

/**
 * @author Pavel Srom
 * @copyright 2011
 */

//sleep(2);
//mb_internal_encoding("UTF-8");

if(!defined("SECURITY_CMS")) exit;

$text = get_post('text');
$html = get_post('html');
$prijemce = get_post('prijemce');
$predmet = get_post('predmet');
$osloveni_muz = get_post('osloveni_muz');
$osloveni_zena = get_post('osloveni_zena');
$osloveni_ostatni = get_post('osloveni_ostatni');
$idKampane = get_int_post('idKampane');

$messages = array("ok" => "","error" => "");
if(!TestEmail($prijemce, 250, false))
    $messages["error"] = ERROR_EMAIL_PRIJEMCE;


if($messages["error"] != '')
{
    echo json_encode($messages);
    exit;
}




require_once('../classes/class.newsletters.php');
$kampan = C_Campaign::get_instance($idKampane);

if($kampan->get_state() == 'rozpracovano')
{
    $kampan->set_custom_salutation($osloveni_muz, $osloveni_zena, $osloveni_ostatni);
    $kampan->set_content_html($html);
    $kampan->set_content_text($text);
    $kampan->set_subject($predmet);
}
$odeslano = $kampan->send($prijemce);


$ok = "";
if($odeslano)
	$messages["ok"] = OK_ODESLANO;
	else
    $messages["error"] = ERROR_ODESLANO;


$chyby = json_encode("");
if($messages["error"] != '')
    $chyby = json_encode("<span style='color: red;'>".$messages["error"]."</span>");

if($ok!='')
    $messages["ok"] = json_encode("<span style='color:green;'>".$ok."</span>");
    else
    $ok = json_encode("");    

echo array2json($messages);

?>