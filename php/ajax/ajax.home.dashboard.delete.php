<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */


if(!defined("SECURITY_CMS")) exit;

$id = get_int_post('id');

$data = $db->query("SELECT nastaveniDashboardLevy AS l, nastaveniDashboardPravy AS p FROM ".TABLE_UZIVATELE." WHERE idUzivatele=".$login_obj->getId());

if($db->numRows($data) == 0) exit;

$user = $db->getAssoc($data);
$l = str_replace("#".$id."#","#",$user['l']);
$p = str_replace("#".$id."#","#",$user['p']);

$update = array("nastaveniDashboardLevy" => $l, "nastaveniDashboardPravy" => $p);
$db->update(TABLE_UZIVATELE,$update,"idUzivatele=".$login_obj->getId());

exit;





?>