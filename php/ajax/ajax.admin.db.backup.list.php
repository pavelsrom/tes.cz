<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined("SECURITY_CMS")) exit;

$vyraz = isset($_GET['vyraz']) ? $db->secureString($_GET['vyraz']) : "";
$vyraz = GetGADate($vyraz);

//nastaveni limitu
$limit = "";
if(isset($_GET['iDisplayLength'])){
    $dStart = "";
    if(isset($_GET['iDisplayStart']))
        $dStart = abs(intval($_GET['iDisplayStart'])).",";
        
    $limit = " LIMIT ".$dStart.abs(intval($_GET['iDisplayLength'])); 
}


//trideni podle sloupcu
if(isset($_GET['iSortCol_0'])){
    $s = abs(intval($_GET['iSortCol_0']));
    if($s == 0) $order = "z.idZalohy";
    elseif($s == 1) $order = "z.jmenoUzivatele";
    elseif($s == 2) $order = "z.datum";
    elseif($s == 3) $order = "z.velikost";
    else $order = "z.idZalohy";
}

if($order != '') 
    $order = "ORDER BY ".$order." ".(isset($_GET['sSortDir_0'])?$_GET['sSortDir_0']:'');


//sestaveni podminky pro vyhledavani
$like = array();
if($vyraz != '')
{
    $like[] = "DATE_FORMAT(z.datum,'%Y-%m-%d') = '".$vyraz."'";
    
}

$where = "";
if(count($like) > 0)
    $where = "WHERE ".implode(" OR ",$like);
    

$data_pocet = $db->Query("SELECT COUNT(idZalohy) AS celkem FROM ".TABLE_ZALOHY_DB." AS z ".$where);
$iTotal_arr = $db->getAssoc($data_pocet);
$iTotal = $iTotal_arr['celkem'];

$data = $db->query("SELECT SQL_CALC_FOUND_ROWS z.idZalohy AS id, z.soubor, 
        DATE_FORMAT(z.datum,'%d.%m.%Y %H:%i') AS datum, z.velikost, z.jmenoUzivatele AS jmeno
        FROM ".TABLE_ZALOHY_DB." AS z 
        ".$where."  
        GROUP BY z.idZalohy 
        ".$order."
        ".$limit."
        ");

$iFilteredTotal_data = $db->Query("SELECT FOUND_ROWS()");
$iFilteredTotal_arr = $db->getRow($iFilteredTotal_data);
$iFilteredTotal = intval($iFilteredTotal_arr[0]);

$tr = array();
while($z = $db->getObject($data))
{
    $odkazy = "";
    
    if($login_obj->UserPrivilege('admin') || $login_obj->UserPrivilege('superadmin'))
        $odkazy .= "<a href='".RELATIVE_PATH.DIRNAME_DB_BACKUP.$z->soubor."' title='".TSTAHNOUT_ZALOHU."'>".icon('download')."</a>";
        else
        $odkazy .= icon_disabled('download');
    
    if($login_obj->UserPrivilege('superadmin'))
        $odkazy .= "<a href='#' class='obnova' id='obnova".$z->id."' title='".TNAHRADIT_DATABAZI_ZALOHOU."'>".icon('rescure')."</a>";
        else
        $odkazy .= icon_disabled('rescure');
    
    if($login_obj->UserPrivilege('settings_delete') && ($login_obj->UserPrivilege('admin') || $login_obj->UserPrivilege('superadmin')))
        $odkazy .= "<a href='#' class='delete' id='delete".$z->id."' title='".TSMAZAT."'>".icon('delete')."</a>";
        else
        $odkazy .= icon_disabled('delete');
    
    $pom = array();
    $pom[] = json_encode($z->id);
    $pom[] = json_encode($z->jmeno);
    $pom[] = json_encode($z->datum);
    $pom[] = json_encode(format_bytes($z->velikost)); 
    $pom[] = json_encode($odkazy);
        
    $tr[] = "[".implode(',', $pom)."]";
    
}

echo "{";
echo '"sEcho": '.(intval($_GET['sEcho'])).', ';
echo '"iTotalRecords": '.$iTotal.', ';
echo '"iTotalDisplayRecords": '.($iFilteredTotal).', ';
echo '"aaData": ['.implode(',', $tr).'] }';








?>