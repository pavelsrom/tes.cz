<?php

/**
 * @author Pavel Srom
 * @copyright 2015
 */


$id_stranky = get_int_request("id_page"); 
$s = $db->get(TABLE_STRANKY, array("nazev","typ","idRodice"),"idStranky=".$id_stranky." AND idDomeny=".$domain->getId());

$nazev_webu = $domain->getName();
$nazev_stranky = $s->nazev;
$vstup_do_administrace_url = $domain->getUrl()."admin";

$typ = $s->typ;
$rodic = $s->idRodice;

include("../functions/functions.ine.php");
include("../config/config.ine.php");

if($nazev_stranky == '')
    return;

$pocet_potomku = $db->get(TABLE_STRANKY,"COUNT(idStranky)","idRodice=".$id_stranky);

if($pocet_potomku > 0)
    $text = TNELZE_SMAZAT_STRANKA_OBSAHUJE_PODRIZENE_STRANKY." ".TSMAZTE_NEJDRIVE_PODSTRANKY_A_POTE_STRANKU_SMAZTE;
    else
    $text = TPO_SMAZANI_BUDETE_PRESMEROVANI_NA_UVODNI_STRANKU." ".sprintf(TOPRAVDU_CHCETE_SMAZAT_STRANKU,"<strong>".$nazev_stranky."</strong>");
    
    


$html = "<p class='ine_message'>".$text."</p><br /><br />";

if($pocet_potomku == 0)
    $html .= "<div><a href='#' class='tool_button delete_button'>".TSMAZAT_STRANKU."</a></div><br /><br />";
    



$result = array(
    "html" => $html,
    "url" => get_ine_odkaz_smazat_stranku($id_stranky, $typ, $config_modules)
        
);

echo array2json($result);


?>