<?php

/**
 * @author Pavel Srom
 * @copyright 2014
 */


include("../classes/class.form.admin.php");
include("../functions/functions.inputs.php");


$idPage = get_int_post("id_page");

//id aktualni stranky v inline editaci, posila se kvuli tomu ze novy clanek (tj. clanek s id 0) lze vytvorit i z clankove stranky tak i z libovolneho clanku. Na zaklade teto informace se automaticky predvyplnuje pole pro rubriku
$idPageINE = get_int_post('id_ine_page'); 


/*
if((!$login_obj->UserPrivilege('content_view') && $idStranky > 0)){
    $result["messages"]["error"] = ERROR_NEDOSTATECNA_PRAVA;
    echo array2json($result);
    exit;
} 
*/



if($action == 'pridat-stranku')
    $idPage = 0;

$page = array();
$page['nazev'] = "";
$page['typ'] = "clanek";
$page['nadpis'] = "";
$page['uvodni'] = 0;
$page['autentizace'] = 0;
$page['idRodice'] = 0;

if($idPage == 0)
{
    $idRubriky = $links->getType($idPageINE) == 'clanky' ? $idPageINE : 0;
    $idRubriky = $idRubriky == 0 && $links->getType($idPageINE) == 'clanek' ? $links->get($idPageINE, 'rodic') : 0;
    $page['idRodice'] = $idRubriky; 
}


$page['idProfiluPanelu'] = 0;
$page['zobrazit'] = 0;
$page['doMenu'] = 1;
$page['obsah'] = "";
$page['idGalerie'] = 0;
$page['idAnkety'] = 0;
$page['idFormulare'] = 0;
$page['diskuze'] = 0;
$page['url'] = "";
$page['title'] = "";
$page['description'] = "";
$page['nazevProMenu'] = "";
$page['autor'] = "";
$page['stav'] = "";
$page['poznamka'] = "";
$page['obrazek'] = "";
$page['datum'] = "";
$page['publikovat_od'] = "";
$page['publikovat_do'] = "";

 





if($idPage > 0) {
    $data = $db->Query("SELECT s.*, j.jazyk,
				IF(s.nadpis='', s.nazev, s.nadpis) AS nadpis,
                IF(s.od IS NULL, '', DATE_FORMAT(s.od,'%d.%m.%Y')) AS publikovat_od,
				IF(s.do IS NULL, '', DATE_FORMAT(s.do,'%d.%m.%Y')) AS publikovat_do,
				IF(s.datum IS NULL, '', DATE_FORMAT(s.datum,'%d.%m.%Y')) AS datum
			FROM ".TABLE_STRANKY." AS s
            LEFT JOIN ".TABLE_JAZYKY." AS j ON s.idJazyka = j.idJazyka 
			WHERE idStranky=".$idPage." 
                AND typ='clanek'
				AND idDomeny=".$domain->getId()."
                AND s.idJazyka=".WEB_LANG_ID."
			LIMIT 1");
			
			
    $page = $db->getAssoc($data);
    $page['publikovat_od'] = $page['publikovat_od'] == '00.00.0000' ? "" : $page['publikovat_od'];
    $page['publikovat_do'] = $page['publikovat_do'] == '00.00.0000' ? "" : $page['publikovat_do'];  
}


$form = new Form();
$form->allow_upload();
$form->set_jquery_ui_css();

$form->add_section(TZAKLADNI_UDAJE, "home");
$form->add_hidden('idStranky',$idPage); 

$form->add_text(TTITULEK, 'nazev', $page['nazev'], 83, true,"","nazev");

$data_rubriky = $db->query("SELECT idStranky, nazev FROM ".TABLE_STRANKY." WHERE typ='clanky' AND idDomeny=".$domain->getId()." ORDER BY nazev");
$select_rubriky = array(0 => "-- ".TVYBERTE." --");
while($r = $db->getAssoc($data_rubriky))
    $select_rubriky[$r['idStranky']] = $r['nazev']; 
    
$form->add_text(TAUTOR, 'autor', $page['autor']);
$form->add_selectbox(TRUBRIKA,'rubrika', $page['idRodice'],$select_rubriky, 0, true);
//$form->add_radiobuttons(TZOBRAZOVAT, 'zobrazit', $page['zobrazit'], array(1 => TANO, 0 => TNE),6);
    
    
$vsechny_stavy = array('rozpracovano' => TROZPRACOVANO, 'kontrola' => TKE_KONTROLE, 'zamitnuto' => TZAMITNUTO, 'hotovo' => THOTOVO);
//operovat se vsemi stavy muze jen opravneni editor/vydavatel a vyssi
if($login_obj->minPrivilege('editor'))
{
    $form->add_selectbox(TSTAV,'stav',$page['stav'],$vsechny_stavy, 402);
    $form->add_textarea(TPOZNAMKA_REDAKCE,'poznamka',$page['poznamka'],399);
}
else
{
    $stavy = array('rozpracovano' => TROZPRACOVANO, 'kontrola' => TKE_KONTROLE);
        
    //pokud stav neni povolen pro redaktora, pak se zobrazi stav v textove forme,aby jej nebyl mozne prepnout
    //stav hotovo a zamitnuto nemuze redaktor prepnout.
    if(isset($stavy[$page['stav']]))
        $form->add_selectbox(TSTAV,'stav',$page['stav'],$stavy);
        else
        {
            $form->add_plain_text(TSTAV, $vsechny_stavy[$page['stav']], 402);
            $form->add_hidden('stav',$page['stav']);
        }
        
    if($page['poznamka']!='')
        $form->add_plain_text(TPOZNAMKA_REDAKCE,$page['poznamka'],400);
        
    $form->add_hidden('poznamka',$page['poznamka']);
        
}

//kvuli porovnani stavu se uklada i stary stav
$form->add_hidden('stary_stav',$page['stav']);

$t = TCLANEK;
$help = "";        
$form->add_hidden('typ',$page['typ']);

     
            
if(MODULE_PRIVATE_PAGES)
    $form->add_radiobuttons(TPRIVATNI_STRANKA, 'autentizace', $page['autentizace'], array(1 => TANO, 0 => TNE), 5)->set_class_tr("prazdna-stranka-hidden");
    else
    $form->add_hidden('autentizace',$page['autentizace']);

        


$form->add_section(TDOPLNUJICI_OBSAH,"obsah");


if(MODULE_INQUIRY)
{
    $d = $db->Query("SELECT idAnkety AS id, nazev FROM ".TABLE_ANKETY." WHERE idDomeny=".$domain->getId());
    
    $sel = array(0 => "-- ".TVYBERTE." --");
    $pocet = $db->numRows($d);
            
    while($s = $db->getAssoc($d))
        $sel[$s['id']] = $s['nazev'];
                
    if(count($sel) == 1){
        $text_neni = "";
        if($login_obj->UserPrivilege('content_add'))
            $text_neni .= tlacitko(get_link('inquiry',"ankety",0,'new'));
        
        $form->add_plain_text(TANKETA, $text_neni)->set_class_tr("prazdna-stranka-hidden");    
        }
        else
        $form->add_selectbox(TANKETA, 'idAnkety', $page['idAnkety'], $sel)->set_class_tr("prazdna-stranka-hidden");
            
}
else
    $form->add_hidden('idAnkety', $page['idAnkety']);
    
    
    
if(MODULE_GALLERY)
{           
    $sel = ShowSelectBoxGallery();
    $sel = array(0 => "-- ".TVYBERTE." --") + $sel;

    if(count($sel) == 1){
        $text_neni = "";
        if($login_obj->UserPrivilege('content_add'))
            $text_neni .= tlacitko(get_link('gallery',"galerie",0,"new"));
        $form->add_plain_text(TGALERIE, $text_neni)->set_class_tr("prazdna-stranka-hidden");

        }
        else
        $form->add_selectbox(TGALERIE, 'idGalerie', $page['idGalerie'], $sel)->set_class_tr("prazdna-stranka-hidden");          
}
else
    $form->add_hidden('idGalerie', $page['idGalerie']);
    
 
    
    
if(MODULE_DISCUSSION)
    $form->add_radiobuttons(TDISKUZE, 'diskuze', $page['diskuze'], array(1 => TANO, 0=>TNE))->set_class_tr("prazdna-stranka-hidden");
    else
    $form->add_hidden('diskuze', $page['diskuze']);
   
   
   
$form->add_section(TFOTKA, 'fotka'); 
$form->add_file(TNAHRAT_OBRAZEK, 'obrazek', false, 85,"","obrazek_input")->set_disabled($page['obrazek']!='');
            
$image_class = "";
if($page['obrazek']=='' || $page['obrazek']==null)
{
    $img = TNENI_ULOZEN_ZADNY_OBRAZEK;
}
else
{	

    $file = $domain->getDir().USER_DIRNAME_ARTICLES_MINI.$page['obrazek'];

    if(file_exists(PRE_PATH.$file)) 
    {
        $src = RELATIVE_PATH.$file; 
        $img = "<span class='block relative'><img src='".$src."' alt='".$page['obrazek']."' title='".$page['obrazek']."' /><br />";
        $image_class = "image";
        if($login_obj->UserPrivilege('content_delete') && $object_access->has_access($idPage))
            $img .= "<a href='#' class='delete' id='delete".$idPage."'>".icon('delete','','absolute top left')."</a></span>";
            else
            $img .= icon_disabled('delete','','absolute top left');
    }
    else 
    {
        $src="";
        $img = "<span class='block relative' style='line-height: 50px; width: 200px;'>".TOBRAZEK_NENALEZEN;
        if($login_obj->UserPrivilege('content_delete'))
            $img .= "<a href='#' class='delete' id='delete".$idPage."'>".icon('delete','absolute top right')."</a></span>";
            else
            $img .= icon_disabled('delete','','absolute top left');
            
        $img .= "</span>";    
    }
}
    
$form->add_plain_text(TOBRAZEK, $img)->set_class_td($image_class." ine_nahled relative tleft");
  
   
$data_panely = $db->Query("SELECT idProfilu,nazev FROM ".TABLE_PANELY_SABLONY." WHERE idDomeny=".$domain->getId()." AND zobrazit=1 AND idJazyka=".WEB_LANG_ID." ORDER BY vychozi DESC ");
                
$profily = array();
while($profil = $db->getAssoc($data_panely))
    $profily[$profil['idProfilu']] = $profil['nazev'];

$form->add_section(TSABLONA_PANELU, 'panel-setting')->set_class_tr("prazdna-stranka-hidden");
$form->add_selectbox(TSABLONA_PANELU, 'idProfiluPanelu', $page['idProfiluPanelu'], $profily, 135)->set_class_tr("prazdna-stranka-hidden");     


$form->add_section(TNACASOVANI_ZOBRAZENI, 'date');
$form->add_text(TDATUM_CLANKU, 'datum', $page['datum'], 0, false, "kalendar","datum");
$form->add_text(TOD, 'od', $page['publikovat_od'], 89, false, "kalendar","od");
$form->add_text(TDO, 'do', $page['publikovat_do'], 88, false, "kalendar","do");
    
    
if($login_obj->UserPrivilege('seo'))
{
    $form->add_section(TSEO_PARAMETRY, 'seo', "", 8);
    $form->add_hidden('url', $page['url']);
    //$form->add_text(TURL_NAZEV, 'url', $page['url'], 9, $idPage > 0);
    $form->add_text(TTITLE, 'title', $page['title'], 10,false,'','title');
    $form->add_textarea(TDESCRIPTION, 'description', $page['description'], 11);	
}
else
{
    $form->add_hidden('url', $page['url']);
    $form->add_hidden('title', $page['title'], 'title');
    $form->add_hidden('description', $page['description']);	
}       


$result = array(
    "html" => $form->get_html_code(),
    "url" => array(
        "save" => get_link_ajax('articles','articles.save.form',$idModulu)
        ),
    "messages" => array()
    );

echo array2json($result);
 
    
    
    
    
    
    
    


?>