<?php

/**
 * @author Pavel Srom
 * @copyright 2010
 */

//ajaxovy skript na odhlaseni kontaktu z odberu newsletteru

if(!isset($_GET['module']) || !defined("SECURITY_CMS")) exit;

$module = trim($_GET['module']); 
$idAkce = get_int_get('id');

//nastaveni limitu
$limit = "";
if(isset($_GET['iDisplayLength'])){
    $dStart = "";
    if(isset($_GET['iDisplayStart']))
        $dStart = abs(intval($_GET['iDisplayStart'])).",";
        
    $limit = " LIMIT ".$dStart.abs(intval($_GET['iDisplayLength'])); 
}


//trideni podle sloupcu
$order = "";
if(isset($_GET['iSortCol_0'])){
    $s = abs(intval($_GET['iSortCol_0']));
    if($s == 0) $order = "k.idTerminu";
    elseif($s == 1) $order = "k.od";
    elseif($s == 2) $order = "k.do";
    elseif($s == 3) $order = "CONCAT(budouci,aktualni,uplynula)";
    else $order = "k.idTerminu";
}

if($order != '') 
    $order = "ORDER BY ".$order." ".(isset($_GET['sSortDir_0'])?$_GET['sSortDir_0']:'');




$where = "WHERE s.idJazyka=".WEB_LANG_ID." AND s.typ='akce' AND s.idDomeny=".$domain->getId()." AND k.idAkce=".$idAkce;

$data_pocet = $db->Query("SELECT COUNT(idTerminu) AS celkem FROM ".TABLE_KALENDAR_TERMINY." WHERE idAkce = ".$idAkce);
$iTotal_arr = $db->getAssoc($data_pocet);
$iTotal = $iTotal_arr['celkem'];


        
$data = $db->Query("SELECT SQL_CALC_FOUND_ROWS k.idTerminu, 
                IF(k.od IS NULL OR k.od = '','',DATE_FORMAT(k.od,'%d.%m.%Y %H:%i')) AS od,
                IF(k.do IS NULL OR k.do = '','',DATE_FORMAT(k.do,'%d.%m.%Y %H:%i')) AS do,
                IF(k.od IS NOT NULL AND k.od!='' AND k.od > NOW(),1,0) AS budouci,
                IF(k.od IS NOT NULL AND k.od!='' AND k.od <= NOW() AND (k.do IS NULL OR k.do='' OR k.do>=NOW()),1,0) AS aktualni,
                IF(k.do IS NOT NULL AND k.do!='' AND k.do < NOW(),1,0) AS uplynula
				FROM ".TABLE_KALENDAR_TERMINY." AS k
                LEFT JOIN ".TABLE_STRANKY." AS s ON k.idAkce=s.idStranky
				".$where."
                GROUP BY k.idTerminu
                ".$order."
				
				");


$iFilteredTotal_data = $db->Query("SELECT FOUND_ROWS()");
$iFilteredTotal_arr = $db->getRow($iFilteredTotal_data);
$iFilteredTotal = intval($iFilteredTotal_arr[0]);


$dis_content = !$login_obj->UserPrivilege('content_view');
$dis_delete = !$login_obj->UserPrivilege('content_delete') || !$object_access->has_access($idAkce);

$i = 1;
$pocet = $db->numRows($data);

$tr = array();
while($c = $db->getAssoc($data))
{
    $ids = $c['idTerminu'];
    
    $pom = array();
    $pom[] = json_encode($ids);
    $pom[] = json_encode($c['od']);
    $pom[] = json_encode($c['do']);
    
    
    if($c['budouci']==1)
        $stav = "<span class='stav2'>".TTERMIN_JESTE_NENI_AKTIVNI."</span>";
    if($c['aktualni']==1)
        $stav = "<span class='stav1'>".TTERMIN_JE_MOMENTALNE_AKTIVNI."</span>";
    elseif($c['uplynula']==1)
        $stav = "<span class='stav0'>".TTERMIN_JIZ_NENI_AKTIVNI."</span>";
        
    $pom[] = json_encode("<em>".$stav."</em>");
       
    $ikony = "";
            
    if($dis_content)
        $ikony .= icon_disabled('content');
        else
        $ikony .= "<a href='".get_link($module,'akce',$idAkce,'termin-edit',$ids)."'>".icon('content')."</a>";
    
    if($dis_delete)
        $ikony .= icon_disabled('delete');
        else
        $ikony .= "<a href='#' class='delete' id='delete".$ids."'>".icon('delete')."</a>";
        
    $pom[] = json_encode($ikony);
        
    $tr[] = "[".implode(',', $pom)."]";
    
    $i++;
}

echo "{";
echo '"sEcho": '.(intval($_GET['sEcho'])).', ';
echo '"iTotalRecords": '.$iTotal.', ';
echo '"iTotalDisplayRecords": '.($iFilteredTotal).', ';
echo '"aaData": ['.implode(',', $tr).'] }';


?>