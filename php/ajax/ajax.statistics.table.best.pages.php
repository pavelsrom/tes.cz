<?php

/**
 * @author Pavel Srom
 * @copyright 2011
 */



include_once(PRE_PATH.'php/classes/class.gapi.php');


 

if(!isset($_REQUEST['od']) || !isset($_REQUEST['do']) || !defined("SECURITY_CMS")) exit;

$razeni = isset($_REQUEST['razeni']) ? $_REQUEST['razeni'] : "pageviews";
$smer = isset($_REQUEST['smer']) && $_REQUEST['smer'] == 'asc' ? '' : '-';
if(!in_array($razeni, array('pageviews','uniquePageviews','exitRate','exits','avgTimeOnPage'))) exit;
$ga_razeni = $smer.$razeni;


$start = (isset($_REQUEST['od'])?$_REQUEST['od']:'');
$end = (isset($_REQUEST['do'])?$_REQUEST['do']:'');

$start = GetGADate($start);
$end = GetGADate($end);

if($start=='' || strlen($start)!=10)
	if($end!=''){
		$start = $s = explode('-', $end);
		$start = $s[0].".".$s[1]."-01";
		}
		else 
		$start=strftime("%Y-%m-01", time());
        
if($end=='' || strlen($end)!=10) $end=strftime("%Y-%m-%d", time());



if(!$login_obj->UserPrivilege('statistics_view')) exit;


$error = "";

try{
	$ga = new gapi(GA_EMAIL,GA_HESLO);
	}
	catch(exception $e){   
	exit;	
	}
	
$idProfile = GA_PROFIL;
$maxRecords = 15;

$filter = null;

$koncovka = "";
if(in_array($domain->data['urlKoncovka'],array('html','htm','php'))) 
    $koncovka = "\.(".$domain->data['urlKoncovka'].")";
    
if($domain->data['urlStruktura'] == 'id-nazev')
    $regularni_vyraz = "^\/([\d]+_[\w\-\d]+".$koncovka."){0,1}$";
elseif($domain->data['urlStruktura'] == 'nazev-id')
    $regularni_vyraz = "^\/([\w\-\d]+_[\d]+".$koncovka."){0,1}$";
else
    $regularni_vyraz = "^\/([\w\-\d]+".$koncovka."){0,1}$";
        
$regularni_vyraz = "pagePath!@? && pagePath=~^\/$ || pagePath=~(".$koncovka.")+$";

//zjisteni klicovych slov a odkazujicich stranek
$ga->requestReportData($idProfile,array('pagePath'),array('pageviews','uniquePageviews','exitRate','exits','avgTimeOnPage'),array($ga_razeni),$regularni_vyraz,null,$start,$end,1,$maxRecords);
$res = $ga->getResults();

$tabulka = array();
$i=0;
foreach($res AS $r){
    $tabulka[$i]['stranka'] = $r->dimensions['pagePath'];
    $tabulka[$i]['zobrazeni'] = $r->metrics['pageviews'];
    $tabulka[$i]['jedinecna_zobrazeni'] = $r->metrics['uniquePageviews'];
    $tabulka[$i]['mira_opusteni'] = round($r->metrics['exitRate'],2);
    $tabulka[$i]['opusteni'] = $r->metrics['exits'];
        
    if($r->metrics['avgTimeOnPage']>0)
        $tabulka[$i]['doba_na_strance'] = date('H:i:s', mktime(0,0,round($r->metrics['avgTimeOnPage'],0),0,0,0));
        else
        $tabulka[$i]['doba_na_strance'] = date('H:i:s', mktime(0,0,0,0,0,0));
 
    $i++;   
    }
 
//vykresleni tabulky

$r = $razeni;
$s = $smer=='-'?'_asc':'_desc';
$def_s = '_desc';

echo "<table class='table w80'>";
echo "<tr>
    <th>".TSTRANKA."</th>
    <th>".TODKAZ."</th>   
    <th><a href='#' class='pageviews".($r=='pageviews'?$s:$def_s)."'>".TPOCET_ZOBRAZENI."</a></th>
    <th><a href='#' class='uniquePageviews".($r=='uniquePageviews'?$s:$def_s)."'>".TPOCET_UNIKATNICH_ZOBRAZENI."</a></th>
    <th><a href='#' class='avgTimeOnPage".($r=='avgTimeOnPage'?$s:$def_s)."'>".TPRUMERNA_DOBA_NA_STRANCE."</a></th>
    <th><a href='#' class='exitRate".($r=='exitRate'?$s:$def_s)."'>".TMIRA_OPUSTENI."</a></th>
    <th><a href='#' class='exits".($r=='exits'?$s:$def_s)."'>".TPOCET_OPUSTENI."</a></th>
    </tr>";
    
if(count($tabulka)>0)
{
    
    
    $url_homepage = $links->get_url("uvodni");
    
    foreach($tabulka AS $p)
    {
        
        $s = ltrim($p['stranka'], '/');
        if($domain->data['urlKoncovka']!='')
            $s = rtrim($s, '.'.$domain->data['urlKoncovka']);
        
        if($s=='') 
            $s = $url_homepage;
            
        
        $adresar = $domain->data['adresar']!="" ? "/".$domain->data['adresar'] : "";
        
        echo "<tr>";

        $str = $db->get(TABLE_STRANKY,array("nazev","typ"),"idStranky=".$s." AND idDomeny=".$domain->getId());
        
        echo "<td class='tleft'>";
        if($str->nazev != ''){
            echo $str->nazev;
            echo " (";
            switch($str->typ){
                case 'clanek': {echo TCLANEK; break;}
                case 'novinka': {echo TNOVINKA; break;}
                case 'stranka': {echo TSTRANKA; break;} 
                case 'clanky': {echo TCLANKY; break;}
                case 'novinky': {echo TNOVINKY; break;}
                case 'stranka-galerie': {echo TSTRANKA_S_GALERII; break;}
                case 'galerie': {echo TGALERIE; break;}
                case 'galerie-slozka': {echo TSLOZKA_GALERIE; break;}
                case 'vyhledavani': {echo TVYHLEDAVANI; break;}
                case '404': {echo TCHYBOVA_STRANKA; break;}
                case 'sitemap': {echo TMAPA_WEBU; break;}
                case 'archiv-clanku': {echo TARCHIV_CLANKU; break;}
                case 'uvodni': {echo TUVODNI_STRANKA; break;}
                default: echo TNEZNAMY_TYP;
                }
            echo ")";
            }
            else
            echo $p['stranka']." (".TNEEXISTUJE.")";
        echo "</td>";
            
        echo "<td>";
        
        if(isset($str->nazev))
            echo "<a href='".$adresar.$p['stranka']."' target='_blank'>".icon('link')."</a>";
            
        echo "</td>";
        
        if($r=='pageviews')    
            echo "<td class='w15'><b>".$p['zobrazeni']."</b></td>";
            else
            echo "<td class='w15'>".$p['zobrazeni']."</td>";
        
        if($r=='uniquePageviews')    
            echo "<td class='w15'><b>".$p['jedinecna_zobrazeni']."</b></td>";
            else
            echo "<td class='w15'>".$p['jedinecna_zobrazeni']."</td>";
        
        if($r=='avgTimeOnPage')    
            echo "<td class='w15'><b>".$p['doba_na_strance']."</b></td>";
            else
            echo "<td class='w15'>".$p['doba_na_strance']."</td>";
        
        if($r=='exitRate')    
            echo "<td class='w10'><b>".$p['mira_opusteni']."%</b></td>";
            else
            echo "<td class='w10'>".$p['mira_opusteni']."%</td>";
            
        if($r=='exits')    
            echo "<td class='w10'><b>".$p['opusteni']."</b></td>";
            else
            echo "<td class='w10'>".$p['opusteni']."</td>";
            
        echo "</tr>";
        
    }
    
}
else
echo "<tr><td colspan='7'>".TNENALEZEN_ZADNY_ZAZNAM."</td></tr>";

echo "</table>";


?>