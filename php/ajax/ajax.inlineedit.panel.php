<?php

/**
 * @author Pavel Srom
 * @copyright 2014
 */

$id_stranky = get_int_post('id_page');

$s = $db->get(TABLE_STRANKY, array("nazev","zobrazit","idRodice","typ"),"idStranky=".$id_stranky." AND idDomeny=".$domain->getId());


$nazev_stranky = $s->nazev; 
$aktivni_stranka = $s->zobrazit ? TANO : TNE;
$aktivni_stranka_class = $s->zobrazit ? "fa-check-circle ine_green" : "fa-times-circle ine_red"; 
$nazev_webu = $domain->getName();
$jmeno_a_prijmeni = $login_obj->getName();
$opravneni = $login_obj->getPrivilegeName();
$vstup_do_administrace_url = $domain->getUrl()."admin";

$typ = $s->typ;
$rodic = $s->idRodice;

$ine_aktivni_class = $_SESSION['ine'] ? "fa-check-circle ine_green" : "fa-times-circle ine_red"; 

$pozice_panelu = isset($_SESSION['ine_panel_position']) && $_SESSION['ine_panel_position'] == 'bottom' ? "bottom" : "top";
$stav_panelu = isset($_SESSION['ine_panel_state']) && $_SESSION['ine_panel_state'] == 'hide' ? 'hide' : "show";


include("../functions/functions.ine.php");
include("../config/config.ine.php");

//menu je definovano v config.ine.php
$menu = get_special_items_to_menu($typ, $menu, $config_modules);

//print_r($menu['stranka']);

?>

<!-- ovladaci panel inline editace -->
<div id="ine_panel" class="position_<?php echo $pozice_panelu;?> <?php echo $stav_panelu;?>_panel">
    <div id="ine_panel_in">

        <!-- ovladani umisteni -->
        <div id="ine_position_panel" class="ine_box">
            <div class="ine_content">
                <a href="#" class="ine_icon_mini fa fa-minus-circle" id="hide_panel" title="<?php echo TMINIMALIZOVAT_PANEL;?>"></a>
                <a href="#" class="ine_icon_mini fa fa-external-link-square" id="show_panel" title="<?php echo TMAXIMALIZOVAT_PANEL;?>"></a>
                <a href="#" class="ine_icon_mini fa fa-chevron-circle-down" id="position_top" title="<?php echo TPRESUNOUT_PANEL_DOLU;?>"></a>
                <a href="#" class="ine_icon_mini fa fa-chevron-circle-up" id="position_bottom" title="<?php echo TPRESUNOUT_PANEL_NAHORU;?>"></a>
                
                <!--
<a href="#" class="ine_icon_mini ine_icon_sipka_vpravo"  id="position_right"></a>
                <a href="#" class="ine_icon_mini ine_icon_sipka_vlevo" id="position_left"></a>
-->
            </div>
        </div>
            
        <!-- panel uzivatele -->
        <div id="ine_user_panel" class="ine_box">
            <div class="ine_content">
                <h2>Uživatel <span><?php echo $jmeno_a_prijmeni;?></span></h2>
                <div class="ine_info">
                    <strong><?php echo TJMENO_A_PRIJMENI;?>:</strong> 
                    <span id="ine_user_name"><?php echo $jmeno_a_prijmeni;?></span>
                    <br />
                    <!--
<strong><?php echo TPRAVA;?>:</strong> 
                    <span><?php echo $opravneni;?></span>
                    <br />
-->
                    <strong><?php echo TINLINE_EDITACE_STAV;?>:</strong> 
                    <a href="#" class="ine_stav fa <?php echo $ine_aktivni_class;?>" id="ine_aktivni" title="<?php echo TZAPNOUT_VYPNOUT_INE;?>"></a>
                </div>
                
                <div class="ine_tools">
                    <?php 
                    
                    foreach($menu["uzivatel"] AS $p) 
                        echo get_ine_polozka_menu($p); 
                    ?>
                </div>        
                
            </div>   
        </div>




        <!-- panel webu -->
    
        <div id="ine_web_panel" class="ine_box">
            <div class="ine_content">
                <h2>Web <span><?php echo $nazev_webu;?></span></h2>
               
                <div class="ine_info">
                    <strong><?php echo TWEB;?>:</strong> 
                    <span id="ine_web_name"><?php echo $nazev_webu;?></span>
                    <br />
                    <strong><?php echo TJAZYK;?>:</strong> 
                    <span><?php echo WEB_LANG;?></span>
                </div>
                
                <div class="ine_tools">
                    <?php 
                    
                    foreach($menu["web"] AS $p) 
                        echo get_ine_polozka_menu($p); 
                    ?>
                </div>
            </div>   
        </div>
            
    
        <!-- panel stranky -->
        <div id="ine_page_panel" class="ine_box">
            <div class="ine_content">
                <h2>Stránka <span><?php echo $nazev_stranky;?></span></h2>

                <div class="ine_info">
                    <strong><?php echo TNAZEV;?>:</strong> 
                    <span id="ine_page_name"><?php echo $nazev_stranky;?></span>
                    <br />
                    <strong><?php echo TAKTIVNI;?>:</strong> 
                    <?php
                    
                    if(in_array($typ,array('uvodni','404')))                                                            
                        echo '<span class="ine_stav fa '.$aktivni_stranka_class.'" ></span>';
                        else
                        echo '<a href="#" class="ine_stav fa '.$aktivni_stranka_class.'" id="ine_stranka_aktivni" title="'.TPOVOLIT_ZAKAZAT_ZOBRAZOVANI_STRANKY.'"></a>';
                        
                    ?>                                                                                                
                </div>
                
                <div class="ine_tools">
                    <?php 
                    
                    foreach($menu["stranka"] AS $p) 
                        echo get_ine_polozka_menu($p); 
                    ?>
                    
                </div> 
            </div>
        </div>
        <div class="ine_cleaner"></div>
    
    
    <div id="ine_logo">
        <img src="inline_edit/img/logo.png" alt="logo" />
    </div>
    

    <!-- formular -->
    <div id="ine_form">
        <div id="ine_content"></div>
        
        <div id="ine_loader"></div>
        <div id="ine_system_message"></div>
    </div>
    
    <!--
<div id="ine_vyhledavani">
        <label for="ine_vyhledavani_input"><?php echo TVYHLEDAT_STRANKU;?>:</label> 
        <input type="text" name="ine_vyhledavani" value="" id="ine_vyhledavani_input"/>
    </div>
    
    <div id="ine_confirm">
        <span class="ui-icon ui-icon-alert"></span>
        <p></p>
    </div>
    -->
    
    </div>
</div>