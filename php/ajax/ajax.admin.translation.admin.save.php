<?php

/**
 * @author Pavel Srom
 * @copyright 2010
 */

if(!defined("SECURITY_CMS")) exit;
if(!$login_obj->UserPrivilege('settings_edit')) exit;

$id = get_int_post('id');
$cz = get_post('cz');
$en = get_post('en');

if($id <= 0) exit;

$update = array(
    'cz' => $cz,
    'en' => $en
    );

$data = $db->update(TABLE_PREKLADY_ADMINISTRACE, $update, "idPrekladu=".$id." LIMIT 1");

$nazev = $db->get(TABLE_PREKLADY_ADMINISTRACE,'konstanta','idPrekladu='.$id);  
$log->add_log('edit-settings',"preklady-admin",$id,$nazev);

exit;

?>