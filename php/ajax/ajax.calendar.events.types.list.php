<?php

/**
 * @author Pavel Srom
 * @copyright 2010
 */

//ajaxovy skript na odhlaseni kontaktu z odberu newsletteru

if(!isset($_GET['module']) || !defined("SECURITY_CMS")) exit;

$module = trim($_GET['module']);
   

$where = "WHERE s.idJazyka=".WEB_LANG_ID." AND s.typ='kalendar-typ' AND s.idDomeny=".$domain->getId();

$data_pocet = $db->Query("SELECT COUNT(idStranky) AS celkem FROM ".TABLE_STRANKY." AS s ".$where);
$iTotal_arr = $db->getAssoc($data_pocet);
$iTotal = $iTotal_arr['celkem'];


        
$data = $db->Query("SELECT SQL_CALC_FOUND_ROWS s.idStranky, s.nazev, s.barva, j.jazyk, s.url, COUNT(r.idStranky) AS pouzito
				FROM ".TABLE_STRANKY." AS s
                LEFT JOIN ".TABLE_JAZYKY." AS j ON s.idJazyka = j.idJazyka
                LEFT JOIN ".TABLE_STRANKY." AS r ON s.idStranky = r.idRodice AND r.typ = 'akce'
				".$where."
				GROUP BY s.idStranky
				ORDER BY s.priorita, s.idStranky
				");


$iFilteredTotal_data = $db->Query("SELECT FOUND_ROWS()");
$iFilteredTotal_arr = $db->getRow($iFilteredTotal_data);
$iFilteredTotal = intval($iFilteredTotal_arr[0]);


$dis_content = !$login_obj->UserPrivilege('content_view');
$dis_delete = !$login_obj->UserPrivilege('content_delete');

$i = 1;
$pocet = $db->numRows($data);

$tr = array();
while($c = $db->getAssoc($data))
{
    $ids = $c['idStranky'];
    
    $private_pages = new C_PrivatePages($ids);
    $privatni = ano_ne($private_pages->is_private(),implode(", ", $private_pages->get_groups())); 

    
    
    $pouzito = intval($c['pouzito']);
    $pom = array();
    $pom[] = json_encode($ids);
    $pom[] = json_encode(secureString($c['nazev']));
    $pom[] = json_encode("<span class='anketa-graf' style='padding: 2px 10px; background-color: #".secureString($c['barva'])."'></span>");
    $pom[] = json_encode($pouzito);
    $pom[] = json_encode($privatni);
    
    $ikony_up_down = "";
    if($i==1 || $dis_content)
        $ikony_up_down .= icon_disabled('up');
        else
        $ikony_up_down .= "<a href='#' id='up".$ids."' class='up'>".icon('up')."</a>";
        				
    if($i == $pocet || $dis_content)
        $ikony_up_down .= icon_disabled('down');
        else
        $ikony_up_down .= "<a href='#' id='down".$ids."' class='down'>".icon('down')."</a>";
    
    //$pom[] = json_encode($ikony_up_down);
    
    
    
    $ikony = "";
    
    $ikony .= "<a href='".urlPage($c['url'],$ids,$c['jazyk'])."' target='_blank'>".icon('link')."</a>";
    
    
    if($dis_content)
        $ikony .= icon_disabled('content');
        else
        $ikony .= "<a href='".get_link($module,'akce-typy',0,'edit-content',$ids)."'>".icon('content')."</a>";
    
    if($dis_delete || !$object_access->has_access($ids))
        $ikony .= icon_disabled('delete');
    elseif($pouzito > 0)
        $ikony .= icon_disabled('delete',TNELZE_SMAZAT_Z_DUVODU_POUZITI_V_KALENDARI_AKCI);
        else
        $ikony .= "<a href='#' class='delete' id='delete".$ids."'>".icon('delete')."</a>";
        
    $pom[] = json_encode($ikony_up_down.$ikony);
        
    $tr[] = "[".implode(',', $pom)."]";
    
    $i++;
}

echo "{";
echo '"sEcho": '.(intval($_GET['sEcho'])).', ';
echo '"iTotalRecords": '.$iTotal.', ';
echo '"iTotalDisplayRecords": '.($iFilteredTotal).', ';
echo '"aaData": ['.implode(',', $tr).'] }';


?>