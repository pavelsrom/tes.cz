<?php

/**
 * @author Pavel Srom
 * @copyright 2015
 */

$id = get_int_request("id_page");

if(!$object_access->has_access($id))
    return;
    
$nazev = $db->get(TABLE_STRANKY,"nazev","idStranky=".$id." AND idDomeny=".$domain->getId());

$db->query("UPDATE ".TABLE_STRANKY." SET zobrazit = IF(zobrazit = 1, 0, 1) WHERE idStranky = ".$id." AND idDomeny=".$domain->getId()." AND idJazyka=".WEB_LANG_ID);
    
$log->add_log('edit-content','stranka',$id,$nazev);

?>