<?php

/**
 * @author Pavel Srom
 * @copyright 2014
 */

$id = get_int_post("id");

$typ = $db->get(TABLE_STRANKY,"typ","idStranky=".$id);

if($typ == 'clanek')
    include('ajax.articles.articles.image.delete.php');
elseif($typ == 'novinka')
    include('ajax.news.news.image.delete.php');

?>