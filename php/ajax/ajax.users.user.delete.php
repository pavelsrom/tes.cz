<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */
 
 
if(!defined("SECURITY_CMS")) exit;

if((!$login_obj->UserPrivilege('admin') && !$login_obj->UserPrivilege('superadmin')) || !$login_obj->UserPrivilege('settings_delete')) exit;

$id = get_int_post('id');

if($id <= 0) exit;

$idPravaSuperAdmin = $db->get(TABLE_CMS_PRAVA,'idPrava','systemovyNazev="superadmin"');

if($login_obj->UserPrivilege('admin'))
    $id = $db->get(TABLE_UZIVATELE, 'idUzivatele', "idUzivatele=".$id." AND idPrava!=".$idPravaSuperAdmin);

if($id <= 0) exit;

$db->delete(TABLE_UZIVATELE, "WHERE idUzivatele=".$id." LIMIT 1");
$db->delete(TABLE_STRANKY_EDITORI, "WHERE idUzivatele=".$id."");
$db->delete(TABLE_UZIVATELE_DOMENY,"idUzivatele=".$id);
$db->delete(TABLE_UZIVATELE_MODULY,"idUzivatele=".$id);
$db->delete(TABLE_UZIVATELE_JAZYKY,"idUzivatele=".$id);
$db->delete(TABLE_UZIVATELE_SKUPINY_RELACE, "idUzivatele=".$id."");

$db->delete(TABLE_ESHOP_ZAKOUPENE_KNIHY, "id_uzivatel=".$id."");
$db->delete(TABLE_PROVIZE, "id_uzivatel=".$id."");





exit;


?>