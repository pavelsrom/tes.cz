<?php

/**
 * @author Pavel Srom
 * @copyright 2014
 */

if(!defined("SECURITY_CMS")) exit;

include("../classes/class.messages.php");
$systemMessage = new C_SystemMessage();

$result = array(
    "redirect" => "",
    "messages" => array(
        "error" => ""
        )
    );

$idStranky = get_int_post('idStranky');

if((!$login_obj->UserPrivilege('content_edit') && $idStranky > 0) || 
    (!$login_obj->UserPrivilege('content_add') && $idStranky == 0)
    ){
    $result["messages"]["error"] = ERROR_NEDOSTATECNA_PRAVA;
    echo array2json($result);
    exit;
} 




$idRodice = get_int_post('idRodice');
$idVlastnika = $login_obj->getId();
$idGalerie = get_int_post('idGalerie');    
$idFormulare = get_int_post('idFormulare');
$idAnkety = get_int_post('idAnkety');
$diskuze = get_int_post('diskuze');

$idProfiluPanelu = get_int_post('idProfiluPanelu',-1);          
$idDomeny = $domain->getId();
$nazev = get_post('nazev');
$menu_nazev = get_post('menu_nazev');
$typ_stranky = get_post('typ','stranka');
$stary_typ_stranky = get_post('stary_typ');			
$url = get_post('url');
$menu_zobr = get_int_post('menu');
$stav = get_int_post('zobrazit',1);
$autentizace = get_int_post('autentizace');
$title = get_post('title');			
$description = get_post('description');		

    
       
$url = $idStranky==0 && $url=='' ? ModifyUrl($nazev) : ModifyUrl($url);
$url_zal = get_url_nazev($idStranky, $url);

if(!TestLength($nazev, 200, false))
	$systemMessage->add_error(ERROR_NEPLATNY_NAZEV);
                
if(!TestLength($menu_nazev, 200, false) && $typ_stranky!='404' && $typ_stranky!='vyhledavani')
	$systemMessage->add_error(ERROR_NEPLATNY_NAZEV_PRO_MENU);

if(!TestLength($title, 100, true))
	$systemMessage->add_error(ERROR_NEPLATNY_TITULEK);
				
if(!TestLength($description, 200, true))
	$systemMessage->add_error(ERROR_NEPLATNY_DESCRIPTION);


if($systemMessage->error_exists())
{
    $result["messages"]["error"] = $systemMessage->get_error();
    echo array2json($result);
    exit;    
};

$update = array(
    "nazev" => $nazev,
    "idDomeny" => $idDomeny,
    "nazevProMenu" => $menu_nazev,
    //"nadpis" => $menu_nazev,
    "idRodice" => $idRodice,
    "zobrazit" => $stav,
    //"autentizace" => $autentizace,
    "doMenu"    => $menu_zobr,
    "idProfiluPanelu" => $idProfiluPanelu,
    "typ"   => $typ_stranky,
    "idGalerie" => $idGalerie,
    //"idFormulare" => $idFormulare,
    //"idAnkety" => $idAnkety,
    //"diskuze" => $diskuze,
    "idJazyka" => WEB_LANG_ID,
    "title" => $title,
    "description" => $description,
    "url"   => $url
    );
 

$log->add_log($idStranky == 0 ? 'create':'edit-content','stranka',$idStranky,$nazev);
 
$nova_stranka = false;
if($idStranky > 0){		
    $update['idAktualizoval'] = $login_obj->getId();
    $update['jmenoAktualizoval'] = $login_obj->getName();
    $update['datumAktualizoval'] = "now";
    $db->update(TABLE_STRANKY,$update,"idStranky=".$idStranky." AND idDomeny=".$idDomeny);
	} 
	else 
	{
    $update['idVytvoril'] = $login_obj->getId();
    $update['jmenoVytvoril'] = $login_obj->getName();
    $update['datumVytvoril'] = "now";
    $update['idAktualizoval'] = $login_obj->getId();
    $update['jmenoAktualizoval'] = $login_obj->getName();
    $update['datumAktualizoval'] = "now";
    
    $update['nadpis'] = $nazev;
    $update['obsah'] = "<p>".str_repeat(TLOREM_IPSUM_OBSAH." ", 10)."</p>";
    
	$db->insert(TABLE_STRANKY,$update);
    $idStranky = $db->lastId();
    
    $object_access->add_allowed_object($idStranky);
    
    $nova_stranka = true;
	}



$redirect_url = UrlPage($url,$idStranky,WEB_LANG);

CreateTypePage($idStranky, $typ_stranky, $stary_typ_stranky);

if($systemMessage->error_exists())
{
    $result["messages"]["error"] = $systemMessage->get_error();
    echo array2json($result);
    exit;    
}

$result["redirect"] = $redirect_url;

if($nova_stranka)
    $result["messages"]["ok"] = OK_ULOZENO_A_PRESMEROVANO;//OK_ULOZENO;
    else
    {
    $result["messages"]["ok"] = OK_ULOZENO;
    
    }

echo array2json($result);
exit; 


    
    

?>