<?php

/**
 * @author Pavel Srom
 * @copyright 2010
 */

//ajaxovy skript na odhlaseni kontaktu z odberu newsletteru

if(!isset($_GET['module']) || !defined("SECURITY_CMS")) exit;

$vyraz = trim($db->secureString($_GET['vyraz']));
$module = trim($_GET['module']);

//nastaveni limitu
$limit = "";
if(isset($_GET['iDisplayLength'])){
    $dStart = "";
    if(isset($_GET['iDisplayStart']))
        $dStart = abs(intval($_GET['iDisplayStart'])).",";
        
    $limit = " LIMIT ".$dStart.abs(intval($_GET['iDisplayLength'])); 
}


//trideni podle sloupcu
$order = "";
if(isset($_GET['iSortCol_0'])){
    $s = abs(intval($_GET['iSortCol_0']));
    if($s == 0) $order = "a.idAnkety";
    elseif($s == 1) $order = "a.nazev";
    elseif($s == 2) $order = "a.otazka";
    elseif($s == 3) $order = "pocet_polozek";
    elseif($s == 4) $order = "pocet_hlasovani";
    elseif($s == 5) $order = "a.zobrazit";
    else $order = "a.idAnkety";
}

if($order != '') 
    $order = "ORDER BY ".$order." ".(isset($_GET['sSortDir_0'])?$_GET['sSortDir_0']:'');

//echo $order;

//sestaveni podminky pro vyhledavani
$like = array();
if($vyraz != '')
{
    $like[] = "a.idAnkety LIKE '".$vyraz."%'";
    $like[] = "a.nazev LIKE '%".$vyraz."%'";
    //$like[] = "a.otazka LIKE '%".$vyraz."%'";
}
    


$where = "WHERE a.idDomeny=".$domain->getId().((count($like) > 0) ? " AND (".implode(" OR ",$like).")" : "");

$data_pocet = $db->Query("SELECT COUNT(idAnkety) AS celkem FROM ".TABLE_ANKETY." AS a ".$where);
$iTotal_arr = $db->getAssoc($data_pocet);
$iTotal = $iTotal_arr['celkem'];


        
$data = $db->Query("SELECT SQL_CALC_FOUND_ROWS a.*, COUNT(DISTINCT o.idOdpovedi) AS pocet_polozek, p.otazka, SUM(o.hlasy) AS pocet_hlasovani,
				IF(a.od IS NULL OR a.od = '0000-00-00', '', DATE_FORMAT(a.od, '%d.%m.%Y')) AS od,
				IF(a.do IS NULL OR a.do = '0000-00-00', '', DATE_FORMAT(a.do, '%d.%m.%Y')) AS do,
				IF(((a.od <= CURDATE() OR a.od IS NULL OR a.od = '0000-00-00') 
					AND (a.do>= CURDATE() OR a.do IS NULL OR a.do = '0000-00-00'))  
					AND a.zobrazit=1, 'ano', 'ne') AS stav
			FROM ".TABLE_ANKETY." AS a
            LEFT JOIN ".TABLE_ANKETY_OTAZKY." AS p ON a.idAnkety=p.idAnkety AND p.idJazyka=1
            LEFT JOIN ".TABLE_ANKETY_ODPOVEDI." AS o ON a.idAnkety=o.idAnkety
            ".$where."
			GROUP BY a.idAnkety
			".$order."
            ".$limit."
            ");


$iFilteredTotal_data = $db->Query("SELECT FOUND_ROWS()");
$iFilteredTotal_arr = $db->getRow($iFilteredTotal_data);
$iFilteredTotal = intval($iFilteredTotal_arr[0]);


$dis_content = !$login_obj->UserPrivilege('content_view');
$dis_delete = !$login_obj->UserPrivilege('settings_delete');
$dis_settings = !$login_obj->UserPrivilege('settings_view');

$tr = array();
while($c = $db->getAssoc($data))
{
    $ids = $c['idAnkety'];
    $pom = array();
    $pom[] = json_encode($ids);
    $pom[] = json_encode(secureString($c['nazev']));
    $pom[] = json_encode(secureString($c['otazka']));
    $pom[] = json_encode(intval($c['pocet_polozek']));
    $pom[] = json_encode(intval($c['pocet_hlasovani']));
    $pom[] = json_encode(ano_ne($c['zobrazit']));
    
    $ikony = "";
    
    if($dis_settings)
        $ikony .=  icon_disabled('setting');
        else
        $ikony .= "<a href='".get_link($module,'ankety',0,'edit-settings',$ids)."'>".icon('setting')."</a>";
    
    if($dis_content)
        $ikony .= icon_disabled('content');
        else
        $ikony .= "<a href='".get_link($module,'ankety',0,'edit-content',$ids)."'>".icon('content')."</a>";
    
    if($dis_delete)
        $ikony .= icon_disabled('delete');
        else
        $ikony .= "<a href='#' id='delete".$ids."' class='delete'>".icon('delete')."</a>";
        
    $pom[] = json_encode($ikony);
        
    $tr[] = "[".implode(',', $pom)."]";
    
}

echo "{";
echo '"sEcho": '.(intval($_GET['sEcho'])).', ';
echo '"iTotalRecords": '.$iTotal.', ';
echo '"iTotalDisplayRecords": '.($iFilteredTotal).', ';
echo '"aaData": ['.implode(',', $tr).'] }';


?>