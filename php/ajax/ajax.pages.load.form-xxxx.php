<?php

/**
 * @author Pavel Srom
 * @copyright 2014
 */


include("../classes/class.form.admin.php");
include("../functions/functions.inputs.php");

$links = new C_Links();

$idPage = get_int_post("id_page");
$action = get_post('ine_action');

if($action == 'pridat-stranku')
    $idPage = 0;

$page = array();
$page['nazev'] = "";
$page['typ'] = "stranka";
$page['nadpis'] = "";
$page['uvodni'] = 0;
$page['autentizace'] = 0;
$page['idRodice'] = 0;
$page['idProfiluPanelu'] = 0;
$page['zobrazit'] = 1;
$page['doMenu'] = 1;
$page['obsah'] = "";
$page['idGalerie'] = 0;
$page['idAnkety'] = 0;
$page['idFormulare'] = 0;
$page['diskuze'] = 0;
$page['url'] = "";
$page['title'] = "";
$page['description'] = "";
$page['nazevProMenu'] = "";







if($idPage > 0) {
    $data = $db->Query("SELECT s.*, j.jazyk,
				IF(s.nadpis='', s.nazev, s.nadpis) AS nadpis,
                IF(s.od IS NULL, '', DATE_FORMAT(s.od,'%d.%m.%Y')) AS publikovat_od,
				IF(s.do IS NULL, '', DATE_FORMAT(s.do,'%d.%m.%Y')) AS publikovat_do,
				IF(s.datum IS NULL, '', DATE_FORMAT(s.datum,'%d.%m.%Y')) AS datum
			FROM ".TABLE_STRANKY." AS s
            LEFT JOIN ".TABLE_JAZYKY." AS j ON s.idJazyka = j.idJazyka 
			WHERE idStranky=".$idPage." 
				AND idDomeny=".$domain->getId()."
                AND s.idJazyka=".WEB_LANG_ID."
			LIMIT 1");
			
			
    $page = $db->getAssoc($data);
    $page['publikovat_od'] = $page['publikovat_od'] == '00.00.0000' ? "" : $page['publikovat_od'];
    $page['publikovat_do'] = $page['publikovat_do'] == '00.00.0000' ? "" : $page['publikovat_do'];  
}

$typ = array(
    'stranka' => TSTANDARDNI_STRANKA, 
    "prazdna-stranka" => TPRAZDNA_STRANKA
    );


$form = new Form();
$form->allow_upload();
$form->set_jquery_ui_css();

$form->add_section(TZAKLADNI_UDAJE, "home");

if(in_array($page['typ'],array('novinka','clanek')))
    $form->add_text(TTITULEK, 'nazev', $page['nazev'], 83, true,"","nazev");
    else
    $form->add_text(TNAZEV, 'nazev', $page['nazev'], 0, true, "", "nazev"); 

if($page['typ'] == 'clanek')
{
    $data_rubriky = $db->query("SELECT idStranky, nazev FROM ".TABLE_STRANKY." WHERE typ='clanky' AND idDomeny=".$domain->getId()." ORDER BY nazev");
    $select_rubriky = array(0 => "-- ".TVYBERTE." --");
    while($r = $db->getAssoc($data_rubriky))
        $select_rubriky[$r['idStranky']] = $r['nazev']; 
    
    $form->add_text(TAUTOR, 'autor', $page['autor']);
    $form->add_selectbox(TRUBRIKA,'rubrika', $page['idRodice'],$select_rubriky, 0, true);
    //$form->add_radiobuttons(TZOBRAZOVAT, 'zobrazit', $page['zobrazit'], array(1 => TANO, 0 => TNE),6);
    
    
    $vsechny_stavy = array('rozpracovano' => TROZPRACOVANO, 'kontrola' => TKE_KONTROLE, 'zamitnuto' => TZAMITNUTO, 'hotovo' => THOTOVO);
    //operovat se vsemi stavy muze jen opravneni editor/vydavatel a vyssi
    if($login_obj->minPrivilege('editor'))
    {
        $form->add_selectbox(TSTAV,'stav',$page['stav'],$vsechny_stavy, 402);
        $form->add_textarea(TPOZNAMKA_REDAKCE,'poznamka',$page['poznamka'],399);
    }
    else
    {
        $stavy = array('rozpracovano' => TROZPRACOVANO, 'kontrola' => TKE_KONTROLE);
        
        //pokud stav neni povolen pro redaktora, pak se zobrazi stav v textove forme,aby jej nebyl mozne prepnout
        //stav hotovo a zamitnuto nemuze redaktor prepnout.
        if(isset($stavy[$page['stav']]))
            $form->add_selectbox(TSTAV,'stav',$page['stav'],$stavy);
            else
            {
                $form->add_plain_text(TSTAV, $vsechny_stavy[$page['stav']], 402);
                $form->add_hidden('stav',$page['stav']);
            }
        if($page['poznamka']!='')
            $form->add_plain_text(TPOZNAMKA_REDAKCE,$page['poznamka'],400);
        
        $form->add_hidden('poznamka',$page['poznamka']);
        
    }
    //kvuli porovnani stavu se uklada i stary stav
    $form->add_hidden('stary_stav',$page['stav']);
}




if($page['typ']!='404' && $page['typ'] != 'clanek')
    $form->add_text(TNAZEV_STRANKY_V_MENU, 'menu_nazev', $page['nazevProMenu'], 0, true, "", "menu_nazev");

if($page['typ']!='404')
{
            
    //overeni zda uz stranka s novinkami nebo galerii existuje - muze byt vytvorena jen jedna stranka s novinkami
    $dis_novinky = false;
    $dis_galerie = false;
    $dn = $db->Query("SELECT typ FROM ".TABLE_STRANKY." WHERE idDomeny=".$domain->getId()." AND (typ='novinky' OR typ='stranka-galerie') AND idJazyka=".WEB_LANG_ID);
            
    while($dna = $db->getAssoc($dn))
    {
        if($dna['typ']=='novinky') $dis_novinky = true;
        if($dna['typ']=='stranka-galerie') $dis_galerie = true;
    }
    
                
    $dis_clanky = false;
    if($idPage>0 && $page['typ']=='clanky')
    {
        $dc = $db->Query("SELECT idStranky FROM ".TABLE_STRANKY." WHERE idDomeny=".$domain->getId()." AND typ='clanek' AND idRodice=".$idPage." AND idJazyka=".WEB_LANG_ID." LIMIT 1");
                
        if($db->numRows($dc)>0)
            $dis_clanky = true;
                
    }
                 
    $radiobutton_typy = "";  
         
    if(in_array($page['typ'], array_keys($typ)))
    {
        $i = 0;
        foreach($typ AS $n => $t)
        {
            if($n == $page['typ']) 
                $ch = "checked='checked'"; 
                else 
                $ch = "";
                
            $dis = "";
            if($n != 'clanky' && $dis_clanky) 
                $dis = "disabled='disabled'";
                  
            $radiobutton_typy .= "<input type='radio' class='radio typ_stranky' name='typ' value='".$n."' ".$ch." ".$dis." id='typ".$n."' /><label for='typ".$n."'>".$t."</label><br />";
            $i++;
        }
        $form->add_plain_text(TTYP, $radiobutton_typy ,137);
    }
    else
    {
        $t = "";
        if($page['typ'] == 'archiv-clanku') $t = TARCHIV_CLANKU;
        elseif($page['typ'] == 'stranka-galerie') $t = TSTRANKA_S_GALERII;
        elseif($page['typ'] == 'prazdna-stranka') $t = TPRAZDNA_STRANKA;
        elseif($page['typ'] == 'sitemap') $t = TMAPA_WEBU;
        elseif($page['typ'] == 'novinky') $t = TNOVINKY;
        elseif($page['typ'] == 'uvodni') $t = TUVODNI_STRANKA;
        elseif($page['typ'] == 'kalendar-akci') $t = TKALENDAR_AKCI;
        elseif($page['typ'] == 'tagy') $t = TSTRANKA_STITKY;
        elseif($page['typ'] == 'vyhledavani') $t = TSTRANKA_S_VYSLEDKY_VYHLEDAVANI;
        elseif($page['typ'] == 'clanek') $t = TCLANEK;
        elseif($page['typ'] == 'novinka') $t = TNOVINKA;
        elseif($page['typ'] == 'akce') $t = TKALENDARNI_AKCE;
        $help = "";
        
        $form->add_hidden('typ',$page['typ']);
        
        if($page['typ'] != 'clanek')
            $form->add_plain_text(TTYP, $t, $help);            
    }
        
    
    //umisteni stranek
    if($page['typ'] != 'clanek')
    {
        $selectbox_pages = array(0 => "-- ".TKOREN_WEBU." --") + get_pages($idPage);
        $form->add_selectbox(TUMISTENI, 'idRodice', $page['idRodice'], $selectbox_pages,298);
    }

    if($page['typ'] != 'clanek')
        $form->add_radiobuttons(TZARADIT_DO_MENU, 'menu', $page['doMenu'], array(1 => TANO, 0 => TNE), 106);
           
            
    if(MODULE_PRIVATE_PAGES)
        $form->add_radiobuttons(TPRIVATNI_STRANKA, 'autentizace', $page['autentizace'], array(1 => TANO, 0 => TNE), 5)->set_class_tr("prazdna-stranka-hidden");
        else
        $form->add_hidden('autentizace',$page['autentizace']);

    		
    if($page['typ']!='uvodni' && $page['typ'] != 'clanek')
        $form->add_radiobuttons(TZOBRAZOVAT, 'zobrazit', $page['zobrazit'], array(1 => TANO, 0 => TNE), 6);
                
}           
elseif($page['typ']=='404')
{
    $form->add_hidden('typ',$page['typ']);
    $form->add_hidden('idRodice',0);
}

$form->add_section(TOBSAH,"obsah");


if($page['typ']!='404' && $page['typ']!='vyhledavani' && MODULE_INQUIRY)
{
    $d = $db->Query("SELECT idAnkety AS id, nazev FROM ".TABLE_ANKETY." WHERE idDomeny=".$domain->getId());
    
    $sel = array(0 => "-- ".TVYBERTE." --");
    $pocet = $db->numRows($d);
            
    while($s = $db->getAssoc($d))
        $sel[$s['id']] = $s['nazev'];
                
    if(count($sel) == 1){
        $text_neni = TNENI_VYTVORENO;
        if($login_obj->UserPrivilege('content_add'))
            $text_neni .= " (<a href='".get_link('inquiry',"ankety",0,'new')."' title='".TVYTVORIT."'>".TVYTVORIT."</a>)";
        
        $form->add_plain_text(TANKETA, $text_neni)->set_class_tr("prazdna-stranka-hidden");    
        }
        else
        $form->add_selectbox(TANKETA, 'idAnkety', $page['idAnkety'], $sel)->set_class_tr("prazdna-stranka-hidden");
            
}
else
    $form->add_hidden('idAnkety', $page['idAnkety']);
    
    
    
if($page['typ']!='404' && $page['typ']!='vyhledavani' && MODULE_GALLERY)
{           
    $sel = ShowSelectBoxGallery();
    $sel = array(0 => "-- ".TVYBERTE." --") + $sel;

    if(count($sel) == 1){
        $text_neni = TNENI_VYTVORENO;
        if($login_obj->UserPrivilege('content_add'))
            $text_neni .= " (<a href='".get_link('gallery',"galerie",0,"new")."' title='".TVYTVORIT."'>".TVYTVORIT."</a>)";
        $form->add_plain_text(TGALERIE, $text_neni)->set_class_tr("prazdna-stranka-hidden");

        }
        else
        $form->add_selectbox(TGALERIE, 'idGalerie', $page['idGalerie'], $sel)->set_class_tr("prazdna-stranka-hidden");          
}
else
    $form->add_hidden('idGalerie', $page['idGalerie']);
    
    
if($page['typ']!='404' && $page['typ']!='vyhledavani' && MODULE_FORMS)
{
    $d = $db->Query("SELECT idFormulare AS id, nazev FROM ".TABLE_FORMULARE." WHERE idDomeny=".$domain->getId());
    $sel = array(0 => "-- ".TVYBERTE." --");
    $pocet = $db->numRows($d);
            
    while($s = $db->getAssoc($d))
        $sel[$s['id']] = $s['nazev'];
            
    if(count($sel) == 1){
        $text_neni = TNENI_VYTVORENO;
        if($login_obj->UserPrivilege('content_add'))
            $text_neni .= " (<a href='".get_link('forms','formulare',0,'new')."' title='".TVYTVORIT."'>".TVYTVORIT."</a>)";
    
        $form->add_plain_text(TFORMULAR, $text_neni)->set_class_tr("prazdna-stranka-hidden");
        } 
        else
        $form->add_selectbox(TFORMULAR, 'idFormulare', $page['idFormulare'], $sel)->set_class_tr("prazdna-stranka-hidden");
            
}   
else
    $form->add_hidden('idFormulare', $page['idFormulare']);    
    
    
if($page['typ']!='404' && $page['typ']!='vyhledavani' && MODULE_DISCUSSION)
    $form->add_radiobuttons(TDISKUZE, 'diskuze', $page['diskuze'], array(1 => TANO, 0=>TNE))->set_class_tr("prazdna-stranka-hidden");
    else
    $form->add_hidden('diskuze', $page['diskuze']);
   
   
   
  
if(in_array($page['typ'],array('novinka','clanek')))
{   
    $form->add_section(TFOTKA, 'fotka'); 
    $form->add_file(TNAHRAT_OBRAZEK, 'obrazek', false, 85,"","obrazek_input")->set_disabled($page['obrazek']!='');
            
    $image_class = "";
    if($page['obrazek']=='' || $page['obrazek']==null)
    {
        $img = TNENI_ULOZEN_ZADNY_OBRAZEK;
    }
    else
    {	

        if($page['typ'] == 'novinka')        
            $file = $domain->getDir().USER_DIRNAME_NEWS.$page['obrazek'];
        elseif($page['typ'] == 'clanek')
            $file = $domain->getDir().USER_DIRNAME_ARTICLES_MINI.$page['obrazek'];

        if(file_exists(PRE_PATH.$file)) 
        {
            $src = RELATIVE_PATH.$file; 
            $img = "<span class='block relative'><img src='".$src."' alt='".$page['obrazek']."' title='".$page['obrazek']."' /><br />";
            $image_class = "image";
            if($login_obj->UserPrivilege('content_delete') && $object_access->has_access($idPage))
                $img .= "<a href='#' class='delete' id='delete".$idPage."'>".icon('delete','','absolute top left')."</a></span>";
                    else
                    $img .= icon_disabled('delete','','absolute top left');
                }
                else 
                {
                $src="";
                $img = "<span class='block relative' style='line-height: 50px; width: 200px;'>".TOBRAZEK_NENALEZEN;
                if($login_obj->UserPrivilege('content_delete'))
                    $img .= "<a href='#' class='delete' id='delete".$idPage."'>".icon('delete','absolute top right')."</a></span>";
                    else
                    $img .= icon_disabled('delete','','absolute top left');
                    $img .= "</span>";    
                    }
    }
    
    $form->add_plain_text(TOBRAZEK, $img)->set_class_td($image_class." relative tleft");
}   
   
   
   

$data_panely = $db->Query("SELECT idProfilu,nazev FROM ".TABLE_PANELY_SABLONY." WHERE idDomeny=".$domain->getId()." AND zobrazit=1 AND idJazyka=".WEB_LANG_ID." ORDER BY vychozi DESC ");
                
$profily = array();
while($profil = $db->getAssoc($data_panely))
    $profily[$profil['idProfilu']] = $profil['nazev'];

$form->add_section(TSABLONA_PANELU, 'panel-setting')->set_class_tr("prazdna-stranka-hidden");
$form->add_selectbox(TSABLONA_PANELU, 'idProfiluPanelu', $page['idProfiluPanelu'], $profily, 135)->set_class_tr("prazdna-stranka-hidden");     


if($page['typ'] == 'novinka' || $page['typ'] == 'clanek')
{
    $form->add_section(TNACASOVANI_ZOBRAZENI, 'date');
    $form->add_text(TDATUM_NOVINKY, 'datum', $page['datum'], 0, false, "kalendar","datum");
    $form->add_text(TOD, 'od', $page['publikovat_od'], 89, false, "kalendar","od");
    $form->add_text(TDO, 'do', $page['publikovat_do'], 88, false, "kalendar","do");
}    
    
if($login_obj->UserPrivilege('seo'))
{
    $form->add_section(TSEO_PARAMETRY, 'seo', "", 8);
    $form->add_text(TURL_NAZEV, 'url', $page['url'], 9, $idPage > 0);
    $form->add_text(TTITLE, 'title', $page['title'], 10,false,'','title');
    $form->add_textarea(TDESCRIPTION, 'description', $page['description'], 11);	
}
else
{
    $form->add_hidden('url', $page['url']);
    $form->add_hidden('title', $page['title'], 'title');
    $form->add_hidden('description', $page['description']);	
}       


echo $form->get_html_code();
 
    
    
    
    
    
    
    


?>