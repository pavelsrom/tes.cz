<?php

/**
 * @author Pavel Srom
 * @copyright 2014
 */

$term = get_request('search');

if($term == "") 
    exit;


function typ_nazev($typ){
    if($typ=='stranka') return TSTRANKA;
    elseif($typ=='clanek') return TCLANEK;
    elseif($typ=='clanky') return TSTRANKA_S_CLANKY;
    elseif($typ=='novinka') return TNOVINKA;
    elseif($typ=='novinky') return TSTRANKA_S_NOVINKAMI;
    elseif($typ=='archiv-clanku') return TARCHIV_CLANKU;
    elseif($typ=='sitemap') return TMAPA_WEBU;
    elseif($typ=='uvodni') return TUVODNI_STRANKA;
    elseif($typ=='akce') return TAKCE; 
    elseif($typ=='kalendar-typ') return TTYP_AKCE; 
    elseif($typ=='kalendar-akci') return TKALENDAR_AKCI; 
    elseif($typ=='tagy') return TTAGY;
    elseif($typ=='tag') return TTAG;  
    elseif($typ=='galerie') return TGALERIE; 
    elseif($typ=='stranka-galerie') return TSTRANKA_S_GALERII;
    elseif($typ=='galerie-slozka') return TSLOZKA_GALERIE;
    elseif($typ=='galerie-fotka') return TFOTKA;  
    elseif($typ=='404') return TCHYBOVA_STRANKA;
    elseif($typ=='produkt') return TPRODUKT;
    elseif($typ=='produkty') return TSTRANKY_S_PRODUKTY;
    elseif($typ=='email') return TSTRANKA_EMAIL;
    elseif($typ=='login') return TPRIHLASOVACI_STRANKA;
    elseif($typ=='obchodni-podminky') return TSTRANKA_OBCHODNI_PODMINKY;
    elseif($typ=='objednavka') return TSTRANKA_OBJEDNAVKA;       
    else return "";
}






$data = $db->query("SELECT s.idStranky AS id, s.perexBezHtml AS perex, s.obsahBezHtml AS obsah,
				IF(s.nadpis='', s.nazev, s.nadpis) AS nazev,
				5 * MATCH(s.nadpis) AGAINST ('".$term."') + 3 * MATCH(s.perexBezHtml) AGAINST ('".$term."') + MATCH(s.obsahBezHtml) AGAINST ('".$term."') AS vaha1,
                IF(s.nadpis LIKE ('%".$term."%') OR s.nazev LIKE ('%".$term."%'), 1, 0) AS vaha2,
                s.typ
			FROM ".TABLE_STRANKY." AS s 
            WHERE s.idDomeny=".$domain->getId()."              
            HAVING (vaha1 + vaha2) > 0
            LIMIT 10
            
            ");



$result = array();

$html = "";

include("../functions/functions.inputs.php");




while($s = $db->getObject($data))
{
    /*
    $result[] = array(
        "value" => $s['id'],
        "name" => $s['nazev'],
        "url" => $links->getUrl($s['id']),
        "type" => typ_nazev($s['typ'])
        );
    */
    
    $nav = get_navigation($s->id);
    
    
    if(!in_array($s->typ, array('uvodni','404')))
    {
        //$u = $db->get(TABLE_STRANKY,array("nazev","idStranky"),"typ='uvodni' AND idDomeny=".$domain->getId()." AND idJazyka=".WEB_LANG_ID);
        //$nav[$u->idStranky] = $u->nazev;
        
        $nav[] = "<span class='fa fa-home'></span>";
    }
    $nav = array_reverse($nav,true);
    
    $obsah = CropAndSaveWords(trim($s->perex." ".$s->obsah),$term, 150);
    
    $html .= '<a href="'.$links->get_url($s->id).'" class="ine_result">
        <em>'.TUMISTENI.': '.implode(" <span>&rsaquo;</span> ", $nav).'</em>
        <strong>'.typ_nazev($s->typ).": ".$s->nazev.'</strong>
        <span class="ine_popis">'.MarkWord($obsah, $term).'</span>
        <span class="ine_sipka">&rsaquo;</span>
        </a>';
    
    
    
}

//echo array2json($result);
echo $html;


?>