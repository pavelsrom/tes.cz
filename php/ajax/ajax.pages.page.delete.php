<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */




$idStranky = get_int_post('id');

if(!is_post('id') || !defined("SECURITY_CMS")) exit;
if(!$login_obj->UserPrivilege('content_delete') || !$object_access->has_access($idStranky)) exit;

$typ = $db->get(TABLE_STRANKY,'typ',"idStranky='".$idStranky."' AND idDomeny=".$domain->getId());

$nemazat = false;
if($typ=="")
    $nemazat = true;
    else
    {

    if(in_array($typ,array('sitemap','stranka-galerie','archiv-clanku','novinky')))
        $nemazat = true;                    
    }
 
$d = $db->Query("SELECT nazev, typ 
    FROM ".TABLE_STRANKY." 
    WHERE idStranky=".$idStranky." 
        AND typ IN ('stranka','clanky','novinky','produkty') 
        AND idDomeny=".$domain->getId()." 
    LIMIT 1");           

$result = array("messages" => array("ok" => "","error" => ""));
            
if($nemazat || $db->numRows($d)==0)
{
    $result['messages']['error'] = ERROR_NELZE_SMAZAT;
    echo array2json($result);
    exit;
}
    
    
			
            
$dtyp = $db->getAssoc($d);
$typ = $dtyp['typ'];
$nazev = $dtyp['nazev'];

            
if($typ=='sitemap')
    $systemMessage->add_error(ERROR_STRANKA_OBSAHUJE_MAPU_WEBU_A_NELZE_JI_SMAZAT);
elseif($typ=='stranka'){
    $d = $db->Query("SELECT idStranky FROM ".TABLE_STRANKY." WHERE idRodice=".$idStranky." LIMIT 1");
    if($db->numRows($d)>0)
        $result['messages']["error"] = ERROR_STRANKA_JE_NADRAZENOU_STRANKOU_JINE_STRANKY;
                  
    }
elseif($typ=='novinky'){
    $d = $db->Query("SELECT idStranky FROM ".TABLE_STRANKY." WHERE typ='novinka' LIMIT 1");
    if($db->numRows($d)>0)
        $result['messages']["error"] = ERROR_STRANKA_OBSAHUJE_NOVINKY;
                
    }
elseif($typ=='clanky'){
    $d = $db->Query("SELECT idStranky FROM ".TABLE_STRANKY." WHERE typ='clanek' AND idRodice=".$idStranky." LIMIT 1");
    if($db->numRows($d)>0)
        $result['messages']["error"] = ERROR_STRANKA_OBSAHUJE_CLANKY;                
    }        
         
				
if($result['messages']["error"] == ""){
    $db->delete(TABLE_STRANKY,"idStranky=".$idStranky." AND idDomeny=".$domain->getId()." LIMIT 1");
    $db->delete(TABLE_STRANKY_TAGY, "idObjektu=".$idStranky." AND typObjektu='stranka'");
    $db->delete(TABLE_STRANKY_EDITORI,"idObjektu=".$idStranky." AND typ = 'stranka'");
    $db->delete(TABLE_ODKAZY,"typ='stranka' AND idObjektu=".$idStranky);
    $db->delete(TABLE_ZALOHY,"typ='stranka' AND idObjektu=".$idStranky);
    $db->delete(TABLE_DISKUZE_POLOZKY,"idStranky=".$idStranky);
    $db->delete(TABLE_STRANKY_SKUPINY, "idStranky=".$idStranky);
    
    $result['messages']["ok"] = OK_SMAZANO;    

    //zjisteni url stranky pro uvodku, po smazani stranky v inline editaci bude uzivatel nasmerovan na uvodni stranku
    $id_uvodni_stranky = $db->get(TABLE_STRANKY,"idStranky","idDomeny=".$domain->getId()." AND typ='uvodni' AND idJazyka=".WEB_LANG_ID);
    $url = UrlPage("", $id_uvodni_stranky, WEB_LANG);
    $result['messages']["redirect_url"] = $url; 
 
    $log->add_log('delete','stranka',$idStranky,$nazev);

    }




echo array2json($result);


?>