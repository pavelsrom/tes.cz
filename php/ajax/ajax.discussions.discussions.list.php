<?php

/**
 * @author Pavel Srom
 * @copyright 2010
 */

//ajaxovy skript na odhlaseni kontaktu z odberu newsletteru

if(!isset($_GET['module']) || !defined("SECURITY_CMS")) exit;

$vyraz = trim($db->secureString($_GET['vyraz']));
$module = trim($_GET['module']);

//nastaveni limitu
$limit = "";
if(isset($_GET['iDisplayLength'])){
    $dStart = "";
    if(isset($_GET['iDisplayStart']))
        $dStart = abs(intval($_GET['iDisplayStart'])).",";
        
    $limit = " LIMIT ".$dStart.abs(intval($_GET['iDisplayLength'])); 
}


//trideni podle sloupcu
$order = "";
if(isset($_GET['iSortCol_0'])){
    $s = abs(intval($_GET['iSortCol_0']));
    if($s == 0) $order = "d.idStranky";
    elseif($s == 1) $order = "d.nazev";
    elseif($s == 2) $order = "pocet_prispevku";
    elseif($s == 3) $order = "d.zobrazit";
    else $order = "d.idStranky";
}

if($order != '') 
    $order = "ORDER BY ".$order." ".(isset($_GET['sSortDir_0'])?$_GET['sSortDir_0']:'');

//echo $order;

//sestaveni podminky pro vyhledavani

$like = array();
if($vyraz != '')
{
    $like[] = "(d.idStranky LIKE '".$vyraz."%' OR d.nazev LIKE '%".$vyraz."%')";
}


$where = "WHERE d.idJazyka=".WEB_LANG_ID." AND d.diskuze=1 AND d.idDomeny=".$domain->getId().((count($like) > 0) ? " AND (".implode(" AND ",$like).")" : "");

$data_pocet = $db->Query("SELECT COUNT(idStranky) AS celkem
    FROM ".TABLE_STRANKY." AS d
    ".$where."
    ");
    
$iTotal_arr = $db->getAssoc($data_pocet);
$iTotal = $iTotal_arr['celkem'];


        
$data = $db->Query("SELECT SQL_CALC_FOUND_ROWS d.nazev, d.url, d.idStranky, COUNT(dp.idPolozky) AS pocet_prispevku, d.zobrazit, d.typ,
    (SELECT COUNT(dp1.idPolozky) FROM ".TABLE_DISKUZE_POLOZKY." AS dp1 WHERE dp1.idStranky = d.idStranky AND novy=1 AND admin=0) AS pocet_novych, j.jazyk
            FROM ".TABLE_STRANKY." AS d
            LEFT JOIN ".TABLE_JAZYKY." AS j ON d.idJazyka = j.idJazyka
            LEFT JOIN ".TABLE_DISKUZE_POLOZKY." AS dp ON d.idStranky = dp.idStranky
            ".$where."
            GROUP BY d.idStranky
            ".$order."
            ".$limit."
            ");


$iFilteredTotal_data = $db->Query("SELECT FOUND_ROWS()");
$iFilteredTotal_arr = $db->getRow($iFilteredTotal_data);
$iFilteredTotal = intval($iFilteredTotal_arr[0]);


$dis_content = !$login_obj->UserPrivilege('content_view');
$dis_delete = !$login_obj->UserPrivilege('settings_delete');
$dis_settings = !$login_obj->UserPrivilege('settings_view');

$tr = array();
while($c = $db->getAssoc($data))
{
    $ids = $c['idStranky'];
    $typ = $c['typ'];
    $pom = array();
    $pom[] = json_encode($ids);
    $pom[] = json_encode(secureString($c['nazev']));
    $pom[] = json_encode(($c['pocet_novych'] > 0 ? "<span class='stav0'>".intval($c['pocet_novych'])."</span>" : 0)." / ".$c['pocet_prispevku']);
    $pom[] = json_encode(ano_ne($c['zobrazit']));
    
    
    $ikony = "";
    $ikony .= "<a href='".urlPage($c['url'],$ids,$c['jazyk'])."' target='_blank'>".icon('link')."</a>";  
    
    if($dis_content)
        $ikony .=  icon_disabled('content');
    elseif($typ == 'stranka' || ($typ == 'novinky' && MODULE_NEWS) || ($typ == 'clanky' && MODULE_ARTICLES) || ($typ == 'stranka-galerie' && MODULE_GALLERY))
        $ikony .= "<a href='".get_link("pages","stranky",0,"edit-content",$ids)."'>".icon('content',TPREJIT_NA_OBSAH_STRANKY_S_DISKUZI)."</a>";
    elseif($typ == 'clanek' && MODULE_ARTICLES)
        $ikony .= "<a href='".get_link("articles","rubriky",$link->get($ids,'rodic'),"edit-content",$ids)."'>".icon('content',TPREJIT_NA_OBSAH_STRANKY_S_DISKUZI)."</a>";
    elseif($typ == 'novinka' && MODULE_NEWS)
        $ikony .= "<a href='".get_link("news","novinky",0,"edit-content",$ids)."'>".icon('content',TPREJIT_NA_OBSAH_STRANKY_S_DISKUZI)."</a>";
    elseif(($typ == 'galerie' || $typ='galerie-slozka') && MODULE_GALLERY)
        $ikony .= "<a href='".get_link("gallery","galerie",0,"edit-settings",$ids)."'>".icon('content',TPREJIT_NA_OBSAH_STRANKY_S_DISKUZI)."</a>";
    elseif($typ == 'akce' && MODULE_CALENDAR)
        $ikony .= "<a href='".get_link("calendar","akce",0,"edit-content",$ids)."'>".icon('content',TPREJIT_NA_OBSAH_STRANKY_S_DISKUZI)."</a>";        
    elseif($typ == 'kalendar-typ' && MODULE_CALENDAR)
        $ikony .= "<a href='".get_link("calendar","akce-typy",0,"edit-content",$ids)."'>".icon('content',TPREJIT_NA_OBSAH_STRANKY_S_DISKUZI)."</a>";
    elseif($typ == 'produkt' && MODULE_ESHOP)
        $ikony .= "<a href='".get_link("eshop","kategorie",$link->get($ids,'rodic'),"edit-content",$ids)."'>".icon('content',TPREJIT_NA_OBSAH_STRANKY_S_DISKUZI)."</a>";
            
    if($dis_content)
        $ikony .=  icon_disabled('discussion');
        else
        $ikony .= "<a href='".get_link($module,'diskuze',$ids)."'>".icon('discussion')."</a>";
    
    $pom[] = json_encode($ikony);
        
    $tr[] = "[".implode(',', $pom)."]";
    
}

echo "{";
echo '"sEcho": '.(intval($_GET['sEcho'])).', ';
echo '"iTotalRecords": '.$iTotal.', ';
echo '"iTotalDisplayRecords": '.($iFilteredTotal).', ';
echo '"aaData": ['.implode(',', $tr).'] }';


?>