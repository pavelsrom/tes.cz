<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */
 
 
if(!$login_obj->UserPrivilege('content_delete') || !defined("SECURITY_CMS")) exit;


$id = get_int_post('id');

if($id <= 0 || !$object_access->has_access($id)) exit;

$data = $db->query("SELECT nazev,obrazek FROM ".TABLE_STRANKY." WHERE idStranky=".$id." AND idDomeny=".$domain->getId()." AND idJazyka=".WEB_LANG_ID." AND typ='produkt' LIMIT 1");
if($db->numRows($data) == 0) exit;

$n = $db->getAssoc($data);
$nazev = $n['nazev'];
$soubor = $n['obrazek'];
  
if($soubor!='' && file_exists(PRE_PATH.$domain->getDir().USER_DIRNAME_ESHOP_MAXI.$soubor))
    @unlink(PRE_PATH.$domain->getDir().USER_DIRNAME_ESHOP_MAXI.$soubor);
  
if($soubor!='' && file_exists(PRE_PATH.$domain->getDir().USER_DIRNAME_ESHOP_MINI.$soubor))
    @unlink(PRE_PATH.$domain->getDir().USER_DIRNAME_ESHOP_MINI.$soubor);
  
if($soubor!='' && file_exists(PRE_PATH.$domain->getDir().USER_DIRNAME_ESHOP_STREDNI.$soubor))
    @unlink(PRE_PATH.$domain->getDir().USER_DIRNAME_ESHOP_STREDNI.$soubor);
  
  
$log->add_log('delete',"produkt",$id,$nazev);

$db->delete(TABLE_STRANKY, "idStranky=".$id." AND idDomeny=".$domain->getId()." LIMIT 1");
$db->delete(TABLE_STRANKY_TAGY, "idObjektu=".$id." AND typObjektu='stranka'");
$db->delete(TABLE_STRANKY_EDITORI,"idObjektu=".$id." AND typ = 'stranka'");
$db->delete(TABLE_ODKAZY,"typ='stranka' AND idObjektu=".$id);
$db->delete(TABLE_ZALOHY,"typ='stranka' AND idObjektu=".$id);
$db->delete(TABLE_DISKUZE_POLOZKY,"idStranky=".$id);
$db->delete(TABLE_STRANKY_SKUPINY, "idStranky=".$id);
exit;


?>