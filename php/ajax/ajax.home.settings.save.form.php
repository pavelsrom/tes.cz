<?php

/**
 * @author Pavel Srom
 * @copyright 2014
 */

$result = array(
    "messages" => array(
        "error" => ""
        )
    );

if(!defined("SECURITY_CMS")) exit;
if(!$login_obj->UserPrivilege('settings_edit')){
    $result["messages"]["error"] = ERROR_NEDOSTATECNA_PRAVA;
    echo array2json($result);
    exit;
} 

$links = new C_Links;


$nastaveni_data = get_array_post('nastaveni');
$n = array();
$typy = array();

foreach($nastaveni_data AS $p)
{
    $id = str_replace("h","",$p['name']);
    $id = intval($id);
    if($id > 0)
    {
        $n[$id] = $db->secureString($p['value'],false);
        continue;
    }
            
    $id = str_replace("typ","",$p['name']);
    $id = intval($id);
    if($id > 0)
    {
        $typy[$id] = $db->secureString($p['value']);
    }  
    
}
   
$data = $db->query("SELECT n.idNastaveni, n.typ, nd.idRelace, 
        IF(nd.hodnota IS NULL, n.hodnota,nd.hodnota) AS hodnota 
        FROM ".TABLE_NASTAVENI." AS n 
        LEFT JOIN ".TABLE_NASTAVENI_DOMENY." AS nd ON n.idNastaveni=nd.idNastaveni AND nd.idDomeny = ".$domain->getId()."
        WHERE n.aktivni=1
            AND n.zakladni=1
        GROUP BY n.idNastaveni
        ");
    
while($nastaveni = $db->getAssoc($data))
{
        $idRelace = intval($nastaveni['idRelace']);
        $idNastaveni = $nastaveni['idNastaveni'];
        $typ = $nastaveni['typ'];
        
        if(!isset($n[$idNastaveni])) 
            continue;
        
        if(!isset($n[$idNastaveni]) && $typ!='ga') 
            continue;
        
        $hodnota = $n[$idNastaveni];
        
        if($typ == 'ga')
        {
            $hash = $domain->get('hash');
            $h = array(
                "GA_PROFIL" => get_post('gaProfil'),
                "GA_HESLO"  => get_post('gaHeslo'),
                "GA_EMAIL"  => get_post('gaEmail')
                );
    
            $rc4 = new rc4crypt;
            $h['GA_HESLO'] = $rc4->endecrypt($hash,$h['GA_HESLO']);
            
            $hodnota = serialize($h);
        }
        elseif($typ == "strom")
        {
            $typh = isset($typy[$idNastaveni]) ? $typy[$idNastaveni] : "";
            
            if($typh == "")
                continue;
            
            $hodnota_stara = array();
            if(trim($nastaveni['hodnota']) != '')
                $hodnota_stara = unserialize($nastaveni['hodnota']);       
               
            $idj = WEB_LANG_ID;
            $h = $hodnota;
        
            $id_rodic_stary = $db->get(TABLE_STRANKY, $typh, "idDomeny=".$domain->getId()." AND idJazyka=".$idj);
            $id_rodic = intval($h);
            $update = array("idRodice" => $id_rodic);
            $where = " idDomeny=".$domain->getId()." AND idRodice=".$id_rodic_stary." AND idJazyka=".$idj." AND typ!='clanky'";
            $where1 = "idDomeny=".$domain->getId()." AND idJazyka=".$idj." AND typ='".$typh."'";
            $where2 = "idDomeny=".$domain->getId()." AND idJazyka=".$idj." AND idStranky=".$id_rodic;
                
            if($typh == "sitemap" || $typh == 'vyhledavani')
            {
                    
            }
            else
            {
                    
                if($typh == 'stranka-galerie')
                    $where .= " AND typ IN ('galerie','galerie-slozka')";
                elseif($typh == 'kalendar-akci')
                    $where .= " AND typ IN ('kalendar-typ')";
                elseif($typh == 'novinky')
                    $where .= " AND typ = 'novinka'";
                elseif($typh == 'tagy')
                    $where .= " AND typ = 'tag'";   
                else
                    continue;
                             
                $db->update(TABLE_STRANKY,$update, $where); 
            }
                
            $db->update(TABLE_STRANKY,array('typ' => 'stranka'),$where1);
            $db->update(TABLE_STRANKY,array('typ' => $typh),$where2);
            
            $hodnota_stara[WEB_LANG_ID] = $h; 
            $hodnota_stara["typ"] = $typh; 

            $hodnota = serialize($hodnota_stara);
            
        }
        
        
        if($idRelace > 0)
            $db->update(TABLE_NASTAVENI_DOMENY,array("hodnota" => $hodnota),"idRelace=".$idRelace);
            else
            $db->insert(TABLE_NASTAVENI_DOMENY,array("hodnota" => $hodnota,"idNastaveni" => $idNastaveni,"idDomeny" => $domain->getId()));
        
        


}

$result["messages"]["ok"] = OK_ULOZENO;

echo array2json($result);

?>