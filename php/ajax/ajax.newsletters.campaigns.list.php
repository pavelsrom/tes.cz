<?php

/**
 * @author Pavel Srom
 * @copyright 2010
 */

//ajaxovy skript na odhlaseni kontaktu z odberu newsletteru

if(!isset($_GET['module']) || !defined("SECURITY_CMS")) exit;

$vyraz = trim($db->secureString($_GET['vyraz']));
$module = trim($_GET['module']);

//nastaveni limitu
$limit = "";
if(isset($_GET['iDisplayLength'])){
    $dStart = "";
    if(isset($_GET['iDisplayStart']))
        $dStart = abs(intval($_GET['iDisplayStart'])).",";
        
    $limit = " LIMIT ".$dStart.abs(intval($_GET['iDisplayLength'])); 
}


//trideni podle sloupcu
$order = "";
if(isset($_GET['iSortCol_0'])){
    $s = abs(intval($_GET['iSortCol_0']));
    if($s == 0) $order = "k.idKampane";
    elseif($s == 1) $order = "k.nazev";
    elseif($s == 2) $order = "jazyk";
    elseif($s == 3) $order = "k.start";
    else $order = "k.idKampane";
}

if($order != '') 
    $order = "ORDER BY ".$order." ".(isset($_GET['sSortDir_0'])?$_GET['sSortDir_0']:'');

//echo $order;

//sestaveni podminky pro vyhledavani
$like = array();
if($vyraz != '')
{
    $like[] = "k.idKampane LIKE '".$vyraz."%'";
    $like[] = "k.nazev LIKE '%".$vyraz."%'";
}
    


$where = "WHERE k.idDomeny=".$domain->getId().((count($like) > 0) ? " AND (".implode(" OR ",$like).")" : "");

$data_pocet = $db->Query("SELECT COUNT(idKampane) AS celkem FROM ".TABLE_EMAILY_KAMPANE." AS k ".$where);
$iTotal_arr = $db->getAssoc($data_pocet);
$iTotal = $iTotal_arr['celkem'];

$data = $db->Query("SELECT SQL_CALC_FOUND_ROWS DISTINCT k.*, j.jazyk AS jazyk,
                IF(k.start IS NOT NULL, DATE_FORMAT(k.start,'%d.%m.%Y'), '') AS start
			FROM ".TABLE_EMAILY_KAMPANE." AS k 
			LEFT JOIN ".TABLE_JAZYKY." AS j ON j.idJazyka=k.idJazyka 

			".$where."
            GROUP BY k.idKampane
            ".$order."
            ".$limit."
            ");

$iFilteredTotal_data = $db->Query("SELECT FOUND_ROWS()");
$iFilteredTotal_arr = $db->getRow($iFilteredTotal_data);
$iFilteredTotal = intval($iFilteredTotal_arr[0]);

$dis_settings = !$login_obj->UserPrivilege('settings_view');
$dis_statistics = !$login_obj->UserPrivilege('statistics_view');
$dis_delete = !$login_obj->UserPrivilege('settings_delete');

$campaign_status = array(
    "rozpracovano" => TROZPRACOVANO, 
    "odesilano" => TPRAVE_PROBIHA, 
    "ukonceno" => TUKONCENA
    );

$tr = array();
while($c = $db->getAssoc($data))
{
    $ids = $c['idKampane'];
    $stav = $c['stav'];
    
    
    $date = explode('-',$c['start']);
				
    $s_start = "";
    if($c['start']!='') 
        $s_start = strftime("%d.%m.%Y", strtotime($c['start']));
        else
        $s_start = "<i>".TNENI_NASTAVENO."</i>";
    
    
    $pom = array();
    $pom[] = json_encode($ids);
    $pom[] = json_encode(secureString($c['nazev']));   
    $pom[] = json_encode($c['jazyk']);
    $pom[] = json_encode($c['start']);

    if($stav == 'rozpracovano')
        $stav_format = "<span class='stav0'>";
    if($stav == 'ukonceno')
        $stav_format = "<span class='stav1'>";
    if($stav == 'odesilano')
        $stav_format = "<span class='stav2'>";
        
    $stav_format .= $campaign_status[$stav];
    $stav_format .= "</span>";

    $pocet_odeslanych = $stav != 'rozpracovano' ? "<br />(".TODESLANO." ".$c['odeslano']."/".$c['celkem'].")" : "";
    $pom[] = json_encode($stav_format.$pocet_odeslanych);
        
    $ikony = "";
    if($dis_settings)
        $ikony .= icon_disabled('content');
        else
        $ikony .=  "<a href='".get_link($module,'kampane',0,'edit-settings',$ids)."'>".icon('content')."</a>";

    if($dis_delete)
        $ikony .= icon_disabled('delete');
        else
        $ikony .= "<a href='#' class='delete' id='delete".$ids."'>".icon('delete')."</a>";
        
    $pom[] = json_encode($ikony);
    $tr[] = "[".implode(',', $pom)."]";
    
}

echo "{";
echo '"sEcho": '.(intval($_GET['sEcho'])).', ';
echo '"iTotalRecords": '.$iTotal.', ';
echo '"iTotalDisplayRecords": '.($iFilteredTotal).', ';
echo '"aaData": ['.implode(',', $tr).'] }';


?>