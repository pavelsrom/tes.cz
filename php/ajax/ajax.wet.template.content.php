<?php

/**
 * @author Pavel Srom
 * @copyright 2010
 */

if(!isset($_POST['idTemplate'], $_POST['idDomain']) || !defined("SECURITY_CMS")) exit;

//ID DOMAIN V POSTU ZDE INDIKUJE ZDA SE MA NACIST SABLONA VYTVORENA PRO UCELY CMS NEBO UZIVATELSKA A SYSTEMOVA SABLONA	
if($_POST['idDomain']!=1)
	$idDomain = 0;	
	
	
$idTemplate = intval($_POST['idTemplate']);

if($idTemplate<0) exit;

if(!$login_obj->UserPrivilege('content_view')) exit;

if($idTemplate>0){
	$data = $db->Query("SELECT obsah FROM ".TABLE_HTML_SABLONY." WHERE idSablony=".$idTemplate." AND (idDomeny=".$idDomain." OR systemova=1) LIMIT 1");
	if($db->numRows($data)==1) {
		$templ = $db->getAssoc($data);
        //$obsah_sablony = strip_selected_tags($templ['obsah'], '<html><body><!doctype><title>',true);
        $obsah_sablony = $templ['obsah'];
		echo strip_selected_tags($obsah_sablony, '<title>',true);
		}
		else
		echo "";
	}

exit;


?>