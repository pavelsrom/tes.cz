<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!$login_obj->UserPrivilege('content_edit') || !defined("SECURITY_CMS")) exit;

$idOdpovedi = get_int_post('id');
$idAnkety = get_int_post('idAnkety'); 
$nazev = $db->get(TABLE_ANKETY,"nazev","idAnkety=".$idAnkety." AND idDomeny=".$domain->getId());
if($idAnkety <= 0 || $idOdpovedi <= 0 || $nazev=="") 
    exit;
                     
$result = $polozky = array();
            
$data = $db->Query("SELECT idOdpovedi 
                FROM ".TABLE_ANKETY_ODPOVEDI."
                WHERE idAnkety=".$idAnkety."
                ORDER BY priorita       
                ");
            
$i=0;
while($p = $db->getAssoc($data)){
    $polozky[$i] = $p['idOdpovedi'];
    $result[$i] = $p['idOdpovedi'];
    $i++;
    }

foreach($polozky AS $i => $pol){
    if($idOdpovedi == $pol && isset($polozky[$i + 1]))
        list($result[$i + 1], $result[$i]) = array($polozky[$i], $polozky[$i + 1]);
    $i++;    
    }

foreach($result AS $priorita => $id)
    $db->Query("UPDATE ".TABLE_ANKETY_ODPOVEDI." SET priorita=".$priorita." WHERE idOdpovedi=".$id." LIMIT 1");
                
$log->add_log('edit-content','anketa',$idAnkety,$nazev);

?>