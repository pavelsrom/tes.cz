<?php

/**
 * @author Pavel Srom
 * @copyright 2014
 */

$idStranky = get_request("page_id");

if (empty($_FILES) || $idStranky <= 0) exit;

$type = $_FILES['fotka']['type'];

$result = array(
    "file" => array("html" => ""),
    "messages" => array(
        "error" => ""
        )

    );
    
if(!stristr($type,'jpeg') && !stristr($type,'jpg') && !stristr($type,'gif') && !stristr($type,'png'))
{
    $result['messages']['error'] = $_FILES['fotka']['name'].": ".TNELZE_NAHRAT." - ".ERROR_NEPODPOROVANY_FORMAT_SOUBORU; 
    echo array2json($result);
    exit; 
}


include_once(PRE_PATH.'php/classes/class.upload.php');



$articles_path_mini = PRE_PATH.$domain->getDir().USER_DIRNAME_STRANKY_MINI;
$articles_path_maxi = PRE_PATH.$domain->getDir().USER_DIRNAME_STRANKY_MAXI;

$uploader = new upload($_FILES['fotka'], 'cs_CS');
$nazev_souboru = $uploader->file_src_name;

$uploader->file_new_name_body = ModifyUrl($uploader->file_src_name_body);
$uploader->image_resize = true;
$uploader->image_ratio = true;
$uploader->image_x = intval(OBRAZEK_STRANKY_MAXI_X);
$uploader->image_y = intval(OBRAZEK_STRANKY_MAXI_Y);
//$uploader->image_convert = 'jpg';
$uploader->process($articles_path_maxi);


$error = $uploader->error;
if($error != '')
{
    $result['messages']['error'] = $nazev_souboru.": ".TNELZE_NAHRAT." - ".$error; 
    echo array2json($result);
    exit; 
}

$uploader->file_new_name_body = ModifyUrl($uploader->file_src_name_body);
$uploader->image_resize = true;
$uploader->image_ratio = true;
$uploader->image_x = intval(OBRAZEK_STRANKY_MINI_X);
$uploader->image_y = intval(OBRAZEK_STRANKY_MINI_Y);
//$uploader->image_convert = 'jpg';
$uploader->process($articles_path_mini);

$nazev = $uploader->file_dst_name;

$error = $uploader->error;
if($error != '')
{
    $result['messages']['error'] = $nazev_souboru.": ".TNELZE_NAHRAT." - ".$error; 
    echo array2json($result);
    exit; 
}

$uploader->clean();
          

$insert = array(
    "idStranky" => $idStranky,
    "soubor" => $nazev
    );

$db->insert(TABLE_STRANKY_FOTKY, $insert);
$idFotky = $db->lastId();

$log->add_log('upload','stranka-fotka',$idStranky,$nazev);

$result['messages']['ok'] = $nazev_souboru.": ".TBYL_USPESNE_NAHRAN; 
$result["file"]["name"] = $nazev;
$result["file"]["html"] = '<div class="sortable_checkbox">
<table>
    <tr>
        <th>
            <img src="'.RELATIVE_URL_USER_DOMAIN.USER_DIRNAME_STRANKY_MINI.$nazev.'" alt="">
            
        </th>
        <td>
            <textarea rows="" cols="" name="fotka['.$idFotky.']" placeholder="'.TPOPIS.'"></textarea>
            <a href="#" class="photo-delete" id="p'.$idFotky.'">'.TSMAZAT.'</a>
        </td>
    </tr>
</table>
</div>
    ';

echo array2json($result);
exit;


?>