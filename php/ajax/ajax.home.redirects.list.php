<?php

/**
 * @author Pavel Srom
 * @copyright 2010
 */

//ajaxovy skript na odhlaseni kontaktu z odberu newsletteru

if(!isset($_GET['module']) || !defined("SECURITY_CMS")) exit;

$vyraz = trim($db->secureString($_GET['vyraz']));
$module = trim($_GET['module']);

//nastaveni limitu
$limit = "";
if(isset($_GET['iDisplayLength'])){
    $dStart = "";
    if(isset($_GET['iDisplayStart']))
        $dStart = abs(intval($_GET['iDisplayStart'])).",";
        
    $limit = " LIMIT ".$dStart.abs(intval($_GET['iDisplayLength'])); 
}


//trideni podle sloupcu
$order = "";
if(isset($_GET['iSortCol_0'])){
    $s = abs(intval($_GET['iSortCol_0']));
    if($s == 0) $order = "id";
    //elseif($s == 1) $order = "nazev";
    else $order = "id";
}

if($order != '') 
    $order = "ORDER BY ".$order." ".(isset($_GET['sSortDir_0'])?$_GET['sSortDir_0']:'');

//echo $order;

//sestaveni podminky pro vyhledavani
$like = array();
if($vyraz != '')
{
    $like[] = "id LIKE '".$vyraz."%'";
    $like[] = "zdroj LIKE '%".$vyraz."%'";
    $like[] = "cil LIKE '%".$vyraz."%'";
}
    


$where = "WHERE idDomeny=".$domain->getId()." ".((count($like) > 0) ? " AND (".implode(" OR ",$like).")" : "");

$data_pocet = $db->Query("SELECT COUNT(id) AS celkem FROM ".TABLE_PRESMEROVANI." ".$where);
$iTotal_arr = $db->getAssoc($data_pocet);
$iTotal = $iTotal_arr['celkem'];


        
$data = $db->Query("SELECT SQL_CALC_FOUND_ROWS * FROM ".TABLE_PRESMEROVANI." AS s
				".$where."
				GROUP BY id
				".$order."
                ".$limit."
				");


$iFilteredTotal_data = $db->Query("SELECT FOUND_ROWS()");
$iFilteredTotal_arr = $db->getRow($iFilteredTotal_data);
$iFilteredTotal = intval($iFilteredTotal_arr[0]);


$dis_content = !$login_obj->UserPrivilege('content_view');
$dis_delete = !$login_obj->UserPrivilege('content_delete');

$tr = array();
while($c = $db->getAssoc($data))
{
    $ids = $c['id'];
    $pom = array();
    $pom[] = json_encode($ids);
    $pom[] = json_encode("<a href='".get_url_for_redirect($c['zdroj'])."' target='_blank'>".secureString($c['zdroj'])."</a>");
    $pom[] = json_encode(secureString($c['cil']));
    $pom[] = json_encode(ano_ne($c['aktivni']));
    
    $ikony = "";
    /*
    if($c['aktivni'])
        $ikony .= "<a href='".get_url_for_redirect($c['cil'])."' target='_blank'>".icon('link',TTEST_PRESMEROVANI)."</a>";
        */
    
    if($dis_content)
        $ikony .= icon_disabled('content');
        else
        $ikony .= "<a href='".get_link($module,'presmerovani',0,'edit-content',$ids)."'>".icon('content')."</a>";
    
    if($dis_delete)
        $ikony .= icon_disabled('delete');
        else
        $ikony .= "<a href='#' class='delete' id='delete".$ids."'>".icon('delete')."</a>";
        
    $pom[] = json_encode($ikony);
        
    $tr[] = "[".implode(',', $pom)."]";
    
}

echo "{";
echo '"sEcho": '.(intval($_GET['sEcho'])).', ';
echo '"iTotalRecords": '.$iTotal.', ';
echo '"iTotalDisplayRecords": '.($iFilteredTotal).', ';
echo '"aaData": ['.implode(',', $tr).'] }';


?>