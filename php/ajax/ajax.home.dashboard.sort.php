<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined("SECURITY_CMS")) exit;

$pravy = is_post("pravy") ? implode("#",(array)$_POST['pravy']) : "#";
$levy = is_post("levy") ? implode("#",(array)$_POST['levy']) : "#";

$update = array(
    'nastaveniDashboardLevy' => "#".$levy."#",
    'nastaveniDashboardPravy'=> "#".$pravy."#"
    );
$db->update(TABLE_UZIVATELE, $update, "idUzivatele=".$login_obj->getId());

exit;

?>