<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!$login_obj->UserPrivilege('settings_delete') || !defined("SECURITY_CMS")) exit;

$db->delete(TABLE_VYHLEDAVANI_VYRAZY,"WHERE idDomeny=".$domain->getId());
//$db->update(TABLE_VYHLEDAVANI_VYRAZY,array("cetnost" => 0),"idDomeny=".$domain->getId());

$log->add_log('delete','statistiky-nastaveni');

echo OK_VYNULOVANO;


?>