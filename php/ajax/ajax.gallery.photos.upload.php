<?php

/**
 * @author Pavel Srom
 * @copyright 2014
 */

$idStranky = get_request("page_id");

if (empty($_FILES) || $idStranky <= 0) exit;


if($idStranky == 0)
    $idStranky = get_int_request('id_ine_page',$idStranky);


$type = $_FILES['file']['type'];

$result = array(
    "file" => array("html" => ""),
    "messages" => array(
        "error" => ""
        )

    );
    
if(!stristr($type,'jpeg') && !stristr($type,'jpg') && !stristr($type,'gif') && !stristr($type,'png'))
{
    $result['messages']['error'] = TNELZE_NAHRAT." - ".ERROR_NEPODPOROVANY_FORMAT_SOUBORU; 
    echo array2json($result);
    exit; 
}


include_once(PRE_PATH.'php/classes/class.upload.php');



$articles_path_mini = PRE_PATH.$domain->getDir().USER_DIRNAME_GALLERY_MINI;
$articles_path_stredni = PRE_PATH.$domain->getDir().USER_DIRNAME_GALLERY_STREDNI;
$articles_path_maxi = PRE_PATH.$domain->getDir().USER_DIRNAME_GALLERY_MAXI;

$uploader = new upload($_FILES['file'], 'cs_CS');
$nazev_souboru = $uploader->file_src_name;

$db->Query("INSERT INTO ".TABLE_STRANKY." (idDomeny,typ, zobrazit, idRodice,doMenu, datum) VALUES (".$domain->getId().",'galerie-fotka',1,".$idStranky.",0,NOW()) ");
                
$idObrazku = $db->lastId();
$file = $idObrazku.".jpg";

$uploader->file_new_name_body = $idObrazku;
$uploader->image_resize = true;
$uploader->image_ratio = true;
$uploader->image_x = intval(OBRAZEK_GALERIE_MAXI_X);
$uploader->image_y = intval(OBRAZEK_GALERIE_MAXI_Y);
$uploader->image_convert = 'jpg';
$uploader->process($articles_path_maxi);


$error = $uploader->error;
if($error != '')
{
    $result['messages']['error'] = TNELZE_NAHRAT." - ".$error; 
    echo array2json($result);
    exit; 
}

//$uploader->file_new_name_body = ModifyUrl($uploader->file_src_name_body);
$uploader->file_new_name_body = $idObrazku;
$uploader->image_resize = true;
$uploader->image_ratio = true;
$uploader->image_x = intval(OBRAZEK_GALERIE_MEDIUM_X);
$uploader->image_y = intval(OBRAZEK_GALERIE_MEDIUM_Y);
$uploader->image_convert = 'jpg';
$uploader->process($articles_path_stredni);

$nazev = $uploader->file_dst_name;

$error = $uploader->error;
if($error != '')
{
    $result['messages']['error'] = TNELZE_NAHRAT." - ".$error; 
    echo array2json($result);
    exit; 
}

$uploader->file_new_name_body = $idObrazku;
$uploader->image_resize = true;
$uploader->image_ratio = true;
$uploader->image_x = intval(OBRAZEK_GALERIE_MINI_X);
$uploader->image_y = intval(OBRAZEK_GALERIE_MINI_Y);
$uploader->image_convert = 'jpg';
$uploader->process($articles_path_mini);


$error = $uploader->error;
if($error != '')
{
    $result['messages']['error'] = TNELZE_NAHRAT." - ".$error; 
    echo array2json($result);
    exit; 
}



$uploader->clean();

if($nazev == '')
    $nazev = $file;

$url = ModifyUrl($nazev);
                
//overeni url nazvu stranky, pokud existuje, tak se automaticky priradi novy url nazev
if($domain->data['urlStruktura']=='nazev')
{  
    $where = "";
    if($idObrazku>0)
        $where = "AND idStranky != ".$idObrazku; 
                        
    $verify_url_name = $db->Query("SELECT url FROM ".TABLE_STRANKY." WHERE url LIKE '".$url."%' AND idDomeny=".$domain->getId()." ".$where." ORDER BY url");
                    
    $url_zal = $url;
    $j=1;
    while($u = $db->getAssoc($verify_url_name))
    {
        if($url_zal==$u['url'])
            $url_zal = $url."-".$j;
        $j++;
    }
                        
    $url = $url_zal;
                    
}

$nazev_galerie = $db->get(TABLE_STRANKY,'nazev','idStranky='.$idStranky);
$poradi = $db->get(TABLE_STRANKY,"MAX(priorita) + 1","idRodice=".$idStranky." AND typ='galerie-fotka'");

$upload = array(
    "obrazek" => $file,
    "url" => $url,
    "nazev" => $nazev_souboru,
    "nazevProMenu" => $nazev_souboru,
    "nadpis" => $nazev_souboru,
    "title" => $nazev_galerie." - ". $nazev_souboru,
    "idVytvoril"    => $login_obj->getId(),
    "jmenoVytvoril" => $login_obj->getName(),
    "datumVytvoril" => "now",
    'idAktualizoval'=> $login_obj->getId(),
    'jmenoAktualizoval' => $login_obj->getName(),
    'datumAktualizoval' => "now",
    "priorita" => $poradi    
    );          




                
$db->update(TABLE_STRANKY,$upload,"idStranky = ".$idObrazku." AND idDomeny=".$domain->getId()." LIMIT 1");

$log->add_log('upload','galerie-fotka',$idObrazku,$nazev_galerie);   

$dis_content = !$login_obj->UserPrivilege('content_view'); 
$dis_delete = !$login_obj->UserPrivilege('content_delete');

$html = "";

$html .= "<div class='image_box' id='img-".$idObrazku."'>";
$html .= "<div class='image_box-in' id='ibi".$idObrazku."' title='".$nazev_souboru."'>"; 		
$html .= "<div class='image_edit hide' id='img_ed".$idObrazku."'>";
		
if(POVOLIT_DETAIL_FOTKY)
    $html .= "<a href='".$links->get_url($idObrazku)."' target='_blank'>".icon('link')."</a>";
        
if($dis_content)
    $html .= icon_disabled('content');
    else
    $html .= "<a href='".get_link('gallery','galerie',$idStranky,'edit-image',$idObrazku)."' title='".TEDITACE_FOTKY."' >".icon('content')."</a>";
			
if($dis_delete || !$object_access->has_access($idStranky))
    $html .= icon_disabled('delete');
	else
	$html .= "<a id='delete_foto".$idObrazku."' href='#' class='delete_foto' title='".TSMAZAT."'>".icon('delete')."</a>";
		
		
$html .= "</div>";
		
$html .= "<div class='image'>";
$html .= "<img src='".RELATIVE_PATH.$domain->getDir().USER_DIRNAME_GALLERY_MINI.$file."' alt='' />";
$html .= "</div>";

$html .= "<div class='image_desc'>";
//$html .= "<span class='popisek' id='p1".$idObrazku."' style='display: block'>".($nazev_souboru!=''?"<b>".$nazev_souboru."</b><br/>":"")."</span>";

$html .= "<textarea name='nazev[".$idObrazku."]'>".secureString($nazev_souboru)."</textarea>";
    

$html .= "</div>";

$html .= "</div>";
$html .= "</div>";
    
    
    
    


$result['messages']['ok'] = TBYL_USPESNE_NAHRAN; 
$result["file"]["name"] = $nazev;
$result["file"]["html"] = $html;

echo array2json($result);
exit;


?>