<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!$login_obj->UserPrivilege('content_edit') || !defined("SECURITY_CMS")) exit;

$id = get_int_post('id');

$p = $db->get(TABLE_STRANKY,array("nazev","idRodice AS rodic"),"idDomeny=".$domain->getId()." AND idJazyka=".WEB_LANG_ID." AND idStranky=".$id);

$idKategorie = $p->rodic;
$nazev = $p->nazev;

if($nazev=="" || $idKategorie == 0) exit;
                     
$result = $polozky = array();
            
$data = $db->Query("SELECT idStranky 
                FROM ".TABLE_STRANKY."
                WHERE idRodice = ".$idKategorie." 
                    AND idDomeny=".$domain->getId()." 
                    AND idJazyka=".WEB_LANG_ID." 
                ORDER BY priorita       
                ");
            
$i=0;
while($p = $db->getAssoc($data)){
    $polozky[$i] = $p['idStranky'];
    $result[$i] = $p['idStranky'];
    $i++;
    }

foreach($polozky AS $i => $pol){
    if($id == $pol && isset($polozky[$i + 1]))
        list($result[$i + 1], $result[$i]) = array($polozky[$i], $polozky[$i + 1]);
    $i++;    
    }

foreach($result AS $priorita => $id)
    $db->Query("UPDATE ".TABLE_STRANKY." SET priorita=".$priorita." WHERE idStranky=".$id." AND idRodice = ".$idKategorie."  AND idDomeny=".$domain->getId()." AND idJazyka=".WEB_LANG_ID." LIMIT 1");
                
$log->add_log('edit-settings','produkt',$id,$nazev);

?>