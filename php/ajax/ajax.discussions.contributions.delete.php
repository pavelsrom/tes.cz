<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */
 
 
if(!$login_obj->UserPrivilege('settings_delete') || !defined("SECURITY_CMS")) exit;


$id = get_int_post('id');

if($id <= 0) exit;

$data = $db->query("SELECT s.idStranky 
                FROM ".TABLE_DISKUZE_POLOZKY." AS dp
                LEFT JOIN ".TABLE_STRANKY." AS s ON dp.idStranky=s.idStranky 
                WHERE s.idDomeny=".$domain->getId()."
                    AND dp.idPolozky=".$id."
                GROUP BY s.idStranky 
                LIMIT 1");

if($db->numRows($data) == 0)
    exit;
    
$nazev = $db->get(TABLE_DISKUZE_POLOZKY,'nadpis',"idPolozky=".$id); 
$log->add_log('delete','diskuze-prispevek',$id,$nazev);
    
$db->delete(TABLE_DISKUZE_POLOZKY, "WHERE idPolozky=".$id." LIMIT 1");

exit;


?>