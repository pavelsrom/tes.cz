<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */
 
 
if(!$login_obj->UserPrivilege('content_delete') || !defined("SECURITY_CMS")) exit;


$id = get_int_post('id');

if($id <= 0 || !$object_access->has_access($id)) exit;

$data = $db->query("SELECT nazev,obrazek FROM ".TABLE_STRANKY." WHERE idStranky=".$id." AND idDomeny=".$domain->getId()."  AND idJazyka=".WEB_LANG_ID." AND typ='novinka'  LIMIT 1");

if($db->numRows($data) == 0) exit;

$n = $db->getAssoc($data);
$nazev = $n['nazev'];
$soubor = $n['obrazek'];
  
if($soubor!='' && file_exists(PRE_PATH.$domain->getDir().USER_DIRNAME_NEWS.$soubor))
    @unlink(PRE_PATH.$domain->getDir().USER_DIRNAME_NEWS.$soubor);
  
$log->add_log('delete',"novinka",$id,$nazev);

$db->delete(TABLE_STRANKY, "idStranky=".$id." AND idDomeny=".$domain->getId()." LIMIT 1");
$db->delete(TABLE_STRANKY_TAGY, "idObjektu=".$id." AND typObjektu='stranka'");
$db->delete(TABLE_STRANKY_EDITORI,"idObjektu=".$id." AND typ = 'stranka'");
$db->delete(TABLE_ODKAZY,"typ='stranka' AND idObjektu=".$id);
$db->delete(TABLE_ZALOHY,"typ='stranka' AND idObjektu=".$id);
$db->delete(TABLE_DISKUZE_POLOZKY,"idStranky=".$id);
$db->delete(TABLE_STRANKY_SKUPINY, "idStranky=".$id);

$result = array("messages");
$result['messages']["ok"] = OK_SMAZANO;    

//zjisteni url stranky pro uvodku, po smazani stranky v inline editaci bude uzivatel nasmerovan na uvodni stranku
$id_uvodni_stranky = $db->get(TABLE_STRANKY,"idStranky","idDomeny=".$domain->getId()." AND typ='uvodni' AND idJazyka=".WEB_LANG_ID);
$url = UrlPage("", $id_uvodni_stranky, WEB_LANG);
$result['messages']["redirect_url"] = $url; 
echo array2json($result);

exit;


?>