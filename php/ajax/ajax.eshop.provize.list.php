<?php

/**
 * @author Pavel Srom
 * @copyright 2010
 */

//ajaxovy skript na odhlaseni kontaktu z odberu newsletteru

if(!isset($_GET['module']) || !defined("SECURITY_CMS")) exit;


$stav = intval($_GET['stav']);
$vyraz = trim($db->secureString($_GET['vyraz']));
/*
$datum_od = isset($_GET['datum_od']) ? $db->secureString($_GET['datum_od']) : "";
$datum_do = isset($_GET['datum_do']) ? $db->secureString($_GET['datum_do']) : "";

$datum_od = $datum_od != "" ? GetGADate($datum_od) : "";
$datum_do = $datum_do != "" ? GetGADate($datum_do) : "";
*/
$module = trim($_GET['module']);

//nastaveni limitu
$limit = "";
if(isset($_GET['iDisplayLength'])){
    $dStart = "";
    if(isset($_GET['iDisplayStart']))
        $dStart = abs(intval($_GET['iDisplayStart'])).",";
        
    $limit = " LIMIT ".$dStart.abs(intval($_GET['iDisplayLength'])); 
}


//trideni podle sloupcu
$order = "";
if(isset($_GET['iSortCol_0'])){
    $s = abs(intval($_GET['iSortCol_0']));
    if($s == 0) $order = "z.id";
    elseif($s == 2) $order = "z.celkem";
    elseif($s == 3) $order = "z.vytvoreno";
    else $order = "z.id";
}

if($order != '') 
    $order = "ORDER BY ".$order." ".(isset($_GET['sSortDir_0'])?$_GET['sSortDir_0']:'');


//sestaveni podminky pro vyhledavani
$where = "";
$like = array();

if($vyraz != '')
{
    if(ctype_digit($vyraz))
    {
        $like[] = "z.id LIKE '".$vyraz."%'";
        //$like[] = "o.id_platby LIKE '%".$vyraz."%'";
    }
    else
        $like[] = "jmeno_uzivatele LIKE '%".$vyraz."%'";
}

if($stav > 0)
{
    $where .= "AND z.id_stav = ".$stav;
}

/*
if($datum_od != '')
{
    $where .= "AND o.datum >= '".$datum_od." 00:00:01'";
}   
if($datum_do != '')
{
    $where .= "AND o.datum <= '".$datum_do." 23:59:59'";
}
*/

$where = " WHERE 1=1 ".$where." ".(count($like) > 0 ? "AND (".implode(" OR ",$like).")" : "");

$data_pocet = $db->Query("SELECT COUNT(id) AS celkem FROM ".TABLE_PROVIZE_ZADOSTI." AS z ".$where);
$iTotal_arr = $db->getAssoc($data_pocet);
$iTotal = $iTotal_arr['celkem'];

        
$data = $db->Query("SELECT SQL_CALC_FOUND_ROWS z.*, s.nazev AS stav, COUNT(p.id) AS pocet,
                    DATE_FORMAT(z.datum,'%d.%m.%Y %H:%i') AS datum
				FROM ".TABLE_PROVIZE_ZADOSTI." AS z
                LEFT JOIN ".TABLE_PROVIZE_ZADOSTI_STAVY." AS s ON s.id = z.id_stav
                LEFT JOIN ".TABLE_PROVIZE." AS p ON p.id_zadosti = z.id
                ".$where."
				GROUP BY z.id
				".$order."
                ".$limit."
				");


$iFilteredTotal_data = $db->Query("SELECT FOUND_ROWS()");
$iFilteredTotal_arr = $db->getRow($iFilteredTotal_data);
$iFilteredTotal = intval($iFilteredTotal_arr[0]);

$dis_content = !$login_obj->UserPrivilege('content_view');




$tr = array();
while($c = $db->getAssoc($data))
{

    $ids = $c['id'];
        
    
    $pom = array();
    //$pom[] = json_encode($ids);
    $pom[] = json_encode($c['id']);
    $pom[] = json_encode($c['jmeno_uzivatele']);
    $pom[] = json_encode(price($c['celkem']));
    $pom[] = json_encode($c['datum']);
    $pom[] = json_encode($c['stav']);
    $pom[] = json_encode($c['pocet']);
  
    
    $ikony = "";

    $ikony .= "<a href='".get_link($module,'provize',0,'edit-content',$ids)."'>".icon('content')."</a>";
    
    $pom[] = json_encode($ikony);
        
    $tr[] = "[".implode(',', $pom)."]";
    
}

echo "{";
echo '"sEcho": '.(intval($_GET['sEcho'])).', ';
echo '"iTotalRecords": '.$iTotal.', ';
echo '"iTotalDisplayRecords": '.($iFilteredTotal).', ';
echo '"aaData": ['.implode(',', $tr).'] }';


?>