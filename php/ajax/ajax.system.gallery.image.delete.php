<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!$login_obj->UserPrivilege('content_delete') || !defined("SECURITY_CMS")) exit;

$idPage = get_int_post('page_id');
$id = get_int_post('id');

if($idPage <= 0 || !$object_access->has_access($idPage)) exit;
			
$idDomeny = $db->get(TABLE_STRANKY,"idDomeny","idStranky=".$idPage);



if($idDomeny != $domain->getId())
    exit;
            
$soubor = $db->get(TABLE_STRANKY_FOTKY, "soubor", "id=".$id);	

if($soubor != '')
{
    @unlink(PRE_PATH.$domain->getDir().USER_DIRNAME_STRANKY_MINI.$soubor);
    @unlink(PRE_PATH.$domain->getDir().USER_DIRNAME_STRANKY_MAXI.$soubor);
}
				
$db->delete(TABLE_STRANKY_FOTKY, "idStranky=".$idPage." AND id = ".$id);
			
$log->add_log('delete','stranka-fotka',$idPage,$soubor);

exit;

?>