<?php

/**
 * @author Pavel Srom
 * @copyright 2010
 */

//ajaxovy skript na sestaveni obsahu praveho a leveho panelu


if(!is_post('idProfilu') || !defined("SECURITY_CMS")) exit;
if(!$login_obj->UserPrivilege('settings_edit')) exit;

$idProfilu = get_int_post('idProfilu');
$nazev_profilu = get_post('nazev');
$zobrazit_profil = get_int_post("zobrazit");
$vychozi_profil = get_int_post("vychozi");

if($idProfilu<=0) exit;

$result = array("ok" => "", "error" => "");

if($nazev_profilu=='')
{
    $result["error"] = ERROR_NEPLATNY_NAZEV;
    echo array2json($result);
    exit;
}

$update = array(
    "vychozi" => $vychozi_profil,
    "nazev" => $nazev_profilu,
    "zobrazit" => $zobrazit_profil
    );
$db->update(TABLE_PANELY_SABLONY, $update, "idDomeny=".$domain->getId()." AND idProfilu=".$idProfilu);



$data = $db->query("SELECT systemovyNazev, idTypu  FROM ".TABLE_PANELY_POLOZKY_TYPY);
$polozky_typy = array();
while($t = $db->getAssoc($data))
    $polozky_typy[$t['systemovyNazev']] = $t['idTypu'];

$data = $db->query("SELECT idPanelu, systemovyNazev FROM ".TABLE_PANELY." WHERE idDomeny=".$domain->getId()." AND zobrazit=1");


while($p = $db->getAssoc($data))
{
    
    
    $idPanelu = $p['idPanelu'];
    $panel = $p['systemovyNazev'];   
    
    if(!is_post($panel)) continue;

    //vytvoreni pole zahrnujici idecka a typy novych bloku - aktualnich
    $bloky_nove = array();
    $bloky_stare = array();
    
    $bloky_nove_compare = array();
    $bloky_stare_compare = array();

    $post_item = get_array_post($panel);
            
    foreach($post_item AS $item){
    	$item_data = explode('_',$item);
        if(count($item_data) != 2) continue;
        
        $id_boxu = intval($item_data[1]);
        $typ = $item_data[0];
        $id_typu = $polozky_typy[$typ];
        if($id_boxu == 0 || $id_typu == 0) continue;
        
    	$bloky_nove[] = array('id'=>$id_boxu, 'typ'=> $id_typu); 
    	$bloky_nove_compare[] = $id_boxu."-".$id_typu;
    	}
	
    //vytvoreni pole zahrnujici idecka a typy starych bloku - neaktualnich
    $d = $db->Query("SELECT idTypu, idObjektu FROM ".TABLE_PANELY_POLOZKY." WHERE idPanelu='".$idPanelu."' AND idProfilu=".$idProfilu);
    while($b = $db->getAssoc($d)){
        $id_boxu = intval($b['idObjektu']);
        $id_typu = intval($b['idTypu']);
    	$bloky_stare[] = array('id'=>$id_boxu, 'typ'=>$id_typu);
    	$bloky_stare_compare[] = $id_boxu."-".$id_typu;
    	}
	
    //porovnavani aktualnich dat s neaktualnimi

    //zjisteni polozek, ktere se budou pridavat do db
    $pridat = array();
    foreach($bloky_nove_compare AS $idx => $n){
    	if(!in_array($n, $bloky_stare_compare))
    		$pridat[] = array('id'=>$bloky_nove[$idx]['id'], 'typ'=>$bloky_nove[$idx]['typ']) ; //vezme zaznam z pole aktualnich zaznamu, ktery se bude pridavat
    	}


    //zjisteni polozek, ktere se budou odebirat z db
    $odebrat = array();
    foreach($bloky_stare_compare AS $idx => $s){
    	if(!in_array($s, $bloky_nove_compare))
    		$odebrat[] = $bloky_stare[$idx]; //vezme zaznam z neaktualnich bloku, ktery bude pozdeji smazan
    	}

    //odebrani polozek z db
    foreach($odebrat AS $del){
    	$db->delete(TABLE_PANELY_POLOZKY,"WHERE idTypu='".$del['typ']."' AND idObjektu=".$del['id']." AND idPanelu='".$idPanelu."' AND idProfilu=".$idProfilu);
    	}
	

    $values = "";

    foreach($pridat AS $val){
    	if($values != '') $values .= ",";
    	$values .= "(".$idProfilu.",'".$idPanelu."','".$val['typ']."',".$val['id'].",0)";
    	}

    if($values!='')
	   $db->Query("INSERT INTO ".TABLE_PANELY_POLOZKY." (idProfilu, idPanelu, idTypu, idObjektu, priorita) VALUES ".$values);
	
    //nastaveni priorit u zaznamu
    $i=0;
    foreach($bloky_nove AS $b){
    	$db->update(TABLE_PANELY_POLOZKY,array("priorita" => $i),"idProfilu=".$idProfilu." AND idPanelu='".$idPanelu."' AND idTypu='".$b['typ']."' AND idObjektu=".$b['id']);
    	$i++;
    	}	

}


$nazev = $db->get(TABLE_PANELY_SABLONY,'nazev','idProfilu='.$idProfilu);
$log->add_log('edit-content','sablona-panelu',$idProfilu,$nazev);

$result["ok"] = OK_ULOZENO;
echo array2json($result);

exit();

?>