<?php

/**
 * @author Pavel Srom
 * @copyright 2010
 */

if(!isset($_GET['idForm']) || !defined("SECURITY_CMS")) exit;

$idFormulare = intval($_GET['idForm']);
if($idFormulare==0) exit;
if(!$login_obj->UserPrivilege('content_view')) exit;

$data = $db->Query("SELECT p.* 
    FROM ".TABLE_FORMULARE." AS f
    LEFT JOIN ".TABLE_FORMULARE_POLOZKY." AS p ON f.idFormulare=p.idFormulare
    WHERE f.idDomeny=".$idDomain."
        AND f.idFormulare=".$idFormulare." 
    ORDER BY p.priorita, p.idPolozky
    
    ");

/*
header('Content-type: text/xml');
header('pragma: no-cache');
header('expires: 0');
*/

    
echo "<form>\n";    
while($p = $db->getAssoc($data)){
    $typ = $p['typ'];
    $nazev = $p['nazev'];
    $req = $p['povinna']==1?'true':'false';
    $title="";
    $id = $p['idPolozky'];
    $format=$p['format'];
    
    if($typ=='text'){
        $typ = 'input_text';
        if($format=='icq') $typ = "icq";
        elseif($format=='datum') $typ="datum";
        elseif($format=='cas') $typ="cas";
        elseif($format=='datum') $typ="datum";
        elseif($format=='email') $typ="email";
        elseif($format=='url') $typ="url";
        elseif($format=='telefon') $typ="telefon";
        elseif($format=='float') $typ="float";
        elseif($format=='integer') $typ="int";
        elseif($format=='ico') $typ="ico";
        elseif($format=='dic') $typ="dic";
        }
    if($typ=='selectbox') $typ = 'select';
    if($typ=='description') $typ = 'popis';
    //if($typ=='label') $typ = 'nadpis';
    
    
    if($typ=='radio' || $typ=='checkbox' || $typ=='select'){
        $title = "title='".$nazev."'";
        $nazev = "";
        }
     
    echo "<field type='".$typ."' required='".$req."' ".$title." id='".$id."'>".$nazev;
    
    if($title!=''){
        $pol = explode('#', $p['hodnoty']);
        if(count($pol)>0){
            foreach($pol AS $p){
                $d = explode(':',$p);
                if(count($d)>1){
                    $r = end($d);
                    if($r==1) $r = "true"; else $r = "false";
                    }
                    else{
                    $r = 'false';
                    }
                    
                $np = $d[0];
                
                $ptyp = $typ;
                if($typ == 'select') 
                    echo "<option checked='".$r."'>".$np."</option>\n";
                    else
                    echo "<".$ptyp." checked='".$r."'>".$np."</".$ptyp.">\n";
                }
            
            }
        
        }
    
    
    echo "</field>\n";
  
}

echo "</form>";



?>