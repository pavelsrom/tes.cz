<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */
 
 
if(!$login_obj->UserPrivilege('superadmin','settings_delete') || !defined("SECURITY_CMS")) exit;


$id = get_int_post('id');

if($id <= 0) exit;

$nazev = $db->get(TABLE_PANELY,'nazev','idPanelu='.$id);
if($nazev == "") exit;

$log->add_log('delete',"panel",$id,$nazev);

$db->delete(TABLE_PANELY, "WHERE idPanelu=".$id." AND idDomeny=".$domain->getId());
$db->delete(TABLE_PANELY_POLOZKY, "WHERE idPanelu=".$id);

echo OK_SMAZANO;

exit;


?>