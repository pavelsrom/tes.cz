<?php

/**
 * @author Pavel Srom
 * @copyright 2010
 */

//ajaxovy skript na odhlaseni kontaktu z odberu newsletteru

if(!isset($_GET['module']) || !defined("SECURITY_CMS")) exit;

$vyraz = trim($db->secureString($_GET['vyraz']));
$module = trim($_GET['module']);

//nastaveni limitu
$limit = "";
if(isset($_GET['iDisplayLength'])){
    $dStart = "";
    if(isset($_GET['iDisplayStart']))
        $dStart = abs(intval($_GET['iDisplayStart'])).",";
        
    $limit = " LIMIT ".$dStart.abs(intval($_GET['iDisplayLength'])); 
}


//trideni podle sloupcu
$order = "";
if(isset($_GET['iSortCol_0'])){
    $s = abs(intval($_GET['iSortCol_0']));
    if($s == 0) $order = "d.idDomeny";
    elseif($s == 1) $order = "d.domena";
    elseif($s == 2) $order = "d.indexovat";
    elseif($s == 3) $order = "d.zobrazit";
    else $order = "d.idDomeny";
}

if($order != '') 
    $order = "ORDER BY ".$order." ".(isset($_GET['sSortDir_0'])?$_GET['sSortDir_0']:'');

//sestaveni podminky pro vyhledavani
$like = array();
if($vyraz != '')
{
    $like[] = "d.idDomeny LIKE '".$vyraz."%'";
    $like[] = "d.domena LIKE '%".$vyraz."%'";
}
    

if(count($like) > 0)
    $where = "WHERE ".implode(" OR ",$like);
    else
    $where = "";

$data_pocet = $db->Query("SELECT COUNT(idDomeny) AS celkem FROM ".TABLE_DOMENY." AS d ".$where);
$iTotal_arr = $db->getAssoc($data_pocet);
$iTotal = $iTotal_arr['celkem'];

$data = $db->Query("SELECT SQL_CALC_FOUND_ROWS d.*
			FROM ".TABLE_DOMENY." AS d 
            ".$where." 
			".$order."
            ".$limit."
            ");



$iFilteredTotal_data = $db->Query("SELECT FOUND_ROWS()");
$iFilteredTotal_arr = $db->getRow($iFilteredTotal_data);
$iFilteredTotal = intval($iFilteredTotal_arr[0]);

$dis_delete = !$login_obj->UserPrivilege('settings_delete');
$dis_settings = !$login_obj->UserPrivilege('settings_view');

$tr = array();
while($c = $db->getAssoc($data))
{
    $ids = $c['idDomeny'];
    $pom = array();
    $pom[] = json_encode($ids);
    $pom[] = json_encode(secureString($c['domena']));
    $pom[] = json_encode($c['zobrazit']==0?"<span class='stav0'>".TNE."</span>":"<span class='stav1'>".TANO."</span>");
    
    $ikony = "";
    
    $ikony .= "<a href='".PROTOKOL.$c['domena'].($c['adresar']==''?'':"/".$c['adresar'])."' target='_blank'>".icon('link',TZOBRAZIT_WEB)."</a>";
    
    
    
    if($dis_settings)
        $ikony .=  icon_disabled('setting');
        else
        $ikony .= "<a href='".get_link($module,'domeny',0,'edit-settings',$ids)."'>".icon('setting')."</a>";

    if($dis_delete)
        $ikony .= icon_disabled('delete');
        else
        $ikony .= "<a href='#' id='delete".$ids."' class='delete'>".icon('delete')."</a>";
        
    $pom[] = json_encode($ikony);
        
    $tr[] = "[".implode(',', $pom)."]";
    
}

echo "{";
echo '"sEcho": '.(intval($_GET['sEcho'])).', ';
echo '"iTotalRecords": '.intval($iTotal).', ';
echo '"iTotalDisplayRecords": '.($iFilteredTotal).', ';
echo '"aaData": ['.implode(',', $tr).'] }';


?>