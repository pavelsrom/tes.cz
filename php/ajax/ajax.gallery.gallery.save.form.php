<?php

/**
 * @author Pavel Srom
 * @copyright 2014
 */


if(!defined('SECURITY_CMS')) exit;

include("../classes/class.messages.php");
$systemMessage = new C_SystemMessage();

$result = array(
    "redirect" => "",
    "messages" => array(
        "error" => ""
        )
    );

$idStranky = get_int_post('idStranky');

if((!$login_obj->UserPrivilege('content_edit') && $idStranky > 0) || 
    (!$login_obj->UserPrivilege('content_add') && $idStranky == 0)
    ){
    $result["messages"]["error"] = ERROR_NEDOSTATECNA_PRAVA;
    echo array2json($result);
    exit;
}

$idRodice = get_int_post('idRodice');
				
$autentizace = 0;
$idVlastnika = $login_obj->getId();
$idProfiluPanelu = get_int_post('idProfiluPanelu',-1);
$stav = get_int_post('zobrazit');
$idDomeny = $domain->getId();

$idFormulare = get_int_post('idFormulare');
$idAnkety = get_int_post('idAnkety');
$diskuze = get_int_post('diskuze');

$nazev = get_post('nazev');
$menu_nazev = $nazev;
$typ_stranky = get_post('typ','galerie');
$nadpis = get_post('nadpis');			
$url = get_post('url');
$menu_zobr = 0;
$title = get_post('title');
$description = get_post('description');

$url = $idStranky==0 && $url=='' ? ModifyUrl($nazev) : ModifyUrl($url);
$url_new = get_url_nazev($idStranky, $url);
if($url!=$url_new)
    $systemMessage->add_warning(WARNING_URLNAZEV_MODIFIKOVAN." ".$url_new);
$url = $url_new;


if($url == "")
    $systemMessage->add_error(ERROR_NEPLATNY_URL_NAZEV);

if(!TestLength($nazev, 150, false))
    $systemMessage->add_error(ERROR_NEPLATNY_NAZEV);
                
if(!TestLength($nadpis, 150, false))
    $systemMessage->add_error(ERROR_NEPLATNY_NADPIS);
	
if(!TestLength($title, 100, true))
    $systemMessage->add_error(ERROR_NEPLATNY_TITULEK);
				
if(!TestLength($description, 200, true))
    $systemMessage->add_error(ERROR_NEPLATNY_DESCRIPTION);
				
if(!TestUrlPage($url) || ($url=='' && $idStranky > 0)) 
    $systemMessage->add_error(ERROR_NEPLATNY_URL_NAZEV);	
    
    
if($systemMessage->error_exists())
{
    $result["messages"]["error"] = $systemMessage->get_first_message();
    echo array2json($result);
    exit;    
};


$hloubka = $db->get(TABLE_STRANKY,"(hloubka + 1)","idStranky=".$idRodice." AND idDomeny=".$domain->getId()." AND typ='galerie-slozka'");
$hloubka = intval($hloubka);
           
$update = array(
    "nazev"         => $nazev,
    "nadpis"        => $nadpis,
    "title"         => $title,
    "description"   => $description,
    "url"           => $url,
    "zobrazit"      => $stav,
    "doMenu"        => $menu_zobr,
    "autentizace"   => $autentizace,
    "idProfiluPanelu"=>$idProfiluPanelu,
    "typ"           => $typ_stranky,
    "idRodice"      => $idRodice,
    "hloubka"       => $hloubka,
    "idFormulare"   => $idFormulare,
    "idAnkety"      => $idAnkety,
    "diskuze"       => $diskuze,
    "idDomeny"      => $idDomeny,
    "idJazyka"      => WEB_LANG_ID
    );    
    
if($idStranky == 0)
{

    $update['idVytvoril'] = $login_obj->getId();
    $update['jmenoVytvoril'] = $login_obj->getName();
    $update['datumVytvoril'] = "now";
    $update['idAktualizoval'] = $login_obj->getId();
    $update['jmenoAktualizoval'] = $login_obj->getName();
    $update['datumAktualizoval'] = "now";
    
    $update['obsah'] = "<p>".str_repeat(TLOREM_IPSUM_OBSAH." ", 10)."</p>";
    $update['perex'] = "<p>".str_repeat(TLOREM_IPSUM_PEREX." ", 5)."</p>";
     
    $db->insert(TABLE_STRANKY,$update);
    $idStranky = $db->lastId();
    $object_access->add_allowed_object($idStranky);
}
else
{
    $update['idAktualizoval'] = $login_obj->getId();
    $update['jmenoAktualizoval'] = $login_obj->getName();
    $update['datumAktualizoval'] = "now";
    
    $db->update(TABLE_STRANKY,$update,"idStranky=".$idStranky." AND idDomeny=".$domain->getId());
}


//ulozeni popisku a nazvu fotek
$fotka_nazev = get_array_post('fotka_nazev');
$fotka_popis = get_array_post('fotka_popis');

if(count($fotka_nazev) > 0)
{
    
   $i = 0;
   foreach($fotka_nazev AS $id_fotky => $nazev_fotky)
   {
        $popis = isset($fotka_popis[$id_fotky]) ? $fotka_popis[$id_fotky] : "";
                               
        $url = ModifyUrl($nazev_fotky);
                     
        //overeni url nazvu stranky, pokud existuje, tak se automaticky priradi novy url nazev
        $url = $id_fotky==0 && $url=='' ? ModifyUrl($nazev_fotky) : ModifyUrl($url);
        $url = get_url_nazev($id_fotky, $url);    
        
        $nazev_fotky = $db->secureString($nazev_fotky);
        $popis = $db->secureString($popis);

        $update = array(
            "nazev"         => $nazev_fotky,
            "nazevProMenu"  => $nazev_fotky,
            "obsah"         => $popis,
            "obsahBezHtml"  => strip_tags($popis),
            "url"           => $url,
            "nadpis"        => $nazev_fotky,
            "title"         => $nazev_fotky,
            "zobrazit"      => 1,
            "description"   => strip_tags($popis),
            'idAktualizoval'=>$login_obj->getId(),
            'jmenoAktualizoval'=>$login_obj->getName(),
            'datumAktualizoval'=>"now",
            "priorita"      => $i
            );
        
        $nazev_galerie = $db->get(TABLE_STRANKY,'nazev',"idStranky=".$idStranky);
        $db->update(TABLE_STRANKY,$update,"idRodice=".$idStranky." AND idStranky=".$id_fotky." AND idDomeny=".$domain->getId());
        
        $log->add_log('edit-content','galerie-fotka',$id_fotky,$nazev_galerie);
        $i++;
   } 
}



$log->add_log(get_int_post('idStranky') == 0 ? 'create':'edit-settings',$typ_stranky,$idStranky,$nazev);    

$redirect_url = UrlPage($url,$idStranky,WEB_LANG);
    
$result["redirect"] = $redirect_url;
$result["messages"]["ok"] = OK_ULOZENO_A_PRESMEROVANO;//OK_ULOZENO;


echo array2json($result);
exit; 
    
    
    
    
    
?>