<?php

/**
 * @author Pavel Srom
 * @copyright 2015
 */


$term = get_request("term");

include("../classes/class.finder.php");
$finder = new C_Finder($term);
$finder->limit = 5;
$finder->fulltext = false;
$finder->Find(false);

$result = array();
foreach($finder->result AS $id => $r)
{
    $obsah = $finder->CropAndSaveWords($r['obsah'], $term, 70);
    //$obsah = $finder->MarkWord($obsah, $term);      
    $result[] = array("id" => $id, "label" => $r['nadpis'], "value" => "", "url" => $r["url"], "desc" => $obsah);
}

echo json_encode($result);




?>
