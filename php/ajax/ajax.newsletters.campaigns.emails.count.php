<?php

/**
 * @author Pavel Srom
 * @copyright 2011
 */

if(!defined("SECURITY_CMS")) exit;

$skupiny = isset($_POST['skupiny'])?$_POST['skupiny']:array();
$skupiny = (array)($skupiny);
$result = array();

if(count($skupiny)==0){
    $result['unikatni'] = 0;
    $result['celkem'] = 0;
    echo json_encode($result); 
    exit;
}

$data = $db->Query("SELECT COUNT(p.idPrijemce) AS pocet, p.email 
			FROM ".TABLE_EMAILY_SKUPINY." AS s 
			LEFT JOIN ".TABLE_EMAILY_SKUPINY_PRIJEMCI." AS sp ON sp.idSkupiny = s.idSkupiny 
			LEFT JOIN ".TABLE_EMAILY_PRIJEMCI." AS p ON sp.idPrijemce = p.idPrijemce 
			WHERE s.idDomeny=".$domain->getId()."
				AND p.stav=1
                AND s.idSkupiny IN (".implode(',', $skupiny).")
				GROUP BY p.email 
				ORDER BY s.idSkupiny");
                
$pom = array();
$celkem = 0;

while($s = $db->getAssoc($data)){
    $pom[$s['email']] = "";
    $celkem += $s['pocet'];
    }

$result['celkem'] = $celkem;
$result['unikatni'] = count($pom);

echo json_encode($result);





?>