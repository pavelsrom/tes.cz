<?php

/**
 * @author Pavel Srom
 * @copyright 2010
 */

//ajaxovy skript na odhlaseni kontaktu z odberu newsletteru

if(!isset($_GET['module']) || !defined("SECURITY_CMS")) exit;

$vyraz = trim($db->secureString($_GET['vyraz']));
$module = trim($_GET['module']);

//nastaveni limitu
$limit = "";
if(isset($_GET['iDisplayLength'])){
    $dStart = "";
    if(isset($_GET['iDisplayStart']))
        $dStart = abs(intval($_GET['iDisplayStart'])).",";
        
    $limit = " LIMIT ".$dStart.abs(intval($_GET['iDisplayLength'])); 
}


//trideni podle sloupcu
$order = "";
if(isset($_GET['iSortCol_0'])){
    $s = abs(intval($_GET['iSortCol_0']));
    if($s == 0) $order = "idSablony";
    elseif($s == 1) $order = "nazev";
    else $order = "idSablony";
}

if($order != '') 
    $order = "ORDER BY ".$order." ".(isset($_GET['sSortDir_0'])?$_GET['sSortDir_0']:'');

//echo $order;

//sestaveni podminky pro vyhledavani
$like = array();
if($vyraz != '')
{
    $like[] = "idSablony LIKE '".$vyraz."%'";
    $like[] = "nazev LIKE '%".$vyraz."%'";
}
    


$where = "WHERE ((idDomeny=".$domain->getId()." AND idJazyka=".WEB_LANG_ID.") OR idDomeny=0)".((count($like) > 0) ? " AND (".implode(" OR ",$like).")" : "");

$data_pocet = $db->Query("SELECT COUNT(idSablony) AS celkem FROM ".TABLE_HTML_SABLONY." ".$where);
$iTotal_arr = $db->getAssoc($data_pocet);
$iTotal = $iTotal_arr['celkem'];


        
$data = $db->Query("SELECT SQL_CALC_FOUND_ROWS * FROM ".TABLE_HTML_SABLONY." AS s
				".$where."
				GROUP BY idSablony
				".$order."
                ".$limit."
				");


$iFilteredTotal_data = $db->Query("SELECT FOUND_ROWS()");
$iFilteredTotal_arr = $db->getRow($iFilteredTotal_data);
$iFilteredTotal = intval($iFilteredTotal_arr[0]);


$dis_content = !$login_obj->UserPrivilege('content_view');
$dis_delete = !$login_obj->UserPrivilege('content_delete');

$tr = array();
while($c = $db->getAssoc($data))
{
    $ids = $c['idSablony'];
    $systemova = $c['systemova'] == 1;
    $systemova_napric_domenami = $c['idDomeny'] == 0;
    $pom = array();
    $pom[] = json_encode($ids);
    $pom[] = json_encode(secureString($c['nazev']));
    $pom[] = json_encode(secureString($c['popis']));
    
    $ikony = "";
    
    if($dis_content || ($systemova && !$login_obj->UserPrivilege('admin') && !$login_obj->UserPrivilege('superadmin')) || ($systemova_napric_domenami && !$login_obj->UserPrivilege('superadmin')))
        $ikony .= icon_disabled('content');
        else
        $ikony .= "<a href='".get_link($module,'sablony',0,'edit-content',$ids)."'>".icon('content')."</a>";
    
    if($dis_delete || ($systemova && !$login_obj->UserPrivilege('admin') && !$login_obj->UserPrivilege('superadmin')) || ($systemova_napric_domenami && !$login_obj->UserPrivilege('superadmin')))
        $ikony .= icon_disabled('delete');
        else
        $ikony .= "<a href='#' class='delete' id='delete".$ids."'>".icon('delete')."</a>";
        
    $pom[] = json_encode($ikony);
        
    $tr[] = "[".implode(',', $pom)."]";
    
}

echo "{";
echo '"sEcho": '.(intval($_GET['sEcho'])).', ';
echo '"iTotalRecords": '.$iTotal.', ';
echo '"iTotalDisplayRecords": '.($iFilteredTotal).', ';
echo '"aaData": ['.implode(',', $tr).'] }';


?>