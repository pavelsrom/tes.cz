<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined("SECURITY_CMS")) exit;

if(!$login_obj->UserPrivilege('superadmin') || !$login_obj->UserPrivilege('settings_edit')) exit;


$idRelace = get_int_post('id');
$idDomeny = get_int_post('domain_id');

if($idRelace <= 0 || $idDomeny <= 0) exit;

$db->delete(TABLE_UZIVATELE_DOMENY,"WHERE idRelace=".$idRelace);

$domena = $db->get(TABLE_DOMENY,'domena','idDomeny='.$idDomeny);
$log->add_log('edit-settings','domeny',$idDomeny,$domena);



exit;

?>