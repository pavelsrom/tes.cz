<?php

/**
 * @author Pavel Srom
 * @copyright 2011
 */


include_once(PRE_PATH.'php/classes/class.gapi.php');


 

if(!isset($_REQUEST['od']) || !isset($_REQUEST['do']) || !defined("SECURITY_CMS")) exit;


$razeni = isset($_REQUEST['razeni']) ? $_REQUEST['razeni'] : "visits";
$smer = isset($_REQUEST['smer']) && $_REQUEST['smer'] == 'asc' ? '' : '-';
if(!in_array($razeni, array('visits','avgTimeOnSite','percentNewVisits','entranceBounceRate', 'pageviewsPerVisit'))) exit;
$ga_razeni = $smer.$razeni;

$start = (isset($_REQUEST['od'])?$_REQUEST['od']:'');
$end = (isset($_REQUEST['do'])?$_REQUEST['do']:'');

$start = GetGADate($start);
$end = GetGADate($end);

if($start=='' || strlen($start)!=10)
	if($end!=''){
		$start = $s = explode('-', $end);
		$start = $s[0].".".$s[1]."-01";
		}
		else 
		$start=strftime("%Y-%m-01", time());
        
if($end=='' || strlen($end)!=10) $end=strftime("%Y-%m-%d", time());



if(!$login_obj->UserPrivilege('statistics_view')) exit;

$error = "";

try{
	$ga = new gapi(GA_EMAIL,GA_HESLO);
	}
	catch(exception $e){
	exit;	
	}
	
$idProfile = GA_PROFIL;
$maxRecords = 15;

$filter = "medium==referral";

//zjisteni klicovych slov a odkazujicich stranek
$ga->requestReportData($idProfile,array('source'),array('visits','avgTimeOnSite','percentNewVisits','entranceBounceRate', 'pageviewsPerVisit'),array($ga_razeni),$filter,null,$start,$end,1,$maxRecords);
$res = $ga->getResults();

$tabulka_odkazujici_stranky = array();

$i=0;
foreach($res AS $r){   
    $tabulka_odkazujici_stranky[$i]['odkazujici_stranka'] = $r->dimensions['source'];
    $tabulka_odkazujici_stranky[$i]['navstevy'] = $r->metrics['visits'];
    $tabulka_odkazujici_stranky[$i]['stranky_navsteva'] = round($r->metrics['pageviewsPerVisit'],2);
        
    if($r->metrics['visits']>0)
        $tabulka_odkazujici_stranky[$i]['doba_na_webu'] = date('H:i:s', mktime(0,0,round($r->metrics['avgTimeOnSite'],0),0,0,0));
        else
        $tabulka_odkazujici_stranky[$i]['doba_na_webu'] = date('H:i:s', mktime(0,0,0,0,0,0));
            
    $tabulka_odkazujici_stranky[$i]['nove_navstevy'] = round($r->metrics['percentNewVisits'],2);
    $tabulka_odkazujici_stranky[$i]['mira_opusteni'] = round($r->metrics['entranceBounceRate'],2);
    $i++;

        
    
    }
 
//vykresleni tabulky

$r = $razeni;
$s = $smer=='-'?'_asc':'_desc';
$def_s = '_desc';
echo "<table class='table w80'>";
echo "<tr>
    <th>".TODKAZUJICI_WEB."</th>
    <th><a href='#' class='visits".($r=='visits'?$s:$def_s)."'>".TNAVSTEVY."</a></th>
    <th><a href='#' class='pageviewsPerVisit".($r=='pageviewsPerVisit'?$s:$def_s)."'>".TSTRANKA_NAVSTEVA."</a></th>
    <th><a href='#' class='avgTimeOnSite".($r=='avgTimeOnSite'?$s:$def_s)."'>".TPRUMERNA_DOBA_NA_STRANCE."</a></th>
    <th><a href='#' class='percentNewVisits".($r=='percentNewVisits'?$s:$def_s)."'>".TNOVE_NAVSTEVY."</a></th>
    <th><a href='#' class='entranceBounceRate".($r=='entranceBounceRate'?$s:$def_s)."'>".TMIRA_OPUSTENI."</a></th>
    </tr>";
    
if(count($tabulka_odkazujici_stranky)>0)
{
    foreach($tabulka_odkazujici_stranky AS $p)
    {
        echo "<tr>";
        echo "<td>".$p['odkazujici_stranka']."</td>";
            
        if($r=='visits')    
            echo "<td class='w15'><b>".$p['navstevy']."</b></td>";
            else
            echo "<td class='w15'>".$p['navstevy']."</td>";
        
        if($r=='pageviewsPerVisit')    
            echo "<td class='w15'><b>".$p['stranky_navsteva']."</b></td>";
            else
            echo "<td class='w15'>".$p['stranky_navsteva']."</td>";
        
        if($r=='avgTimeOnSite')    
            echo "<td class='w15'><b>".$p['doba_na_webu']."</b></td>";
            else
            echo "<td class='w15'>".$p['doba_na_webu']."</td>";
        
        if($r=='percentNewVisits')    
            echo "<td class='w15'><b>".$p['nove_navstevy']."%</b></td>";
            else
            echo "<td class='w15'>".$p['nove_navstevy']."%</td>";
        
        if($r=='entranceBounceRate')    
            echo "<td class='w15'><b>".$p['mira_opusteni']."%</b></td>";
            else
            echo "<td class='w15'>".$p['mira_opusteni']."%</td>";
        echo "</tr>";
        
    }
    
}
else
echo "<tr><td colspan='5'>".TNENALEZEN_ZADNY_ZAZNAM."</td></tr>";

echo "</table>";


?>