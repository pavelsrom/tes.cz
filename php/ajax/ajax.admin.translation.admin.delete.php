<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */


if(!defined("SECURITY_CMS")) exit;
if(!$login_obj->UserPrivilege('settings_delete','superadmin')) exit;

$id = get_int_post('id');

if($id <= 0) exit;

$nazev = $db->get(TABLE_PREKLADY_ADMINISTRACE,'konstanta',"idPrekladu=".$id); 
$log->add_log('delete','preklady-admin',$id,$nazev);

$data = $db->delete(TABLE_PREKLADY_ADMINISTRACE, "WHERE idPrekladu=".$id." LIMIT 1");



exit;

?>