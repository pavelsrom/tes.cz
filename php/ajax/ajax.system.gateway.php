<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(isset($_POST['session_id']))
    session_id($_POST['session_id']);

session_start();
 
DEFINE('PRE_PATH','../../');
DEFINE('SECURITY_CMS','');
DEFINE('ADMIN_SCRIPT','carmeo'); //cms.php

include_once(PRE_PATH.'php/config/config.db.php');
include_once(PRE_PATH.'php/config/config.paths.php');
include_once(PRE_PATH.'php/config/config.admin.php');
include_once(PRE_PATH.'php/config/config.others.php');
include_once(PRE_PATH.CMS_THEME_PATH.'config.php');

include_once(PRE_PATH.'php/functions/functions.system.php');
include_once(PRE_PATH.'php/functions/functions.show.php');
include_once(PRE_PATH.'php/functions/functions.admin.php');
include_once(PRE_PATH.'php/functions/functions.input.formats.php');
include_once(PRE_PATH.'php/functions/functions.error.handlers.php');

include_once(PRE_PATH.'php/classes/class.domain.admin.php');
include_once(PRE_PATH.'php/classes/class.langs.admin.php');
include_once(PRE_PATH.'php/classes/class.table.php');
include_once(PRE_PATH.'php/classes/class.db.php');
include_once(PRE_PATH.'php/classes/class.login.php');
include_once(PRE_PATH.'php/classes/class.url.web.php');
include_once(PRE_PATH.'php/classes/class.modules.php');
include_once(PRE_PATH.'php/classes/class.log.php');
include_once(PRE_PATH.'php/classes/class.object.access.php');
include_once(PRE_PATH.'php/classes/class.private.pages.admin.php');

$db = new C_Db();	
$db->Connect();

loadTranslations(CMS_LANG);

error_settings();

//overeni existence session pro cms - existence domeny
$domain = new C_Domains();
if($domain->getId()<=0) exit;
$idDomain = $domain->getId();
$idModulu = isset($_REQUEST['module_id']) ? intval($_REQUEST['module_id']) : 0;
loadSettings($domain->getId());

$login_obj = new C_User();
$idUser = intval($login_obj->getId());

if($idUser==0) exit;

$object_access = new C_ObjectAccess();

GetActiveModules();
$lang = new C_WebLangs($login_obj->getLanguages(), $domain->getId(), WEB_LANG_ID);
$log = new LogItem($idModulu,$domain->getId(), WEB_LANG_ID, $login_obj->getId(), $login_obj->getName());
define("WEB_LANG",strtolower($lang->get_active_lang('jazyk')));
define("LANG",WEB_LANG);

define('RELATIVE_URL_USER_DOMAIN', RELATIVE_PATH.$domain->getDir());
define('ABSOLUTE_URL_USER_DOMAIN', $domain->getUrl().$domain->getDir());

$links = new C_Url();

if(!$lang->is_allowed())
{
    echo TBYL_JSTE_AUTOMATICKY_ODHLASEN_Z_DUVODU_PRISTUPU_K_NEPOVOLENE_JAZYKOVE_MUTACI;
    exit;
}

$action = isset($_REQUEST['action']) ? $_REQUEST['action'] : "";
$module_name = isset($_GET['module']) ? $_GET['module'] : "";

if($action == "") exit;

if($module_name!='wet' && file_exists("ajax.".$module_name.".".$action.".php"))
    include("ajax.".$module_name.".".$action.".php");
elseif($module_name=='wet' && file_exists("ajax.wet.".$action.".php"))
    include("ajax.wet.".$action.".php");
elseif(file_exists("ajax.system.".$action.".php"))
    include("ajax.system.".$action.".php");
else
    echo "skript pro akci '".$action."' a modul '".$module_name."' nenalezen!";
    
    
//include("php/modules/module.".$module_name."/ajax.admin.".$action.".php");

exit;


?>