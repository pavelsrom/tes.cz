<?php

/**
 * @author Pavel Srom
 * @copyright 2010
 */

//ajaxovy skript na odhlaseni kontaktu z odberu newsletteru

if(!defined("SECURITY_CMS")) exit;

$idObjektu = intval($_GET['object_id']);
$typ = trim($_GET['object_type']);
$vyraz = isset($_GET['vyraz']) ? trim($db->secureString($_GET['vyraz'])) : "";

//nastaveni limitu
$limit = "";
if(isset($_GET['iDisplayLength'])){
    $dStart = "";
    if(isset($_GET['iDisplayStart']))
        $dStart = abs(intval($_GET['iDisplayStart'])).",";
        
    $limit = " LIMIT ".$dStart.abs(intval($_GET['iDisplayLength'])); 
}


//trideni podle sloupcu
$order = "ORDER BY z.idZalohy DESC";
/*
if(isset($_GET['iSortCol_0'])){
    $s = abs(intval($_GET['iSortCol_0']));
    if($s == 0) $order = "s.idStranky";
    elseif($s == 1) $order = "s.nazev";
    elseif($s == 2) $order = "s.zobrazit";
    else $order = "s.idStranky";
}

if($order != '') 
    $order = "ORDER BY ".$order." ".(isset($_GET['sSortDir_0'])?$_GET['sSortDir_0']:'');
*/

//echo $order;

//sestaveni podminky pro vyhledavani
$like = array();
if($vyraz != '')
{
    $like[] = "z.perexBezHtml LIKE '%".$vyraz."%'";
    $like[] = "z.obsahBezHtml LIKE '%".$vyraz."%'";
}
    


$where = "WHERE idObjektu=".$idObjektu." AND z.typ='".$typ."' AND z.idDomeny=".$domain->getId().((count($like) > 0) ? " AND (".implode(" OR ",$like).")" : "");

$data_pocet = $db->Query("SELECT COUNT(idZalohy) AS celkem FROM ".TABLE_ZALOHY." AS z ".$where);
$iTotal_arr = $db->getAssoc($data_pocet);
$iTotal = $iTotal_arr['celkem'];


$data = $db->query("SELECT SQL_CALC_FOUND_ROWS z.idZalohy AS id, z.typ, 
        DATE_FORMAT(z.datum,'%d.%m.%Y %H:%i') AS datum, 
        TRIM(CONCAT(u.jmeno,' ',u.prijmeni)) AS jmeno 
        FROM ".TABLE_ZALOHY." AS z 
        LEFT JOIN ".TABLE_UZIVATELE." AS u ON z.idUzivatele=u.idUzivatele
        ".$where."  
        GROUP BY z.idZalohy  
        ".$order."
        ".$limit."
        ");

$iFilteredTotal_data = $db->Query("SELECT FOUND_ROWS()");
$iFilteredTotal_arr = $db->getRow($iFilteredTotal_data);
$iFilteredTotal = intval($iFilteredTotal_arr[0]);

$tr = array();
while($z = $db->getObject($data))
{
    $odkazy = "<a href='#' class='nahled' id='n".$z->id."' title='".TNAHLED."'><span class=' fa fa-eye'></span></a>";
    
    $pom = array();
    $pom[] = json_encode($z->datum);
    $pom[] = json_encode(secureString($z->jmeno));    
    $pom[] = json_encode($odkazy);
        
    $tr[] = "[".implode(',', $pom)."]";
    
}

echo "{";
echo '"sEcho": '.(intval($_GET['sEcho'])).', ';
echo '"iTotalRecords": '.$iTotal.', ';
echo '"iTotalDisplayRecords": '.($iFilteredTotal).', ';
echo '"aaData": ['.implode(',', $tr).'] }';


?>