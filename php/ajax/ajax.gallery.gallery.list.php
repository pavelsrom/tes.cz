<?php

/**
 * @author Pavel Srom
 * @copyright 2010
 */

//ajaxovy skript na odhlaseni kontaktu z odberu newsletteru

if(!isset($_GET['module']) || !defined("SECURITY_CMS")) exit;

$vyraz = trim($db->secureString($_GET['vyraz']));
$module = trim($_GET['module']);


//nastaveni limitu
$limit = "";
if(isset($_GET['iDisplayLength'])){
    $dStart = "";
    if(isset($_GET['iDisplayStart']))
        $dStart = abs(intval($_GET['iDisplayStart'])).",";
        
    $limit = " LIMIT ".$dStart.abs(intval($_GET['iDisplayLength'])); 
}


//trideni podle sloupcu
$order = "";
if(isset($_GET['iSortCol_0'])){
    $s = abs(intval($_GET['iSortCol_0']));
    if($s == 0) $order = "s.idStranky";
    else $order = "s.idStranky";
}

if($order != '') 
    $order = "ORDER BY ".$order." ".(isset($_GET['sSortDir_0'])?$_GET['sSortDir_0']:'');

//sestaveni podminky pro vyhledavani
$like = array();
if($vyraz != '')
{
    $like[] = "g.idStranky LIKE '".$vyraz."%'";
    $like[] = "g.nazev LIKE '%".$vyraz."%'";
}
    
$where = "WHERE g.idJazyka=".WEB_LANG_ID." AND g.idDomeny=".$domain->getId()." AND (g.typ = 'galerie' OR g.typ = 'galerie-slozka')";
if(count($like) > 0)
    $where .= " AND (".implode(" OR ",$like).")";
    
$data_pocet = $db->Query("SELECT COUNT(idStranky) AS celkem FROM ".TABLE_STRANKY." AS g ".$where);
$iTotal_arr = $db->getAssoc($data_pocet);
$iTotal = $iTotal_arr['celkem'];

$data = $db->Query("SELECT SQL_CALC_FOUND_ROWS g.*, COUNT(gp.idStranky) AS pocet_fotografii, j.jazyk
			FROM ".TABLE_STRANKY." AS g 
            LEFT JOIN ".TABLE_JAZYKY." AS j ON g.idJazyka = j.idJazyka
			LEFT JOIN ".TABLE_STRANKY." AS gp ON g.idStranky=gp.idRodice
			".$where." 
			GROUP BY g.idStranky
			ORDER BY g.priorita
			");


$iFilteredTotal_data = $db->Query("SELECT FOUND_ROWS()");
$iFilteredTotal_arr = $db->getRow($iFilteredTotal_data);
$iFilteredTotal = intval($iFilteredTotal_arr[0]);




function Tree($idRodice,$mSystemName,$id_stranek_ke_zobrazeni, $prvni_pruchod=false, $iterace=0)
{
    global $db;
    global $domain;
    global $login_obj;
    global $links;
    global $object_access;
    global $tr;
           
    $idRodice = intval($idRodice);
            
    $dis_content = !$login_obj->UserPrivilege('content_view');
    $dis_settings = !$login_obj->UserPrivilege('content_view');
    $dis_delete = !$login_obj->UserPrivilege('content_delete');
                
    $where = "WHERE s.typ IN ('galerie', 'galerie-slozka') AND s.idDomeny=".$domain->getId();
	
	$record = $db->Query("SELECT s.*, COUNT(gp.idStranky) AS pocet_fotografii, j.jazyk
	   FROM ".TABLE_STRANKY." AS s
       LEFT JOIN ".TABLE_JAZYKY." AS j ON s.idJazyka = j.idJazyka
       LEFT JOIN ".TABLE_STRANKY." AS gp ON s.idStranky = gp.idRodice AND gp.typ = 'galerie-fotka'
	   ".$where." AND s.idRodice=".$idRodice." 
	   GROUP BY s.idStranky
	   ORDER BY s.priorita
	   ");

                
            
    $pocet = $db->numRows($record);
    if($pocet==0) return '';
    $i = 1;
            
    $iterace++;
    while($page = $db->getAssoc($record)){
        $id = $page['idStranky'];
        $pom = array();
            
        if(!in_array($id,$id_stranek_ke_zobrazeni))
        {
            Tree($id,$mSystemName,$id_stranek_ke_zobrazeni,false,$iterace);
            continue;
        }
            
        $private_pages = new C_PrivatePages($id);
        $privatni = ano_ne($private_pages->is_private(),implode(", ", $private_pages->get_groups())); 

            
        $nazev = $page['nazev'];
        $zobrazit = $page['zobrazit'];
        $hloubka = $page['hloubka'];
        $typ = $page['typ'];
        
            
        if($iterace > 1)
            $res = GetGraphWidth($hloubka, 10, $hloubka-1);
            else
            $res = GetGraphWidth($hloubka, 0);
    
        if($prvni_pruchod) 
            $res['mezera'] = "";
                    
        
        $mezera = $res['mezera'];
    
        $ikony_nazvy = $mezera;
        
        /*            
        if(!$prvni_pruchod)
            $ikony_nazvy .= icon('tree', "","icon_tree");
        */
        
                        
        $ikony_nazvy .=  $page['typ']=='galerie-slozka'?icon('gallery-folder','','icon_gallery'):icon('gallery','','icon_gallery');
        $ikony_nazvy .=  secureString($nazev);
                 
          
          
        if($page['typ']=='galerie-slozka')
            $pocet_fotek = "";
            else
            $pocet_fotek = intval($page['pocet_fotografii']);
        
        $pom["DT_RowClass"] = "uroven".$hloubka;
        $pom[] = $id;
        $pom[] = $ikony_nazvy;
        $pom[] = $res['graf'];            
        $pom[] = $pocet_fotek;
        $pom[] = $privatni;
        $pom[] = ano_ne($page['zobrazit']);
        
        $ikony = ""; 
        
                            
        if($i==1 || !$login_obj->UserPrivilege('settings_edit'))
            $ikony .= icon_disabled('up');
            else
            $ikony .= "<a href='#' id='up".$id."' class='up'>".icon('up')."</a>";
        				
        if($i == $pocet || !$login_obj->UserPrivilege('settings_edit'))
            $ikony .= icon_disabled('down');
            else
            $ikony .= "<a href='#' id='down".$id."' class='down'>".icon('down')."</a>";
        
        /*            
        $pom[] = $ikony;
    
        $ikony = ""; 
        */       
        
        $ikony .= "<a href='".UrlPage($page['url'],$id,$page['jazyk'])."' target='_blank'>".icon('link')."</a>";
                
        			  	
    	if($dis_content)
    	   $ikony .=  icon_disabled('content');
    	   else	
    	   $ikony .=  "<a href='".get_link($mSystemName,'galerie',0,'edit-settings',$id)."'>".icon('content')."</a>";
           
        if($dis_content || $typ != 'galerie')
                $ikony .=  icon_disabled('gallery-photos');
    	       else	
                $ikony .= "<a href='".get_link($mSystemName,'galerie',0,'edit-content',$id)."'>".icon('gallery-photos')."</a>";    
        			
    	if($dis_delete || $links->HaveChildren($id, array('galerie', 'galerie-slozka')) || !$object_access->has_access($id))
    	   $ikony .=  icon_disabled('delete');
    	   else
    	   $ikony .=  "<a href='#' id='delete".$id."' class='delete'>".icon('delete')."</a>";

        $pom[] = $ikony;
    
    
        $tr[] = array2json($pom);
        $i++;
        Tree($id,$mSystemName,$id_stranek_ke_zobrazeni,false, $iterace);
    
    }
    
}

$tr = array();

if($vyraz == "")
{
    $id_stranek_ke_zobrazeni = array();
    while($p = $db->getAssoc($data))
        $id_stranek_ke_zobrazeni[] = $p['idStranky'];
    
    $d = $db->Query("SELECT idStranky FROM ".TABLE_STRANKY." WHERE typ='stranka-galerie' AND idDomeny=".$domain->getId()." LIMIT 1");
    $idRoot = 0;
    $root = $db->getAssoc($d);
    $idRoot = $root['idStranky'];
    Tree($idRoot,$module,$id_stranek_ke_zobrazeni,true);
}
else
{
    
    $dis_content = !$login_obj->UserPrivilege('content_view');
    $dis_settings = !$login_obj->UserPrivilege('settings_view');
    $dis_delete = !$login_obj->UserPrivilege('settings_delete');
    
    while($c = $db->getAssoc($data))
    {
        $ids = $c['idStranky'];
        if($c['typ']=='galerie-slozka')
            $pocet_fotek = "";
            else
            $pocet_fotek = intval($c['pocet_fotografii']);
        
        $private_pages = new C_PrivatePages($ids);
        $privatni = ano_ne($private_pages->is_private(),implode(", ", $private_pages->get_groups())); 

        
        $pom = array();
        $pom[] = json_encode($ids);
        $pom[] = json_encode(($c['typ']=='galerie-slozka'?icon('gallery-folder','','icon_gallery'):icon('gallery','','icon_gallery'))." ".secureString($c['nazev']));
        $pom[] = json_encode("---");
        $pom[] = json_encode($pocet_fotek);
        $pom[] = json_encode($privatni);
        $pom[] = json_encode(ano_ne($c['zobrazit']));
        
            
        $ikony = "";
        $ikony .= icon_disabled('up');
        $ikony .= icon_disabled('down');
        /*
        $pom[] = json_encode($ikony);
            
        $ikony = "";
        */
        $ikony .= "<a href='".UrlPage($c['url'],$ids,$c['jazyk'])."' target='_blank'>".icon('link')."</a>";
        
        if($dis_content)
            $ikony .=  icon_disabled('content');
            else
            $ikony .= "<a href='".get_link($module,'galerie',0,'edit-settings',$ids)."'>".icon('content')."</a>";
    
        if($dis_delete || !$object_access->has_access($ids))
            $ikony .= icon_disabled('delete');
            else
            $ikony .=  "<a href='#' id='delete".$ids."' class='delete'>".icon('delete')."</a>";
            
        $pom[] = json_encode($ikony);
            
        $tr[] = "[".implode(',', $pom)."]";
        
    }
}

echo "{";
echo '"sEcho": '.(intval($_GET['sEcho'])).', ';
echo '"iTotalRecords": '.intval($iTotal).', ';
echo '"iTotalDisplayRecords": '.($iFilteredTotal).', ';
echo '"aaData": ['.implode(',', $tr).'] }';


?>