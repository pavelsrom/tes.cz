<?php

/**
 * @author Pavel Srom
 * @copyright 2010
 */


if(!defined("SECURITY_CMS")) exit;
if(!$login_obj->UserPrivilege('settings_edit','translations')) exit;

$id = get_int_post('id');

if($id <= 0) exit;

$update = array();

$jazyky = $lang->get_langs();
foreach($jazyky AS $j)
    if(is_post($j['jazyk']))
        $update[$j['jazyk']] = get_post($j['jazyk']);

if(count($update) > 0)
    $data = $db->update(TABLE_PREKLADY, $update, "idPrekladu=".$id." LIMIT 1");

$nazev = $db->get(TABLE_PREKLADY,'konstanta','idPrekladu='.$id);  
$log->add_log('edit-settings',"preklady-web",$id,$nazev);

exit;

?>