<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */
 
 
if(!$login_obj->UserPrivilege('settings_delete') || !defined("SECURITY_CMS")) exit;


$id = get_int_post('id');

if($id <= 0 || !$object_access->has_access($id,"editbox")) exit;

if($db->get(TABLE_EDITBOXY, "idEditboxu","idEditboxu=".$id." AND idDomeny=".$domain->getId()) == "") 
    exit;

$typ = $db->get(TABLE_EDITBOXY,'stridani','idEditboxu='.$id);
$nazev = $db->get(TABLE_EDITBOXY,'nazev','idEditboxu='.$id);  
$log->add_log('delete',$typ=='zadne' ? 'editbox' : 'editbox-kolotoc',$id,$nazev);

$db->delete(TABLE_EDITBOXY, "WHERE idEditboxu=".$id." LIMIT 1");
$db->delete(TABLE_STRANKY_EDITORI,"WHERE idObjektu=".$id." AND typ = 'editbox'");
$db->delete(TABLE_EDITBOXY_POLOZKY, "WHERE idEditboxu=".$id);
$db->delete(TABLE_PANELY_POLOZKY, "WHERE idTypu = 5 AND idObjektu=".$id);



exit;


?>