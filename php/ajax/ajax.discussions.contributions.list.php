<?php

/**
 * @author Pavel Srom
 * @copyright 2010
 */

//ajaxovy skript na odhlaseni kontaktu z odberu newsletteru

if(!isset($_GET['module']) || !defined("SECURITY_CMS")) exit;

$vyraz = trim($db->secureString($_GET['vyraz']));
$module = trim($_GET['module']);
$idDiskuze = isset($_GET['idDiskuze']) ? intval($_GET['idDiskuze']) : 0;


$idSmiles = SMAJLICI;
if($idSmiles > 0){
	include_once(PRE_PATH.'php/classes/class.smiles.php');
	$smiles = new C_Smiles($idSmiles);
	}
            
//nastaveni limitu
$limit = "";
if(isset($_GET['iDisplayLength'])){
    $dStart = "";
    if(isset($_GET['iDisplayStart']))
        $dStart = abs(intval($_GET['iDisplayStart'])).",";
        
    $limit = " LIMIT ".$dStart.abs(intval($_GET['iDisplayLength'])); 
}


//trideni podle sloupcu
$order = "";
if(isset($_GET['iSortCol_0'])){
    $s = abs(intval($_GET['iSortCol_0']));
    if($s == 0) $order = "dp.idPolozky";
    elseif($s == 1) $order = "dp.jmeno";
    elseif($s == 2) $order = "dp.text";
    elseif($s == 3) $order = "novy";
    elseif($s == 4) $order = "dp.datum";
    elseif($s == 5) $order = "dp.zobrazit";
    else $order = "dp.idPolozky";
}

if($order != '') 
    $order = "ORDER BY ".$order." ".(isset($_GET['sSortDir_0'])?$_GET['sSortDir_0']:'');

//echo $order;

//sestaveni podminky pro vyhledavani

$like = array();
if($vyraz != '')
{
    $like[] = "(dp.idPolozky LIKE '".$vyraz."%' OR dp.jmeno LIKE '%".$vyraz."%' OR dp.text LIKE '%".$vyraz."%')";
}

$where = "WHERE s.idDomeny=".$domain->getId()."	AND dp.idPolozky IS NOT NULL AND dp.idStranky=".$idDiskuze.((count($like) > 0) ? " AND (".implode(" AND ",$like).")" : "");

$data_pocet = $db->Query("SELECT COUNT(idPolozky) AS celkem
    FROM ".TABLE_DISKUZE_POLOZKY." AS dp
    LEFT JOIN ".TABLE_STRANKY." AS s ON dp.idStranky=s.idStranky		
    ".$where."
    ");
    
$iTotal_arr = $db->getAssoc($data_pocet);
$iTotal = $iTotal_arr['celkem'];


$data = $db->Query("SELECT SQL_CALC_FOUND_ROWS s.nazev, dp.*, IF(dp.datum!='', DATE_FORMAT(dp.datum, '%d.%m.%Y (%H:%i)'),'') AS datum, dp.zobrazit, (dp.novy = 1 AND dp.admin = 0) AS novy, dp.admin 
			FROM ".TABLE_DISKUZE_POLOZKY." AS dp
			LEFT JOIN ".TABLE_STRANKY." AS s ON dp.idStranky=s.idStranky
			".$where."
			".$order."
            ".$limit."
				
			");

$iFilteredTotal_data = $db->Query("SELECT FOUND_ROWS()");
$iFilteredTotal_arr = $db->getRow($iFilteredTotal_data);
$iFilteredTotal = intval($iFilteredTotal_arr[0]);


$dis_content = !$login_obj->UserPrivilege('content_view');
$dis_delete = !$login_obj->UserPrivilege('settings_delete');
$dis_settings = !$login_obj->UserPrivilege('content_view');

$tr = array();
while($c = $db->getAssoc($data))
{
    $ids = $c['idPolozky'];
    if(isset($smiles)) 
        $text = $smiles->DeleteSmilesFromText($c['text']);
        else
        $text = $c['text'];
        
    $stav = secureString($c['novy']);
    $stav_flag="";
    if($stav==1) 
        $stav_flag = "<span class='stav0'>".TNOVY."</span>";
                
    $pom = array();
    $pom[] = json_encode($ids);
    $pom[] = json_encode(secureString($c['jmeno']));
    $pom[] = json_encode(secureString(strip_tags($text)));
    $pom[] = json_encode($stav_flag);
    $pom[] = json_encode($c['datum']);
    $pom[] = json_encode(ano_ne($c['zobrazit']));
    
    $ikony = "";
    if($dis_settings)
        $ikony .=  icon_disabled('content');
        else
        $ikony .= "<a href='".get_link($module,'diskuze',$idDiskuze,'edit-content',$ids)."'>".icon('content')."</a>";
    
    if($dis_delete)
        $ikony .=  icon_disabled('delete');
        else
        $ikony .= "<a href='#' id='delete".$ids."' class='delete'>".icon('delete')."</a>";
    
    $pom[] = json_encode($ikony);
        
    $tr[] = "[".implode(',', $pom)."]";
    
}

echo "{";
echo '"sEcho": '.(intval($_GET['sEcho'])).', ';
echo '"iTotalRecords": '.$iTotal.', ';
echo '"iTotalDisplayRecords": '.($iFilteredTotal).', ';
echo '"aaData": ['.implode(',', $tr).'] }';


?>