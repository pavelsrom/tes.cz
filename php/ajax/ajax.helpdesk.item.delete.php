<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */
 
 
if(!$login_obj->UserPrivilege('content_delete') || !defined("SECURITY_CMS")) exit;


$id = get_int_post('id');

if($id <= 0 || !$object_access->has_access($id)) exit;


$log->add_log('delete',"podpora",$id,$id);

$result = array("messages");
$result['messages']["ok"] = OK_SMAZANO;    

//zjisteni url stranky pro uvodku, po smazani stranky v inline editaci bude uzivatel nasmerovan na uvodni stranku
$db->delete(TABLE_PODPORA,"id=".$id." AND id_domeny=".$domain->getId()." AND id_jazyka=".WEB_LANG_ID);

echo array2json($result);
exit;


?>