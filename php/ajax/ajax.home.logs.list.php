<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined("SECURITY_CMS")) exit;
$uzivatel = isset($_GET['uzivatel']) ? intval($_GET['uzivatel']) : 0;
$jazyk = isset($_GET['jazyk']) ? intval($_GET['jazyk']) : 0;
$akce = isset($_GET['akce']) ? intval($_GET['akce']) : 0;
$datum_od = isset($_GET['datum_od']) ? $db->secureString($_GET['datum_od']) : "";
$datum_do = isset($_GET['datum_do']) ? $db->secureString($_GET['datum_do']) : "";

$akce_typy = array();
if($akce == 1)
    $akce_typy = array("'create'","'edit-settings'","'edit-content'","'delete'","'upload'");
elseif($akce == 2)
    $akce_typy = array("'create'","'edit-settings'","'edit-content'","'upload'");
elseif($akce == 3)
    $akce_typy = array("'edit-settings'","'edit-content'","'upload'");
elseif($akce == 4)
    $akce_typy = array("'delete'");
elseif($akce == 5)
    $akce_typy = array("'login'","'logout'");
elseif($akce == 6)
    $akce_typy = array("'export'","'import'","'send'");
    
    
    
//nastaveni limitu
$limit = "";
if(isset($_GET['iDisplayLength'])){
    $dStart = "";
    if(isset($_GET['iDisplayStart']))
        $dStart = abs(intval($_GET['iDisplayStart'])).",";
        
    $limit = $dStart.abs(intval($_GET['iDisplayLength'])); 
}
    
$logList = new LogList($domain->getId(),$jazyk,$uzivatel,0,"",$akce_typy, $datum_od,$datum_do,$limit);
    
$iFilteredTotal_data = $db->Query("SELECT FOUND_ROWS()");
$iFilteredTotal_arr = $db->getRow($iFilteredTotal_data);
$iFilteredTotal = intval($iFilteredTotal_arr[0]);

$logy = $logList->get_list();
$iTotal = $logList->get_logs_count();

$tr = array();
foreach($logy AS $c)
{
    $pom = array();
    $pom[] = json_encode($c->get_date());
    $pom[] = json_encode(secureString($c->get_user_name()));
    $pom[] = json_encode($c->get_text());
              
    $tr[] = "[".implode(',', $pom)."]";
    
}

echo "{";
echo '"sEcho": '.(intval($_GET['sEcho'])).', ';
echo '"iTotalRecords": '.$iTotal.', ';
echo '"iTotalDisplayRecords": '.($iFilteredTotal).', ';
echo '"aaData": ['.implode(',', $tr).'] }';

?>