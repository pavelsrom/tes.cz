<?php

/**
 * @author Pavel Srom
 * @copyright 2014
 */

$result = array(
    "messages" => array(
        "ok" => "",
        "error" => ""
        )
    );

if(!defined("SECURITY_CMS")) exit;
if(!$login_obj->UserPrivilege('settings_edit','translations')){
    $result["messages"]["error"] = ERROR_NEDOSTATECNA_PRAVA;
    
    exit;
} 

$preklady = get_array_post('preklady');


foreach($preklady AS $p)
{
    $id = str_replace("p","",$p['name']);
    $id = intval($id);
    
    $preklad = $db->secureString($p['value'],false);
    
    $update = array(
        WEB_LANG => $preklad
        );
    
    $db->update(TABLE_PREKLADY,$update,"idPrekladu=".$id);  
    
}

$result["messages"]["ok"] = OK_ULOZENO;

echo array2json($result);

?>