<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */
 
 
if(!$login_obj->UserPrivilege('content_delete') || !defined("SECURITY_CMS")) exit;


$idAnkety = get_int_post('id');

if($idAnkety <= 0) exit;

//overeni spravnosti domeny
$nazev = $db->get(TABLE_ANKETY,"idAnkety","idAnkety=".$idAnkety." AND idDomeny=".$domain->getId());

if($nazev == "") exit;

$log->add_log('delete',"anketa",$idAnkety,$nazev);

$db->delete(TABLE_ANKETY,"WHERE idAnkety=".$idAnkety);
$db->delete(TABLE_ANKETY_OTAZKY,"WHERE idAnkety=".$idAnkety);

$data = $db->query("SELECT idOdpovedi FROM ".TABLE_ANKETY_ODPOVEDI." WHERE idAnkety=".$idAnkety);
while($a = $db->getAssoc($data))
    $db->delete(TABLE_ANKETY_ODPOVEDI_NAZVY,"WHERE idOdpovedi='".$a['idOdpovedi']."'");

$db->delete(TABLE_ANKETY_ODPOVEDI,"WHERE idAnkety=".$idAnkety);

exit;


?>