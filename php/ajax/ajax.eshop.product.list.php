<?php

/**
 * @author Pavel Srom
 * @copyright 2010
 */

//ajaxovy skript na odhlaseni kontaktu z odberu newsletteru

if(!isset($_GET['module']) || !defined("SECURITY_CMS")) exit;

$vyraz = trim($db->secureString($_GET['vyraz']));
$module = trim($_GET['module']);
$idRubriky = isset($_GET['rubrika']) ? intval($_GET['rubrika']) : 0;

//nastaveni limitu
$limit = "";
if(isset($_GET['iDisplayLength'])){
    $dStart = "";
    if(isset($_GET['iDisplayStart']))
        $dStart = abs(intval($_GET['iDisplayStart'])).",";
        
    $limit = " LIMIT ".$dStart.abs(intval($_GET['iDisplayLength'])); 
}


//trideni podle sloupcu
$order = "";
if(isset($_GET['iSortCol_0'])){
    $s = abs(intval($_GET['iSortCol_0']));
    if($s == 0) $order = "s.idStranky";
    elseif($s == 1) $order = "s.nazev";
    elseif($s == 2) $order = "kategorie";
    elseif($s == 3) $order = "cena_bez_dph";
    elseif($s == 4) $order = "dph";
    elseif($s == 6) $order = "cena_s_dph";
    elseif($s == 7) $order = "s.diskuze";
    elseif($s == 9) $order = "s.zobrazit";
    else $order = "s.priorita";
}

if($order != '') 
    $order = "ORDER BY ".$order." ".(isset($_GET['sSortDir_0'])?$_GET['sSortDir_0']:'');

$order = "ORDER BY s.priorita, s.idStranky";

//echo $order;

//sestaveni podminky pro vyhledavani
$like = array();
if($idRubriky > 0)
{
    $like[] = "s.idRodice=".$idRubriky;
}
if($vyraz != '')
{
    $like[] = "(s.idStranky LIKE '".$vyraz."%' OR s.nazev LIKE '%".$vyraz."%')";
}
   


$where = "WHERE s.idJazyka = ".WEB_LANG_ID." AND s.typ='produkt' AND s.idDomeny=".$domain->getId().((count($like) > 0) ? " AND ".implode(" AND ",$like) : "");

$data_pocet = $db->Query("SELECT COUNT(idStranky) AS celkem FROM ".TABLE_STRANKY." AS s ".$where);
$iTotal_arr = $db->getAssoc($data_pocet);
$iTotal = $iTotal_arr['celkem'];


        
$data = $db->Query("SELECT SQL_CALC_FOUND_ROWS s.*, DATE_FORMAT(s.datum,'%d.%m.%Y') AS datum, r.nazev AS kategorie,
                s.cena_bez_dph, s.cena_s_dph, s.dph, j.jazyk
				FROM ".TABLE_STRANKY." AS s
                LEFT JOIN ".TABLE_JAZYKY." AS j ON s.idJazyka = j.idJazyka
                LEFT JOIN ".TABLE_STRANKY." AS r ON s.idRodice=r.idStranky
				".$where."
				GROUP BY s.idStranky
				".$order."
                ".$limit."
				");


$iFilteredTotal_data = $db->Query("SELECT FOUND_ROWS()");
$iFilteredTotal_arr = $db->getRow($iFilteredTotal_data);
$iFilteredTotal = intval($iFilteredTotal_arr[0]);


$dis_content = !$login_obj->UserPrivilege('content_view');
$dis_delete = !$login_obj->UserPrivilege('content_delete');


$tr = array();
$i = 1;
$pocet = $db->numRows($data);

while($c = $db->getAssoc($data))
{
    $ids = $c['idStranky'];
    $idr = $c['idRodice'];
    
    $private_pages = new C_PrivatePages($ids);
    $privatni = ano_ne($private_pages->is_private(),implode(", ", $private_pages->get_groups())); 

    
    $pom = array();
    $pom[] = json_encode($ids);
    $pom[] = json_encode(secureString($c['nazev']));
    $pom[] = json_encode(secureString($c['kategorie']));
    $pom[] = json_encode(price($c['cena_bez_dph']));
    $pom[] = json_encode(secureString($c['dph']."%"));
    $pom[] = json_encode(price($c['cena_s_dph']));
    $pom[] = json_encode($c['diskuze']==0?"<span class='stav0'>".TNE."</span>":(MODULE_DISCUSSION ? "<a href=".get_link("discussions","diskuze",$ids)." title='".TZOBRAZIT_DISKUZNI_PRISPEVKY."'><span class='stav1'>".TANO."</span>":"<span class='stav1'>".TANO."</span>"));
    $pom[] = json_encode($privatni);
    $pom[] = json_encode(ano_ne($c['zobrazit']));
    
    $ikony_up_down = "";
    if($i==1 || $dis_content)
        $ikony_up_down .= icon_disabled('up');
        else
        $ikony_up_down .= "<a href='#' id='up".$ids."' class='up'>".icon('up')."</a>";
        				
    if($i == $pocet || $dis_content)
        $ikony_up_down .= icon_disabled('down');
        else
        $ikony_up_down .= "<a href='#' id='down".$ids."' class='down'>".icon('down')."</a>";
    
    //$pom[] = json_encode($ikony_up_down);
    
    $ikony = "";
    
    $ikony = "<a href='".urlPage($c['url'],$ids, $c['jazyk'])."' target='_blank'>".icon('link')."</a>";
    
    
    if($dis_content)
        $ikony .= icon_disabled('content');
        else
        $ikony .= "<a href='".get_link('eshop','kategorie',$idr,'edit-content',$ids)."'>".icon('content')."</a>";
    
    if($dis_delete || !$object_access->has_access($ids))
        $ikony .= icon_disabled('delete');
        else
        $ikony .= "<a href='#' id='delete".$ids."' class='delete'>".icon('delete')."</a>";
        
    $pom[] = json_encode($ikony_up_down.$ikony);
        
    $tr[] = "[".implode(',', $pom)."]";
    $i++;
}

echo "{";
echo '"sEcho": '.(intval($_GET['sEcho'])).', ';
echo '"iTotalRecords": '.$iTotal.', ';
echo '"iTotalDisplayRecords": '.($iFilteredTotal).', ';
echo '"aaData": ['.implode(',', $tr).'] }';


?>