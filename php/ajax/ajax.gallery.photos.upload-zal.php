<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if (empty($_FILES)) exit;

include_once(PRE_PATH.'php/classes/class.upload.php');

$result = array(
    "file" => array("name" => isset($_FILES['Filedata']['name']) ? $_FILES['Filedata']['name'] : ""),
    "messages" => array(
        "error" => ""
        )

    );

 
$nazev = $db->secureString($_FILES['Filedata']['name']);
    
$n = explode('.',$nazev);
$nazev = $n[0];
    
$idGalerie = isset($_REQUEST['gallery_id']) ? intval($_REQUEST['gallery_id']) : 0;
$idGalerie = $idGalerie == 0 && isset($_REQUEST['id_ine_page']) ? intval($_REQUEST['id_ine_page']) : $idGalerie;

$nazev_galerie = $db->get(TABLE_STRANKY,'nazev','idStranky='.$idGalerie);

if($idGalerie == 0)
{
    echo array2json($result);
    exit;
}

$upload_obrazek = array(
        'name' => $_FILES['Filedata']['name'],
        'type' => $_FILES['Filedata']['type'],
        'tmp_name' => $_FILES['Filedata']['tmp_name'],
        'error' => $_FILES['Filedata']['error'],
        'size' => $_FILES['Filedata']['size']
        );

if($_FILES['Filedata']['tmp_name'] == '' || $_FILES['Filedata']['name'] == '')
{
    
    echo array2json($result);
    exit;   
}
	

                
$nazev_souboru_arr = explode('.', $upload_obrazek['name']);
$ext = strtolower(end($nazev_souboru_arr));
array_pop($nazev_souboru_arr);
$nazev_souboru = implode(".",$nazev_souboru_arr);;
					
                    
if(!in_array($ext,array("jpg","gif","png")))
{
    $result['messages']['error'] = ERROR_NEPODPOROVANY_FORMAT_SOUBORU; 
    echo array2json($result);
    exit; 
    
}
                    
$articles_path_mini = PRE_PATH.$domain->getDir().USER_DIRNAME_GALLERY_MINI;
$articles_path_stredni = PRE_PATH.$domain->getDir().USER_DIRNAME_GALLERY_STREDNI;
$articles_path_maxi = PRE_PATH.$domain->getDir().USER_DIRNAME_GALLERY_MAXI;
											            
$error = GetUploadError($upload_obrazek['error']);
if($error != '')
{

    $result['messages']['error'] = $error; 
    echo array2json($result);
    exit; 
}


 
 
              
$db->Query("INSERT INTO ".TABLE_STRANKY." (idDomeny,typ, zobrazit, idRodice,doMenu, datum) VALUES (".$domain->getId().",'galerie-fotka',1,".$idGalerie.",0,NOW()) ");
                
$idObrazku = $db->lastId();
$file = $idObrazku.".jpg";
                    
$uploader = new upload($upload_obrazek, 'cs_CS');
$uploader->file_new_name_body = $idObrazku;
$uploader->file_overwrite = true;
$uploader->image_resize = true;
$uploader->image_ratio = true;
$uploader->image_x = intval(OBRAZEK_GALERIE_MAXI_X);
$uploader->image_y = intval(OBRAZEK_GALERIE_MAXI_Y);
$uploader->image_convert = 'jpg';
$uploader->process($articles_path_maxi);
                
$error = $uploader->error;
if($error != '')
{
    $result['messages']['error'] = $error; 
    echo array2json($result);
    exit; 
}
                
$uploader->file_new_name_body = $idObrazku;
$uploader->image_resize = true;
$uploader->image_ratio = true;
$uploader->image_convert = 'jpg';
$uploader->image_x = intval(OBRAZEK_GALERIE_MEDIUM_X);
$uploader->image_y = intval(OBRAZEK_GALERIE_MEDIUM_Y);
$uploader->process($articles_path_stredni);
       
$error = $uploader->error;
if($error != '')
{
    $result['messages']['error'] = $error; 
    echo array2json($result);
    exit; 
}
                    
$uploader->file_new_name_body = $idObrazku;
$uploader->image_resize = true;
$uploader->image_ratio = true;
$uploader->image_convert = 'jpg';
$uploader->image_x = intval(OBRAZEK_GALERIE_MINI_X);
$uploader->image_y = intval(OBRAZEK_GALERIE_MINI_Y);
$uploader->process($articles_path_mini);
      
$error = $uploader->error;
if($error != '')
{
    $result['messages']['error'] = $error; 
    echo array2json($result);
    exit; 
}
                   
$uploader->clean();

                
if($nazev == '')
    $nazev = $file;

$url = ModifyUrl($nazev);
                
//overeni url nazvu stranky, pokud existuje, tak se automaticky priradi novy url nazev
if($domain->data['urlStruktura']=='nazev')
{  
    $where = "";
    if($idObrazku>0)
        $where = "AND idStranky != ".$idObrazku; 
                        
    $verify_url_name = $db->Query("SELECT url FROM ".TABLE_STRANKY." WHERE url LIKE '".$url."%' AND idDomeny=".$domain->getId()." ".$where." ORDER BY url");
                    
    $url_zal = $url;
    $j=1;
    while($u = $db->getAssoc($verify_url_name))
    {
        if($url_zal==$u['url'])
            $url_zal = $url."-".$j;
        $j++;
    }
                        
    $url = $url_zal;
                    
}
      
$upload = array(
    "obrazek" => $file,
    "url" => $url,
    "nazev" => $nazev_souboru,
    "nazevProMenu" => $nazev_souboru,
    "nadpis" => $nazev_souboru,
    "title" => $links->get($idGalerie,'nazev')." - ". $nazev_souboru,
    "idVytvoril"    => $login_obj->getId(),
    "jmenoVytvoril" => $login_obj->getName(),
    "datumVytvoril" => "now",
    'idAktualizoval'=> $login_obj->getId(),
    'jmenoAktualizoval' => $login_obj->getName(),
    'datumAktualizoval' => "now"    
    );          
                
$db->update(TABLE_STRANKY,$upload,"idStranky = ".$idObrazku." AND idDomeny=".$domain->getId()." LIMIT 1");

$log->add_log('upload','galerie-fotka',$idObrazku,$nazev_galerie);   

$img = "";
$img .= "<div class='ine_fotka' id='f".$idObrazku."'>";
$img .= "<img src='".RELATIVE_PATH.$domain->getDir().USER_DIRNAME_GALLERY_MINI.$file."' alt='' />";
$img .= "<input type='text' value='".$nazev_souboru."' class='input ui-widget-content ui-corner-all' title='".TNAZEV."' name='fotka_nazev[".$idObrazku."]'/><br />";
$img .= "<textarea class='input ui-widget-content ui-corner-all' title='".TPOPIS."' name='fotka_nazev[".$idObrazku."]'></textarea>";
$img .= "<a href='".RELATIVE_PATH.$domain->getDir().USER_DIRNAME_GALLERY_MAXI.$file."' target='_blank'>".TDETAIL_FOTKY."</a> | <a href='#' class='ine_delete'>".TSMAZAT."</a>";
$img .= "</div>";


$result['messages']['ok'] = TBYL_USPESNE_NAHRAN;
$result['file']['html'] = $img; 
echo array2json($result);

exit;


?>