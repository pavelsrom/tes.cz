<?php

/**
 * @author Pavel Srom
 * @copyright 2013
 */

//print_r($_GET);


$den = get_int_get('day');
$mesic = get_int_get('month');
$rok = get_int_get('year');

if($rok <= 0 || $mesic <= 0)
    exit;
    
$mesic++;

$result = array();

if($den <= 0)
{
    $datum_od = $rok."-".$mesic."-01 00:00:00";
    $datum_do = $rok."-".$mesic."-31 23:59:59";
}
else
{
    $datum_od = $rok."-".$mesic."-".$den." 00:00:00";
    $datum_do = $rok."-".$mesic."-".$den." 23:59:59";
}    


include("../classes/class.event.calendar.php");
$eventCalendar = new C_EventCalendar('box',0,$datum_od, $datum_do);
$events = $eventCalendar->get_items();
//print_r($events);
//exit;

foreach($events AS $e)
{
    $result[] = array(
        "date" => $den > 0 ? $datum_od : $e['od'],
        "date_format" => $e['format_datum'],
        "barva"=> $e['barva'],
        "misto"=> $e['misto'],
        "image"=> $e['obrazek_path'],
        "type" => $e['kategorie'],
        "title"=> $e['nazev'],
        "description" => strip_tags($e['perex']),
        "url"  => $e['je_obsah'] ? $links->get_url($e['id_akce']) : ""
        );
}

header('Content-type: text/json');
echo json_encode($result);
exit;

?>