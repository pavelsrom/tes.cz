<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */
 
 
if(!$login_obj->UserPrivilege('settings_delete') || !defined("SECURITY_CMS")) exit;


$id = get_int_post('id');

if($id <= 0) exit;

$nazev = $db->get(TABLE_EMAILY_SKUPINY,'nazev','idSkupiny='.$id." AND idDomeny=".$domain->getId());

if($nazev == '')
{
    exit;
}            
            
$db->Query("DELETE FROM ".TABLE_EMAILY_SKUPINY." WHERE idDomeny=".$domain->getId()." AND idSkupiny=".$id);
$db->Query("DELETE FROM ".TABLE_EMAILY_SKUPINY_KAMPANE." WHERE idSkupiny=".$id);
$db->Query("DELETE FROM ".TABLE_EMAILY_SKUPINY_PRIJEMCI." WHERE idSkupiny=".$id);

$log->add_log('delete','newsletter-skupiny',$id,$nazev);

exit;


?>