<?php

/**
 * @author Pavel Srom
 * @copyright 2013
 */

if(!isset($_GET['module']) || !defined("SECURITY_CMS")) exit;

$module = trim($_GET['module']);

$idInquiry = intval($_GET['id']);

$sum_data = $db->Query("SELECT sum(hlasy) AS sum FROM ".TABLE_ANKETY_ODPOVEDI." WHERE idAnkety=".$idInquiry);
$sum_arr = $db->getAssoc($sum_data);
$sum = $sum_arr['sum'];
if($sum<1) $sum = 1;

$data_pocet = $db->Query("SELECT COUNT(idOdpovedi) AS celkem FROM ".TABLE_ANKETY_ODPOVEDI." AS a GROUP BY idAnkety");
$iTotal_arr = $db->getAssoc($data_pocet);
$iTotal = $iTotal_arr['celkem'];


        
$data = $db->Query("SELECT odp.*, odpn.nazev 
            FROM ".TABLE_ANKETY_ODPOVEDI." AS odp
            LEFT JOIN ".TABLE_ANKETY_ODPOVEDI_NAZVY." AS odpn ON odp.idOdpovedi = odpn.idOdpovedi 
            WHERE odp.idAnkety=".$idInquiry."
                AND odpn.idJazyka=1
                AND odpn.nazev IS NOT NULL
            GROUP BY odp.idOdpovedi
            ORDER BY priorita, idOdpovedi
            ");
            
$iFilteredTotal_data = $db->Query("SELECT FOUND_ROWS()");
$iFilteredTotal_arr = $db->getRow($iFilteredTotal_data);
$iFilteredTotal = intval($iFilteredTotal_arr[0]);


$dis_content = !$login_obj->UserPrivilege('content_view');
$dis_delete = !$login_obj->UserPrivilege('settings_delete');
$dis_settings = !$login_obj->UserPrivilege('settings_view');

$tr = array();
$i = 1;
$pocet = $db->numRows($data);

while($c = $db->getAssoc($data))
{
   
    $ids = $c['idOdpovedi'];

    $proc = (round($c['hlasy']/$sum*100));
    
    $score_procenta = $proc."%";
    $score_hlasy = $c['hlasy'];
    
    $pom = array();
    $pom[] = json_encode($ids);
    $pom[] = json_encode(secureString($c['nazev']));
    
    $pom[] = json_encode("<div class='anketa-graf'><div class='anketa-graf-hlasy' style='width: ".$score_procenta."; background-color: #".$c['barva']."'></div>");
    $pom[] = json_encode($score_hlasy." (".$score_procenta.")");
    
    $ikony_up_down = "";
    if($i==1 || $dis_content)
        $ikony_up_down .= icon_disabled('up');
        else
        $ikony_up_down .= "<a href='#' id='up".$ids."' class='up'>".icon('up')."</a>";
        				
    if($i == $pocet || $dis_content)
        $ikony_up_down .= icon_disabled('down');
        else
        $ikony_up_down .= "<a href='#' id='down".$ids."' class='down'>".icon('down')."</a>";
    
    //$pom[] = json_encode($ikony_up_down);
    
    
    $ikony = "";
    /*
    if($dis_settings)
        $ikony .=  icon_disabled('setting');
        else
        $ikony .= "<a href='".get_link($module,'ankety',$ids,'edit-settings',$idInquiry)."'>".icon('setting')."</a>";
    */
    if($dis_content)
        $ikony .= icon_disabled('content');
        else
        $ikony .= "<a href='".get_link($module,'ankety',$ids,'edit-content',$idInquiry)."'>".icon('content')."</a>";
    
    if($dis_delete)
        $ikony .= icon_disabled('delete');
        else
        $ikony .= "<a href='#' id='delete".$ids."' class='delete'>".icon('delete')."</a>";
        
    $pom[] = json_encode($ikony_up_down.$ikony);
        
    $tr[] = "[".implode(',', $pom)."]";
    $i++;
}

echo "{";
echo '"sEcho": '.(intval($_GET['sEcho'])).', ';
echo '"iTotalRecords": '.$iTotal.', ';
echo '"iTotalDisplayRecords": '.($iFilteredTotal).', ';
echo '"aaData": ['.implode(',', $tr).'] }';
            




?>