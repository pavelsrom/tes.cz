<?php

/**
 * @author Pavel Srom
 * @copyright 2010
 */


if(!defined("SECURITY_CMS")) exit;


$vyraz = trim($db->secureString($_GET['vyraz']));
$module = trim($_GET['module']);

$jazyky = $lang->get_langs();
$jazyky_nazvy = array();
foreach($jazyky AS $j)
    $jazyky_nazvy[] = $j['jazyk']; 

//nastaveni limitu
$limit = "";
if(isset($_GET['iDisplayLength'])){
    $dStart = "";
    if(isset($_GET['iDisplayStart']))
        $dStart = abs(intval($_GET['iDisplayStart'])).",";
        
    $limit = " LIMIT ".$dStart.abs(intval($_GET['iDisplayLength'])); 
}


//trideni podle sloupcu
$order = "";
if(isset($_GET['iSortCol_0'])){
    $s = abs(intval($_GET['iSortCol_0']));
    if($s == 0) $order = "idPrekladu";
    elseif($s == 1) $order = "konstanta";
    else $order = "idPrekladu";
}

if($order != '') 
    $order = "ORDER BY ".$order." ".(isset($_GET['sSortDir_0'])?$_GET['sSortDir_0']:'');

//sestaveni podminky pro vyhledavani
$like = array();

if($vyraz != '')
{
    $like[] = "idPrekladu = '".$vyraz."'";
    $like[] = "konstanta LIKE '%".$vyraz."%'";

    foreach($jazyky AS $j)
        $like[] = $j['jazyk']." LIKE '%".$vyraz."%'";

}
    

if(count($like) > 0)
    $where = "WHERE ".implode(" OR ",$like);
    else
    $where = "";


$data_pocet = $db->Query("SELECT COUNT(idPrekladu) AS celkem FROM ".TABLE_PREKLADY." ".$where);
$iTotal_arr = $db->getAssoc($data_pocet);
$iTotal = $iTotal_arr['celkem'];

$data = $db->Query("SELECT SQL_CALC_FOUND_ROWS idPrekladu, konstanta, ".implode(",",$jazyky_nazvy)." 
                FROM ".TABLE_PREKLADY."
				".$where." 
                ".$order."
                ".$limit."
                
                ");


$iFilteredTotal_data = $db->Query("SELECT FOUND_ROWS()");
$iFilteredTotal_arr = $db->getRow($iFilteredTotal_data);
$iFilteredTotal = intval($iFilteredTotal_arr[0]);

$dis_delete = !$login_obj->UserPrivilege('superadmin');
$dis_settings = !$login_obj->UserPrivilege('settings_edit','translations');

$tr = array();
while($c = $db->getAssoc($data))
{
    $ids = $c['idPrekladu'];
    $pom = array();
    $pom[] = json_encode($ids);
    $pom[] = json_encode(secureString($c['konstanta']));
    
    $table = new Table("","tPreklady");

    $i = 0;
    if(count($jazyky) == 1)
    {
        foreach($jazyky AS $j)
            $table->tr()
                ->add_th(strtoupper($j['jazyk']))
                ->add_td("<input type='text' name='".$j['jazyk']."[".$ids."]' value='".$c[$j['jazyk']]."' class='input w100' id='".$j['jazyk']."".$ids."'/>");
    }
    else
    {
        
        $trt = $table->tr();
        foreach($jazyky AS $j)
        {
            if($i > 0 && $i % 2 == 0)
                $trt = $table->tr();
            
            
            $trt->add_th(strtoupper($j['jazyk']))
               ->add_td("<input type='text' name='".$j['jazyk']."[".$ids."]' value='".$c[$j['jazyk']]."' class='input w100' id='".$j['jazyk']."".$ids."'/>");
                
            $i++;
        }
    }

    if($i%2 == 1)
        $trt->add()->add();
    
    $pom[] = json_encode($table->get_html());
    
    $ikony = "";
    
    if($dis_settings)
        $ikony .=  icon_disabled('save');
        else
        $ikony .= "<a href='#' id='save".$ids."' class='save'>".icon('save')."</a>";
        //$ikony .= "<a href='".get_link($module,'preklady-admin',0,'edit-settings',$ids)."'>".icon('save')."</a>";
    
    
    if($dis_delete)
        $ikony .= icon_disabled('delete');
        else
        $ikony .= "<a id='delete".$ids."' class='delete' href='#'>".icon('delete')."</a>";
        
        //$ikony .= "<a href=\"javascript:confirm_url('".sprintf(TOPRAVDU_SMAZAT,$c['konstanta'])."?', '".get_link($module,'preklady-admin',0,'delete',$ids)."') \">".icon('delete')."</a>";
        
    $pom[] = json_encode($ikony);
        
    $tr[] = "[".implode(',', $pom)."]";
    
}

echo "{";
echo '"sEcho": '.(intval($_GET['sEcho'])).', ';
echo '"iTotalRecords": '.intval($iTotal).', ';
echo '"iTotalDisplayRecords": '.($iFilteredTotal).', ';
echo '"aaData": ['.implode(',', $tr).'] }';


?>