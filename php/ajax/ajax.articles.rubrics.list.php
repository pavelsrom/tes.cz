<?php

/**
 * @author Pavel Srom
 * @copyright 2010
 */

//ajaxovy skript na odhlaseni kontaktu z odberu newsletteru

if(!isset($_GET['module']) || !defined("SECURITY_CMS")) exit;

$vyraz = trim($db->secureString($_GET['vyraz']));
$module = trim($_GET['module']);


//nastaveni limitu
$limit = "";
if(isset($_GET['iDisplayLength'])){
    $dStart = "";
    if(isset($_GET['iDisplayStart']))
        $dStart = abs(intval($_GET['iDisplayStart'])).",";
        
    $limit = " LIMIT ".$dStart.abs(intval($_GET['iDisplayLength'])); 
}


//trideni podle sloupcu
$order = "";
if(isset($_GET['iSortCol_0'])){
    $s = abs(intval($_GET['iSortCol_0']));
    if($s == 0) $order = "s.idStranky";
    elseif($s == 1) $order = "s.nazev";
    elseif($s == 2) $order = "pocet_clanku";
    elseif($s == 3) $order = "s.zobrazit";
    else $order = "s.idStranky";
}

if($order != '') 
    $order = "ORDER BY ".$order." ".(isset($_GET['sSortDir_0'])?$_GET['sSortDir_0']:'');

//echo $order;

//sestaveni podminky pro vyhledavani
$like = array();
if($vyraz != '')
{
    $like[] = "s.idStranky LIKE '".$vyraz."%'";
    $like[] = "s.nazev LIKE '%".$vyraz."%'";
}
    


$where = "WHERE s.idJazyka = ".WEB_LANG_ID." AND s.typ='clanky' AND s.idDomeny=".$domain->getId().((count($like) > 0) ? " AND (".implode(" OR ",$like).")" : "");

$data_pocet = $db->Query("SELECT COUNT(idStranky) AS celkem FROM ".TABLE_STRANKY." AS s ".$where);
$iTotal_arr = $db->getAssoc($data_pocet);
$iTotal = $iTotal_arr['celkem'];


        
$data = $db->Query("SELECT SQL_CALC_FOUND_ROWS s.*, COUNT(c.idStranky) AS pocet_clanku, j.jazyk
				FROM ".TABLE_STRANKY." AS s
                LEFT JOIN ".TABLE_JAZYKY." AS j ON s.idJazyka = j.idJazyka
                LEFT JOIN ".TABLE_STRANKY." AS c ON s.idStranky = c.idRodice AND c.typ='clanek' AND c.idJazyka=".WEB_LANG_ID."
				".$where."
                    
				GROUP BY s.idStranky
				".$order."
                ".$limit."
				");


$iFilteredTotal_data = $db->Query("SELECT FOUND_ROWS()");
$iFilteredTotal_arr = $db->getRow($iFilteredTotal_data);
$iFilteredTotal = intval($iFilteredTotal_arr[0]);

$dis_content = !$login_obj->UserPrivilege('content_view');

$tr = array();
while($c = $db->getAssoc($data))
{

    $ids = $c['idStranky'];
    
    $private_pages = new C_PrivatePages($ids);
    $privatni = ano_ne($private_pages->is_private(),implode(", ", $private_pages->get_groups())); 

    
    $pom = array();
    $pom[] = json_encode($ids);
    $pom[] = json_encode("<a href='".get_link('pages','stranky',0,'edit-content',$ids)."' title='".TEDITOVAT_RUBRIKU."'>".secureString($c['nazev'])."</a>");
    $pom[] = json_encode(intval($c['pocet_clanku']));
    $pom[] = json_encode($privatni);
    $pom[] = json_encode(ano_ne($c['zobrazit']));
    
    
    $ikony = "";
    $ikony = "<a href='".urlPage($c['url'],$ids,$c['jazyk'])."' target='_blank'>".icon('link')."</a>";
    
    if($dis_content)
        $ikony .= icon_disabled('content');
        else
        $ikony .= "<a href='".get_link('pages','stranky',0,'edit-content',$ids)."'>".icon('content')."</a>";
    
    $ikony .= "<a href='".get_link($module,'rubriky',$ids)."'>".icon('articles')."</a>";
    
    $pom[] = json_encode($ikony);
        
    $tr[] = "[".implode(',', $pom)."]";
    
}

echo "{";
echo '"sEcho": '.(intval($_GET['sEcho'])).', ';
echo '"iTotalRecords": '.$iTotal.', ';
echo '"iTotalDisplayRecords": '.($iFilteredTotal).', ';
echo '"aaData": ['.implode(',', $tr).'] }';


?>