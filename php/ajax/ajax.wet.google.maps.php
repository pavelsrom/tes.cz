<?php

/**
 * @author Pavel Srom
 * @copyright 2010
 */


if(!isset($_REQUEST['adresa']) || !isset($_REQUEST['zoom']) || !defined("SECURITY_CMS")) {
    redirect('',404);
    exit;
    }


if(isset($_GET['nahled'])) $nahled = 1; else $nahled = 0;

$zoom = intval($_POST['zoom']);
if($zoom<=0) $zoom = 16;

$adresa = urlencode(trim($_POST['adresa']));
$typ='google';


$url = "";
if($typ=='seznam'){
    $url = "http://www.mapy.cz/printMap?attch=0&mm=ZTtTcP&sa=s&st=s&ssq=".$adresa."&sss=1&redir=1";
    echo "<iframe src='".$url."' width='400' height='400' ></iframe>";
    }
elseif($typ=='google'){
    if($nahled==1){
        $url = "http://maps.google.com/maps/api/staticmap?center=".$adresa."&zoom=".$zoom."&format=jpg&size=400x400&markers=color:blue|label:S|".$adresa."&sensor=false";
        echo $url;
        }
        else
        {
        $url = "http://maps.google.com/maps/api/staticmap?center=".$adresa."&zoom=".$zoom."&size=400x400&markers=color:blue|label:S|".$adresa."&sensor=false";
        echo "<a href='http://www.google.cz/maps?q=".$adresa."'><img src='".$url."' alt='mapka' class='mapka'/></a>";
        }
    }



exit;


?>