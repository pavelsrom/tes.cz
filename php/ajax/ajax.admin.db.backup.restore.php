<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */
 
 
if(!defined("SECURITY_CMS")) exit;

if((!$login_obj->UserPrivilege('admin') && !$login_obj->UserPrivilege('superadmin')) || !$login_obj->UserPrivilege('settings_edit')) exit;

$id = get_int_post('id');

if($id <= 0) exit;

$soubor_zalohy = $db->get(TABLE_ZALOHY_DB,"soubor","idZalohy=".$id);
$soubor_zalohy_path = PRE_PATH.DIRNAME_DB_BACKUP.$soubor_zalohy;
$output = "";

//vytvoreni zalohy pred prehranim cele databaze
include(PRE_PATH."php/classes/class.mysql.backup.php");

$backup = new MySQL_Backup();
$backup->drop_tables = true;
$backup->tables = array();
$backup->disallow_tables = array(TABLE_ZALOHY_DB);
$backup->table_prefix = TABLE_PREFIX;
$backup->comments = false;

$task = MSB_SAVE;
$use_gzip = POUZIT_GZ_KOMPRESI;
$filename = 'db-backup-'.substr(md5(uniqid().time()),0,10).".sql.gz"; 
$file_path = PRE_PATH.DIRNAME_DB_BACKUP.$filename;


if (!$backup->Execute($task, $file_path, $use_gzip))
	 $output = $backup->error; 



//ulozeni do db
if($output == "" && file_exists($file_path))
{
    $insert = array(
        "soubor" => $filename,
        "velikost" => filesize($file_path),
        "datum" => "now",
        "jmenoUzivatele" => "system"
        );
    
    $db->insert(TABLE_ZALOHY_DB, $insert);
}

//nacteni a parsovani gz souboru s mysql zalohou, ulozeni do db
include(PRE_PATH."php/classes/class.mysql.parser.php");
$parser = new MySQL_Restore($soubor_zalohy_path, POUZIT_GZ_KOMPRESI);
if($parser->getError() == "")
{
    //vymaze databazi krome nekterych tabulek
    $tabulky_ktere_se_nemazou = array(TABLE_ZALOHY_DB);
    
    $data = $db->query("SHOW TABLES LIKE '".TABLE_PREFIX."%'");
    while($t = $db->getRow($data))
    {
        //continue;
        if(!in_array($t[0],$tabulky_ktere_se_nemazou))
            $db->query("DROP TABLE ".$t[0]);
            
    }
    
    
    //importuje do db ze zalohy
    //$parser->startParsing();
    $sql_querys = $parser->split_sql_file();
    foreach($sql_querys AS $q)
        $db->query($q);
}
    
echo $parser->getError();

exit;


?>