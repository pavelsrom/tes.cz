<?php

/**
 * @author Pavel Srom
 * @copyright 2014
 */


if(!defined("SECURITY_CMS")) exit;

if(WEB_LANG == "" || !$login_obj->UserPrivilege('settings_view')) exit;

include("../classes/class.form.admin.php");
include("../functions/functions.inputs.php");
include("../functions/functions.ine.php");

$table = new Table("tIneNastaveni","ine_table");
$table->tr_head()
    ->add(TID,"w25")
    ->add(TNAZEV,"t-left")
    ->add(THODNOTA);
    
    
//nacteni aktivnich modulu pro uzivatele a pro domenu
$mu = $login_obj->getModules();
$md = $domain->data['moduly'];

//prunik prav, zustanou jen ty spolecne pro uzivatele a pro domenu
$moduly = array_intersect($mu,$md); 


$form = new Form();
$form->allow_upload();
//$form->set_jquery_ui_css();

$data = $db->query("SELECT if(nd.hodnota IS NULL, n.hodnota,nd.hodnota) AS hodnota, n.typ, n.nazev, n.idNapovedy, n.idNastaveni, m.nazev AS modul, n.idModulu AS idModulu
        FROM ".TABLE_NASTAVENI." AS n
        LEFT JOIN ".TABLE_NASTAVENI_DOMENY." AS nd ON n.idNastaveni=nd.idNastaveni AND nd.idDomeny=".$domain->getId()."
        LEFT JOIN ".TABLE_CMS_MODULY." AS m ON m.idModulu = n.idModulu
        WHERE n.aktivni=1
            AND n.zakladni=1
        GROUP BY n.idNastaveni
        ORDER BY m.podmodul, m.priorita, m.nazev
        ");
        
    
    
    $mn = "";
    while($nastaveni = $db->getAssoc($data))
    {
        
        
        
        if($mn != $nastaveni['modul'])
        {
            $nazev_modulu = constant($nastaveni['modul']);
            $form->add_section($nazev_modulu,'mid'.$nastaveni['idModulu']);
            $mn = $nastaveni['modul'];
        }
        
        $typ = $nastaveni['typ'];
        $hodnota = $nastaveni['hodnota'];
        $nazev = constant($nastaveni['nazev']);
        $napoveda = $nastaveni['idNapovedy'];
        $idNastaveni = $nastaveni['idNastaveni'];
        $name = "h".$idNastaveni;
        
        if($typ == 'input')
        {
            $form->add_text($nazev,$name,$hodnota,$napoveda);
        }
        elseif($typ == 'barva')
        {
            $form->add_text($nazev,$name,$hodnota,$napoveda,false,"color");
        }
        elseif($typ == 'editor')
        {
            $form->add_editor($nazev,$name,$hodnota,$napoveda);
        }
        elseif($typ == "textarea")
        {
            $form->add_textarea($nazev,$name,$hodnota,$napoveda);
        }
        elseif($typ == "radio")
        {
            $form->add_radiobuttons($nazev,$name,$hodnota,null,$napoveda);
        }
        elseif($typ == "ga")
        {
            $rc4 = new rc4crypt;
            $hodnota = unserialize($hodnota);
            $hodnota["GA_HESLO"] = $rc4->endecrypt($domain->get('hash'),$hodnota["GA_HESLO"],'de');
            
            $form->add_section(TPRISTUP_KE_GOOGLE_ANALYTICS,'ga');
            $form->add_text(TEMAIL,'gaEmail',$hodnota["GA_EMAIL"],131,false,'','gaEmail');
            $form->add_password(THESLO,'gaHeslo',$hodnota["GA_HESLO"],132,false,'','gaHeslo');
            $form->add_text(TID_PROFILU,'gaProfil',$hodnota["GA_PROFIL"],133,false,'','gaProfil');
            $form->add_plain_text("","<a href='#' id='overit_dostupnost'>".TOVERIT_DOSTUPNOST."</a>");	
        }
        elseif($typ == "smajlici")
        {
            $smiles = array();
            $data_smajlici = $db->Query("SELECT idTematu, nazev FROM ".TABLE_SMAJLICI." WHERE zobrazit=1");
            while($smile = $db->getAssoc($data_smajlici))
                $smiles[$smile['idTematu']] = $smile['nazev'];
                
            $form->add_selectbox($nazev,$name,$hodnota,array('' => "-- ".TVYBERTE." --") + $smiles,0,false,"","smajlici_select");
        
            $form->add_plain_text("","<div id='smajlici'></div>");
        }
        elseif($typ == "strom")
        {
            $hodnota = unserialize($hodnota);
            $typh = isset($hodnota['typ']) ? $hodnota['typ'] : "";
            $stranky = array(0 => "-- ".TVYBERTE." --") + get_pages(0, WEB_LANG_ID);
            $h = isset($hodnota[WEB_LANG_ID]) ? intval($hodnota[WEB_LANG_ID]) : 0;
            
            $nazev_selectboxu = TSTRANKY;
            if($typh == 'tagy')
                $nazev_selectboxu = TSTRANKA_STITKY;
            elseif($typh == 'kalendar-akci')
                $nazev_selectboxu = TSTRANKA_KALENDAR_AKCI;
            elseif($typh == 'novinky')
                $nazev_selectboxu = TSTRANKA_NOVINKY;
            elseif($typh == 'stranka-galerie')
                $nazev_selectboxu = TSTRANKA_GALERIE;
            elseif($typh == 'vyhledavani')
                $nazev_selectboxu = TSTRANKA_VYHLEDAVANI;
            elseif($typh == 'sitemap')
                $nazev_selectboxu = TSTRANKA_SITEMAP;    
                
            $form->add_selectbox($nazev_selectboxu,$name,$h,$stranky);
            
            $form->add_hidden('typ'.$idNastaveni,$typh);
                    
        }
    }
   
$form->add_submit();
    
$result = array(
    "html" => $form->get_html_code(),
    "url" => array(
        "save" => get_link_ajax('home','settings.save.form',$idModulu)
        ),
    "messages" => array()
    );

echo array2json($result);

?>