<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */
 
 
if(!$login_obj->UserPrivilege('settings_delete') || !defined("SECURITY_CMS")) exit;


$id = get_int_post('id');

if($id <= 0) exit;

$nazev = $db->get(TABLE_DISKUZE_BLACKLIST,'ip',"idPolozky=".$id); 
$log->add_log('delete','blacklist',$id,$nazev);

$db->delete(TABLE_DISKUZE_BLACKLIST, "WHERE idPolozky=".$id." AND idDomeny=".$domain->getId()." LIMIT 1");

exit;


?>