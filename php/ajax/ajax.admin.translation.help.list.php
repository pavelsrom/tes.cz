<?php

/**
 * @author Pavel Srom
 * @copyright 2010
 */


if(!isset($_GET['module']) || !defined("SECURITY_CMS")) exit;


$vyraz = trim($db->secureString($_GET['vyraz']));
$module = trim($_GET['module']);

//nastaveni limitu
$limit = "";
if(isset($_GET['iDisplayLength'])){
    $dStart = "";
    if(isset($_GET['iDisplayStart']))
        $dStart = abs(intval($_GET['iDisplayStart'])).",";
        
    $limit = " LIMIT ".$dStart.abs(intval($_GET['iDisplayLength'])); 
}


//trideni podle sloupcu
$order = "";
if(isset($_GET['iSortCol_0'])){
    $s = abs(intval($_GET['iSortCol_0']));
    if($s == 0) $order = "idNapovedy";
    elseif($s == 1) $order = "nadpis";
    else $order = "idNapovedy";
}

if($order != '') 
    $order = "ORDER BY ".$order." ".(isset($_GET['sSortDir_0'])?$_GET['sSortDir_0']:'');

//sestaveni podminky pro vyhledavani
$like = array();

if($vyraz != '')
{
    $like[] = "idNapovedy = '".intval($vyraz)."'";
    $like[] = "nadpis LIKE '%".$vyraz."%'";
        
    $like_and = array();
    $slova=explode(' ',$vyraz);
    for($f=0;$f<count($slova);$f++)
        $like_and[] = "obsahBezHtml LIKE '%".$slova[$f]."%'";
            
    $like[] = "(".implode(" AND ", $like_and).")";
    //$like[] = "MATCH (nadpis, obsahBezHtml) AGAINST ('$fulltext' IN BOOLEAN MODE)";
    
}
    

if(count($like) > 0)
    $where = "WHERE ".implode(" OR ",$like);
    else
    $where = "";


$data_pocet = $db->Query("SELECT COUNT(idNapovedy) AS celkem FROM ".TABLE_NAPOVEDA." ".$where);
$iTotal_arr = $db->getAssoc($data_pocet);
$iTotal = $iTotal_arr['celkem'];

$data = $db->Query("SELECT SQL_CALC_FOUND_ROWS idNapovedy, nadpis, obsahBezHtml AS obsah
                FROM ".TABLE_NAPOVEDA."
				".$where." 
                ".$order."
                ".$limit."
                ");


$iFilteredTotal_data = $db->Query("SELECT FOUND_ROWS()");
$iFilteredTotal_arr = $db->getRow($iFilteredTotal_data);
$iFilteredTotal = intval($iFilteredTotal_arr[0]);

$dis_delete = !$login_obj->UserPrivilege('settings_delete','superadmin');
$dis_settings = !$login_obj->UserPrivilege('settings_view');

$tr = array();
while($c = $db->getAssoc($data))
{
    $ids = $c['idNapovedy'];
    $obsah = $c['obsah'];
    $nadpis = $c['nadpis'];
    
    if($vyraz != "")
    {
    //$obsah = CropAndSaveWords($obsah,$vyraz,200);
    $obsah = MarkWord($obsah, $vyraz);
    //$nadpis = CropAndSaveWords($nadpis,$vyraz, 200);
    $nadpis = MarkWord($nadpis, $vyraz);
    }
    
    $pom = array();
    $pom[] = json_encode($ids);
    $pom[] = json_encode($nadpis);
    $pom[] = json_encode($obsah);
    
    $ikony = "";
    
    if($dis_settings)
        $ikony .=  icon_disabled('content');
        else
        $ikony .= "<a href='".get_link($module,'preklady-napovedy',0,'edit-settings',$ids)."'>".icon('content')."</a>";
    
    
    if($dis_delete)
        $ikony .= icon_disabled('delete');
        else
        $ikony .= "<a href='#' id='help".$ids."' title='".TSMAZAT."' class='delete'>".icon('delete')."</a>";
        //$ikony .= "<a href=\"javascript:confirm_url('".sprintf(TOPRAVDU_SMAZAT,$ids)."?', '".get_link($module,'preklady-napovedy',0,'delete',$ids)."') \">".icon('delete')."</a>";
        
    $pom[] = json_encode($ikony);
        
    $tr[] = "[".implode(',', $pom)."]";
    
}

echo "{";
echo '"sEcho": '.(intval($_GET['sEcho'])).', ';
echo '"iTotalRecords": '.intval($iTotal).', ';
echo '"iTotalDisplayRecords": '.($iFilteredTotal).', ';
echo '"aaData": ['.implode(',', $tr).'] }';


?>