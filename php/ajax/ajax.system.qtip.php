<?php

/**
 * @author Pavel Srom
 * @copyright 2010
 */

if(!defined("SECURITY_CMS")) exit;

$id = isset($_GET['id'])?intval($_GET['id']):0;
if($id <= 0) exit;


$data = $db->Query("SELECT ".CMS_LANG." AS obsah FROM ".TABLE_NAPOVEDA." WHERE idNapovedy=".$id." LIMIT 1");

if($db->numRows($data)<=0)
{
    echo "<p>".TNAPOVEDA_NENI_ZATIM_PRELOZENA."</p>";
    exit;
    
} 

$napoveda = $db->getAssoc($data);
if($napoveda['obsah'] != '')
    echo $napoveda['obsah'];
    else
    echo "<p>".TNAPOVEDA_NENI_ZATIM_PRELOZENA."</p>";

exit;

?>