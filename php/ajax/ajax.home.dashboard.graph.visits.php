<?php

/**
 * @author Pavel Srom
 * @copyright 2011
 */

include_once(PRE_PATH.'php/classes/class.gapi.php');

//-----------------------------------------------------------------
//cacheovani grafu
//-----------------------------------------------------------------
$posledni_zmena = mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y"));
$gmt_posledni_zmena = gmdate("D, d M Y H:i:s", $posledni_zmena)." GMT"; 

header('Content-Type: application/json');		
Header("Cache-Control: must-revalidate");
header("Pragma: private");
$offset = 60 * 60 * (24 - abs(date("H") - 24)); //den
$ExpStr = "Expires: " . gmdate("D, d M Y H:i:s", time() + $offset) . " GMT";
Header($ExpStr);
	
if(isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) && $_SERVER['HTTP_IF_MODIFIED_SINCE'] == $gmt_posledni_zmena){
	header('Last-Modified: '.$_SERVER['HTTP_IF_MODIFIED_SINCE'],true,304);
	exit;
	}

Header("Last-Modified: ".$gmt_posledni_zmena);
Header("Cache-Control: public");
//-----------------------------------------------------------------
 

if(!isset($_REQUEST['typ']) || !isset($_REQUEST['od']) || !isset($_REQUEST['do']) || !defined("SECURITY_CMS")) exit;

$start = (isset($_REQUEST['od'])?$_REQUEST['od']:'');
$end = (isset($_REQUEST['do'])?$_REQUEST['do']:'');
$type = (isset($_REQUEST['typ'])?$_REQUEST['typ']:1);
$idStranky = (isset($_REQUEST['id'])?intval($_REQUEST['id']):0);

$start = GetGADate($start);
$end = GetGADate($end);

if($type < 0 || $type > 4) return;
if($start=='' || strlen($start)!=10)
	if($end!=''){
		$start = $s = explode('-', $end);
		$start = $s[0]."-".$s[1]."-01";
		}
		else 
		$start=strftime("%Y-%m-%d", strtotime("-15 days"));
        
if($end=='' || strlen($end)!=10) 
    $end=strftime("%Y-%m-%d", time());


if(!$login_obj->UserPrivilege('statistics_view')) exit;

$error = "";

try{
	$ga = new gapi(GA_EMAIL,GA_HESLO);
	}
	catch(exception $e){
   
	exit;	
	}
	
$idProfile = GA_PROFIL;
$maxRecords = GetMaxRecords($start, $end, $type);;

$filter = null;
if($idStranky > 0)
{
    $url = $links->get_url($idStranky);
    //$url = str_replace("/pavel","",$url);
    $filter = "pagePath==".$url;
    $metrika = "pageviews";

}
else
$metrika = "visits";

//ziskani dat z google analytics
try{	
  switch($type){
	case 0: {
		$ga->requestReportData(
			$idProfile, 
			array('hour'),
			array('visits','newVisits'), 
			array('hour'), 
			$filter,
            null, 
			$start, 
			$end, 
			1, 
			$maxRecords
			);
		break;
		}
	case 1: {
		$ga->requestReportData(
			$idProfile, 
			array('date'),
			array($metrika),
			array('date'), 
			$filter, 
            null,
			$start, 
			$end, 
			1, 
			$maxRecords
			);
		break;
		}
		
	case 2:{
		$ga->requestReportData(
			$idProfile, 
			array('month', 'year'),
			array('visits'), 
			array(), 
			$filter, 
            null,
			$start, 
			$end, 
			1, 
			$maxRecords
			);
			
		break;
		}
	
	case 3:{
		$ga->requestReportData(
			$idProfile, 
			array('year'),
			array('visits'), 
			array('year'), 
			$filter, 
            null,
			$start, 
			$end, 
			1, 
			$maxRecords
			);
			
		break;
		}
		
	case 4:{
		$ga->requestReportData(
			$idProfile, 
			array('language'),
			array('visits'), 
			array(), 
			$filter, 
            null,
			$start, 
			$end, 
			1, 
			$maxRecords
			);
			
		break;
		}
	}
  }catch(exception $e){
  	/*
  	$err = $e->GetMessage();
  	if(strstr($err, 'access profile'))
  		$error = "Zadali jste špatné id profilu v nastavení statistik, prosím překontrolujte";
  		else
  		$error = "Služba Google Analytics je pravděpodobně momentálně nedostupná, zkuste to prosím později";
    echo '{"error": "'.$error.'"}';
    */
  	exit;
  }






$datay = array();
$datax = array();

$result = $ga->getResults();

//print_r($result);

$limit = 10;
$values_limit = 60;
$count = count($result);

if($count==0) exit;

$diff_value_xaxis = 1;
$diff_values = 1; 

if($type==2 || $type==1){
	$diff_value_xaxis = ceil($count/$limit);
	$diff_values = ceil($count/$values_limit);
	}
	else{
	$diff_value_xaxis = 1;
	$diff_values = 1;	
	}

	
   
    
$i=0;

//inicializace zakladnich dat grafu


$subtitle = "";
$souhrn = "";
/*
switch($type){
	case 0: {$souhrn = "hodinové souhrny"; break;}
	case 1: {$souhrn = "denní souhrny"; break;}
	case 2: {$souhrn = "měsíční souhrny"; break;}
	case 3: {$souhrn = "roční souhrny"; break;}
	case 4: {$souhrn = "souhrny podle jazyků"; break;}
	}
*/
$s = explode('-', $start);
$e = explode('-', $end);
$subtitle = strtolower(TOD)." ".intval($s[2]).".".intval($s[1]).".".$s[0]." ".strtolower(TDO)." ".intval($e[2]).".".intval($e[1]).".".$e[0];//." - ".$souhrn."";





switch($type){
	case 0:{
		foreach($result AS $v){
			$datay[] = $v->getVisits();
            
			$hour = $v->dimensions['hour'].":00";
			$datax[] = $hour;
			$targ[$i] = "#".$i;
    		$alts[$i] = THODINA.": ".$hour." - ".$str1." %d";
    		$i++;
			}
		break;
		}
	case 1:{
	  
		foreach($result AS $v){
            
			if(($i%$diff_values)==0){
			     if($idStranky == 0)
                    $datay[] = $v->getVisits();
                    else
                    $datay[] = $v->getPageviews();
                    
                $date = ConvertGADateToRealDate($v->dimensions['date']);
				$datax[] = $date;
				$targ[] = "#".$i;
    			$alts[] = TDEN.": ".$date." %d";
				}
			$i++;
			}
         
		break;
		}
	
	case 2:{
		$res = $res1 = array();

		foreach($result AS $v){
            $res[$v->dimensions['year'].$v->dimensions['month']] = $v->getVisits();
            }
        
		ksort($res);
		foreach($res AS $idx => $v){
			$datay[] = $res[$idx];
			$mesic = GetMonthName(substr($idx, 4, 2));
			$date = $mesic." ".substr($idx, 0, 4);
			$datax[] = $date;
			$targ[$i] = "#".$i;
			$alts[$i] = TMESIC.": ".$date." %d";
			$i++;
			}
		break;
		}
	case 3:{
		foreach($result AS $v){
			$datay[] = $v->getVisits();
            $year = $v->dimensions['year'];
			$datax[] = $year;
			$targ[$i] = "#".$i;
    		$alts[$i] = TROK.": ".$year." %d";
    		$i++;
			}
		break;
		}
	case 4:{
		$res = $res1 = array();
		foreach($result AS $v){
		    $zkratka_jazyka = explode('-',$v->dimensions['language']);
            $zkr = $zkratka_jazyka[0];
            if(isset($res[$zkr]))
			 $res[$zkr] += $v->getVisits();
             else
             $res[$zkr] = $v->getVisits();               
			}

		arsort($res);
		foreach($res AS $idx => $v){
			if($i == 10) break;
            $datay[] = $res[$idx];
			$datax[] = $idx;
			$targ[$i] = "#".$i;
    		$alts[$i] = TJAZYK.": ".$idx." %d";
    		$i++;
			}
		break;
		}

	
	
}


//zjisteni maximalni hodnoty grafu
$max_y = 0;
$celkem_navstev = 0;
foreach($datay AS $dy){
    $celkem_navstev += $dy;
    if($max_y < $dy) $max_y = $dy;
    }


$titulek = sprintf(TCELKEM_NAVSTEV,number_format($celkem_navstev, 0, ',', ' '));




$options = array(
    "chart" => array(
        "type" => "line",
        "height" => 250
        ),
    "title" => array(
        "text" => $titulek,
        "style" => array(
            "fontFamily" => "Arial",
            "fontSize"  => "16px",
            "fontWeight"  => "bold"
            )
        ),
    "subtitle" => array(
        "text" => $subtitle,
        "style" => array(
            "fontFamily" => "Arial",
            "fontSize"  => "14px"
            )
        ),
    "xAxis" => array(
        "categories" => $datax,
        "labels" => array(
            "rotation" => 70,
            //"step" => $diff_value_xaxis,
            "y" => 40,
            "style" => array(
                "fontFamily" => "Arial",
                "fontSize"  => "10px"
                ) 
            ),
        "tickmarkPlacement" => "on"
        ),
    "yAxis" => array(
        "title" => array(
            "text" => ""
            ),
        "labels" => array(
            "style" => array(
                "fontFamily" => "Arial",
                "fontSize"  => "10px"
                )
            )
        ),
    "tooltip" => array(
        "valueSuffix" => ""
        ),
    "legend" => array(
        "layout" => "horizontal",
        "align" => "top",
        "verticalAlign" => "top",
        "x" => -10,
        "y" => 50,
        "borderWidth" => 0,
        "style" => array(
                "fontFamily" => "Arial",
                "fontSize"  => "10px"
                )        
        ),
    "series" => array(
        array(
            "name" => TNAVSTEVNOST,
            "data" => $datay
            )
        ),
        
    "labels" => array(
        "style" => array(
                "fontFamily" => "Arial",
                "fontSize"  => "10px"
                )
        )                
        
    );
    
echo json_encode($options);
exit;

?>