<?php

/**
 * @author Pavel Srom
 * @copyright 2014
 */


if(!defined("SECURITY_CMS")) exit;

if(WEB_LANG == "" || !$login_obj->UserPrivilege('settings_view')) exit;


$data = $db->query("SELECT idPrekladu,konstanta,".WEB_LANG." AS preklad FROM ".TABLE_PREKLADY." ORDER BY idPrekladu");


$table = new Table("","ine_table");
$table->tr_head()
    ->add(TID)
    ->add(TKONSTANTA)
    ->add(TPREKLADY);

while($p = $db->getAssoc($data))
{
    $table->tr()
          ->add( $p['idPrekladu'] )
          ->add( $p['konstanta'] )
          ->add("<input type='text' name='p".$p['idPrekladu']."' value='".$p['preklad']."' class='input ui-widget-content ui-corner-all'/>");
}



$html = $table->get_html();
$html = "<form method='post' action=''>".$html."</form>";

$result = array(
    "html" => $html,
    "url" => array(
        "save" => get_link_ajax('home','translation.save.form',$idModulu)
        )
    );
    
echo array2json($result);


?>