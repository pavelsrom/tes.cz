<?php

/**
 * @author Pavel Srom
 * @copyright 2010
 */

//ajaxovy skript na odhlaseni kontaktu z odberu newsletteru

if(!isset($_GET['module']) || !defined("SECURITY_CMS")) exit;

$vyraz = trim($db->secureString($_GET['vyraz']));
$module = trim($_GET['module']);

//nastaveni limitu
$limit = "";
if(isset($_GET['iDisplayLength'])){
    $dStart = "";
    if(isset($_GET['iDisplayStart']))
        $dStart = abs(intval($_GET['iDisplayStart'])).",";
        
    $limit = " LIMIT ".$dStart.abs(intval($_GET['iDisplayLength'])); 
}


//trideni podle sloupcu
$order = "";
if(isset($_GET['iSortCol_0'])){
    $s = abs(intval($_GET['iSortCol_0']));
    if($s == 0) $order = "idPanelu";
    elseif($s == 1) $order = "nazev";
    elseif($s == 2) $order = "systemovyNazev";
    elseif($s == 3) $order = "zobrazit";
    else $order = "idPanelu";
}

if($order != '') 
    $order = "ORDER BY ".$order." ".(isset($_GET['sSortDir_0'])?$_GET['sSortDir_0']:'');

//echo $order;

//sestaveni podminky pro vyhledavani
$like = array();
if($vyraz != '')
{
    $like[] = "idPanelu LIKE '".$vyraz."%'";
    $like[] = "nazev LIKE '%".$vyraz."%'";
}
    


$where = "WHERE idDomeny=".$domain->getId().((count($like) > 0) ? " AND (".implode(" OR ",$like).")" : "");

$data_pocet = $db->Query("SELECT COUNT(idPanelu) AS celkem FROM ".TABLE_PANELY." ".$where);
$iTotal_arr = $db->getAssoc($data_pocet);
$iTotal = $iTotal_arr['celkem'];


        
$data = $db->Query("SELECT SQL_CALC_FOUND_ROWS * FROM ".TABLE_PANELY." AS s
				".$where."
				GROUP BY idPanelu
				".$order."
                ".$limit."
				");


$iFilteredTotal_data = $db->Query("SELECT FOUND_ROWS()");
$iFilteredTotal_arr = $db->getRow($iFilteredTotal_data);
$iFilteredTotal = intval($iFilteredTotal_arr[0]);


$dis_content = !$login_obj->UserPrivilege('settings_view','superadmin');
$dis_delete = !$login_obj->UserPrivilege('settings_delete','superadmin');

$tr = array();
while($c = $db->getAssoc($data))
{
    $ids = $c['idPanelu'];
    $pom = array();
    $pom[] = json_encode($ids);
    if(defined($c['nazev']))
        $pom[] = json_encode(secureString(constant($c['nazev'])));
        else
        $pom[] = json_encode($c['nazev']." (".TNEDEFINOVANO.")");
        
    $pom[] = json_encode(secureString($c['systemovyNazev']));
    $pom[] = json_encode(ano_ne($c['zobrazit']));
    
    $ikony = "";
    
    if($dis_content)
        $ikony .= icon_disabled('setting');
        else
        $ikony .= "<a href='".get_link($module,'panely_nastaveni',0,'edit-settings',$ids)."'>".icon('setting')."</a>";
    
    if($dis_delete)
        $ikony .= icon_disabled('delete');
        else
        $ikony .= "<a href='#' class='delete' id='delete".$ids."'>".icon('delete')."</a>";
        
    $pom[] = json_encode($ikony);
        
    $tr[] = "[".implode(',', $pom)."]";
    
}

echo "{";
echo '"sEcho": '.(intval($_GET['sEcho'])).', ';
echo '"iTotalRecords": '.$iTotal.', ';
echo '"iTotalDisplayRecords": '.($iFilteredTotal).', ';
echo '"aaData": ['.implode(',', $tr).'] }';


?>