<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

//ajaxovy skript na odhlaseni kontaktu z odberu newsletteru

if(!isset($_GET['module']) || !defined("SECURITY_CMS")) exit;

$vyraz = trim($db->secureString($_GET['vyraz']));
$stav = isset($_GET['stav']) ? intval($_GET['stav']) : 0;
$skupina = isset($_GET['skupina']) ? intval($_GET['skupina']) : 0;
$module = trim($_GET['module']);

//nastaveni limitu
$limit = "";
if(isset($_GET['iDisplayLength'])){
    $dStart = "";
    if(isset($_GET['iDisplayStart']))
        $dStart = abs(intval($_GET['iDisplayStart'])).",";
        
    $limit = " LIMIT ".$dStart.abs(intval($_GET['iDisplayLength'])); 
}


//trideni podle sloupcu
$order = "";
if(isset($_GET['iSortCol_0'])){
    $s = abs(intval($_GET['iSortCol_0']));
    if($s == 0) $order = "p.idPrijemce";
    elseif($s == 1) $order = "CONCAT(p.jmeno, p.prijmeni)";
    elseif($s == 2) $order = "p.email";
    elseif($s == 3) $order = "stav";
    else $order = "p.idPrijemce";
}

if($order != '') 
    $order = "ORDER BY ".$order." ".(isset($_GET['sSortDir_0'])?$_GET['sSortDir_0']:'');

//echo $order;

//sestaveni podminky pro vyhledavani

$like = array();
if($vyraz != '')
{
    $like[] = "(p.idPrijemce LIKE '".$vyraz."%' OR p.email LIKE '%".$vyraz."%' OR p.jmeno LIKE '%".$vyraz."%' OR p.prijmeni LIKE '%".$vyraz."%')";
}
if($stav != 3)
{
    $like[] = "stav = '".$stav."'";
}
if($skupina > 0)
{
    $like[] = "r.idSkupiny=".$skupina; 
}




$where = "WHERE p.idDomeny=".$domain->getId().((count($like) > 0) ? " AND (".implode(" AND ",$like).")" : "");

$data_pocet = $db->Query("SELECT COUNT(p.idPrijemce) AS celkem 
    FROM ".TABLE_EMAILY_PRIJEMCI." AS p 
    LEFT JOIN ".TABLE_EMAILY_SKUPINY_PRIJEMCI." AS r ON p.idPrijemce = r.idPrijemce 
    ".$where."
    GROUP BY p.idPrijemce
    ");   
$iTotal_arr = $db->getAssoc($data_pocet);
$iTotal = intval($iTotal_arr['celkem']);


$data = $db->Query("SELECT SQL_CALC_FOUND_ROWS p.*
    FROM ".TABLE_EMAILY_PRIJEMCI." AS p 
    LEFT JOIN ".TABLE_EMAILY_SKUPINY_PRIJEMCI." AS r ON p.idPrijemce = r.idPrijemce 
    ".$where."
    GROUP BY p.idPrijemce
    ".$order."
    ".$limit."
    ");
 

$iFilteredTotal_data = $db->Query("SELECT FOUND_ROWS()");
$iFilteredTotal_arr = $db->getRow($iFilteredTotal_data);
$iFilteredTotal = intval($iFilteredTotal_arr[0]);


$dis_content = !$login_obj->UserPrivilege('content_view');
$dis_delete = !$login_obj->UserPrivilege('settings_delete');
$dis_settings = !$login_obj->UserPrivilege('settings_view');

$tr = array();
$stavy = array(0 => TNEPOTVRZEN, 1 => TAKTIVNI, 2 => TODHLASEN);
while($c = $db->getAssoc($data))
{
    $ids = $c['idPrijemce'];
    
    if($c['stav'] == 0)
        $s = 2;
    elseif($c['stav'] == 1)
        $s = 1;
    else
        $s = 0;
    
    
    $pom = array();
    $pom[] = json_encode($ids);
    $pom[] = json_encode(secureString(trim($c['jmeno']." ".$c['prijmeni'])));
    $pom[] = json_encode(secureString($c['email']));
    $pom[] = json_encode(strtoupper($lang->get($c['idJazyka'],'jazyk')));
    $pom[] = json_encode("<span class='stav".$s."'>".$stavy[$c['stav']]."</span>");
        
    $ikony = "";
    
    if($dis_settings)
        $ikony .=  icon_disabled('setting');
        else
        $ikony .= "<a href='".get_link($module,'prijemci',0,'edit-settings',$ids)."'>".icon('setting')."</a>";
    
    if($dis_delete)
        $ikony .= icon_disabled('delete');
        else
        $ikony .= "<a href='#' id='delete".$ids."' class='delete'>".icon('delete')."</a>";
    
        
    $pom[] = json_encode($ikony);
        
    $tr[] = "[".implode(',', $pom)."]";
    
}

echo "{";
echo '"sEcho": '.(intval($_GET['sEcho'])).', ';
echo '"iTotalRecords": '.$iTotal.', ';
echo '"iTotalDisplayRecords": '.($iFilteredTotal).', ';
echo '"aaData": ['.implode(',', $tr).'] }';


?>