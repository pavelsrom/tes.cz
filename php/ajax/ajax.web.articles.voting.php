<?php

/**
 * @author Pavel Srom
 * @copyright 2011
 */

//sleep(5);

if(!isset($_POST['rate']) || !isset($_POST['idClanku'])) exit;


$idClanku = intval($_POST['idClanku']);
$hlasu = intval($_POST['rate']);


if($idClanku<=0 || $hlasu<=0) exit;


$db->Query("UPDATE ".TABLE_STRANKY." SET pocetHlasu = (pocetHlasu + ".$hlasu."), pocetHlasovani = pocetHlasovani + 1 WHERE idStranky=".$idClanku." AND typ='clanek' AND idDomeny=".$domain->getId()." AND zobrazit=1 LIMIT 1");

$data = $db->Query("SELECT pocetHlasu, pocetHlasovani FROM ".TABLE_STRANKY." WHERE idStranky=".$idClanku." AND typ='clanek' AND idDomeny=".$domain->getId()." AND zobrazit=1 LIMIT 1");

if($db->numRows($data)==0) exit;

$d = $db->getAssoc($data);

$result = array();
$result['prumer'] = round($d['pocetHlasu']/$d['pocetHlasovani'],1);
$result['pocet_hlasu'] = $d['pocetHlasu'];
$result['pocet_hlasovani'] = $d['pocetHlasovani'];
for($i=1;$i<=10;$i++)
    if($result['prumer'] >= $i && $result['prumer'] < ($i+1)){
        $result['skupina'] = $i;
        break;
        }

setcookie('hlasovani-clanek-'.$idClanku,"1",time()+(3600*24*30),'/');

echo json_encode($result)


?>