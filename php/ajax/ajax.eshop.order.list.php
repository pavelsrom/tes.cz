<?php

/**
 * @author Pavel Srom
 * @copyright 2010
 */

//ajaxovy skript na odhlaseni kontaktu z odberu newsletteru

if(!isset($_GET['module']) || !defined("SECURITY_CMS")) exit;


$stav = intval($_GET['stav']);
$vyraz = trim($db->secureString($_GET['vyraz']));
$datum_od = isset($_GET['datum_od']) ? $db->secureString($_GET['datum_od']) : "";
$datum_do = isset($_GET['datum_do']) ? $db->secureString($_GET['datum_do']) : "";

$datum_od = $datum_od != "" ? GetGADate($datum_od) : "";
$datum_do = $datum_do != "" ? GetGADate($datum_do) : "";

$module = trim($_GET['module']);

//nastaveni limitu
$limit = "";
if(isset($_GET['iDisplayLength'])){
    $dStart = "";
    if(isset($_GET['iDisplayStart']))
        $dStart = abs(intval($_GET['iDisplayStart'])).",";
        
    $limit = " LIMIT ".$dStart.abs(intval($_GET['iDisplayLength'])); 
}


//trideni podle sloupcu
$order = "";
if(isset($_GET['iSortCol_0'])){
    $s = abs(intval($_GET['iSortCol_0']));
    if($s == 0) $order = "o.cislo_faktury";
    //elseif($s == 1) $order = "o.id_platby";
    elseif($s == 3) $order = "o.celkem_s_dph";
    elseif($s == 4) $order = "o.datum";
    elseif($s == 5) $order = "o.zaplaceno";
    else $order = "o.cislo_faktury";
}

if($order != '') 
    $order = "ORDER BY ".$order." ".(isset($_GET['sSortDir_0'])?$_GET['sSortDir_0']:'');


//sestaveni podminky pro vyhledavani
$where = "";
$like = array();

if($vyraz != '')
{
    if(ctype_digit($vyraz))
    {
        $like[] = "o.cislo_faktury LIKE '".$vyraz."%'";
        $like[] = "o.id_platby LIKE '%".$vyraz."%'";
        $like[] = "o.id = '".$vyraz."'";
    }
    else
        $like[] = "o.jmeno_uzivatele LIKE '%".$vyraz."%'";
}

if($stav > 0)
{
    $where .= "AND o.id_stav = ".$stav;
}
if($datum_od != '')
{
    $where .= "AND o.datum >= '".$datum_od." 00:00:01'";
}   
if($datum_do != '')
{
    $where .= "AND o.datum <= '".$datum_do." 23:59:59'";
}

$where = " WHERE 1=1 ".$where." ".(count($like) > 0 ? "AND (".implode(" OR ",$like).")" : "");

$data_pocet = $db->Query("SELECT COUNT(id) AS celkem FROM ".TABLE_ESHOP_OBJEDNAVKY." AS o ".$where);
$iTotal_arr = $db->getAssoc($data_pocet);
$iTotal = $iTotal_arr['celkem'];

        
$data = $db->Query("SELECT SQL_CALC_FOUND_ROWS o.*, COUNT(op.id) AS pocet, s.nazev AS stav, u.hash,
                    DATE_FORMAT(datum,'%d.%m.%Y %H:%i') AS datum, TRIM(CONCAT(o.jmeno, ' ', o.prijmeni)) AS jmeno_uzivatele,
                    IF(zaplaceno IS NULL,' - ', DATE_FORMAT(zaplaceno,'%d.%m.%Y %H:%i')) AS zaplaceno
				FROM ".TABLE_ESHOP_OBJEDNAVKY." AS o
                LEFT JOIN ".TABLE_ESHOP_OBJEDNAVKY_POLOZKY." AS op ON o.id = op.id_objednavka
                LEFT JOIN ".TABLE_ESHOP_OBJEDNAVKY_STAVY." AS s ON s.id = o.id_stav
                LEFT JOIN ".TABLE_UZIVATELE." AS u ON u.idUzivatele = o.id_uzivatel
                ".$where."
				GROUP BY o.id
				".$order."
                ".$limit."
				");

$iFilteredTotal_data = $db->Query("SELECT FOUND_ROWS()");
$iFilteredTotal_arr = $db->getRow($iFilteredTotal_data);
$iFilteredTotal = intval($iFilteredTotal_arr[0]);

$dis_content = !$login_obj->UserPrivilege('content_view');




$tr = array();
while($c = $db->getAssoc($data))
{

    $ids = $c['id'];
        
    
    $pom = array();
    //$pom[] = json_encode($ids);
    $pom[] = json_encode($c['cislo_faktury']);
    //$pom[] = json_encode($c['id_platby']);
    $pom[] = json_encode($c['jmeno_uzivatele']);
    $pom[] = json_encode(price($c['celkem_s_dph']));
    $pom[] = json_encode($c['datum']);
    $pom[] = json_encode($c['zaplaceno']);
    $pom[] = json_encode($c['poznamka']);
    $pom[] = json_encode($c['stav']);
    
    
    $ikony = "";
    //$ikony = "<a href='".urlPage($c['url'],$ids,$c['jazyk'])."' target='_blank'>".icon('link')."</a>";
    
    /*
    if($dis_content)
        $ikony .= icon_disabled('content');
        else
        $ikony .= "<a href='".get_link($module,'eshop',0,'edit-content',$ids)."'>".icon('content')."</a>";
    */
    
    $url_faktury = PROTOKOL.$_SERVER['HTTP_HOST'].RELATIVE_PATH."invoice/".$c['cislo_faktury']."/".$c['hash'];
    
    if($c['id_stav'] == 2)
        $ikony .= "<a href='".$url_faktury."'>".icon('link')."</a>";
    
    $ikony .= "<a href='".get_link($module,'objednavky',0,'edit-content',$ids)."'>".icon('content')."</a>";
    
    $pom[] = json_encode($ikony);
        
    $tr[] = "[".implode(',', $pom)."]";
    
}

echo "{";
echo '"sEcho": '.(intval($_GET['sEcho'])).', ';
echo '"iTotalRecords": '.$iTotal.', ';
echo '"iTotalDisplayRecords": '.($iFilteredTotal).', ';
echo '"aaData": ['.implode(',', $tr).'] }';


?>