<?php

/**
 * @author Pavel Srom
 * @copyright 2010
 */


if(!defined("SECURITY_CMS")) exit;

function typ_nazev($typ){
    if($typ=='stranka') return TSTRANKA;
    elseif($typ=='clanek') return TCLANEK;
    elseif($typ=='clanky') return TSTRANKA_S_CLANKY;
    elseif($typ=='novinka') return TNOVINKA;
    elseif($typ=='novinky') return TSTRANKA_S_NOVINKAMI;
    elseif($typ=='archiv-clanku') return TARCHIV_CLANKU;
    elseif($typ=='sitemap') return TMAPA_WEBU;
    elseif($typ=='uvodni') return TUVODNI_STRANKA;
    elseif($typ=='akce') return TAKCE; 
    elseif($typ=='kalendar-typ') return TTYP_AKCE; 
    elseif($typ=='kalendar-akci') return TKALENDAR_AKCI; 
    elseif($typ=='tagy') return TTAGY;
    elseif($typ=='tag') return TTAG;  
    elseif($typ=='galerie') return TGALERIE; 
    elseif($typ=='stranka-galerie') return TSTRANKA_S_GALERII;
    elseif($typ=='galerie-slozka') return TSLOZKA_GALERIE;
    elseif($typ=='galerie-fotka') return TFOTKA;  
    elseif($typ=='404') return TCHYBOVA_STRANKA;    
    else return "";
}


// You can't simply echo everything right away because we need to set some headers first!
$output = ''; // Here we buffer the JavaScript code we want to send to the browser.
$delimiter = "\n"; // for eye candy... code gets new lines

//$output .= "<script language='javascript' type='javascript'>";
$output .= 'var tinyMCELinkList = new Array(';



// Since TinyMCE3.x you need absolute image paths in the list...

$data = $db->Query("SELECT idStranky, url, s.nazev, typ, j.jazyk
    FROM ".TABLE_STRANKY." AS s
    LEFT JOIN ".TABLE_JAZYKY." AS j ON s.idJazyka = j.idJazyka 
    WHERE idDomeny=".$idDomain." 
        AND zobrazit=1 
        AND typ NOT IN ('404','prazdna-stranka','galerie-fotka')
        AND s.idJazyka=".WEB_LANG_ID." 
    ORDER BY typ ");

while($page = $db->getAssoc($data)){
	
	$output .= $delimiter
                    . '["'
                    . typ_nazev($page['typ'])." - ".$page['nazev']
                    . '", "'
                    . UrlPage($page['url'], $page['idStranky'],$page['jazyk'])
                    . '"],';
            }


$output = substr($output, 0, -1); // remove last comma from array item list (breaks some browsers)
$output .= $delimiter;

// Finish code: end of array definition. Now we have the JavaScript code ready!
$output .= ');';

//$output .= "</script>";

// Make output a real JavaScript file!
header('Content-type: text/javascript;charset=utf-8'); // browser will now recognize the file as a valid JS file

// prevent browser from caching
header('pragma: no-cache');
header('expires: 0'); // i.e. contents have already expired

// Now we can send data to the browser because all headers have been set!
echo $output;


?>