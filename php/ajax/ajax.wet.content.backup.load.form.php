<?php

/**
 * @author Pavel Srom
 * @copyright 2014
 */

$idStranky = get_int_request('id_page');

$data = $db->query("SELECT idZalohy AS id, perexBezHtml AS perex, obsah, popis1BezHtml AS popis1, popis2BezHtml AS popis2,popis3BezHtml AS popis3,popis4BezHtml AS popis4,popis5BezHtml AS popis5,DATE_FORMAT(datum,'%d.%m.%Y %H:%i:%s') AS datum 
    FROM ".TABLE_ZALOHY." 
    WHERE idDomeny=".$domain->getId()." 
        AND idObjektu=".$idStranky." 
        AND typ='stranka'
    ORDER BY datum DESC
    LIMIT 15
    ");
    
$contents = array();
$zalohy = array();

while($c = $db->getAssoc($data))
{
    $contents[] = $c;
}


$html = '<div id="ine_zalohy">';


if(count($contents) > 0)
{
    $html .= '<div class="ine_sl1">';
    $html .= '<h3>'.TPOSLEDNI_ZALOHY.'</h3>';
    $html .= '<ul>';
    foreach($contents AS $c)
    {
        $html .= '<li><a href="#ine_tabs_'.$c['id'].'" class="ine_datum_zalohy">'.TZALOHA_ZE_DNE." ".$c['datum'].'</a></li>';
        $z = '<div id="ine_tabs_'.$c['id'].'" class="ine_zaloha">';
        
        if($c['perex'] != '')
        {
            $z.= '<strong>'.$c['perex'].'</strong>';
            $z.= '<br /><br />';
        }
        $z.= $c['obsah'];
        $z.= '</div>';
        
        $zalohy[] = $z;
        
    }
    $html .= '</ul>';
    
    $html .= '</div>'; 
    
}

$html .= '<div class="ine_sl2">';
$html .= '<h3>'.TOBSAH_ZALOHY.'</h3>';
$html .= implode("",$zalohy);
$html .= '<a href="#" id="vydat_zalohu" class="tool_button save_button">'.TNAHRADIT_OBSAH_ZALOHOU.'</a>';
$html .= '</div>';

$html .= '<div class="ine_cleaner"></div>';
$html .= '</div>';

$result = array(
    "html" => $html,
    "url" => array(
        "save" => get_link_ajax('wet','content.backup.save')
        )
);

echo array2json($result);



?>
