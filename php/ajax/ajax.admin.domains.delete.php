<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

//sleep(5);

if(!defined("SECURITY_CMS")) exit;
if(!$login_obj->UserPrivilege('settings_delete') && !$login_obj->UserPrivilege('superadmin')) exit;

$id = get_int_post('id');

if($id <= 0) exit;

$db->delete(TABLE_DOMENY, "WHERE idDomeny=".$id);
$db->delete(TABLE_UZIVATELE_DOMENY, "WHERE idDomeny=".$id);
$db->delete(TABLE_DOMENY_JAZYKY, "WHERE idDomeny=".$id);
$db->delete(TABLE_DOMENY_MODULY, "WHERE idDomeny=".$id);
$db->delete(TABLE_LOG, "WHERE idDomeny=".$id);
$db->delete(TABLE_ODKAZY, "WHERE idDomeny=".$id);
$db->delete(TABLE_ZALOHY, "WHERE idDomeny=".$id);				

			
//vymazani anket vcetne polozek anket
$data = $db->Query("SELECT * FROM ".TABLE_ANKETY." WHERE idDomeny=".$id);
while($ankety = $db->getAssoc($data)){
    $db->delete(TABLE_ANKETY_OTAZKY,"WHERE idAnkety=".$ankety['idAnkety']);
    $db->delete(TABLE_ANKETY_IP, "WHERE idAnkety=".$ankety['idAnkety']);
    }
$db->delete(TABLE_ANKETY, " WHERE idDomeny=".$id);
					
				
//vymazani stranek vcetne bloku stranek
$data = $db->Query("SELECT * FROM ".TABLE_STRANKY." WHERE idDomeny=".$id);
while($stranky = $db->getAssoc($data)){
    $db->delete(TABLE_STRANKY_EDITORI, "WHERE idStranky=".$stranky['idStranky']);
    $db->delete(TABLE_STRANKY_PRIVATNI_UZIVATELE," WHERE idStranky=".$stranky['idStranky']);
    $db->delete(TABLE_SOUVISEJICI_CLANKY," WHERE idClanku=".$stranky['idStranky']);
    $db->delete(TABLE_DISKUZE_POLOZKY,"WHERE idStranky=".$stranky['idStranky']);
    }

$db->delete(TABLE_STRANKY,"WHERE idDomeny=".$id);

$db->delete(TABLE_VYHLEDAVANI_VYRAZY,"WHERE idDomeny=".$id);
							
//vymazani emailovych kampani
$data = $db->Query("SELECT * FROM ".TABLE_EMAILY_KAMPANE." WHERE idDomeny=".$id);
while($kampan = $db->getAssoc($data)){
    $db->delete(TABLE_EMAILY_PRILOHY,"WHERE idKampane=".$kampan['idKampane']);
    $db->delete(TABLE_EMAILY_SKUPINY_KAMPANE,"WHERE idKampane=".$kampan['idKampane']);
    $db->delete(TABLE_EMAILY_STAVY,"WHERE idKampane=".$kampan['idKampane']);
    $db->delete(TABLE_EMAILY_SABLONY_NASTAVENI,"WHERE idKampane=".$kampan['idKampane']);
    }

$db->delete(TABLE_EMAILY_KAMPANE,"WHERE idDomeny=".$id);
$db->delete(TABLE_HTML_SABLONY,"WHERE idDomeny=".$id);
$db->delete(TABLE_EMAILY_SKUPINY,"WHERE idDomeny=".$id);
$db->delete(TABLE_EMAILY_IMPORT,"WHERE idDomeny=".$id);
            
//vymazani prijemcu emailovych kampani
$data = $db->Query("SELECT * FROM ".TABLE_EMAILY_PRIJEMCI." WHERE idDomeny=".$id);
while($prijemce = $db->getAssoc($data))
    $db->delete(TABLE_EMAILY_SKUPINY_PRIJEMCI,"WHERE idPrijemce=".$prijemce['idPrijemce']);
						
$db->delete(TABLE_EMAILY_PRIJEMCI,"WHERE idDomeny=".$id);	
				
//vymazani diskuzi
$db->delete(TABLE_DISKUZE_BLACKLIST,"WHERE idDomeny=".$id);
$db->delete(TABLE_DISKUZE_FILTR,"WHERE idDomeny=".$id);

					
//vymazani editboxu
$data = $db->Query("SELECT idEditboxu FROM ".TABLE_EDITBOXY." WHERE idDomeny=".$id);
while($editbox = $db->getAssoc($data))
    $db->delete(TABLE_EDITBOXY_POLOZKY,"WHERE idEditboxu=".$editbox['idEditboxu']);

$db->delete(TABLE_EDITBOXY,"WHERE idDomeny=".$id);
					
//vymazani panelu	
$db->delete(TABLE_PANELY, "WHERE idDomeny=".$id);
$data = $db->Query("SELECT * FROM ".TABLE_PANELY_SABLONY." WHERE idDomeny=".$id);
while($panely = $db->getAssoc($data))
    $db->delete(TABLE_PANELY_POLOZKY,"WHERE idBloku=".$panely['idBloku']);

$db->delete(TABLE_PANELY_SABLONY,"WHERE idDomeny=".$id);			
				
$db->delete(TABLE_NASTAVENI_DOMENY,"WHERE idDomeny=".$id);
$db->delete(TABLE_FACEBOOK,"WHERE idDomeny=".$id);
$db->delete(TABLE_FACEBOOK_PLUGINY,"WHERE idDomeny=".$id);

$db->delete(TABLE_LOG,"WHERE idDomeny=".$id);
$db->delete(TABLE_ZALOHY,"WHERE idDomeny=".$id);             
$db->delete(TABLE_ODKAZY,"WHERE idDomeny=".$id); 
                               
exit;

?>