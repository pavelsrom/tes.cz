<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

mb_internal_encoding("UTF-8");
session_start();

DEFINE('PRE_PATH','../../');
DEFINE('SECURITY',1);
//DEFINE('LANG',isset($_GET['lang']) ? $_GET['lang'] : "cz"); //cms.php

require_once('../config/config.db.php');
include_once('../config/config.paths.php');
require_once('../config/config.web.php');
include_once('../config/config.others.php');

include_once('../functions/functions.system.php');
include_once('../functions/functions.web.php');
include_once('../functions/functions.input.formats.php');
require_once('../functions/functions.error.handlers.php');

require_once('../classes/class.db.php');
require_once('../classes/class.domain.web.php');
include_once('../classes/class.langs.web.php');
include_once('../classes/class.url.web.php');

$db = new C_Db();	
$db->Connect();

$domain = new C_Domains();
$idDomain = $domain->getId();
if($idDomain == 0) exit;

$lang = new C_WebLangs(LANG, $domain->getId());
define('WEB_LANG_ID',$lang->get_id());
define('DOMAIN_ID',$domain->getId());

$links = new C_Url();

GetActiveModules();
loadSettings(DOMAIN_ID);
loadTranslations(LANG, true);

error_settings();
    
$action = isset($_REQUEST['action']) ? $_REQUEST['action'] : "";

if(file_exists("ajax.web.".$action.".php"))
    include("ajax.web.".$action.".php");    
elseif(file_exists("ajax.system.".$action.".php"))
    include("ajax.system.".$action.".php"); 

?>