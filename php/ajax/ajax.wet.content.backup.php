<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */


$id = isset($_GET['id'])?intval($_GET['id']):0;
if($id <= 0) exit;


$data = $db->Query("SELECT idObjektu,perex, obsah, typ, popis1, popis2,popis3,popis4,popis5, DATE_FORMAT(datum,'%d.%m.%Y %H:%i') AS datum 
    FROM ".TABLE_ZALOHY." 
    WHERE idZalohy=".$id." 
        AND idDomeny=".$domain->getId()." 
    LIMIT 1");

if($db->numRows($data)<=0) exit;

$zaloha = $db->getAssoc($data);

$typ_stranky = "";
if($zaloha['typ'] == 'stranka')
{
    $s = $db->get(TABLE_STRANKY,array('nazev','typ'),'idDomeny='.$domain->getId()." AND idStranky=".$zaloha['idObjektu']);
    $typ_stranky = $s->typ;
    $nadpis = $s->nazev;    
}
elseif($zaloha['typ'] == 'newsletter')
    $nadpis = $db->get(TABLE_EMAILY_KAMPANE,'nazev','idDomeny='.$domain->getId()." AND idKampane=".$zaloha['idObjektu']);
elseif($zaloha['typ'] == 'editbox')
    $nadpis = $db->get(TABLE_EDITBOXY,'nazev','idDomeny='.$domain->getId()." AND idEditboxu=".$zaloha['idObjektu']);    


?>

<div class="hlavicka">
<h3><?php echo $nadpis; ?></h3>
<p class="datum"><?php echo TDATUM_ULOZENI.": ".$zaloha['datum'];?></p>
</div>


<?php if(trim(strip_tags($zaloha['perex'])) != "" )
    {
        
        ?>
<div class="hlavicka-podnadpis">
<h4><?php echo TPEREX;?></h4>
</div>

<div class="perex" id="perex_<?php echo $id;?>">
<?php echo $zaloha['perex'];?>
</div>

<div class="cleaner"></div>
<a href="#" class="vlozit_perex_do_editoru tlacitko" id="perex_a<?php echo $id;?>"><span class="fa fa-check"></span> <?php echo TVLOZIT_PEREX_DO_EDITORU;?></a>
<div class="cleaner"></div>
<br />

<?php }  ?>

<div class="hlavicka-podnadpis">

<h4><?php echo TOBSAH;?></h4>

</div>
<div class="obsah" id="obsah_<?php echo $id;?>">
<?php echo $zaloha['obsah'];?>
</div>

<div class="cleaner"></div>
<a href="#" class="vlozit_obsah_do_editoru tlacitko" id="obsah_a<?php echo $id;?>"><span class="fa fa-check"></span> <?php echo TVLOZIT_OBSAH_DO_EDITORU;?></a>
<div class="cleaner"></div>

<?php

if($typ_stranky == 'produkt')
{
    for($i = 1; $i <= 5; $i++)
    {
        echo '<div class="hlavicka-podnadpis"><h4>'.constant('TPRODUKT_ZALOZKA'.$i).'</h4></div>';
        echo '<div class="popis'.$i.'" id="popis'.$i.'_'.$id.'">'.$zaloha['popis'.$i].'</div>';
        echo '<div class="cleaner"></div>
            <a href="#" class="vlozit_popis'.$i.'_do_editoru tlacitko" id="obsah_a'.$id.'">'.TVLOZIT_OBSAH_DO_EDITORU.'</a>
            <div class="cleaner"></div>';
    }
    
    
    
}



?>



