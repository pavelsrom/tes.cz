<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined("SECURITY_CMS")) exit;
if(!$login_obj->UserPrivilege("admin") && !$login_obj->UserPrivilege("superadmin")) exit;


include(PRE_PATH."php/classes/class.mysql.backup.php");

$backup = new MySQL_Backup();
$backup->drop_tables = true;
$backup->comments = false;
$backup->tables = array();
$backup->disallow_tables = array(TABLE_ZALOHY_DB);
$backup->table_prefix = TABLE_PREFIX;

$task = MSB_SAVE;
$use_gzip = POUZIT_GZ_KOMPRESI;
$filename = 'db-backup-'.substr(md5(uniqid().time()),0,10).".sql.gz"; 
$file_path = PRE_PATH.DIRNAME_DB_BACKUP.$filename;

$output = "";
if (!$backup->Execute($task, $file_path, $use_gzip))
	 $output = $backup->error; 

//ulozeni do db
if($output == "" && file_exists($file_path))
{
    $insert = array(
        "soubor" => $filename,
        "velikost" => filesize($file_path),
        "datum" => "now",
        "jmenoUzivatele" => $login_obj->getName()
        );
    
    $db->insert(TABLE_ZALOHY_DB, $insert);
}

echo $output;
    
exit;


?>