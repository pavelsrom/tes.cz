<?php

/**
 * @author Pavel Srom
 * @copyright 2010
 */


if(!isset($_POST['id_form']) || !isset($_POST['ul']) || !defined("SECURITY_CMS")) exit;

$idFormulare = intval($_POST['id_form']);
$nove_polozky = (array)$_POST['ul'];


//print_r($_POST);exit;

if($idFormulare<=0) exit;


if(!$login_obj->UserPrivilege('content_edit') || !$login_obj->UserPrivilege('content_delete')) exit;

//zjisteni starych (dosavadnich polozek)
$data = $db->Query("SELECT p.idPolozky AS id 
    FROM ".TABLE_FORMULARE." AS f
    LEFT JOIN ".TABLE_FORMULARE_POLOZKY." AS p ON f.idFormulare=p.idFormulare
    WHERE f.idDomeny=".$idDomain."
        AND f.idFormulare=".$idFormulare." 
   ");
   
$stare_polozky = array();
while($pol = $db->getAssoc($data))
    $stare_polozky[] = $pol['id'];
    

//ulozeni polozek do databaze podle priority

$priorita = 0;

$nove = array();

if(count($nove_polozky)>0){
    
    foreach($nove_polozky AS $pol){
        $id = intval($pol['id']);
        
        if($id>0) $nove[] = $id;
        
        $typ = $pol['class'];
        $povinna = $pol['required'] == 'checked' ? 1 : 0;
        
        
        $format="text";
        $hodnoty = "";
        $nazev = "";
        
        if($typ=='input_text') {
            $typ = "text";
            $nazev = $pol['values'];
            }
        elseif($typ=='textarea'){
            $nazev = $pol['values'];
            }
        elseif($typ=='select') {
            $typ="selectbox";
            }
        elseif($typ=='popis'){
            $typ="description";
            $hodnoty = $pol['values'];
            }
        elseif($typ=='datum'){
            $typ="text";
            $format="datum";
            $nazev = $pol['values'];
            }    
        elseif($typ=='cas'){
            $typ="text";
            $format="cas";
            $nazev = $pol['values'];
            }  
        elseif($typ=='icq'){
            $typ="text";
            $format="icq";
            $nazev = $pol['values'];
            }
        elseif($typ=='email'){
            $typ="text";
            $format="email";
            $nazev = $pol['values'];
            }    
        elseif($typ=='url'){
            $typ="text";
            $format="url";
            $nazev = $pol['values'];
            } 
        elseif($typ=='telefon'){
            $typ="text";
            $format="telefon";
            $nazev = $pol['values'];
            }    
        elseif($typ=='int'){
            $typ="text";
            $format="integer";
            $nazev = $pol['values'];
            }
        elseif($typ=='float'){
            $typ="text";
            $format="float";
            $nazev = $pol['values'];
            }  
        elseif($typ=='ico'){
            $typ="text";
            $format="ico";
            $nazev = $pol['values'];
            }
        elseif($typ=='dic'){
            $typ="text";
            $format="dic";
            $nazev = $pol['values'];
            }
        elseif($typ=='label'){
            $typ="label";
            $format="text";
            $hodnoty = $pol['values'];
            }   
        
        if($typ=='radio' || $typ=='checkbox' || $typ=='selectbox'){
            $h = $pol['values'];
            $nazev = $pol['title'];
            //if(count($h)==0) continue;  //pokud nema selectbox nebo checkbox nebo radiogroup zadany zadne polozky pak se polozka nevytvori
            //print_r($h);
            if(count($h)>0){
                $vysl = array();
                foreach($h AS $v){
                    $val = trim($v['value']);
                   
                    if($v['default']=='true') $default=1; else $default=0;
                    if($val=='') continue;
                    $vysl[] = $val.":".$default;
                    }
                
                $hodnoty = implode('#',$vysl);
                }
                else
                {
                $hodnoty = "";
                }
                
            }
        
        $nazev = $db->secureString(strip_tags($nazev));
        $typ = $db->secureString(strip_tags($typ));
        $hodnoty = $db->secureString(strip_tags($hodnoty));
        $format = $db->secureString(strip_tags($format));
     
        
        if($id==0)
            $db->Query("INSERT INTO ".TABLE_FORMULARE_POLOZKY." (idFormulare, nazev, typ, hodnoty, format, povinna, priorita) VALUES(".$idFormulare.", '".$nazev."', '".$typ."', '".$hodnoty."','".$format."' ,".$povinna.", ".$priorita.")");
        elseif($id>0)
            $db->Query("UPDATE ".TABLE_FORMULARE_POLOZKY." SET nazev='".$nazev."', typ='".$typ."', hodnoty='".$hodnoty."', format='".$format."', povinna=".$povinna.", priorita=".$priorita." WHERE idFormulare=".$idFormulare." AND idPolozky=".$id." LIMIT 1");
     
        else continue; 
        
        $priorita++;

        }
        
    
    
    }
    

if(count($stare_polozky)>0){

    $delete = array();
    
    foreach($stare_polozky AS $pol)
        if(!in_array($pol, $nove))
            $delete[] = $pol;

    if(count($delete)>0)    
        $db->Query("DELETE FROM ".TABLE_FORMULARE_POLOZKY." WHERE idFormulare=".$idFormulare." AND idPolozky IN (".implode(',', $delete).")");
    
    
    
    }

$nazev = $db->get(TABLE_FORMULARE, 'nazev','idFormulare='.$idFormulare);
$log->add_log('edit-content','formular',$idFormulare,$nazev);
    
  
exit;





















?>