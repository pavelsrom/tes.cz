<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!$login_obj->UserPrivilege('settings_edit') || !defined("SECURITY_CMS")) exit;

$idStranky = get_int_post('id');
if($idStranky <= 0) exit;
            
$where = "AND typ NOT IN ('galerie','galerie-slozka','novinka','clanek','galerie-fotka','galerie-slozka','galerie','404')";

$idRodice = $db->get(TABLE_STRANKY,'idRodice',"idStranky=".$idStranky." AND idDomeny=".$domain->getId()." ".$where);
          
$result = $polozky = array();
            
$data = $db->Query("SELECT idStranky FROM ".TABLE_STRANKY."
                WHERE idDomeny=".$domain->getId()."
                    AND idRodice=".$idRodice."
                    AND idJazyka=".WEB_LANG_ID."
                ORDER BY priorita
                         
                ");
            
$i=0;
while($p = $db->getAssoc($data)){
    $polozky[$i] = $p['idStranky'];
    $result[$i] = $p['idStranky'];
    $i++;
    }

foreach($polozky AS $i => $pol){
    if($idStranky == $pol && isset($polozky[$i - 1]))
        list($result[$i - 1], $result[$i]) = array($polozky[$i], $polozky[$i - 1]);
        $i++;    
    }

foreach($result AS $priorita => $id)
    $db->Query("UPDATE ".TABLE_STRANKY." SET priorita=".$priorita." WHERE idStranky=".$id." AND idDomeny=".$domain->getId()." ".$where." AND idRodice=".$idRodice." LIMIT 1");
                
$log->add_log('edit-settings','stranky-nastaveni');

?>