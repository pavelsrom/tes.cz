<?php

/**
 * @author Pavel Srom
 * @copyright 2010
 */


if(!isset($_GET['email'],$_GET['profil'],$_GET['pass']) || !defined("SECURITY_CMS")) exit;
$email = trim($_GET['email']);
$profil = trim($_GET['profil']);
$pass = trim($_GET['pass']);

$error = array();
$result = array("ok" => "", "error" => "");

if($email=='' || $pass=='' || $profil=='') {
	$result["error"] = TNENI_ZADAN_EMAIL_NEBO_ID_PROFILU;
    echo json_encode($result);
	exit;
	}
	
    
    
include(PRE_PATH.'php/classes/class.gapi.php');
	
try{
	$ga = new gapi($email,$pass);
	}
	catch(exception $e){
    $result["error"] = TNESPRAVNE_PRIHLASOVACI_UDAJE_K_UCTU_GA;
    echo json_encode($result);
	exit;
	}


	
try{	
	$ga->requestReportData($profil,array('year'), array('visits'), null, null,null, '', '', 1,0);
	}
	catch(exception $e){
		$err = $e->GetMessage();
        //echo $err;
        
  		if(strstr($err, 'access profile') || strstr($err, 'Invalid value for ids'))
            $result["error"] = TCHYBNE_ID_PROFILU;
        elseif(strstr($err,"Failed to request report data"))
            $result["error"] = TNESPRAVNE_PRIHLASOVACI_UDAJE_K_UCTU_GA;
  			else
  			$result["error"] = TSLUZBA_JE_MOMENTALNE_NEDOSTUPNA;
        
        echo json_encode($result);
  		exit;
		}
			

$result["ok"] = TPRISTUP_K_UCTU_GA_POVOLEN;
echo json_encode($result);

?>