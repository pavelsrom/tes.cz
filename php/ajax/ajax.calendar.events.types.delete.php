<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */
 
 
if(!$login_obj->UserPrivilege('content_delete') || !defined("SECURITY_CMS")) exit;


$id = get_int_post('id');

if($id <= 0 || !$object_access->has_access($id)) exit;

$nazev = $db->get(TABLE_STRANKY,'nazev',"idStranky=".$id." AND idDomeny=".$domain->getId()." AND idJazyka=".WEB_LANG_ID." AND typ='kalendar-typ'");
if($nazev == "") exit;
  
$log->add_log('delete',"kalendar-akci-typy",$id,$nazev);

$db->delete(TABLE_STRANKY, "idStranky=".$id." AND idDomeny=".$domain->getId()." LIMIT 1");
$db->delete(TABLE_STRANKY_TAGY, "idObjektu=".$id." AND typObjektu='stranka'");
$db->delete(TABLE_STRANKY_EDITORI,"idObjektu=".$id." AND typ = 'stranka'");
$db->delete(TABLE_ODKAZY,"typ='stranka' AND idObjektu=".$id);
$db->delete(TABLE_ZALOHY,"typ='stranka' AND idObjektu=".$id);
$db->delete(TABLE_DISKUZE_POLOZKY,"idStranky=".$id);
$db->delete(TABLE_STRANKY_SKUPINY, "idStranky=".$id);
exit;


?>