<?php

/**
 * @author Pavel Srom
 * @copyright 2010
 */


if(!isset($_POST['html']) || !defined("SECURITY_CMS")) exit;

if(!$login_obj->UserPrivilege('content_edit')) exit;




$soubor = isset($_POST['nazev_souboru'])?strtolower(trim(strip_tags($_POST['nazev_souboru']))):'';

if($soubor=='') 
    $soubor = "moje_stranka.pdf";
    else
    {
        $data_s = explode('.',$soubor);
        if(end($data_s)!='pdf')
            $soubor .= ".pdf";
    }


$html = urldecode(trim($_POST['html']));


include(PRE_PATH.'php/classes/pdfwriter/mpdf.php');

$pdf = new mPDF('utf-8','A4');
//$pdf = new mPDF('utf-8','A2-L');

$pdf->WriteHTML($html);


$filename = $soubor;
header("Pragma: public");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");;
header("Content-Disposition: attachment;filename=".$filename);
header("Content-Transfer-Encoding: binary ");


echo $pdf->output($filename, 'S');
exit;

//echo file_get_contents('../../test.pdf');


?>