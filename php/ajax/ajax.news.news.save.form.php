<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined('SECURITY_CMS')) exit;

include("../classes/class.messages.php");
$systemMessage = new C_SystemMessage();

$result = array(
    "redirect" => "",
    "messages" => array(
        "error" => ""
        )
    );

$idStranky = get_int_post('idStranky');

if((!$login_obj->UserPrivilege('content_edit') && $idStranky > 0) || 
    (!$login_obj->UserPrivilege('content_add') && $idStranky == 0)
    ){
    $result["messages"]["error"] = ERROR_NEDOSTATECNA_PRAVA;
    echo array2json($result);
    exit;
}

	
$titulek = $nadpis = get_post('nazev');
$file = get_post('obrazek');
$url = get_post('url');
$title = get_post('title');

if($idStranky==0 && $title=='')
    $title = $titulek;
            
$desc = get_post('description');
$zobrazit = get_int_post('zobrazit');
			
$od = get_post('od');
$do = get_post('do');
$datum = get_post('datum');
$datum = GetUniDate($datum);
            
$idProfiluPanelu = get_int_post('idProfiluPanelu');
$idGalerie = get_int_post('idGalerie');    
$idFormulare = get_int_post('idFormulare');
$idAnkety = get_int_post('idAnkety');
$diskuze = get_int_post('diskuze');
            
$url = $idStranky==0 && $url=='' ? ModifyUrl($titulek) : ModifyUrl($url);
$url_new = get_url_nazev($idStranky, $url);
if($url!=$url_new)
    $systemMessage->add_warning(WARNING_URLNAZEV_MODIFIKOVAN." ".$url_new);
$url = $url_new;

if($url == "")
    $systemMessage->add_error(ERROR_NEPLATNY_URL_NAZEV);

$update = array();

if($datum!='')
    $update["datum"] = $datum;
	else
	$update["datum"] = "null";
    
        
if(!TestSentenceCZ($titulek, 200, false))
    $systemMessage->add_error(ERROR_PRAZDNY_TITULEK);

			
if(!TestDate($od, true) || !TestDate($do, true)){
    $systemMessage->add_error(ERROR_CHYBNE_DATUM);
    }
    else
    {
    if(!TestTwoDate($od, $do)){
        $systemMessage->add_warning(WARNING_CHYBNE_DATUM);
        }
			
    $od = GetUniDate($od);
    $do = GetUniDate($do);
    if($od!='') 
        $update["od"] = $od;
        else    
        $update["od"] = "null";
    					
    if($do!='')
        $update["do"] = $do;
        else    
        $update["do"] = "null";
    }

			
if($systemMessage->error_exists())
{
    $result["messages"]["error"] = $systemMessage->get_first_message();
    echo array2json($result);
    exit;    
};

$idStrankyNovinky = $db->get(TABLE_STRANKY,"idStranky", "typ='novinky' AND idDomeny=".$domain->getId()." AND idJazyka=".WEB_LANG_ID);
$idStrankyNovinky = intval($idStrankyNovinky);

$update = $update + array(
    "idDomeny"      => $domain->getId(),
    "typ"           => "novinka",
    "nazev"         => $titulek,
    "idRodice"      => $idStrankyNovinky,
    "nazevProMenu"  => $titulek,
    "nadpis"        => $nadpis,
    "zobrazit"      => $zobrazit,
    "title"         => $title,
    "description"   => $desc,
    "url"           => $url,
    "idProfiluPanelu"=>$idProfiluPanelu,
    "idGalerie"     => $idGalerie,
    "idFormulare"   => $idFormulare,
    "idAnkety"      => $idAnkety,
    "diskuze"       => $diskuze,
    "idJazyka"      => WEB_LANG_ID
    );
			
if($idStranky == 0){

    $update['idVytvoril'] = $login_obj->getId();
    $update['jmenoVytvoril'] = $login_obj->getName();
    $update['datumVytvoril'] = "now";
    $update['idAktualizoval'] = $login_obj->getId();
    $update['jmenoAktualizoval'] = $login_obj->getName();
    $update['datumAktualizoval'] = "now";
    $update['obsah'] = "<p>".str_repeat(TLOREM_IPSUM_OBSAH." ", 10)."</p>";
    $update['perex'] = "<p>".str_repeat(TLOREM_IPSUM_PEREX." ", 5)."</p>";
     
    $db->insert(TABLE_STRANKY,$update);
    $idStranky = $db->lastId();
    $object_access->add_allowed_object($idStranky);
    }
    else
    {
    $update['idAktualizoval'] = $login_obj->getId();
    $update['jmenoAktualizoval'] = $login_obj->getName();
    $update['datumAktualizoval'] = "now";
    
    $db->update(TABLE_STRANKY,$update,"idStranky=".$idStranky." AND idDomeny=".$domain->getId());
	}
    			

if($file == '' && isset($_FILES['obrazek']) && $_FILES['obrazek']['error']!=4){
    $upload = $_FILES['obrazek'];
	$news_path = "../../".$domain->getDir().USER_DIRNAME_NEWS;
                    
    include_once('../classes/class.upload.php');
                    
    $uploader = new upload($_FILES['obrazek'], 'cs_CS');
    $uploader->file_new_name_body = $idStranky;
    $uploader->file_overwrite = true;
    $uploader->image_resize = true;
    $uploader->image_x = intval(OBRAZEK_NOVINKY_MINI_X);
    $uploader->image_y = intval(OBRAZEK_NOVINKY_MINI_Y);
    $uploader->image_ratio = true;
    $uploader->image_convert = 'jpg';
    $uploader->process($news_path);
	$uploader->clean();
    $file = $idStranky.".jpg";
                    
    $db->update(TABLE_STRANKY,array("obrazek" => $file),"idDomeny=".$domain->getId()." AND idStranky=".$idStranky." AND typ='novinka' LIMIT 1");
				
}


$log->add_log(get_int_post('idStranky') == 0 ? 'create':'edit-content','novinka',$idStranky,$titulek);

$redirect_url = UrlPage($url,$idStranky,WEB_LANG);
    
$result["redirect"] = $redirect_url;
$result["messages"]["ok"] = OK_ULOZENO_A_PRESMEROVANO;


echo array2json($result);
exit; 

?>