<?php

/**
 * @author Pavel Srom
 * @copyright 2010
 */


if(!is_post('typ') || !is_post('id') || !is_post('val') || !defined("SECURITY_CMS")) exit;
if(!$login_obj->UserPrivilege('superadmin') || !$login_obj->UserPrivilege('settings_edit')) exit;

$typy = array(
    'settings_add',
	'settings_delete',
	'settings_edit',
	'settings_view',
	'content_view',
	'content_add',
	'content_delete',
	'content_edit',
	'send_email',
	'statistics_view',
	'export',
	'import',
	'set_delete',
    'seo',
    'translations'
	);


$typ = get_post('typ');
$idPrava = get_int_post('id');
$val = get_int_post('val');

if($idPrava==0 || $typ == '' || !in_array($typ,$typy)) exit;

$db->update(TABLE_CMS_PRAVA,array($typ => $val),"idPrava=".$idPrava);

exit();

?>