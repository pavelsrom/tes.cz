<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */
 
 
if(!$login_obj->UserPrivilege('content_delete') || !defined("SECURITY_CMS")) exit;


$id = get_int_post('id');

if($id <= 0) exit;

$data = $db->query("SELECT nazev FROM ".TABLE_STRANKY." WHERE idStranky=".$id." AND idDomeny=".$domain->getId()." AND idJazyka=".WEB_LANG_ID." AND typ='tag' LIMIT 1");
if($db->numRows($data) == 0) exit;

$n = $db->getAssoc($data);
$nazev = $n['nazev'];
  
$log->add_log('delete',"tag",$id,$nazev);

$db->delete(TABLE_STRANKY, "WHERE idStranky=".$id." AND idDomeny=".$domain->getId()." LIMIT 1");
$db->delete(TABLE_STRANKY_TAGY, "WHERE idTagu=".$id);
$db->delete(TABLE_STRANKY_EDITORI,"WHERE idObjektu=".$id." AND typ = 'stranka'");
$db->delete(TABLE_ODKAZY,"WHERE typ='stranka' AND idObjektu=".$id);
$db->delete(TABLE_ZALOHY,"WHERE typ='stranka' AND idObjektu=".$id);
$db->delete(TABLE_DISKUZE_POLOZKY,"WHERE idStranky=".$id);
$db->delete(TABLE_STRANKY_SKUPINY, "idStranky=".$id);
exit;


?>