<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */
 
 
if(!$login_obj->UserPrivilege('content_delete') || !defined("SECURITY_CMS")) exit;


$id = get_int_post('id');

if($id <= 0) exit;

$nazev = $db->get(TABLE_HTML_SABLONY,'nazev','idSablony='.$id);	
$log->add_log('delete',"sablona-stranky",$id,$nazev);

if($login_obj->UserPrivilege('admin') || $login_obj->UserPrivilege('superadmin'))
    $db->delete(TABLE_HTML_SABLONY, "WHERE idSablony=".$id." AND (idDomeny=".$domain->getId()." OR idDomeny=0)");
    else
    $db->delete(TABLE_HTML_SABLONY, "WHERE idSablony=".$id." AND idDomeny=".$domain->getId()." LIMIT 1");

echo OK_SMAZANO;

exit;


?>