<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!isset($_GET['module']) || !defined("SECURITY_CMS")) exit;

$jen_chybne = intval($_GET['jen_chybne']);
$module = trim($_GET['module']);

//nastaveni limitu
$limit = "";
if(isset($_GET['iDisplayLength'])){
    $dStart = "";
    if(isset($_GET['iDisplayStart']))
        $dStart = abs(intval($_GET['iDisplayStart'])).",";
        
    $limit = " LIMIT ".$dStart.abs(intval($_GET['iDisplayLength'])); 
}

$order = "ORDER BY o.typ, o.datumKontroly";

$like = "";
if($jen_chybne > 0)
{
    if($jen_chybne == 1)
        $like = "o.kod >= 400";
    elseif($jen_chybne == 2)
        $like = "o.kod < 400";    
}


$typy = array(
    'uvodni'    => TUVODNI_STRANKA,
    'stranka'   => TSTRANKA, 
    'novinka'   => TNOVINKA, 
    'editbox'   => TEDITBOX, 
    'clanek'    => TCLANEK, 
    'galerie'   => TGALERIE,
    'mapa-webu' => TMAPA_WEBU,
    'novinky'   => TNOVINKY,
    'clanky'    => TCLANKY,
    'archiv-clanku'=>TARCHIV_CLANKU,
    '404'       => TCHYBOVA_STRANKA,
    'stranka-galerie'=>TSTRANKA_S_GALERII,
    'galerie-slozka'=>TSLOZKA_GALERIE,
    'galerie-fotka'=>TFOTKA,
    'newsletter'=> TEMAILOVA_KAMPAN,
    'editbox'   => TEDITBOX,
    'akce'      => TAKCE,
    'kalendar-typ' => TTYP_AKCE,
    'tagy'      => TTAGY,
    'kalendar-akci'=>TKALENDAR_AKCI,
    'produkty'  => TSTRANKY_S_PRODUKTY,
    'produkt' => TPRODUKT,
    'email' => TSTRANKA_ODBER_NOVINEK,
    'login' => TPRIHLASOVACI_STRANKA,
    'obchodni-podminky' => TSTRANKA_OBCHODNI_PODMINKY,
    'objednavka' => TSTRANKA_OBJEDNAVKA
    );

$data = $db->query("SELECT SQL_CALC_FOUND_ROWS o.idObjektu, o.typ, o.perex, o.idJazyka, j.nazev, j.jazyk, o.idPopisu
    FROM ".TABLE_ODKAZY." AS o
    LEFT JOIN ".TABLE_JAZYKY." AS j ON o.idJazyka=j.idJazyka
    WHERE o.idDomeny=".$domain->getId()." 
        AND j.aktivni = 1
        AND o.idJazyka IN (".implode(',',$domain->data['jazyky'] + array(0)).")
        ".($like != "" ? "AND ".$like : "")."
    GROUP BY CONCAT(o.idObjektu, o.typ)
    ".$order." 
    ".$limit."
    ");
    
    
$iFilteredTotal_data = $db->Query("SELECT FOUND_ROWS()");
$iFilteredTotal_arr = $db->getRow($iFilteredTotal_data);
$iFilteredTotal = intval($iFilteredTotal_arr[0]);

$iTotal = $db->numRows($data);
  
    

$tr = array();
while($o = $db->getAssoc($data))
{

    $pom = array();
    
    $odkaz_na_web = "";
    $odkaz_do_administrace = "";
     
    $idObjektu = $o['idObjektu'];
    $idJazyka = $o['idJazyka'];
    
    $nazev = "";
    
    if($o['typ'] == 'stranka')
    {
        $s = $db->get(TABLE_STRANKY,array('nazev','typ','idRodice AS rodic'),"idStranky=".$idObjektu);
        
        $odkaz_na_web = $links->get_url($idObjektu);
        $typ = $s->typ;   
             
        $typ_nazev = $typy[$typ];
        $nazev = $s->nazev;
        
        switch($typ)
        {
            case 'stranka':
            case 'clanky':
            case 'novinky':
            case 'sitemap':
            case '404':
            case 'archiv-clanku':
            case 'uvodni':
            case 'stranka-galerie':
            case 'email':
            case 'login':
            case 'obchodni-podminky':
            case 'objednavka':
            case 'vyhledavani': 
                {
                    if((MODULE_PAGES && !in_array($typ,array('clanky','novinky','archiv-clanku'))) || 
                    (MODULE_ARTICLES && in_array($typ, array('clanky','archiv-clanku'))) || 
                    (MODULE_NEWS && in_array($typ,array('novinky'))) ||
                    (MODULE_ESHOP && in_array($typ,array('produkty')))
                    )
                        if($login_obj->UserPrivilege('content_view'))
                            $odkaz_do_administrace = get_link('pages','stranky',0,'edit-content',$idObjektu,"",$idJazyka);
                            
                    break;
                }
            case 'galerie':
            case 'galerie-slozka':
                {
                    if(MODULE_GALLERY && $login_obj->UserPrivilege('content_view'))
                        $odkaz_do_administrace = get_link('gallery','galerie',0,'edit-settings',$idObjektu,"",$idJazyka);
                    break;
                }
            case 'novinka':
                {
                    if(MODULE_NEWS && $login_obj->UserPrivilege('content_view'))
                        $odkaz_do_administrace = get_link('news','novinky',0,'edit-content',$idObjektu,"",$idJazyka);
                    break;
                }
            case 'clanek':
                {
                    if(MODULE_ARTICLES && $login_obj->UserPrivilege('content_view'))
                        $odkaz_do_administrace = get_link('articles','rubriky',$s->rodic,'edit-content',$idObjektu,"",$idJazyka);
                    break;
                }
            case 'produkt':
                {
                    if(MODULE_ESHOP && $login_obj->UserPrivilege('content_view'))
                        $odkaz_do_administrace = get_link('eshop','kategorie',$s->rodic,'edit-content',$idObjektu,"",$idJazyka);
                    break;
                }
            default: break;
                   
                
                
        }
        
        
    }
    
    if($o['typ'] == 'editbox' && MODULE_EDITBOX)
    {
        $d = $db->query("SELECT nazev, stridani FROM ".TABLE_EDITBOXY." WHERE idEditboxu=".$idObjektu);
        $de = $db->getAssoc($d);
        $nazev = $de['nazev'];
        $typ_nazev = $typy[$o['typ']];
        
        if($login_obj->UserPrivilege('content_view'))
            $odkaz_do_administrace = get_link('editboxes','editboxy',0,$de['stridani']=='zadne' ? 'edit-content' : 'edit-content-kolotoc',$idObjektu,"",$idJazyka);
        
    }
    
    if($o['typ'] == 'newsletter' && MODULE_NEWSLETTER)
    {
        $nazev = $db->get(TABLE_EMAILY_KAMPANE,'nazev','idKampane='.$idObjektu);
        $typ_nazev = $typy[$o['typ']];
        
        if($login_obj->UserPrivilege('settings_view'))
            $odkaz_do_administrace = get_link('newsletters','kampane',0,'edit-settings',$idObjektu,"",$idJazyka);
        
    }
    
    $ikony = "";
    if($odkaz_na_web != "")
        $ikony .= "<a href='".$links->get_url($idObjektu)."' target='_blank'>".icon('link')."</a>";
        else
        $ikony .= icon_disabled('link');
    
    if($odkaz_do_administrace == "")
        $ikony .= icon_disabled('content');
    	else	
    	$ikony .= "<a href='".$odkaz_do_administrace."'>".icon('content')."</a>";
    	
    
    $data_chybne_odkazy = $db->query("SELECT o.* FROM ".TABLE_ODKAZY." AS o
            WHERE o.idObjektu=".$idObjektu." 
                AND o.typ='".$o['typ']."' 
                ".($like != "" ? "AND ".$like : "")." 
            ORDER BY o.perex");
        
        $chybne_odkazy = "";
        $chybne_odkazy_v_perexu = "";
        $chybne_odkazy_v_popisu1 = "";
        $chybne_odkazy_v_popisu2 = "";
        $chybne_odkazy_v_popisu3 = "";
        $chybne_odkazy_v_popisu4 = "";
        $chybne_odkazy_v_popisu5 = "";
        
        while($l = $db->getAssoc($data_chybne_odkazy))
        {
            if(in_array($l['typSouboru'],array('file','image')))
                $anchor = "[".TSOUBOR_OBRAZEK."]";
                else
                $anchor = $l['text'];
            
            $anchor = "<b>".$anchor."</b>";

            $style = $l['kod'] < 600 && $l['kod'] >= 400 ? "color: red;" : "";
            $img = $l['kod'] < 600 && $l['kod'] >= 400 ? icon("error","","stav0") : icon("ok","","stav1");    
            $img .= "&nbsp;";
            
            if($l['perex'] == 1)
                $chybne_odkazy_v_perexu .= $img." ".$anchor." - ".$l['url']."<br />";
            elseif($l['idPopisu'] == 1)
                $chybne_odkazy_v_popisu1 .= $img." ".$anchor." - ".$l['url']."<br />";
            elseif($l['idPopisu'] == 2)
                $chybne_odkazy_v_popisu2 .= $img." ".$anchor." - ".$l['url']."<br />";
            elseif($l['idPopisu'] == 3)
                $chybne_odkazy_v_popisu3 .= $img." ".$anchor." - ".$l['url']."<br />";
            elseif($l['idPopisu'] == 4)
                $chybne_odkazy_v_popisu4 .= $img." ".$anchor." - ".$l['url']."<br />";
            elseif($l['idPopisu'] == 5)
                $chybne_odkazy_v_popisu5 .= $img." ".$anchor." - ".$l['url']."<br />";
            else
                $chybne_odkazy .= $img." ".$anchor." - ".$l['url']."<br />";
        }
    
    $seznam_odkazu = "";
    if($chybne_odkazy!="")
        $seznam_odkazu = TV_OBSAHU.':<br />'.$chybne_odkazy.'';
        
    $seznam_odkazu_v_perexu = "";
    if($chybne_odkazy_v_perexu!="")
        $seznam_odkazu_v_perexu = TV_PEREXU.':<br />'.$chybne_odkazy_v_perexu.'';    
    
    $seznam_odkazu_v_popisu1 = "";
    if($chybne_odkazy_v_popisu1!="")
        $seznam_odkazu_v_popisu1 = TPRODUKT_ZALOZKA1.':<br />'.$chybne_odkazy_v_popisu1.''; 
        
    $seznam_odkazu_v_popisu2 = "";
    if($chybne_odkazy_v_popisu2!="")
        $seznam_odkazu_v_popisu2 = TPRODUKT_ZALOZKA2.':<br />'.$chybne_odkazy_v_popisu2.''; 
     
    $seznam_odkazu_v_popisu3 = "";
    if($chybne_odkazy_v_popisu3!="")
        $seznam_odkazu_v_popisu3 = TPRODUKT_ZALOZKA3.':<br />'.$chybne_odkazy_v_popisu3.''; 
    
    $seznam_odkazu_v_popisu4 = "";
    if($chybne_odkazy_v_popisu4!="")
        $seznam_odkazu_v_popisu4 = TPRODUKT_ZALOZKA4.':<br />'.$chybne_odkazy_v_popisu4.''; 
     
    $seznam_odkazu_v_popisu5 = "";
    if($chybne_odkazy_v_popisu5!="")
        $seznam_odkazu_v_popisu5 = TPRODUKT_ZALOZKA5.':<br />'.$chybne_odkazy_v_popisu5.''; 
       
    
    if($seznam_odkazu_v_perexu != "")
        $seznam_odkazu = "<br />".$seznam_odkazu;
    
    $pom[] = json_encode($typ_nazev);
    $pom[] = json_encode(secureString($nazev));
    $pom[] = json_encode(strtoupper($o['jazyk']));
    $pom[] = json_encode($seznam_odkazu_v_perexu.$seznam_odkazu.$seznam_odkazu_v_popisu1.$seznam_odkazu_v_popisu2.$seznam_odkazu_v_popisu3.$seznam_odkazu_v_popisu4.$seznam_odkazu_v_popisu5);
    $pom[] = json_encode($ikony);
    
    $tr[] = "[".implode(',', $pom)."]";
    
    
}


echo "{";
echo '"sEcho": '.(intval($_GET['sEcho'])).', ';
echo '"iTotalRecords": '.$iTotal.', ';
echo '"iTotalDisplayRecords": '.($iFilteredTotal).', ';
echo '"aaData": ['.implode(',', $tr).'] }';


?>