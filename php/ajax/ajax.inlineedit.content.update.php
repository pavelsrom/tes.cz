<?php

/**
 * @author Pavel Srom
 * @copyright 2013
 */

if(!$login_obj->UserPrivilege('content_edit') || !defined("SECURITY_CMS")) exit;

$id = get_post('id'); 
$id = explode("-",$id);

$content = get_post('content');

if(count($id) != 2) exit;



$content_type = $id[0];

$content_id = $content_type == 'preklad' ? $id[1] : intval($id[1]);

if((($content_id == '' && $content_type == 'preklad') || ($content_id <= 0 && $content_type != 'preklad')) && $content_type != "") exit;


include_once('../classes/class.content.backup.php');
include_once('../classes/class.links.checker.php');


$result = "";
$error = "";


if($content_type == 'perex')
{
    $s = $db->get(TABLE_STRANKY,array('nazev','typ','obsah'),"idStranky=".$content_id);
    $nazev = $s->nazev; 
    $typ_do_logu = $s->typ;
    $obsah = $s->obsah;
    
    $update = array(
        'perex' => $content, 
        'perexBezHtml'  => strip_tags($content),
        "idAktualizoval"    => $login_obj->getId(),
        "jmenoAktualizoval" => $login_obj->getName(),
        "datumAktualizoval" => "now"
        );
    $db->update(TABLE_STRANKY,$update,"idStranky='".$content_id."' LIMIT 1");
    
    if(MODULE_CONTENT_BACKUP)
    {
        $content_backup = new ContentBackup($domain->getId(),$content_id,$login_obj->getId());
        $content_backup->AddStore($obsah,$content);
    }


    if(MODULE_LINK_CHECKER)
    {
        $chybne_odkazy = new LinksChecker($domain->getId(), $content_id, "stranka", $content, false, KONTROLOVAT_ODKAZY_PRI_UKLADANI);
        $chybne_odkazy->set_base_url( $domain->getUrl() );
        $chybne_odkazy->process();
        $pocet_chybnych_obrazku = $chybne_odkazy->get_sum_error_images();
        $pocet_chybnych_odkazu = $chybne_odkazy->get_sum_error_links();
        
        if($pocet_chybnych_obrazku > 0)
            $error .= TPOCET_CHYBNYCH_OBRAZKU_V_PEREXU.": ".$pocet_chybnych_obrazku."\n";
            
        if($pocet_chybnych_odkazu > 0)
            $error .= TPOCET_CHYBNYCH_ODKAZU_V_PEREXU.": ".$pocet_chybnych_odkazu."\n";
        
    }

    $log->add_log('edit-content',get_log_type($typ_do_logu),$content_id,$nazev);
}
elseif($content_type == 'h1')
{
    $s = $db->get(TABLE_STRANKY,array('nazev','typ','obsah'),"idStranky=".$content_id);
    $nazev = $s->nazev; 
    $typ_do_logu = $s->typ;
    
    $update = array(
        'nadpis' => $content,
        "idAktualizoval"    => $login_obj->getId(),
        "jmenoAktualizoval" => $login_obj->getName(),
        "datumAktualizoval" => "now"
        );
    $db->update(TABLE_STRANKY,$update,"idStranky='".$content_id."' LIMIT 1");  
    
    $log->add_log('edit-content',get_log_type($typ_do_logu),$content_id,$nazev);
}
elseif($content_type == 'content')
{
    
    $s = $db->get(TABLE_STRANKY,array('nazev','typ','perex'),"idStranky=".$content_id);
    $nazev = $s->nazev; 
    $typ_do_logu = $s->typ;
    $perex = $s->perex;
    
    $update = array(
        'obsah' => $content, 
        'obsahBezHtml' => strip_tags($content),
        "idAktualizoval"    => $login_obj->getId(),
        "jmenoAktualizoval" => $login_obj->getName(),
        "datumAktualizoval" => "now"
        );
        
    $db->update(TABLE_STRANKY,$update,"idStranky='".$content_id."' LIMIT 1");
    
    if(MODULE_CONTENT_BACKUP)
    {
        $content_backup = new ContentBackup($domain->getId(),$content_id,$login_obj->getId());
        $content_backup->AddStore($content,$perex);
    }

    $chybne_odkazy_indik = false;
    
    if(MODULE_LINK_CHECKER)
    {
        $chybne_odkazy = new LinksChecker($domain->getId(), $content_id, "stranka", $content, false, KONTROLOVAT_ODKAZY_PRI_UKLADANI);
        $chybne_odkazy->set_base_url( $domain->getUrl() );
        $chybne_odkazy->process();
        $pocet_chybnych_obrazku = $chybne_odkazy->get_sum_error_images();
        $pocet_chybnych_odkazu = $chybne_odkazy->get_sum_error_links();
        
        if($pocet_chybnych_obrazku > 0)
            $error .= TPOCET_CHYBNYCH_OBRAZKU_NA_STRANCE.": ".$pocet_chybnych_obrazku."\n";
            
        if($pocet_chybnych_odkazu > 0)
            $error .= TPOCET_CHYBNYCH_ODKAZU_NA_STRANCE.": ".$pocet_chybnych_odkazu."\n";  
                    
    }
    
    $log->add_log('edit-content',get_log_type($typ_do_logu),$content_id,$nazev);
}
elseif($content_type == 'editbox')
{
    
    $nazev = $db->get(TABLE_EDITBOXY,'nazev',"idEditboxu=".$content_id);
    
    $update = array('obsah' => $content);
    $db->update(TABLE_EDITBOXY,$update,"idEditboxu='".$content_id."' LIMIT 1");
    
    if(MODULE_CONTENT_BACKUP)
    {
        $content_backup = new ContentBackup($domain->getId(),$content_id,$login_obj->getId(),'editbox');
        $content_backup->AddStore($content);
    }

    if(MODULE_LINK_CHECKER)
    {
        $chybne_odkazy = new LinksChecker($domain->getId(), $content_id, "editbox", $content, false, KONTROLOVAT_ODKAZY_PRI_UKLADANI);
        $chybne_odkazy->set_base_url( $domain->getUrl() );
        $chybne_odkazy->process();
        $pocet_chybnych_obrazku = $chybne_odkazy->get_sum_error_images();
        $pocet_chybnych_odkazu = $chybne_odkazy->get_sum_error_links();
        
        if($pocet_chybnych_obrazku > 0)
            $error .= TPOCET_CHYBNYCH_OBRAZKU_NA_STRANCE.": ".$pocet_chybnych_obrazku;
            
        if($pocet_chybnych_odkazu > 0)
            $error .= TPOCET_CHYBNYCH_ODKAZU_NA_STRANCE.": ".$pocet_chybnych_odkazu;
            
    }

    $log->add_log('edit-settings','editbox',$content_id,$nazev);

}
elseif(stristr($content_type,'popis'))
{
    
    $s = $db->get(TABLE_STRANKY,array('nazev','typ','perex'),"idStranky=".$content_id);
    $nazev = $s->nazev; 
    $typ_do_logu = $s->typ;
    
    $update = array(
        $content_type => $content, 
        $content_type.'BezHtml' => strip_tags($content),
        "idAktualizoval"    => $login_obj->getId(),
        "jmenoAktualizoval" => $login_obj->getName(),
        "datumAktualizoval" => "now"
        );
        
    $db->update(TABLE_STRANKY,$update,"idStranky='".$content_id."' LIMIT 1");
    $o = $db->get(TABLE_STRANKY,array("perex","obsah","popis1","popis2","popis3","popis4","popis5"),"idStranky=".$content_id);
    
    $obsah = $o->obsah;
    $perex = $o->perex;
    $popis1 = $content_type != 'popis1' ? $o->popis1 : $content;
    $popis2 = $content_type != 'popis2' ? $o->popis2 : $content;
    $popis3 = $content_type != 'popis3' ? $o->popis3 : $content;
    $popis4 = $content_type != 'popis4' ? $o->popis4 : $content; 
    $popis5 = $content_type != 'popis5' ? $o->popis5 : $content;
      
    if(MODULE_CONTENT_BACKUP)
    {
        $content_backup = new ContentBackup($domain->getId(),$content_id,$login_obj->getId());
        $content_backup->AddStore($content,$perex,$popis1, $popis2, $popis3, $popis4, $popis5);
    }


    $chybne_odkazy_indik = false;
    
    if(MODULE_LINK_CHECKER)
    {
        $idPopisu = str_replace('popis','',$content_type);
        $chybne_odkazy = new LinksChecker($domain->getId(), $content_id, "stranka", $content, false, KONTROLOVAT_ODKAZY_PRI_UKLADANI,"",$idPopisu);
        $chybne_odkazy->set_base_url( $domain->getUrl() );
        $chybne_odkazy->process();
        $pocet_chybnych_obrazku = $chybne_odkazy->get_sum_error_images();
        $pocet_chybnych_odkazu = $chybne_odkazy->get_sum_error_links();
        
        if($pocet_chybnych_obrazku > 0)
            $error .= TPOCET_CHYBNYCH_OBRAZKU_NA_STRANCE.": ".$pocet_chybnych_obrazku."\n";
            
        if($pocet_chybnych_odkazu > 0)
            $error .= TPOCET_CHYBNYCH_ODKAZU_NA_STRANCE.": ".$pocet_chybnych_odkazu."\n";  
                    
    }
    
    $log->add_log('edit-content',get_log_type($typ_do_logu),$content_id,$nazev);
}
elseif($content_type == 'preklad')
{
    //$id_prekladu = $db->get(TABLE_PREKLADY,"idPrekladu","konstanta='".$db->secureString($content_id)."'");
    
    //$content = strip_tags($content);

    $update = array(LANG => $content);
    $db->update(TABLE_PREKLADY,$update,"konstanta='".$db->secureString($content_id)."'");
    
}



if($error != '')
    $result["error"] = $error;
    
echo json_encode($result);
exit;





?>