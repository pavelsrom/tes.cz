<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */
 
 
if(!$login_obj->UserPrivilege('content_delete') || !defined("SECURITY_CMS")) exit;


$id = get_int_post('id');

if($id <= 0) exit;

$nazev = $db->get(TABLE_PRESMEROVANI,'CONCAT(zdroj," => ", cil)','id='.$id);	
$log->add_log('delete',"presmerovani",$id,$nazev);

$db->delete(TABLE_PRESMEROVANI, "WHERE id=".$id." AND idDomeny=".$domain->getId()." LIMIT 1");

echo OK_SMAZANO;

exit;


?>