<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined('SECURITY_CMS')) exit;

include("../classes/class.messages.php");
$systemMessage = new C_SystemMessage();

$result = array(
    "redirect" => "",
    "messages" => array(
        "error" => ""
        )
    );

$idStranky = get_int_post('idStranky');

if((!$login_obj->UserPrivilege('content_edit') && $idStranky > 0) || 
    (!$login_obj->UserPrivilege('content_add') && $idStranky == 0)
    ){
    $result["messages"]["error"] = ERROR_NEDOSTATECNA_PRAVA;
    echo array2json($result);
    exit;
}



$idStranky = get_int_post("idStranky");		
$titulek = $nadpis = get_post('nazev');
$autor = get_post('autor');
$file = get_post('obrazek');
$url = get_post('url');
$title = get_post('title');

if($idStranky==0 && $title=='')
    $title = $titulek;
        
              
            
$desc = get_post('description');
$perex = get_post('perex');
$rubrika = get_int_post('rubrika');
$stav = get_post('stav');
$stary_stav = get_post('stary_stav');
$zobrazit = $stav == 'hotovo' ? 1 : 0;
$poznamka = get_post('poznamka');

$archivovat = get_int_post('archivovat');
$idProfiluPanelu = get_int_post('idProfiluPanelu');
$idGalerie = get_int_post('idGalerie');    
$idFormulare = get_int_post('idFormulare');
$idEditboxu = get_int_post('idEditboxu');
$idAnkety = get_int_post('idAnkety');
$diskuze = get_int_post('diskuze');
$obsah = get_post('obsah');
            
$od = get_post('od');
$do = get_post('do');
$datum = get_post('datum');
$datum = GetUniDate($datum);

$url = $idStranky==0 && $url=='' ? ModifyUrl($titulek) : ModifyUrl($url);
$url_zal = get_url_nazev($idStranky, $url);


$update = array();

if($datum!='')
    $update["datum"] = $datum;
	else
	$update["datum"] = "null";			
        
if(!TestSentenceCZ($titulek, 200, false))
    $systemMessage->add_error(ERROR_PRAZDNY_TITULEK);
    
if($rubrika <= 0)
    $systemMessage->add_error(ERROR_RUBRIKA_MUSI_BYT_ZADANA);
			
if(!TestDate($od, true) || !TestDate($do, true)){
    $systemMessage->add_error(ERROR_CHYBNE_DATUM);
    }
    else
    {
    if(!TestTwoDate($od, $do)){
        $systemMessage->add_warning(WARNING_CHYBNE_DATUM);
        }
			
    $od = GetUniDate($od);
    $do = GetUniDate($do);
    if($od!='') 
        $update["od"] = $od;
        else    
        $update["od"] = "null";
    					
    if($do!='')
        $update["do"] = $do;
        else    
        $update["do"] = "null";
    }

			
if($systemMessage->error_exists())
{
    $result["messages"]["error"] = $systemMessage->get_first_message();
    echo array2json($result);
    exit;    
};



$update = $update + array(
    "idDomeny"      => $domain->getId(),
    "typ"           => "clanek",
    "nazev"         => $titulek,
    "idRodice"      => $rubrika,
    "nazevProMenu"  => $titulek,
    //"obrazek"       => $file,
    "zobrazit"      => $zobrazit,
    "title"         => $title,
    "description"   => $desc,
    "url"           => $url,
    "archivovat"    => $archivovat,
    "idProfiluPanelu"=>$idProfiluPanelu,
    "idGalerie"     => $idGalerie,
    "idFormulare"   => $idFormulare,
    "idAnkety"      => $idAnkety,
    "idEditboxu"    => $idEditboxu,
    "diskuze"       => $diskuze,
    "autor"         => $autor,
    "idJazyka"      => WEB_LANG_ID,
    'stav'          => $stav,
    'poznamka'      => $poznamka
    );
			
if($idStranky == 0){

    $update['idVytvoril'] = $login_obj->getId();
    $update['jmenoVytvoril'] = $login_obj->getName();
    $update['datumVytvoril'] = "now";
    $update['idAktualizoval'] = $login_obj->getId();
    $update['jmenoAktualizoval'] = $login_obj->getName();
    $update['datumAktualizoval'] = "now";
    
    $update['nadpis'] = $nadpis;
    $update['obsah'] = "<p>".str_repeat(TLOREM_IPSUM_OBSAH." ", 10)."</p>";
    $update['perex'] = "<p>".str_repeat(TLOREM_IPSUM_PEREX." ", 5)."</p>";
     
    $db->insert(TABLE_STRANKY,$update);
    $idStranky = $db->lastId();
    $object_access->add_allowed_object($idStranky);
    }
    else
    {
    $update['idAktualizoval'] = $login_obj->getId();
    $update['jmenoAktualizoval'] = $login_obj->getName();
    $update['datumAktualizoval'] = "now";
    
    $db->update(TABLE_STRANKY,$update,"idStranky=".$idStranky." AND idDomeny=".$domain->getId());
	}
    			
                
//ulozeni tagu
/*
$tagy = new C_Tags($idStranky, 'stranka', true, true);
$tagy->save_tags();


$souvisejici = get_array_post('souvisejici');
    
$db->delete(TABLE_SOUVISEJICI_CLANKY,"WHERE idClanku=".$idStranky);

if(count($souvisejici)>0)
{
    foreach($souvisejici AS $s)
    {
        $insert_souvisejici = array("idClanku" => $idStranky, "idSouvisejicihoClanku" => $s);
        $db->insert(TABLE_SOUVISEJICI_CLANKY,$insert_souvisejici);
    }
}

*/



if(isset($_FILES['obrazek']) && $_FILES['obrazek']['error']!=4)
{

    $articles_path_mini = "../../".$domain->getDir().USER_DIRNAME_ARTICLES_MINI;
    $articles_path_maxi = "../../".$domain->getDir().USER_DIRNAME_ARTICLES_MAXI;
                        
    include_once('../classes/class.upload.php');
                        
    $uploader = new upload($_FILES['obrazek'], 'cs_CS');
    $uploader->file_new_name_body = $idStranky;
    $uploader->file_overwrite = true;
    $uploader->image_resize = true;
    $uploader->image_x = intval(OBRAZEK_CLANKU_MAXI_X);
    $uploader->image_y = intval(OBRAZEK_CLANKU_MAXI_Y);
    $uploader->image_ratio = true;
    $uploader->image_convert = 'jpg';
    $uploader->process($articles_path_maxi);
    
    $uploader = new upload($_FILES['obrazek'], 'cs_CS');
    $uploader->file_new_name_body = $idStranky;
    $uploader->file_overwrite = true;
    $uploader->image_resize = true;
    $uploader->image_ratio = true;
    $uploader->image_convert = 'jpg';
    $uploader->image_x = intval(OBRAZEK_CLANKU_MINI_X);
    $uploader->image_y = intval(OBRAZEK_CLANKU_MINI_Y);
    $uploader->process($articles_path_mini);
    $uploader->clean();
    $file = $idStranky.".jpg";
    
    $db->update(TABLE_STRANKY,array("obrazek" => $file),"idDomeny=".$domain->getId()." AND idStranky=".$idStranky." AND typ='clanek'");

}

//pokud editor prepne na nejaky novy stav bude odeslan email redaktorovi (zakladateli clanku)
if(ODESILAT_UPOZORNENI_PRI_ZMENE_STAVU_CLANKU && $login_obj->minPrivilege('editor') && $stary_stav != '' && $stary_stav != $stav && in_array($stav, array('rozpracovano','hotovo','zamitnuto')))
{
    $vsechny_stavy = array('rozpracovano' => TROZPRACOVANO, 'kontrola' => TKE_KONTROLE, 'zamitnuto' => TZAMITNUTO, 'hotovo' => THOTOVO);
    $data_autor = $db->query("SELECT s.idVytvoril, s.idRodice, s.nazev, DATE_FORMAT(s.datumVytvoril,'%d.%m.%Y %h:%I'), s.jmenoVytvoril, u.email 
        FROM ".TABLE_STRANKY." AS s
        LEFT JOIN ".TABLE_UZIVATELE." AS u ON s.idVytvoril = u.idUzivatele
        WHERE idStranky=".$idStranky."
        LIMIT 1
        ");

    if($db->numRows($data_autor) > 0)
    {
        $u = $db->getObject($data_autor);
  
        if($u->idVytvoril != $login_obj->getId())
        {
            $message = "Dobrý den,<br /><br />";
            $message.= "Vámi vytvořený článek s názvem <a href='".PROTOKOL.$domain->getName().get_link('articles','rubriky',$u->idRodice,'edit-content',$idStranky)."'><b>".$u->nazev."</b></a> byl vydavatelem ".$login_obj->getName()." přepnut do stavu: <b>".$vsechny_stavy[$stav]."</b>.<br />";
            if($poznamka != '')
                $message .= "K článku byla zapsána redakční poznámka: <i>".$poznamka."</i>";  
                
            $predmet = "Změna stavu článku: ".$u->nazev;
            
            SendMail($login_obj->getEmail(), $login_obj->getName(), $u->email, $predmet, $message);        
        }
    }
}

$log->add_log(get_int_post('idStranky') == 0 ? 'create':'edit-content','clanek',$idStranky,$titulek);

$redirect_url = UrlPage($url,$idStranky,WEB_LANG);
    
$result["redirect"] = $redirect_url;

if($redirect_url == "")
    $result["messages"]["ok"] = OK_ULOZENO_A_PRESMEROVANO;//OK_ULOZENO;
    else
    {
    $result["messages"]["ok"] = OK_ULOZENO_A_PRESMEROVANO;
    }

echo array2json($result);
exit; 

?>