<?php

/**
 * @author Pavel Srom
 * @copyright 2010
 */


//skript nacita html kod nahledu fotek pro cast modulu galerie - obsah galerie pro funkci kopirovani obrazku

if(!is_post('id_gallery') || !defined("SECURITY_CMS")) exit;

$idGalerie = get_int_post('id_gallery');

if($idGalerie==0) exit;

$data = $db->query("SELECT idStranky AS id, obrazek AS url, nazev 
    FROM ".TABLE_STRANKY." 
    WHERE idDomeny=".$domain->getId()." 
        AND idRodice='".$idGalerie."'
    ");

$html_template = "
    <a class='nahled_obrazku' id='gn{id}'>
    <img src='{url}' alt='{nazev}' title='{nazev}'/>
    </a>
    ";

$html = "";
while($img = $db->getAssoc($data))
{
    $url = RELATIVE_PATH.$domain->getDir().USER_DIRNAME_GALLERY_MINI.$img['url'];
    $html_zal = $html_template;
    $html_zal = str_replace("{id}",$img['id'],$html_zal);
    $html_zal = str_replace("{nazev}",$img['nazev'],$html_zal);
    $html_zal = str_replace("{url}",$url,$html_zal);
   
    $html .= $html_zal;
}

$html .= "<div class='cleaner'></div>";
echo $html;
	
exit;

?>