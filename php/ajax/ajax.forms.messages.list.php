<?php

/**
 * @author Pavel Srom
 * @copyright 2010
 */

//ajaxovy skript na odhlaseni kontaktu z odberu newsletteru

if(!isset($_GET['module']) || !defined("SECURITY_CMS")) exit;

$vyraz = trim($db->secureString($_GET['vyraz']));
$formular = isset($_GET['formular']) ? intval($_GET['formular']) : 0;
$stav = isset($_GET['stav']) ? $db->secureString($_GET['stav']) : "0";
$module = trim($_GET['module']);

//nastaveni limitu
$limit = "";
if(isset($_GET['iDisplayLength'])){
    $dStart = "";
    if(isset($_GET['iDisplayStart']))
        $dStart = abs(intval($_GET['iDisplayStart'])).",";
        
    $limit = " LIMIT ".$dStart.abs(intval($_GET['iDisplayLength'])); 
}


//trideni podle sloupcu
$order = "";
if(isset($_GET['iSortCol_0'])){
    $s = abs(intval($_GET['iSortCol_0']));
    if($s == 0) $order = "z.idZpravy";
    elseif($s == 1) $order = "z.email";
    elseif($s == 2) $order = "z.datum";
    elseif($s == 3) $order = "z.stav";
    else $order = "z.idZpravy";
}

if($order != '') 
    $order = "ORDER BY ".$order." ".(isset($_GET['sSortDir_0'])?$_GET['sSortDir_0']:'');

//echo $order;

//sestaveni podminky pro vyhledavani

$like = array();
if($vyraz != '')
{
    $like[] = "(z.idZpravy LIKE '".$vyraz."%' OR z.email LIKE '%".$vyraz."%')";
}
if($stav != "0")
{
    $like[] = "z.stav='".$stav."'"; 
}
if($formular != 0)
{
    $like[] = "z.idFormulare='".$formular."'"; 
}
    



$where = "WHERE f.idDomeny=".$domain->getId().((count($like) > 0) ? " AND (".implode(" AND ",$like).")" : "");

$data_pocet = $db->Query("SELECT COUNT(idZpravy) AS celkem
    FROM ".TABLE_FORMULARE." AS f 
    LEFT JOIN ".TABLE_FORMULARE_ZPRAVY." AS z ON f.idFormulare=z.idFormulare 
    ".$where."
    ");
    
$iTotal_arr = $db->getAssoc($data_pocet);
$iTotal = $iTotal_arr['celkem'];


        
$data = $db->Query("SELECT SQL_CALC_FOUND_ROWS z.*, f.nazev, 
			IF(datum IS NULL, NULL, DATE_FORMAT(z.datum,'%d.%m.%Y')) AS datum_prijeti 
			FROM ".TABLE_FORMULARE_ZPRAVY." AS z 
			LEFT JOIN ".TABLE_FORMULARE." AS f ON z.idFormulare=f.idFormulare 
			".$where."
            ".$order."
            ".$limit."
            ");


$iFilteredTotal_data = $db->Query("SELECT FOUND_ROWS()");
$iFilteredTotal_arr = $db->getRow($iFilteredTotal_data);
$iFilteredTotal = intval($iFilteredTotal_arr[0]);


$dis_content = !$login_obj->UserPrivilege('content_view');
$dis_delete = !$login_obj->UserPrivilege('settings_delete');
$dis_settings = !$login_obj->UserPrivilege('settings_view');

$tr = array();
while($c = $db->getAssoc($data))
{
    $ids = $c['idZpravy'];
    $pom = array();
    $pom[] = json_encode($ids);
    $pom[] = json_encode(secureString($c['email']));
    $pom[] = json_encode($c['datum_prijeti']);
    $stav = $c['stav'];
    $stav_flag = "";
    if($stav=='nova') 
        $stav_flag = "<span class='stav0'>".TNOVA."</span>";
    elseif($stav=='prijata') 
        $stav_flag = "<span class='stav2'>".TPRIJATA."</span>";
    elseif($stav=='odpovezeno') 
        $stav_flag = "<span class='stav1'>".TODPOVEZENA."</span>";
				
                
    $pom[] = json_encode($stav_flag);
    
    $ikony = "";
    
    if($dis_settings)
        $ikony .=  icon_disabled('setting');
        else
        $ikony .= "<a href='".get_link($module,'zpravy',0,'edit-content',$ids)."'>".icon('setting')."</a>";
    
    if($dis_delete)
        $ikony .= icon_disabled('delete');
        else
        $ikony .= "<a href=\"javascript:confirm_url('".sprintf(TOPRAVDU_SMAZAT, "ID ".$ids)."?', '".get_link($module,'zpravy',0,'delete',$ids)."')\">".icon('delete')."</a>";
        
    $pom[] = json_encode($ikony);
        
    $tr[] = "[".implode(',', $pom)."]";
    
}

echo "{";
echo '"sEcho": '.(intval($_GET['sEcho'])).', ';
echo '"iTotalRecords": '.$iTotal.', ';
echo '"iTotalDisplayRecords": '.($iFilteredTotal).', ';
echo '"aaData": ['.implode(',', $tr).'] }';


?>