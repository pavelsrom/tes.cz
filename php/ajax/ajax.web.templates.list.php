<?php

/**
 * @author Pavel Srom
 * @copyright 2013
 */



if(!defined("SECURITY_CMS")) exit;

$data = $db->query("SELECT * FROM ".TABLE_HTML_SABLONY." WHERE idDomeny IN (0,".$domain->getId().") ORDER BY nazev");

/*
$data = $db->query("SELECT * FROM ".TABLE_HTML_SABLONY." WHERE idDomeny IN (0,".$domain->getId().") AND idJazyka=".WEB_LANG_ID." ORDER BY nazev");
*/

$result = array();
while($s = $db->getObject($data))
{
    $result[] = array("title" => $s->nazev, "description" => $s->popis, "html" => $s->obsah);
}

?>

CKEDITOR.addTemplates("default",{imagesPath:CKEDITOR.getUrl(CKEDITOR.plugins.getPath("templates")+"templates/images/"),templates:<?php echo array2json($result);?>});