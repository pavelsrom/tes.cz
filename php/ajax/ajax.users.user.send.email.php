<?php

/**
 * @author Pavel Srom
 * @copyright 2011
 */


if(!is_post('text') || !is_post('predmet') || !defined("SECURITY_CMS")) exit;

$id = get_int_post('id');
$html = get_post('text',false);
$odesilatel_nazev = FROM_ADMIN_NAME;
$odesilatel_email = FROM_ADMIN_EMAIL;
$prijemce = $db->get(TABLE_UZIVATELE, 'email',"idUzivatele=".$id);
$predmet = get_secure_post('predmet');

$error = array();

if(!TestEmail($odesilatel_email, 250, false))
    $error[] = ERROR_EMAIL_ODESILATELE;
    
if(!TestEmail($prijemce, 250, false))
    $error[] = ERROR_EMAIL_PRIJEMCE;
  
$res = false;
if(count($error)==0)
   $res = SendMail($odesilatel_email, $odesilatel_nazev,$prijemce, $predmet, $html, strip_tags($html));
    
$ok = "";
if($res)
	$ok = OK_ODESLANO;
	else
    $error[] = ERROR_ODESLANO;

$chyby = json_encode("");
if(count($error)>0)
    $chyby = json_encode(implode('<br />',$error));

if($ok!='')
    $ok = json_encode($ok);
    else
    $ok = json_encode("");    

echo "
{
    \"error\": ".$chyby.",
    \"ok\": ".$ok." 
    
}
";


?>