<?php

/**
 * @author Pavel Srom
 * @copyright 2010
 */

//ajaxovy skript na odhlaseni kontaktu z odberu newsletteru

if(!isset($_GET['module']) || !defined("SECURITY_CMS")) exit;

$limit = 10;//intval($_GET['limit']);
$vyraz = isset($_GET['q']) ? $db->secureString($_GET['q']) : "";
$module = trim($_GET['module']);

$result = array();

if ($vyraz != "" && $limit > 0)
	{
	
    $data = $db->query("SELECT idStranky AS id, nazev 
            FROM ".TABLE_STRANKY."  
            WHERE typ = 'tag'
                AND idDomeny='".$domain->getId()."'
                AND idJazyka='".WEB_LANG_ID."'
                AND nazev LIKE '%".$vyraz."%' 
            LIMIT 100"
            );
    
    $i = 1;
    while($r = $db->getObject($data))
    {
        if($i == $limit) 
            break;

        $result[] = array("value" => $r->id, "name" => $r->nazev, "image" => "");
        $i++;
    }

}

echo array2json($result);


?>