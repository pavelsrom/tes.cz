<?php

/**
 * @author Pavel Srom
 * @copyright 2015
 */


$html = '<form method="post" action="#" id="ine_find_form">
    <div>
        <input type="text" id="ine_find_input" placeholder="'.TZADEJTE_HLEDANY_VYRAZ.'" name="vyraz"  autocomplete="off"/>
    </div>   
    <div id="ine_find_result">
    
    </div> 
    </form>';

$result = array(
    "html" => $html,
    "url" => array(
        "autocomplete" => get_link_ajax('inlineedit','find.pages',$idModulu)
        ),
    "messages" => array()
    );

echo array2json($result);


?>