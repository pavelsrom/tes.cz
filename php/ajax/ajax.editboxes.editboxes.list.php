<?php

/**
 * @author Pavel Srom
 * @copyright 2010
 */

//ajaxovy skript na odhlaseni kontaktu z odberu newsletteru

if(!isset($_GET['module']) || !defined("SECURITY_CMS")) exit;

$vyraz = trim($db->secureString($_GET['vyraz']));
$module = trim($_GET['module']);

//nastaveni limitu
$limit = "";
if(isset($_GET['iDisplayLength'])){
    $dStart = "";
    if(isset($_GET['iDisplayStart']))
        $dStart = abs(intval($_GET['iDisplayStart'])).",";
        
    $limit = " LIMIT ".$dStart.abs(intval($_GET['iDisplayLength'])); 
}


//trideni podle sloupcu
$order = "";
if(isset($_GET['iSortCol_0'])){
    $s = abs(intval($_GET['iSortCol_0']));
    if($s == 0) $order = "e.idEditboxu";
    elseif($s == 1) $order = "e.nazev";
    elseif($s == 2) $order = "e.stridani";
    elseif($s == 3) $order = "e.zobrazeno";
    elseif($s == 4) $order = "e.prokliky";
    elseif($s == 5) $order = "pomer";
    elseif($s == 6) $order = "e.zobrazit";
    else $order = "e.idEditboxu";
}

if($order != '') 
    $order = "ORDER BY ".$order." ".(isset($_GET['sSortDir_0'])?$_GET['sSortDir_0']:'');

//sestaveni podminky pro vyhledavani
$like = array();
if($vyraz != '')
{
    $like[] = "e.idEditboxu LIKE '".$vyraz."%'";
    $like[] = "e.nazev LIKE '%".$vyraz."%'";
} 

$where = "WHERE e.idJazyka=".WEB_LANG_ID." AND e.idDomeny=".$domain->getId().((count($like) > 0) ? " AND (".implode(" OR ",$like).")" : "");

$data_pocet = $db->Query("SELECT COUNT(idEditboxu) AS celkem FROM ".TABLE_EDITBOXY." AS e ".$where);
$iTotal_arr = $db->getAssoc($data_pocet);
$iTotal = $iTotal_arr['celkem'];

$data = $db->Query("SELECT SQL_CALC_FOUND_ROWS e.*, COUNT(ep.idEditboxu) AS pocet_boxu,
                IF(e.od >= CURDATE() OR e.do <= CURDATE() OR (COUNT(ep.idEditboxu) = 0 AND e.stridani!='zadne') OR e.zobrazit=0, 0, 1) AS aktivni, e.povolit_mazani, 
                e.zobrazeno, e.prokliky,
                IF(e.zobrazeno > 0, ROUND(e.prokliky / e.zobrazeno, 2), 0) AS pomer
			FROM ".TABLE_EDITBOXY." AS e 
			LEFT JOIN ".TABLE_EDITBOXY_POLOZKY." AS ep ON e.idEditboxu=ep.idKolotoce 
            LEFT JOIN ".TABLE_EDITBOXY." AS e1 ON ep.idEditboxu=e1.idEditboxu
			".$where." 
			GROUP BY e.idEditboxu
			".$order."
            ".$limit."
			");

$iFilteredTotal_data = $db->Query("SELECT FOUND_ROWS()");
$iFilteredTotal_arr = $db->getRow($iFilteredTotal_data);
$iFilteredTotal = intval($iFilteredTotal_arr[0]);

$dis_content = !$login_obj->UserPrivilege('content_view');
$dis_delete = !$login_obj->UserPrivilege('content_delete');
$dis_settings = !$login_obj->UserPrivilege('content_view');

$tr = array();
while($c = $db->getAssoc($data))
{
    $ids = $c['idEditboxu'];
    $stridani = $c['stridani'];
    $pocet_boxu = intval($c['pocet_boxu']);
    if($stridani == 'zadne') $stridani_text = TEDITBOX;//TZADNE;
    elseif($stridani == 'nahodne') $stridani_text =  TKOLOTOC_NAHODNE." (".TPOCET_BOXU.": ".$pocet_boxu.")";
    elseif($stridani == 'kolotoc') $stridani_text =  TKOLOTOC." (".TPOCET_BOXU.": ".$pocet_boxu.")";
    elseif($stridani == 'samorotujici') $stridani_text =  TSAMOROTUJICI_KOLOTOC." (".TPOCET_BOXU.": ".$pocet_boxu.")";
    else $stridani_text = "";
    
    
    $pom = array();
    //$pom["DT_RowClass"] = $stridani != "zadne"? "zvyrazni_radek" : "";
    $pom[] = $ids;
    $pom[] = ($stridani == "zadne" ? icon('editbox',"","icon_box") : icon('editbox-kolotoc',"","icon_box"))."&nbsp;&nbsp;".secureString($c['nazev']);
    $pom[] = $stridani_text;
    $pom[] = $c['zobrazeno'];
    $pom[] = $c['prokliky'];
    $pom[] = $c['pomer'];
    $pom[] = ano_ne($c['zobrazit']);
    
    
    $ikony = "";
    
    if($dis_settings)
        $ikony .=  icon_disabled('setting');
        else
        if($stridani == 'zadne')
            $ikony .=  "<a href='".get_link($module,'editboxy',0,'edit-content',$ids)."'>".icon('content')."</a>";
            else
            $ikony .=  "<a href='".get_link($module,'editboxy',0,'edit-content-kolotoc',$ids)."'>".icon('content')."</a>";
    
    if($c["povolit_mazani"] == 0 && !$login_obj->minPrivilege("admin"))
        $ikony .= icon_disabled('delete',TMAZANI_BOXU_BYLO_ZAKAZANO_ADMINEM);
    elseif($dis_delete || !$object_access->has_access($ids,"editbox"))
        $ikony .= icon_disabled('delete');
    else
        $ikony .= "<a href='#' id='delete".$ids."' class='delete'>".icon('delete')."</a>";
        
    $pom[] = ($ikony);
        
    $tr[] = array2json($pom);
    
}

echo "{";
echo '"sEcho": '.(intval($_GET['sEcho'])).', ';
echo '"iTotalRecords": '.$iTotal.', ';
echo '"iTotalDisplayRecords": '.($iFilteredTotal).', ';
echo '"aaData": ['.implode(',', $tr).'] }';


?>