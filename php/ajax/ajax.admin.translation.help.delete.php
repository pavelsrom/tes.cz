<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

//sleep(5);

if(!defined("SECURITY_CMS")) exit;
if(!$login_obj->UserPrivilege('settings_delete','superadmin')) exit;

$id = get_int_post('id');

if($id <= 0) exit;

$nazev = $db->get(TABLE_NAPOVEDA,'nadpis','idNapovedy='.$id);  
$log->add_log('delete',"napoveda",$id,$nazev);

$data = $db->delete(TABLE_NAPOVEDA, "WHERE idNapovedy=".$id." LIMIT 1");



exit;

?>