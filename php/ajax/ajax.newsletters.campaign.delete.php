<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */
 
 
if(!$login_obj->UserPrivilege('content_delete') || !defined("SECURITY_CMS")) exit;


$id = get_int_post('id');

if($id <= 0) exit;

$nazev = $db->get(TABLE_EMAILY_KAMPANE,"nazev", "idKampane=".$id." AND idDomeny=".$domain->getId());
if($nazev == "") 
    exit;

$db->delete(TABLE_EMAILY_KAMPANE, "idKampane=".$id);
$db->delete(TABLE_EMAILY_SKUPINY_KAMPANE, "idKampane=".$id);


$data = $db->query("SELECT soubor FROM ".TABLE_EMAILY_PRILOHY." WHERE idKampane=".$id);
while($s = $db->getObject($data))
    @unlink(PRE_PATH.$domain->getDir().USER_DIRNAME_ATTACHMENT.$soubor);


$db->delete(TABLE_EMAILY_PRILOHY, "idKampane=".$id);

$log->add_log('delete',"newsletter-kampane",$id,$nazev);

?>