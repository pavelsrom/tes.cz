<?php

/**
 * @author Pavel Srom
 * @copyright 2010
 */


if(!isset($_GET['module']) || !defined("SECURITY_CMS")) exit;

$vyraz = trim($db->secureString($_GET['vyraz']));
$module = trim($_GET['module']);

//nastaveni limitu
$limit = "";
if(isset($_GET['iDisplayLength'])){
    $dStart = "";
    if(isset($_GET['iDisplayStart']))
        $dStart = abs(intval($_GET['iDisplayStart'])).",";
        
    $limit = " LIMIT ".$dStart.abs(intval($_GET['iDisplayLength'])); 
}


//trideni podle sloupcu
$order = "";
if(isset($_GET['iSortCol_0'])){
    $s = abs(intval($_GET['iSortCol_0']));
    if($s == 0) $order = "u.idUzivatele";
    elseif($s == 1) $order = "CONCAT(u.jmeno,u.prijmeni)";
    elseif($s == 2) $order = "u.login";
    elseif($s == 3) $order = "u.email";
    elseif($s == 4) $order = "prava";
    elseif($s == 5) $order = "u.posledniPristup";
    elseif($s == 6) $order = "u.posledniPristupWeb";
    elseif($s == 7) $order = "u.aktivni";
    else $order = "u.idUzivatele";
}

if($order != '') 
    $order = "ORDER BY ".$order." ".(isset($_GET['sSortDir_0'])?$_GET['sSortDir_0']:'');

//sestaveni podminky pro vyhledavani
$like = array();
if($vyraz != '')
{
    $like[] = "u.idUzivatele LIKE '".$vyraz."%'";
    $like[] = "u.email LIKE '%".$vyraz."%'";
    $like[] = "u.login LIKE '%".$vyraz."%'";
    $like[] = "CONCAT(u.jmeno,u.prijmeni) LIKE '%".$vyraz."%'";
}
    
    
$idPravaSuperAdmin = $db->get(TABLE_CMS_PRAVA,'idPrava','systemovyNazev="superadmin"');
if($login_obj->UserPrivilege('admin'))
    $like[] = "u.idPrava != ".$idPravaSuperAdmin;


if(count($like) > 0)
    $where = "WHERE ".implode(" OR ",$like);
    else
    $where = "";




$data_pocet = $db->Query("SELECT COUNT(idUzivatele) AS celkem FROM ".TABLE_UZIVATELE." AS u ".$where);
$iTotal_arr = $db->getAssoc($data_pocet);
$iTotal = $iTotal_arr['celkem'];

$data = $db->Query("SELECT SQL_CALC_FOUND_ROWS u.*, p.nazev AS prava, 
                IF(posledniPristup IS NULL, '".TDOSUD_NEPRIHLASEN."', DATE_FORMAT(posledniPristup,'%d.%m.%Y %H:%i')) AS datum,  
                IF(posledniPristupWeb IS NULL, '".TDOSUD_NEPRIHLASEN."', DATE_FORMAT(posledniPristupWeb,'%d.%m.%Y %H:%i')) AS datumWeb 
				FROM ".TABLE_UZIVATELE." AS u 
				LEFT JOIN ".TABLE_CMS_PRAVA." AS p ON u.idPrava=p.idPrava
				".$where." 
                ".$order."
                ".$limit."
                
                ");


$iFilteredTotal_data = $db->Query("SELECT FOUND_ROWS()");
$iFilteredTotal_arr = $db->getRow($iFilteredTotal_data);
$iFilteredTotal = intval($iFilteredTotal_arr[0]);

$dis_delete = !$login_obj->UserPrivilege('settings_delete') || !$login_obj->minPrivilege('admin');
$dis_settings = !$login_obj->UserPrivilege('settings_view') || !$login_obj->minPrivilege('admin');

$tr = array();
while($c = $db->getAssoc($data))
{
    
    $ids = $c['idUzivatele'];
    $pom = array();
    $pom[] = json_encode($ids);
    $pom[] = json_encode(trim(secureString($c['jmeno'])." ".secureString($c['prijmeni'])));
    $pom[] = json_encode($c['login']);
    $pom[] = json_encode($c['email']);
    $pom[] = json_encode(constant($c['prava']));
    $pom[] = json_encode($c['datum']);
    $pom[] = json_encode($c['datumWeb']);
    $pom[] = json_encode(ano_ne($c['aktivni']));
    
    $ikony = "";
    
    /*
    if($login_obj->minPrivilege('admin'))
        $ikony .= "<a href='".get_link($module,'uzivatele',0,'navstevy',$ids)."'>".icon('statistics')."</a>";
        else
        $ikony .= icon_disabled('statistics','');
    */    
    
    if($dis_settings && !(($login_obj->UserPrivilege('user') || $login_obj->UserPrivilege('editor')) && $login_obj->getId() == $ids))
        $ikony .= icon_disabled('setting');
        else
        $ikony .= "<a href='".get_link($module,'uzivatele',0,'edit-settings',$ids)."'>".icon('setting')."</a>";

    if($dis_delete)
        $ikony .= icon_disabled('delete');
        else
        $ikony .= "<a href=\"#\" id='delete".$ids."' class='delete'>".icon('delete')."</a>";
        
    $pom[] = json_encode($ikony);
        
    $tr[] = "[".implode(',', $pom)."]";
    
}

echo "{";
echo '"sEcho": '.(intval($_GET['sEcho'])).', ';
echo '"iTotalRecords": '.intval($iTotal).', ';
echo '"iTotalDisplayRecords": '.($iFilteredTotal).', ';
echo '"aaData": ['.implode(',', $tr).'] }';


?>