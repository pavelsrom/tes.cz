<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */
 
 
if(!$login_obj->UserPrivilege('settings_delete') || !defined("SECURITY_CMS")) exit;


$id = get_int_post('id');

if($id <= 0) exit;

$nazev = $db->get(TABLE_PANELY_SABLONY,'nazev','idProfilu='.$id);
if($nazev == "") exit;

$log->add_log('delete',"sablona-panelu",$id,$nazev);

$cond = "";
if(!$login_obj->UserPrivilege("superadmin"))
    $cond = " AND vychozi=0";

$id = $db->get(TABLE_PANELY_SABLONY,"idProfilu","idProfilu=".$id." AND idDomeny=".$domain->getId().$cond);
$id = intval($id);

$result = array("ok" => "", "error" => "");
if($id > 0)
{
    $db->delete(TABLE_PANELY_SABLONY, "WHERE idProfilu=".$id." AND idDomeny=".$domain->getId().$cond);
    $db->delete(TABLE_PANELY_POLOZKY, "WHERE idProfilu=".$id);
    $result['ok'] = OK_SMAZANO;
}
else
$result['error'] = ERROR_NELZE_SMAZAT;

echo array2json($result);

exit;


?>