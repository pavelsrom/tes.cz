<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!$login_obj->UserPrivilege('content_delete') || !defined("SECURITY_CMS")) exit;

$idNews = get_int_post('id');

if($idNews <= 0 || !$object_access->has_access($idNews)) exit;
			
$data = $db->Query("SELECT obrazek2 AS obrazek, nazev FROM ".TABLE_STRANKY."
				WHERE idDomeny=".$domain->getId()."
					AND idStranky=".$idNews."
				LIMIT 1");	
                
if($db->numRows($data)==0 || $idNews==0) exit;
    
$obr = $db->getAssoc($data);
$nazev = $obr['nazev'];
if($obr['obrazek']!='')
{
    @unlink(PRE_PATH.$domain->getDir().USER_DIRNAME_HLAVNI_OBRAZEK2.$obr['obrazek']);
}
    
    
				
$db->Query("UPDATE ".TABLE_STRANKY." SET obrazek2='' WHERE idStranky=".$idNews." AND idDomeny=".$domain->getId()." LIMIT 1");
			
$log->add_log('delete','stranka-fotka',$idNews,$nazev);

exit;

?>