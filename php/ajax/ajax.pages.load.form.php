<?php

/**
 * @author Pavel Srom
 * @copyright 2014
 */


include("../classes/class.form.admin.php");
include("../functions/functions.inputs.php");

/*
if((!$login_obj->UserPrivilege('content_view') && $idStranky > 0)){
    $result["messages"]["error"] = ERROR_NEDOSTATECNA_PRAVA;
    echo array2json($result);
    exit;
}
*/


$idPage = get_int_post("id_page");
$action = get_post('ine_action');

if($action == 'pridat-stranku')
    $idPage = 0;

$page = array();
$page['nazev'] = "";
$page['typ'] = "stranka";
$page['nadpis'] = "";
$page['uvodni'] = 0;
$page['autentizace'] = 0;
$page['idRodice'] = 0;
$page['idProfiluPanelu'] = 0;
$page['zobrazit'] = 0;
$page['doMenu'] = 1;
$page['obsah'] = "";
$page['idGalerie'] = 0;
$page['idAnkety'] = 0;
$page['idFormulare'] = 0;
$page['diskuze'] = 0;
$page['url'] = "";
$page['title'] = "";
$page['description'] = "";
$page['nazevProMenu'] = "";







if($idPage > 0) {
    $data = $db->Query("SELECT s.*, j.jazyk,
				IF(s.nadpis='', s.nazev, s.nadpis) AS nadpis,
                IF(s.od IS NULL, '', DATE_FORMAT(s.od,'%d.%m.%Y')) AS publikovat_od,
				IF(s.do IS NULL, '', DATE_FORMAT(s.do,'%d.%m.%Y')) AS publikovat_do,
				IF(s.datum IS NULL, '', DATE_FORMAT(s.datum,'%d.%m.%Y')) AS datum
			FROM ".TABLE_STRANKY." AS s
            LEFT JOIN ".TABLE_JAZYKY." AS j ON s.idJazyka = j.idJazyka 
			WHERE idStranky=".$idPage." 
				AND idDomeny=".$domain->getId()."
                AND s.idJazyka=".WEB_LANG_ID."
			LIMIT 1");
			
			
    $page = $db->getAssoc($data);
    $page['publikovat_od'] = $page['publikovat_od'] == '00.00.0000' ? "" : $page['publikovat_od'];
    $page['publikovat_do'] = $page['publikovat_do'] == '00.00.0000' ? "" : $page['publikovat_do'];  
}

$typ = array(
    'stranka' => TSTANDARDNI_STRANKA, 
    "prazdna-stranka" => TPRAZDNA_STRANKA
    );


$form = new Form();
$form->allow_upload();
//$form->set_jquery_ui_css();

//osetreni vypnuteho modulu Clanky
if(MODULE_ARTICLES)
{
    $typ["clanky"] = TSTRANKA_S_CLANKY;
}
else
{
    $form->add_hidden('typ','clanky');   
}


$form->add_hidden('idStranky',$idPage); 

$form->add_section(TZAKLADNI_UDAJE, "home");

$form->add_text(TNAZEV, 'nazev', $page['nazev'], 0, true, "", "nazev"); 

if($page['typ']!='404')
{
    $form->add_text(TNAZEV_STRANKY_V_MENU, 'menu_nazev', $page['nazevProMenu'], 0, true, "", "menu_nazev");        
    //overeni zda uz stranka s novinkami nebo galerii existuje - muze byt vytvorena jen jedna stranka s novinkami
    $dis_novinky = false;
    $dis_galerie = false;
    $dn = $db->Query("SELECT typ FROM ".TABLE_STRANKY." WHERE idDomeny=".$domain->getId()." AND (typ='novinky' OR typ='stranka-galerie') AND idJazyka=".WEB_LANG_ID);
            
    while($dna = $db->getAssoc($dn))
    {
        if($dna['typ']=='novinky') $dis_novinky = true;
        if($dna['typ']=='stranka-galerie') $dis_galerie = true;
    }
    
                
    $dis_clanky = false;
    if($idPage>0 && $page['typ']=='clanky')
    {
        $dc = $db->Query("SELECT idStranky FROM ".TABLE_STRANKY." WHERE idDomeny=".$domain->getId()." AND typ='clanek' AND idRodice=".$idPage." AND idJazyka=".WEB_LANG_ID." LIMIT 1");
                
        if($db->numRows($dc)>0)
            $dis_clanky = true;
                
    }
                 
    $radiobutton_typy = "";  
         
    if(in_array($page['typ'], array_keys($typ)))
    {
        $i = 0;
        foreach($typ AS $n => $t)
        {
            if($n == $page['typ']) 
                $ch = "checked='checked'"; 
                else 
                $ch = "";
                
            $dis = "";
            if($n != 'clanky' && $dis_clanky) 
                $dis = "disabled='disabled'";
                  
            $radiobutton_typy .= "<input type='radio' class='radio typ_stranky' name='typ' value='".$n."' ".$ch." ".$dis." id='typ".$n."' /><label for='typ".$n."'>".$t."</label><br />";
            $i++;
        }
        $form->add_plain_text(TTYP, $radiobutton_typy ,137);
    }
    else
    {
        $t = "";
        if($page['typ'] == 'archiv-clanku') $t = TARCHIV_CLANKU;
        elseif($page['typ'] == 'stranka-galerie') $t = TSTRANKA_S_GALERII;
        elseif($page['typ'] == 'prazdna-stranka') $t = TPRAZDNA_STRANKA;
        elseif($page['typ'] == 'sitemap') $t = TMAPA_WEBU;
        elseif($page['typ'] == 'novinky') $t = TNOVINKY;
        elseif($page['typ'] == 'uvodni') $t = TUVODNI_STRANKA;
        elseif($page['typ'] == 'kalendar-akci') $t = TKALENDAR_AKCI;
        elseif($page['typ'] == 'tagy') $t = TSTRANKA_STITKY;
        elseif($page['typ'] == 'vyhledavani') $t = TSTRANKA_S_VYSLEDKY_VYHLEDAVANI;
        //elseif($page['typ'] == 'clanek') $t = TCLANEK;
        //elseif($page['typ'] == 'novinka') $t = TNOVINKA;
        //elseif($page['typ'] == 'akce') $t = TKALENDARNI_AKCE;
        $help = "";
        
        $form->add_hidden('typ',$page['typ']);
    
        $form->add_plain_text(TTYP, $t, $help);            
    }
        
    
    //umisteni stranek
    $selectbox_pages = array(0 => "-- ".TKOREN_WEBU." --") + get_pages($idPage);
    $form->add_selectbox(TUMISTENI, 'idRodice', $page['idRodice'], $selectbox_pages,298);
    
    $form->add_radiobuttons(TZARADIT_DO_MENU, 'menu', $page['doMenu'], array(1 => TANO, 0 => TNE), 106);
           
    /*        
    if(MODULE_PRIVATE_PAGES)
        $form->add_radiobuttons(TPRIVATNI_STRANKA, 'autentizace', $page['autentizace'], array(1 => TANO, 0 => TNE), 5)->set_class_tr("prazdna-stranka-hidden");
        else
        $form->add_hidden('autentizace',$page['autentizace']);
    */
    		
    if($page['typ']!='uvodni' && $idPage > 0)
        $form->add_radiobuttons(TZOBRAZOVAT, 'zobrazit', $page['zobrazit'], array(1 => TANO, 0 => TNE), 6);
    elseif($page['typ'] == 'uvodni')
        $form->add_hidden('zobrazit',1);
    else
        $form->add_hidden('zobrazit',0);
                
}           
elseif($page['typ']=='404')
{
    $form->add_hidden('typ',$page['typ']);
    $form->add_hidden('idRodice',0);
}

if($idPage>0)
    $form->add_hidden('stary_typ',$page['typ']);

$form->add_section(TDOPLNUJICI_OBSAH,"obsah");

/*
if($page['typ']!='404' && $page['typ']!='vyhledavani' && MODULE_INQUIRY)
{
    $d = $db->Query("SELECT idAnkety AS id, nazev FROM ".TABLE_ANKETY." WHERE idDomeny=".$domain->getId());
    
    $sel = array(0 => "-- ".TVYBERTE." --");
    $pocet = $db->numRows($d);
            
    while($s = $db->getAssoc($d))
        $sel[$s['id']] = $s['nazev'];
                
    if(count($sel) == 1){
        $text_neni = "";
        if($login_obj->UserPrivilege('content_add'))
            $text_neni .= tlacitko(get_link('inquiry',"ankety",0,'new'));
        
        $form->add_plain_text(TANKETA, $text_neni)->set_class_tr("prazdna-stranka-hidden");    
        }
        else
        $form->add_selectbox(TANKETA, 'idAnkety', $page['idAnkety'], $sel)->set_class_tr("prazdna-stranka-hidden");
            
}
else
    $form->add_hidden('idAnkety', $page['idAnkety']);
*/    
    
    
if($page['typ']!='404' && $page['typ']!='vyhledavani' && MODULE_GALLERY)
{           
    $sel = ShowSelectBoxGallery();
    $sel = array(0 => "-- ".TVYBERTE." --") + $sel;

    if(count($sel) == 1){
        $text_neni = "";
        if($login_obj->UserPrivilege('content_add'))
            $text_neni .= tlacitko(get_link('gallery',"galerie",0,"new"));
        $form->add_plain_text(TGALERIE, $text_neni)->set_class_tr("prazdna-stranka-hidden");

        }
        else
        $form->add_selectbox(TGALERIE, 'idGalerie', $page['idGalerie'], $sel)->set_class_tr("prazdna-stranka-hidden");          
}
else
    $form->add_hidden('idGalerie', $page['idGalerie']);
    
    
/*   
if($page['typ']!='404' && $page['typ']!='vyhledavani' && MODULE_DISCUSSION)
    $form->add_radiobuttons(TDISKUZE, 'diskuze', $page['diskuze'], array(1 => TANO, 0=>TNE))->set_class_tr("prazdna-stranka-hidden");
    else
    $form->add_hidden('diskuze', $page['diskuze']);
*/   
   
   
/*
$data_panely = $db->Query("SELECT idProfilu,nazev FROM ".TABLE_PANELY_SABLONY." WHERE idDomeny=".$domain->getId()." AND zobrazit=1 AND idJazyka=".WEB_LANG_ID." ORDER BY vychozi DESC ");
                
$profily = array();
while($profil = $db->getAssoc($data_panely))
    $profily[$profil['idProfilu']] = $profil['nazev'];

$form->add_section(TSABLONA_PANELU, 'panel-setting')->set_class_tr("prazdna-stranka-hidden");
$form->add_selectbox(TSABLONA_PANELU, 'idProfiluPanelu', $page['idProfiluPanelu'], $profily, 135)->set_class_tr("prazdna-stranka-hidden");     
*/

    
if($login_obj->UserPrivilege('seo'))
{
    $form->add_section(TSEO_PARAMETRY, 'seo', "", 8);
    //$form->add_text(TURL_NAZEV, 'url', $page['url'], 9, $idPage > 0);
    $form->add_hidden('url', $page['url']);
    $form->add_text(TTITLE, 'title', $page['title'], 10,false,'','title');
    $form->add_textarea(TDESCRIPTION, 'description', $page['description'], 11);	
}
else
{
    $form->add_hidden('url', $page['url']);
    $form->add_hidden('title', $page['title'], 'title');
    $form->add_hidden('description', $page['description']);	
}       

$form->add_submit();

$result = array(
    "html" => $form->get_html_code(),
    "url" => array(
        "save" => get_link_ajax('pages','page.save.form',$idModulu)
        ),
    "messages" => array()
    );

echo array2json($result);
 
    
    
    
    
    
    
    


?>