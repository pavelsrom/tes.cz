<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */


if(!defined("SECURITY_CMS")) exit;

$data = $db->query("SELECT u.*, p.nazev AS prava FROM ".TABLE_UZIVATELE." AS u
    LEFT JOIN ".TABLE_CMS_PRAVA." AS p ON u.idPrava = p.idPrava 
    WHERE u.idUzivatele=".$login_obj->getId()." 
    LIMIT 1
    ");
if($db->numRows($data) == 0) exit;
$user = $db->getAssoc($data);

$jazyky = implode(",",$domain->get("jazyky"));
$data = $db->query("SELECT jazyk FROM ".TABLE_JAZYKY." WHERE idJazyka IN (".$jazyky.") ORDER BY idJazyka");
$jazyky_seznam = array();
while($j = $db->getAssoc($data))
    $jazyky_seznam[] = strtoupper($j['jazyk']);

$jazyky = implode(", ",$jazyky_seznam);


$posledni_prihlaseni = $db->get(TABLE_LOG,"DATE_FORMAT(datum, '%d.%m.%Y %H:%i')", "idUzivatele=".$login_obj->getId()." AND akce='login' ORDER BY datum DESC","1,2");


?>


<table class="tZakladniInformace">
<tr>
    <td class="td_ikona user">
        <span class="fa fa-user fa-3x"></span>
    </td>
    <td>
        <p>
        <?php echo TPRIHLASEN;?>: <b><?php echo trim($user['jmeno']." ".$user['prijmeni']);?></b><br />
        <?php echo TEMAIL;?>: <b><?php echo $user['email'];?></b><br />
        <?php echo TPRAVA;?>: <b><?php echo constant($user['prava']);?></b><br />
        <?php echo TNAPOSLEDY_PRIHLASEN;?>: <b><?php echo $posledni_prihlaseni;?></b><br /><br />
        <a href="<?php echo get_link('home','osobniudaje'); ?>" title="<?php echo TUPRAVIT_OSOBNI_UDAJE;?>"><?php echo TUPRAVIT_OSOBNI_UDAJE;?></a>
        </p>
    </td>

    <td class="td_ikona web">
        <span class="fa fa-laptop fa-3x"></span>
    </td>
    <td>
        <p>
        <?php echo TWEB;?>: <b><?php echo $domain->getName();?></b><br />
        <?php echo TEMAIL;?>: <b><?php echo EMAIL_WEBU;?></b><br />
        <?php echo TPOVOLENE_JAZYKY;?>: <b><?php echo $jazyky;?></b><br />
        <?php echo TAKTIVNI;?>: <b><?php echo POVOLIT_INDEXACI_WEBU ? TANO : TNE;?></b><br /><br />
        <a href="<?php echo get_link('home','nastaveni',0,"",0,"",0,"","#home"); ?>" title="<?php echo TUPRAVIT_NASTAVENI_WEBU;?>"><?php echo TUPRAVIT_NASTAVENI_WEBU;?></a>
        </p>
    </td>
</tr>
</table>