<?php

/**
 * @author Pavel Srom
 * @copyright 2011
 */



include_once(PRE_PATH.'php/classes/class.gapi.php');
 
 
//-----------------------------------------------------------------
//cacheovani grafu
//-----------------------------------------------------------------
$posledni_zmena = mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y"));
$gmt_posledni_zmena = gmdate("D, d M Y H:i:s", $posledni_zmena)." GMT"; 

header('Content-Type: application/json');		
Header("Cache-Control: must-revalidate");
header("Pragma: private");
$offset = 60 * 60 * (24 - abs(date("H") - 24)); //den
$ExpStr = "Expires: " . gmdate("D, d M Y H:i:s", time() + $offset) . " GMT";
Header($ExpStr);
	
if(isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) && $_SERVER['HTTP_IF_MODIFIED_SINCE'] == $gmt_posledni_zmena){
	header('Last-Modified: '.$_SERVER['HTTP_IF_MODIFIED_SINCE'],true,304);
	exit;
	}

Header("Last-Modified: ".$gmt_posledni_zmena);
Header("Cache-Control: public");
//-----------------------------------------------------------------

if(!isset($_REQUEST['stranka']) || !isset($_REQUEST['typ']) || !isset($_REQUEST['od']) || !isset($_REQUEST['do']) || !defined("SECURITY_CMS")) exit;

//file_put_contents('test.txt', "test22");

$start = (isset($_REQUEST['od'])?$_REQUEST['od']:'');
$end = (isset($_REQUEST['do'])?$_REQUEST['do']:'');
$type = (isset($_REQUEST['typ'])?$_REQUEST['typ']:1);
$stranka = (isset($_REQUEST['stranka'])?intval($_REQUEST['stranka']):0);


$start = GetGADate($start);
$end = GetGADate($end);

if($type < 0 || $type > 4) return;
if($start=='' || strlen($start)!=10)
	if($end!=''){
		$start = $s = explode('-', $end);
		$start = $s[0].".".$s[1]."-01";
		}
		else 
		$start=strftime("%Y-%m-01", time());
        
if($end=='' || strlen($end)!=10) $end=strftime("%Y-%m-%d", time());


if(!$login_obj->UserPrivilege('statistics_view')) exit;

$error = "";

try{
	$ga = new gapi(GA_EMAIL,GA_HESLO);
	}
	catch(exception $e){  
	exit;	
	}
	
$idProfile = GA_PROFIL;
$maxRecords = GetMaxRecords($start, $end, $type);

//echo $maxRecords;
//file_put_contents('test.txt', $maxRecords);
$stranka = $links->getUrl($stranka) != "" ? $links->get_url($stranka) : "";

$filter = "pagePath==/".$stranka;
$metrics_visits = "pageviews";
$metrics_new_visits = "uniquePageviews";
    



//ziskani dat z google analytics
try{	
  switch($type){
	case 0: {
		$ga->requestReportData(
			$idProfile, 
			array('hour'),
			array($metrics_visits,$metrics_new_visits), 
			array('hour'), 
			$filter,
            null, 
			$start, 
			$end, 
			1, 
			$maxRecords
			);
		break;
		}
	case 1: {
		$ga->requestReportData(
			$idProfile, 
			array('date'),
			array($metrics_visits,$metrics_new_visits),
			array('date'), 
			$filter, 
            null,
			$start, 
			$end, 
			1, 
			$maxRecords
			);
		break;
		}
		
	case 2:{
		$ga->requestReportData(
			$idProfile, 
			array('month', 'year'),
			array($metrics_visits,$metrics_new_visits), 
			array(), 
			$filter, 
            null,
			$start, 
			$end, 
			1, 
			$maxRecords
			);
			
		break;
		}
	
	case 3:{
		$ga->requestReportData(
			$idProfile, 
			array('year'),
			array($metrics_visits,$metrics_new_visits), 
			array('year'), 
			$filter, 
            null,
			$start, 
			$end, 
			1, 
			$maxRecords
			);
			
		break;
		}
		
	case 4:{
		$ga->requestReportData(
			$idProfile, 
			array('language'),
			array($metrics_visits,$metrics_new_visits), 
			array(), 
			$filter, 
            null,
			$start, 
			$end, 
			1, 
			$maxRecords
			);
			
		break;
		}
	}
  }catch(exception $e){
  	exit;
  }


$datay = array();
$datax = array();

$result = $ga->getResults();

//print_r($result);

$limit = 150;
$values_limit = 60;
$count = count($result);

if($count==0) exit;

$diff_value_xaxis = 1;
$diff_values = 1; 

if($type==2 || $type==1){
	$diff_value_xaxis = ceil($count/$limit);
	$diff_values = ceil($count/$values_limit);
	}
	else{
	$diff_value_xaxis = 1;
	$diff_values = 1;	
	}

	
   
    
$i=0;

//inicializace zakladnich dat grafu

$souhrn = "";
/*
switch($type){
	case 0: {$souhrn = "hodinové souhrny"; break;}
	case 1: {$souhrn = "denní souhrny"; break;}
	case 2: {$souhrn = "měsíční souhrny"; break;}
	case 3: {$souhrn = "roční souhrny"; break;}
	case 4: {$souhrn = "souhrny podle jazyků"; break;}
	}
*/

$s = explode('-', $start);
$e = explode('-', $end);
$subtitle = strtolower(TOD)." ".intval($s[2]).".".intval($s[1]).".".$s[0]." ".strtolower(TDO)." ".intval($e[2]).".".intval($e[1]).".".$e[0];//." - ".$souhrn."";

//print_r($result);

$pocet_vracejicich_navstevniku_cislo = 0;
$pocet_novych_navstevniku_cislo = 0;

switch($type){
	case 0:{
		foreach($result AS $v){
			$datay[] = $v->metrics[$metrics_visits];
            $datay_newvisits[$i][0] =  $v->metrics[$metrics_visits] - $v->metrics[$metrics_new_visits];
            $datay_newvisits[$i][1] = $v->metrics[$metrics_new_visits];
            
            $pocet_vracejicich_navstevniku[] =  $v->metrics[$metrics_visits] - $v->metrics[$metrics_new_visits];
            $pocet_novych_navstevniku[] = $v->metrics[$metrics_new_visits];
            
            $pocet_vracejicich_navstevniku_cislo +=  $v->metrics[$metrics_visits] - $v->metrics[$metrics_new_visits];
            $pocet_novych_navstevniku_cislo += $v->metrics[$metrics_new_visits];
            
			$hour = $v->dimensions['hour'].":00";
			$datax[] = $hour;
			$targ[$i] = "#".$i;
    		$alts[$i] = THODINA.": ".$hour." %d";
    		$i++;
			}
		break;
		}
	case 1:{
	  
		foreach($result AS $v){
            
            
            $pocet_vracejicich_navstevniku_cislo +=  $v->metrics[$metrics_visits] - $v->metrics[$metrics_new_visits];
            $pocet_novych_navstevniku_cislo += $v->metrics[$metrics_new_visits];
            
			if(($i%$diff_values)==0){
			    $pocet_vracejicich_navstevniku[] =  $v->metrics[$metrics_visits] - $v->metrics[$metrics_new_visits];
                $pocet_novych_navstevniku[] = $v->metrics[$metrics_new_visits]; 
             
				$datay[] =  $v->metrics[$metrics_visits];
                $datay_newvisits[$i][0] = $v->metrics[$metrics_new_visits];
                $datay_newvisits[$i][1] = $v->metrics[$metrics_new_visits];
				$date = ConvertGADateToRealDate($v->dimensions['date']);
				$datax[] = $date;
				$targ[] = "#".$i;
    			$alts[] = TDEN.": ".$date." %d";
				}
			$i++;
			}
         
		break;
		}
	
	case 2:{
		$res = $res1 = array();
        
        
		foreach($result AS $v){
		    
            $pocet_vracejicich_navstevniku_cislo += $v->metrics[$metrics_visits] - $v->metrics[$metrics_new_visits];
            $pocet_novych_navstevniku_cislo += $v->metrics[$metrics_new_visits];  
          
            $pocet_vracejicich_navstevniku[] =  $v->metrics[$metrics_visits] - $v->metrics[$metrics_new_visits];
            $pocet_novych_navstevniku[] = $v->metrics[$metrics_new_visits];
			$res[$v->dimensions['year'].$v->dimensions['month']] =  $v->metrics[$metrics_visits];
            $res1[$v->dimensions['year'].$v->dimensions['month']][0] =  $v->metrics[$metrics_visits] - $v->metrics[$metrics_new_visits];
            $res1[$v->dimensions['year'].$v->dimensions['month']][1] = $v->metrics[$metrics_new_visits];
			}
        
		ksort($res);
		foreach($res AS $idx => $v){
			$datay_newvisits[$i][0] = $res1[$idx][0];
            $datay_newvisits[$i][1] = $res1[$idx][1];
			$datay[] = $res[$idx];
			$mesic = GetMonthName(substr($idx, 4, 2));
			$date = $mesic." ".substr($idx, 0, 4);
			$datax[] = $date;
			$targ[$i] = "#".$i;
			$alts[$i] = TMESIC.": ".$date." %d";
			$i++;
			}
		break;
		}
	case 3:{
		foreach($result AS $v){
			$datay[] =  $v->metrics[$metrics_visits];
            $datay_newvisits[$i][0] =  $v->metrics[$metrics_visits] - $v->metrics[$metrics_new_visits];
            $datay_newvisits[$i][1] = $v->metrics[$metrics_new_visits];
            $pocet_vracejicich_navstevniku[] =  $v->metrics[$metrics_visits] - $v->metrics[$metrics_new_visits];
            $pocet_novych_navstevniku[] = $v->metrics[$metrics_new_visits];
            
            $pocet_vracejicich_navstevniku_cislo +=  $v->metrics[$metrics_visits] - $v->metrics[$metrics_new_visits];
            $pocet_novych_navstevniku_cislo += $v->metrics[$metrics_new_visits];
            
			$year = $v->dimensions['year'];
			$datax[] = $year;
			$targ[$i] = "#".$i;
    		$alts[$i] = TROK.": ".$year." %d";
    		$i++;
			}
		break;
		}
	case 4:{
		$res = $res1 = array();
		foreach($result AS $v){
		    $zkratka_jazyka = explode('-',$v->dimensions['language']);
            $zkr = $zkratka_jazyka[0];
            
            $pocet_vracejicich_navstevniku_cislo +=  $v->metrics[$metrics_visits] - $v->metrics[$metrics_new_visits];
            $pocet_novych_navstevniku_cislo += $v->metrics[$metrics_new_visits];
            
            if(isset($res[$zkr]))
                $res[$zkr] +=  $v->metrics[$metrics_visits];
                else
	           $res[$zkr] =  $v->metrics[$metrics_visits];
                
            
            if(isset($pocet_vracejicich_navstevniku[$zkr]))
                $pocet_vracejicich_navstevniku[$zkr] += ( $v->metrics[$metrics_visits] - $v->metrics[$metrics_new_visits]);
                else
                $pocet_vracejicich_navstevniku[$zkr] =  $v->metrics[$metrics_visits] - $v->metrics[$metrics_new_visits];
            
            if(isset($pocet_novych_navstevniku[$zkr]))    
                $pocet_novych_navstevniku[$zkr] += $v->metrics[$metrics_new_visits];
                else
                $pocet_novych_navstevniku[$zkr] = $v->metrics[$metrics_new_visits];
                
            if(isset($res1[$zkr][0]))
                $res1[$zkr][0] += ( $v->metrics[$metrics_visits] - $v->metrics[$metrics_new_visits]);
                else
                $res1[$zkr][0] = $v->metrics[$metrics_visits] - $v->metrics[$metrics_new_visits];
                
            if(isset($res1[$zkr][1]))   
                $res1[$zkr][1] += $v->metrics[$metrics_new_visits];
                else
                $res1[$zkr][1] = $v->metrics[$metrics_new_visits];
                
			}
        
        arsort($pocet_novych_navstevniku);
        arsort($pocet_vracejicich_navstevniku);
        
        //prevedeni asociativniho pole na bezne pole    
        $res_zal = $pocet_novych_navstevniku;
        $pocet_novych_navstevniku = array();
        foreach($res_zal AS $h)
            $pocet_novych_navstevniku[] = $h;
        
        $res_zal = $pocet_vracejicich_navstevniku;
        $pocet_vracejicich_navstevniku = array();
        foreach($res_zal AS $h)
            $pocet_vracejicich_navstevniku[] = $h;
           
        
		arsort($res);
        //print_r($res);
		foreach($res AS $idx => $v){
			//if($i == 10) break;
            $datay_newvisits[$i][0] = $res1[$idx][0];
            $datay_newvisits[$i][1] = $res1[$idx][1];
			$datay[] = $res[$idx];
			$datax[] = $idx;
			$targ[$i] = "#".$i;
    		$alts[$i] = "";//TJAZYK.": ".$idx." - ".$str1." %d";
    		$i++;
			}
		break;
		}

	
	
}


//zjisteni maximalni hodnoty grafu
$max_y = 0;
$soucet = 0;
foreach($datay AS $dy){
    $soucet += $dy;
    if($max_y < $dy) $max_y = $dy;
    }

$prumer = round($soucet / count($datay));

$suma_vracejici = $pocet_vracejicich_navstevniku_cislo;
$suma_novi = $pocet_novych_navstevniku_cislo;
$prumer_vracejici = 0;
$prumer_novi = 0;
    
$prumer_novi = round($suma_novi/$count);
$prumer_vracejici = round($suma_vracejici/$count);




$titulek = sprintf(TSTATISTIKA_PAGEVIEW_VYHODNOCENI,number_format($soucet, 0, ',', ' '), number_format($prumer, 0, ',', ' '),number_format($suma_novi, 0, ',', ' '), number_format($prumer_novi, 0, ',', ' '));


$options = array(
    "chart" => array(
        "type" => "line",
        "marginBottom" => 100
        ),
    "title" => array(
        "text" => $titulek,
        "style" => array(
            "fontFamily" => "Arial",
            "fontSize"  => "16px",
            "fontWeight"  => "bold"
            )
        ),
    "subtitle" => array(
        "text" => $subtitle,
        "style" => array(
            "fontFamily" => "Arial",
            "fontSize"  => "14px"
            )
        ),
    "xAxis" => array(
        "categories" => $datax,
        "labels" => array(
            "rotation" => 70,
            "step" => $diff_value_xaxis,
            "y" => 40,
            "style" => array(
                "fontFamily" => "Arial",
                "fontSize"  => "10px"
                ) 
            ),
        "tickmarkPlacement" => "on"
        ),
    "yAxis" => array(
        array(
            "title" => array(
                "text" => TPOCET_NAVSTEV
                ),
            "labels" => array(
                "style" => array(
                    "fontFamily" => "Arial",
                    "fontSize"  => "10px"
                    )
                )
            )
        ),
    "tooltip" => array(
        "valueSuffix" => "",
        "shared" => true,
        "crosshairs" => true
        ),
    "legend" => array(
        "layout" => "horizontal",
        "align" => "top",
        "verticalAlign" => "top",
        "x" => -10,
        "y" => 50,
        "borderWidth" => 0,
        "style" => array(
                "fontFamily" => "Arial",
                "fontSize"  => "10px"
                )        
        ),
    "series" => array(
        array(
            "name" => TVSECHNA_ZOBRAZENI,
            "data" => $datay
            ),
        array(
            "name" => TJEDINECNA_ZOBRAZENI,
            "data" => $pocet_novych_navstevniku
            )
        ),
        
    "labels" => array(
        "style" => array(
                "fontFamily" => "Arial",
                "fontSize"  => "10px"
                )
        )                
        
    );
    
echo json_encode($options);
exit;




include (PRE_PATH.'php/classes/chart/open-flash-chart.php');


$title = new title($titulek." ".$subtitle);
$title->set_style("font-size: 12px; padding: 10px 0; font-family: Arial");


// ------- LINE 1,2,3 -----
/*
$d1 = new solid_dot();
$d1->size(3)->halo_size(1)->colour('#CCCCCC')->tooltip("#x_label#<br>Počet zobrazení: #val#");
*/
$d2 = new solid_dot();
$d2->size(3)->colour('#0060ff')->tooltip('#x_label#<br>'.TVSECHNA_ZOBRAZENI.': #val#');

$d3 = new solid_dot();
$d3->size(3)->colour('#cf0404')->tooltip('#x_label#<br>'.TJEDINECNA_ZOBRAZENI.': #val#');

/*
$line = new line();
$line->set_default_dot_style($d1);
$line->set_values( $datay );
$line->set_width( 2 );
$line->set_colour( '#CCCCCC' );
$line->set_key("Celkový počet zobrazení", 10);
*/

//vracejici se
$line2 = new line();
$line2->set_default_dot_style($d2);
$line2->set_values( $datay );
$line2->set_width( 2 );
$line2->set_colour( '#0060ff' );
$line2->set_key(TVSECHNA_ZOBRAZENI, 10);

//novi navstevnici
$line3 = new line();
$line3->set_default_dot_style($d3);
$line3->set_values( $pocet_novych_navstevniku );
$line3->set_width( 2 );
$line3->set_colour( '#cf0404' );
$line3->set_key(TJEDINECNA_ZOBRAZENI, 10);

$pocet_hodnot_y = round(($max_y / 10) + 1);
$y = new y_axis();
$y->set_range( 0, $max_y, $pocet_hodnot_y);
$y->set_grid_colour("#d5d5d6");
$y->set_colour("#6D6D6D");

$x = new x_axis();
$x->set_labels_from_array($datax);
$x->set_grid_colour("#d5d5d6");
$x->set_colour("#6D6D6D");

$x_labels = new x_axis_labels();
$x_labels->set_vertical();
$x_labels->set_labels( $datax );
$x_labels->rotate(45);


$pocet_hodnot_x = round($count/20);
if($pocet_hodnot_x <= 0) $pocet_hodnot_x = 1;


$x_labels->set_steps($pocet_hodnot_x);
$x->set_labels($x_labels);

$t = new tooltip();
$t->set_stroke( 2 );
$t->set_colour( "#6D6D6D" );
$t->set_background_colour( "#ffffff" );
$t->set_title_style( "{font-size: 10px; color: #000000; font-family: Arial}" );
$t->set_body_style( "{font-size: 12px; font-weight: bold;color: #000000;, font-family: Arial}" );


$chart = new open_flash_chart();
$chart->set_title( $title );
$chart->add_element( $line2 );
$chart->add_element( $line3 );
$chart->set_y_axis( $y );
$chart->set_x_axis( $x );
$chart->set_bg_colour(-1);
$chart->set_tooltip($t);

echo $chart->toPrettyString();



?>