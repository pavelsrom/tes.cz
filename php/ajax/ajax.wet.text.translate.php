<?php

/**
 * @author Pavel Srom
 * @copyright 2010
 */


if(!isset($_POST['jazyk1']) || !isset($_POST['jazyk2']) || !isset($_POST['text']) || !defined("SECURITY_CMS")) exit;

function unescapeUTF8EscapeSeq($str) {

      return preg_replace_callback("/\\\u([0-9a-f]{4})/i", create_function('$matches', 'return html_entity_decode(\'&#x\'.$matches[1].\';\', ENT_NOQUOTES, \'UTF-8\');'), $str);

      } 

$text = strip_tags($_POST['text']); 
$from = $_POST['jazyk1'];
$to = $_POST['jazyk2'];


//$url = 'http://ajax.googleapis.com/ajax/services/language/translate?v=1.0&q='.rawurlencode($text).'&langpair='.rawurlencode($from.'|'.$to);

$url = 'http://mymemory.translated.net/api/get?q='.rawurlencode($text).'&langpair='.rawurlencode($from.'|'.$to);

$response = file_get_contents(
      $url,
      null,
      stream_context_create(
          array(
              'http'=>array(
              'method'=>"GET",
              'header'=>"Referer: ".PROTOKOL.$_SERVER['HTTP_HOST']."/\r\n"
              )
          )
      )

    );

//print_r($response);

if (preg_match("/{\"translatedText\":\"([^\"]+)\"/i", $response, $matches)) {

      echo  unescapeUTF8EscapeSeq($matches[1]);

      }
    
      
exit;


?>