<?php

/**
 * @author Pavel Srom
 * @copyright 2010
 */


if(!isset($_GET['module']) || !defined("SECURITY_CMS")) exit;

$vyraz = trim($db->secureString($_GET['vyraz']));
$module = trim($_GET['module']);

//nastaveni limitu
$limit = "";
if(isset($_GET['iDisplayLength'])){
    $dStart = "";
    if(isset($_GET['iDisplayStart']))
        $dStart = abs(intval($_GET['iDisplayStart'])).",";
        
    $limit = " LIMIT ".$dStart.abs(intval($_GET['iDisplayLength'])); 
}


//trideni podle sloupcu
$order = "";
if(isset($_GET['iSortCol_0'])){
    $s = abs(intval($_GET['iSortCol_0']));
    if($s == 0) $order = "idSkupiny";
    elseif($s == 1) $order = "nazev";
    elseif($s == 2) $order = "aktivni";
    else $order = "idSkupiny";
}

if($order != '') 
    $order = "ORDER BY ".$order." ".(isset($_GET['sSortDir_0'])?$_GET['sSortDir_0']:'');

//sestaveni podminky pro vyhledavani
$like = array();
if($vyraz != '')
{
    $like[] = "idSkupiny LIKE '".$vyraz."%'";
    $like[] = "nazev LIKE '%".$vyraz."%'";
    
}
    

if(count($like) > 0)
    $where = " WHERE ".implode(" OR ",$like);
    else
    $where = "";

$data_pocet = $db->Query("SELECT COUNT(idSkupiny) AS celkem FROM ".TABLE_UZIVATELE_SKUPINY.$where);
$iTotal_arr = $db->getAssoc($data_pocet);
$iTotal = $iTotal_arr['celkem'];

$data = $db->Query("SELECT SQL_CALC_FOUND_ROWS nazev, aktivni, idSkupiny   
				FROM ".TABLE_UZIVATELE_SKUPINY." 
				".$where." 
                ".$order."
                ".$limit."
                
                ");


$iFilteredTotal_data = $db->Query("SELECT FOUND_ROWS()");
$iFilteredTotal_arr = $db->getRow($iFilteredTotal_data);
$iFilteredTotal = intval($iFilteredTotal_arr[0]);

$dis_content = !$login_obj->UserPrivilege('settings_view');
$dis_delete = !$login_obj->UserPrivilege('settings_delete');

$tr = array();
while($c = $db->getAssoc($data))
{
    $ids = $c['idSkupiny'];
    $pom = array();
    $pom[] = json_encode($ids);
    $pom[] = json_encode(secureString($c['nazev']));
    $pom[] = json_encode(ano_ne($c['aktivni']));
    
    $ikony = "";
    
    if($dis_content)
        $ikony .= icon_disabled('setting');
        else
        $ikony .= "<a href='".get_link($module,'skupiny',0,'edit-settings',$ids)."'>".icon('setting')."</a>";

    if($dis_delete)
        $ikony .= icon_disabled('delete');
        else
        $ikony .= "<a href=\"#\" id='delete".$ids."' class='delete'>".icon('delete')."</a>";
        
    $pom[] = json_encode($ikony);
        
    $tr[] = "[".implode(',', $pom)."]";
    
}

echo "{";
echo '"sEcho": '.(intval($_GET['sEcho'])).', ';
echo '"iTotalRecords": '.intval($iTotal).', ';
echo '"iTotalDisplayRecords": '.($iFilteredTotal).', ';
echo '"aaData": ['.implode(',', $tr).'] }';


?>