<?php

/**
 * @author Pavel Srom
 * @copyright 2010
 */

//ajaxovy skript na odhlaseni kontaktu z odberu newsletteru

if(!isset($_GET['module']) || !defined("SECURITY_CMS")) exit;

$vyraz = trim($db->secureString($_GET['vyraz']));
$module = trim($_GET['module']);

//nastaveni limitu
$limit = "";
if(isset($_GET['iDisplayLength'])){
    $dStart = "";
    if(isset($_GET['iDisplayStart']))
        $dStart = abs(intval($_GET['iDisplayStart'])).",";
        
    $limit = " LIMIT ".$dStart.abs(intval($_GET['iDisplayLength'])); 
}


//trideni podle sloupcu
$order = "";
if(isset($_GET['iSortCol_0'])){
    $s = abs(intval($_GET['iSortCol_0']));
    if($s == 0) $order = "s.idSkupiny";
    elseif($s == 1) $order = "s.nazev";
    elseif($s == 2) $order = "s.typ";
    elseif($s == 3) $order = "pocet";
    else $order = "s.idSkupiny";
}

if($order != '') 
    $order = "ORDER BY ".$order." ".(isset($_GET['sSortDir_0'])?$_GET['sSortDir_0']:'');

//echo $order;

//sestaveni podminky pro vyhledavani
$like = array();
if($vyraz != '')
{
    $like[] = "s.idSkupiny LIKE '".$vyraz."%'";
    $like[] = "s.nazev LIKE '%".$vyraz."%'";
}
    


$where = "WHERE s.idDomeny=".$domain->getId().((count($like) > 0) ? " AND (".implode(" OR ",$like).")" : "");

$data_pocet = $db->Query("SELECT COUNT(idSkupiny) AS celkem FROM ".TABLE_EMAILY_SKUPINY." AS s ".$where);
$iTotal_arr = $db->getAssoc($data_pocet);
$iTotal = $iTotal_arr['celkem'];


$data = $db->Query("SELECT SQL_CALC_FOUND_ROWS s.idSkupiny, s.nazev, COUNT(sp.idPrijemce) AS pocet, s.verejna
			FROM ".TABLE_EMAILY_SKUPINY." AS s 
			LEFT JOIN ".TABLE_EMAILY_SKUPINY_PRIJEMCI." AS sp ON sp.idSkupiny = s.idSkupiny 
			LEFT JOIN ".TABLE_EMAILY_PRIJEMCI." AS p ON sp.idPrijemce = p.idPrijemce 
			".$where."
			GROUP BY s.idSkupiny 
			".$order."
            ".$limit);

//AND p.odhlasen IS NULL 
//AND p.overen=1

$iFilteredTotal_data = $db->Query("SELECT FOUND_ROWS()");
$iFilteredTotal_arr = $db->getRow($iFilteredTotal_data);
$iFilteredTotal = intval($iFilteredTotal_arr[0]);

$dis_settings = !$login_obj->UserPrivilege('settings_view');
$dis_delete = !$login_obj->UserPrivilege('content_delete');

$tr = array();
while($c = $db->getAssoc($data))
{
    $ids = $c['idSkupiny'];
    $pom = array();
    $pom[] = json_encode($ids);
    $pom[] = json_encode($c['nazev']);
    $pom[] = json_encode($c['verejna'] == 1 ? TVEREJNA : TPRIVATNI);
    $pom[] = json_encode($c['pocet']);
    
    $ikony = "";
    
    if($dis_settings)
        $ikony .= icon_disabled('setting');
		else
		$ikony .= "<a href='".get_link('newsletters','skupiny',0,'edit-settings',$ids)."'>".icon('setting')."</a>";
        
    if($dis_delete)
		$ikony .= icon_disabled('delete');
		else        
        $ikony .= "<a href='#' id='delete".$ids."' class='delete'>".icon('delete')."</a>";
    
    
    $pom[] = json_encode($ikony);
        
    $tr[] = "[".implode(',', $pom)."]";
    
}

echo "{";
echo '"sEcho": '.(intval($_GET['sEcho'])).', ';
echo '"iTotalRecords": '.$iTotal.', ';
echo '"iTotalDisplayRecords": '.($iFilteredTotal).', ';
echo '"aaData": ['.implode(',', $tr).'] }';


?>