<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!$login_obj->UserPrivilege('settings_delete') || !defined("SECURITY_CMS")) exit;

$id = get_int_post('id');

if($id <= 0) exit;


			
$soubor = $db->get(TABLE_EMAILY_PRILOHY, "soubor", "idPrilohy=".$id);

                
if($soubor == '') exit;
    
@unlink(PRE_PATH.$domain->getDir().USER_DIRNAME_ATTACHMENT.$soubor); 
    
				
$db->delete(TABLE_EMAILY_PRILOHY,"idPrilohy=".$id);
			
$log->add_log('delete','newsletter-priloha',$id,$soubor);

exit;

?>