<?php

/**
 * @author Pavel Srom
 * @copyright 2010
 */

if(!defined("SECURITY_CMS")) exit;

$error = "";
$msg = "";
$fileElementName = 'fileToUpload';
    
if(!empty($_FILES[$fileElementName]['error']))
{
	$error = GetUploadError($_FILES[$fileElementName]['error']);
       
}elseif(empty($_FILES[$fileElementName]['tmp_name']) || $_FILES[$fileElementName]['tmp_name'] == 'none')
{
	$error = TZADNY_SOUBOR_NEBYL_ZADAN;
       
}
   else 
{
    
    
	   $ext = strtolower(end(explode('.', $_FILES[$fileElementName]['name'])));
	   if($ext == 'html' || $ext == 'htm' || $ext == 'txt'){
	       if(function_exists('file_get_contents')){
	           
                $msg = file_get_contents($_FILES['fileToUpload']['tmp_name']);
                
                }
                else{
            
                $dom = new DOMDocument();
                $dom->loadHTMLFile($_FILES[$fileElementName]['tmp_name']);
                $msg = $dom->saveHTML();
                }
                
            //$msg = json_encode($msg);
             
            $msg = trim($msg);
            $msg = @eregi_replace(PHP_EOL,'',$msg);
            $msg = @eregi_replace(PHP_EOL,'',$msg);
            //$msg = htmlentities($msg,ENT_QUOTES,'UTF-8');
            $msg = preg_replace('/[\n\r\t\f]/','',$msg);
            
            
            
            }
            else
            $error = TSOUBOR_NEMA_PLATNOU_PRIPONU_HTML;
            
       @unlink($_FILES['fileToUpload']);
       	
	}		
	echo "{error: '".$error."',msg: '".$msg."'}";

?>