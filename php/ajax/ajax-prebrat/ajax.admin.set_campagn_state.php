<?php

/**
 * @author Pavel Srom
 * @copyright 2010
 */
 
 


if(!isset($_POST['idKampane'], $_POST['stav']) || !defined("SECURITY_CMS")) exit;


$id = $db->secureString($_POST['idKampane']);
$stav = $db->secureString($_POST['stav']);

if(!$login_obj->UserPrivilege('settings_edit')) exit;

$data = $db->Query("SELECT k.*, r.idRelace AS skupina FROM ".TABLE_EMAILY_KAMPANE." AS k LEFT JOIN ".TABLE_EMAILY_SKUPINY_KAMPANE." AS r ON k.idKampane=r.idKampane WHERE k.idKampane=".$id." AND idDomeny=".$idDomain);
$cam = $db->getAssoc($data);



$start = trim($cam['start']);
$duvod_nespusteni = "";
$date = explode('-',$cam['start']);

if($start!=''){
	if($stav=='cekajici' || $stav=='bezici'){
		$ts_start = mktime(0,0,0, $date[1], $date[2], $date[0]);
		$ts_dnes = mktime(0,0,0);
		if($ts_dnes < $ts_start)
			$stav='cekajici';
			else
			$stav='bezici';
		}
	elseif($stav=='pozastavena') $stav='pozastavena';
	elseif($stav=='ukoncena') $stav='ukoncena';
	}
	else{
	if($stav=='pozastavena') $stav='pozastavena';
	elseif($stav=='ukoncena') $stav='ukoncena';
	else{
		$stav = 'nelze';
		$duvod_nespusteni .= "- ".TCHYBI_DATUM_SPUSTENI."<br/>";
		}
	}
			
			
if($stav!='pozastavena' && $stav!='ukoncena'){	
if($cam['odEmail']=='') {
	$duvod_nespusteni .= "- ".TCHYBI_EMAIL_ODESILATELE."<br/>";
	$stav = 'nelze';						
	}
if($cam['predmet']=='') {
	$duvod_nespusteni .= "- ".TCHYBI_PREDMET_EMAILU."<br/>";
	$stav = 'nelze';						
	}
if($cam['obsah']=='') {
	$duvod_nespusteni .= "- ".TOBSAH_EMAILU_JE_PRAZDNY."<br/>";
	$stav = 'nelze';						
	}	
if($cam['skupina']==null) {
	$duvod_nespusteni .= "- ".TNENI_ZVOLENA_CILOVA_SKUPINA_PRIJEMCU."<br/>";
	$stav = 'nelze';						
	}
}

$campaign_status = array("nelze" => TNELZE_SPUSTIT,"cekajici" => TCEKA_NA_SPUSTENI, "bezici" => TPRAVE_PROBIHA, "pozastavena" => TPOZASTAVENA, "ukoncena" => TUKONCENA);

$form = new C_FormItem();
if(!isset($campaign_status[$stav]) || $id!=intval($id) || $id=='' || $id<1) exit;

if($stav!='nelze')
	$res = $db->Query("UPDATE ".TABLE_EMAILY_KAMPANE." SET stav='".$stav."' WHERE idKampane=".$id." AND idDomeny=".$dom->ACTIVE_DOMAIN_ID);
	else
	$res = true;

if(!$res) exit;

$odkaz_stavu="";
if($stav=='bezici') 
	$odkaz_stavu = "<a href=\"javascript:change_campagn_state(".$id.",'pozastavena','stav')\">".TPOZASTAVIT."</a>"; 
				
if($stav=='pozastavena')
	$odkaz_stavu = "<a href=\"javascript:change_campagn_state(".$id.",'bezici','stav')\">".TSPUSTIT."</a>"; 
	
	
if($odkaz_stavu!='')
	echo "<b>".$campaign_status[$stav]."</b> (".$odkaz_stavu.")";
	else 
	echo "<b>".$campaign_status[$stav]."</b>";
exit;


?>