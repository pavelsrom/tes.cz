<?php

/**
 * @author Pavel Srom
 * @copyright 2010
 */

if(!isset($_POST['idInquiry']) || !isset($_POST['idItem']) || !defined("SECURITY_CMS")) exit;

$idInquiry = intval($_POST['idInquiry']);
$idPolozky = intval($_POST['idItem']);

if($idInquiry==0 || $idPolozky==0) exit;

if(!$login_obj->UserPrivilege('content_delete')) exit;

$security = $db->Query("SELECT a.idAnkety FROM ".TABLE_ANKETY." AS a 
	LEFT JOIN ".TABLE_ANKETY_OTAZKY." AS p ON a.idAnkety=p.idAnkety
	WHERE a.idAnkety=".$idInquiry."
		AND a.idDomeny=".$domain->getId()."
		AND p.idPolozky=".$idPolozky." 
	LIMIT 1");

if($db->numRows($security)==0) exit;


$res1 = $db->Query("DELETE FROM ".TABLE_ANKETY_OTAZKY." WHERE idAnkety=".$idInquiry." AND idPolozky=".$idPolozky);

$nazev_ankety = $db->get(TABLE_ANKETY,'nazev',"idAnkety=".$idInquiry);
$log->add_log('edit-content','anketa',$idInquiry,$nazev_ankety);

if($res1)
	echo "ok";
	else
	echo "error";
	
exit;


?>