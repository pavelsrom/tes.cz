<?php

/**
 * @author Pavel Srom
 * @copyright 2013
 */


$jazyk = $lang->get_id();
$nazev = $db->secureString($_GET["nazev"]);

//overeni zdali tag uz neexistuje
$d = $db->query("SELECT idStranky 
    FROM ".TABLE_STRANKY." 
    WHERE nazev='".$nazev."' 
        AND idJazyka='".$jazyk."'
        AND typ = 'tag'
        AND idDomeny = ".$domain->getId()."  
    LIMIT 1");

$result = array();

if($db->numRows($d) > 0)
{
    exit;
}

$url=ModifyUrl($nazev);
for($dupl=1;$dupl<=50;$dupl++)
{
    $sql=$db->query("SELECT url 
        FROM ".TABLE_STRANKY." 
        WHERE url = '$url' 
            AND idDomeny='".$domain->getId()."'
        LIMIT 1
            ");
    if ($db->numRows($sql)>0)
        $url=ModifyUrl($nazev.$dupl);
        else
        break;
}


$insert = array(
    "nazev" => $nazev, 
    "idRodice" => $db->get(TABLE_STRANKY,"idRodice","idDomeny=".$domain->getId()." AND typ='tagy' AND idJazyka=".$jazyk),
    "idJazyka" => $jazyk,
    "idDomeny" => $domain->getId(), 
    'url' => $url,
    'doMenu' => 0,
    'title' => $nazev,
    'typ' => 'tag'
    );
    
$db->insert(TABLE_STRANKY, $insert);
$id = $db->lastID();

$result['id'] = $id;
$result['nazev'] = $nazev;

$log->add_log('create','tag',$id,$nazev);

echo json_encode($result);
?>