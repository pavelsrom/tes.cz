<?php

/**
 * @author Pavel Srom
 * @copyright 2013
 */


$jazyk = $lang->get_id();

$input = $db->secureString($_GET["q"]);

$data = array();
// query your DataBase here looking for a match to $input
$query = $db->query("SELECT idStranky AS id_tag, nazev
    FROM ".TABLE_STRANKY." 
    WHERE nazev LIKE '%$input%' 
        AND idJazyka='".$jazyk."'
        AND idDomeny=".$domain->getId()." 
        AND zobrazit=1
        AND typ='tag'
    LIMIT 10");
    
while ($row = $db->getAssoc($query))
{
    $json = array();
    $json['value'] = $row['id_tag'];
    $json['name'] = $row['nazev'];
    $json['image'] = "";
    $data[] = $json;
}

header("Content-type: application/json");
echo json_encode($data);

?>