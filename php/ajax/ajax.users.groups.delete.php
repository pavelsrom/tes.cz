<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */
 
 
if(!defined("SECURITY_CMS")) exit;

if((!$login_obj->UserPrivilege('admin') && !$login_obj->UserPrivilege('superadmin')) || !$login_obj->UserPrivilege('settings_delete')) exit;

$id = get_int_post('id');

if($id <= 0) exit;

$db->delete(TABLE_UZIVATELE_SKUPINY, "idSkupiny=".$id." AND idDomeny=".$domain->getId()." LIMIT 1");
$db->delete(TABLE_UZIVATELE_SKUPINY_RELACE, "idSkupiny=".$id."");
$db->delete(TABLE_STRANKY_SKUPINY, "idSkupiny=".$id."");



exit;


?>