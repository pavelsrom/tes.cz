<?php

/**
 * @author Pavel Srom
 * @copyright 2010
 */

if(!is_post('idItem') || !defined("SECURITY_CMS")) exit;

$idGalerie = get_int_post('idGallery');
$idPolozky = get_array_post('idItem');


$result = array(
    "messages" => array("error" => "", "ok" => "")
    );

$spusteno_pres_inline_editaci = false;
if($idGalerie == 0)
{
    
    $idGalerie = $db->get(TABLE_STRANKY,"idRodice","idStranky=".$idPolozky[0]);
    
    $spusteno_pres_inline_editaci = true;

}


if($idGalerie==0 || count($idPolozky)==0 || !$object_access->has_access($idGalerie) || !$login_obj->UserPrivilege('content_delete'))
{

    if($spusteno_pres_inline_editaci)
    {
        $result['messages']['error'] = TNEMATE_OPRAVNENI_K_MAZANI;
        echo array2json($result);   
    }
    
    exit;
}

$dir1 = PRE_PATH.$domain->getDir().USER_DIRNAME_GALLERY_MAXI;
$dir2 = PRE_PATH.$domain->getDir().USER_DIRNAME_GALLERY_MINI;
$dir3 = PRE_PATH.$domain->getDir().USER_DIRNAME_GALLERY_STREDNI;



foreach($idPolozky AS $idp)
{

    $security = $db->Query("SELECT g.idStranky, g.obrazek AS soubor, g1.nazev AS nazev_galerie 
        FROM ".TABLE_STRANKY." AS g
        LEFT JOIN ".TABLE_STRANKY." AS g1 ON g.idRodice=g1.idStranky 
        WHERE g.idStranky=".$idp."
            AND g.idRodice = ".$idGalerie."
    		AND g.idDomeny=".$domain->getId()."
            AND g.typ = 'galerie-fotka'
    	LIMIT 1");
    
    if($db->numRows($security)==0)
    {
        
        
        if($spusteno_pres_inline_editaci)
        {
            $result['messages']['error'] = TNEMATE_OPRAVNENI_K_MAZANI;
            echo array2json($result);   
        }
        
        exit;
    }

    
    $item = $db->getAssoc($security); 

    $idp = intval($idp);
    $db->delete(TABLE_STRANKY,"idStranky=".$idp);
    $db->delete(TABLE_STRANKY_TAGY, "idObjektu=".$idp." AND typObjektu='stranka'");
    $db->delete(TABLE_STRANKY_EDITORI,"idObjektu=".$idp." AND typ = 'stranka'");
    $db->delete(TABLE_ODKAZY,"typ='stranka' AND idObjektu=".$idp);
    $db->delete(TABLE_ZALOHY,"typ='stranka' AND idObjektu=".$idp);
    $db->delete(TABLE_DISKUZE_POLOZKY,"idStranky=".$idp);
    $db->delete(TABLE_STRANKY_SKUPINY, "idStranky=".$idp);
    
    $log->add_log('delete','galerie-fotka',$idp,$item['nazev_galerie']);
    
    //overeni zda se jeden obrazek nevyskytuje na vice fotkach
    $d = $db->query("SELECT idStranky FROM ".TABLE_STRANKY." WHERE idDomeny='".$domain->getId()."' AND obrazek='".$item['soubor']."' AND idStranky!='".$idp."'");
    
    if($db->numRows($d) > 0)
    {
        
        if($spusteno_pres_inline_editaci)
        {
            $result['messages']['ok'] = OK_SMAZANO;
            echo array2json($result);   
        }
        else
            echo OK_SMAZANO;
            
        exit;
    }
    
    if(!TestHttpUrl($item['soubor']) && $item['soubor']!='' && file_exists($dir1.$item['soubor']))
    	$res2 = @unlink($dir1.$item['soubor']);
    	else
    	$res2 = true;
    		
    if(!TestHttpUrl($item['soubor']) && $item['soubor']!='' && file_exists($dir2.$item['soubor'])) 
    	$res3 = @unlink($dir2.$item['soubor']);
    	else
    	$res3 = true;
        
    if(!TestHttpUrl($item['soubor']) && $item['soubor']!='' && file_exists($dir3.$item['soubor'])) 
    	$res3 = @unlink($dir3.$item['soubor']);
    	else
    	$res3 = true;
    
}



if($spusteno_pres_inline_editaci)
{
    $result['messages']['ok'] = OK_SMAZANO;
    echo array2json($result);   
}
else
    echo OK_SMAZANO;

	
exit;

?>