<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */
 
 
if(!$login_obj->UserPrivilege('settings_delete') || !defined("SECURITY_CMS")) exit;


$id = get_int_post('id');

if($id <= 0) exit;

$security = $db->query("SELECT CONCAT(jmeno,' ',prijmeni,' (',email,')') AS nazev FROM ".TABLE_EMAILY_PRIJEMCI." WHERE idPrijemce=".$id." AND idDomeny=".$domain->getId());

if($db->numRows($security) == 0)
{
    exit;
}   

$n = $db->getAssoc($security);
$nazev = $n['nazev'];         
            
$db->Query("DELETE FROM ".TABLE_EMAILY_PRIJEMCI." WHERE idDomeny=".$domain->getId()." AND idPrijemce=".$id);
$db->Query("DELETE FROM ".TABLE_EMAILY_SKUPINY_PRIJEMCI." WHERE idPrijemce=".$id);
$db->Query("DELETE FROM ".TABLE_EMAILY_FRONTA." WHERE idPrijemce=".$id);


$log->add_log('delete','newsletter-prijemci',$id,$nazev);

exit;


?>