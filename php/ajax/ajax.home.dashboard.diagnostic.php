<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined("SECURITY_CMS")) exit;

$result_info = array();
$result_warning = array();
$result_error = array();

//------------------------------------------------------------------------
//test 1 - pocet novych zprav z formularu
$data = $db->query("SELECT idZpravy 
    FROM ".TABLE_FORMULARE." AS f
    LEFT JOIN ".TABLE_FORMULARE_ZPRAVY." AS z ON f.idFormulare=z.idFormulare
    WHERE f.idDomeny=".$domain->getId()." 
        AND stav='nova' 
        ");

$pocet = $db->numRows($data);
if($pocet > 0 && MODULE_FORMS)
{
    $text = $pocet." ";
    if($pocet == 1)
        $text .= TNOVA_ZPRAVA;
    elseif($pocet > 1 && $pocet < 5)
        $text .= TNOVE_ZPRAVY;
    else    
        $text .= TNOVYCH_ZPRAV;
  
    $text =  "<b class='stav0'>".$text."</b> ".Z_FORMULARU." (<a href='".get_link("forms","zpravy")."' title='".TVICE_INFORMACI."'>".TVICE_INFORMACI."</a>)";
    
    $result_info[] = $text;
         
}


//-----------------------------------------------------------------
//test 2 - pocet novych diskuznich prispevku
$data = $db->query("SELECT COUNT(p.idPolozky) AS pocet 
    FROM ".TABLE_STRANKY." AS s 
    LEFT JOIN ".TABLE_DISKUZE_POLOZKY." AS p ON s.idStranky=p.idStranky
    WHERE s.idDomeny=".$domain->getId()."
        AND p.admin = 0
        AND p.novy = 1
    ");
$d = $db->getAssoc($data);
$pocet = intval($d['pocet']);

if($pocet > 0 && MODULE_DISCUSSION)
{
    $text = $pocet." ";
    if($pocet == 1)
        $text .= TNOVY_PRISPEVEK;
    elseif($pocet > 1 && $pocet < 5)
        $text .= TNOVE_PRISPEVKY;
    else    
        $text .= TNOVYCH_PRISPEVKU;
    
    $text = "<b class='stav0'>".$text."</b> ".V_DISKUZICH." (<a href='".get_link("discussions")."' title='".TVICE_INFORMACI."'>".TVICE_INFORMACI."</a>)";
    
    $result_info[] = $text;
         
}


//--------------------------------------------------------------------
//test 3 - pocet chybnych odkazu
$pocet = $db->get(TABLE_ODKAZY,'COUNT(idOdkazu)',"idDomeny=".$domain->getId()." AND kod>=400");

if($pocet > 0)
{
    $text = $pocet." ";
    if($pocet == 1)
        $text .= TCHYBNY_ODKAZ;
    elseif($pocet > 1 && $pocet < 5)
        $text .= TCHYBNE_ODKAZY;
    else    
        $text .= TCHYBNYCH_ODKAZU;
    
    $result_warning[] = "<b class='stav0'>".$text."</b> ".V_OBSAHU_WEBU." (<a href='".get_link("home","odkazy")."' title='".TVICE_INFORMACI."'>".TVICE_INFORMACI."</a>)";
}

//test 4 - povolene/zakazane indexovani webu
if(POVOLIT_INDEXACI_WEBU == 0)
    $result_warning[] = TWEB_MA_ZAKAZANO_INDEXOVANI;


//test 5 - pocet stranek s prazdnym obsahem nebo nedostatkem textoveho obsahu   
$data = $db->query("SELECT s.nazev, s.idStranky, typ, idJazyka 
    FROM ".TABLE_STRANKY." AS s 
    WHERE idDomeny=".$domain->getId()." 
        AND typ IN ('stranka','novinka','novinky','clanek','clanky','404','uvodni','stranka-galerie','akce') 
        AND TRIM(obsahBezHtml)='' 
    ORDER BY nazev
    ");
  
$clanky = $stranky = $novinky = array();
while($s = $db->getAssoc($data))
{
    $id = $s['idStranky'];
    $typ = $s['typ'];
    $nazev = $s['nazev'];
    
    
    if($typ == 'clanek')
        $clanky[$id] = $nazev;
    elseif($typ == 'novinka')
        $novinky[$id] = $nazev;
    else
        $stranky[$id] = $nazev;      
    
}

$pocet = count($stranky);
if($pocet > 0)
{
    $i = 0;
    foreach($stranky AS $ids => $str)
    {
        if($i>2)
            unset($stranky[$ids]);
        $i++;
    }
        
    if($pocet == 1)
        $text = TSTRANKA." <i>".implode(", ", $stranky)."</i> ".TNEOBSAHUJE_ZADNY_TEXT;    
    if($pocet > 1 && $pocet <= 3)
        $text = TTSTRANKY." <i>".implode(", ", $stranky)."</i> ".TNEOBSAHUJI_ZADNY_TEXT;
    if($pocet > 3)
        $text = TTSTRANKY." <i>".implode(", ", $stranky)."</i> ".TA_DALSI." ".TNEOBSAHUJI_ZADNY_TEXT;
        
    $result_warning[] = $text." (<a href='".get_link("pages","stranky")."' title='".TVICE_INFORMACI."'>".TVICE_INFORMACI."</a>)";
     
}


$pocet = count($novinky);
if($pocet > 0 && MODULE_NEWS)
{
    $i = 0;
    foreach($novinky AS $ids => $str)
    {
        if($i>2)
            unset($novinky[$ids]);
        $i++;
    }
        
    if($pocet == 1)
        $text = TNOVINKA." <i>".implode(", ", $novinky)."</i> ".TNEOBSAHUJE_ZADNY_TEXT;    
    if($pocet > 1 && $pocet <= 3)
        $text = TNOVINKY." <i>".implode(", ", $novinky)."</i> ".TNEOBSAHUJI_ZADNY_TEXT;
    if($pocet > 3)
        $text = TNOVINKY." <i>".implode(", ", $novinky)."</i> ".TA_DALSI." ".TNEOBSAHUJI_ZADNY_TEXT;
        
    $result_warning[] = $text." (<a href='".get_link("news","novinky")."' title='".TVICE_INFORMACI."'>".TVICE_INFORMACI."</a>)";
     
}

$pocet = count($clanky);
if($pocet > 0 && MODULE_ARTICLES)
{
    $i = 0;
    foreach($clanky AS $ids => $str)
    {
        if($i>2)
            unset($clanky[$ids]);
        $i++;
    }
        
    if($pocet == 1)
        $text = TCLANEK." <i>".implode(", ", $clanky)."</i> ".TNEOBSAHUJE_ZADNY_TEXT;    
    if($pocet > 1 && $pocet <= 3)
        $text = TCLANKY." <i>".implode(", ", $clanky)."</i> ".TNEOBSAHUJI_ZADNY_TEXT;
    if($pocet > 3)
        $text = TCLANKY." <i>".implode(", ", $clanky)."</i> ".TA_DALSI." ".TNEOBSAHUJI_ZADNY_TEXT;
        
    $result_warning[] = $text." (<a href='".get_link("articles","rubriky")."' title='".TVICE_INFORMACI."'>".TVICE_INFORMACI."</a>)"; 
}


//test 6 - udaje o webu
$co = array();
if(EMAIL_WEBU == "")
    $co[] = TEMAIL;
if(NAZEV_WEBU == "")
    $co[] = TNAZEV;
if(GA_KOD == "")
    $co[] = TGA_KOD;    
if(POPIS_WEBU == "")
    $co[] = TPOPIS;     

if(count($co) > 0)
    $result_warning[] = TNENI_VYPLNEN." <i>".implode(", ",$co)."</i> ".TWEBU;


if(MODULE_STATISTICS && (GA_PROFIL == "" || GA_HESLO=="" || GA_EMAIL==""))
    $result_warning[] = TNEJSOU_VYPLNENY_UDAJE_PRO_PRIPOJENI_KE_GA_STATISTIKAM;



//text 7 - navstevnost
$zapnout = false; //zdrzovani pri nacteni, proto deaktivovano
if(MODULE_STATISTICS && STATISTIKA_UPOZORNENI_NA_POKLES_NAVSTEVNOSTI > 0 && $zapnout)
{
    include_once('../classes/class.gapi.php');
    $hranice = intval(STATISTIKA_UPOZORNENI_NA_POKLES_NAVSTEVNOSTI);
    $posledniDen = date('Ymd', strtotime('-1 day'));
    $vcera = date('d.m.Y', strtotime('-1 day'));
    
    $start = date('Y-m-d', strtotime('-1 day'));
    $end = date('Y-m-d', strtotime('-1 day'));

    try{
    	$ga = new gapi(GA_EMAIL,GA_HESLO);
    	}
    	catch(exception $e){
        }
     
    $idProfile = GA_PROFIL;
    $maxRecords = 500;
    $filter = null;
    
    $ga->requestReportData($idProfile,array('date'),array('visits'),null,null,null,$start,$end,1,$maxRecords);
    $res = $ga->getResults();  
    
    $v = 0;
    foreach($res AS $r)
        $v += $r->metrics['visits'];

    /*
    $d1 = navstevnost($v,$posledniTydenOd,$posledniTydenDo);
    $d2 = navstevnost($v,$posledniMesicOd,$posledniMesicDo);
           
    $prumerMinulyTyden = $d1['prumer'];
    $prumerMinulyMesic = $d2['prumer'];
    */
    
    if($hranice > $v){
        $result_warning[] = TVCERA." ".$vcera." ".TDOSLO_K_PREKROCENI_SPODNI_HRANICE_NAVSTEVNOSTI." ".$hranice.".".TPOCET_NAVSTEV_BYL." ".$v.".";
           
    }
    
            
}

//test 8 - zjisteni zda je vyplnen email pro kontaktni formular
if(MODULE_CONTACT_FORM && !TestEmail(KONTAKTNI_FORMULAR_EMAIL, 250,false))
{
    $result_warning[] = ERROR_NENI_VYPLNEN_EMAIL_U_KONTAKTNIHO_FORMULARE;
}




echo "<table class='tDiagnostika'>";
foreach($result_info AS $r)
    echo "<tr><td>".icon("info")."</td><td>".$r."</td></tr>";
foreach($result_warning AS $r)
    echo "<tr><td>".icon("warning")."</td><td>".$r."</td></tr>";

echo "</table>";
echo "<br />";
//echo "<a class='f-right' href='".get_link("home","log")."'>".TPODIVAT_SE_NA_KOMPLETNI_LOG."</a>";
echo "<div class='cleaner'></div>";

?>