<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */


$id_zalohy = get_int_get('id_zalohy');
$id_stranky = get_int_get('id_stranky');
$s = $db->get(TABLE_STRANKY,array('nazev','typ'),"idStranky=".$id_stranky);

$nazev = $s->nazev;
$typ_do_logu = $s->typ;


if($id_zalohy <= 0 || $id_stranky <= 0)
{
    $result = array("messages" => array("error" => TCHYBA_SYSTEMU_CMS));
    echo array2json($result);
    exit;
}

include_once('../classes/class.content.backup.php');
$content_backup = new ContentBackup($domain->getId(), $id_stranky, $login_obj->getId());
$content = $content_backup->GetContent($id_zalohy);

$log->add_log('edit-content',get_log_type($typ_do_logu),$id_stranky,$nazev);

$result = array(
    "messages" => array(
        "ok" => TOBSAH_STRANKY_BYL_OBNOVEN_ZE_ZALOHY 
        ),
    "html" => array(
        "perex" => $content["perex"],
        "obsah" => $content["obsah"]
        )
);

$update = $result["html"];
$update['perexBezHtml'] = strip_tags($update['perex']);
$update['obsahBezHtml'] = strip_tags($update['obsah']);

$db->update(TABLE_STRANKY,$update,"idStranky=".$id_stranky); 



echo array2json($result);

?>