<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */
 
 
if(!$login_obj->UserPrivilege('content_delete') || !defined("SECURITY_CMS")) exit;


$id = get_int_post('id');

if($id <= 0) exit;

//overeni spravnosti domeny
$idAnkety = $db->get(TABLE_ANKETY_ODPOVEDI,"idAnkety","idOdpovedi=".$id);
$nazev = $db->get(TABLE_ANKETY,"nazev","idAnkety=".$idAnkety." AND idDomeny=".$domain->getId());

if($nazev == "") exit;

 
$log->add_log('edit-content',"anketa",$id,$nazev);

$db->delete(TABLE_ANKETY_ODPOVEDI,"WHERE idOdpovedi=".$id);
$db->delete(TABLE_ANKETY_ODPOVEDI_NAZVY,"WHERE idOdpovedi=".$id);
exit;


?>