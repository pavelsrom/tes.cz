<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */
 
 
if(!defined("SECURITY_CMS")) exit;

if((!$login_obj->UserPrivilege('admin') && !$login_obj->UserPrivilege('superadmin')) || !$login_obj->UserPrivilege('settings_delete')) exit;



$id = get_int_post('id');

if($id <= 0) exit;

$soubor_name = $db->get(TABLE_ZALOHY_DB,"soubor","idZalohy=".$id);

$db->delete(TABLE_ZALOHY_DB, "WHERE idZalohy=".$id." LIMIT 1");

$soubor = PRE_PATH.DIRNAME_DB_BACKUP.$soubor_name;
if($soubor_name!='' && file_exists($soubor))
    unlink($soubor);

$log->add_log('delete','zaloha-db',$id,"");


exit;


?>