<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */
 

define('POCET_ODKAZU_PRO_ZKONTROLOVANI', isset($_GET['limit']) ? intval($_GET['limit']) : 20);

require("../classes/class.links.checker.php");	

$chybne_odkazy = new LinksChecker($domain->getId(), 0, "", "", false, true);
$chybne_odkazy->process_cron(POCET_ODKAZU_PRO_ZKONTROLOVANI, false);


$result = array(
    'chybne_odkazy' => $chybne_odkazy->get_sum_error_links(), 
    'chybne_odkazy_obrazku' => $chybne_odkazy->get_sum_error_images()
    );

echo json_encode($result); 
    

?>