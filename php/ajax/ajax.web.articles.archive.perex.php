<?php

/**
 * @author Pavel Šrom
 * @copyright 2011
 */



if(!isset($_POST['idPolozky'])) exit;


$idPolozky = abs(intval($_POST['idPolozky']));

$data = $db->Query("SELECT perex, idStranky, url, obrazek, j.jazyk
    FROM ".TABLE_STRANKY." AS s
    LEFT JOIN ".TABLE_JAZYKY." AS j ON s.idJazyka = j.idJazyka
    WHERE zobrazit=1 
        AND idDomeny=".$domain->getId()." 
        AND typ='clanek' 
        AND idStranky=".$idPolozky." 
        AND archivovat=1 
    LIMIT 1");
    
if($db->numRows($data)==0) exit;

$c = $db->getAssoc($data);

$obrazek = $obsah = "";
$obrazek = $domain->getDir().USER_DIRNAME_ARTICLES_MINI.$c['obrazek'];
    
if(file_exists("../../".$obrazek) && $c['obrazek']!=''){
    $obrazek = $domain->getUrl().$obrazek;
    $obrazek = "<a href='".UrlPage($c['url'],$c['idStranky'],$c['jazyk'])."' title='".TCIST_VICE."'>
            <img src='".$obrazek."' alt='".$c['nadpis']."' title='".$c['nadpis']."' style='float: left'/>
            </a>";
            
        }
        else
        $obrazek = "";


$obsah .= $obrazek.strip_tags($c['perex'],'<p><i><em><b><strong><u><a>');
$obsah .= "<div style='text-align: right;'><a href='".UrlPage($c['url'],$c['idStranky'],$c['jazyk'])."' title='".TCIST_VICE."' class='input button'>".TCIST_VICE."</a></div>";
echo $obsah;
?> 