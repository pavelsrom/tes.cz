<?php

/**
 * @author Pavel Srom
 * @copyright 2010
 */

//ajaxovy skript na odhlaseni kontaktu z odberu newsletteru

if(!isset($_GET['module']) || !defined("SECURITY_CMS")) exit;

$vyraz = trim($db->secureString($_GET['vyraz']));
$module = trim($_GET['module']);


//nastaveni limitu
$limit = "";
if(isset($_GET['iDisplayLength'])){
    $dStart = "";
    if(isset($_GET['iDisplayStart']))
        $dStart = abs(intval($_GET['iDisplayStart'])).",";
        
    $limit = " LIMIT ".$dStart.abs(intval($_GET['iDisplayLength'])); 
}


//trideni podle sloupcu
$order = "";
if(isset($_GET['iSortCol_0'])){
    $s = abs(intval($_GET['iSortCol_0']));
    if($s == 0) $order = "s.id";
    elseif($s == 1) $order = "s.jmeno";
    elseif($s == 4) $order = "s.dotaz_datum";
    elseif($s == 5) $order = "s.aktivni";
    else $order = "s.id";
}

if($order != '') 
    $order = "ORDER BY ".$order." ".(isset($_GET['sSortDir_0'])?$_GET['sSortDir_0']:'');

//echo $order;

//sestaveni podminky pro vyhledavani
$like = array();
if($vyraz != '')
{
    $like[] = "s.id LIKE '".$vyraz."%'";
    $like[] = "s.jmeno LIKE '%".$vyraz."%'";
    $like[] = "s.dotaz LIKE '%".$vyraz."%'";
    $like[] = "s.odpoved LIKE '%".$vyraz."%'";
}
    


$where = "WHERE s.id_jazyka=".WEB_LANG_ID." AND s.id_domeny=".$domain->getId().((count($like) > 0) ? " AND (".implode(" OR ",$like).")" : "");

$data_pocet = $db->Query("SELECT COUNT(id) AS celkem FROM ".TABLE_PODPORA." AS s ".$where);
$iTotal_arr = $db->getAssoc($data_pocet);
$iTotal = $iTotal_arr['celkem'];


        
$data = $db->Query("SELECT SQL_CALC_FOUND_ROWS s.*, DATE_FORMAT(s.dotaz_datum,'%d.%m.%Y') AS datum, j.jazyk
				FROM ".TABLE_PODPORA." AS s
                LEFT JOIN ".TABLE_JAZYKY." AS j ON s.id_jazyka = j.idJazyka
				".$where."
				GROUP BY s.id
				".$order."
                ".$limit."
				");


$iFilteredTotal_data = $db->Query("SELECT FOUND_ROWS()");
$iFilteredTotal_arr = $db->getRow($iFilteredTotal_data);
$iFilteredTotal = intval($iFilteredTotal_arr[0]);


$dis_content = !$login_obj->UserPrivilege('content_view');
$dis_delete = !$login_obj->UserPrivilege('content_delete');

$tr = array();
while($c = $db->getAssoc($data))
{
    $ids = $c['id'];
    
    $pom = array();
    $pom[] = json_encode($ids);
    $pom[] = json_encode(secureString($c['jmeno']));
    $pom[] = json_encode(secureString(cut_text($c['dotaz'],150)));
    $pom[] = json_encode(secureString(cut_text($c['odpoved'],150)));
    $pom[] = json_encode(secureString($c['datum']));
    $pom[] = json_encode(ano_ne($c['aktivni']));
    
    $ikony = "";
    
    
    if($dis_content)
        $ikony .= icon_disabled('content');
        else
        $ikony .= "<a href='".get_link($module,'helpdesk',0,'edit-content',$ids)."'>".icon('content')."</a>";
    
    if($dis_delete || !$object_access->has_access($ids))
        $ikony .= icon_disabled('delete');
        else
        $ikony .= "<a href='#' class='delete' id='delete".$ids."'>".icon('delete')."</a>";
        
    $pom[] = json_encode($ikony);
        
    $tr[] = "[".implode(',', $pom)."]";
    
}

echo "{";
echo '"sEcho": '.(intval($_GET['sEcho'])).', ';
echo '"iTotalRecords": '.$iTotal.', ';
echo '"iTotalDisplayRecords": '.($iFilteredTotal).', ';
echo '"aaData": ['.implode(',', $tr).'] }';


?>