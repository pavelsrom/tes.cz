<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined("SECURITY_CMS")) exit;

$allowed_action = array("'create'","'edit-settings'","'edit-content'","'delete'","'upload'");
$logList = new LogList($domain->getId(),0,0,0,0,$allowed_action,"","",10);

$list = $logList->get_list();

echo "<table class='tAkceUzivatelu'>";
foreach($list AS $l)
{
    echo "<tr><td>".icon("user_time")."</td><td>".$l->get_date()." ".$l->get_user_name()." - ".strip_tags($l->get_text())."</td></tr>";
}

echo "</table>";
echo "<br />";
echo "<a class='f-right' href='".get_link("home","log")."' title='".TPODIVAT_SE_NA_KOMPLETNI_LOG."'>".TPODIVAT_SE_NA_KOMPLETNI_LOG."</a>";
echo "<div class='cleaner'></div>";

?>