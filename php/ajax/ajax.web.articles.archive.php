<?php

/**
 * @author Pavel Šrom
 * @copyright 2011
 */



if(!isset($_GET['sSearch'])) exit;


$vyraz = isset($_GET['vyraz'])?$db->secureString(strip_tags($_GET['vyraz'])):'';
$od = isset($_GET['datum_od'])?GetUniDate($_GET['datum_od']):'';
$do = isset($_GET['datum_do'])?GetUniDate($_GET['datum_do']):'';
$rubrika = isset($_GET['rubrika'])?abs(intval($_GET['rubrika'])):0;

//trideni podle sloupcu
$order = "";
if(isset($_GET['iSortCol_0'])){
    $s = abs(intval($_GET['iSortCol_0']));
    if($s == 0 || $s == 1) $order = "c.datum";
    elseif($s == 2) $order = "u.prijmeni";
    elseif($s == 3) $order = "r.nadpis";
    elseif($s == 4) $order = "c.nadpis";
}

if($order != '') 
    $order = "ORDER BY ".$order." ".(isset($_GET['sSortDir_0'])?$_GET['sSortDir_0']:'');

//vyhledavani retezce
$like = "";
if($vyraz !=''){
    $like = "AND (c.nadpis LIKE '%".$vyraz."%' OR c.perexBezHtml LIKE '%".$vyraz."%' OR c.obsahBezHtml LIKE '%".$vyraz."%')";
    
}

//filtrovani podle data
$date_range = "";
if($od != '' || $do != ''){
    
    if($od != '')
        $date_range .= " AND c.datum >= '".$od."' ";
    if($do != '')
        $date_range .= " AND c.datum <= '".$do."' ";
}

//filtrovani podle rubriky
$like_rubrika = "";
if($rubrika > 0){
    $like_rubrika = " AND r.idStranky=".$rubrika;
}


//nastaveni limitu
$limit = "";
if(isset($_GET['iDisplayLength'])){
    $dStart = "";
    if(isset($_GET['iDisplayStart']))
        $dStart = abs(intval($_GET['iDisplayStart'])).",";
        
    $limit = " LIMIT ".$dStart.abs(intval($_GET['iDisplayLength'])); 
}


$data_pocet = $db->Query("SELECT COUNT(idStranky) AS celkem FROM ".TABLE_STRANKY." WHERE zobrazit=1 AND idDomeny=".$domain->getId()." AND typ='clanek'");

$iTotal_arr = $db->getAssoc($data_pocet);
$iTotal = $iTotal_arr['celkem'];

$data = $db->Query("SELECT SQL_CALC_FOUND_ROWS r.nadpis AS rubrika, c.*, 
    DATE_FORMAT(c.datum, '%d.%m.%Y') AS datum, r.autor, j.jazyk
    IF(c.obrazek is null,'',c.obrazek) AS obrazek  
    FROM ".TABLE_STRANKY." AS r 
    LEFT JOIN ".TABLE_JAZYKY." AS j ON r.idJazyka = j.idJazyka
    RIGHT JOIN ".TABLE_STRANKY." AS c ON r.idStranky=c.idRodice
    WHERE r.zobrazit=1
        AND c.zobrazit=1 
        AND c.idDomeny=".$domain->getId()."
        AND r.idDomeny=".$domain->getId()."
        AND c.archivovat=1
        ".$like."
        ".$date_range."
        ".$like_rubrika."
    GROUP BY c.idStranky
    ".$order."
    ".$limit."
    ");

$iFilteredTotal_data = $db->Query("SELECT FOUND_ROWS()");
$iFilteredTotal_arr = $db->getRow($iFilteredTotal_data);
$iFilteredTotal = intval($iFilteredTotal_arr[0]);

$tr = array();
while($c = $db->getAssoc($data)){
    $pom = array();
    $obrazek = $obsah = "";
    
    /*
    $obrazek = $domain->getDir().USER_DIRNAME_ARTICLES_MINI.$c['obrazek'];
    
    if(file_exists("../../".$obrazek) && $c['obrazek']!=''){
        //echo $obrazek;
        $obrazek = $domain->getUrl().$obrazek;
        $obrazek = "<a href='".UrlPage($c['url'],$c['idStranky'])."' title='číst více'>
            <img src='".$obrazek."' alt='".$c['nadpis']."' title='".$c['nadpis']."' />
            </a>";
            
        }
        else
        $obrazek = "";
    */    
    
    $obsah = $obrazek."<a href='".UrlPage($c['url'],$c['idStranky'],$c['jazyk'])."'>".$c['nadpis']."</a>";
    //$obsah .= "<br />".strip_tags($c['perex'],'<p><i><em><b><strong><u><a>');
    $pom[] = json_encode("<img src='img/icon_plus.gif' id='c".$c['idStranky']."' alt='".TDETAIL."' title='".TDETAIL."' style='cursor: pointer' class='ikona-detail'/>");
    $pom[] = json_encode($c['datum']);
    $pom[] = json_encode($c['autor']);
    $pom[] = json_encode($c['rubrika']);
    $pom[] = json_encode($obsah);
    
    $tr[] = "[".implode(',', $pom)."]";
    }    
    

echo "{";
echo '"sEcho": '.(intval($_GET['sEcho'])).', ';
echo '"iTotalRecords": '.$iTotal.', ';
echo '"iTotalDisplayRecords": '.($iFilteredTotal).', ';
echo '"aaData": ['.implode(',', $tr).'] }';

?>