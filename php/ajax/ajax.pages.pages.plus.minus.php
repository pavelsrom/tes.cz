<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined("SECURITY_CMS")) exit;

$idStranky = get_int_post('id');
$typ = get_secure_post('typ'); 

if($idStranky <= 0 || $typ == "") exit;

$zakazane_zobrazeni = $db->get(TABLE_UZIVATELE,"nastaveniSeznamStranek","idUzivatele=".$login_obj->getId());

if(strstr($zakazane_zobrazeni,"#".$idStranky."#") && $typ == "plus")
    $zakazane_zobrazeni = str_replace("#".$idStranky, "", $zakazane_zobrazeni);
elseif(!strstr($zakazane_zobrazeni,"#".$idStranky."#") && $typ == "minus")
    $zakazane_zobrazeni = $zakazane_zobrazeni."#".$idStranky."#";
    
$zakazane_zobrazeni = str_replace(array("##","###"),array("#","#"),$zakazane_zobrazeni);

$db->update(TABLE_UZIVATELE,array("nastaveniSeznamStranek" => $zakazane_zobrazeni),"idUzivatele=".$login_obj->getId());

exit;
        
?>