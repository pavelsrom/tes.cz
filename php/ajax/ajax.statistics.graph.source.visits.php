<?php

/**
 * @author Pavel Srom
 * @copyright 2011
 */



include_once(PRE_PATH.'php/classes/class.gapi.php');


//-----------------------------------------------------------------
//cacheovani grafu
//-----------------------------------------------------------------
$posledni_zmena = mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y"));
$gmt_posledni_zmena = gmdate("D, d M Y H:i:s", $posledni_zmena)." GMT"; 

header('Content-Type: application/json');		
Header("Cache-Control: must-revalidate");
header("Pragma: private");
$offset = 60 * 60 * (24 - abs(date("H") - 24)); //den
$ExpStr = "Expires: " . gmdate("D, d M Y H:i:s", time() + $offset) . " GMT";
Header($ExpStr);
	
if(isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) && $_SERVER['HTTP_IF_MODIFIED_SINCE'] == $gmt_posledni_zmena){
	header('Last-Modified: '.$_SERVER['HTTP_IF_MODIFIED_SINCE'],true,304);
	exit;
	}

Header("Last-Modified: ".$gmt_posledni_zmena);
Header("Cache-Control: public");
//-----------------------------------------------------------------

 

if(!isset($_REQUEST['od']) || !isset($_REQUEST['do']) || !defined("SECURITY_CMS")) exit;

//file_put_contents('test.txt', "test22");

$start = (isset($_REQUEST['od'])?$_REQUEST['od']:'');
$end = (isset($_REQUEST['do'])?$_REQUEST['do']:'');

$start = GetGADate($start);
$end = GetGADate($end);

if($start=='' || strlen($start)!=10)
	if($end!=''){
		$start = $s = explode('-', $end);
		$start = $s[0].".".$s[1]."-01";
		}
		else 
		$start=strftime("%Y-%m-01", time());
        
if($end=='' || strlen($end)!=10) $end=strftime("%Y-%m-%d", time());



if(!$login_obj->UserPrivilege('statistics_view')) exit;

$error = "";

try{
	$ga = new gapi(GA_EMAIL,GA_HESLO);
	}
	catch(exception $e){
    
	exit;	
	}
	
$idProfile = GA_PROFIL;
$maxRecords = 10;

$filter = null;
//sbírání dat pro pie graf odkud návštěvníci chodí.
$ga->requestReportData($idProfile,array('medium'),array('visits'),null,null,null,$start,$end,1,$maxRecords);
$res = $ga->getResults();

//print_r($res);
$suma_campaign = $suma_search = $suma_referral = $suma_direct = 0;

foreach($res AS $r){
    $v = $r->getVisits();
    $medium = $r->dimensions['medium'];
    if($medium == 'organic' || strstr($medium,'cpc'))
        $suma_search += $v;
    elseif($medium == 'referral')
        $suma_referral += $v;
    elseif($medium == '(none)')
        $suma_direct += $v;
    elseif($medium != '(non set)')
        $suma_campaign += $v;
    else
        continue;
    
    }

//vypocet procent
$suma_celkem = $suma_direct + $suma_referral + $suma_search + $suma_campaign; 
$suma_direct_procent = round(($suma_direct / $suma_celkem) * 100,1);
$suma_referral_procent = round(($suma_referral / $suma_celkem) * 100,1);
$suma_search_procent = round(($suma_search / $suma_celkem) * 100,1);
$suma_campaign_procent = round(($suma_campaign / $suma_celkem) * 100,1);

$titulek = TZDROJE_NAVSTEVNOSTI;

$s = explode('-', $start);
$e = explode('-', $end);
$subtitle = strtolower(TOD)." ".intval($s[2]).".".intval($s[1]).".".$s[0]." ".strtolower(TDO)." ".intval($e[2]).".".intval($e[1]).".".$e[0];



$options = array(
    "chart" => array(
        "plotBackgroundColor" => null,
        "plotBorderWidth" => null
        ),
    "title" => array(
        "text" => $titulek,
        "style" => array(
            "fontFamily" => "Arial",
            "fontSize"  => "16px",
            "fontWeight"  => "bold"
            )
        ),
    "tooltip" => array(
        "pointFormat" => '{series.name}: <b>{point.y}%</b>',
        "valueDecimals" => 1
    ),            
    "subtitle" => array(
        "text" => $subtitle,
        "style" => array(
            "fontFamily" => "Arial",
            "fontSize"  => "14px"
            )
        ),
        
    "plotOptions" => array(
        "pie" => array(
            "dataLabels" => array(
                "enabled" => true,
                "format" => "<b>{point.name}</b>: {point.y}%"    
                )
            )
        ),
        
    "series" => array(
        array(
            "type" => "pie",
            "name" => TNAVSTEVNOST,
            "data" => array(
                array(TPRIMA_NAVSTEVNOST, $suma_direct_procent),
                array(TVYHLEDAVACE, $suma_search_procent),
                array(TODKAZUJICI_STRANKY, $suma_referral_procent),
                array(TKAMPANE, $suma_campaign_procent)
                ),
            "style" => array(
                "fontFamily" => "Arial",
                "fontSize"  => "15px"
                )
            )
        ),
        
    "labels" => array(
        "style" => array(
                "fontFamily" => "Arial",
                "fontSize"  => "15px"
                )
        )                
        
    );
    
echo json_encode($options);

?>