<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!is_post('id') || !defined("SECURITY_CMS")) exit;
if(!$login_obj->UserPrivilege('content_delete')) exit;

$id = get_int_post('id');
$typ = $db->get(TABLE_STRANKY,'typ',"idStranky='".$id."' AND idDomeny=".$domain->getId()." AND idJazyka=".WEB_LANG_ID."");


//overeni jestli galerie neobsahuje dalsi vnorene galerie nebo slozky			
$data = $db->Query("SELECT idStranky 
            FROM ".TABLE_STRANKY." AS g 				
            WHERE g.idRodice=".$id."
                AND g.typ IN ('galerie-slozka','galerie')
				AND g.idDomeny=".$domain->getId()
                );            

if($db->numRows($data) > 0) exit;            

$data = $db->Query("SELECT obrazek AS soubor, idStranky AS id FROM ".TABLE_STRANKY." AS g 
				WHERE g.idRodice=".$id."
                    AND g.typ = 'galerie-fotka'
					AND g.idDomeny=".$domain->getId());
					
$dir1 = PRE_PATH.$domain->getDir().USER_DIRNAME_GALLERY_MAXI;
$dir2 = PRE_PATH.$domain->getDir().USER_DIRNAME_GALLERY_MINI;
$dir3 = PRE_PATH.$domain->getDir().USER_DIRNAME_GALLERY_STREDNI;
            
while($img = $db->getAssoc($data))
{
    //overeni zda se jeden obrazek nevyskytuje na vice fotkach
    $d = $db->query("SELECT idStranky FROM ".TABLE_STRANKY." WHERE idDomeny='".$domain->getId()."' AND obrazek='".$img['soubor']."' AND idStranky!='".$img['id']."'");
    if($db->numRows($d) > 0) continue;
    
    if($img['soubor']!='' && file_exists($dir1.$img['soubor'])) 
        @unlink($dir1.$img['soubor']);
					
    if($img['soubor']!='' && file_exists($dir2.$img['soubor'])) 
        @unlink($dir2.$img['soubor']);
					
    if($img['soubor']!='' && file_exists($dir3.$img['soubor'])) 
        @unlink($dir3.$img['soubor']);
}	
            
            
$nazev = $db->get(TABLE_STRANKY,'nazev','idStranky='.$id);        
$db->delete(TABLE_STRANKY,"(idStranky=".$id." OR idRodice=".$id.") AND idDomeny=".$domain->getId());
$db->delete(TABLE_STRANKY_TAGY, "idObjektu=".$id." AND typObjektu='stranka'");
$db->delete(TABLE_STRANKY_EDITORI,"idObjektu=".$id." AND typ = 'stranka'");
$db->delete(TABLE_ODKAZY,"typ='stranka' AND idObjektu=".$id);
$db->delete(TABLE_ZALOHY,"typ='stranka' AND idObjektu=".$id);
$db->delete(TABLE_DISKUZE_POLOZKY,"idStranky=".$id);
$db->delete(TABLE_STRANKY_SKUPINY, "idStranky=".$id);

$log->add_log('delete',$typ,$id,$nazev);

?>