<?php

/**
 * @author Pavel Srom
 * @copyright 2010
 */

//ajaxovy skript na odhlaseni kontaktu z odberu newsletteru

if(!isset($_GET['module']) || !defined("SECURITY_CMS")) exit;



function get_typ($id, $ityp, $pocet_potomku = 0)
{
    $typ = "";
    if($ityp=='stranka') 
        $typ=TSTRANKA;
    elseif($ityp=='tagy' && MODULE_RELATED_ARTICLES) 
        $typ=TTAGY;
    elseif($ityp=='sitemap') 
        $typ=TMAPA_WEBU;
    elseif($ityp == 'obchodni-podminky' && MODULE_ESHOP) 
        $typ = TSTRANKA_OBCHODNI_PODMINKY;
    elseif($ityp == 'objednavka' && MODULE_ESHOP) 
        $typ = TSTRANKA_OBJEDNAVKA;
    elseif($ityp=='novinky' && MODULE_NEWS) 
        $typ=TNOVINKY;
    elseif($ityp=='clanky' && MODULE_ARTICLES) 
        $typ=TCLANKY;
    elseif($ityp=='archiv-clanku' && MODULE_ARTICLES_ARCHIVE) 
        $typ=TARCHIV_CLANKU;
    elseif($ityp=='404') 
        $typ=TCHYBOVA_STRANKA;
    elseif($ityp=='stranka-galerie' && MODULE_GALLERY) 
        $typ=TSTRANKA_S_GALERII;
    elseif($ityp=='prazdna-stranka') 
        $typ=TPRAZDNA_STRANKA;
    elseif($ityp=='uvodni') 
        $typ=TUVODNI_STRANKA;
    elseif($ityp=='kalendar-akci' && MODULE_EVENT_CALENDAR) 
        $typ=TKALENDAR_AKCI;
    elseif($ityp=='vyhledavani' && MODULE_FINDER) 
        $typ=TSTRANKA_S_VYSLEDKY_VYHLEDAVANI;
    elseif($ityp=='login' && MODULE_PRIVATE_PAGES) 
        $typ=TPRIHLASOVACI_STRANKA;
    elseif($ityp == 'email' && MODULE_NEWSLETTER) 
        $typ = TSTRANKA_ODBER_NOVINEK;
    elseif($ityp == 'produkty' && MODULE_ESHOP) 
        $typ = TSTRANKY_S_PRODUKTY;
    else 
        $typ=TSTRANKA;


    if($ityp == 'tagy' && MODULE_ARTICLES && MODULE_RELATED_ARTICLES)
        $typ = "<a href='".get_link('articles','tagy')."'>".$typ."</a> (".$pocet_potomku.")";
    elseif($ityp == 'clanky' && MODULE_ARTICLES)
        $typ = "<a href='".get_link('articles','rubriky',$id)."'>".$typ."</a> (".$pocet_potomku.")";
    elseif($ityp == 'produkty' && MODULE_ESHOP)
        $typ = "<a href='".get_link('eshop','kategorie',$id)."'>".$typ."</a> (".$pocet_potomku.")";
    elseif($ityp == 'novinky' && MODULE_NEWS)
        $typ = "<a href='".get_link('news','novinky')."'>".$typ."</a> (".$pocet_potomku.")";    
    elseif($ityp == 'stranka-galerie' && MODULE_GALLERY)
        $typ = "<a href='".get_link('gallery','galerie')."'>".$typ."</a>";
    elseif($ityp == 'kalendar-akci' && MODULE_EVENT_CALENDAR)
        $typ = "<a href='".get_link('calendar','akce-typy')."'>".$typ."</a> (".$pocet_potomku.")";
        
    return $typ;  
}













$vyraz = trim($db->secureString($_GET['vyraz']));
$module = trim($_GET['module']);


//nacteni nastaveni uzivatele, ktere stranky budou skryty a ktere odkryty
$zakazane_zobrazeni = $db->get(TABLE_UZIVATELE,"nastaveniSeznamStranek","idUzivatele=".$login_obj->getId());
$zakazane_stranky = explode("#",$zakazane_zobrazeni);
foreach($zakazane_stranky AS $idzs => $zs)
    if($zs == "") 
        unset($zakazane_stranky[$idzs]);


//nastaveni limitu
$limit = "";
if(isset($_GET['iDisplayLength'])){
    $dStart = "";
    if(isset($_GET['iDisplayStart']))
        $dStart = abs(intval($_GET['iDisplayStart'])).",";
        
    $limit = " LIMIT ".$dStart.abs(intval($_GET['iDisplayLength'])); 
}


//trideni podle sloupcu
$order = "";
if(isset($_GET['iSortCol_0'])){
    $s = abs(intval($_GET['iSortCol_0']));
    if($s == 0) $order = "s.idStranky";
    else $order = "s.idStranky";
}

if($order != '') 
    $order = "ORDER BY ".$order." ".(isset($_GET['sSortDir_0'])?$_GET['sSortDir_0']:'');

//sestaveni podminky pro vyhledavani
$like = array();
if($vyraz != '')
{
    $like[] = "s.idStranky LIKE '".$vyraz."%'";
    $like[] = "s.nazev LIKE '%".$vyraz."%'";
}
    
$archivClanku = $modul_vyhledavani = $modul_kalendar = "";
if(MODULE_ARTICLES && MODULE_ARTICLES_ARCHIVE)
    $archivClanku = ",'archiv-clanku, tagy'";
if(MODULE_FINDER)
    $modul_vyhledavani = ",'vyhledavani'";
if(MODULE_EVENT_CALENDAR)
    $modul_kalendar = ",'kalendar-akci'";
        
$where = "WHERE s.typ IN ('stranka','clanky','novinky','stranka-galerie','prazdna-stranka','sitemap','kalendar-akci','404', 'tagy','uvodni','login','email','produkty','obchodni-podminky','objednavka' ".$modul_vyhledavani." ".$archivClanku." ".$modul_kalendar.") AND s.idDomeny=".$domain->getId()." AND s.idJazyka=".WEB_LANG_ID;


if(count($like) > 0)
    $where .= " AND (".implode(" OR ",$like).")";
    
$data_pocet = $db->Query("SELECT COUNT(idStranky) AS celkem FROM ".TABLE_STRANKY." AS s ".$where);
$iTotal_arr = $db->getAssoc($data_pocet);
$iTotal = $iTotal_arr['celkem'];


$data = $db->Query("SELECT SQL_CALC_FOUND_ROWS s.*, j.jazyk, COUNT(p.idStranky) AS potomci
    				FROM ".TABLE_STRANKY." AS s
                    LEFT JOIN ".TABLE_STRANKY." AS p ON s.idStranky = p.idRodice
                    LEFT JOIN ".TABLE_JAZYKY." AS j ON s.idJazyka = j.idJazyka
    				".$where."
    				GROUP BY s.idStranky
    				ORDER BY ".($vyraz == "" ? "s.priorita" : "s.typ IN ('404','vyhledavani','uvodni') DESC")."
			");


$iFilteredTotal_data = $db->Query("SELECT FOUND_ROWS()");
$iFilteredTotal_arr = $db->getRow($iFilteredTotal_data);
$iFilteredTotal = intval($iFilteredTotal_arr[0]);




function Tree($idRodice=0,$hloubka=0,$mSystemName,$id_stranek_ke_zobrazeni,$zobrazit_v_menu=true){
    global $db;
    global $domain;
    global $login_obj;
    global $links; 
    global $object_access;
    global $tr;      
    global $zakazane_stranky;  
    
    $dis_content = !$login_obj->UserPrivilege('content_view');
    $dis_settings = !$login_obj->UserPrivilege('settings_view');
    $dis_delete = !$login_obj->UserPrivilege('settings_delete');
    		
    $archivClanku = "";
    if(MODULE_ARTICLES && MODULE_ARTICLES_ARCHIVE)
        $archivClanku = ",'archiv-clanku'";
                
    $where = "WHERE s.typ IN ('uvodni','vyhledavani','stranka','clanky','novinky','stranka-galerie','prazdna-stranka','sitemap','tagy', 'kalendar-akci','login','email','produkty','obchodni-podminky', 'objednavka' ".$archivClanku.") 
        AND s.idDomeny=".$domain->getId()." 
        AND s.idJazyka=".WEB_LANG_ID."
        AND s.idRodice=".$idRodice."
        ";
            
    	
    $record = $db->Query("SELECT s.*, COUNT(r.idUzivatele) AS pocet_uzivatelu, j.jazyk, COUNT(p.idStranky) AS potomci
        FROM ".TABLE_STRANKY." AS s
        LEFT JOIN ".TABLE_STRANKY." AS p ON s.idStranky = p.idRodice
        LEFT JOIN ".TABLE_JAZYKY." AS j ON s.idJazyka = j.idJazyka
        LEFT JOIN ".TABLE_STRANKY_PRIVATNI_UZIVATELE." AS r ON s.idStranky=r.idStranky
        ".$where." 
        GROUP BY s.idStranky
        ORDER BY s.priorita 
        ");

    $pocet = $db->numRows($record);

    if($pocet==0) return '';
    $i = 0;       
    
            
    while($page = $db->getAssoc($record))
    {
        $i++;
        $pom = array();
        
        $id = $page['idStranky'];
     
        
        
        $autentizace = $page['autentizace'];
        $zobrazit = $page['zobrazit'];
        $menu = $page['doMenu'];
        //$hloubka = $page['hloubka'];   
        $zobrazit_stranku_v_menu = $menu==1 && ($zobrazit_v_menu || $hloubka == 0);			
        $doMenu = ano_ne($zobrazit_stranku_v_menu);   	
        //$zobrazit_v_menu	
                     
        $stav = ano_ne($page['zobrazit']);
        
        $res = $hloubka > 0 ? GetGraphWidth($hloubka, 7, $hloubka) : GetGraphWidth($hloubka, 0);
        
        /*
        if($menu==0)
            $zobrazit_v_menu = false;
        */       
        if(!in_array($id,$id_stranek_ke_zobrazeni))
        {
            Tree($id,$hloubka +1,$mSystemName,$id_stranek_ke_zobrazeni, $zobrazit_v_menu);
            continue;
        }
                        
        $nazev = $page['nazev'];
        $ityp = $page['typ'];
        $potomci = $page['potomci'];
        
        $typ = get_typ($id, $ityp,$potomci);
        
        $private_pages = new C_PrivatePages($id);
        $privatni = ano_ne($private_pages->is_private(),implode(", ", $private_pages->get_groups())); 

        $ikony_up_down = "";
        if($i==1 || !$login_obj->UserPrivilege('settings_edit'))
            $ikony_up_down .= icon_disabled('up');
            else
            $ikony_up_down .= "<a href='#' id='up".$id."' class='up'>".icon('up')."</a>";
        				
        if($i == $pocet || !$login_obj->UserPrivilege('settings_edit'))
            $ikony_up_down .= icon_disabled('down');
            else
            $ikony_up_down .= "<a href='#' id='down".$id."' class='down'>".icon('down')."</a>";
               
         
        $ikony = "";
            
        $ikony .= "<a href='".urlPage($page['url'],$id,$page['jazyk'])."' target='_blank'>".icon('link')."</a>";
        
        
        if($dis_content)
            $ikony .= icon_disabled('content');
    		else	
    		$ikony .= "<a href='".get_link($mSystemName,'stranky',0,'edit-content',$id)."'>".icon('content')."</a>";
    			
        $nelze_smazat_stranky = array('stranka','sitemap','archiv-clanku','stranka-galerie','novinky','prazdna-stranka','clanky','clanek','novinka','produkty','produkt','obchodni-podminky','objednavka','email');
        $nelze_smazat_stranky_typy = array('sitemap','archiv-clanku','stranka-galerie','404','vyhledavani','uvodni','novinky','kalendar-akci','login','email','obchodni-podminky','objednavka','email');
        
        if($page["povolit_mazani"] == 0 && !$login_obj->minPrivilege("admin"))
            $ikony .=  icon_disabled('delete',TMAZANI_STRANKY_BYLO_ZAKAZNO_ADMINEM);
        elseif($dis_delete || !$object_access->has_access($id))
            $ikony .=  icon_disabled('delete'); 
        elseif($links->HaveChildren($id, $nelze_smazat_stranky))
            $ikony .=  icon_disabled('delete',TNELZE_SMAZAT_STRANKA_OBSAHUJE_PODRIZENE_STRANKY);
        elseif(in_array($ityp, $nelze_smazat_stranky_typy))
            $ikony .=  icon_disabled('delete',TNELZE_SMAZAT_STRANKA_JE_SYSTEMOVA);
        else
            $ikony .=  "<a href='#' id='delete".$id."' class='delete'>".icon('delete')."</a>";

        
        //overeni zda ma stranka podstranky
        $icon_plus_minus = $links->hasSubpage($id, array('akce','kalendar-typ','clanek','novinka','galerie-slozka','galerie','galerie-fotka','tag','produkt')) ? "" : icon('plus_minus_disabled');
        $icon_plus_minus_class = "";
        $icon_plus_minus_id = "";
        if($icon_plus_minus == "")
        {
            $icon_plus_minus_class = (in_array($id, $zakazane_stranky) ? "plus" : "minus");
            $icon_plus_minus = icon($icon_plus_minus_class);
            $icon_plus_minus_id = "id='".$icon_plus_minus_class.$id."'";
            $icon_plus_minus_class = "class='".$icon_plus_minus_class."'";
            $icon_plus_minus_a = "<a href='#' ".$icon_plus_minus_class." ".$icon_plus_minus_id.">".$icon_plus_minus."</a> ";
        } 
        else
        $icon_plus_minus_a = "<span class='plus_minus_none'>".$icon_plus_minus."</span>";

        

        $pom["DT_RowClass"] = "uroven".$hloubka;
        $pom["0"] = $id;
        $pom["1"] = $res['mezera'].$icon_plus_minus_a." ".secureString($nazev);
        $pom["2"] = $res['graf'];
        $pom["3"] = $typ;
        $pom["4"] = $doMenu;
        $pom["5"] = $privatni;
        $pom["6"] = $stav;
        //$pom["7"] = $ikony_up_down;
        $pom["7"] = $ikony_up_down.$ikony;

        $tr[] = array2json($pom);
        
        if(!in_array($id, $zakazane_stranky))
            Tree($id,$hloubka+1,$mSystemName,$id_stranek_ke_zobrazeni, $zobrazit_v_menu);
    
        }
    
    }
    
    

$tr = array();
$dis_content = !$login_obj->UserPrivilege('content_view');
$dis_settings = !$login_obj->UserPrivilege('settings_view');
$dis_delete = !$login_obj->UserPrivilege('settings_delete');
   
$specialni_stranky = array(/*'404', 'uvodni'*/);
/*
if(MODULE_FINDER)
    $specialni_stranky[] = "vyhledavani";
*/
    
if($vyraz == "")
{
    $id_stranek_ke_zobrazeni = array();
    $pocet_specialnich_stranek = count($specialni_stranky);
    $i = 1;
  
    while($p = $db->getAssoc($data))
    {
        
        if(in_array($p['typ'], $specialni_stranky))
        {
            $id = $p['idStranky'];		
            $ityp = secureString($p['typ']);    
            $typ = get_typ($id, $ityp);
                                            
            $nazev = secureString($p['nazev']);
            $autentizace = $p['autentizace'];
            $zobrazit = $p['zobrazit'];
        	$stav = $p['zobrazit']==0?"<span class='stav0'>".TNE."</span>":"<span class='stav1'>".TANO."</span>";
            $doMenu =  $p['doMenu']==1 ? "<span class='stav1'>".TANO."</span>" : "<span class='stav0'>".TNE."</span>";            
            $url = ($p['typ']=='uvodni' ? $domain->getRelativeUrl().(WEB_LANG == DEFAULT_LANG ? "" : WEB_LANG."/"):UrlPage($p['url'],$id,$p['jazyk']));

             
            $ikony = "";
            $ikony .= "<a href='".UrlPage($p['url'],$id,$p['jazyk'])."' target='_blank'>".icon('link')."</a>";
            
            $private_pages = new C_PrivatePages($id);
            $privatni = ano_ne($private_pages->is_private(),implode(", ", $private_pages->get_groups())); 
        
            if($dis_content)
                $ikony .= icon_disabled('content');
                else	
                $ikony .= "<a href='".get_link($module,'stranky',0,'edit-content',$id)."'>".icon('content')."</a>";
            $pom = array();
            $pom["DT_RowClass"] = "zvyrazni_radek ".($pocet_specialnich_stranek == $i ? "posledni_radek" : "");
            $pom["0"] = $id;
            $pom["1"] = $nazev;      
            $pom["2"] = "";
            $pom["3"] = $typ;
            $pom["4"] = $doMenu;
            $pom["5"] = $privatni;
            $pom["6"] = $stav;
            //$pom["7"] = "";
            $pom["7"] = $ikony;
            
            $tr[] = array2json($pom);
  
            $i++;
        }
        else
        {
        $id_stranek_ke_zobrazeni[] = $p['idStranky'];
        }
    }

    Tree(0,0,$module,$id_stranek_ke_zobrazeni);
}
else
{
    $pocet_specialnich_stranek = 0;
    $page_data = array();
    while($p = $db->getAssoc($data))
    {
        if(in_array($p['typ'], $specialni_stranky))
            $pocet_specialnich_stranek++;
            
        $page_data[] = $p;
    }

    $i = 1;
    foreach($page_data AS $page)
    {
        
        $id = $page['idStranky'];           
        $nazev = $page['nazev'];
        $ityp = $page['typ'];
        /*
        if($ityp=='stranka') $typ=TSTRANKA;
        elseif($ityp=='tagy' && MODULE_RELATED_ARTICLES) $typ=TTAGY;
        elseif($ityp=='sitemap') $typ=TMAPA_WEBU;
        elseif($ityp=='novinky' && MODULE_NEWS) $typ=TNOVINKY;
        elseif($ityp=='clanky' && MODULE_ARTICLES) $typ=TCLANKY;
        elseif($ityp=='archiv-clanku' && MODULE_ARTICLES_ARCHIVE) $typ=TARCHIV_CLANKU;
        elseif($ityp=='404') $typ=TCHYBOVA_STRANKA;
        elseif($ityp=='stranka-galerie' && MODULE_GALLERY) $typ=TSTRANKA_S_GALERII;
        elseif($ityp=='prazdna-stranka') $typ=TPRAZDNA_STRANKA;
        elseif($ityp=='uvodni') $typ=TUVODNI_STRANKA;
        elseif($ityp=='kalendar-akci' && MODULE_EVENT_CALENDAR) $typ=TKALENDAR_AKCI;
        elseif($ityp=='vyhledavani' && MODULE_FINDER) $typ=TSTRANKA_S_VYSLEDKY_VYHLEDAVANI;
        elseif($ityp=='login' && MODULE_PRIVATE_PAGES) $typ=TPRIHLASOVACI_STRANKA;
        else $typ='';
        */
        $typ = get_typ($id,$ityp);
        
        $autentizace = $page['autentizace'];
        $zobrazit = $page['zobrazit'];
        $menu = $page['doMenu'];
        $hloubka = $page['hloubka'];   			
        $doMenu = ($page['doMenu']==1 && $links->isShowInMenu($id)) ? "<span class='stav1'>".TANO."</span>" : "<span class='stav0'>".TNE."</span>";   		       
        	
        $stav = ano_ne($page['zobrazit']);
        $res = $hloubka > 0 ? GetGraphWidth($hloubka, 5, $hloubka) : GetGraphWidth($hloubka, 0);
               
        $private_pages = new C_PrivatePages($id);
            $privatni = ano_ne($private_pages->is_private(),implode(", ", $private_pages->get_groups())); 
            
        $link = "<a href='".urlPage($page['url'],$id,$page['jazyk'])."' target='_blank'>".icon('link')."</a>";
        $pom = array();
        
        $ikony = $link;
        
        if($dis_content)
            $ikony .= icon_disabled('content');
    		else	
    		$ikony .= "<a href='".get_link($module,'stranky',0,'edit-content',$id)."'>".icon('content')."</a>";
    	
        
        
        $nelze_smazat_stranky = array('stranka','sitemap','archiv-clanku','stranka-galerie','novinky','prazdna-stranka','clanky','clanek','novinka','produkty','produkt','obchodni-podminky','objednavka','email');
        $nelze_smazat_stranky_typy = array('sitemap','archiv-clanku','stranka-galerie','404','vyhledavani','uvodni','novinky','kalendar-akci','login','email','obchodni-podminky','objednavka','email');
        
        
        
        if(!in_array($ityp, $specialni_stranky))		
            if($page["povolit_mazani"] == 0 && !$login_obj->minPrivilege("admin"))
                $ikony .=  icon_disabled('delete',TMAZANI_STRANKY_BYLO_ZAKAZNO_ADMINEM);		
            elseif($dis_delete)

                $ikony .=  icon_disabled('delete'); 
            elseif($links->HaveChildren($id, $nelze_smazat_stranky))
                $ikony .=  icon_disabled('delete',TNELZE_SMAZAT_STRANKA_OBSAHUJE_PODRIZENE_STRANKY);
            elseif(in_array($ityp, $nelze_smazat_stranky_typy))
                $ikony .=  icon_disabled('delete',TNELZE_SMAZAT_STRANKA_JE_SYSTEMOVA);
            else
                $ikony .=  "<a href='#' id='delete".$id."' class='delete'>".icon('delete')."</a>";
                
        if(in_array($ityp, $specialni_stranky))
        {
            $pom["DT_RowClass"] = "zvyrazni_radek ".($pocet_specialnich_stranek == $i ? "posledni_radek" : "");
            $i++;
        }

        $ikony_up_down = "";
        if(!in_array($ityp,array('404','uvodni','vyhledavani')))
        {
            $ikony_up_down .= icon_disabled('up',TNELZE_POSOUVAT_Z_DUVODU_FILTRACE);
            $ikony_up_down .= icon_disabled('down',TNELZE_POSOUVAT_Z_DUVODU_FILTRACE);
        }
        
              
        $pom["0"] = $id;
        $pom["1"] = $nazev;      
        $pom["2"] = "";
        $pom["3"] = $typ;
        $pom["4"] = $doMenu;
        $pom["5"] = $privatni;
        $pom["6"] = $stav;
        //$pom["7"] = $ikony_up_down;
        $pom["7"] = $ikony;
        
        $tr[] = array2json($pom);
            
        
    }
}

echo "{";
echo '"sEcho": '.(intval($_GET['sEcho'])).', ';
echo '"iTotalRecords": '.intval($iTotal).', ';
echo '"iTotalDisplayRecords": '.($iFilteredTotal).', ';
echo '"aaData": ['.implode(',', $tr).'] }';


?>