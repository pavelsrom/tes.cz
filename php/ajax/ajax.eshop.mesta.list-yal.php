<?php

/**
 * @author Pavel Srom
 * @copyright 2010
 */

//ajaxovy skript na odhlaseni kontaktu z odberu newsletteru

if(!isset($_GET['module']) || !defined("SECURITY_CMS")) exit;

$vyraz = trim($db->secureString($_GET['vyraz']));
$module = trim($_GET['module']);

//nastaveni limitu
$limit = "";
if(isset($_GET['iDisplayLength'])){
    $dStart = "";
    if(isset($_GET['iDisplayStart']))
        $dStart = abs(intval($_GET['iDisplayStart'])).",";
        
    $limit = " LIMIT ".$dStart.abs(intval($_GET['iDisplayLength'])); 
}


//trideni podle sloupcu
$order = "";
if(isset($_GET['iSortCol_0'])){
    $s = abs(intval($_GET['iSortCol_0']));
    if($s == 0) $order = "s.id";
    elseif($s == 1) $order = "s.nazev";
    elseif($s == 2) $order = "pocet_clanku";
    else $order = "s.id";
}


$order = "CONCAT(s.id_rodic,'-',s.id)";


if($order != '') 
    $order = "ORDER BY ".$order." ".(isset($_GET['sSortDir_0'])?$_GET['sSortDir_0']:'');

//echo $order;

//sestaveni podminky pro vyhledavani
$like = array();
if($vyraz != '')
{
    $like[] = "s.id LIKE '".$vyraz."%'";
    $like[] = "s.nazev LIKE '%".$vyraz."%'";
}
    


$where = "WHERE ".((count($like) > 0) ? " AND (".implode(" OR ",$like).")" : "1=1");

$data_pocet = $db->Query("SELECT COUNT(idStranky) AS celkem FROM ".TABLE_STRANKY." AS s ".$where);
$iTotal_arr = $db->getAssoc($data_pocet);
$iTotal = $iTotal_arr['celkem'];

        
$data = $db->Query("SELECT SQL_CALC_FOUND_ROWS s.*, COUNT(c.idStranky) AS pocet_clanku
				FROM ".TABLE_CISELNIK_MESTA." AS s
                LEFT JOIN ".TABLE_STRANKY." AS c ON c.mesto = s.id			
                ".$where."
				GROUP BY s.id
				".$order."
                ".$limit."
				");


$iFilteredTotal_data = $db->Query("SELECT FOUND_ROWS()");
$iFilteredTotal_arr = $db->getRow($iFilteredTotal_data);
$iFilteredTotal = intval($iFilteredTotal_arr[0]);

$dis_content = !$login_obj->UserPrivilege('content_view');
$dis_delete = !$login_obj->UserPrivilege('content_delete');

$tr = array();
while($c = $db->getAssoc($data))
{
    $ids = $c['id'];
    $pom = array();
    $pom[] = json_encode($ids);
    $pom[] = json_encode(($c['id_rodic'] == 0 ? '' : str_repeat("&nbsp;",6))."<a href='".get_link('eshop','mesta',$ids,'edit-content',0)."' >".secureString($c['nazev'])."</a>");
    $pom[] = json_encode(intval($c['pocet_clanku']));

    $ikony = "";
    
    if($dis_content)
        $ikony .= icon_disabled('content');
        else
        $ikony .= "<a href='".get_link('eshop','mesta',$ids,'edit-content',0)."'>".icon('content')."</a>";
        
    if($dis_delete)
        $ikony .= icon_disabled('delete');
    elseif($c['pocet_clanku'] > 0)
        $ikony .= icon_disabled('delete');
        else
        $ikony .= "<a href='#' id='delete".$ids."' class='delete'>".icon('delete')."</a>";
    
    $pom[] = json_encode($ikony);
        
    $tr[] = "[".implode(',', $pom)."]";
    
}

echo "{";
echo '"sEcho": '.(intval($_GET['sEcho'])).', ';
echo '"iTotalRecords": '.$iTotal.', ';
echo '"iTotalDisplayRecords": '.($iFilteredTotal).', ';
echo '"aaData": ['.implode(',', $tr).'] }';


?>