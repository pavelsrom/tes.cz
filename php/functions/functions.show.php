<?php


    function get_next_action_code()
    {
        return isset($_SESSION['dalsi_akce']) ? intval($_SESSION['dalsi_akce']) : 0;
    }
    
    function set_next_action_code()
    {
        $_SESSION['dalsi_akce'] = get_int_post("dalsi_akce");
    }
    
    


    function ano_ne($val = 0, $title_ano = "", $title_ne = "")
    {
        $title_ano = $title_ano != '' ? 'title="'.$title_ano.'"' : '';
        $title_ne = $title_ne != '' ? 'title="'.$title_ne.'"' : '';
        
        return $val == 0 ? "<span class='stav0 fa fa-circle' ".$title_ne." title='".TNE."'></span>":"<span class='stav1 fa fa-circle' ".$title_ano." title='".TANO."'></span>";
    }
    
    
    function tlacitko($link,$nazev = TVYTVORIT,$text_pred = TNENI_VYTVORENO, $ikona="plus",$class="",$id="")
    {
        
        $id = $id == "" ? "" : "id='".$id."'";
        
        return $text_pred." <a href='".$link."' title='".$nazev."' class='tlacitko ".$class."' ".$id."><span class='fa fa-".$ikona."'></span> ".$nazev."</a>";
    } 
	

	function icon($typ, $title="", $classes=''){
		
		$classes = "icon ".$classes;
			
		$path = RELATIVE_PATH.ICON_DIR."/";
		$title_nahradni = "";
		
        $ikony = array(
            'info'                  => ICON_INFO,
            'warning'               => ICON_WARNING,
            'plus'                  => ICON_PLUS,
            'minus'                 => ICON_MINUS,
            'plus_minus_disabled'   => ICON_PLUS_MINUS_DISABLED,
            'rescure'               => ICON_RESCURE,
            'gallery'               => array(ICON_GALLERY,TGALERIE),
            'statistics'            => array(ICON_STATISTICS,TSTATISTIKA),
            'save'                  => array(ICON_SAVE,TULOZIT),
            'delete'                => array(ICON_DELETE,TSMAZAT),
            'preview'               => array(ICON_LINK,TZOBRAZIT_FOTKU_NA_WEBU),
            'setting'               => array(ICON_SETTING,TNASTAVENI),
            'content'               => array(ICON_CONTENT,TEDITOVAT_OBSAH),
            'tree'                  => ICON_TREE,
            'add'                   => array(ICON_ADD,TPRIDAT),
            'download'              => array(ICON_DOWNLOAD,TSTAHNOUT),
            'gallery-folder'        => array(ICON_GALLERY_FOLDER,TSLOZKA_GALERIE),
            'gallery-photos'        => array(ICON_GALLERY_CONTENT,TNAHRANE_FOTKY),
            'verify'                => "",
            "send_email"            => "",
            'up'                    => array(ICON_UP,TPOSUNOUT_NAHORU),
            'down'                  => array(ICON_DOWN,TPOSUNOUT_DOLU),
            'user_time'             => ICON_USER_TIME,
            'ok'                    => ICON_OK,
            'error'                 => ICON_ERROR,
            'attachment'            => array(ICON_ATTACHMENT,TPRILOHA),
            'tip'                   => ICON_TIP,
            'help'                  => ICON_HELP,
            'req'                   => array(ICON_REQ,TPOVINNA_POLOZKA),
            'link'                  => array(ICON_LINK,TZOBRAZIT_STRANKU_NA_WEBU),
            'articles'              => array(ICON_ARTICLES,TCLANKY),
            'discussion'            => array(ICON_DISCUSSION,TZOBRAZIT_DISKUZNI_PRISPEVKY),
            'editbox'               => array(ICON_EDITBOX,TEDITBOX),
            'editbox-kolotoc'       => array(ICON_EDITBOX_KOLOTOC,TEDITBOX_KOLOTOC),
            "date"                  => ICON_DATE,
            'rescure'               => TNAHRADIT_DATABAZI_ZALOHOU,
            'req'                   => TPOVINNA_POLOZKA,
            'arrows'                   => TPRESUNOUT_NA_JINE_MISTO
            );
        
        $icon = isset($ikony[$typ]) ? (array) $ikony[$typ] : "";
        //if(!$icon) return "";

        $title_nahradni = isset($icon[1]) ? $icon[1] : "";
        $title = $title == "" ? $title_nahradni : $title;
        //$path = $path.$icon[0];
        
        
		//$img = "<img class='".$classes."' src='".$path."' title='".$title."' alt='".$title."' />";
        $fa = "";
        $fa = $typ == "delete"  ? "remove" : $fa;
        $fa = $typ == "content" ? "pencil" : $fa;
        $fa = $typ == "link"    ? "eye" : $fa;
        $fa = $typ == "articles"? "navicon" : $fa;
        $fa = $typ == "up"      ? "angle-up " : $fa;
        $fa = $typ == "down"    ? "angle-down " : $fa;
        $fa = $typ == "plus"    ? "plus-square " : $fa;
        $fa = $typ == "minus"   ? "minus-square " : $fa;
        $fa = $typ == "save"    ? "save " : $fa;
        $fa = $typ == "setting" ? "cog " : $fa;
        $fa = $typ == "info" ? "info " : $fa;
        $fa = $typ == "warning" ? "warning " : $fa;
        $fa = $typ == "user_time" ? "flash " : $fa;
        $fa = $typ == "ok" ? "check " : $fa;
        $fa = $typ == "error" ? "exclamation  " : $fa;
        $fa = $typ == "gallery-photos" ? "navicon" : $fa;
        $fa = $typ == "gallery-folder" ? "folder-o" : $fa;
        $fa = $typ == "gallery" ? "file-image-o" : $fa;
        $fa = $typ == "copy" ? "copy" : $fa;
        $fa = $typ == "editbox" ? "archive" : $fa;
        $fa = $typ == "editbox-kolotoc" ? "recycle"/*"retweet"*/: $fa;
        $fa = $typ == "discussion" ? "navicon"/*"retweet"*/: $fa;
        $fa = $typ == "date" ? "calendar": $fa;
        $fa = $typ == "download" ? "download": $fa;
        $fa = $typ == "rescure" ? "rotate-left": $fa;
        $fa = $typ == "req" ? "hand-o-left": $fa; //star
        $fa = $typ == "arrows" ? "arrows": $fa; //star
        
        $fa = $fa != "" ? $fa : "";
        
        
        $img = "<span class='".$classes." fa fa-".$fa." ' title='".$title."'></span>";
		return $img;
	}
	
	function icon_disabled($typ, $title='', $classes=''){
		
		$classes = "icon ".$classes;
			 
		$path = RELATIVE_PATH.ICON_DISABLED_DIR."/";
		$title_nahradni = "";
		
        $ikony = array(
            'info'                  => ICON_INFO,
            'warning'               => ICON_WARNING,
            'plus'                  => ICON_PLUS,
            'minus'                 => ICON_MINUS,
            'plus_minus_disabled'   => ICON_PLUS_MINUS_DISABLED,
            'rescure'               => ICON_RESCURE,
            'gallery'               => ICON_GALLERY,
            'statistics'            => array(ICON_STATISTICS,TNEMATE_OPRAVNENI_SLEDOVAT_STATISTIKY),
            'save'                  => array(ICON_SAVE,TNEMATE_OPRAVNENI_K_UKLADANI),
            'delete'                => array(ICON_DELETE,TNEMATE_OPRAVNENI_K_MAZANI),
            'preview'               => array(ICON_LINK),
            'setting'               => array(ICON_SETTING,TNEMATE_OPRAVNENI_MENIT_NASTAVENI),
            'content'               => array(ICON_CONTENT,TNEMATE_OPRAVNENI_MENIT_OBSAH),
            'tree'                  => ICON_TREE,
            'add'                   => array(ICON_ADD,TNEMATE_OPRAVNENI_VYTVARET),
            'download'              => ICON_DOWNLOAD,
            'gallery-folder'        => array(ICON_GALLERY_FOLDER,TNEMATE_OPRAVNENI_PRO_VSTUP_DO_GALERIE),
            'gallery-photos'        => "",
            'verify'                => "",
            "send_email"            => "",
            'up'                    => array(ICON_UP,TNELZE_POSOUVAT),
            'down'                  => array(ICON_DOWN,TNELZE_POSOUVAT),
            'ok'                    => ICON_OK,
            'error'                 => ICON_ERROR,
            'play'                  => ICON_PLAY,
            'stop'                  => ICON_STOP,
            'pause'                 => ICON_PAUSE,
            'attachment'            => ICON_ATTACHMENT,
            'tip'                   => ICON_TIP,
            'help'                  => ICON_HELP,
            'req'                   => ICON_REQ,
            'discussion'            => array(ICON_DISCUSSION,TNEMATE_OPRAVNENI_PRO_ZOBRAZENI_PRISPEVKU),
            "date"                  => ICON_DATE
            );
        
        
        $icon = isset($ikony[$typ]) ? (array) $ikony[$typ] : "";
        //if(!$icon) return "";

        $title_nahradni = isset($icon[1]) ? $icon[1] : "";
        $title = $title == "" ? $title_nahradni : $title;
        //$path = $path.$icon[0];
        
		//$img = "<img class='".$classes."' src='".$path."' title='".$title."' alt='".$title."' />";
        
        $fa = "";
        $fa = $typ == "delete"  ? "remove" : $fa;
        $fa = $typ == "content" ? "pencil" : $fa;
        $fa = $typ == "link"    ? "eye" : $fa;
        $fa = $typ == "articles"? "navicon" : $fa;
        $fa = $typ == "up"      ? "angle-up " : $fa;
        $fa = $typ == "down"    ? "angle-down " : $fa;
        $fa = $typ == "plus"    ? "plus-square " : $fa;
        $fa = $typ == "minus"   ? "minus-square " : $fa;
        $fa = $typ == "save"    ? "save " : $fa;
        $fa = $typ == "setting" ? "cog " : $fa;
        $fa = $typ == "gallery-photos" ? "navicon" : $fa;
        $fa = $typ == "gallery-folder" ? "folder-o" : $fa;
        $fa = $typ == "gallery" ? "file-image-o" : $fa;
        $fa = $typ == "copy" ? "copy" : $fa;
        $fa = $typ == "editbox" ? "archive" : $fa;
        $fa = $typ == "editbox-kolotoc" ? "recycle"/*"retweet"*/: $fa;
        $fa = $typ == "discussion" ? "navicon"/*"retweet"*/: $fa;
        $fa = $typ == "date" ? "calendar": $fa;
        
        
        
        $fa = $fa != "" ? $fa : "";
        
        
        $img = "<em><span class='".$classes." fa fa-".$fa." ' title='".$title."'></span></em>";
        
		return $img;
	}
	
    
function ShowH3($nadpis, $ikona='',$poznamka="",$class=""){
    $res = "";
    
    $img="";
    if($ikona=='home') $img = H3ICON_HOME;
    elseif($ikona=='email') $img = H3ICON_EMAIL;
    elseif($ikona=='seo') $img = H3ICON_SEO;
    elseif($ikona=='content') $img = H3ICON_CONTENT;
    elseif($ikona=='users') $img = H3ICON_USERS;
    elseif($ikona=='pass') $img = H3ICON_PASS;
    elseif($ikona=='stat') $img = H3ICON_STAT;
    elseif($ikona=='panel-setting') $img = H3ICON_PANEL_SETTING;
    elseif($ikona=='page-setting') $img = H3ICON_PAGE_SETTING;
    elseif($ikona=='date') $img = H3ICON_DATE;
    elseif($ikona=='receive') $img = H3ICON_RECEIVE;
    elseif($ikona=='photos') $img = H3ICON_PHOTOS;
    
    
    $img = "";
    
    $res = "<h3>".$nadpis."</h3>";
    return $res;
    
    
    
    
    if($img!='') 
        $img = "<img src='".RELATIVE_PATH.$img."' alt='' class='big-icon'/>";
    
    if($poznamka!='') 
        $poznamka = "<span class='popisek'>".$poznamka."</span>";
        
    $res .= "<div class='div-h3 ".$class."' >
        <table>
            <tr>
            <th>".$img."</th>
            <td><h3>".$nadpis."</h3>
            ".$poznamka."
            </td></tr>
        </table>
        </div>";
    return $res;
}


function main_tools($tlacitka){
    $res = "";
    if(count($tlacitka)==0) return "";
    $res .= "<div class='horni-tlacitka'>";    
    foreach($tlacitka AS $d){
        if(isset($d['aktivni']) && $d['aktivni']=='1') $a = "active"; else $a="";
        $ikona = $d['ikona'];
        $id_css = isset($d['id']) ? "id='".$d['id']."'" : ""; 
        if(!isset($d['title'])) $d['title'] = $d['nazev'];
        $res .= "<div class='tlacitko ".$ikona."' ".$id_css.">";
        
        $fa = "";
        $fa = $ikona == "back" ? "fa-reply" : $fa; 
        $fa = $ikona == "new" ? "fa-plus" : $fa;
        $fa = $ikona == "preview" ? "fa-eye" : $fa;
        $fa = $ikona == "copy" ? "fa-copy" : $fa;
        $fa = $ikona == "setting" ? "fa-cog" : $fa;
        $fa = $ikona == "content" ? "fa-pencil" : $fa;
        $fa = $ikona == "date" ? "fa-calendar" : $fa;
        $fa = $ikona == "import" ? "fa-download" : $fa;
        $fa = $ikona == "export" ? "fa-upload" : $fa;
        
        $fa = $fa != "" ? "<em class='fa ".$fa."'></em>&nbsp;&nbsp;" : "";
        
        if(isset($d['kod_pred']))
            $res .= $d['kod_pred'];
        
        if(isset($d['target']) && $d['target']=='blank')
        {
            if(strstr($d['odkaz'], PROTOKOL))
                $res .= "<a href='".$d['odkaz']."' target='_blank' class='".$a."' title='".$d['title']."'>".$fa.$d['nazev']."</a>";
                else
                $res .= "<a href='".$d['odkaz']."' target='_blank' class='".$a."' title='".$d['title']."'>".$fa.$d['nazev']."</a>";
        }
            
            else
            {
            if(strstr($d['odkaz'], PROTOKOL))
                $res .= "<a href='".$d['odkaz']."' target='_blank' class='".$a."' title='".$d['title']."'>".$fa.$d['nazev']."</a>";
                else
                $res .= "<a href='".$d['odkaz']."' class='".$a."' title='".$d['title']."'>".$fa.$d['nazev']."</a>";
            }
        
        
        if(isset($d['kod_po']))
            $res .= $d['kod_po'];
            
        
        $res .= "</div>";
        
        
        
        }
    $res .= "</div>";
    $res .= "<div class='cleaner'></div>";    
    return $res;
    
}

/*
function main_tools($tlacitka){
    $res = "";
    if(count($tlacitka)==0) return "";
    $res .= "<div class='horni-tlacitka'>";    
    foreach($tlacitka AS $d){
        if(isset($d['aktivni']) && $d['aktivni']=='1') $a = "active"; else $a="";
        $ikona = $d['ikona'];
        $id_css = isset($d['id']) ? "id='".$d['id']."'" : ""; 
        if(!isset($d['title'])) $d['title'] = $d['nazev'];
        $res .= "<div class='tlacitko ".$ikona."' ".$id_css.">";
        
        if(isset($d['kod_pred']))
            $res .= $d['kod_pred'];
        
        if(isset($d['target']) && $d['target']=='blank')
        {
            if(strstr($d['odkaz'], "http://"))
                $res .= "<a href='".$d['odkaz']."' target='_blank' class='".$a."' title='".$d['title']."'>".$d['nazev']."</a>";
                else
                $res .= "<a href='".$d['odkaz']."' target='_blank' class='".$a."' title='".$d['title']."'>".$d['nazev']."</a>";
        }
            
            else
            {
            if(strstr($d['odkaz'], "http://"))
                $res .= "<a href='".$d['odkaz']."' target='_blank' class='".$a."' title='".$d['title']."'>".$d['nazev']."</a>";
                else
                $res .= "<a href='".$d['odkaz']."' class='".$a."' title='".$d['title']."'>".$d['nazev']."</a>";
            }
        
        
        if(isset($d['kod_po']))
            $res .= $d['kod_po'];
            
        
        $res .= "</div>";
        
        
        
        }
    $res .= "</div>";
    $res .= "<div class='cleaner'></div>";    
    return $res;
    
}
*/

//zobrazi formular pro nastaveni modulu

function show_settings()
{
    global $modules;
    global $db;
    global $domain;
    global $login_obj;
    global $lang;
    
    $where = "";
    if(!$login_obj->UserPrivilege('superadmin'))
        $where = "AND n.admin=0"; 
    
    
    $data = $db->query("SELECT if(nd.hodnota IS NULL, n.hodnota,nd.hodnota) AS hodnota, n.typ, n.nazev, n.idNapovedy, n.idNastaveni
        FROM ".TABLE_NASTAVENI." AS n
        LEFT JOIN ".TABLE_NASTAVENI_DOMENY." AS nd ON n.idNastaveni=nd.idNastaveni AND nd.idDomeny=".$domain->getId()."
        WHERE n.aktivni=1
            AND n.idModulu = ".$modules->GetActiveModule()->get_id()."
            ".$where."
        GROUP BY n.idNastaveni
        ");
    
    $form = new Form();
    $form->add_section(TNASTAVENI,'home');
        
    while($nastaveni = $db->getAssoc($data))
    {
        $typ = $nastaveni['typ'];
        $hodnota = $nastaveni['hodnota'];
        $nazev = constant($nastaveni['nazev']);
        $napoveda = $nastaveni['idNapovedy'];
        $idNastaveni = $nastaveni['idNastaveni'];
        $name = "hodnota_".$idNastaveni;
        
        
        if($typ == 'input')
        {
            $form->add_text($nazev,$name,$hodnota,$napoveda);
        }
        elseif($typ == 'barva')
        {
            $form->add_text($nazev,$name,$hodnota,$napoveda,false,"color");
        }
        elseif($typ == 'editor')
        {
            $form->add_editor($nazev,$name,$hodnota,$napoveda);
        }
        elseif($typ == "textarea")
        {
            $form->add_textarea($nazev,$name,$hodnota,$napoveda);
        }
        elseif($typ == "radio")
        {
            $form->add_radiobuttons($nazev,$name,$hodnota,null,$napoveda);
        }
        elseif($typ == "ga")
        {
            $rc4 = new rc4crypt;
            $hodnota = unserialize($hodnota);
            $hodnota["GA_HESLO"] = $rc4->endecrypt($domain->get('hash'),$hodnota["GA_HESLO"],'de');
            
            $form->add_section(TPRISTUP_KE_GOOGLE_ANALYTICS,'ga');
            $form->add_text(TEMAIL,'gaEmail',$hodnota["GA_EMAIL"],131,false,'','gaEmail');
            $form->add_password(THESLO,'gaHeslo',$hodnota["GA_HESLO"],132,false,'','gaHeslo');
            $form->add_text(TID_PROFILU,'gaProfil',$hodnota["GA_PROFIL"],133,false,'','gaProfil');
            $form->add_plain_text("","<a href='#' id='overit_dostupnost'>".TOVERIT_DOSTUPNOST."</a>");	
        }
        elseif($typ == "smajlici")
        {
            $smiles = array();
            $data_smajlici = $db->Query("SELECT idTematu, nazev FROM ".TABLE_SMAJLICI." WHERE zobrazit=1");
            while($smile = $db->getAssoc($data_smajlici))
                $smiles[$smile['idTematu']] = $smile['nazev'];
                
            $form->add_selectbox($nazev,$name,$hodnota,array('' => "-- ".TVYBERTE." --") + $smiles,0,false,"","smajlici_select");
        
            $form->add_plain_text("","<div id='smajlici'></div>");
        }
        elseif($typ == "strom")
        {
            $hodnota = unserialize($hodnota);
            $typh = isset($hodnota['typ']) ? $hodnota['typ'] : "";
            $l = $lang->get_langs();
        
            $nazev_selectboxu = $nazev;
            if($typh == 'tagy')
                $nazev_selectboxu = TSTRANKA_STITKY;
            elseif($typh == 'kalendar-akci')
                $nazev_selectboxu = TSTRANKA_KALENDAR_AKCI;
            elseif($typh == 'novinky')
                $nazev_selectboxu = TSTRANKA_NOVINKY;
            elseif($typh == 'stranka-galerie')
                $nazev_selectboxu = TSTRANKA_GALERIE;
            elseif($typh == 'vyhledavani')
                $nazev_selectboxu = TSTRANKA_VYHLEDAVANI;
            elseif($typh == 'sitemap')
                $nazev_selectboxu = TSTRANKA_SITEMAP;
            
            foreach($l AS $jazyk)
            {
        
                $stranky = array(0 => "-- ".TVYBERTE." --") + get_pages(0, $jazyk['idJazyka']);
                $h = isset($hodnota[$jazyk['idJazyka']]) ? intval($hodnota[$jazyk['idJazyka']]) : 0;
                $form->add_selectbox($nazev_selectboxu." ".strtoupper($jazyk['jazyk']),$name."[".$jazyk['idJazyka']."]",$h,$stranky);
            }
            
            $form->add_hidden('typ_'.$idNastaveni,$typh);
                    
        }
        elseif($typ == "textarea-lang" || $typ == 'editor-lang' || $typ == 'input-lang')
        {
            $hodnota = unserialize($hodnota);
            $typh = isset($hodnota['typ']) ? $hodnota['typ'] : "";
            $l = $lang->get_langs();
            $j = 0;
            foreach($l AS $jazyk)
            {
                $napoveda = $j == 0 ? $napoveda : 0;
                $h = isset($hodnota[$jazyk['idJazyka']]) ? $hodnota[$jazyk['idJazyka']] : "";
                                
                if($typ == 'editor-lang')
                    $form->add_editor($nazev." ".strtoupper($jazyk['jazyk']),$name."[".$jazyk['idJazyka']."]",$h,$napoveda);
                elseif($typ == 'textarea-lang')
                    $form->add_textarea($nazev." ".strtoupper($jazyk['jazyk']),$name."[".$jazyk['idJazyka']."]",$h,$napoveda);
                    else
                    $form->add_text($nazev." ".strtoupper($jazyk['jazyk']),$name."[".$jazyk['idJazyka']."]",$h,$napoveda);
                   
               $j++; 
            }
            
            $form->add_hidden('typ_'.$idNastaveni,$typh);
                    
        }
    }
    
        $res = $login_obj->UserPrivilege('settings_edit');
        $form->add_submit()->set_disabled(!$res);

        echo $form->get_html_code();
    
}

//univerzalni procedura na ulozeni nastaveni modulu
function save_settings($idModulu)
{
    global $db;
    global $domain;
    global $login_obj;
    global $links;
    
    $where = "";
    if(!$login_obj->UserPrivilege('superadmin'))
        $where = "AND n.admin=0"; 
    
    $data = $db->query("SELECT n.idNastaveni, n.typ, nd.idRelace 
        FROM ".TABLE_NASTAVENI." AS n 
        LEFT JOIN ".TABLE_NASTAVENI_DOMENY." AS nd ON n.idNastaveni=nd.idNastaveni AND nd.idDomeny = ".$domain->getId()."
        WHERE n.aktivni=1
            AND n.idModulu = ".$idModulu."
            ".$where."
        GROUP BY n.idNastaveni
        ");
    
    while($nastaveni = $db->getAssoc($data))
    {

        $idRelace = intval($nastaveni['idRelace']);
        $idNastaveni = $nastaveni['idNastaveni'];
        $typ = $nastaveni['typ'];
        $post_name = "hodnota_".$idNastaveni;

        
        
        
        if(!is_post($post_name) && $typ!='ga') continue;
        //echo $typ." ".$idNastaveni."<br>";
        
        if($typ == 'ga')
        {
            $hash = $domain->get('hash');
            $h = array(
                "GA_PROFIL" => get_post('gaProfil'),
                "GA_HESLO"  => get_post('gaHeslo'),
                "GA_EMAIL"  => get_post('gaEmail')
                );
    
            $rc4 = new rc4crypt;
            $h['GA_HESLO'] = $rc4->endecrypt($hash,$h['GA_HESLO']);
            
            $hodnota = serialize($h);
        }
        elseif($typ == "strom")
        {
            $hodnoty = get_array_post($post_name);
            $typh = get_post('typ_'.$idNastaveni);           

        
            foreach($hodnoty AS $idj => $h)
            {
                $id_rodic_stary = $db->get(TABLE_STRANKY,"idStranky","idDomeny=".$domain->getId()." AND typ='".$typh."' AND idJazyka=".$idj);
                $id_rodic_stary = intval($id_rodic_stary);
                $id_rodic = intval($h);
                $update = array("idRodice" => $id_rodic);
                $where = " idDomeny=".$domain->getId()." AND idRodice=".$id_rodic_stary." AND idJazyka=".$idj." AND typ!='clanky'";
                $where1 = "idDomeny=".$domain->getId()." AND idJazyka=".$idj." AND typ='".$typh."'";
                $where2 = "idDomeny=".$domain->getId()." AND idJazyka=".$idj." AND idStranky=".$id_rodic;
                
                if(in_array($typh, array("sitemap",'vyhledavani','objednavka','obchodni-podminky','email','helpdesk')))
                {
                    
                }
                else
                {
                    
                    if($typh == 'stranka-galerie')
                        $where .= " AND typ IN ('galerie','galerie-slozka')";
                    elseif($typh == 'kalendar-akci')
                        $where .= " AND typ IN ('kalendar-typ')";
                    elseif($typh == 'novinky')
                        $where .= " AND typ = 'novinka'";
                    elseif($typh == 'tagy')
                        $where .= " AND typ = 'tag'";   
                    else
                        continue;
                             
                    $db->update(TABLE_STRANKY,$update, $where); 
                }
                
                $db->update(TABLE_STRANKY,array('typ' => 'stranka'),$where1);
                $db->update(TABLE_STRANKY,array('typ' => $typh),$where2);
                
            }
            
            $hodnota = array();
            $hodnoty['typ'] = $typh;
            $hodnota = serialize($hodnoty);
            
        }
        elseif($typ == 'textarea-lang' || $typ == 'editor-lang' || $typ == 'input-lang')
        {
            $hodnoty = get_array_post($post_name);
            //$typh = get_post('typ_'.$idNastaveni);
            //$hodnoty['typ'] = $typh;  
            $hodnota = serialize($hodnoty);
            
            
            
        }
        else
        $hodnota = get_post($post_name);
        
        
        if($idRelace > 0)
            $db->update(TABLE_NASTAVENI_DOMENY,array("hodnota" => $hodnota),"idRelace=".$idRelace);
            else
            $db->insert(TABLE_NASTAVENI_DOMENY,array("hodnota" => $hodnota,"idNastaveni" => $idNastaveni,"idDomeny" => $domain->getId()));
        
        
    }

    //exit;

}


//zobrazi nastaveni stitku u clanku, popr. dalsich stranek nebo objektu
function show_tags_settings($id_stranky = 0, $typ_objektu='stranka', $jazyk = 'cz', & $form = null)
{
    
    //global $form;



    $tagy = new C_Tags($id_stranky, $typ_objektu, $id_stranky > 0, true);

    

    $form->add_text(TTAGY,'tagy',"",387,false,'text',"tagy");
        
        
        
    $tagy_prefill = $tagy->get_tags();
    
    
    //trideni podle nazvu
    $nazvy = array();
    foreach($tagy_prefill AS $idt => $nazev)    
        $nazvy[$idt] = $nazev;
        
    array_multisort($tagy_prefill,SORT_ASC,$nazvy);
    //print_r($tagy_prefill);
    
        
    $tagy_prefill_out = array();
    foreach($tagy_prefill AS $idt => $t)
    {
        $id_tag = intval($idt);
        if($idt<=0) 
            continue;
                    
        $tagy_prefill_out[] = '{value: "'.$id_tag.'", name: "'.$t->nazev.'"}';
    }
      
        
?>
        
        <script type="text/javascript">
        <!--
        
        jQuery(function($){      
            
            $("#tagy").attr("placeholder","<?php echo TNOVY_TAG;?>");
            var data = {items: [<?php echo implode(",",$tagy_prefill_out);?>]};
            
            $("#tagy").autoSuggest(
                "<?php echo AJAX_GATEWAY;?>tags.autosuggest", 
                {
                    asHtmlID: "tagy",
                    selectedItemProp: "name", 
                    searchObjProps: "name",
                    startText: "<?php echo TNAPISTE_NAZEV_TAGU;?>",
                    emptyText: "<?php echo TZADNY_TAG_NENALEZEN;?>",
                    preFill: data.items
                }
            );
                       
            
        })
        
        // -->
        </script>

    
  <?php  
    
    
    
}

function show_links($idStranky = 0, & $form = null)
{
    global $db;
    global $domain;
    
    if($idStranky == 0)
        return false;
    
    $data = $db->query("SELECT id, url, popis FROM ".TABLE_STRANKY_ODKAZY." WHERE idStranky=".$idStranky." ORDER BY priorita, id");
    
    $form->add_section(TODKAZY_POD_STRANKOU, 'links');
    //$form->add_text(TODKAZY_POD_STRANKOU, 'odkaz', false, 0,"","odkazy")->set_multiple();
  
    $code = "";
    $code .= '<div id="page-links">';

    $table = new Table("table-links", "table table-inner");


    $table->tr_head()
        ->add(TURL_ODKAZ)
        ->add(TPOPIS)
        ->add();

    $table->tr("nodisplay", "link-pattern")
        ->add('<input type="text" name="file_url[]" value="" class="input w100" />'.req(), 'tright')
        ->add('<textarea name="file_desc[]" rows="" cols=""  class="w100"></textarea>', 'tright')
        ->add("<a href='#' class='url-delete'>".icon('delete')."</a>", 'akce');

    if(is_post("file_url"))
    {
        $urls = get_array_post("file_url");
        $desc = get_array_post("file_desc");

        //print_r($urls);
        //dd($urls);

        foreach($urls AS $i => $url)
        {

            if($url == '' && $desc[$i] == '')
            continue; 

            $table->tr("sortable_checkbox")
                ->add('<input type="text" name="file_url[]" value="'.ss($url).'" class="input w100" />'.req(), 'tright')
                ->add('<textarea name="file_desc[]" rows="" cols="" class="w100">'.ss($desc[$i]).'</textarea>', 'tright')
                ->add("<a href='#' class='url-delete'>".icon('delete')."</a>", 'akce');

        }


    }
    else
    {
        $data = $db->query("SELECT id, url, popis FROM ".TABLE_STRANKY_ODKAZY." WHERE idStranky=".$idStranky." ORDER BY priorita, id");
        
        while($o = $db->getObject($data))
        {
            $table->tr("sortable_checkbox")
                ->add('<input type="text" name="file_url[]"  value="'.ss($o->url).'"  class="input w100"/>'.req(), 'tright')
                ->add('<textarea name="file_desc[]" rows="" cols="" class="w100">'.ss($o->popis).'</textarea>', 'tright')
                ->add("<a href='#' class='url-delete'>".icon('delete')."</a>", 'akce');

        }
    }


    $table->tr()->add()->add()->add(tlacitko("#", TPRIDAT, "", "plus", "", "pridat-url"));

    $code .= $table->get_html();
    
    $code .= '</div>';
    
   
    $form->add_plain_text(TODKAZY_POD_STRANKOU_PRIDANE, $code);
    
  
    ?>
    
    <script type="text/javascript">
    
    $(function(){
                

        $("#pridat-url").click(function(){
            html = $("#link-pattern").html();
            console.log(html);
            $("#table-links tr:last").before('<tr class="sortable_checkbox">' + html + '</tr>');
            return false;
        })

        $("#page-links").on('click', "a.url-delete", function(){
            $(this).parents(".sortable_checkbox").first().remove();
            return false;
        })


        pocet_odkazu = $(".sortable_checkbox").length;
        if(pocet_odkazu == 0)
            $("#pridat-url").click();


        $("#page-links").sortable({
			opacity: 0.8, 
			cursor: 'move',
            items: '.sortable_checkbox',
			placeholder: 'placeholder-uploaded-photos'

        });
        /*
        $(document).on("click","a.url-delete", function(){
            var id = $(this).attr('id').substring(1);
            var text = "<?php echo TOPRAVDU_SMAZAT;?>";
            var parent = $(this).parents(".sortable_checkbox").first();
            
            parent.addClass("deleted");
            
            custom_confirm(text, function(){
                $.msg();
                $.post("<?= AJAX_GATEWAY ?>gallery.url.delete",{id: id, page_id: <?= $idStranky ?>},function(data){
                    $.msg( 'setClass', 'ok_message' );
                    $.msg( 'replace', '<?php echo OK_SMAZANO;?>' );
                    $.msg( 'unblock', 1000);
                    
                    parent.fadeOut(500, function(){
                        parent.remove();
                    })
                    
    
                    });
            
            })
            
            return false;
            
        })
        */
       
        
        
        
    })
    
    
    </script>
    
    
    <?php
    
}



function show_photos($idStranky = 0, & $form = null)
{
    global $db;
    global $domain;
    
    if($idStranky == 0)
        return false;
    
    $data = $db->query("SELECT id, soubor, popis FROM ".TABLE_STRANKY_FOTKY." WHERE idStranky=".$idStranky." ORDER BY priorita, id");
    
    $form->add_section(TFOTKY_POD_STRANKOU, 'photos');
    $form->add_file(TNAHRAT_OBRAZEK, 'fotky', false, 85,"","fotky")->set_multiple();
  
    $code = '
                
                <div class="graph_box">
                    <div class="graph_kontrola" style="width: 0%;"></div>
                    <div class="graph_text"><span class="proc">0</span>%</div>
                </div>
                <div class="protokol"></div>
                
            ';
            
    $form->add_plain_text(TPRUBEH_NAHRAVANI,$code, 0,"","progress")->set_class_tr('progress');
    
    $code = '<div id="uploaded_photos">';
    while($o = $db->getObject($data))
    {
        
        $code .= '<div class="sortable_checkbox"><table>
            <tr>
                <th>
                    <img src="'.RELATIVE_URL_USER_DOMAIN.USER_DIRNAME_STRANKY_MINI.$o->soubor.'" alt="">
                    
                </th>
                <td>
                    <textarea rows="" cols="" name="fotka['.$o->id.']" placeholder="'.TPOPIS.'">'.$o->popis.'</textarea>
                    '.tlacitko('#',TSMAZAT,"<br /><br />","times","photo-delete","p".$o->id).'
                </td>
            </tr>
            </table>
            </div>
            ';
    }
    
    
    $code .= '</div>';
    
    $form->add_plain_text(TNAHRANE_OBRAZKY,$code);
    
    
    
  
    ?>
    
    <script type="text/javascript">
    
    $(function(){
        
        $(".progress").hide();
        
        $("#uploaded_photos").sortable({
			opacity: 0.8, 
			cursor: 'move',
			placeholder: 'placeholder-uploaded-photos'

        });
        
        $(document).on("click","a.photo-delete", function(){
            var id = $(this).attr('id').substring(1);
            var text = "<?php echo TOPRAVDU_SMAZAT_TENTO_OBRAZEK;?>";
            var parent = $(this).parents(".sortable_checkbox").first();
            
            parent.addClass("deleted");
            
            custom_confirm(text, function(){
                $.msg();
                $.post("<?php echo AJAX_GATEWAY;?>gallery.image.delete",{id: id, page_id: <?php echo $idStranky;?>},function(data){
                    $.msg( 'setClass', 'ok_message' );
                    $.msg( 'replace', '<?php echo OK_SMAZANO;?>' );
                    $.msg( 'unblock', 1000);
                    
                    parent.fadeOut(500, function(){
                        parent.remove();
                    })
                    
    
                    });
            
            })
            
            return false;
            
        })
        
        
        $(document).on("change","form input[name=fotky]", function(){
        
        
        
        //inicializace progressbaru
        var progressbar = $( "#ine_progressbar" );
        var progressLabel = $( ".graph_box span.proc" );
        var graf = $(".graph_kontrola");
        
        progressLabel.text("0");
        graf.css("width","0%");
        
        //progressbar.fadeIn();
        
        $(".progress").show();
        $(".protokol").html("");

        var o = $(this);
        
        if(o.length == 0)
            return;
                
        var soubory = o[0].files; 
        var pocet_souboru = soubory.length;
        
        var prog = Math.round((100 / pocet_souboru) + 0.5); 
        var pocet_uploadu = 0;
               
        $.each(soubory, function(i, file){
            formData = new FormData();
            formData.append("page_id", <?php echo $idStranky;?>);
            formData.append("fotka", file);
            
            
            $.ajax({
                url: "<?php echo AJAX_GATEWAY;?>product.photos.upload",
                type: 'POST',
                async: false,
                xhr: function() {
                    myXhr = $.ajaxSettings.xhr();
                    if(myXhr.upload){
                        myXhr.upload.addEventListener('progress',function(prog){
                            //var value = Math.round((~~((prog.loaded / prog.total) * 100)) / pocet_souboru);
                            
                        }, false);
                    }
                    return myXhr;
                },
                data: formData,
                cache: false,
                dataType: 'json',
                contentType: false,
                processData: false,
                complete: function(){
                    var val = parseInt(progressLabel.text());
                    val += val + (100 / pocet_souboru);
                    val = Math.round(val);
                    if(val > 100)
                        val = 100;
                                        
                    progressLabel.text(val);
                    graf.css("width",val + "%");
                    pocet_uploadu++;
                    
                    if(pocet_uploadu == pocet_souboru)
                    {

                    }
                    
                    
                },
                success : function(res) {
                    if(res.file.html)
                    {
                        $("#uploaded_photos").append(res.file.html);
                    }
                    
                    m = res.messages.error && res.messages.error != "" ? res.messages.error : res.messages.ok;
                    if(!m)
                        m = "---";
                        
                    $(".protokol").prepend(m + '<br />');
                    
                    
                    //aktivuj_zpravu(res.messages);
                    //if(successFn) successFn(res);
                },
                error: function(jqXHR, textStatus, errorThrown)
                {
                    alert(textStatus + ": " + errorThrown);
                    // Handle errors here
                    //console.log('ERRORS: ' + textStatus);
                    // STOP LOADING SPINNER
                }
            });
        });
        
        var input = $("#fotky");    
        input.replaceWith(input.val('').clone(true));
        
        
        
        
    })
        
        
        
    })
    
    
    </script>
    
    
    <?php
    
}


function show_private_pages_settings($idStranky = 0 , $page = null, & $form = null)
{
    global $db;
    global $domain;
    
    $default_values = array();
    $values = array();
    $data = $db->Query("SELECT s.idSkupiny,s.nazev, IF(ss.idSkupiny IS NULL,0,1) AS checked 
                FROM ".TABLE_UZIVATELE_SKUPINY." AS s
                LEFT JOIN ".TABLE_STRANKY_SKUPINY." ss ON s.idSkupiny = ss.idSkupiny AND ss.idStranky=".$idStranky."
                WHERE s.idDomeny=".$domain->getId()."
                GROUP BY s.idSkupiny
                ORDER BY s.idSkupiny
                ");

    while($c = $db->getAssoc($data))
    {
        if($c['checked']==1 && $idStranky > 0)
            $default_values[] = $c['idSkupiny'];
                
            
        //$values[$c['idStranky']] = $c['nazev'];
        $values[$c['idSkupiny']] = $c['nazev'];
    }

    
    
    
    if(MODULE_PRIVATE_PAGES && (!isset($page['typ']) || $page['typ'] != 'login'))
    {
        
        $private_pages = new C_PrivatePages($idStranky);
        $disabled = $private_pages->get_inherited_groups();
        $ma_zdedenou_privatnost = $private_pages->is_inherited_private();

        $form->add_section(TPRISTUP_KE_STRANKAM, 'users',help(18,true));
        
        if($ma_zdedenou_privatnost)
            $form->add_plain_text("",TSTRANKA_MA_ZDEDENOU_PRIVATNOST);

        $aut = intval($page['autentizace'] || $ma_zdedenou_privatnost);

        $form->add_radiobuttons(TPRIVATNI, 'autentizace', $aut, array(1 => TANO, 0=>TNE))->set_disabled($ma_zdedenou_privatnost);
        
        
        if($ma_zdedenou_privatnost)
            $form->add_hidden('autentizace', $page['autentizace']);
          
        $default_values = array_merge($default_values, $disabled);
        
           
        if($db->numRows($data)==0)
            $form->add_plain_text("", TNENALEZEN_ZADNY_ZAZNAM);
            else
            $form->add_checkboxes(TSKUPINY, 'skupiny', $default_values, $values, 0,false,"","",true)->set_disabled_values($disabled)->set_class_tr("tr_privatni");
    
        
    
        ?>
        <script type="text/javascript">
        
        $(function(){
            
            $('input[name=autentizace]').on('ifChecked', function(){
                    
                prepni_privatni();
            });
            
            function prepni_privatni()
            {
                v = $("input[name='autentizace']:checked").val();
                if(v == 1)
                    $('.tr_privatni').show();
                    else
                    $('.tr_privatni').hide();
            }
            
            $("input[name='autentizace']").click(function(){
                v = $(this).val();
                if(v == 1)
                    $('.tr_privatni').fadeIn();
                    else
                    $('.tr_privatni').fadeOut();
            });
            
            prepni_privatni();
            
            
        })
        
        </script>
    
        <?php
    
    
        
    }
    else
    {
        $form->add_hidden('autentizace', $page['autentizace']);
        
        foreach($default_values AS $v)
            $form->add_hidden('skupiny[]', $v);
    }
    

    
}


function nahravani_hlavniho_obrazku($id_stranky = 0, & $form = null, $zobrazit_znamku = false)
{
    global $db;
    global $domain;
    global $login_obj;
    global $object_access;
    
    $o = $db->get(TABLE_STRANKY,array("hlavni_obrazek",'obrazek2','znamka'),"idStranky=".$id_stranky);
    
    $znamka = $o ? $o->znamka : 0;
    $hlavni_obrazek = $o ? $o->hlavni_obrazek : "";
    $hlavni_obrazek2 = $o ? $o->obrazek2 : "";
    
    $form->add_section(THLAVNI_OBRAZEK_STRANKY, 'users');
        
    $form->add_file(TNAHRAT_OBRAZEK, 'hlavni_obrazek', false, 85,"","obrazek_hlavni_input")->set_disabled($hlavni_obrazek!='');
    
        
$image_class = "";
if($hlavni_obrazek=='' || $hlavni_obrazek==null){
    $img = TNENI_ULOZEN_ZADNY_OBRAZEK;
    }
    else
    {	
    $file = $domain->getDir().USER_DIRNAME_HLAVNI_OBRAZEK.$hlavni_obrazek;
				
    if(file_exists($file)) {
        $src = RELATIVE_PATH.$file."?".time(); 
        $img = "<img src='".$src."' alt='".$hlavni_obrazek."' title='".$hlavni_obrazek."' style='max-width: 600px;max-height: 200px'/><br /><br />";
        $image_class = "image";
        if($login_obj->UserPrivilege('content_delete') && $object_access->has_access($id_stranky))
            $img .= tlacitko('#',TSMAZAT,"","times","hlavni-delete photo-delete","delete".$id_stranky);
            else
            $img .= icon_disabled('delete');
        }
        else 
        {
        $src="";
        $img = TOBRAZEK_NENALEZEN;
        if($login_obj->UserPrivilege('content_delete'))
            $img .= tlacitko('#',TSMAZAT,"","times","hlavni-delete photo-delete","delete".$id_stranky);
            else
            $img .= icon_disabled('delete');
  
            }
        }

        $form->add_plain_text(TOBRAZEK, $img)->set_class_td($image_class." relative tleft");
        $form->add_hidden('hlavni_obrazek',$hlavni_obrazek);


$image_class = "";
if($hlavni_obrazek2=='' || $hlavni_obrazek2==null){
    $img = TNENI_ULOZEN_ZADNY_OBRAZEK;
    }
    else
    {	
    $file = $domain->getDir().USER_DIRNAME_HLAVNI_OBRAZEK2.$hlavni_obrazek2;
				
    if(file_exists($file)) {
        $src = RELATIVE_PATH.$file."?".time(); 
        $img = "<img src='".$src."' alt='".$hlavni_obrazek2."' title='".$hlavni_obrazek2."' style='max-width: 600px;max-height: 200px'/><br /><br />";
        $image_class = "image";
        if($login_obj->UserPrivilege('content_delete') && $object_access->has_access($id_stranky))
            $img .= tlacitko('#',TSMAZAT,"","times","hlavni-delete2 photo-delete","delete".$id_stranky);
            else
            $img .= icon_disabled('delete');
        }
        else 
        {
        $src="";
        $img = TOBRAZEK_NENALEZEN;
        if($login_obj->UserPrivilege('content_delete'))
            $img .= tlacitko('#',TSMAZAT,"","times","hlavni-delete2 photo-delete","delete".$id_stranky);
            else
            $img .= icon_disabled('delete');
  
            }
        }

        $form->add_file(TNAHRAT_OBRAZEK." 2", 'hlavni_obrazek2', false, 85,"","obrazek_hlavni_input2")->set_disabled($hlavni_obrazek2!='');
  
        $form->add_plain_text(TOBRAZEK." 2 - ".TOBRAZEK2_POPIS, $img)->set_class_td($image_class." relative tleft");
        $form->add_hidden('hlavni_obrazek2',$hlavni_obrazek2);
  
        
        
        
        /*
        if($zobrazit_znamku)
        {
            $znamky = array(
                0 => TZADNA,
                1 => "Instant sauce - orange",
                2 => "Instant sauce - blue",
                3 => "Instant sauce - pink",
                4 => "No grain formula - blue",
                5 => "No grain formular - yellow",
                6 => "No grain formular - ocean", 
                7 => "No grain formular - pink",
                8 => "No grain formular - orange",
                9 => "No grain formular - brown"
                );
            $form->add_selectbox(TZNAMKA_CAROUSEL, 'znamka', $znamka, $znamky);
        }
        */

        ?>
        <script type="text/javascript">
        
        $(function(){
            
            $("a.hlavni-delete").click(function(){
                var id = $(this).attr('id').substring(6);
                
                var text = "<?php echo TOPRAVDU_SMAZAT_TENTO_OBRAZEK;?>";
                var td = $(this).parents("td.image").first();
                
                custom_confirm(text, function(){
                    $.msg();
                    td.addClass("deleted");
                    $.post("<?php echo AJAX_GATEWAY ;?>main.image.delete",{id: id},function(data){
                        $.msg( 'setClass', 'ok_message' );
                        $.msg( 'replace', '<?php echo OK_SMAZANO;?>' );
                        $.msg( 'unblock', 1000);
                        td.html("<?php echo TNENI_ULOZEN_ZADNY_OBRAZEK;?>");
                        $("#obrazek_hlavni_input").removeAttr('disabled');
                        td.removeAttr("class");
                        });
                    
                    })
                    
                return false;
                
            })
            
            $("a.hlavni-delete2").click(function(){
                var id = $(this).attr('id').substring(6);
                
                var text = "<?php echo TOPRAVDU_SMAZAT_TENTO_OBRAZEK;?>";
                var td = $(this).parents("td.image").first();
                
                custom_confirm(text, function(){
                    $.msg();
                    td.addClass("deleted");
                    $.post("<?php echo AJAX_GATEWAY ;?>main.image2.delete",{id: id},function(data){
                        $.msg( 'setClass', 'ok_message' );
                        $.msg( 'replace', '<?php echo OK_SMAZANO;?>' );
                        $.msg( 'unblock', 1000);
                        td.html("<?php echo TNENI_ULOZEN_ZADNY_OBRAZEK;?>");
                        $("#obrazek_hlavni_input2").removeAttr('disabled');
                        td.removeAttr("class");
                        });
                    
                    })
                    
                return false;
                
            })
            
            
        })
        
        </script>
    
        <?php


    
}







 

    
  
?>