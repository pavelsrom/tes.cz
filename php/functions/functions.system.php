<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */


//provede presmerovani stranky	
function Redirect($page="", $code=301){
	
    if($page == "" || (isset($page[0]) && $page[0] == '#'))
    {
        
        $page = (isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : "").(isset($page[0]) && $page[0] == "#" ? $page : "");
    }
    
    
	if($code==301){
		header("HTTP/1.1 301 Moved Permanently");
		}
	if($code==404){
		header("HTTP/1.1 404 Not Found");
		header("Connection: close");
		return;
	   }
    if($code==503){
        header("HTTP/1.1 503 Service unavailable");
		header("Connection: close");
		return;
        }
        
	header("Location: ".$page);
	header("Connection: close");
	exit();
	}
	
function strip_selected_tags($str, $tags = "", $stripContent = false)
{
   
    preg_match_all("/<([^>]+)>/i", $tags, $allTags, PREG_PATTERN_ORDER);
    foreach ($allTags[1] as $tag) {
        $replace = "%(<$tag.*?>)(.*?)(<\/$tag.*?>)%is";
        $replace2 = "%(<$tag.*?>)%is";

        if ($stripContent) {
            $str = preg_replace($replace,'',$str);
            $str = preg_replace($replace2,'',$str);
        }
            $str = preg_replace($replace,'${2}',$str);
            $str = preg_replace($replace2,'${2}',$str);
    }
    return $str;
} 
    
//odesle email
function SendMail($from, $fromName, $to, $subject, $text, $altBody='', $attach=null, $inlineImage=false){
	include_once (PRE_PATH.'php/classes/class.phpmailer.php');
    include_once (PRE_PATH.'php/classes/class.smtp.php');
	
 	$mail = new PHPMailer();
 	//$mail->SMTPDebug = 3;
    $smtp_login = constant("SMTP_LOGIN");
    $smtp_heslo = constant("SMTP_HESLO");
    
 	if(SMTP_AKTIVNI && SMTP_HOST != ''){
 		$mail->IsSMTP(); 
        $mail->SMTPAuth = true;
        $mail->Host = SMTP_HOST;
        $mail->Port = SMTP_PORT;
        $mail->Username = $smtp_login;
        $mail->Password = $smtp_heslo;
		}
	
	if($attach!=null && count($attach)>0){
		foreach($attach AS $a)
			$mail->AddAttachment($a["file"], $a["name"]);
	}

    $mail->AddReplyTo($from, $fromName);
    $mail->SetFrom($smtp_login == '' ? $from : $smtp_login, $fromName);
    
	$mail->AddAddress($to);
	$mail->Subject = $subject;  
 	$mail->IsHTML(true);
	$mail->CharSet = "UTF-8"; 
 
    if($altBody!=''){
        $altBody = strip_selected_tags($altBody, '<title>',true);
        $mail->AltBody = strip_tags(br2nl(p2nl($altBody)));
        }
        else
        $mail->AltBody = strip_tags(br2nl(p2nl($text)));
        
    
    $mail->Body = $text;
    if($inlineImage){
        $mail->AllImagesInlineImages('',PRE_PATH);
        
        }
    
        
	
 	return $mail->Send();
}

	//zjisti zda se pracuje na localhostu, v tomto pripade vraci false; v opacnem pripade vraci true.
	function IsOnline(){
		return !stristr($_SERVER['SERVER_NAME'], "localhost") && !stristr($_SERVER['SERVER_NAME'], ".local") && strstr($_SERVER['SERVER_NAME'], ".");
	}

	//upravi retezec url adresy do prijatelneho tvaru
	function ModifyUrl($url){
		mb_internal_encoding("UTF-8");
		$castOdkazu = trim($url);
		$upravenyOdkaz=str_replace(" ","-",$castOdkazu);
		$poleZnaku = array("ą","Ą","Ä","ć","Ć","ę","Ę","ł","Ł","Ľ","ń","Ń","ś","Ś","ż","Ż","ź","Ź","ü","ű","ö","ő","ä","ě","š","č","ř","ŕ","ž","ý","ĺ","ľ","á","í","é","ú","ů","ť","ď","ň","ó","ô","Ě","Š","Č","Ř","Ž","Ý","Á","Í","É","Ü","Ú","Ů","Ď","Ť","Ň","Ó","Ö","ß", " ","+",",",":",".","&","\\","/","(",")","'","%","–", "*", "~", "`", "@","#", "^", "=", "{", "}", ";", "|", ">", "<","_","!","?");
     	$poleZnakuNahradnich = array("a","A","A","c","C","e","E","l","L","L","n","N","s","S","z","Z","z","Z","u","u","o","o","a","e","s","c","r","r","z","y","l","l","a","i","e","u","u","t","d","n","o","o","E","S","C","R","Z","Y","A","I","E","U","U","U","D","T","N","O","O","ss","-","-","-","-","-","-","", "" ,"" , "", "", "","-", "-", "-", "", "-", "-", "", "-", "", "", "", "-", "", "","-","","");
			
		for($i=0;$i<count($poleZnaku);$i++){
			$upravenyOdkaz=str_replace($poleZnaku[$i],$poleZnakuNahradnich[$i],$upravenyOdkaz);
			}
		$upravenyOdkaz=str_replace("---","-",$upravenyOdkaz);
		$upravenyOdkaz=str_replace("--","-",$upravenyOdkaz);
		return urlencode(strtolower($upravenyOdkaz));
	}

	
    
    
//vrati seo friendly url adresu
	function UrlPage($name, $id, $lang_name = DEFAULT_LANG, $relative=true){
				
        global $domain;
        
        
        $koncovka = $domain->data['urlKoncovka'];
        if($koncovka!='') $koncovka = ".".$koncovka;
        
		//$result = $name."-x".$id.".php";
        $struktura = $domain->data['urlStruktura'];
        
        $result = "";
        if($struktura=='id-nazev')
        	$result = $id."_".$name.$koncovka;
        elseif($struktura=='nazev-id')
            $result = $name."_".$id.$koncovka; 
        elseif($struktura)
            $result = $name.$koncovka;
        
        if(MULTILANGUAGE_WEB && $lang_name != DEFAULT_LANG)
            $result = $lang_name."/".$result;
        
		return $relative ? $domain->getRelativeUrl().$result : $domain->getUrl().$result;
		}
        
    //osetri retezec proti xss utoku
	function secureString($string){
		$result = htmlspecialchars($string, ENT_QUOTES); 
		return $result;
	}

    function ss($string)
    {
        return secureString($string);
    }

    
    	//funkce na ochranu wysiwyg editoru proti xss utoku
	function secureStringEditor($string, $fullpage=false){
        /*
		$TinyMceProtect = new InputFilter($fullpage,0,0,0);
		$string = $TinyMceProtect->process($string);
        */
		return $string;
		
	}
    
    function req(){
        return icon('req',TPOVINNA_POLOZKA,'req');
    }
    
    //vraci nazev mesice podle cisla mesice v roce
	function GetMonthName($digitMonth, $type=1){
		$digitMonth = intval($digitMonth);
		if($type==1){
			switch(intval($digitMonth)){
				case 1 : {return TLEDEN;}
				case 2 : {return TUNOR;}
				case 3 : {return TBREZEN;}
				case 4 : {return TDUBEN;}
				case 5 : {return TKVETEN;}
				case 6 : {return TCERVEN;}
				case 7 : {return TCERVENEC;}
				case 8 : {return TSRPEN;}
				case 9 : {return TZARI;}
				case 10 : {return TRIJEN;}
				case 11 : {return TLISTOPAD;}
				case 12 : {return TPROSINEC;}
				default : {return '';}
				}
			}
		if($type==2){
			switch(intval($digitMonth)){
				case 1 : {return TLEDNA;}
				case 2 : {return TUNORA;}
				case 3 : {return TBREZNA;}
				case 4 : {return TDUBNA;}
				case 5 : {return TKVETNA;}
				case 6 : {return TCERVNA;}
				case 7 : {return TCERVENCE;}
				case 8 : {return TSRPNA;}
				case 9 : {return TZARI;}
				case 10 : {return TRIJNA;}
				case 11 : {return TLISTOPADU;}
				case 12 : {return TPROSINCE;}
				default : {return '';}
			
				}
			return '';	
			}	
	}
    
    
    function loadTranslations($jazyk, $jen_pro_frontend=false){
        global $db;
        
        if($jazyk=='') return;
        
        if($jen_pro_frontend)
            $data = $db->Query("SELECT konstanta, ".$jazyk." AS text FROM ".TABLE_PREKLADY);
            else
            $data = $db->Query("SELECT konstanta, ".$jazyk." AS text FROM ".TABLE_PREKLADY_ADMINISTRACE);
        
        //$inline_editace = $jen_pro_frontend && MODULE_INLINE_EDIT && logged_into_cms();
        
        while($p = $db->getAssoc($data)){
            $preklad = $p['text'];
            /*
            
            if($inline_editace)
            {
                $preklad = "<span contenteditable='true'>".$preklad."</span>";
            }
            */
            define($p['konstanta'],$preklad);
            
            }
        
        
    }
    
//nacte nastaveni    
function loadSettings($idDomain, $hash = "", $pro_administraci = true)
{
    
    global $db;
    global $domain;
    
    $hash = $hash == "" ? $domain->get("hash") : $hash;

    $data = $db->query("SELECT if(nd.hodnota IS NULL, n.hodnota,nd.hodnota) AS hodnota, n.typ, n.konstanta
        FROM ".TABLE_NASTAVENI." AS n
        LEFT JOIN ".TABLE_NASTAVENI_DOMENY." AS nd ON n.idNastaveni=nd.idNastaveni AND nd.idDomeny=".$idDomain."
        WHERE n.aktivni=1
        GROUP BY n.idNastaveni
        ");
    
    while($n = $db->getAssoc($data))
    {
        $hodnota = trim($n['hodnota']);
        if($n['typ'] == 'ga')
        {
            if($hodnota != "")
            {
                $rc4 = new rc4crypt;
                $hodnota = (array) unserialize($hodnota);
                $hodnota["GA_HESLO"] = $rc4->endecrypt($hash,$hodnota["GA_HESLO"],'de');
                foreach($hodnota AS $c => $h)
                    define($c,$h);
            }
            else
            {
                define("GA_HESLO","");
                define("GA_EMAIL","");
                define("GA_PROFIL","");
            }
            
            continue;
        }
        elseif(in_array($n['typ'],array("input-lang","textarea-lang","editor-lang")) && !$pro_administraci)
        {

            $s = unserialize($hodnota);
            $val = isset($s[WEB_LANG_ID]) ? $s[WEB_LANG_ID] : "";
            define($n['konstanta'], $val);
            
            continue;
        }      
        
        define($n['konstanta'],$hodnota);
        
    }
    
    
    
    
}


function loader($class='', $id='', $skryt=true){
    if($id!='') $id = "id='".$id."'";
    
    $style = "";
    if($skryt)
        $style = "style='display:none'";     
    return "<div class='loader ".$class."' ".$id." ".$style."><center>".TVAS_POZADAVEK_SE_ZPRACOVAVA."</center></div>"; 
}


function cut_text($text, $delka, $pripojit = ' ...')
{
$c_text='';
$text=stripslashes(strip_tags($text));
//echo 'vstup text:'.mb_strlen($text,'utf8');
//var_dump($text);
$t_array = explode(" ",$text);
if (count($t_array)>1)
    {
    for ($i = 0; $i < count($t_array); $i++)
        {
		if (mb_strlen($t_array[$i],'utf8')+mb_strlen($c_text,'utf8')>$delka)
			{
			$c_text=substr($c_text, 0, -1);
            $c_text .= $pripojit;
            break;
			}
		else
			{
			$c_text .= $t_array[$i].' ';				
			}
        }
    return $c_text;
    }
else return $text;
}


//vraci nazev adresare pro domenu 
/*
function getDir($idDomeny, $domena=""){ 
    $idDomeny = get_sysint($idDomeny);
	$name = mb_eregi_replace("\.","_",$domena);
	$dir = $idDomeny;//."-".$name;	 
    return DIRNAME_DOMAINS.$dir;

}
*/


    //vraci systemove cislo poradi
function get_sysint($number){
	$id = $number;
	if($number < 10 ) $id = "000".$number;
	elseif($number < 100 ) $id = "00".$number; 
	elseif($number < 1000 ) $id = "0".$number; 
	elseif($number >= 1000 ) $id = $number; 
	return $id;
	}
    
function array2json($arr) {
    if(function_exists('json_encode')) return json_encode($arr); //Lastest versions of PHP already has this functionality.
    $parts = array();
    $is_list = false;

    //Find out if the given array is a numerical array
    $keys = array_keys($arr);
    $max_length = count($arr)-1;
    if(($keys[0] == 0) and ($keys[$max_length] == $max_length)) {//See if the first key is 0 and last key is length - 1
        $is_list = true;
        for($i=0; $i<count($keys); $i++) { //See if each key correspondes to its position
            if($i != $keys[$i]) { //A key fails at position check.
                $is_list = false; //It is an associative array.
                break;
            }
        }
    }

    foreach($arr as $key=>$value) {
        if(is_array($value)) { //Custom handling for arrays
            if($is_list) $parts[] = array2json($value); /* :RECURSION: */
            else $parts[] = '"' . $key . '":' . array2json($value); /* :RECURSION: */
        } else {
            $str = '';
            if(!$is_list) $str = '"' . $key . '":';

            //Custom handling for multiple data types
            if(is_numeric($value)) $str .= $value; //Numbers
            elseif($value === false) $str .= 'false'; //The booleans
            elseif($value === true) $str .= 'true';
            else $str .= '"' . addslashes($value) . '"'; //All other things
            // :TODO: Is there any more datatype we should be in the lookout for? (Object?)

            $parts[] = $str;
        }
    }
    $json = implode(',',$parts);
    
    if($is_list) return '[' . $json . ']';//Return numerical JSON
    return '{' . $json . '}';//Return associative JSON
} 


//nastavi konstanty zapnutych modulu
function GetActiveModules()
{
    global $domain;
    global $db;
    
    $moduly = $domain->data['moduly'];
    
    if(count($moduly)==0) return false;
    $data = $db->Query("SELECT konstanta, IF(zobrazit = 1 AND idModulu IN (".implode(',',$moduly)."),1,0) AS hodnota FROM ".TABLE_CMS_MODULY." WHERE admin = 0");
    
    while($m = $db->getAssoc($data))
        if($m['hodnota']==1)
            define($m['konstanta'], true);
            else
            define($m['konstanta'], false);
    
}

//vraci retezec mezi dvema retezci nebo znaky
function get_string_between ($text, $s1, $s2) 
{
    $mid_url = "";
    $pos_s = strpos($text,$s1);
    $pos_e = strpos($text,$s2);
    for ( $i=$pos_s+strlen($s1) ; (( $i<($pos_e)) && $i < strlen($text)) ; $i++ ) {
    $mid_url .= $text[$i];
    }
    return $mid_url;
}


function is_session($name)
{
    return isset($_SESSION[$name]);
}

function set_session($name, $value = "")
{
    $_SESSION[$name] = $value;
}

function get_session($name, $default_value = null)
{
    return is_session($name) ? $_SESSION[$name] : $default_value;
}

function remove_session($name, $default_value = null)
{
    return is_session($name) ? session_unset($name) : false;
}

//funkce vraci post hodnotu predanou modulu
    function get_post( $key , $default_value = "")
    {
                
        return is_post($key) ? trim($_POST[$key]) : $default_value;
        
    }
    
    function get_request( $key , $default_value = "")
    {
                
        return is_request($key) ? trim($_REQUEST[$key]) : $default_value;
        
    }

    //funkce vraci post hodnotu predanou modulu osetrenou
    function get_secure_post( $key , $strip_tags = true, $default_value = "", $we = false)
    {
        $post_key = get_string_between($key,"[","]");

        
        if($post_key != "")
        {
            $post_name = rtrim($key,"[".$post_key."]");
            $string = is_post($key) ? htmlspecialchars($_POST[$post_name][$post_key], ENT_QUOTES) : $default_value; 
        }
        else
            $string = is_post($key) ? htmlspecialchars($_POST[$key], ENT_QUOTES) : $default_value;
            
        $string = $strip_tags ? strip_tags($string) : $string;
        return trim($string);
    }
    
    //funkce vraci post hodnotu konvertovanou na int
    function get_int_post($key, $default_value = 0)
    {
        return is_post($key) ? intval($_POST[$key]) : $default_value;
    }
    
    //funkce vraci post hodnotu konvertovanou na int
    function get_int_request($key, $default_value = 0)
    {
        return is_request($key) ? intval($_REQUEST[$key]) : $default_value;
    }
    
    
    //funkce vraci get hodnotu konvertovanou na int
    function get_int_get($key, $default_value = 0)
    {
        return is_get($key) ? intval($_GET[$key]) : $default_value;
    }
    
    //funkce vraci post hodnotu konvertovanou na pole
    function get_array_post($key, $default_value = array())
    {
        return is_post($key) ? (array)$_POST[$key] : (array)$default_value;
    }
    
    //overi zda existuje post promenna s danym klicem
    function is_post($key)
    {
        $post_key = get_string_between($key,"[","]");
        
        if($post_key != "")
        {
            $post_name = rtrim($key,"[".$post_key."]");
            
            return isset($_POST[$post_name][$post_key]); 
        }
        else
        return isset($_POST[$key]);
    }
    
    //overi zda existuje post promenna s danym klicem
    function is_request($key)
    {
        $post_key = get_string_between($key,"[","]");
        
        if($post_key != "")
        {
            $post_name = rtrim($key,"[".$post_key."]");
            
            return isset($_REQUEST[$post_name][$post_key]); 
        }
        else
        return isset($_REQUEST[$key]);
    }
    
    //overi zda existuje post promenna s danym klicem
    function is_get($key)
    {
        return isset($_GET[$key]);
    }
    
    //overi zda existuje post promenna s danym klicem
    function is_action($btnName = 'btnAction')
    {
        return is_post($btnName);
    }
    
    function CropAndSaveWords($Where,$What,$WordsAround)
		{
		$Where = trim($Where);
		$What = trim($What);
		
		//$What = "Úklid";
		//echo $What;
		$i = mb_stripos($Where,$What);
		if($i==false)
			{
			$i = 0;
			}
	
		while($i>0 && isset($Where[$i-$WordsAround]) && $Where[$i-$WordsAround]!=" ")
			{
			$i--;
			}
	
		if($i-$WordsAround>=0)
			{
			$Where = substr($Where,$i-$WordsAround);
			}
		else
			{
			$WordsAround = $WordsAround ;
			}
		$i = strpos($Where,$What);
		if($i==false)
			{
			$i = 0;
			}
			while($i<strlen($Where) && isset($Where[$i+$WordsAround+strlen($What)]) && $Where[$i+$WordsAround+strlen($What)]!=" ")
				{
				$i++;
				}
	
		$Where = substr($Where,0,$i+$WordsAround+strlen($What));
		return "...".$Where."...";
	}
    
    
    
    function MarkWord($text, $slovo) {
        if(mb_strlen($slovo) < 3) return $text;
        
    	$slova=explode(' ',$slovo);
		
    	for($f=0;$f<count($slova);$f++)	{
			    $search = preg_quote(htmlspecialchars(stripslashes($slova[$f])), '~');
			    //$search = $slova[$f];
			    if ($search) {
			        $text = preg_replace("~$search~i", '<span class="search-term">\\0</span>', $text);
			        //$text = eregi_replace($search, '<span class="search-term">'.$search.'</span>', $text);
			        
			        // odstranění zvýrazňování z obsahu <option> a <textarea> a zevnitř značek a entit
			        $span = '<span class="search-term">[^<]*</span>';
			        $pattern = "~<(option|textarea)[\\s>]([^<]*$span)+|<([^>]*$span)+|&([^;]*$span)+~i";
			        $text = preg_replace_callback($pattern, 'UnmarkWord', $text);
			    }
			}
		return $text;
		}
        
        
  function unmarkWord($matches)
    {
    	return preg_replace('~<span class="search-term">([^<]*)</span>~i', '\\1', $matches[0]);
    }
    
    
  function format_bytes($size) {
    $units = array(' B', ' kB', ' MB', ' GB', ' TB');
    for ($i = 0; $size >= 1024 && $i < 4; $i++) $size /= 1024;
    return round($size, 2).$units[$i];
}

//vraci novou url adresu
//vstup id stranky a url nazev
function get_url_nazev($id, $url_nazev)
{
    global $domain;
    global $db;
    
    if($domain->data['urlStruktura']=='nazev')
    {  
        $where = "";
        if($id>0)
            $where = "AND idStranky != ".$id; 
                        
        $verify_url_name = $db->Query("SELECT url FROM ".TABLE_STRANKY." WHERE url LIKE '".$url_nazev."%' AND idDomeny=".$domain->getId()." AND idJazyka=".WEB_LANG_ID." ".$where." ORDER BY url");
                    
        $url_zal = $url_nazev;
        $i=1;
        while($u = $db->getAssoc($verify_url_name)){
            if($url_zal==$u['url'])
                $url_zal = $url_nazev."-".$i;
                $i++;
            }
                    
        return $url_zal;
                    
    } 
    
    return $url_nazev;
    
}

function strToHex($string)
{
    $hex='';
    for ($i=0; $i < strlen($string); $i++)
    {
        $hex .= dechex(ord($string[$i]));
    }
    return $hex;
}

function hexToStr($hex)
{
    $string='';
    for ($i=0; $i < strlen($hex)-1; $i+=2)
    {
        $string .= chr(hexdec($hex[$i].$hex[$i+1]));
    }
    return $string;
}



class rc4crypt {
    
    private $kod = "A1t5Z9";
    
    function endecrypt ($pwd, $data, $case='') {
        
        
        if ($case == 'de') {
            $data = urldecode(base64_decode(str_replace($this->kod,"=",hexToStr($data))));
        }

        $key[] = "";
        $box[] = "";
        $temp_swap = "";
        $pwd_length = 0;

        $pwd_length = strlen($pwd);

        for ($i = 0; $i <= 255; $i++) {
            $key[$i] = ord(substr($pwd, ($i % $pwd_length), 1));
            $box[$i] = $i;
        }

        $x = 0;

        for ($i = 0; $i <= 255; $i++) {
            $x = ($x + $box[$i] + $key[$i]) % 256;
            $temp_swap = $box[$i];

            $box[$i] = $box[$x];
            $box[$x] = $temp_swap;
        }

        $temp = "";
        $k = "";

        $cipherby = "";
        $cipher = "";

        $a = 0;
        $j = 0;

        for ($i = 0; $i < strlen($data); $i++) {
            $a = ($a + 1) % 256;
            $j = ($j + $box[$a]) % 256;

            $temp = $box[$a];
            $box[$a] = $box[$j];

            $box[$j] = $temp;

            $k = $box[(($box[$a] + $box[$j]) % 256)];
            $cipherby = ord(substr($data, $i, 1)) ^ $k;

            $cipher .= chr($cipherby);
        }

        if ($case == 'de') {
            $cipher = urldecode(urlencode($cipher));

        } else {
            $cipher = strToHex(str_replace("=",$this->kod,base64_encode(urlencode($cipher))));
        }

        return $cipher;
    }
}


//pokud je uzivatel prihlasen do cms, pak vraci true, jinak false
function logged_into_cms()
{
        return isset($_SESSION['idUzivatele']) || (isset($_SESSION['inline_editace']) && $_SESSION['inline_editace']);

}
    
//prevede tagy br na znak pro novy radek
function br2nl($text)
{
    return  preg_replace('/<br\\s*?\/??>/i', '\n', $text);
}

//prevede tagy p na znak pro novy radek
function p2nl ($str) {
    return preg_replace(array("/<p[^>]*>/iU","/<\/p[^>]*>/iU"),
                        array("","\n"),
                        $str);
}

function price($amount)
{
    return number_format((float) $amount, 0, ',', ' ')." Kč";
    
    
    
}


function zapocitej_zobrazeni_stranky()
{
    global $db;
    global $page;
    
    if($page->get_type() == '404' || $page->get_type() == 'uvodni')
        return false;
        
    
    $db->query("UPDATE ".TABLE_STRANKY." SET 
            pocetZobrazeni = pocetZobrazeni + 1, 
            prvniMereniZobrazeni = IF(prvniMereniZobrazeni IS NULL, CURDATE(), prvniMereniZobrazeni)
        WHERE idStranky = ".$page->get_id()."
        ");
        
    
        
        
}


//z data ve formatu DD.MM.RRRR vytvori retezec pro google analytics typu RRRR-MM-DD
function get_sql_date($date){
		$arr = explode('.', $date);
		if(count($arr)!=3) return '';
		$d = intval($arr[0]);
		$m = intval($arr[1]);
		$y = intval($arr[2]);
		
		if($d <= 31 && $d > 0 && $m < 13 && $m > 0 && $y < 2030 && $y > 2000){
			if(strlen($d)<2 && strlen($d)>0) $d = "0".$d;
			if(strlen($m)<2 && strlen($m)>0) $m = "0".$m;
			switch(strlen($y)){
				case 2: {$y = "20".$y; break;}
				case 3: {$y = "2".$y; break;}
				case 4: {break;}
				default: {return '';}
				}   
			} 
			else
			return '';
			
		$res = $y."-".$m."-".$d;	
		return $res;
		
	}

function get_url_for_redirect($cil)
{
    global $domain;
    
    
    
    if(strstr($cil,"http://") || strstr($cil,"https://") || strstr($cil,"ftp://") || strstr($cil,"ftps://"))
        $url = $cil;
    elseif(strstr($cil,"www.") && $cil[0] == "w")
        $url = "http://".$cil;
    elseif($cil == "/")
        $url = $domain->getUrl();
    elseif($cil[0] == "/")
    {
        $cil = substr($cil,1);
        $url = $domain->getUrl().$cil;
    }
    else
        $url = $domain->getUrl()."/".$cil;
    
    return $url;
}


function Redirect301()
{
    global $db;
    global $domain;
    
    $uri = str_replace($domain->getRelativeUrl(),"",$_SERVER['REQUEST_URI']);
    
    $absolute_url = $domain->getUrl().$uri;
           
    $relative_url = $uri;  
    
    $cil = $db->get(TABLE_PRESMEROVANI, "cil", "aktivni=1 AND idDomeny=".$domain->getId()."
            AND (zdroj LIKE '".$db->secureString($absolute_url)."' OR zdroj LIKE '".$db->secureString($relative_url)."' OR zdroj LIKE '/".$db->secureString($relative_url)."')");
    

    if($cil != '')
    {
        
        $cilova_url = $cil;
        
        if(substr($cilova_url,0, 1) == '/')
            $cilova_url = substr($cilova_url,1);
        
        if(substr($cilova_url,0, 7) != PROTOKOL && substr($cilova_url,0, 8) != PROTOKOL)
            $cilova_url = $domain->getUrl().$cilova_url;

        //echo $cilova_url;    
        header("Location: ".$cilova_url."",true,301);
        exit();
    }
    
    return;
      
    
}


function password_gen($length=30,$chars = 'abcdefghijkmnopqrstuvwxyz123456789'){
    $numChars = strlen($chars);
    $r = '';
    for ($i = 0; $i < $length; $i++) {
        $r .= substr($chars, mt_rand(1, $numChars) - 1, 1);
        }
    return $r;
}


function add_log($modul_nazev, $error_popis = "", $code = "", $user_id = 0)
{
    
    global $db;
    global $private_pages;
    
    if(isset($private_pages) && $private_pages->get_user_id() > 0 && $user_id == 0)
    {
        $user_id = $private_pages->get_user_id();
    }
    
    
    $insert = array(
        "id_uzivatel" => $user_id, 
        "datum" => "now",
        "modul" => $modul_nazev,
        "popis" => $error_popis,
        "post" => count($_POST) > 0 ? print_r($_POST, true) : "",
        "get_param" => count($_GET) > 0 ? print_r($_GET, true) : "",
        "session" => isset($_SESSION) && count($_SESSION) > 0 ? print_r($_SESSION, true) : "",
        "code" => $code
        );
    
    $db->insert(TABLE_LOG_ERROR, $insert);
    
    
    
}

//vytvori casovou znacku, pro mereni kritickych mist skriptu, aby se dalo zjistit jak dlouho ktera cast trva
function point($i)
{
    echo $i.": ".date("H:i:s")."<br>";
}


//zjisti zda jde o ajaxovy pozadavek
function is_ajax()
{
    //osetreni detekce ajaxu, viz clanek https://blog.justincarmony.com/2015/03/11/x-requested-with-ajax-requests-and-android-webviews/ a emaily od seznam.cz
    //ve zkratce jde o to, ze prohlizece postavene na WebView a OS Android posilaji tuto hlavicku automaticky, system si mysli ze jde o ajax a protoze nevyhovi pozadavku hodi exit.
    //po obnoveni stranky v danem prohlizeci to paradoxne funguje, ale pri prvnim nacteni nikoliv. 
    return is_request("ajax");
    //return !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']);
}

    
function dd($var)
{
    echo var_dump($var);
    exit;
}

   
//overi zda je recaptcha spravne
function recaptcha_check()
{
        
    $url = 'https://www.google.com/recaptcha/api/siteverify?secret='.RECAPTCHA_SECRET_KEY.'&response='.get_post('g-recaptcha-response')."&remoteip=".get_ip();

    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    //curl_setopt($ch, CURLOPT_POSTFIELDS, $post);

    $response = curl_exec($ch);   
    curl_close($ch);
    
    
    if(!$response)
        return false;
    
    $r = json_decode($response);
    
    //var_dump($r->success);
    
    return $r->success;
        
}    

?>