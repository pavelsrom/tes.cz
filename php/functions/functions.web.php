<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */





//zobrazi strankovani
//parametry: celkovy pocet vsech stranek, cislo aktivni stranky, id bloku kvuli sestrojeni url adresy pro strankovani, typ bloku se strankovanim (povolene hodnoty: sample, gallery, news, discussion), okoli urcuje kolik stranek se ma zobrazovat po prave a po leve strane od aktivni stranky, to co presahuje okoli je nahrazeno teckami. Prvni a posledni stranka se zobrazuje vzdy.

function GetPaging($pocet_stranek, $aktivni_stranka, $konec_url="", $okoli=4, $class=""){
	global $domain;
    global $page;
    global $links;
	
	$stranka = $aktivni_stranka;
	$tecky_leve = false;
	$tecky_prave = false;
	
	$html = "";
	
    //$konec_url = "#".$konec_url;
    
    $url = $links->get_url($page->get_id());
    
	if($pocet_stranek>1){
				$html .= "<div class='strankovani ".$class."'>";
                $html .= '<div class="strankovani_in">';
                
                if($pocet_stranek > 3)
                    if($stranka == 1)
				        $html .= "<span>".TPREDCHOZI."</span>";
					    else		
					    $html .= "<a href='".$url."?stranka=".($stranka-1).$konec_url."'>".TPREDCHOZI."</a>";
                
                
				for($j=1; $j<=$pocet_stranek; $j++){
					if($j==1){
						if($j==$stranka)
							$html .= "<span class='btn'>".$j."</span>";
							else
							{
							$html .= "<a class='btn' href='".$url.$konec_url."'>".$j."</a>";
							
							}
						continue;
						}
						
					if($j==$pocet_stranek){
						if($j==$stranka)
							$html .= "<span class='btn'>".$j."</span>";
							else
							$html .= "<a class='btn' href='".$url."?stranka=".$j.$konec_url."'>".$j."</a>";
							
						continue;
						}
						
					if($stranka>($j+$okoli)) {
						if(!$tecky_leve) $html .= ".....";
						$tecky_leve = true;
						continue;
						}
						
					if($stranka<($j-$okoli)){
						if(!$tecky_prave) $html .= ".....";
						$tecky_prave = true;
						continue;
						}

					if($j==$stranka)
						 $html .= "<span class='btn'>".$j."</span>";
						 else
                         $html .= "<a class='btn' href='".$url."?stranka=".$j.$konec_url."'>".$j."</a>";
							
					}
				
                if($pocet_stranek > 3)
                    if($stranka == $pocet_stranek)
				        $html .= "<span class='btn'>".TNASLEDUJICI."</span>";
					    else		
					    $html .= "<a class='btn' href='".$url."?stranka=".($stranka+1).$konec_url."'>".TNASLEDUJICI."</a>";
                
                	
                
				$html .= "</div>";
                $html .= "<div class='cleaner'></div>";
                $html .= "</div>";
				}
	
	return $html;
	
}




//zapocita proklik v emailove kampani
function zapocitatProklikEmailoveKampane(){
    global $db;
    global $domain;
    
    if(!MODULE_NEWSLETTER || !isset($_GET['click'], $_GET['p'], $_GET['k']) || isset($_COOKIE['proklik-kampane-'.$_GET['k']])) 
        return false;
        
    $hash_prijemce = trim($_GET['p']);
    $idKampane = intval($_GET['k']);
       
    if($idKampane<=0) return false;
       
    //zjisteni id prijemce
    $idPrijemce = $db->get(TABLE_EMAILY_PRIJEMCI,"idPrijemce","hash='".$hash_prijemce."'");
    $idPrijemce = intval($idPrijemce);
    
    if($idPrijemce <= 0)
        return false;
    
    //zjisteni zda uz byl proklik zapocitan
    $id_fronta = $db->update(TABLE_EMAILY_FRONTA,array("kliknuto" => 1, "otevreno" => 1),"idPrijemce=".$idPrijemce." AND idKampane=".$idKampane);
    
    $idRelace = $db->get(TABLE_EMAILY_FRONTA,"id","idKampane=".$idKampane." AND idPrijemce=".$idPrijemce." AND otevreno = 0");
    if($idRelace > 0)
        $db->query("UPDATE ".TABLE_EMAILY_KAMPANE." SET otevreno = otevreno + 1 WHERE idKampane=".$idKampane." AND idDomeny=".$domain->getId());
        
        
        
    $idRelace = $db->get(TABLE_EMAILY_FRONTA,"id","idKampane=".$idKampane." AND idPrijemce=".$idPrijemce." AND kliknuto = 0");
    if($idRelace > 0)
        $db->query("UPDATE ".TABLE_EMAILY_KAMPANE." SET kliknuto = kliknuto + 1 WHERE idKampane=".$idKampane." AND idDomeny=".$domain->getId());
    
       
    //opakovany proklik je mozny az za 24 * 30 hodin = 30 dni od predesleho prokliku pres email.
    setcookie('proklik-kampane-'.$idKampane, 1, time() + 3600 * 24 * 30, '/');
    return true;

    }
   

function zapocitatKonverziEmailoveKampane($idKampane){
    global $db;

    if(!isset($_COOKIE['proklik-kampane-'.$idKampane])) return false;
        
    $hash_prijemce = trim($_COOKIE['proklik-kampane-'.$idKampane]);
       
    if($idKampane<=0 || $hash_prijemce == '') return false;
       
       
    $data = $db->Query("SELECT s.idPolozky AS id 
        FROM ".TABLE_EMAILY_PRIJEMCI." AS p
        LEFT JOIN ".TABLE_EMAILY_STAVY." AS s ON s.idPrijemce=p.idPrijemce 
        WHERE s.idKampane=".$idKampane."
            AND p.hash = '".$hash_prijemce."'
        LIMIT 1
        ");    
        
    if($db->numRows($data)==0) return false;
    $s = $db->getAssoc($data);
            
    $db->Query("UPDATE ".TABLE_EMAILY_STAVY." SET konverze = konverze + 1 WHERE idPolozky=".intval($s['id'])." AND idKampane=".$idKampane." LIMIT 1");    
    
    //opakovana konverze je mozna az za 24 * 2 = 2 dny od predesleho prokliku pres email.
    setcookie('konverze-kampane-'.$idKampane, time(), time() + 3600 * 24 * 1, '/');
    return true;

    }

function odzvyraznit($matches) {
    return preg_replace('~<span class="search-result">([^<]*)</span>~i', '\\1', $matches[0]);
}



//zv�razn� text - v�echna slova
function zvyraznit($text, $slovo) {

    $slova=explode(' ',$slovo);
    for($f=0;$f<count($slova);$f++)
		{
		    $search = preg_quote(htmlspecialchars(stripslashes($slova[$f])), '~');
		    if ($search) {
		        $text = preg_replace("~$search~i", '<span class="search-result">\\0</span>', $text);
		        // odstran�n� zv�raz�ov�n� z obsahu <option> a <textarea> a zevnit� zna�ek a entit
		        $span = '<span class="search-result">[^<]*</span>';
		        $pattern = "~<(option|textarea)[\\s>]([^<]*$span)+|<([^>]*$span)+|&([^;]*$span)+~i";
		        $text = preg_replace_callback($pattern, 'odzvyraznit', $text);
		    }
		}
return $text;
} 

function validate_email($e){
    return (bool)preg_match("`^[a-z0-9!#$%&'*+\/=?^_\`{|}~-]+(?:\.[a-z0-9!#$%&'*+\/=?^_\`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$`i", trim($e));
}


function add_user_log()
{
    global $db;
    global $page;
    global $domain;
    global $private_pages;
    
    if(!$private_pages->is_logged())
        return false;
    
    
        
    //ulozi stranku jen kdyz je stranka privatni    
    if($private_pages->is_private($page->get_id()))
    {
        
        $insert = array(
            "idUzivatele" => $private_pages->get_user_id(),
            "idStranky" => $page->get_id(),
            "datum" => "now"
            );
        
        $db->insert(TABLE_UZIVATELE_LOG, $insert);
        
        //smazani zaznamu starsich 60 dni
        $db->delete(TABLE_UZIVATELE_LOG, "ABS(DATEDIFF(DATE(datum), CURDATE())) > 60");
    }
    return true;
    
    
}


function get_ip()
{
    $ip = "";
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    
    return $ip;
}


//zapocita hlasovani v ankete
function zapocitatHlasovaniAnkety()
{
    global $db;
    global $domain;
    global $userSystemMessage;
    
    if(!MODULE_INQUIRY)
        return false;
    
    
    //kontrolni cookies
    //pokud neni v prohlizeci ulozena tato cookies, bude hlasovani znemozneno
    
    
    
    if(!isset($_COOKIE['voting_allow']))
    {
        $res = setcookie( 'voting_allow', 'ok' , time() + 3600 * 24 * 3);
   
    }
    
    
    $vote = get_request('vote');
    if($vote == "")
        return false;
    
    $arr = explode('|',$vote);
    $idAnkety = intval($arr[0]);
    $idOdpovedi = intval($arr[1]);
    
    if($idAnkety <= 0 || $idOdpovedi <= 0)
        return false;
    
    if(isset($_SERVER['HTTP_REFERER']))
        $return_url = $_SERVER['HTTP_REFERER']."#a".$idAnkety;
        else
        $return_url = "http://www.".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
    
    $ip = get_ip();
    
    $data = $db->query("SELECT o.idOdpovedi, IF(ip.ip IS NULL OR ip.datum != CURDATE(), 0,1) hlasoval 
            FROM ".TABLE_ANKETY." AS a
            LEFT JOIN ".TABLE_ANKETY_IP." AS ip ON a.idAnkety = ip.idAnkety AND ip = '".$ip."' 
            LEFT JOIN ".TABLE_ANKETY_ODPOVEDI." AS o ON a.idAnkety = o.idAnkety
            LEFT JOIN ".TABLE_ANKETY_ODPOVEDI_NAZVY." AS n ON n.idOdpovedi = o.idOdpovedi
            WHERE a.idAnkety = ".$idAnkety."
                AND (a.od IS NULL OR a.od = '0000-00-00' OR a.od <= CURDATE())
                AND (a.do IS NULL OR a.do = '0000-00-00' OR a.do >= CURDATE())
                AND a.zobrazit = 1
                AND n.idJazyka = ".WEB_LANG_ID."
                AND a.idDomeny = ".$domain->getId()."
                AND o.idOdpovedi = ".$idOdpovedi."
            GROUP BY a.idAnkety
            LIMIT 1
            ");
    
    if($db->numRows($data) == 0)
    {
        Redirect($return_url);
    }
    
    $a = $db->getObject($data);
    
    		
    $system_name = 'inquiry_'.$idAnkety;
		
    $userSystemMessage->set_form_id($system_name);
        
    if($a->hlasoval || isset($_COOKIE[$system_name]) || isset($_SESSION[$system_name])) {
    	$userSystemMessage->add_error(TMUZETE_HLASOVAT_JEN_JEDNOU);
    	Redirect($return_url);
    	}
    
    
    $db->Query("UPDATE ".TABLE_ANKETY_ODPOVEDI." SET hlasy = hlasy + 1 WHERE idOdpovedi=".$idOdpovedi." AND idAnkety=".$idAnkety." LIMIT 1");

    setcookie( $system_name, 'set' , time() + INQUIRY_NO_VOTING);
    
    if($a->hlasoval)
   	    $db->insert(TABLE_ANKETY_IP, array("idAnkety" => $idAnkety, "ip" => $ip, "datum" => "now"));
        else
        $db->update(TABLE_ANKETY_IP, array("datum" => "now"),"idAnkety = ".$idAnkety." AND ip='".$ip."'");
        
   	$userSystemMessage->add_ok(TVAS_HLAS_BYL_ZAPOCITAN); 
    		
    Redirect($return_url);
 
}

//funkce obaluje obsah do elementu pro inline editaci
function editable($id, $content, $type = "preklad", $editable_cond = true, $element = "")
{
    
    $editace_na_webu_povolena = isset($_SESSION['ine']) && $_SESSION['ine'];
    
    $editable = MODULE_INLINE_EDIT && logged_into_cms() && $editace_na_webu_povolena && $editable_cond;
    //var_dump($editace_na_webu_povolena);
    
        
    $el = "";
    if($type == "h1")
        $el = "h1";    
        
    if($element != '')
        $el = $element;
        
    if(!$editable)
    {
        if($el != '')
            return "<".$el." class='ed'>".$content."</".$el.">";
            else
            return $content;

    }    

    $el = $el == "" ? 'div' : $el;
    $style = "";
    if($type == 'preklad')
        $style = "style='display:inline'";
    
    $html = "<".$el." class='ed' contenteditable='true' id='".$type."-".$id."' ".$style.">".$content."</".$el.">";
    
    
    return $html;
    
    
    
    
    
}



function getLangs()
{
    global $lang;
    
    $langs = $lang->get_langs();
    $html = "<ul>";
    arsort($langs);
    foreach($langs AS $j => $l)
    {
        $html .= '<li>';

        if(LANG == $j)
            $html .= '<span>'.strtoupper($j).'</span>';
            else
            $html .= '<a href="'.RELATIVE_PATH.$j.'/" title="'.constant($l['nazev']).'">'.strtoupper($j).'</a>';
        
        $html .= '</li>';

    }

    $html .= '</ul>';
    
    return $html;
    
}



?>