<?php

/**
 * @author Pavel Srom
 * @copyright 2010
 */

//funkce ktere zpracovavaji chyby a odesilaji je



function error_settings()
{
    set_error_handler('error_handler');
    error_reporting(SHOW_ERRORS ? (E_ALL & ~E_DEPRECATED) : 0);
    
}


//osetruje chyby pri behu cms systemu - administracni cast	
function error_handler($typ, $chyba, $soubor, $radek){
	
	if(ERROR_ADMIN_EMAIL=='') return;
		
	$host = $_SERVER['HTTP_HOST'];
	$typ = GetTypeSystemMessage($typ, $admin);
		
	if($typ=='') return false;
		
	$zprava = "V redakčním systému na domeně ".$host." došlo k následující chybě<br/><br/>";
	$zprava .= "
		Doména: <b>".$host."</b><br/>
		Typ chyby: <b>".$typ."</b><br/>
		Popis: ".$chyba."<br/>
		V souboru: ".$soubor."<br/>
		Na řádku: ".$radek."<br/><br/>
		";
	
	$zprava .= GetVars($_GET, 'get');
	$zprava .= GetVars($_POST,'post');
	$zprava .= GetVars($_SESSION,'session');
	
	if(ERROR_ADMIN_EMAIL!='')
		SendMail(ERROR_ADMIN_EMAIL, FROM_ADMIN_NAME, ERROR_ADMIN_EMAIL, 'Chyba v redakčním systému - '.$host, "<html><body>".$zprava."</body></html>", $zprava);	
	
	return false;
	}
	
    
    
//osetruje chyby, ktere se jevi jako pokus o napadeni redakcniho systemu
function error_handler_hacking($moduleName, $message=''){
	global $login_obj;
	
	if(ERROR_ADMIN_EMAIL=='' || !SEND_HACKING) return;
	
	$host = $_SERVER['HTTP_HOST'];
	
	$zprava = "Uživatel <b>".$login_obj->getName()."</b> (id: <b>".$login_obj->getId()."</b>ip: ".$_SERVER['REMOTE_ADDR'].", práva: <b>".$login_obj->getPrivileges()."</b>) se zřejmě pokusil narušit běh CMS v modulu <b>".$moduleName."</b>. Přidělená doména: <b>".$domain->getName()."</b>. Pokus o narušení bezpečnosti byl proveden v redakčním systému na doméně ".$host.".<br />";
	if($message!='') $zprava .= "Upřesňující zpráva: ".$message."<br />";
	
	$zprava .= GetVars($_GET, 'get');
	$zprava .= GetVars($_POST,'post');
	$zprava .= GetVars($_SESSION,'session');
	
	SendMail(ERROR_ADMIN_EMAIL, FROM_ADMIN_NAME, ERROR_ADMIN_EMAIL, 'Pokus o narušení bezpečnosti - '.$host, "<html><body>".$zprava."</body></html>", $zprava);
	
	}
	
    
    
function sql_error_handler($query, $errno, $error){

	$host = $_SERVER['HTTP_HOST'];
	
	$zprava = '';
	$zprava .= "<div>Domena: ".$host."</div>";
	$zprava .= "<strong>".$query."</strong>\n";
	$zprava .= "<div>".$errno." - ".$error."</div>\n";
	
	if(SHOW_ERRORS==1) 
		echo $zprava;
			
	if(ERROR_ADMIN_EMAIL!='' && SEND_SQL_ERROR) {
		$zprava .= GetVars($_GET, 'get');
		$zprava .= GetVars($_POST,'post');
		$zprava .= GetVars($_SESSION,'session');

		SendMail(ERROR_ADMIN_EMAIL, FROM_ADMIN_NAME, ERROR_ADMIN_EMAIL, 'SQL CHYBA - '.$host, "<html><body>".$zprava."</body></html>", $zprava);
		}

	}



//vypise get promenne
function GetVars($var, $var_name=''){
	$vars = '<br />';
	
	if(is_array($var) && count($var)>0){
		foreach($var AS $i => $v)
			if(is_array($v) && count($v)>0){
				$vars .= "<br />----- vypis pole: ".$i."-----<br />";
				foreach($v AS $j => $w)
					$vars .= $j." => ".$w."<br/>";
				$vars .= "----------------------<br /><br />";
				}
				else
				$vars .= $i." => ".$v."<br />";
		}
		
	$vars = "Výpis proměnných ".strtoupper($var_name).": <br />".$vars."<br />";
	return $vars;
	
}


//vrati typ systemove zpravy podle systemoveho typu chybove zpravy
function GetTypeSystemMessage($typ,  & $admin){
	switch($typ){
		case E_ERROR:{
			if(SEND_FATAL_ERROR)
				$typ = "Error";
				else
				$typ = "";
			break;
			}
		case E_PARSE:{
			if(SEND_FATAL_ERROR)
				$typ = "Parse error";
				else
				$typ = "";
			break;
			}
		case E_WARNING:{
			if(SEND_WARNING)
				$typ = "Varování";
				else
				$typ = "";
			break;
			}
			
		case E_NOTICE:{
			if(SEND_NOTICE)
				$typ = "Upozornění";
				else
				$typ = "";
			break;
			}
		default:{
			$typ = "";
			break;
			}
		
		}
	
	return $typ;
	
}



function GetUploadError($error){
	if($error == 0) return '';
	switch($error){
		case UPLOAD_ERR_INI_SIZE:{
			return TUPLOAD_ERR_INI_SIZE;
			}
		case UPLOAD_ERR_FORM_SIZE:{
			return TUPLOAD_ERR_FORM_SIZE;
			}
		case UPLOAD_ERR_PARTIAL:{
			return TUPLOAD_ERR_PARTIAL;
			}
		case UPLOAD_ERR_NO_FILE:{
			return TUPLOAD_ERR_NO_FILE;
			}
		case UPLOAD_ERR_NO_TMP_DIR:{
			return TUPLOAD_ERR_NO_TMP_DIR;
			}
		case UPLOAD_ERR_CANT_WRITE:{
			return TUPLOAD_ERR_CANT_WRITE;
			}
		case UPLOAD_ERR_EXTENSION:{
			return TUPLOAD_ERR_EXTENSION;
			}
		default:{return '';};	
		
		
	}

}


?>