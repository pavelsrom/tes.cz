<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

//prevede retezec do ascii - pouziva se ke spravnemu nazvu souboru
	function ToAscii($text, $toLower=true, $full_str_replace=false) {
         $convertTable = array (
              'Ě' => 'e', 'Š' => 's', 'Č' => 'c', 'Ř' => 'r', 'Ž' => 'z',
              'Ý' => 'y', 'Á' => 'a', 'Í' => 'i', 'É' => 'e', 'Ů' => 'u',
              'Ú' => 'u', 'Ň' => 'n', 'Ď' => 'd', 'Ť' => 't', 
			  'ě' => 'e', 'š' => 's', 'č' => 'c', 'ř' => 'r', 'ž' => 'z', 
              'á' => 'a', 'í' => 'i', 'é' => 'e', 'ď' => 'd', 'ť' => 't',
              'ň' => 'n', 'ú' => 'u', 'ů' => 'u', 'ý' => 'y'
         );

         foreach($convertTable as $i=>$u) {
        	$text = mb_eregi_replace($i,$u,$text);
    		} 
    		
         //$result = strtolower(strtr($text, $convertTable));
         
         $result = Str_Replace(Array(" "), "_", $text); //nahradí mezery podtržítky
         
         if($full_str_replace)
            $result = Str_Replace(Array("(",")","!",",","\"","'","?","/",".","-","=",'+',']','[','}','{','*','@','#','$','%','^','&','?','!','|',',','/','>',"<",";","§","\\"), "", $result);
            else
            $result = Str_Replace(Array("(",")","!",",","\"","'","?","/"), "", $result); //odstraní nestandardní znaky ­ ()!,"'
         if($toLower) 
		 	$result = strtolower($result);
		 	
         return $result;
       }
       
       
    //prevede retezec na datum 
	function DateValue($value){
		$rezim = 0;
		
        $r = strtotime($value);
        if($r)
            return date('Y-m-d', $r);
            else
            return $r;
        
		if(preg_match("/[[:digit:]]{4,4}(\.){1,1}[[:digit:]]{1,2}(\.){1,1}[[:digit:]]{1,2}$/", $value)){
			$data = explode('.',$value);
			$rezim = 1;
			}
		elseif(preg_match("/[[:digit:]]{4,4}(\-){1,1}[[:digit:]]{1,2}(\-){1,1}[[:digit:]]{1,2}$/", $value)){
			$data = explode('-',$value);
			$rezim = 1;
			}
		elseif(preg_match("/[[:digit:]]{4,4}(\/){1,1}[[:digit:]]{1,2}(\/){1,1}[[:digit:]]{1,2}$/", $value)){
			$data = explode('/',$value);
			$rezim = 1;
			}
		elseif(preg_match("/[[:digit:]]{1,2}(\.){1,1}[[:digit:]]{1,2}(\.){1,1}[[:digit:]]{4,4}$/", $value)){
			$data = explode('.',$value);
			$rezim = 2;
			}
		elseif(preg_match("/[[:digit:]]{1,2}(\-){1,1}[[:digit:]]{1,2}(\-){1,1}[[:digit:]]{4,4}$/", $value)){
			$data = explode('-',$value);
			$rezim = 2;
			}
		elseif(preg_match("/[[:digit:]]{1,2}(\/){1,1}[[:digit:]]{1,2}(\/){1,1}[[:digit:]]{4,4}$/", $value)){
			$data = explode('/',$value);
			$rezim = 2;
			}
			
			
		if($rezim == 1){
			return implode('-',$data);
			
			}
		elseif($rezim == 2){
			return $data[2]."-".$data[1]."-".$data[0];
		}
		else return $value;
		
		
	}
    
    
    //prevede retezec na desetinne cislo
	function FloatValue($value) 
    {
        //$value = str_replace(array(' ',','), array("",""),$value);
        //return floatval($value);
        return floatval(preg_replace('#^([-]*[0-9\.,\' ]+?)((\.|,){1}([0-9-]{1,2}))*$#e', "str_replace(array('.', ',', \"'\", ' '), '.', '\\1') . '.\\4'", $value));
} 

//z data ve formatu DD.MM.RRRR vytvori retezec pro google analytics typu RRRR-MM-DD
	function GetGADate($date){
		$arr = explode('.', $date);
		if(count($arr)!=3) return '';
		$d = intval($arr[0]);
		$m = intval($arr[1]);
		$y = intval($arr[2]);
		
		if($d <= 31 && $d > 0 && $m < 13 && $m > 0 && $y < 2030 && $y > 2000){
			if(strlen($d)<2 && strlen($d)>0) $d = "0".$d;
			if(strlen($m)<2 && strlen($m)>0) $m = "0".$m;
			switch(strlen($y)){
				case 2: {$y = "20".$y; break;}
				case 3: {$y = "2".$y; break;}
				case 4: {break;}
				default: {return '';}
				}   
			} 
			else
			return '';
			
		$res = $y."-".$m."-".$d;	
		return $res;
		
	}
    
    
    //prekonvertuje retezec zaslany z google analytics ve formatu YYYYMMDD na format D. m Y (napr. 12. rijen 2009)
	//pokud je nastavena promenna convMonthToName na true, pak se cislo mesice prevede na jmeno mesice v cestine
	function ConvertGADateToRealDate($date, $convMonthToName=false){
		if(strlen($date)!=8) return '';
		$y = substr($date, 0, 4);
		$m = substr($date, 4, 2);
		$d = substr($date, 6, 2);
	
		if($convMonthToName)
			return intval($d).". ".GetMonthName($m,2)." ".$y;
			else
			return intval($d).".".intval($m).".".$y;
		
	}
	
 	//vstupem je datum ve formatu dd.mm.rrrr, vystupem je datum ve formatu rrrr-mm-dd
 	function GetUniDate($datum, $cas='')
    {

 		$vystup=false;
        $datum=trim($datum);
        if($datum=='' || $datum=='00.00.0000') return null;
        $rozdeleny=explode('.',$datum);
        if (count($rozdeleny)==3)
            {
            $den=intval($rozdeleny[0]);
            $mesic=intval($rozdeleny[1]);
            $rok=intval($rozdeleny[2]);
            if (($den>=1 and $den<=31) and ($mesic>=1 and $mesic<=12))
                {
                if (checkdate ($mesic,$den,$rok))
                    {
                    if ($den<10)
                        $den='0'.$den;
                    if ($mesic<10)
                        $mesic='0'.$mesic;
                    $vystup=$rok.'-'.$mesic.'-'.$den;
                    }
                }
                
            //zpracovani casu
            if($vystup != "" && $cas != "")
                {
                    $rozdeleny_cas = explode(":",$cas);
                    if(in_array(count($rozdeleny_cas),array(2,3)))
                    {
                        $h = intval($rozdeleny_cas[0]);
                        $m = intval($rozdeleny_cas[1]);
                        $s = isset($rozdeleny_cas[2]) ? intval($rozdeleny_cas[2]) : 0;
                        if($h >= 0 && $h < 24 && $m >= 0 && $m < 60 && $s >= 0 && $s < 60)
                        {
                            $h = $h < 10 ? "0".$h : $h;
                            $m = $m < 10 ? "0".$m : $m;
                            $s = $s < 10 ? "0".$s : $s;
                            $vystup .= " ".$h.":".$m.":".$s;
                            
                        }
                    }
                }
                
            }
    return $vystup;
		
 	}
    
    
    function getRemoteFileCurl($url){
	
    	if (function_exists('curl_init')) {
       // initialize a new curl resource
       $ch = curl_init();
    
       // set the url to fetch
       curl_setopt($ch, CURLOPT_URL, $url);
    
       // don't give me the headers just the content
       curl_setopt($ch, CURLOPT_HEADER, 0);
    
       // return the value instead of printing the response to browser
       curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    
       // use a user agent to mimic a browser
       curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.7.5) Gecko/20041107 Firefox/1.0');
    
       $content = curl_exec($ch);
    
       // remember to always close the session and free all resources
       curl_close($ch);
       return $content;
    	} 
    
       return '';	
    	
    }


    function ftp_get_file($path)
    {
        
        return getRemoteFileCurl("ftp://".FTP_LOGIN.":".FTP_PASSWORD."@".FTP_HOST.":".FTP_PORT."".$path);
    }

    /*
    function ftp_delete_data_files($file_names)
{
    $ftp_server = (FTP_CONNECTION_TYPE == "test") ? FTP_CONNECTION_FTP_SERVER_TEST : FTP_CONNECTION_FTP_SERVER_LIVE;

    // for each file xml
    foreach ($file_names as $file => $value) {
        // skip if false
        if($file == false) continue;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "ftp://$ftp_server/" . FTP_DIR);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_USERPWD, FTP_CONNECTION_USERNAME . ":" . FTP_CONNECTION_PASS);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_QUOTE, array('DELE /' . FTP_DIR."/".$file.'.xml'));
        $result = curl_exec($ch);
        $error_no = curl_errno($ch);
        curl_close($ch);
        if ($error_no == 0) {
            log_tracking('', $file, 'DELETE success', 'OK', 'DELETE XML', '');
        } else {
            log_tracking('', $file, 'DELETE error', 'ERROR', 'DELETE XML', '');
        }
    }
}
*/


    
   //vraci mime typ vzdaleneho souboru
function GetMimeType($url){
	$result="";
	
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_HEADER, 1);
	curl_setopt($ch, CURLOPT_NOBODY, 1);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	
	$results = preg_split("/\n/", trim(curl_exec($ch)));
	foreach($results as $line) {
	    if (strtok($line, ':') == 'Content-Type') {
	    	$parts = explode(":", $line);
	        $result = trim($parts[1]);
      		}
		}
	if($result!=''){
		$data = explode(';',$result);
		$result = trim($data[0]);
		
		}
	return $result;

	}
    
  //nahrada za funkci str_getcsv	
function str_getcsv_alt($input, $delimiter = ',', $enclosure = '"', $escape = '\\', $eol = '\n') {
        if (is_string($input) && !empty($input)) {
            $output = array();
            $tmp    = preg_split("/".$eol."/",$input);
            if (is_array($tmp) && !empty($tmp)) {
                while (list($line_num, $line) = each($tmp)) {
                    if (preg_match("/".$escape.$enclosure."/",$line)) {
                        while ($strlen = strlen($line)) {
                            $pos_delimiter       = strpos($line,$delimiter);
                            $pos_enclosure_start = strpos($line,$enclosure);
                            if (
                                is_int($pos_delimiter) && is_int($pos_enclosure_start)
                                && ($pos_enclosure_start < $pos_delimiter)
                                ) {
                                $enclosed_str = substr($line,1);
                                $pos_enclosure_end = strpos($enclosed_str,$enclosure);
                                $enclosed_str = substr($enclosed_str,0,$pos_enclosure_end);
                                $output[$line_num][] = $enclosed_str;
                                $offset = $pos_enclosure_end+3;
                            } else {
                                if (empty($pos_delimiter) && empty($pos_enclosure_start)) {
                                    $output[$line_num][] = substr($line,0);
                                    $offset = strlen($line);
                                } else {
                                    $output[$line_num][] = substr($line,0,$pos_delimiter);
                                    $offset = (
                                                !empty($pos_enclosure_start)
                                                && ($pos_enclosure_start < $pos_delimiter)
                                                )
                                                ?$pos_enclosure_start
                                                :$pos_delimiter+1;
                                }
                            }
                            $line = substr($line,$offset);
                        }
                    } else {
                        $line = preg_split("/".$delimiter."/",$line);
   
                        /*
                         * Validating against pesky extra line breaks creating false rows.
                         */
                        if (is_array($line) && !empty($line[0])) {
                            $output[$line_num] = $line;
                        } 
                    }
                }
                return $output;
            } else {
                return false;
            }
        } else {
            return false;
        }
    } 
    

   




function help($idNapovedy=0, $get_content=false){
    
    
    if($idNapovedy==0) return "";
    
    if($get_content){
        global $db;
        $data = $db->Query("SELECT ".CMS_LANG." AS obsah FROM ".TABLE_NAPOVEDA." WHERE idNapovedy=".$idNapovedy." LIMIT 1");
        if($db->numRows($data)<=0) return "";
        $napoveda = $db->getAssoc($data);
        return $napoveda['obsah']; 
        
        }
        else
        //return "<a href='".htmlspecialchars(AJAX_GATEWAY."qtip&id=".$idNapovedy)."' class='jtip' id='qt".$idNapovedy."' title='čekejte prosím'>".icon('help')."</a>";
        return "<span class='jtip fa fa-question' id='qt".$idNapovedy."' title=''></span>";
}


function GetGraphWidth($hloubka, $pocet_mezer = 5, $iterace=0){
    if($iterace == 0)
        $iterace = $hloubka;
        
    $width = $hloubka * 7;
    $res = array();
    $res['graf'] = "<div class='hloubka hloubka".$hloubka."'>".$hloubka."</div>";
    $res['mezera'] = "";
    if($hloubka==0) return $res;
    for($i=0;$i<$iterace;$i++)
        $res['mezera'] .= str_repeat("&nbsp;",$pocet_mezer);
    $res['mezera'] .= "";
    return $res;
}


function sucfirst($string) {
    $encoding = "utf-8";
    $string = mb_strtoupper(mb_substr($string, 0, 1, $encoding), $encoding) . mb_substr($string, 1,mb_strlen($string,$encoding), $encoding);
    return $string;
    }

function slcfirst($string) {
    $encoding = "utf-8";
    $string = mb_strtolower(mb_substr($string, 0, 1, $encoding), $encoding) . mb_substr($string, 1,mb_strlen($string,$encoding), $encoding);
    return $string;
    }
 
//stripos pro php<5
if (!function_exists("stripos")) {
	function stripos($haystack, $needle){
    	return strpos($haystack, stristr( $haystack, $needle ));
	}
}


function GetMaxRecords($start, $end, $typ=0){
    $s = explode('-', $start);
    $e = explode('-', $end);
    
    if($typ == 0) return 24;
    elseif($typ == 1){
        $ss = $s[0] * 12 * 31 + $s[1] * 31 + $s[2]; 
        $es = $e[0] * 12 * 31 + $e[1] * 31 + $e[2]; 
        return $es - $ss + 1;
        }
    elseif($typ == 2){
        $ss = $s[0] * 12 + $s[1];
        $es = $e[0] * 12 + $e[1];
        return $es - $ss + 1;
        }  
    elseif($typ == 3){
        $ss = $s[0];
        $es = $e[0];
        return $es - $ss + 1;
        }
    elseif($typ == 4)
        return 10;
        
    return 0;   
    
}

//prevede domenu na tvar bez www
	function GetDomainWithoutWww($domainName){
		$domainName = strtolower(trim($domainName));
		
		if($domainName=='') 
			return false;
			
		$data = explode('.', $domainName);
		
		if($data[0]=='www')
			$domainName = substr($domainName, 4, strlen($domainName));
			
		return $domainName;
	}
	
	//prevede domenu na tvar s www
	function GetDomainWithWww($domainName){
		$domainName = strtolower(trim($domainName));
		
		if($domainName=='') 
			return false;
			
		$data = explode('.', $domainName);
		
		if($data[0]!='www')
			return 'www.'.$domainName;
			
		return $domainName;
		
	}
    
    function getDirByParam($id, $domain=""){
		$resId = '';
		if($id >= 1000 ) $resId = $id; 
		if($id < 1000 ) $resId = "0".$id; 
		if($id < 100 ) $resId = "00".$id; 
		if($id < 10 ) $resId = "000".$id;  
        if($domain == "")
            $dir = $resId."/";
            else
            {
            $name = mb_eregi_replace("\.","_",$domain);
            $dir = $resId."-".$name."/";
            }
		
		return $dir;
		
	}
    
    function createDirTreeByParam($id, $domain=""){

        $ok = true;	
		$chmod = true;
        
        $root = DIRNAME_DOMAINS.getDirByParam($id, $domain);

        $adresare = array(
            "",
            USER_DIRNAME_JS,
            USER_DIRNAME_CSS,
            USER_DIRNAME_CSS.USER_DIRNAME_IMG,
            USER_DIRNAME_IMG,
            USER_DIRNAME_GALLERY,
            USER_DIRNAME_GALLERY_MINI,
            USER_DIRNAME_GALLERY_MAXI,
            USER_DIRNAME_GALLERY_STREDNI,
            USER_DIRNAME_CLASSES,
            USER_DIRNAME_ATTACHMENT,
            USER_DIRNAME_NEWS,
            USER_DIRNAME_CALENDAR,
            USER_DIRNAME_ARTICLES,
            USER_DIRNAME_ARTICLES_MINI,
            USER_DIRNAME_ARTICLES_MAXI
            );
            
        foreach($adresare AS $a) 
        {
            $dir = $root.$a;
            $dir = substr($dir, 0, -1);
            if($ok && !file_exists($dir)) 
		  	   $ok = mkdir($dir, 0777);
               
            if($chmod) chmod($dir, 0777);
        }   	
  
                
		//vytvoreni slozek pro tinymce
		
		/*
		if($ok && !file_exists($root."/useruploads")) 
			$ok = mkdir($root."/useruploads", 0777);
			
		if($chmod) chmod($root."/useruploads", 0777);
		
		
		if($ok && !file_exists($root."/useruploads/files")) 
			$ok = mkdir($root."/useruploads/files", 0777);
			
		if($chmod) chmod($root."/useruploads/files", 0777);
		
		if($ok && !file_exists($root."/useruploads/images")) 
			$ok = mkdir($root."/useruploads/images", 0777);
			
		if($chmod) chmod($root."/useruploads/images", 0777);
		
		if($ok && !file_exists($root."/useruploads/media")) 
			$ok = mkdir($root."/useruploads/media", 0777);
			
		if($chmod) chmod($root."/useruploads/media", 0777);
		*/
	
		return $ok;		
	}
   
//vraci url adresu modulu
function get_link($module_name, $page = "", $id_page = 0, $action = "", $id_action = 0, $value="",$weblang=0,$absolute_base_url="", $kotva="")
{
        $url_parts = array();
        if($page!='')
        {
            $url_parts[] = "page=".$page;
            if($id_page > 0)
                $url_parts[] = "id_page=".$id_page;
        }
        
        if($action!='')
        {
            $url_parts[] = "action=".$action;
            if($id_action > 0)
                $url_parts[] = "id_action=".$id_action;
        }
        
        if($value!='')
        {
            $url_parts[] = "value=".$value;
        }
        
        if($weblang == 0)
            $url_parts[] = "weblang=".WEB_LANG_ID;
            else
            $url_parts[] = "weblang=".$weblang;
        
        
        $url = count($url_parts) > 0 ? "?".implode("&",$url_parts) : "";
    
    $base_url = $absolute_base_url == "" ? RELATIVE_PATH : $absolute_base_url;
    return $base_url.ADMIN_SCRIPT."/".$module_name.$url.$kotva;
}


//vraci url adresu modulu
function get_link_ajax($module_name, $action, $module_id=0)
{
        return str_replace(array("MODULE_NAME","MODULE_ID"),array($module_name, $module_id),AJAX_GATEWAY_TEMPLATE).$action;
}


//vraci pole s prumernou navstevnosti a celkovou navstevnosti a poctem sledovanych dni
//vstupem jsou data z google analytics, interval definovany od, do, format je RRRRMMDD
function navstevnost(& $data, $od,$do){
    
    $suma = 0;
    $prumer = 0;
    $pocet_dni = 0;
      
    
    foreach($data AS $datum => $navstevy){
        if($datum >= $od && $datum <= $do){
            $suma += $navstevy;
            $pocet_dni++;
            }
        
        }
    
    if($pocet_dni>0)
        $prumer = round($suma / $pocet_dni , 2);
        
    return array('suma'=>$suma, 'pocet'=>$pocet_dni, 'prumer'=>$prumer); 
    
    
    
}

//funkce vraci procentni rozdil
function rozdil_procent($navstevnost_minula, $navstevnost_soucasna, $html=true){
    
    $rozdil = 0;
    
    if($navstevnost_minula > 0)
        $rozdil = round((( 100 / $navstevnost_minula ) * $navstevnost_soucasna) - 100, 2);
        else
        return "";
    
    if($html){
        
        if($rozdil<0)
            $rozdil = "pokles o <span style='color:red'> ".$rozdil." %</span>";
        elseif($rozdil>=0)
            $rozdil = "nárust o <span style='color:green'> +".$rozdil." %</span>";
        else $rozdil = "";       
        }
    
    return $rozdil;
       
}

//overi zda se termin neprekryva s terminem jine akce
function over_terminy($id_akce, $datum_od, $datum_do, $id_termin = 0)
    {
        global $db;

        $data = $db->query("SELECT idTerminu 
            FROM ".TABLE_KALENDAR_TERMINY." 
            WHERE (od = '".$datum_od."' 
                OR ((('".$datum_od."' BETWEEN od AND do) OR ('".$datum_do."' BETWEEN od AND do))  
                    AND do IS NOT NULL
                    )
                OR ((od BETWEEN '".$datum_od."' AND '".$datum_do."') 
                    AND '".$datum_do."' != ''
                    AND od != ''
                    AND od != '0000-00-00'
                    )
                OR ((od = '0000-00-00' OR od = '' OR od IS NULL) AND od = '".$datum_od."')
                )
                
                AND idAkce = ".$id_akce."
                AND idTerminu != '".$id_termin."'
            LIMIT 1
            ");
        
        if($db->numRows($data) > 0)
        {
            $t = $db->getAssoc($data);
            return $t['idTerminu'];
        }
        
        return 0;
        
    }


function get_log_type($typ_stranky)
{
    if(in_array($typ_stranky, array('stranka','novinka','clanek','galerie','galerie-slozka','galerie-fotka','akce','kalendar-typ','produkt')))
        return $typ_stranky;
    
    return 'stranka';
}


//overi zda lze priradit strance pozadovany typ ci nikoliv, vymaze nebo vytvori pozadovany blok do obsahu stranky
function CreateTypePage($idStranky, $novy_typ, $stary_typ=''){
    global $domain;
    global $db;
    global $systemMessage;
    
    if($stary_typ == $novy_typ) return;
        
    if($stary_typ=='novinky'){
        //pokud stranka prechazi z typu novinky, overi zda existuje nejaka novinka
        $d = $db->Query("SELECT idStranky FROM ".TABLE_STRANKY." WHERE typ='novinka' AND idRodice=".$idStranky." LIMIT 1");
        if($db->numRows($d)>0) {
            //pokud novinka existuje pak neni mozno prejit na novy typ
            $systemMessage->add_warning(ERROR_STRANKA_OBSAHUJE_NOVINKY_NELZE_PREVEST_NA_JINY_TYP);
            $db->Query("UPDATE ".TABLE_STRANKY." SET typ='".($stary_typ==''?'stranka':$stary_typ)."' WHERE idStranky=".$idStranky." LIMIT 1");
            return;
            }
        }
        
    if($stary_typ=='clanky'){
        $d = $db->Query("SELECT idStranky FROM ".TABLE_STRANKY." WHERE typ='clanek' AND idRodice=".$idStranky." LIMIT 1");
        if($db->numRows($d)>0) {
            $systemMessage->add_warning(ERROR_STRANKA_OBSAHUJE_CLANKY_NELZE_PREVEST_NA_JINY_TYP);
            $db->Query("UPDATE ".TABLE_STRANKY." SET typ='".($stary_typ==''?'stranka':$stary_typ)."' WHERE idStranky=".$idStranky." LIMIT 1");
            return;
            }    
        }
    
    if($stary_typ=='produkty'){
        $d = $db->Query("SELECT idStranky FROM ".TABLE_STRANKY." WHERE typ='produkt' AND idRodice=".$idStranky." LIMIT 1");
        if($db->numRows($d)>0) {
            $systemMessage->add_warning(ERROR_STRANKA_OBSAHUJE_PRODUKTY_NELZE_PREVEST_NA_JINY_TYP);
            $db->Query("UPDATE ".TABLE_STRANKY." SET typ='".($stary_typ==''?'stranka':$stary_typ)."' WHERE idStranky=".$idStranky." LIMIT 1");
            return;
            }    
        }
        
    if($stary_typ=='stranka-galerie'){
        $d = $db->Query("SELECT idStranky FROM ".TABLE_STRANKY." WHERE (typ='galerie' OR typ='galerie-slozka') AND idRodice=".$idStranky." LIMIT 1");
        if($db->numRows($d)>0) {
            $systemMessage->add_warning(ERROR_STRANKA_OBSAHUJE_GALERIE_NELZE_PREVEST_NA_JINY_TYP);
            $db->Query("UPDATE ".TABLE_STRANKY." SET typ='".($stary_typ==''?'stranka':$stary_typ)."' WHERE idStranky=".$idStranky." LIMIT 1");
            return;
            }    
        }    
    }

?>