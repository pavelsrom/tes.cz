<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

//-------------------------------------------------------------------------------
/* FUNKCE PRO TEXTOVANI A VYKRESLOVANI INPUTU FORMULARE */
//-------------------------------------------------------------------------------


//test delky retezce
function TestLength($string, $maxLength = 200, $empty = true)
{
		$string = Trim($string);
		$delka = mb_strlen($string);
		if(!$empty && $string=='') return false;
		if($empty && $string=='') return true;
		if($delka > $maxLength) return false; 
		return true;
}


//test omezeni delky retezce bez diakritiky
function TestStringEN($string, $maxLength = 200, $empty = true)
{
		$string = Trim($string);
		if(!$empty && $string=='') return false;
		if($empty && $string=='') return true;
		
		if(!TestLength($string, $maxLength, $empty)) return false;
		$maska = '/^([[:alnum:]+]{1,'.$maxLength.'})/';
		if(preg_match($maska ,$string)) return true; else return false;	
		
	}

//test delky a struktury retezce typu login
function TestStringLoginEN($string, $maxLength = 200, $empty = true)
{
		$string = Trim($string);
		if(!$empty && $string=='') return false;
		if($empty && $string=='') return true;
		
		if(!TestLength($string, $maxLength, $empty)) return false;
		$maska = '/^([[:alnum:]]+[-_]*[[:alnum:]]+)/';
		if(preg_match($maska ,$string)) return true; else return false;
		}

//test retezce s ceskou diakritikou
function TestStringCZ($string, $maxLength = 200, $empty = true)
{
		$string = Trim($string);
		if(!$empty && $string=='') return false;
		if($empty && $string=='') return true;
		if(!TestLength($string, $maxLength, $empty)) return false;
		$maska = "/^([ěščřžýáíéóůúťďň]|[ĚŠČŘŽÝÁÍÉÓŮÚŤĎŇ]|[[:alpha:]])+/";
		if(preg_match($maska,$string)) return true; else return false;
    	}
        
//testuje retezec typu datetime, napr. 20.5.1970 20:00
function TestDateTime($string)
{
		$maska = "/^[0-9]{1,2}\.[0-9]{1,2}\.[0-9]{4}(\s)*([0-9]{1,2}\:[0-9]{1,2})?$/";
		if(preg_match($maska,$string)) return true; else return false;
	}
    
//test vety s ceskou diakritikou
function TestSentenceCZ($string, $maxLength = 200, $empty = true)
{
		$string = Trim($string);

		if(!$empty && $string=='') return false;
		if($empty && $string=='') return true;
		
		if(!TestLength($string, $maxLength, $empty)) return false;
		$maska = "/^(([ěščřžýáíéóůúťďň]|[ĚŠČŘŽÝÁÍÉÓŮÚŤĎŇ]|[[:alnum:]])*[[:blank:]]?)*/";
		if(preg_match($maska,$string)) return true; else return false;
	}  


//testuje retezec typu float
function TestFloat($string, $maxLength = 10, $empty = true)
{
		$string = Trim($string);
		if(!TestLength($string, $maxLength, $empty)) return false;
		$maska = "/^([[:digit:]]*)[\.]{0,1}([[:digit:]]*)/";
		if(preg_match($maska ,$string)) return true; else return false;	
		
	}


//test celeho cisla
function TestInt($string, $maxLength = 10, $empty = true)
{
		$string = Trim($string);
		if($string=='' && $empty) return true;
		if(!TestLength($string, $maxLength, $empty)) return false;
		if(ctype_digit($string)) return true; else return false;	
			
	}
    
    
//test ip adresy
function TestIp($string, $empty=true)
    {
        $string = trim($string);
        if(!$empty && $string=='') return false;
		if($empty && $string=='') return true;
		if(!TestLength($string, 15, $empty)) return false;

        $maska = "/^(\d{1,3}\.){0,3}((\d){1,3}|\*{1})$/";
        if(preg_match($maska ,$string)) return true; else return false;
    }

function TestAdress($ulice, $cp, $mesto, $psc)
{
	return mb_strlen($cp) != 0 && mb_strlen(str_replace(" ", "", $psc)) == 5 && mb_strlen($mesto) > 1;

}


//test emailove adresy
function TestEmail($string, $maxLength = 200, $empty=true){
		$string = Trim($string);
		if(!$empty && $string=='') return false;
		if($empty && $string=='') return true;
        
        if($maxLength > 0 && !TestLength($string, $maxLength, $empty))
		   return false;
          
		$maska = '/^(([[:alnum:]]+[\-\.\_]?)+[[:alnum:]]+)@(([[:alnum:]]+[\-\.\_]?)+[[:alnum:]]+)\.[[:alpha:]]{2,10}$/';
		if(preg_match($maska ,$string)) return true; else return false;
	}

//test domeny vcetne i bez www
function TestDomain($string){
		$string = trim($string);
		$maska = "/^([[w]{3}\.])?[[:alnum:]]+([-_\.]?[[:alnum:]])*\.[[:alpha:]]{2,4}/";
		if(preg_match($maska, $string) || $string=='localhost') return true; else return false;
		
	}

//test hodnoty formatu data
function TestDate($date, $empty = true){
		$date = trim($date);
		if(!$empty && $date=='') return false;
		if($empty && $date=='') return true;
		if($date=='00.00.0000') return false;
		
		$rezim = 0;
		
		$value = $date;
		if(preg_match("/[[:digit:]]{4,4}(\.){1,1}[[:digit:]]{1,2}(\.){1,1}[[:digit:]]{1,2}$/", $value)){
			$d = explode('.',$value);
			$rezim = 1;
			}
		elseif(preg_match("/[[:digit:]]{4,4}(\-){1,1}[[:digit:]]{1,2}(\-){1,1}[[:digit:]]{1,2}$/", $value)){
			$d = explode('-',$value);
			$rezim = 1;
			}
		elseif(preg_match("/[[:digit:]]{4,4}(\/){1,1}[[:digit:]]{1,2}(\/){1,1}[[:digit:]]{1,2}$/", $value)){
			$d = explode('/',$value);
			$rezim = 1;
			}
		elseif(preg_match("/[[:digit:]]{1,2}(\.){1,1}[[:digit:]]{1,2}(\.){1,1}[[:digit:]]{4,4}$/", $value)){
			$d = explode('.',$value);
			$rezim = 2;
			}
		elseif(preg_match("/[[:digit:]]{1,2}(\-){1,1}[[:digit:]]{1,2}(\-){1,1}[[:digit:]]{4,4}$/", $value)){
			$d = explode('-',$value);
			$rezim = 2;
			}
		elseif(preg_match("/[[:digit:]]{1,2}(\/){1,1}[[:digit:]]{1,2}(\/){1,1}[[:digit:]]{4,4}$/", $value)){
			$d = explode('/',$value);
			$rezim = 2;
			}
			

		if($rezim == 1 && count($d)==3){
			
			if(checkdate($d[1],$d[2],$d[0]))
				return true;
				else
				return false;
			
			}
		elseif($rezim == 2 && count($d)==3){
			if(checkdate($d[1],$d[0],$d[2]))
				return true;
				else
				return false;
		}
		else return false;

		}
        
        
        
//test formatu telefonniho cisla        
function TestPhone($string, $empty = true)
{
		
		$string = Trim($string);
		if(!$empty && $string=='') return false;
		if($empty && $string=='') return true;
		$string = str_replace(' ', '', $string);
		$maska = '/^[+]?[[:digit:]]{9,15}$/';
		if(preg_match($maska ,$string)) 
			return true; 
			else 
			return false;	
	}	       
        
        
//testuje zda dve data jsou logicky spravne, tzn. jestli datum od neni vetsi nez datum do a nebo pokud datum do není menší než dnešní datum	
function TestTwoDate($od, $do)
{
		$od = trim($od);
		$do = trim($do);
		
		if($od!=''){
			$d1 = explode('.',$od);
			if(count($d1)==3) 
				$s1 = intval($d1[2].$d1[1].$d1[0]);
				else
				$s1 = 0;
			}
			else
			$s1 = 0;
		
		if($do!=''){
			$d2 = explode('.',$do);
			if(count($d2)==3)
				$s2 = intval($d2[2].$d2[1].$d2[0]);
				else
				$s2 = 0;
			}
			else
			$s2 = 0;
            
		if($s1 > 0 && $s2 > 0)
			if($s1 > $s2) return false;
			
		
		if($s2 > 0){
			$now = date('Ymd');
            
			if($s2 < $now) return false;
		}
	
		
		return true;
	
	}        
        
        
//test formatu icq cisla  
function TestICQFormat($string, $maxLength = 11, $empty = true){
		$string = Trim($string);
		$delka = strlen($string);
		if(!$empty && $string=='') return false;
		if($empty && $string=='') return true;
		if($delka > $maxLength) {
			return false;
			
		}
		$maska = '/^([[:digit:]]{1,3})[\-]?([[:digit:]]{1,3})[\-]?([[:digit:]]{1,3})/';
		if(preg_match($maska ,$string)) return true; else return false;	
		
	}
    
   
//testuje format ico  
function TestIco($string, $empty = true){
		$string = Trim($string);
		if(!$empty && $string=='') return false;
		if($empty && $string=='') return true;
		$string = str_replace(' ', '', $string);
		$maska = '/^[[:digit:]]{8}$/';
		if(preg_match($maska ,$string)) return true; else return false;	
		
	}        
        
        
        
//test formatu DIC        
function TestDic($string, $maxLength = 200, $empty = true){
		$string = Trim($string);
		$delka = strlen($string);
		if(!$empty && $string=='') return false;
		if($empty && $string=='') return true;
		if($delka > $maxLength) {
			return false;
		}
		$string = str_replace(' ', '', $string);
		$maska = '/^[[:alpha:]]{2}[[:blank:]]?[[:digit:]]{8,'.$maxLength.'}$/';
		if(preg_match($maska ,$string)) return true; else return false;	
		
	}        
        
        
//test formatu url stranky        
function TestUrlPage($string){
		$string = trim($string);
		$delka = strlen($string);
		$maska = "/^([[:alnum:]]|[-_])*/";
		if(preg_match($maska ,$string)) return true; else return false;
	}
    
    
//test formatu absolutni url adresy
function TestHttpUrl($url){
		$url = trim($url);
		$maska = "/^http:\/\/[[:alnum:]]+([-_\.]?[[:alnum:]])*\.[[:alpha:]]{2,6}/";
		$maska_https = "/^https:\/\/[[:alnum:]]+([-_\.]?[[:alnum:]])*\.[[:alpha:]]{2,6}/";
		$maska2 = "/^http:\/\/localhost/";
		if(preg_match($maska ,$url)) return true;
		if(preg_match($maska_https ,$url)) return true;
		if(preg_match($maska2 ,$url)) return true;
		return false;
	}    
  
  
//test formatu url obrazku
function TestHttpUrlImage($url){
		if(!TestHttpUrl($url)) return false;
		$casti_url = explode('.', $url);
		$pocet_casti = count($casti_url);
		$posledni_cast = strtolower($casti_url[$pocet_casti-1]);
		if($posledni_cast == 'jpg' || $posledni_cast == 'gif' || $posledni_cast == 'png' || $posledni_cast == 'jpeg' || $posledni_cast == 'bmp') return true; else return false;
	}






           
?>