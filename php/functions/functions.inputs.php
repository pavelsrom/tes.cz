<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */


function get_navigation($id_stranka, $nav = array())
{
    global $db;
    global $domain;
    
    if($id_stranka == 0)
        return $nav;
    
    $s = $db->get(TABLE_STRANKY,array("idRodice, nazev"),"idStranky=".$id_stranka." AND idDomeny=".$domain->getId());    
    $nav[$id_stranka] = $s->nazev; 
    
    $nav = get_navigation($s->idRodice, $nav);
    
    return $nav;
    
}



//vypise wysiwyg editor TinyMCE
function wysiwygEditor($name='', $content='', $class='wysiwygEditor w100 h500', $id='wysiwygEditor',$template_admin=0, $show_we_tools = true)
{
		
        $content = htmlspecialchars($content);
        
        if($show_we_tools)
        {
            $wysiwygEditorId = $id;
            include("php/classes/wysiwygEditorTools/wet.php");
        }
            //$this->we_tools($template_admin);
        
        
        if($id != '')
            $id = "id='".$id."'";
       
       /*
		echo "<noscript>
  			<div class='error'><ul><li>Povolte pros�m Javascript!!! N�kter� funkce redak�n�ho syst�mu nejsou k dispozici.</li></ul></div></noscript>";
  		*/
        echo "<textarea class='".$class."' ".$id." name='".$name."' rows='' cols='' >".$content."</textarea>";  	
        return;   
        
    }


function ShowSelectBoxToString($name, $items, $prTyp, $emptyItem=false, $onChangeFunction = '', $id='', $other='', $class='w100'){
		
		$html = "";
		
		if($onChangeFunction != '') $onChangeFunction = "onchange='".$onChangeFunction."'"; 
		
		$html .= "<select name='".$name."' size='1' class='select ".$class."' ".($id!=''?"id='".$id."'":'')." ".$other." ".$onChangeFunction.">\n";
		
		if($emptyItem) $html .=  "<option value=''>-- ".TVYBERTE." --</option>\n";
		$prTyp = ''.$prTyp.'';	
		
		$sel = true;
		
		foreach($items as $i => $v){
			if($i == $prTyp && $sel){ 
				$html .=  "<option value='".($i)."' selected='selected'>".secureString($v)."</option>\n";
				$sel = false;
				} 
				else 
				$html .=  "<option value='".($i)."'>".secureString($v)."</option>\n";		
			}
		$html .=  "</select>";
		
		return $html; 
	}


//vraci galerie pro editaci galerie a slozky galerie
//ve stromu se neobevi vetev, ktera obsahuje zadane id stranky
function get_gallery($id_stranky_zakazane = 0)
{

    global $db;
    global $domain;
    
    $povolene_typy = array('galerie-slozka');   
    $result = array();

    function gallery_tree($id_rodice, & $result, &$povolene_typy)
    {
        global $db;
        global $domain;
        
        $data = $db->query("SELECT idStranky AS id, nazev, idRodice AS rodic FROM ".TABLE_STRANKY." WHERE idDomeny=".$domain->getId()." AND idJazyka=".WEB_LANG_ID." AND idStranky=".$id_rodice." AND typ='galerie-slozka'");
        
        
        while($s = $db->getAssoc($data))
        {
            $id = $s['id'];
            $result[$id] = $s['nazev'];
            
            gallery_tree($s['rodic'], $result, $povolene_typy);
               
        }
        
        return;
    }
    
    $root = unserialize(ID_STRANKY_GALERIE);
    $id_rodice = isset($root[1]) && $root[1] > 0 ? intval($root[1]) : 0;
    $data = $db->query("SELECT idStranky AS id, nazev, idRodice AS rodic FROM ".TABLE_STRANKY." WHERE idDomeny=".$domain->getId()." AND idJazyka=".WEB_LANG_ID." AND idStranky!=".$id_stranky_zakazane."  AND typ='galerie-slozka'");
    
    while($s = $db->getAssoc($data))
    {
        $id = $s['id'];
        $result[$id] = array($id => $s['nazev']); 
        gallery_tree($s['rodic'], $result[$id], $povolene_typy);
         
    }  
   
    foreach($result AS $idr => $r)
    {
        if(isset($r[$id_stranky_zakazane]))
        {
            unset($result[$idr]);
            continue;
        }
            
        $result[$idr] = implode(" -> ", array_reverse($r));
    }
    
    asort($result);
    return $result;
    
    

}



//vraci stranky pro editaci stranky
//ve stromu se neobevi vetev, ktera obsahuje zadane id stranky
function get_pages($id_stranky_zakazane = 0, $id_jazyk = WEB_LANG_ID)
{

    global $db;
    global $domain;
        
    $result = array();
    
    $povolene_typy = array('helpdesk','vyhledavani','stranka','sitemap','novinky','stranka-galerie','prazdna-stranka','clanky','kalendar-akci','tagy','autori','archiv-clanku','login','email','obchodni-podminky','objednavka');
   
    if(!function_exists('pages_tree'))
    {
    
        function pages_tree($id_rodice, & $result, $id_jazyk, & $povolene_typy)
        {
            global $db;
            global $domain;
            
            $data = $db->query("SELECT idStranky AS id, nazev, idRodice AS rodic, typ 
                FROM ".TABLE_STRANKY." 
                WHERE idDomeny=".$domain->getId()." 
                    AND idJazyka=".$id_jazyk." 
                    AND idStranky=".$id_rodice." 
                    ");
            
            //echo $db->numRows($data)." ";
            
            
            while($s = $db->getAssoc($data))
            {
                if(!in_array($s['typ'],$povolene_typy))
                    continue;
                
                $id = $s['id'];
                $result[$id] = $s['nazev'];
                pages_tree($s['rodic'], $result, $id_jazyk, $povolene_typy);
                   
            }
            
            //$db->free($data);
            
            return;
        }
    }
    
    $data = $db->query("SELECT idStranky AS id, nazev, idRodice AS rodic, typ 
                FROM ".TABLE_STRANKY." 
                WHERE idDomeny=".$domain->getId()."
                    AND idJazyka=".$id_jazyk." 
                    AND idStranky!=".$id_stranky_zakazane."
                ORDER BY priorita, idStranky
                    ");
              
    //echo $db->numRows($data). " ";
                    
    while($s = $db->getAssoc($data))
    {
        if(!in_array($s['typ'],$povolene_typy))
            continue;
        
        $id = $s['id'];
        $result[$id] = array($id => $s['nazev']); 
        pages_tree($s['rodic'], $result[$id],$id_jazyk,$povolene_typy);
       
    }  
    
    //$db->free($data);

    //print_r($result);    

   
    foreach($result AS $idr => $r)
    {
        
        if(isset($r[$id_stranky_zakazane]))
        {
            
            unset($result[$idr]);
            continue;
            
        }
            
        $result[$idr] = implode(" -> ",  array_reverse($r));
    }
    
    asort($result);
    /*
    $result_array = array(
        "stranky" => $result, 
        "disabled" => $disabled
        );
    
    return $result_array;
    */
    
    return $result;
    

}


//vraci stranky pro selectbox stranek v ckeditoru
//ve stromu se neobevi vetev, ktera obsahuje zadane id stranky
function get_pages_for_editor()
{

    global $db;
    global $domain;
    global $links;
    
    $result = array();
    
    $povolene_typy = array('helpdesk','uvodni','vyhledavani','stranka','sitemap','novinky','stranka-galerie','prazdna-stranka','clanky','clanek');

    function pages_tree1($id_rodice, & $result, $povolene_typy)
    {
        global $db;
        global $domain;
        
        $data = $db->query("SELECT idStranky AS id, nazev, idRodice AS rodic, typ 
                FROM ".TABLE_STRANKY." 
                WHERE idDomeny=".$domain->getId()." 
                    AND idJazyka=".WEB_LANG_ID." 
                    AND idStranky=".$id_rodice." 
                    ");
        
        while($s = $db->getAssoc($data))
        {
            $id = $s['id'];
            $result[$id] = $s['nazev'];
            pages_tree1($s['rodic'], $result, $povolene_typy);
               
        }
        
        return;
    }
    
    $data = $db->query("SELECT idStranky AS id, nazev, idRodice AS rodic, typ 
                FROM ".TABLE_STRANKY." 
                WHERE idDomeny=".$domain->getId()." 
                    AND idJazyka=".WEB_LANG_ID." 
                    ");
    
    while($s = $db->getAssoc($data))
    {
        
        if(!in_array($s['typ'],$povolene_typy))
            continue;
        
        $id = $s['id'];
        $result[$id] = array($id => $s['nazev']); 
        pages_tree1($s['rodic'], $result[$id], $povolene_typy);
               
    }  
   
    //$pages_list_data = array();
    $pages_list = array(array("-- ".TVYBERTE." --",""));

    foreach($result AS $idr => $r)
    {
            
        $url_nazev = implode(" -> ", array_reverse($r));
        $pages_list[$url_nazev] = array($url_nazev, $links->get_url($idr));
    }

    ksort($pages_list);
    
/*
    foreach($pages_list_data AS $idr => $r)
    {
            
        $url = implode(" -> ", array_reverse($r));
        $pages_list_data[$url] = array($url, $links->data[$idr]['url']);
    }
  */
  
    $pages_list = array_values($pages_list);  
    
    return json_encode($pages_list);
    
    

}



//vypise selectbox se vsemi strankami vsech typu
//ShowSelectBoxAllPages
function get_all_pages(){
        global $db;
        global $domain;
        global $links;

        
        $result_all_pages = array();        
        
        foreach($links->data AS $ids => $s){
            if($s['typ']!='uvodni') continue;
    		$result_all_pages[""] = array(0 => $s['nazev']." (".TUVODNI_STRANKA.")");
            break;
            }
        
        
        //stranky
        $archivClanku = "";
        if(MODULE_ARTICLES)
            $archivClanku = ",'archiv-clanku'";
            
        
        $where = "WHERE s.idJazyka=".WEB_LANG_ID." AND s.typ IN ('helpdesk','stranka','clanky','novinky','stranka-galerie','sitemap','kalendar-akci' ".$archivClanku.") AND s.idDomeny=".$domain->getId();
        
        
        $record = $db->Query("SELECT s.idStranky, COUNT(sr.idRodice) AS pocet_podrizenych
    				FROM ".TABLE_STRANKY." AS s
                    LEFT JOIN ".TABLE_STRANKY." AS sr ON s.idStranky=sr.idRodice
    				".$where."
    				GROUP BY s.idStranky
    				ORDER BY s.priorita
    				");
        
        $pocet = $db->numRows($record);
        $id_stranek_ke_zobrazeni = array();
		while($p = $db->getAssoc($record))
		      $id_stranek_ke_zobrazeni[] = $p['idStranky'];
        
        $result_pages = array();
        global $result_pages;
        
        function TreePages($idRodice,$id_stranek_ke_zobrazeni,$hloubka=0){
            global $db;
            global $domain;
            global $links;
            global $result_pages;

            $archivClanku = "";
            if(MODULE_ARTICLES)
                $archivClanku = ",'archiv-clanku'";
            
            $where = "WHERE s.idJazyka=".WEB_LANG_ID." AND typ IN ('helpdesk','stranka','clanky','novinky','stranka-galerie','prazdna-stranka','sitemap','kalendar-akci' ".$archivClanku.") AND idDomeny=".$domain->getId();
	
    			$record = $db->Query("SELECT s.*, COUNT(r.idUzivatele) AS pocet_uzivatelu
    				FROM ".TABLE_STRANKY." AS s
    				LEFT JOIN ".TABLE_STRANKY_PRIVATNI_UZIVATELE." AS r ON s.idStranky=r.idStranky
    				".$where." AND s.idRodice=".$idRodice."
    				GROUP BY s.idStranky
    				ORDER BY s.doMenu DESC, s.priorita 
    				");

                
            
            
            if($db->numRows($record)==0) return '';
            
            
            $hloubka++;
            
            while($page = $db->getAssoc($record)){
                $id = $page['idStranky'];
			
                if(!in_array($id,$id_stranek_ke_zobrazeni)){   
                    TreePages($id,$id_stranek_ke_zobrazeni,$hloubka);
                    continue;
                    }
            
    			$nazev = $page['nazev'];
                $ityp = $page['typ'];
                
                if($ityp == 'novinky' && !MODULE_NEWS) continue;
                if(in_array($ityp, array('clanky','archiv-clanku')) && !MODULE_ARTICLES) continue;
                if(in_array($ityp, array('stranka-galerie','galerie')) && !MODULE_GALLERY) continue;

                if($ityp=='stranka') $typ=TSTRANKA;
                elseif($ityp=='sitemap') $typ=TMAPA_WEBU;
                elseif($ityp=='novinky') $typ=TNOVINKY;
                elseif($ityp=='clanky') $typ=TCLANKY;
                elseif($ityp=='archiv-clanku') $typ=TARCHIV_CLANKU;
                elseif($ityp=='404') $typ=TCHYBOVA_STRANKA;
                elseif($ityp=='stranka-galerie') $typ=TSTRANKA_S_GALERII;
                elseif($ityp=='prazdna-stranka') $typ=TPRAZDNA_STRANKA;
                elseif($ityp=='helpdesk') $typ=THELPDESK;
                else $typ='';

                $result_pages[$id] = str_repeat("--",$hloubka-1)." ".$nazev." (".$typ.")";
                TreePages($id,$id_stranek_ke_zobrazeni,$hloubka);
    
                }
    
            }
        
        if($pocet>0)
        {
            TreePages(0,$id_stranek_ke_zobrazeni, 0);
            $result_all_pages[TSTRANKY] = $result_pages;
        }
            
        
        
        //galerie
        $result_gallery = array();
        global $result_gallery;
        
        $record = $db->Query("SELECT g.*, COUNT(gp.idStranky) AS pocet_fotografii
				
			FROM ".TABLE_STRANKY." AS g 
			LEFT JOIN ".TABLE_STRANKY." AS gp ON g.idStranky=gp.idStranky
			WHERE g.idJazyka=".WEB_LANG_ID." 
                AND g.idDomeny=".$domain->getId()."
                AND (g.typ = 'galerie' OR g.typ = 'galerie-slozka' OR g.typ = 'galerie-fotka')
                
			GROUP BY g.idStranky
			ORDER BY g.priorita
			");
		
        	
		$pocet = $db->numRows($record);
		
        $id_stranek_ke_zobrazeni = array();
		while($p = $db->getAssoc($record))
		      $id_stranek_ke_zobrazeni[] = $p['idStranky'];
              
        
        function TreeGallery($idRodice,$id_stranek_ke_zobrazeni, $hloubka=0){
            global $db;
            
            global $domain;
            global $links;
            global $result_gallery;
                        
            $where = "WHERE idJazyka=".WEB_LANG_ID." AND typ IN ('galerie', 'galerie-slozka', 'galerie-fotka') AND idDomeny=".$domain->getId();
	
    			$record = $db->Query("SELECT s.*
    				FROM ".TABLE_STRANKY." AS s
    				".$where." AND s.idRodice=".$idRodice."
    				GROUP BY s.idStranky
    				ORDER BY s.priorita 
    				");

                
            
            $pocet = $db->numRows($record);
            if($pocet==0) return '';
            $i = 1;
            
            $hloubka++;
            while($page = $db->getAssoc($record)){
                $id = $page['idStranky'];
			
                if(!in_array($id,$id_stranek_ke_zobrazeni)){
                    TreeGallery($id,$id_stranek_ke_zobrazeni,$hloubka);
                    continue;
                    }
            
    			$nazev = $page['nazev'];
    			$typ = $page['typ'];
    			
                if($typ == 'galerie') $typ = TGALERIE;
                elseif($typ == 'galerie-slozka') $typ = TSLOZKA_GALERIE;
                elseif($typ == 'galerie-fotka') $typ = TFOTKA;

                $result_gallery[$id] = str_repeat("--",$hloubka-1)." ".$nazev." (".$typ.")";                
                TreeGallery($id,$id_stranek_ke_zobrazeni,$hloubka);
                }
    
            }
        
        $d = $db->Query("SELECT idStranky FROM ".TABLE_STRANKY." WHERE idJazyka=".WEB_LANG_ID." AND typ='stranka-galerie' AND idDomeny=".$domain->getId()." LIMIT 1");
        $idRoot = 0;
        while($root = $db->getAssoc($d))
            $idRoot = $root['idStranky'];
        
        
        if($pocet>0 && MODULE_GALLERY){
            TreeGallery($idRoot,$id_stranek_ke_zobrazeni,0);  
            if(count($result_gallery) > 0)
                $result_all_pages[TGALERIE] = $result_gallery;      
            }
        
        
        if(MODULE_ARTICLES){
            foreach($links->data AS $idp => $p){
                if($p['typ']!='clanky' || $p['idJazyka'] != WEB_LANG_ID) continue;
                $result_articles = array();
                foreach($links->data AS $idr => $r){
                    if($r['typ']!='clanek' || $r['rodic'] != $idp) continue;
                    $result_articles[$idr] = str_repeat("--",1)." ".$r['nazev']." (".$r['typ'].")";                    
                }
                
                if(count($result_articles) > 0)
                    $result_all_pages[TCLANKY." - ".$p['nazev']] = $result_articles; 
                    
                }
            }
            
            
        if(MODULE_NEWS){
            foreach($links->data AS $idp => $p){
                if($p['typ']!='novinky' || $p['idJazyka'] != WEB_LANG_ID) continue;
                $result_news = array();
                foreach($links->data AS $idr => $r){
                    if($r['typ']!='novinka' || $r['rodic'] != $idp) continue;
                    $result_news[$idr] = str_repeat("--",1)." ".$r['nazev']." (".$r['typ'].")";               
                }
                if(count($result_news) > 0)
                    $result_all_pages[TNOVINKY] = $result_news;
                    
                }
            }
		
        return $result_all_pages;                                                
        
    }



//vypise selectbox galerii
function ShowSelectBoxGallery($typ='galerie', $idGalerie=0){
    global $domain;
    global $db;
    
    $data = $db->Query("SELECT idStranky, idRodice, nazev, typ 
            FROM ".TABLE_STRANKY." 
            WHERE idDomeny=".$domain->getId()."
                AND (typ='galerie-slozka' OR typ='galerie')
                AND idStranky!=".$idGalerie."
                AND idJazyka=".WEB_LANG_ID."
            ");
            
        $slozky = array();
        $slozky_data = array();

        
        while($s = $db->getAssoc($data))
            $slozky_data[$s['idStranky']] = $s; 

 
        
        function getFolderTree($idStranky, $slozky_data, & $folders){
            
            if(count($slozky_data) == 0 || $idStranky == 0) return;

            foreach($slozky_data AS $ids => $s){
                if($ids == $idStranky){
                    $folders[$ids] = $s['nazev']; //$ids;
                    getFolderTree($s['idRodice'], $slozky_data, $folders);
                    }
                }
            
            return ;
            }
        

        
        foreach($slozky_data AS $idStranky => $s){
            $folders = array($idStranky => $s['nazev']);

            if($typ == 'slozky' && $s['typ']=='galerie') continue;
            if($typ == 'galerie' && $s['typ']=='galerie-slozka') continue;
            getFolderTree($s['idRodice'], $slozky_data, $folders);
            $slozky[$idStranky] = array_reverse($folders);

            } 

        $slozky_selectbox = array();
        foreach($slozky AS $ids => $s)
            $slozky_selectbox[$ids] = implode(' -> ', $s);
            
        return $slozky_selectbox;
    }

?>