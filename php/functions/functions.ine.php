<?php

/**
 * @author Pavel Srom
 * @copyright 2014
 */

//vypise polozku menu INE panelu
function get_ine_polozka_menu($p)
{
    
    if(!$p['zobrazit']) 
        return false;
    
    $cl = "";
    if(!$p['aktivni'])
    {
        $p['odkaz'] = "#";
        $cl = " ine_disabled ";
    }
    
    $target = "";
    if(isset($p['target']) && $p['target'])
    {
        $target = 'target="_blank"';
    }
    
    $title = "title='".(isset($p['title']) ? $p['title'] : $p['nazev'])."'";
    
    return '<a href="'.$p['odkaz'].'" class="ine_icon '.$cl.'" '.$target.' '.$title.' id="ine_action-'.$p['id'].'">
        <span class="fa fa-'.$p['icon'].'"></span><br />
        '.$p['nazev'].'
        </a>';
    
    
}


function get_ine_odkaz_editace_stranky($id_stranky, $typ, $config_modules)
{
    $url = "#"; //url ajaxu
    $mid = 0; //id modulu
    $mn = ""; //systemovy nazev modulu
    $ajax_template = AJAX_GATEWAY_TEMPLATE;
    if($typ == 'clanek' && MODULE_ARTICLES)
    {
        $mn = 'articles'; 
        $ajax_template .= "articles.load.form";
        
    } 
    elseif($typ == 'novinka' && MODULE_NEWS)
    {
        $mn = 'news'; 
        $ajax_template .= "news.load.form";
    }   
    elseif($typ == 'kalendar-typ' && MODULE_EVENT_CALENDAR)
    {
        $mn = 'calendar';
        $ajax_template .= "types.load.form";
    }
    elseif($typ == 'akce' && MODULE_EVENT_CALENDAR)
    {
        $mn = 'calendar';
        $ajax_template .= "action.load.form";
    }
    elseif(($typ == 'galerie-slozka' || $typ == 'galerie') && MODULE_GALLERY)
    {
        $mn = "gallery";
        $ajax_template .= "gallery.load.form";
    }
    elseif($typ == 'galerie-fotka' && MODULE_GALLERY)
    {
        $mn = "gallery";
        $ajax_template .= "photos.load.form";
    }
    elseif($typ == 'tag' && MODULE_ARTICLES && MODULE_RELATED_ARTICLES)
    {
        $mn = "gallery";
        $ajax_template .= "photos.load.form";
    }
    else
    {
        $mn = "pages";
        $ajax_template .= "load.form";
    }
    
    
    
    $mid = isset($config_modules[$mn]) ? $config_modules[$mn] : 0;
    if($mid > 0)
        $url = str_replace(array("MODULE_ID","MODULE_NAME"),array($mid,$mn),$ajax_template);
    
    
    return $url;
    
}

function get_ine_odkaz_smazat_stranku($id_stranky, $typ, $config_modules)
{
    global $login_obj;
    
    $add = array();
    
    $url = "#";
    
    if(!$login_obj->UserPrivilege("content_delete"))
        return $url;
    
    if($typ == 'clanek' && MODULE_ARTICLES)
    {
        $mn = 'articles'; 
        $url = get_ajax_url($mn,"articles.delete",$config_modules);
    }
    elseif($typ == 'novinka' && MODULE_NEWS)
    {
        $mn = 'news';
        $url = get_ajax_url($mn,"news.delete",$config_modules);
    }
    elseif($typ == 'kalendar-typ' && MODULE_EVENT_CALENDAR)
    {
        $mn = 'calendar';
        $url = get_ajax_url($mn,"events.types.delete",$config_modules);        
    }
    elseif($typ == 'akce' && MODULE_EVENT_CALENDAR)
    {
        $mn = 'calendar';
        $url = get_ajax_url($mn,"calendar.events.delete",$config_modules);        
    }
    elseif(($typ == 'galerie-slozka' || $typ == 'galerie') && MODULE_GALLERY)
    {
        $mn = "gallery";
        $url = get_ajax_url($mn,"gallery.delete",$config_modules);
    }
    elseif($typ == 'galerie-fotka' && MODULE_GALLERY)
    {
        $mn = "gallery";
        $url = get_ajax_url($mn,"photos.delete",$config_modules);
    }
    elseif($typ == 'tag' && MODULE_ARTICLES && MODULE_RELATED_ARTICLES)
    {
        $mn = "articles";
        $url = get_ajax_url($mn,"tags.delete",$config_modules);
    }
    else
    {
        $mn = "pages";
        $url = get_ajax_url($mn,"page.delete",$config_modules);
    }
    
    return $url;
}


function get_ajax_url($module_name, $action, $config_modules)
{
    $ajax_template = AJAX_GATEWAY_TEMPLATE;
    $mid = isset($config_modules[$module_name]) ? $config_modules[$module_name] : 0;
    $url = "#";
    if($mid > 0 || $module_name == 'wet' || $module_name == 'inlineedit')
        $url = str_replace(array("MODULE_ID","MODULE_NAME"),array($mid,$module_name),$ajax_template).$action;
    
    
    return $url;
    
    
}

//prida do menu dalsi polozky tykajici se pridavani novych clanku/novinek/akci/fotek
function get_special_items_to_menu($typ, $menu, $config_modules)
{
    global $login_obj;
    
    $add = array();
    
    
    if(($typ == 'clanky' || $typ == 'clanek') && MODULE_ARTICLES)
    {
        $mn = 'articles'; 
        $nazev = TPRIDAT_CLANEK;
        $aktivni = $login_obj->UserPrivilege("content_add");
        $add[] = array("nazev" => $nazev, "aktivni" => $aktivni, "url" => get_ajax_url($mn,"articles.load.form",$config_modules));
    }
    elseif(($typ == 'novinky' || $typ == 'novinka') && MODULE_NEWS)
    {
        $mn = 'news';
        $nazev = TPRIDAT_NOVINKU;
        $aktivni = $login_obj->UserPrivilege("content_add");
        $add[] = array("nazev" => $nazev, "aktivni" => $aktivni, "url" => get_ajax_url($mn,"load.form",$config_modules));
    }
    elseif(($typ == 'kalendar-akci' || $typ == 'kalendar-typ' || $typ == 'akce') && MODULE_EVENT_CALENDAR)
    {
        $mn = 'calendar';
        $nazev = TPRIDAT_AKCI;
        $aktivni = $login_obj->UserPrivilege("content_add");
        $add[] = array("nazev" => $nazev, "aktivni" => $aktivni, "url" => get_ajax_url($mn,"types.load.form",$config_modules));
                
        $nazev = TPRIDAT_TYP_AKCE;
        $aktivni = $login_obj->UserPrivilege("content_add");
        $add[] = array("nazev" => $nazev, "aktivni" => $aktivni, "id" => "pridat-jine2", "url" => get_ajax_url($mn,"load.form",$config_modules));
        
    }
    elseif($typ == 'stranka-galerie' && MODULE_GALLERY)
    {
        $mn = "gallery";
        $nazev = TPRIDAT_GALERII;
        $aktivni = $login_obj->UserPrivilege("content_add");
        $add[] = array("nazev" => $nazev, "aktivni" => $aktivni, "url" => get_ajax_url($mn,"load.form",$config_modules));
    }
    elseif($typ == 'galerie-slozka' && MODULE_GALLERY)
    {
        $mn = "gallery";
        $nazev = TPRIDAT_GALERII;
        $aktivni = $login_obj->UserPrivilege("content_add");
        $add[] = array("nazev" => $nazev, "aktivni" => $aktivni, "url" => get_ajax_url($mn,"load.form",$config_modules));
    }
    elseif(($typ == 'galerie-fotka' || $typ == 'galerie') && MODULE_GALLERY)
    {
        $mn = "gallery";
        $nazev = TPRIDAT_FOTKY;
        $aktivni = $login_obj->UserPrivilege("content_add");
        $add[] = array("nazev" => $nazev, "aktivni" => $aktivni, "url" => get_ajax_url($mn,"photos.load.form",$config_modules));
    }
    elseif(($typ == 'tagy' || $typ == 'tag') && MODULE_ARTICLES && MODULE_RELATED_ARTICLES)
    {
        $mn = "articles";
        $nazev = TPRIDAT_TAG;
        $aktivni = $login_obj->UserPrivilege("content_add");
        $add[] = array("nazev" => $nazev, "aktivni" => $aktivni, "url" => get_ajax_url($mn,"tags.load.form",$config_modules));
    }
    
    foreach($add AS $a)
        $menu["stranka"][] = array(
            "id"        => isset($a['id']) ? $a['id'] : "pridat-jine1",
            "icon"      => "pridat",
            "nazev"     => $a['nazev'],
            "aktivni"   => $a['aktivni'],
            "odkaz"     => $a['url'],
            "zobrazit"  => true 
            );
            
            
    return $menu;
}


//zjisti zda ma uzivatel pristup k dane atyp strance
function ine_has_privilege($typ)
{
    global $login_obj;
    
    if($typ == 'clanek' && MODULE_ARTICLES)
        return true;
    elseif($typ == 'novinka' && MODULE_NEWS)
        return true;
    elseif(($typ == 'galerie' || $typ == 'galerie-slozka' || $typ == 'galerie-fotka') && MODULE_GALLERY)
        return true;
    elseif(($typ == 'akce' || $typ == 'kalendar-typ') && MODULE_EVENT_CALENDAR)
        return true;
    elseif($typ == 'tag' && MODULE_RELATED_ARTICLES && MODULE_ARTICLES)
        return true;
    elseif(MODULE_PAGES)
        return true;
    else
        return false;
        
        
        
        
}

//nacte pole systemovy nazev modulu => id modulu 
function config_modules($moduly)
{
    global $db;
    
    if(count($moduly) == 0)
        return false;
    
    $data = $db->query("SELECT systemovyNazev AS nazev, idModulu AS id FROM ".TABLE_CMS_MODULY." WHERE idModulu IN (".implode(",",$moduly).") AND zobrazit=1");
    
    $result = array();
    while($m = $db->getAssoc($data))
        $result[$m['nazev']] = $m['id'];
        
        
    return $result;
    
    
}

?>