<?php

/**
 * @author Pavel Srom
 * @copyright 2010
 */


class C_ModuleFacebook extends C_ModuleInfo{
    
    
    public function C_ModuleFacebook($inicialize = null){
		
		global $systemMessage;
		global $db;
		global $login_obj;
		global $domain;
    
    //overovani pristupu k modulu
		
		if(!$login_obj->logged()) return;
		if(!$domain->exists()) return;
		if(!$domain->UserDomains()) return;
		
		C_ModuleInfo::Inicialize($inicialize);
			
		$this->Security();
    
        if(isset($_POST['btnUlozNastaveniPluginu'])){
            
            $idPluginu = isset($_POST['idPluginu'])?intval($_POST['idPluginu']):0;
            $nazev = isset($_POST['nazev'])?$db->secureString(strip_tags($_POST['nazev'])):'';
            $barva = isset($_POST['barva'])?$db->secureString(strip_tags($_POST['barva'])):'light';
            $font = isset($_POST['font'])?$db->secureString(strip_tags($_POST['font'])):'arial';
            $struktura = isset($_POST['struktura'])?$db->secureString(strip_tags($_POST['struktura'])):'standard';
            $pocetPolozek = isset($_POST['pocet'])?intval($_POST['pocet']):0;
            $vyska = isset($_POST['vyska'])?intval($_POST['vyska']):0;
            $sirka = isset($_POST['sirka'])?intval($_POST['sirka']):0;
            $stream = isset($_POST['stream'])?intval($_POST['stream']):0;
            $zobrazit = isset($_POST['zobrazit'])?intval($_POST['zobrazit']):0;
            $zobrazitFotky = isset($_POST['zobrazitFotky'])?intval($_POST['zobrazitFotky']):0;
            
            $zobrazitFbSdileni = isset($_POST['zobrazitFbSdileni'])?intval($_POST['zobrazitFbSdileni']):0;
            $zobrazitTwSdileni = isset($_POST['zobrazitTwSdileni'])?intval($_POST['zobrazitTwSdileni']):0;
            $zobrazitGoogleBookmarksSdileni = isset($_POST['zobrazitGoogleBookmarksSdileni'])?intval($_POST['zobrazitGoogleBookmarksSdileni']):0;
            $zobrazitGoogleReaderSdileni = isset($_POST['zobrazitGoogleReaderSdileni'])?intval($_POST['zobrazitGoogleReaderSdileni']):0;
            $zobrazitDeliciousSdileni = isset($_POST['zobrazitDeliciousSdileni'])?intval($_POST['zobrazitDeliciousSdileni']):0;
            $zobrazitLinkujSdileni = isset($_POST['zobrazitLinkujSdileni'])?intval($_POST['zobrazitLinkujSdileni']):0;
            $zobrazitEmailSdileni = isset($_POST['zobrazitEmailSdileni'])?intval($_POST['zobrazitEmailSdileni']):0;
            $zobrazitRssSdileni = isset($_POST['zobrazitRssSdileni'])?intval($_POST['zobrazitRssSdileni']):0;
            $zobrazitPrintSdileni = isset($_POST['zobrazitPrintSdileni'])?intval($_POST['zobrazitPrintSdileni']):0;
            
            $obsah = isset($_POST['obsahEmailu'])?$db->secureString(strip_tags($_POST['obsahEmailu'])):'';
            $predmet = isset($_POST['predmetEmailu'])?$db->secureString(strip_tags($_POST['predmetEmailu'])):'';
            
            
            if($idPluginu<=0){
					$systemMessage->add_error(ERROR_OBJEKT_NEEXISTUJE);
					error_handler_hacking($this->moduleName, ERROR_OBJEKT_NEEXISTUJE);
					$url = ADMIN_SCRIPT."?".$this->moduleSystemName;
					Redirect($url);
					}
            
            if(!$login_obj->UserPrivilege('settings_edit')){
				$systemMessage->add_error(ERROR_NEDOSTATECNA_PRAVA);
				error_handler_hacking($this->moduleName,ERROR_NEDOSTATECNA_PRAVA);
				$url = ADMIN_SCRIPT."?".$this->moduleSystemName;
				Redirect($url);
				}
                
            $set = "
                nazev='".$nazev."',
                barva='".$barva."',
                font='".$font."',
                struktura='".$struktura."',
                zobrazitStream=".$stream.",
                vyska=".$vyska.",
                sirka=".$sirka.",
                pocetPolozek=".$pocetPolozek.",
                zobrazit=".$zobrazit.",
                zobrazitFotky=".$zobrazitFotky.",
                zobrazitFbSdileni=".$zobrazitFbSdileni.",
                zobrazitTwSdileni=".$zobrazitTwSdileni.",
                zobrazitDeliciousSdileni=".$zobrazitDeliciousSdileni.",
                zobrazitLinkujSdileni=".$zobrazitLinkujSdileni.",
                zobrazitEmailSdileni=".$zobrazitEmailSdileni.",
                zobrazitGoogleReaderSdileni=".$zobrazitGoogleReaderSdileni.",
                zobrazitGoogleBookmarksSdileni=".$zobrazitGoogleBookmarksSdileni.",
                zobrazitRssSdileni=".$zobrazitRssSdileni.",
                zobrazitPrintSdileni=".$zobrazitPrintSdileni.",
                predmetEmailu='".$predmet."',
                obsahEmailu='".$obsah."'
               ";
            
        $res = $db->Query("UPDATE ".TABLE_FACEBOOK_PLUGINY." SET ".$set." WHERE idPluginu=".$idPluginu." LIMIT 1");            
        if($res)
            $systemMessage->add_ok(OK_ULOZENO);
            else
            $systemMessage->add_error(ERROR_CHYBA_DATABAZE);
            
               
        $url = ADMIN_SCRIPT."?".$this->moduleSystemName."&EditovatNastaveni=".$idPluginu;
		Redirect($url);    
            
        }
    
        if(isset($_POST['btnUlozNastaveni'])){
            $nazevWebu = isset($_POST['nazev'])?strip_tags(trim($_POST['nazev'])):'';
            $appid = isset($_POST['appid'])?strip_tags(trim($_POST['appid'])):'';
            $appkey = isset($_POST['appkey'])?strip_tags(trim($_POST['appkey'])):'';
            $appsec = isset($_POST['appsec'])?strip_tags(trim($_POST['appsec'])):'';
            $adminid = isset($_POST['adminid'])?strip_tags(trim($_POST['adminid'])):'';
            $urlStrankyFB = isset($_POST['urlStrankyFb'])?strip_tags(trim($_POST['urlStrankyFb'])):'';
            $urlStrankyTW = isset($_POST['urlStrankyTw'])?strip_tags(trim($_POST['urlStrankyTw'])):'';
            //$typ = isset($_POST['typ'])?strip_tags(trim($_POST['typ'])):'likebutton';
            $zobrazit = isset($_POST['zobrazit'])?intval($_POST['zobrazit']):0;
            
            $set = "";
            
            
            if(!$login_obj->UserPrivilege('settings_edit')){
				$systemMessage->add_error(ERROR_NEDOSTATECNA_PRAVA);
				error_handler_hacking($this->moduleName,ERROR_NEDOSTATECNA_PRAVA);
				$url = ADMIN_SCRIPT."?".$this->moduleSystemName;
				Redirect($url);
				}
            
            if($nazevWebu=='')
                $systemMessage->add_error(ERROR_NAZEV_WEBU);
                else
                $set .= "nazevWebu='".$nazevWebu."',";
                
            if($appid=='')
                $systemMessage->add_error(ERROR_APPID);
                else
                $set .= "appId='".$appid."',";
                
                
            if($adminid=='')
                $systemMessage->add_error(ERROR_ADMINID);
                else
                $set .= "adminId='".$adminid."',";
            
            if(!TestHttpUrl($urlStrankyFB) && $urlStrankyFB!='')
                $systemMessage->add_error(ERROR_URL);
                else
                $set .= "urlFbStranky='".$urlStrankyFB."',";
            
            if(!TestHttpUrl($urlStrankyTW) && $urlStrankyTW!='')
                $systemMessage->add_error(ERROR_URL);
                else
                $set .= "urlTwStranky='".$urlStrankyTW."',";
            /*    
            if($appkey=='')
                $systemMessage->add_error(ERROR_APPKEY);
                else
                $set .= "apiKlic='".$appkey."',";
                
            if($appsec=='')
                $systemMessage->add_error(ERROR_APPSEC);
                else
                $set .= "appSecret='".$appsec."',";
            */    
            
            $set .= "zobrazit=".$zobrazit;
            
            //$set .= "typ='".$typ."'";
            
            $res = $db->Query("UPDATE ".TABLE_FACEBOOK." SET ".$set." WHERE idDomeny=".$domain->getId()." LIMIT 1");
            
            if($res && !$systemMessage->error_exists())
                $systemMessage->add_ok(OK_ULOZENO);
            elseif($res && $systemMessage->error_exists())
                $systemMessage->add_warning(WARNING_ULOZENO);
                else
                $systemMessage->add_error(ERROR_CHYBA_DATABAZE);
                    
            $url = ADMIN_SCRIPT."?".$this->moduleSystemName."&Nastaveni";
            Redirect($url);
                
            
            
        }
    
    
    
    
    }
    
    public function CreateMenu(){
		global $login_obj;
		
		$menu = array();
        $menu[$this->moduleSystemName.''] = TSOCIALNI_PLUGINY;
		$menu[$this->moduleSystemName.'&amp;Nastaveni'] = TNASTAVENI;
		return $menu;
		}
    
    
    public function GetActiveItemMenu(){
        if(isset($_GET['Nastaveni'])) return 'Nastaveni';
		//if(isset($_GET['Napoveda'])) return 'Napoveda';
		return '';
		}
    
    public function Create(){
        global $systemMessage;
		global $login_obj;
		global $domain;

		if(!$this->active) return false;
		
		if(!$login_obj->logged()) return false;
		if(!$domain->exists()) return false;
		if(!$domain->UserDomains()) return false;

		$ind = true;
        
        
        if($ind && isset($_GET['EditovatNastaveni'])){
			$this->ShowPluginsSettings($_GET['EditovatNastaveni']);
			$ind = false;
			}
        if($ind && isset($_GET['Nastaveni'])){
			$this->ShowSettings();
			$ind = false;
			}
		if($ind){
		  $this->ShowPluginsList();
		}
		
		return true;
        
        
        
    }
    
    
    private function Security(){
        global $db;
		global $domain;
		global $systemMessage;
		global $login_obj;
		
       
		//overeni zda s daty nemanipuluje neopravnena osoba
		$indik = true;
		if(isset($_GET['EditovatNastaveni'])) 
			if(!ctype_digit($_GET['EditovatNastaveni'])) $indik = false;
		
		//overeni kombinaci get
		$indik2 = true;
		$pocet = count($_GET);
		
		if($pocet == 1) $indik2 = false;
		if($pocet == 2){
            if($indik2 && isset($_GET['Nastaveni'])) $indik2 = false;
			if($indik2 && isset($_GET['EditovatNastaveni'])) $indik2 = false;
			}

		
		if($indik2) $indik = false;
        
        if(!$indik){
			error_handler_hacking($this->moduleName, ERROR_NARUSENI_SYSTEMU);
			$systemMessage->add_error(ERROR_NARUSENI_SYSTEMU);
			$url = ADMIN_SCRIPT."?".$this->moduleSystemName;
			Redirect($url);
			}
        
        
    }
    
    
    
    private function ShowSettings(){
        global $db;
        global $domain;
        
        global $login_obj;
        
        
        $data = $db->Query("SELECT * FROM ".TABLE_FACEBOOK." WHERE idDomeny=".$domain->getId()." LIMIT 1");
        if($db->numRows($data)>0){
            $fb = $db->getAssoc($data);
            }
            else{
            $db->Query("INSERT INTO ".TABLE_FACEBOOK." (idDomeny) VALUES(".$domain->getId().")");
            $data = $db->Query("SELECT * FROM ".TABLE_FACEBOOK." WHERE idDomeny=".$domain->getId()." LIMIT 1");
            $fb = $db->getAssoc($data);
            
            }
        
        echo "<h2>".TNASTAVENI."</h2>";
        showH3(TZAKLADNI_UDAJE,'home');
        
        echo "<form method='post' action=''>";
		echo "<table class='table2'>";
		
    
        $ch1 = $ch2 = '';
        echo "<tr><th>".TNAZEV_WEBU." ".help(108)."</th><td><input type='text' class='input w100' name='nazev' maxlength='250' value='".$fb['nazevWebu']."' />".req()."</td></tr>";
        
        echo "<tr><th>".TURL_STRANKY_NA_FACEBOOKU." ".help(109)."</th><td><input type='text' class='input w100' name='urlStrankyFb' maxlength='250' value='".$fb['urlFbStranky']."' /></td></tr>";
        
        echo "<tr><th>".TURL_STRANKY_NA_TWITTERU." ".help(110)."</th><td><input type='text' class='input w100' name='urlStrankyTw' maxlength='250' value='".$fb['urlTwStranky']."' /></td></tr>";
        
        echo "<tr><th>".TID_APLIKACE." ".help(111)."</th><td><input type='text' class='input w100' name='appid' maxlength='50' value='".$fb['appId']."' />".req()."</td></tr>";
        
        //echo "<tr><th>Klíč aplikace".help(64)."</th><td><input type='text' class='input w100' name='appkey' maxlength='200' value='".$fb['apiKlic']."' />".req()."</td></tr>";
        
        //echo "<tr><th>Tajný klíč aplikace".help(64)."</th><td><input type='text' class='input w100' name='appsec' maxlength='200' value='".$fb['appSecret']."' />".req()."</td></tr>";
        
        echo "<tr><th>".TID_ADMIN." ".help(112)."</th><td><input type='text' class='input w100' name='adminid' maxlength='50' value='".$fb['adminId']."' />".req()."</td></tr>";
        
        //$sel = array('likebutton' => "Tlačítko To se mi líbí",'likebox'=>"Box To se mi líbí", 'livestream'=>'LiveStream','comments'=>'Facebook komentáře', 'recommendations'=>'Doporučení','facepile'=>'Tváře');
        
        /*
        echo "<tr><th>Typ aplikace ".help(64)."</th><td>";
        ShowSelectBox('typ',$sel,$fb['typ'],false,'','');
        echo "</td></tr>";
        */
        
        
        if($fb['zobrazit']==1) $ch1 = "checked = 'checked'"; else $ch2 = "checked = 'checked'";
		echo "<tr><th>".TZOBRAZIT." ".help(113)."</th><td>";
		echo "<input class='radio' type='radio' name='zobrazit' value='1' ".$ch1."/>ano";
		echo "<input class='radio' type='radio' name='zobrazit' value='0' ".$ch2."/>ne";
		echo "</td></tr>";


    	echo "</table>";
		
		echo "<div class='dolni-tlacitka'>";
       
		if($login_obj->UserPrivilege('settings_edit'))
			echo "<input class='tool_button save_button' type='submit' name='btnUlozNastaveni' value='".TULOZIT."' title='".TULOZIT."' />";
			else
			echo "<input class='tool_button_disabled save_button_disabled' type='submit' name='btnUlozNastaveni' value='".TULOZIT."' title='".TULOZIT."' disabled='disabled'/>";
		

		echo "</div>";
		
		echo "</form>";
        
        
        
        
    }
    
    
    private function ShowPluginsList(){
        global $db;
        global $domain;
        global $login_obj;
        
        $data = $db->Query("SELECT idNastaveni FROM ".TABLE_FACEBOOK." WHERE idDomeny=".$domain->getId()." LIMIT 1");
        if($db->numRows($data)==0){
            $db->Query("INSERT INTO ".TABLE_FACEBOOK." (idDomeny) VALUES(".$domain->getId().")");
            }
        
        $idNastaveni = $domain->getId();        
        
        $record = $db->Query("SELECT * FROM ".TABLE_FACEBOOK_PLUGINY." WHERE idDomeny=".$idNastaveni);
        
        if($db->numRows($record)==0){
            $db->Query("INSERT INTO ".TABLE_FACEBOOK_PLUGINY." (idDomeny,popis,typ,nazev) 
                VALUES 
                (".$idNastaveni.",'".TAKTIVITY_FEED_INFO."','activityFeed','".TAKTIVITY_FEED."'),
                (".$idNastaveni.",'".TCOMMENTS_INFO."','comments','".TCOMMENTS."'),
                (".$idNastaveni.",'".TFACEPILE_INFO."','facepile','".TFACEPILE."'),
                (".$idNastaveni.",'".TLIKEBUTTON_INFO."','likeButton','".TLIKEBUTTON."'),
                (".$idNastaveni.",'".TLIKEBOX_INFO."','likeBox','".TLIKEBOX."'),
                (".$idNastaveni.",'".TLIVE_STREAM_INFO."','liveStream','".TLIVE_STREAM."'),
                (".$idNastaveni.",'".TRECOMMENDATIONS_INFO."','recommendations','".TRECOMMENDATION."'),
                (".$idNastaveni.",'".TIKONY_PRO_SDILENI_INFO."','socialPanel','".TIKONY_PRO_SDILENI."')");
            
            $record = $db->Query("SELECT * FROM ".TABLE_FACEBOOK_PLUGINY." WHERE idDomeny=".$idNastaveni);
            }
                
        $pocet = $db->numRows($record);
		
        $main_tools = array();
        
    
        echo "<h2>".TSOCIALNI_PLUGINY."</h2>";
        
		showH3(TSEZNAM_PLUGINU, 'list');
        
        echo "<table class='table'>";
		
		
		echo "<tr>";
		echo "<th class='w25'>".TID."</th>";
        echo "<th>".TNAZEV."</th>";
		echo "<th class='w10'>".TAKTIVNI."</th>";
		echo "<th class='w150'>".TAKCE."</th>";
		echo "</tr>";

		$dis_content = false;
		$dis_settings = false;
		$dis_delete = false;
		if(!$login_obj->UserPrivilege('content_view')) $dis_content = true;
		if(!$login_obj->UserPrivilege('settings_view')) $dis_settings = true;
		if(!$login_obj->UserPrivilege('settings_delete')) $dis_delete = true;
			
		while($fb = $db->getAssoc($record)){
			$id = $fb['idPluginu'];
			
			$nazev = $fb['nazev'];
            $popis = $fb['popis'];
			$zobrazit = $fb['zobrazit'];
			
			$stav = $fb['zobrazit']==0?"<span class='stav0'>".TNE."</span>":"<span class='stav1'>".TANO."</span>";

			echo "<tr>";
			
			echo "<td>".$id."</td>";
			echo "<td class='tleft'>".secureString($nazev)."&nbsp;</td>";
			echo "<td>".$stav."&nbsp;</td>";
			echo "<td>";
			
			if($dis_settings)
				echo icon_disabled('setting');
				else	
				echo "<a href='".ADMIN_SCRIPT."?".$this->moduleSystemName."&amp;EditovatNastaveni=".$id."'>".icon('setting')."</a>";

			
			echo "</td>";
			echo "</tr>";
			
			
		}
		if($pocet==0) 
			echo "<tr><td colspan='4' style='text-align: center;'>".TNENALEZEN_ZADNY_ZAZNAM."</td></tr>";
		echo "</table>";	

        }


    private function ShowPluginsSettings($idPlugin){
        global $db;
        global $domain;
        global $login_obj;
        
        
        
        $main_tools = array();
          
        $main_tools[] = array('ikona' => 'back', "nazev" => TZPET_NA_SEZNAM_PLUGINU, "aktivni" => 0, "odkaz" => $this->moduleSystemName);
        echo main_tools($main_tools); 
        
        $data = $db->Query("SELECT * FROM ".TABLE_FACEBOOK_PLUGINY." 
            WHERE idDomeny=".$domain->getId()."
                AND idPluginu=".$idPlugin."
            LIMIT 1
            ");
            
        if($db->numRows($data)==0) return;
        
        
        
        
        $fb = $db->getAssoc($data);
        
        $typ = $fb['typ'];
        
        echo "<h2>".TNASTAVENI_PLUGINU."</h2>";
        showH3(TZAKLADNI_UDAJE,'home');
        
        echo "<form method='post' action=''>";
		echo "<table class='table2'>";
        
        
        echo "<tr><th>".TNAZEV." ".help(125)."</th><td><input type='text' class='input w100' name='nazev' value='".$fb['nazev']."' /></td></tr>";
        
        echo "<tr><th>".TPOPIS."</th><td>".$fb['popis']."</td></tr>";
        
  
        if($typ!='socialPanel'){
            
            echo "<tr><th>".TVYSKA." ".help(114)."</th><td><input type='text' class='input w100' name='vyska' maxlength='4' value='".$fb['vyska']."' /></td></tr>";
            
            echo "<tr><th>".TSIRKA." ".help(115)."</th><td><input type='text' class='input w100' name='sirka' maxlength='4' value='".$fb['sirka']."' /></td></tr>";
            
            }
        
        
        if($typ=='comments')
            echo "<tr><th>".TPOCET_KOMENTARU." ".help(116)."</th><td><input type='text' class='input w100' name='pocet' maxlength='4' value='".$fb['pocetPolozek']."' /></td></tr>";
        elseif($typ=='facepile')
            echo "<tr><th>".TPOCET_RADKU_FOTEK." ".help(117)."</th><td><input type='text' class='input w100' name='pocet' maxlength='4' value='".$fb['pocetPolozek']."' /></td></tr>";
        elseif($typ=='likeBox')
            echo "<tr><th>".TPOCET_PROFILOVYCH_FOTEK." ".help(118)."</th><td><input type='text' class='input w100' name='pocet' maxlength='4' value='".$fb['pocetPolozek']."' /></td></tr>";
        
        if($typ=='activityFeed' || $typ=='likeButton' || $typ=='recommendations'){
            $barva = array('light'=>TSVETLY,'dark'=>TTMAVY);
            echo "<tr><th>".TBARVA." ".help(119)."</th><td>";
            echo ShowSelectBoxToString('barva',$barva,$fb['barva'],false);
            echo "</td></tr>";

            $font = array('arial'=>'Arial','verdana'=>'Verdana');
            echo "<tr><th>".TFONT_PISMA." ".help(120)."</th><td>";
            echo ShowSelectBoxToString('font',$font,$fb['font'],false);
            echo "</td></tr>";
            }
        
        if($typ=='likeButton'){
            $layout = array('standard'=>TBEZ_POCITADLA,'button_count'=>TTLACITKO_S_POCITADLEM,'box_count'=>TBOX_S_POCITADLEM);
            echo "<tr><th>".TTYP." ".help(121)."</th><td>";
            echo ShowSelectBoxToString('struktura',$layout,$fb['struktura'],false);
            echo "</td></tr>";
            }
        
        if($typ=='likeButton'){
            $ch1 = $ch2 = "";
            if($fb['zobrazitFotky']==1) $ch1 = "checked = 'checked'"; else $ch2 = "checked = 'checked'";
    		echo "<tr><th>".TZOBRAZOVAT_PROFILOVE_FOTKY." ".help(124)."</th><td>";
    		echo "<input class='radio' type='radio' name='zobrazitFotky' value='1' ".$ch1."/>".TANO;
    		echo "<input class='radio' type='radio' name='zobrazitFotky' value='0' ".$ch2."/>".TNE;
    		echo "</td></tr>";
           }
        
        if($typ=='likeBox'){
            $ch1 = $ch2 = "";
            if($fb['zobrazitStream']==1) $ch1 = "checked = 'checked'"; else $ch2 = "checked = 'checked'";
    		echo "<tr><th>".TZOBRAZOVAT_STREAM." ".help(122)."</th><td>";
    		echo "<input class='radio' type='radio' name='stream' value='1' ".$ch1."/>".TANO;
    		echo "<input class='radio' type='radio' name='stream' value='0' ".$ch2."/>".TNE;
    		echo "</td></tr>";
           }
        
        if($typ=='socialPanel'){
            $ch1 = $ch2 = "";
            if($fb['zobrazitFbSdileni']==1) $ch1 = "checked = 'checked'"; else $ch2 = "checked = 'checked'";
        	echo "<tr><th>".TFACEBOOK_SDILENI." ".help(152)."</th><td>";
        	echo "<input class='radio' type='radio' name='zobrazitFbSdileni' value='1' ".$ch1."/>".TANO;
        	echo "<input class='radio' type='radio' name='zobrazitFbSdileni' value='0' ".$ch2."/>".TNE;
        	echo "</td></tr>";
            
            $ch1 = $ch2 = "";
            if($fb['zobrazitTwSdileni']==1) $ch1 = "checked = 'checked'"; else $ch2 = "checked = 'checked'";
        	echo "<tr><th>".TTWITTER_SDILENI." ".help(153)."</th><td>";
        	echo "<input class='radio' type='radio' name='zobrazitTwSdileni' value='1' ".$ch1."/>".TANO;
        	echo "<input class='radio' type='radio' name='zobrazitTwSdileni' value='0' ".$ch2."/>".TNE;
        	echo "</td></tr>";
            
            $ch1 = $ch2 = "";
            if($fb['zobrazitLinkujSdileni']==1) $ch1 = "checked = 'checked'"; else $ch2 = "checked = 'checked'";
        	echo "<tr><th>".TLINKUJ_CZ_SDILENI." ".help(154)."</th><td>";
        	echo "<input class='radio' type='radio' name='zobrazitLinkujSdileni' value='1' ".$ch1."/>".TANO;
        	echo "<input class='radio' type='radio' name='zobrazitLinkujSdileni' value='0' ".$ch2."/>".TNE;
        	echo "</td></tr>";
            
            $ch1 = $ch2 = "";
            if($fb['zobrazitDeliciousSdileni']==1) $ch1 = "checked = 'checked'"; else $ch2 = "checked = 'checked'";
        	echo "<tr><th>".TDELICIOUS_SDILENI." ".help(155)."</th><td>";
        	echo "<input class='radio' type='radio' name='zobrazitDeliciousSdileni' value='1' ".$ch1."/>".TANO;
        	echo "<input class='radio' type='radio' name='zobrazitDeliciousSdileni' value='0' ".$ch2."/>".TNE;
        	echo "</td></tr>";
            
            $ch1 = $ch2 = "";
            if($fb['zobrazitGoogleBookmarksSdileni']==1) $ch1 = "checked = 'checked'"; else $ch2 = "checked = 'checked'";
        	echo "<tr><th>".TGOOGLE_BOOKMARKS_SDILENI." ".help(156)."</th><td>";
        	echo "<input class='radio' type='radio' name='zobrazitGoogleBookmarksSdileni' value='1' ".$ch1."/>".TANO;
        	echo "<input class='radio' type='radio' name='zobrazitGoogleBookmarksSdileni' value='0' ".$ch2."/>".TNE;
        	echo "</td></tr>";
            
            $ch1 = $ch2 = "";
            if($fb['zobrazitGoogleReaderSdileni']==1) $ch1 = "checked = 'checked'"; else $ch2 = "checked = 'checked'";
        	echo "<tr><th>".TGOOGLE_READER_SDILENI." ".help(157)."</th><td>";
        	echo "<input class='radio' type='radio' name='zobrazitGoogleReaderSdileni' value='1' ".$ch1."/>".TANO;
        	echo "<input class='radio' type='radio' name='zobrazitGoogleReaderSdileni' value='0' ".$ch2."/>".TNE;
        	echo "</td></tr>";
            
            $ch1 = $ch2 = "";
            if($fb['zobrazitRssSdileni']==1) $ch1 = "checked = 'checked'"; else $ch2 = "checked = 'checked'";
        	echo "<tr><th>".TRSS." ".help(158)."</th><td>";
        	echo "<input class='radio' type='radio' name='zobrazitRssSdileni' value='1' ".$ch1."/>".TANO;
        	echo "<input class='radio' type='radio' name='zobrazitRssSdileni' value='0' ".$ch2."/>".TNE;
        	echo "</td></tr>";
            
            $ch1 = $ch2 = "";
            if($fb['zobrazitPrintSdileni']==1) $ch1 = "checked = 'checked'"; else $ch2 = "checked = 'checked'";
        	echo "<tr><th>".TZOBRAZOVAT_IKONU_PRO_TISK." ".help(159)."</th><td>";
        	echo "<input class='radio' type='radio' name='zobrazitPrintSdileni' value='1' ".$ch1."/>".TANO;
        	echo "<input class='radio' type='radio' name='zobrazitPrintSdileni' value='0' ".$ch2."/>".TNE;
        	echo "</td></tr>";
            
            $ch1 = $ch2 = "";
            if($fb['zobrazitEmailSdileni']==1) $ch1 = "checked = 'checked'"; else $ch2 = "checked = 'checked'";
        	echo "<tr><th>".TZOBRAZOVAT_IKONU_PRO_EMAIL." ".help(160)."</th><td>";
        	echo "<input class='radio' type='radio' name='zobrazitEmailSdileni' value='1' ".$ch1."/>".TANO;
        	echo "<input class='radio' type='radio' name='zobrazitEmailSdileni' value='0' ".$ch2."/>".TNE;
        	echo "</td></tr>";
            
            echo "<tr><th>".TPREDMET." ".help(161)."</th><td><input type='text' class='input w100' name='predmetEmailu' value='".$fb['predmetEmailu']."' /></td></tr>";
            
            echo "<tr><th>".TPREDNASTAVENY_TEXT_PRO_EMAILOVE_SDILENI." ".help(162)."</th><td><textarea class='input w100 h100' name='obsahEmailu' cols='' rows=''>".$fb['obsahEmailu']."</textarea></td></tr>";
            
            
            
        }
        
        
        
        $ch1 = $ch2 = "";
        if($fb['zobrazit']==1) $ch1 = "checked = 'checked'"; else $ch2 = "checked = 'checked'";
    	echo "<tr><th>".TZOBRAZOVAT." ".help(123)."</th><td>";
    	echo "<input class='radio' type='radio' name='zobrazit' value='1' ".$ch1."/>".TANO;
    	echo "<input class='radio' type='radio' name='zobrazit' value='0' ".$ch2."/>".TNE;
    	echo "</td></tr>";
        
        echo "</table>";
        echo "<div class='dolni-tlacitka'>";
       
        echo "<input type='hidden' name='idPluginu' value=".$idPlugin." />";
       
		if($login_obj->UserPrivilege('settings_edit'))
			echo "<input class='tool_button save_button' type='submit' name='btnUlozNastaveniPluginu' value='".TULOZIT."' title='".TULOZIT."' />";
			else
			echo "<input class='tool_button_disabled save_button_disabled' type='submit' name='btnUlozNastaveniPluginu' value='".TULOZIT."' title='".TULOZIT."' disabled='disabled'/>";
		

		echo "</div>";
        echo "</form>";    
            
    }    
    
    
}

?>