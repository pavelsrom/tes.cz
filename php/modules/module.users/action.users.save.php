<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined('SECURITY_CMS')) exit;
if(!is_action() || 
    (!$this->is_access('uzivatele','edit-settings',0,1,'settings_edit') &&
    !$this->is_access('uzivatele','new',0,0,'settings_add')
    )) return;
    
//UPRAVY VSTUPU PRED ZAPSANIM DO DATABAZE
$idUzivatele = $this->get_id_action();	 
$heslo = get_post('heslo','',false);
$jmeno = get_post('jmeno');
$login = get_post('login');
$prijmeni = get_post('prijmeni');
$firma = get_post('firma');
$ico = get_post('ico');
$dic = get_post('dic');
$email = get_post('email');
$telefon = get_post('telefon');
$stav = get_int_post('aktivni');
$poznamka = get_post('poznamka');
$psc = get_post('psc');
$mesto = get_post('mesto');
$ulice = get_post('ulice');
$prava = get_int_post('idPrava');

$moduly = get_array_post('moduly');
$domeny = get_array_post('domeny');
$jazyky = get_array_post('jazyky');
$skupiny = get_array_post('skupiny');


            
//TESTOVANI SPRAVNOSTI VSTUPU
$set = '';
			
//testovani spravnosti loginu
if(!TestStringLoginEN($login, 250, false)) 
    $systemMessage->add_error(ERROR_LOGIN); 
				
//testovani duplicity loginu
if($idUzivatele==0) 
    $duplicita = $db->Query("SELECT email FROM ".TABLE_UZIVATELE." WHERE login LIKE '".strtolower($login)."'");
    else
    $duplicita = $db->Query("SELECT email FROM ".TABLE_UZIVATELE." WHERE login LIKE '".$login."' AND idUzivatele != ".$idUzivatele);	
	
if($db->numRows($duplicita)!=0)
    $systemMessage->add_error(TEMAIL_PRO_PRIHLASENI_JIZ_EXISTUJE);	
			
//pokud je vybran privatni uzivatel, pak se nemusi moduly vyplnovat
if($prava != 10 && count($moduly) == 0)
    $systemMessage->add_warning(ERROR_ZADNY_MODUL_NEPRIDELEN_UZIVATELI);

if(count($jazyky) == 0)
    $systemMessage->add_warning(ERROR_ZADNY_JAZYK_NEPRIDELEN_UZIVATELI);
            
if(count($domeny) == 0)
    $systemMessage->add_warning(ERROR_ZADNA_DOMENA_NEPRIDELENA_UZIVATELI);
    			
if($idUzivatele==0 && $heslo=='')
    $systemMessage->add_error(ERROR_NEPLATNE_HESLO);
					
if(!TestStringCZ($jmeno, 100, true)) 
    $systemMessage->add_error(ERROR_JMENO_UZIVATELE); 	
                
if(!TestStringCZ($prijmeni, 100, true)) 
    $systemMessage->add_error(ERROR_PRIJMENI_UZIVATELE);

if(!TestIco($ico, true)) 
    $systemMessage->add_error(ERROR_ICO);
				
if(!TestDic($dic, 15, true)) 
	$systemMessage->add_error(ERROR_DIC);
			
if(!TestInt($psc, 6, true)) 
	$systemMessage->add_error(ERROR_PSC);
			
if(!TestSentenceCZ($mesto, 100, true)) 
    $systemMessage->add_error(ERROR_MESTO);

if(!TestLength($ulice, 100, true)) 
    $systemMessage->add_error(ERROR_ULICE);

		
if($systemMessage->error_exists()) return;

$update = array(
    "jmeno"     => $jmeno,
    "prijmeni"  => $prijmeni,
    "login"  => $login,
    "ulice"     => $ulice,
    "mesto"     => $mesto,
    "psc"       => $psc,
    "firma"     => $firma,
    "ico"       => $ico,
    "dic"       => $dic,
    "email"     => $email,
    "telefon"   => $telefon,
    "idPrava"   => $prava,
    "aktivni"   => $stav,
    "poznamka"  => $poznamka
    );	
    
if(($heslo != '' && $idUzivatele>0) || $idUzivatele == 0)
    $update["heslo"] = password_hash($heslo, PASSWORD_DEFAULT);		

    //$update["heslo"] = sha1($heslo);		
			
			
if($idUzivatele > 0)
    $db->update(TABLE_UZIVATELE,$update,"idUzivatele=".$idUzivatele);
else
{

    $update["datumRegistrace"] = "now";
    $update["hash"] = uniqid(rand(0,1000));
    
    
    $db->insert(TABLE_UZIVATELE,$update);
	$idUzivatele = $db->lastId();    
}			


//ulozeni domen
$db->delete(TABLE_UZIVATELE_DOMENY,"WHERE idUzivatele = ".$idUzivatele);
$record1 = $db->query("SELECT idDomeny FROM ".TABLE_DOMENY);
if(count($domeny)>0){
    $res_arr = array();
    while($domena = $db->getAssoc($record1)){
        if(in_array($domena['idDomeny'], $domeny))
				$res_arr[] = "(".$domena['idDomeny'].",".$idUzivatele.")";
    }
	
    $db->Query("INSERT INTO ".TABLE_UZIVATELE_DOMENY." (idDomeny, idUzivatele) VALUE ".implode(',', $res_arr));	
}
                
                
//ulozeni modulu
$db->delete(TABLE_UZIVATELE_MODULY,"WHERE idUzivatele = ".$idUzivatele);
$record1 = $db->query("SELECT idModulu FROM ".TABLE_CMS_MODULY." WHERE zobrazit=1 AND podmodul=0");
if(count($moduly)>0){
   $res_arr = array();
   while($modul = $db->getAssoc($record1)){
       if(in_array($modul['idModulu'], $moduly))
			$res_arr[] = "(".$modul['idModulu'].",".$idUzivatele.")";
       }
	   
    $db->Query("INSERT INTO ".TABLE_UZIVATELE_MODULY." (idModulu, idUzivatele) VALUE ".implode(',', $res_arr));	
	}
                
              
//ulozeni jazyku
$db->delete(TABLE_UZIVATELE_JAZYKY,"WHERE idUzivatele = ".$idUzivatele);
$record1 = $db->query("SELECT idJazyka FROM ".TABLE_JAZYKY." WHERE aktivni=1");
if(count($jazyky)>0){
   $res_arr = array();
   while($jazyk = $db->getAssoc($record1)){
       if(in_array($jazyk['idJazyka'], $jazyky))
			$res_arr[] = "(".$jazyk['idJazyka'].",".$idUzivatele.")";	
       }
   $db->Query("INSERT INTO ".TABLE_UZIVATELE_JAZYKY." (idJazyka, idUzivatele) VALUE ".implode(',', $res_arr));	
} 

//ulozeni skupin
$db->delete(TABLE_UZIVATELE_SKUPINY_RELACE,"WHERE idUzivatele = ".$idUzivatele);
if(count($skupiny)>0){
   $res_arr = array();
   foreach($skupiny AS $idSkupiny)
       $res_arr[] = "(".$idSkupiny.",".$idUzivatele.")";	
   
   $db->Query("INSERT INTO ".TABLE_UZIVATELE_SKUPINY_RELACE." (idSkupiny, idUzivatele) VALUE ".implode(',', $res_arr));	
} 
                                   
if($idUzivatele == $login_obj->getId())
    $login_obj->reload();
         

$log->add_log($this->get_url('id_action') == 0 ? 'create':'edit-settings','uzivatele',$idUzivatele,trim($jmeno." ".$prijmeni));          

$systemMessage->add_ok(OK_ULOZENO);
$url = $this->get_link('uzivatele',0,'edit-settings',$idUzivatele); 
Redirect($url);
            
            
?>