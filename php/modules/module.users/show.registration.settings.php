<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined('SECURITY_CMS')) exit;
if(!$this->is_access('registrace','edit-settings',0,1,'settings_edit') &&
   !$this->is_access('registrace','new',0,0,'settings_view') 
    ) return;
    
$idUser = $this->get_id_action();

//opravneni k editaci ma jen admin, superadmin. Editor a User mohou editovat pouze svuj zaznam
if(($login_obj->UserPrivilege('user') || $login_obj->UserPrivilege('editor')) && $idUser != $login_obj->getId())
    return;


$main_tools = array();
        
		if($idUser == 0){
			$user = array();
			$user['jmeno_uzivatele'] = "";
			$user['prijmeni_uzivatele'] = "";
            $user['email'] = "";
			$user['datumRegistrace'] = "";
			$user['posledniPristupWeb'] = "";
			$user['poznamka'] = "";
			$user['aktivni'] = 1;

            
            
			}
			else
			{
			$record = $db->Query("SELECT uz.*,uz.jmeno AS jmeno_uzivatele, uz.prijmeni AS prijmeni_uzivatele
                    FROM ".TABLE_UZIVATELE." AS uz		
					WHERE uz.idUzivatele=".$idUser." 
					LIMIT 1");

		
			$user = $db->getAssoc($record);

            

        }
		
  
$main_tools[] = array(
    'ikona' => 'back', 
    "nazev" => TZPET, 
    "aktivni" => 0, 
    "odkaz" => $this->get_link('registrace'), 
    "title" => TZPET_NA_SEZNAM_UZIVATELU
    );

echo main_tools($main_tools);
        
$form = new Form();
$form->add_section(TZAKLADNI_UDAJE, "home");
/*
$form->add_text(TJMENO,'jmeno',$user['jmeno_uzivatele'],0,true);
$form->add_text(TPRIJMENI,'prijmeni',$user['prijmeni_uzivatele'],0,true);
*/

$form->add_plain_text(TJMENO,$user['jmeno_uzivatele']);
$form->add_plain_text(TPRIJMENI, $user['prijmeni_uzivatele']);
$form->add_plain_text(TEMAIL, $user['email']);	
$form->add_plain_text(TTELEFON, $user['telefon']);

$form->add_section(TINFORMACE_O_SPOLECNOSTI, "home");
$form->add_plain_text(TSPOLECNOST, $user['firma']);
$form->add_plain_text(TICO, $user['ico']);
$form->add_plain_text(TDIC, $user['dic']);


$form->add_section(TADRESA, "home");
$form->add_plain_text(TULICE, $user['ulice']);
$form->add_plain_text(TCISLO_POPISNE, $user['cislo']);
$form->add_plain_text(TMESTO, $user['mesto']);
$form->add_plain_text(TPSC, $user['psc']);

$form->add_section(TADRESA_DODACI, "home");
$form->add_plain_text(TULICE, $user['ulice_dod']);
$form->add_plain_text(TCISLO_POPISNE, $user['cislo_dod']);
$form->add_plain_text(TMESTO, $user['mesto_dod']);
$form->add_plain_text(TPSC, $user['psc_dod']);

$form->add_section(TNASTAVENI_UZIVATELE, "panel-setting");

if($idUser==0)
    $form->add_password(THESLO,'heslo',"",181,true);
	else
    $form->add_password(TNOVE_HESLO,'heslo',"",182);

$form->add_radiobuttons(TAKTIVNI,'aktivni',$user['aktivni'],null,183);
$form->add_radiobuttons("Velkoobchod",'velkoobchod',$user['velkoobchod']);
$form->add_textarea(TPOZNAMKA,$user['poznamka'],$user['poznamka']);	


//$form->add_section(TPRISTUPOVE_UDAJE,"pass");
   

//skupiny uzivatelu
/*
$d = $db->query("SELECT s.idSkupiny, s.nazev, IF(r.idRelace IS NULL, 0, 1) AS checked 
    FROM ".TABLE_UZIVATELE_SKUPINY." AS s 
    LEFT JOIN ".TABLE_UZIVATELE_SKUPINY_RELACE." AS r ON r.idSkupiny = s.idSkupiny AND r.idUzivatele=".intval($idUser)." 
    GROUP BY s.idSkupiny
    ORDER BY s.idSkupiny
    ");
 
 
$skupiny_vsechny = array();
$skupiny_checked = array();
       
while($j = $db->getAssoc($d))
{
    if($j['checked'] == 1)
        $skupiny_checked[] = $j['idSkupiny'];
        
    $skupiny_vsechny[$j['idSkupiny']] = $j['nazev'];
        
}
            
$form->add_checkboxes(TSKUPINY,'skupiny',$skupiny_checked,$skupiny_vsechny)->set_class_tr("rozsirene_nastaveni");
*/

 		
if($idUser>0 && trim($user['email']!=''))
{

	$predmet = get_secure_post('predmet');	
    $zprava = get_secure_post('zprava',false);
    	
	$form->add_section(TNAPSAT_EMAIL, 'email',help(187,true));
    $form->add_plain_text(TOD,FROM_ADMIN_EMAIL);
    $form->add_plain_text(TKOMU,$user['email']." (".$user['jmeno_uzivatele']." ".$user['prijmeni_uzivatele'].")");
    $form->add_text(TPREDMET,'predmet',$predmet,0,false,"","predmet");
    $form->add_editor(TZPRAVA,'zprava', $zprava,0,false,"wysiwygEditor h300 w100");
    $form->add_plain_text("","<a href='#' id='odeslat_email' class='oe".$idUser."' title='".TODESLAT_EMAIL."'>".TODESLAT_EMAIL."</a>");
		
}

$res = ($login_obj->UserPrivilege('settings_add') && $idUser==0) || ($login_obj->UserPrivilege('settings_edit') && $idUser>0);

$form->add_submit()->set_disabled(!$res);
    

echo $form->get_html_code();

?>


<script type="text/javascript">

<!--
$(function(){
    
    $("select[name='idPrava']").change(function(){
        v = $(this).val();
        m = $(".moduly");
        if(v == 10)
            m.hide();
            else
            m.show();
            
    })
    
    $("select[name='idPrava']").trigger("change");
    
    $("a#odeslat_email").click(function(){
        $.msg();
        var id = $(this).attr("class").substring(2);
        var predmet = $("#predmet").val();
        var editor = CKEDITOR.instances.wysiwygEditor;
        var text = editor.getData();

        $.post("<?php echo AJAX_GATEWAY;?>user.send.email",{id: id, text: text, predmet: predmet},function(data){
            if(data.error== "" && data.ok!= "")
            {
                $.msg( 'setClass', 'ok_message' );
                $.msg( 'replace', data.ok );
            }
            else
            {
                $.msg( 'setClass', 'error_message' );
                $.msg( 'replace', data.error );
            }
            
            $.msg( 'unblock', 1000);
            
                
        },"json");
        
        
        return false;
    })
    
    if(1 == <?php echo intval($login_obj->UserPrivilege('user') || $login_obj->UserPrivilege('editor')); ?>)
        $(".rozsirene_nastaveni").hide();
    
})

// -->
</script>