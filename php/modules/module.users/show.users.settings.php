<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined('SECURITY_CMS')) exit;
if(!$this->is_access('uzivatele','edit-settings',0,1,'settings_edit') &&
   !$this->is_access('uzivatele','new',0,0,'settings_view') 
    ) return;
    
$idUser = $this->get_id_action();

//opravneni k editaci ma jen admin, superadmin. Editor a User mohou editovat pouze svuj zaznam
if(($login_obj->UserPrivilege('user') || $login_obj->UserPrivilege('editor')) && $idUser != $login_obj->getId())
    return;


$main_tools = array();
        
		if($idUser == 0){
			$user = array();
			$user['jmeno_uzivatele'] = "";
			$user['prijmeni_uzivatele'] = "";
            $user['login'] = "";
			$user['firma'] = "";
			$user['ico'] = "";
			$user['dic'] = "";
			$user['email'] = "";
			$user['telefon'] = "";
			$user['idPrava'] = 1;
			$user['datumRegistrace'] = "";
			$user['posledniPristup'] = "";
			$user['poznamka'] = "";
			$user['psc'] = "";
			$user['ulice'] = "";
			$user['mesto'] = "";
			$user['moduly'] = array();
            $user['jazyky'] = array();
            $user['aktivni'] = 1;
            
            
            
			}
			else
			{
			$record = $db->Query("SELECT uz.*,uz.jmeno AS jmeno_uzivatele, uz.prijmeni AS prijmeni_uzivatele
                    FROM ".TABLE_UZIVATELE." AS uz		
					WHERE uz.idUzivatele=".$idUser." 
					LIMIT 1");

		
			$user = $db->getAssoc($record);

            $d = $db->query("SELECT idModulu FROM ".TABLE_UZIVATELE_MODULY." WHERE idUzivatele=".$idUser);   
			$user['moduly'] = array();
            while($m = $db->getAssoc($d))
            {
                $user['moduly'][] = $m['idModulu'];
            }

        }
		
  
$main_tools[] = array(
    'ikona' => 'back', 
    "nazev" => TZPET, 
    "aktivni" => 0, 
    "odkaz" => $this->get_link('uzivatele'), 
    "title" => TZPET_NA_SEZNAM_UZIVATELU
    );

echo main_tools($main_tools);
        
$form = new Form();
$form->add_section(TZAKLADNI_UDAJE, "home");
$form->add_text(TJMENO,'jmeno',$user['jmeno_uzivatele'],0,true);
$form->add_text(TPRIJMENI,'prijmeni',$user['prijmeni_uzivatele'],0,true);
$form->add_text(TEMAIL,'email',$user['email']);	
$form->add_text(TTELEFON,'telefon',$user['telefon']);
$form->add_text(TLOGIN,'login',$user['login'],0,true);
if($idUser==0)
    $form->add_password(THESLO,'heslo',"",181,true);
	else
    $form->add_password(TNOVE_HESLO,'heslo',"",182);
$form->add_radiobuttons(TAKTIVNI,'aktivni',$user['aktivni'],null,183);

if($idUser > 0 && $user['idPrava'] != ID_PRAVA_PRIVATNI)
{
    $form->add_plain_text(TAUTOLOGIN_URL,$domain->getUrl()."admin?login&uid=".$idUser."&p=".$user['heslo']);
}

$form->add_section(TOSTATNI_UDAJE, "panel-setting");
$form->add_text(TSPOLECNOST,'firma',$user['firma']);
$form->add_text(TICO,'ico',$user['ico']);
$form->add_text(TDIC,'dic',$user['dic']);
$form->add_text(TULICE,'ulice',$user['ulice']);
$form->add_text(TMESTO,'mesto',$user['mesto']);
$form->add_text(TPSC,'psc',$user['psc']);
$form->add_textarea(TPOZNAMKA,$user['poznamka'],$user['poznamka']);	

//$form->add_section(TPRISTUPOVE_UDAJE,"pass");
   
$form->add_section(TPRISTUP_K_MODULUM_A_DOMENAM, "pass")->set_class_tr("rozsirene_nastaveni");

if($login_obj->UserPrivilege('superadmin'))
	$data_prava = $db->Query("SELECT idPrava,nazev FROM ".TABLE_CMS_PRAVA." ORDER BY priorita");
    else
    $data_prava = $db->Query("SELECT idPrava,nazev FROM ".TABLE_CMS_PRAVA." WHERE zobrazit=1 ORDER BY priorita");
    
			
$arr_prava = array();
while($p = $db->getAssoc($data_prava))
	$arr_prava[$p['idPrava']] = constant($p['nazev']);

$form->add_selectbox(TPRAVA,"idPrava",$user['idPrava'],$arr_prava,184)->set_class_tr("rozsirene_nastaveni");
		
$domeny_vsechny = array();
$domeny_checked = array();

if($idUser == 0){
	$record2 = $db->Query("SELECT idDomeny, domena FROM ".TABLE_DOMENY);
	if(isset($_POST['domeny']) && count($_POST['domeny'])>0){
		while($domena = $db->getAssoc($record2)){
					
            if(in_array($domena['idDomeny'], $_POST['domeny']))
                $domeny_checked[] =  $domena['idDomeny'];
                        
            $domeny_vsechny[$domena['idDomeny']] = $domena['domena'];            

            }
                    
            
            }
            else
            {
            while($domena = $db->getAssoc($record2))
                $domeny_vsechny[$domena['idDomeny']] = $domena['domena']; 
    					
                }
        }
        else
        {
			$record2 = $db->Query("SELECT idDomeny FROM ".TABLE_UZIVATELE_DOMENY." WHERE idUzivatele=".$idUser);
			$ud = array();
			while($domena1 = $db->getAssoc($record2))
				$ud[] = $domena1['idDomeny'];
			
			
			$record3 = $db->Query("SELECT idDomeny, domena FROM ".TABLE_DOMENY);
			while($domena2 = $db->getAssoc($record3)){
			     if(in_array($domena2['idDomeny'], $ud))
                    $domeny_checked[] = $domena2['idDomeny'];
                    
			     $domeny_vsechny[$domena2['idDomeny']] = $domena2['domena']; 

				}				
    }
		
			
$form->add_checkboxes(TDOMENY,'domeny',$domeny_checked,$domeny_vsechny,186)->set_class_tr("rozsirene_nastaveni");

$moduly_vsechny = array();
$moduly_checked = array();
        
foreach($modules->allModules AS $id => $nazev){
    if(/*!$modules->is_allowed($id,'domain') || */ $modules->is_function($id)) continue;

    if(in_array($id,$user['moduly']))
        $moduly_checked[] = $id;

			
    if($modules->IsBasic($id))	
        $moduly_vsechny[$id] = "<b>".$nazev."</b>";
        else
        $moduly_vsechny[$id] = $nazev;
          	
}

$form->add_checkboxes(TMODULY,'moduly',$moduly_checked,$moduly_vsechny,185)->setSecureString(false)->set_class_tr("rozsirene_nastaveni moduly");
        

$d = $db->query("SELECT j.idJazyka, j.nazev, j.jazyk, IF(r.idRelace IS NULL, 0, 1) AS checked 
    FROM ".TABLE_JAZYKY." AS j 
    LEFT JOIN ".TABLE_DOMENY_JAZYKY." AS d ON j.idJazyka=d.idJazyka 
    LEFT JOIN ".TABLE_UZIVATELE_JAZYKY." AS r ON r.idJazyka = d.idJazyka AND r.idUzivatele=".intval($idUser)."
    WHERE j.aktivni = 1 
    GROUP BY j.idJazyka
    ORDER BY j.idJazyka
    ");
 
 
$jazyky_vsechny = array();
$jazyky_checked = array();
       
while($j = $db->getAssoc($d))
{
    if($j['checked'] == 1)
        $jazyky_checked[] = $j['idJazyka'];
        
    $jazyky_vsechny[$j['idJazyka']] = "<img src='".RELATIVE_PATH."img/jazyky/".$j['jazyk'].".png' alt='".$j['jazyk']."' height='13' /> ".constant($j['nazev']);
        
}
            
$form->add_checkboxes(TJAZYKY,'jazyky',$jazyky_checked,$jazyky_vsechny)->setSecureString(false)->set_class_tr("rozsirene_nastaveni");

//skupiny uzivatelu
$d = $db->query("SELECT s.idSkupiny, s.nazev, IF(r.idRelace IS NULL, 0, 1) AS checked 
    FROM ".TABLE_UZIVATELE_SKUPINY." AS s 
    LEFT JOIN ".TABLE_UZIVATELE_SKUPINY_RELACE." AS r ON r.idSkupiny = s.idSkupiny AND r.idUzivatele=".intval($idUser)." 
    GROUP BY s.idSkupiny
    ORDER BY s.idSkupiny
    ");
 
 
$skupiny_vsechny = array();
$skupiny_checked = array();
       
while($j = $db->getAssoc($d))
{
    if($j['checked'] == 1)
        $skupiny_checked[] = $j['idSkupiny'];
        
    $skupiny_vsechny[$j['idSkupiny']] = $j['nazev'];
        
}
            
$form->add_checkboxes(TSKUPINY,'skupiny',$skupiny_checked,$skupiny_vsechny)->set_class_tr("rozsirene_nastaveni");

		
if($idUser>0 && trim($user['email']!=''))
{

	$predmet = get_secure_post('predmet');	
    $zprava = get_secure_post('zprava',false);
    	
	$form->add_section(TNAPSAT_EMAIL, 'email',help(187,true));
    $form->add_plain_text(TOD,FROM_ADMIN_EMAIL);
    $form->add_plain_text(TKOMU,$user['email']." (".$user['jmeno_uzivatele']." ".$user['prijmeni_uzivatele'].")");
    $form->add_text(TPREDMET,'predmet',$predmet,0,false,"","predmet");
    $form->add_editor(TZPRAVA,'zprava', $zprava,0,false,"wysiwygEditor h300 w100");
    $form->add_plain_text("","<a href='#' id='odeslat_email' class='oe".$idUser." tlacitko' title='".TODESLAT_EMAIL."'>".TODESLAT_EMAIL."</a>");
		
}

$res = ($login_obj->UserPrivilege('settings_add') && $idUser==0) || ($login_obj->UserPrivilege('settings_edit') && $idUser>0);

$form->add_submit()->set_disabled(!$res);
    

echo $form->get_html_code();

?>


<script type="text/javascript">

<!--
$(function(){
    
    $("select[name='idPrava']").change(function(){
        v = $(this).val();
        m = $(".moduly");
        if(v == 10)
            m.hide();
            else
            m.show();
            
    })
    
    $("select[name='idPrava']").trigger("change");
    
    $("a#odeslat_email").click(function(){
        $.msg();
        var id = $(this).attr("class").substring(2);
        var predmet = $("#predmet").val();
        var editor = CKEDITOR.instances.wysiwygEditor;
        var text = editor.getData();

        $.post("<?php echo AJAX_GATEWAY;?>user.send.email",{id: id, text: text, predmet: predmet},function(data){
            if(data.error== "" && data.ok!= "")
            {
                $.msg( 'setClass', 'ok_message' );
                $.msg( 'replace', data.ok );
            }
            else
            {
                $.msg( 'setClass', 'error_message' );
                $.msg( 'replace', data.error );
            }
            
            $.msg( 'unblock', 1000);
            
                
        },"json");
        
        
        return false;
    })
    
    if(1 == <?php echo intval($login_obj->UserPrivilege('user') || $login_obj->UserPrivilege('editor')); ?>)
        $(".rozsirene_nastaveni").hide();
    
})

// -->
</script>