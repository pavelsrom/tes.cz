<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined('SECURITY_CMS')) exit;
if(!$this->is_access('skupiny','edit-settings',0,1,'settings_edit') &&
   !$this->is_access('skupiny','new',0,0,'settings_view') 
    ) return;
    
$idSkupiny = $this->get_id_action();

//opravneni k editaci ma jen admin, superadmin. Editor a User mohou editovat pouze svuj zaznam
if(!$login_obj->minPrivilege('user'))
    return;


$main_tools = array();
        
		if($idSkupiny == 0){
			$group = array();
			$group['nazev'] = "";
            $group['aktivni'] = 1;
            
            
            
			}
			else
			{
			$record = $db->Query("SELECT nazev, aktivni
                    FROM ".TABLE_UZIVATELE_SKUPINY."		
					WHERE idSkupiny=".$idSkupiny." 
					LIMIT 1");

		
			$group = $db->getAssoc($record);
            

        }
		
  
$main_tools[] = array(
    'ikona' => 'back', 
    "nazev" => TZPET, 
    "aktivni" => 0, 
    "odkaz" => $this->get_link('skupiny'), 
    "title" => TZPET_NA_SEZNAM_SKUPIN
    );

echo main_tools($main_tools);
        
$form = new Form();
$form->add_section(TZAKLADNI_UDAJE, "home");
$form->add_text(TNAZEV,'nazev',$group['nazev'],0,true);
$form->add_radiobuttons(TAKTIVNI,'aktivni',$group['aktivni']);


$res = ($login_obj->UserPrivilege('settings_add') && $idSkupiny==0) || ($login_obj->UserPrivilege('settings_edit') && $idSkupiny>0);

$form->add_submit()->set_disabled(!$res);
    

echo $form->get_html_code();

?>
