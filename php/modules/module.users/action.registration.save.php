<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined('SECURITY_CMS')) exit;
if(!is_action() || 
    (!$this->is_access('registrace','edit-settings',0,1,'settings_edit') &&
    !$this->is_access('registrace','new',0,0,'settings_add')
    )) return;
    
//UPRAVY VSTUPU PRED ZAPSANIM DO DATABAZE
$idUzivatele = $this->get_id_action();	 
$heslo = get_post('heslo','',false);
$jmeno = get_post('jmeno');
$login = get_post('email');
$prijmeni = get_post('prijmeni');
$email = get_post('email');
$stav = get_int_post('aktivni');
$velkoobchod = get_int_post('velkoobchod');
$poznamka = get_post('poznamka');

$prava = 10;
//$skupiny = get_array_post('skupiny');


            
//TESTOVANI SPRAVNOSTI VSTUPU
$set = '';
			
//testovani spravnosti loginu
/*
if(!TestStringLoginEN($login, 250, false)) 
    $systemMessage->add_error(ERROR_LOGIN); 
*/				
//testovani duplicity loginu
/*
if($idUzivatele==0) 
    $duplicita = $db->Query("SELECT email FROM ".TABLE_UZIVATELE." WHERE login LIKE '".strtolower($login)."'");
    else
    $duplicita = $db->Query("SELECT email FROM ".TABLE_UZIVATELE." WHERE login LIKE '".$login."' AND idUzivatele != ".$idUzivatele);	
	
if($db->numRows($duplicita)!=0)
    $systemMessage->add_error(TEMAIL_PRO_PRIHLASENI_JIZ_EXISTUJE);	
			  			
if($idUzivatele==0 && $heslo=='')
    $systemMessage->add_error(ERROR_NEPLATNE_HESLO);
					
if(!TestStringCZ($jmeno, 100, true)) 
    $systemMessage->add_error(ERROR_JMENO_UZIVATELE); 	
                
if(!TestStringCZ($prijmeni, 100, true)) 
    $systemMessage->add_error(ERROR_PRIJMENI_UZIVATELE);
*/

		
if($systemMessage->error_exists()) return;

$update = array(
    /*
    "jmeno"     => $jmeno,
    "prijmeni"  => $prijmeni,
    "login"  => $login,
    "email"     => $email,
    "idPrava"   => $prava,
    */
    "aktivni"   => $stav,
    "velkoobchod"   => $velkoobchod,
    "poznamka"  => $poznamka
    );	
    
if(($heslo != '' && $idUzivatele>0) || $idUzivatele == 0)
    $update["heslo"] = password_hash($heslo, PASSWORD_DEFAULT);		

    //$update["heslo"] = sha1($heslo);		
			
			
if($idUzivatele > 0)
    $db->update(TABLE_UZIVATELE,$update,"idUzivatele=".$idUzivatele);
else
{
    //generovani kodu
    $refkod = "";   
    for($i = 0; $i < 100; $i++)
    {
        $refkod = password_gen(8);
        
        $d = $db->query("SELECT refkod FROM ".TABLE_UZIVATELE." WHERE refkod='".$refkod."'");
        if($db->numRows($d) > 0)
        {
            continue;
        }
        else
        break;
    
            
    }


    $update["datumRegistrace"] = "now";
    $update["hash"] = uniqid(rand(0,1000));
    $db->insert(TABLE_UZIVATELE,$update);
	$idUzivatele = $db->lastId();
    
    //ulozeni vazeb
    $db->insert(TABLE_UZIVATELE_DOMENY, array("idUzivatele" => $idUzivatele, "idDomeny" => $domain->getId()));
    $db->insert(TABLE_UZIVATELE_JAZYKY, array("idUzivatele" => $idUzivatele, "idJazyka" => WEB_LANG_ID));
    $db->insert(TABLE_UZIVATELE_SKUPINY_RELACE, array("idUzivatele" => $idUzivatele, "idSkupiny" => 1));
        
}			




//ulozeni skupin
/*
$db->delete(TABLE_UZIVATELE_SKUPINY_RELACE,"WHERE idUzivatele = ".$idUzivatele);
if(count($skupiny)>0){
   $res_arr = array();
   foreach($skupiny AS $idSkupiny)
       $res_arr[] = "(".$idSkupiny.",".$idUzivatele.")";	
   
   $db->Query("INSERT INTO ".TABLE_UZIVATELE_SKUPINY_RELACE." (idSkupiny, idUzivatele) VALUE ".implode(',', $res_arr));	
} 
  
 */                                 
        

$log->add_log($this->get_url('id_action') == 0 ? 'create':'edit-settings','uzivatele',$idUzivatele,trim($jmeno." ".$prijmeni));          

$systemMessage->add_ok(OK_ULOZENO);
$url = $this->get_link('registrace',0,'edit-settings',$idUzivatele); 
Redirect($url);
            
            
?>