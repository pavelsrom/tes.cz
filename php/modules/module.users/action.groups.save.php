<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined('SECURITY_CMS')) exit;
if(!is_action() || 
    (!$this->is_access('skupiny','edit-settings',0,1,'settings_edit') &&
    !$this->is_access('skupiny','new',0,0,'settings_add')
    )) return;
    
//UPRAVY VSTUPU PRED ZAPSANIM DO DATABAZE
$idSkupiny = $this->get_id_action();	 
$nazev = get_post('nazev');
$aktivni = get_int_post('aktivni');
            
//TESTOVANI SPRAVNOSTI VSTUPU
			
if(!TestSentenceCZ($nazev, 200, false))
    $systemMessage->add_error(ERROR_NEPLATNY_NAZEV);
    	
if($systemMessage->error_exists()) return;

$update = array(
    "idDomeny"  => $domain->getId(),
    "nazev"     => $nazev,
    "aktivni"   => $aktivni
    );	
    
		
if($idSkupiny > 0)
    $db->update(TABLE_UZIVATELE_SKUPINY,$update,"idSkupiny=".$idSkupiny);
else
{

    $db->insert(TABLE_UZIVATELE_SKUPINY,$update);
	$idSkupiny = $db->lastId();    
}			

$log->add_log($this->get_url('id_action') == 0 ? 'create':'edit-settings','uzivatele-skupiny',$idSkupiny,$nazev);          

$systemMessage->add_ok(OK_ULOZENO);
$url = $this->get_link('skupiny',0,'edit-settings',$idSkupiny); 
Redirect($url);
            
            
?>