<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined('SECURITY_CMS')) exit;
if(!$this->is_access('uzivatele','navstevy',0,1,'settings_edit')) 
    return;
    
$idUzivatele = $this->get_id_action();

//opravneni k editaci ma jen admin, superadmin. Editor a User mohou editovat pouze svuj zaznam
if(!$login_obj->minPrivilege('admin'))
    return;

$main_tools[] = array(
    'ikona' => 'back', 
    "nazev" => TZPET, 
    "aktivni" => 0, 
    "odkaz" => $this->get_link('uzivatele'), 
    "title" => TZPET_NA_SEZNAM_UZIVATELU
    );

echo main_tools($main_tools);

$html = "";

$data = $db->query("SELECT DATE_FORMAT(l.datum, '%d.%m.%Y') AS datum, COUNT(l.id) AS pocet, s.nazev, l.idStranky AS id 
    FROM ".TABLE_UZIVATELE_LOG." AS l
    LEFT JOIN ".TABLE_STRANKY." AS s ON l.idStranky = s.idStranky
    WHERE idUzivatele=".$idUzivatele."
    GROUP BY l.id
    ORDER BY l.datum DESC
    ");

$posledni_datum = "";
$vypis = array();
while($u = $db->getObject($data))
{
    if($posledni_datum != $u->datum)
    {
        $vypis[$u->datum] = array();
        $posledni_datum = $u->datum;
    }
    
    if(!isset($vypis[$u->datum][$u->id]))
        $vypis[$u->datum][$u->id] = array("pocet" => 1, "nazev" => $u->nazev);
        else
        $vypis[$u->datum][$u->id]["pocet"]++;
       
}
    
$table = new Table("tNastaveni");
$table->tr_head()->add_th(TSTRANKA,"t-left")->add_th(TPOCET_ZOBRAZENI);

$posledni_datum = "";
foreach($vypis AS $datum => $stranky)
{
    if($posledni_datum != $datum)
    {
        $table->tr("sekce")->add($datum,"nazev-sekce t-left",0,"",2);
        $html .= '<h3>'.$datum.'</h3>';
        $posledni_datum = $datum;
    }
    
    foreach($stranky AS $id => $s)
        $table->tr()->add($s['nazev'],"t-left")->add($s['pocet']."x");
        //$html .= $s['nazev']." (<b>".$s['pocet']."x</b>)<br />";
      
}


echo $table->get_html();







?>
