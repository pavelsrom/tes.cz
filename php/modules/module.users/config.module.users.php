<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */


//------------------------------------------------
//definice polozek menu
//------------------------------------------------

$module_menu = array();

if($login_obj-> minPrivilege('superadmin'))
    $module_menu[] = array(
        "page"  => "uzivatele",
        "name"  => TSEZNAM_UZIVATELU
        );
    

$module_menu[] = array(
    "page"  => "registrace",
    "name"  => TSEZNAM_REGISTROVANYCH_UZIVATELU
    );
        
$module_menu[] = array(
    "page"  => "skupiny",
    "name"  => TSEZNAM_SKUPIN
    );
         
//------------------------------------------------
//definice nadpisu h1 a textu k nadpisu
//------------------------------------------------ 
$module_h1 = array();


$module_h1[] = array(
    "page"  => "uzivatele",
    "action"=> "edit-settings",
    "verify_id_action" => 1,
    "name"  => TEDITACE_UZIVATELE
    );
$module_h1[] = array(
    "page"  => "uzivatele",
    "action"=> "new",
    "name"  => TNOVY_UZIVATEL
    );    
$module_h1[] = array(
    "page"  => "uzivatele",
    "name"  => TSEZNAM_UZIVATELU,
    "text"  => 376
    ); 
    
$module_h1[] = array(
    "page"  => "registrace",
    "action"=> "edit-settings",
    "verify_id_action" => 1,
    "name"  => TEDITACE_UZIVATELE
    );
$module_h1[] = array(
    "page"  => "registrace",
    "action"=> "new",
    "name"  => TNOVY_UZIVATEL
    );    
$module_h1[] = array(
    "page"  => "registrace",
    "name"  => TSEZNAM_REGISTROVANYCH_UZIVATELU
    );
    
$module_h1[] = array(
    "page"  => "skupiny",
    "action"=> "edit-settings",
    "verify_id_action" => 1,
    "name"  => TEDITACE_SKUPINY
    );
$module_h1[] = array(
    "page"  => "skupiny",
    "action"=> "new",
    "name"  => TNOVA_SKUPINA
    );    
$module_h1[] = array(
    "page"  => "skupiny",
    "name"  => TSEZNAM_SKUPIN,
    "text"  => 404
    );
    
$module_h1[] = array(
    "page"  => "uzivatele",
    "action"=> "navstevy",
    "verify_id_action" => 1,
    "name"  => TNAVSTEVY_UZIVATELU
    );
    
?>