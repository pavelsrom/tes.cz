<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */


//------------------------------------------------
//definice polozek menu
//------------------------------------------------

$module_menu = array();
$module_menu[] = array(
    "page"  => "novinky",
    "name"  => TSEZNAM_NOVINEK." ".WEB_LANG
    );
$module_menu[] = array(
    "page"  => "nastaveni",
    "name"  => TNASTAVENI
    );
    
//------------------------------------------------
//definice nadpisu h1 a textu k nadpisu
//------------------------------------------------ 
$module_h1 = array();
$module_h1[] = array(
    "page"  => "novinky",
    "action"=> "edit-content",
    "verify_id_action" => 1,
    "name"  => TEDITACE_NOVINKY." ".WEB_LANG
    );

$module_h1[] = array(
    "page"  => "novinky",
    "action"=> "new",
    "name"  => TNOVA_NOVINKA." ".WEB_LANG
    );    
$module_h1[] = array(
    "page"  => "novinky",
    "name"  => TSEZNAM_NOVINEK." ".WEB_LANG,
    "text"  => 357
    ); 
$module_h1[] = array(
    "page"  => "nastaveni",
    "name"  => TNASTAVENI
    );



?>