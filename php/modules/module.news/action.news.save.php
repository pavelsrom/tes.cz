<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */


if(!defined('SECURITY_CMS')) exit;
if(!is_action() ||
    (!$this->is_access('novinky','edit-content',0,1,'content_edit') &&
     !$this->is_access('novinky','new',0,0,'content_add'))
     || !$object_access->has_access($this->get_id_action())
    ) return;
    
$idStranky = $this->get_id_action();
$titulek = $nadpis = get_post('titulek');
$file = get_post('obrazek');
$url = get_post('url');
$title = get_post('title');

if($idStranky==0 && $title=='')
    $title = $titulek;
            
$desc = get_post('description');
$perex = get_post('perex','',false);
$zobrazit = get_int_post('zobrazit');
$autentizace = get_int_post('autentizace');
            
$od = get_post('od');
$do = get_post('do');
$datum = get_post('datum');
$datum = GetUniDate($datum);
            
$idProfiluPanelu = get_int_post('idProfiluPanelu');
$idGalerie = get_int_post('idGalerie');    
$idAnkety = get_int_post('idAnkety');
$diskuze = get_int_post('diskuze');
$obsah = get_post('obsah','',false);
            
$url = $idStranky==0 && $url=='' ? ModifyUrl($titulek) : ModifyUrl($url);
$url_new = get_url_nazev($idStranky, $url);
if($url!=$url_new)
    $systemMessage->add_warning(WARNING_URLNAZEV_MODIFIKOVAN." ".$url_new);
$url = $url_new;

if($url == "")
    $systemMessage->add_error(ERROR_NEPLATNY_URL_NAZEV);

$update = array();

if($datum!='')
    $update["datum"] = $datum;
	else
	$update["datum"] = "null";

if(!TestSentenceCZ($titulek, 200, false))
    $systemMessage->add_error(ERROR_PRAZDNY_TITULEK);
			

if(!TestDate($od, true) || !TestDate($do, true)){
    $systemMessage->add_error(ERROR_CHYBNE_DATUM);
    }
    else
    {
    if(!TestTwoDate($od, $do)){
        $systemMessage->add_warning(WARNING_CHYBNE_DATUM);
        }
			
    $od = GetUniDate($od);
    $do = GetUniDate($do);
    if($od!='') 
        $update["od"] = $od;
        else    
        $update["od"] = "null";
    					
    if($do!='')
        $update["do"] = $do;
        else    
        $update["do"] = "null";
    }

			
if($systemMessage->error_exists()) return;

$idStrankyNovinky = $db->get(TABLE_STRANKY,"idStranky", "typ='novinky' AND idDomeny=".$domain->getId()." AND idJazyka=".WEB_LANG_ID);
$idStrankyNovinky = intval($idStrankyNovinky);

$update = $update + array(
    "idDomeny"      => $domain->getId(),
    "typ"           => "novinka",
    "nazev"         => $titulek,
    "idRodice"      => $idStrankyNovinky,
    "nazevProMenu"  => $titulek,
    "nadpis"        => $nadpis,
    "perex"         => $perex,
    "perexBezHtml"  => strip_tags($perex),
    //"obrazek"       => $file,
    "zobrazit"      => $zobrazit,
    "title"         => $title,
    "description"   => $desc,
    "url"           => $url,
    "idProfiluPanelu"=>$idProfiluPanelu,
    "idGalerie"     => $idGalerie,
    "idAnkety"      => $idAnkety,
    "diskuze"       => $diskuze,
    "obsah"         => $obsah,
    "obsahBezHtml"  => strip_tags($obsah),
    "idJazyka"      => WEB_LANG_ID,
    "autentizace"   => $autentizace
    );			
		

        
if($idStranky == 0){
    $update['idVytvoril'] = $login_obj->getId();
    $update['jmenoVytvoril'] = $login_obj->getName();
    $update['datumVytvoril'] = "now";
    
    $update['idAktualizoval'] = $login_obj->getId();
    $update['jmenoAktualizoval'] = $login_obj->getName();
    $update['datumAktualizoval'] = "now";
    
    $db->insert(TABLE_STRANKY, $update);
    $idStranky = $db->lastId();
    $object_access->add_allowed_object($idStranky);
    }
	else
    {
    $update['idAktualizoval'] = $login_obj->getId();
    $update['jmenoAktualizoval'] = $login_obj->getName();
    $update['datumAktualizoval'] = "now";
	$db->update(TABLE_STRANKY,$update,"idStranky=".$idStranky." AND idDomeny=".$domain->getId());
	}			




if($file == '' && isset($_FILES['obrazek']) && $_FILES['obrazek']['error']!=4){
    $upload = $_FILES['obrazek'];
	$news_path = $domain->getDir().USER_DIRNAME_NEWS;
            
    include_once('php/classes/class.upload.php');
     
    $uploader = new upload($_FILES['obrazek'], 'cs_CS');
    $uploader->file_new_name_body = $idStranky;
    $uploader->file_overwrite = true;
    $uploader->image_resize = true;
    $uploader->image_x = intval(OBRAZEK_NOVINKY_MINI_X);
    $uploader->image_y = intval(OBRAZEK_NOVINKY_MINI_Y);
    $uploader->image_ratio = true;
    $uploader->image_convert = 'jpg';  

    $uploader->process($news_path);
	$uploader->clean();
    $file = $idStranky.".jpg";
            
    

    $db->update(TABLE_STRANKY,array("obrazek" => $file),"idDomeny=".$domain->getId()." AND idStranky=".$idStranky." AND typ='novinka' LIMIT 1");
				
}

$skupiny = get_array_post('skupiny');
$db->delete(TABLE_STRANKY_SKUPINY,"WHERE idStranky=".$idStranky);
if(count($skupiny)>0)
{
    foreach($skupiny AS $s)
    {
        $insert = array("idStranky" => $idStranky, "idSkupiny" => $s);
        $db->insert(TABLE_STRANKY_SKUPINY,$insert);
    }
}


if(MODULE_CONTENT_BACKUP)
{
    $content_backup = new ContentBackup($domain->getId(),$idStranky,$login_obj->getId());
    $content_backup->AddStore($obsah,$perex);
}


$chybne_odkazy_indik = false;

if(MODULE_LINK_CHECKER)
{
    $chybne_odkazy = new LinksChecker($domain->getId(), $idStranky, "stranka", $obsah, false, KONTROLOVAT_ODKAZY_PRI_UKLADANI);
    $chybne_odkazy->set_base_url( $domain->getUrl() );
    $chybne_odkazy->process();
    $pocet_chybnych_obrazku = $chybne_odkazy->get_sum_error_images();
    $pocet_chybnych_odkazu = $chybne_odkazy->get_sum_error_links();
    
    if($pocet_chybnych_obrazku > 0)
        $systemMessage->add_warning(TPOCET_CHYBNYCH_OBRAZKU_NA_STRANCE.": ".$pocet_chybnych_obrazku);
        
    if($pocet_chybnych_odkazu > 0)
        $systemMessage->add_warning(TPOCET_CHYBNYCH_ODKAZU_NA_STRANCE.": ".$pocet_chybnych_odkazu);
    
    if(!$chybne_odkazy_indik && ($pocet_chybnych_obrazku > 0 || $pocet_chybnych_odkazu > 0)) 
        $chybne_odkazy_indik = true;
        





    $chybne_odkazy = new LinksChecker($domain->getId(), $idStranky, "stranka", $perex, true, KONTROLOVAT_ODKAZY_PRI_UKLADANI);
    $chybne_odkazy->set_base_url( $domain->getUrl() );
    $chybne_odkazy->process();
    $pocet_chybnych_obrazku = $chybne_odkazy->get_sum_error_images();
    $pocet_chybnych_odkazu = $chybne_odkazy->get_sum_error_links();
    
    if($pocet_chybnych_obrazku > 0)
        $systemMessage->add_warning(TPOCET_CHYBNYCH_OBRAZKU_V_PEREXU.": ".$pocet_chybnych_obrazku);
        
    if($pocet_chybnych_odkazu > 0)
        $systemMessage->add_warning(TPOCET_CHYBNYCH_ODKAZU_V_PEREXU.": ".$pocet_chybnych_odkazu);
    
    if(!$chybne_odkazy_indik && ($pocet_chybnych_obrazku > 0 || $pocet_chybnych_odkazu > 0)) 
        $chybne_odkazy_indik = true;
    
}


if($chybne_odkazy_indik)
    $systemMessage->add_warning(TODKAZY_ZKONTROLUJTE_A_OPRAVTE);
 
$log->add_log($this->get_url('id_action') == 0 ? 'create':'edit-content','novinka',$idStranky,$titulek);
 
$systemMessage->add_ok(OK_ULOZENO);

set_next_action_code();
$dalsi_akce = get_next_action_code();
if($dalsi_akce == 0) //zpet na vypis
    $url = $this->get_link('novinky');
elseif($dalsi_akce == 1) //zpet na editaci
    $url = $this->get_link('novinky',0,'edit-content',$idStranky);
elseif($dalsi_akce == 2) //vytvorit dalsi
    $url = $this->get_link('novinky',0,'new');


Redirect($url)

?>