<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined('SECURITY_CMS')) exit;
if(!$this->is_access('zalohy_db') || (!$login_obj->UserPrivilege('superadmin') && !$login_obj->UserPrivilege('admin'))) return;
 	
$main_tools = array();
       
if($login_obj->UserPrivilege('settings_add'))
    $main_tools[] = array(
        'ikona' => 'new', 
        "nazev" => TVYTVORIT_ZALOHU, 
        "aktivni" => 0, 
        "odkaz" => $this->get_link('zalohy_db',0,'new'),
        "id"    => "vytvorit_zalohu"
        );
	
		
$form = new FormFilter();
$form->add_text(TDATUM, 'datum', "", 0, false, "kalendar", "vyraz");
echo $form->get_html_code(main_tools($main_tools));
        
$table = new Table('tList');
$table->tr_head()
    ->add(TID,'w25')
    ->add(TJMENO_A_PRIJMENI,'t-left')
    ->add(TDATUM,'w150')
    ->add(TVELIKOST,'w150')
    ->add(TAKCE,'w90');
    
echo $table->get_html();

?>
<script type="text/javascript">
<!--

$(function(){
var oTable = $("#tList").dataTable({
    "sAjaxSource": "<?php echo AJAX_GATEWAY;?>db.backup.list",
    "aoColumns": [
        {"sClass": "sl1"},{"sClass": "sl2 t-left"},{"sClass": "sl3"},{"sClass": "sl4"},{"sClass": "akce", "bSortable": false}
        ],
    "fnServerData": function ( sSource, aoData, fnCallback ) {
            aoData.push({ "name": "vyraz", "value": $("#vyraz").val() });
            $.getJSON( sSource, aoData, function (json) { fnCallback(json); } );
            }
 
})

$("input#vyraz").bind('keyup change', function() {
        oTable.fnDraw(); 
        });

//prida k bunce th span
$(".dataTable th").each(function(){
    $(this).html("<span>" + $(this).html() + "</span>");
});

$("#vytvorit_zalohu a").click(function(){

    $.msg();
    $.post("<?php echo AJAX_GATEWAY;?>db.backup.create",null,function(data){
        
        if(data == "")
        {
            $.msg( 'setClass', 'ok_message' );
            $.msg( 'replace', '<?php echo OK_ZALOHA_VYTVORENA;?>' );
        }
        else
        {
            $.msg( 'setClass', 'error_message' );
            $.msg( 'replace', '<?php echo ERROR_NELZE_VYTVORIT_ZALOHU.": ";?>' + data );
        }
        
        oTable.fnDraw();
        $.msg( 'unblock', 1000);
        
        
        })
    
    return false;
    
    
    })
    
$(document).on("click","a.delete", function(){
        var id = $(this).attr('id').substring(6);
        var tr = $(this).parents("tr").first();
        var c = $(this).parents("tr").find("td").first().text(); 
        
        var text = "<?php echo TOPRAVDU_SMAZAT;?> ID ";

        custom_confirm(text.replace("%s",c) + '?', function(){
            $.msg();
            tr.addClass("deleted");
            
            $.post("<?php echo AJAX_GATEWAY;?>db.backup.delete",{id: id},function(data){
                tr.fadeOut(function(){
                    $.msg( 'setClass', 'ok_message' );
                    $.msg( 'replace', '<?php echo OK_SMAZANO;?>' );
                    oTable.fnDraw();
                    $.msg( 'unblock', 1000);
                    });
                });
            
            })
        
        return false;
        
        
    });
    
$(document).on("click","a.obnova", function(){
        var id = $(this).attr('id').substring(6);
        var tr = $(this).parents("tr").first();
        var c = $(this).parents("tr").find("td").first().text(); 
        
        var text = "<?php echo TOPRAVDU_OBNOVIT_DB;?>";

        confirm(text.replace("%s",c) + '?', function(){
            $.msg();
            tr.addClass("saved");
            
            $.post("<?php echo AJAX_GATEWAY;?>db.backup.restore",{id: id},function(data){
                tr.fadeOut(function(){
                    $.msg( 'setClass', 'ok_message' );
                    $.msg( 'replace', '<?php echo OK_DB_OBNOVENA;?>' );
                    oTable.fnDraw();
                    $.msg( 'unblock', 1000);
                    });
                });
            
            })
        
        return false;
        
        
    });
    


})

// -->
</script>