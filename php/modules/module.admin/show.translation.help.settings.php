<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */


if(!defined('SECURITY_CMS')) exit;
if(!$this->is_access('preklady-napovedy','edit-settings',0,1,'translations') &&
   !$this->is_access('preklady-napovedy','new',0,0,'translations') 
    ) return;
    
$idPrekladu = $this->get_id_action();

   
$main_tools = array();
        
if($idPrekladu == 0){
    $preklad = array();
	$preklad['nadpis'] = "";
	$preklad['cz'] = "";
	$preklad['en'] = "";
			
	}
	else
	{			 
	$data = $db->Query("SELECT idNapovedy,nadpis,cz,en FROM ".TABLE_NAPOVEDA." WHERE idNapovedy=".$idPrekladu);
	$preklad = $db->getAssoc($data);
    }
		
  
$main_tools[] = array(
    'ikona' => 'back', 
    "nazev" => TZPET, 
    "aktivni" => 0, 
    "odkaz" => $this->get_link('preklady-napovedy'), 
    "title" => TZPET_NA_PREKLADY_NAPOVEDY
    );

echo main_tools($main_tools);
        
$form = new Form();
$form->add_section(TZAKLADNI_UDAJE, "home");

$form->add_text(TNADPIS,'nadpis',$preklad['nadpis'],0,true);    
$form->add_editor("CZ",'cz',$preklad['cz'],0,false,"wysiwygEditor h100 w100","");
$form->add_editor("EN",'en',$preklad['en'],0,false,"wysiwygEditor h100 w100","");

$disabled = !(($login_obj->UserPrivilege('settings_add') && $idPrekladu==0) || ($login_obj->UserPrivilege('settings_edit') && $idPrekladu>0));
$form->add_submit()->set_disabled($disabled);
echo $form->get_html_code();
   
?>