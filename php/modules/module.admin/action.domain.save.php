<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */


if(!defined('SECURITY_CMS')) exit;
if(!is_action() || 
    (!$this->is_access('domeny','edit-settings',0,1,'settings_edit') &&
   !$this->is_access('domeny','new',0,0,'settings_add')) 
    ) return;

$idDomeny = $this->get_id_action();				
$domena = strtolower(get_post('domena'));
$adr = get_post('adresar');
$www = get_int_post('www');
$urlStruktura = get_post('urlStruktura','id-nazev');
$urlKoncovka = get_post('urlKoncovka');
            
$pravyPanel = intval(isset($_POST['panely']) && in_array('pravy',$_POST['panely']));
$levyPanel = intval(isset($_POST['panely']) && in_array('levy',$_POST['panely']));
$dolniPanel = intval(isset($_POST['panely']) && in_array('dolni',$_POST['panely']));
$horniPanel = intval(isset($_POST['panely']) && in_array('horni',$_POST['panely']));
$zobrazit = get_int_post('zobrazit');

$jazyky = isset($_POST['jazyky']) ? (array)$_POST['jazyky'] : array();
$moduly = isset($_POST['moduly']) ? (array)$_POST['moduly'] : array();
			
$set = '';

if(!TestDomain($domena) || $domena=='')
    $systemMessage->add_error(ERROR_NAZEV_DOMENY);

if(count($moduly) == 0)
    $systemMessage->add_warning(ERROR_ZADNY_MODUL_NEPRIDELEN);
    
if(count($jazyky) == 0)
    $systemMessage->add_warning(ERROR_ZADNY_JAZYK_NEPRIDELEN);
       
//overeni existence domeny
$name1 = GetDomainWithoutWww($domena);
$name2 = GetDomainWithWww($domena); 
		
if($name1!=$name2) 
    $cond = "(domena='".$name1."' OR domena='".$name2."')";
	else
	$cond = "domena='".$name1."'";
		
$idDomeny_test = $db->get(TABLE_DOMENY,"idDomeny","(domena='".$name1."' OR domena='".$name2."') AND idDomeny!=".$idDomeny);
$idDomeny_test = intval($idDomeny_test);

if($idDomeny_test > 0){
	$systemMessage->add_error(ERROR_DOMENA_EXISTUJE);
	$systemMessage->add_warning(WARNING_DOMENA_EDITACE." <a href='".$this->get_link('domeny',0,'edit-settings',$idDomeny_test)."'>".TZDE."</a>.");
	return false;
	}
       
       
if($systemMessage->error_exists()) return;
			
$update = array(
    "domena"        => $domena,
    "www"           => $www,
    "adresar"       => $adr,
    "zobrazit"      => $zobrazit,
    "urlStruktura"  => $urlStruktura,
    "urlKoncovka"   => $urlKoncovka
    );
            	
if($idDomeny>0)
    $db->update(TABLE_DOMENY,$update,"idDomeny=".$idDomeny);

else
{
    $update['hash'] = sha1(uniqid());
    $db->insert(TABLE_DOMENY,$update);
    $idDomeny = $db->lastId();                   
}

//ulozeni modulu
$db->query("DELETE FROM ".TABLE_DOMENY_MODULY." WHERE idDomeny = ".$idDomeny);
$record1 = $db->query("SELECT idModulu FROM ".TABLE_CMS_MODULY." WHERE zobrazit=1");
    				
if(count($moduly)>0){
    $res_arr = array();
    while($modul = $db->getAssoc($record1)){
        if(in_array($modul['idModulu'], $moduly))
            $res_arr[] = "(".$modul['idModulu'].",".$idDomeny.")";
        }
    $db->Query("INSERT INTO ".TABLE_DOMENY_MODULY." (idModulu, idDomeny) VALUE ".implode(',', $res_arr));	
}
                     
    
//ulozeni jazyku
$db->query("DELETE FROM ".TABLE_DOMENY_JAZYKY." WHERE idDomeny = ".$idDomeny);
$record1 = $db->query("SELECT idJazyka FROM ".TABLE_JAZYKY." WHERE aktivni=1");
    				
if(count($jazyky)>0){
    $res_arr = array();
    while($jazyk = $db->getAssoc($record1)){
  		if(in_array($jazyk['idJazyka'], $jazyky))
 			$res_arr[] = "(".$jazyk['idJazyka'].",".$idDomeny.")";						
    }
    $db->Query("INSERT INTO ".TABLE_DOMENY_JAZYKY." (idJazyka, idDomeny) VALUE ".implode(',', $res_arr));	
} 
                    
$res = CreateDirTreeByParam($idDomeny);                    
DomainDefaultContent($idDomeny, $jazyky);

if(!$res)
	$systemMessage->add_warning(WARNING_PRAVA_PRO_ZAPIS);

if($this->get_id_action() == 0)
    $log->add_log('create','domeny',$idDomeny,$domena);
    else
    $log->add_log('edit-settings','domeny',$idDomeny,$domena);
    
$systemMessage->add_ok(OK_ULOZENO);
$url = $this->get_link('domeny',0,'edit-settings',$idDomeny);
Redirect($url);

?>