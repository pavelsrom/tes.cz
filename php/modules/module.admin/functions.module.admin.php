<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined('SECURITY_CMS')) exit;


	
	//nastavi defaultni obsah domeny - obsah stranek, blok menu, obrazky atd.
	function DomainDefaultContent($idDomeny, $jazyky){
		global $db;

		//naplneni stranek a bloku odkazu obsahem
		$lorem_ipsum = "<p>Nulla est. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos. Etiam sapien elit, consequat eget, tristique non, venenatis quis, ante. Suspendisse sagittis ultrices augue. In convallis. Proin pede metus, vulputate nec, fermentum fringilla, vehicula vitae, justo. Pellentesque ipsum. Donec vitae arcu. Donec iaculis gravida nulla. Duis risus. Sed convallis magna eu sem. Donec quis nibh at felis congue commodo. Etiam ligula pede, sagittis quis, interdum ultricies, scelerisque eu. Morbi imperdiet, mauris ac auctor dictum, nisl ligula egestas nulla, et sollicitudin sem purus in lacus. Etiam commodo dui eget wisi. Mauris metus. Etiam bibendum elit eget erat. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat. Sed ac dolor sit amet purus malesuada congue. Integer tempor.</p>";

        $stranky = array(
            "uvodni" => array(
                "typ"   => "uvodni",
                "nazev" => TUVODNI_STRANKA,
                "obsah" => $lorem_ipsum,
                "url"   => "uvodni-stranka"
                ),
                /*
            "sitemap" => array(
                "typ"   => "sitemap",
                "nazev" => TMAPA_WEBU,
                "obsah" => "",
                "url"   => "mapa-stranek"
                ),
                */
            "404" => array(
                "typ"   => "404",
                "nazev" => TCHYBOVA_STRANKA,
                "obsah" => "",
                "url"   => "stranka-nenalezena"
                ),    
                /*
            "archiv-clanku" => array(
                "typ"   => "archiv-clanku",
                "nazev" => TARCHIV_CLANKU,
                "obsah" => "",
                "url"   => "archiv-clanku"
                ),
            "stranka-galerie" => array(
                "typ"   => "stranka-galerie",
                "nazev" => TGALERIE,
                "obsah" => "",
                "url"   => "galerie"
                ),  
            "vyhledavani" => array(
                "typ"   => "vyhledavani",
                "nazev" => TVYHLEDAVANI,
                "obsah" => "",
                "url"   => "vyhledavani"
                ),  
            "novinky" => array(
                "typ"   => "novinky",
                "nazev" => TNOVINKY,
                "obsah" => "",
                "url"   => "novinky"
                ),
            "kalendar-akci" => array(
                "typ"   => "kalendar-akci",
                "nazev" => TKALENDAR_AKCI,
                "obsah" => "",
                "url"   => "kalendar-akci"
                ) 
                */          
            );
        
        $pocet_jazyku = count($jazyky);
        
	    
        foreach($jazyky AS $j)
        {
            foreach($stranky AS $s)
            {
                $idStranky = $db->get(TABLE_STRANKY,"idStranky","idDomeny=".$idDomeny." AND idJazyka=".$j." AND typ='".$s['typ']."'");
                $idStranky = intval($idStranky);
                if($idStranky > 0) continue;
                $insert = array(
                    "idDomeny"      => $idDomeny,
                    "idJazyka"      => $j,
                    "nazev"         => $s['nazev'],
                    "nadpis"        => $s['nazev'],
                    "nazevProMenu"  => $s['nazev'],
                    "typ"           => $s['typ'],
                    "url"           => $pocet_jazyku > 1 ? ($s['url']."-".$j) : $s['url'],
                    "title"         => $s['nazev'],
                    "obsah"         => $s['obsah'],
                    "obsahBezHtml"  => strip_tags($s['obsah']),
                    "zobrazit"      => 1,
                    "doMenu"        => 0  
                    );
                    
                $db->insert(TABLE_STRANKY, $insert);
            }
   
        }
		
		
    }

?>