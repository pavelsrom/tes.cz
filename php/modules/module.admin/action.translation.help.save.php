<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined('SECURITY_CMS')) exit;
if(!is_action() || 
    (!$this->is_access('preklady-napovedy','edit-settings',0,1,'settings_edit') &&
   !$this->is_access('preklady-napovedy','new',0,0,'settings_add')) 
    ) return;
    
    
$idPrekladu = $this->get_id_action();

$nadpis = get_post('nadpis');
$cz = get_post('cz');
$en = get_post('en');

$insert = array();

if($nadpis=='')
    $systemMessage->add_error(ERROR_NEPLATNY_NAZEV);
    else
    $insert["nadpis"] = $nadpis;

if($systemMessage->error_exists()) return;

$insert = $insert + array("cz" => $cz, "en" => $en,"obsahBezHtml" => strip_tags($cz));

if($idPrekladu == 0)
{    
    $db->insert(TABLE_NAPOVEDA, $insert);
    $idPrekladu = $db->lastId();
}
else
{
    $db->update(TABLE_NAPOVEDA,$insert,"idNapovedy=".$idPrekladu);
}

$log->add_log('create',"napoveda",$idPrekladu,$nadpis);

$systemMessage->add_ok(OK_ULOZENO." ".TID." ".$idPrekladu);
if($this->get_id_action() == 0)
    $url = $this->get_link('preklady-napovedy',0,'new');
    else
    $url = $this->get_link('preklady-napovedy',0,'edit-settings',$idPrekladu);
     
    
Redirect($url);

?>