<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined('SECURITY_CMS')) exit;
if(!is_action() || 
    (!$this->is_access('preklady-admin','edit-settings',0,1,'settings_edit') &&
   !$this->is_access('preklady-admin','new',0,0,'settings_add')) 
    ) return;
    
    
$idPrekladu = $this->get_id_action();

$konstanta = get_post('konstanta');
$cz = get_post('cz');
$en = get_post('en');

if(!TestStringEN($konstanta, 240, false)) 
    $systemMessage->add_error(ERROR_KONSTANTA_MUSI_BYT_VYPLNENA); 

$test = $db->query("SELECT * FROM ".TABLE_PREKLADY_ADMINISTRACE." WHERE konstanta='".$konstanta."' AND idPrekladu!=".$idPrekladu);
if($db->numRows($test) > 0)
    $systemMessage->add_error(ERROR_KONSTANTA_JIZ_EXISTUJE);

if($systemMessage->error_exists()) return;

if($idPrekladu == 0)
{
    $insert = array("konstanta" => $konstanta, "cz" => $cz, "en" => $en);
    $db->insert(TABLE_PREKLADY_ADMINISTRACE, $insert);
    $idPrekladu = $db->lastId();
}
else
{
    $update = array("cz" => $cz, "en" => $en);
    $db->update(TABLE_PREKLADY_ADMINISTRACE,$update,"idPrekladu=".$idPrekladu);
}

     
$log->add_log('create',"preklady-admin",$idPrekladu,$konstanta);

$systemMessage->add_ok(OK_ULOZENO);
$url = $this->get_link('preklady-admin',0,'new'); 
Redirect($url);

?>