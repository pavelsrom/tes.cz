<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */


if(!defined('SECURITY_CMS')) exit;
if(!$this->is_access('preklady-admin','edit-settings',0,1,'translations') &&
   !$this->is_access('preklady-admin','new',0,0,'translations') 
    ) return;
    
$idPrekladu = $this->get_id_action();

   
$main_tools = array();
        
if($idPrekladu == 0){
    $preklad = array();
	$preklad['konstanta'] = "";
	$preklad['cz'] = "";
	$preklad['en'] = "";
			
	}
	else
	{			 
	$data = $db->Query("SELECT * FROM ".TABLE_PREKLADY_ADMINISTRACE." WHERE idPrekladu=".$idPrekladu);
	$preklad = $db->getAssoc($data);
    }
		
  
$main_tools[] = array(
    'ikona' => 'back', 
    "nazev" => TZPET, 
    "aktivni" => 0, 
    "odkaz" => $this->get_link('preklady-admin'), 
    "title" => TZPET_NA_PREKLADY
    );

echo main_tools($main_tools);
        
$form = new Form();
$form->add_section(TZAKLADNI_UDAJE, "home");
if($idPrekladu==0)
    $form->add_text(TKONSTANTA,'konstanta',$preklad['konstanta'],0,true);
    else
    $form->add_plain_text(TKONSTANTA,$preklad['konstanta']);
    
$form->add_text("CZ",'cz',$preklad['cz']);
$form->add_text("EN",'en',$preklad['en']);

$disabled = !(($login_obj->UserPrivilege('settings_add') && $idPrekladu==0) || ($login_obj->UserPrivilege('settings_edit') && $idPrekladu>0));
$form->add_submit()->set_disabled($disabled);
echo $form->get_html_code();
   
?>