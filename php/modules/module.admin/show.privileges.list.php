<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined('SECURITY_CMS')) exit;
if(!$this->is_access('prava') || (!$login_obj->UserPrivilege('superadmin') && !$login_obj->UserPrivilege('admin'))) return;

$data = $db->Query("SELECT * FROM ".TABLE_CMS_PRAVA." ORDER BY priorita");
		
$nazev = array();
$prava = array();
		
while($p = $db->getAssoc($data)){
    $id = $p['idPrava'];
    $nazev[$id] = $p['nazev'];
			
    $prava[TVYTVARET_OBJEKTY][$id] = array(
			    'id' => $id, 
				'name' => 'settings_add', 
				'val' => $p['settings_add']
				);
						
    $prava[TMAZAT_OBJEKTY][$id]  = array(
				'id' => $id, 
				'name' => 'settings_delete', 
				'val' => $p['settings_delete']
				);
	$prava[TPROHLIZET_NASTAVENI][$id]  = array(
				'id' => $id, 
				'name' => 'settings_view', 
				'val' => $p['settings_view']
				);
	$prava[TEDITOVAT_NASTAVENI][$id]  = array(
				'id' => $id, 
				'name' => 'settings_edit', 
				'val' => $p['settings_edit']
				);
			
	$prava[TPRIDAVAT_OBSAH][$id]  = array(
	       		'id' => $id, 
				'name' => 'content_add', 
				'val' => $p['content_add']
				);
	$prava[TPROHLIZET_OBSAH][$id]  = array(
				'id' => $id, 
				'name' => 'content_view', 
				'val' => $p['content_view']
				);
	$prava[TEDITOVAT_OBSAH][$id]  = array(
				'id' => $id, 
				'name' => 'content_edit', 
				'val' => $p['content_edit']
				);
	$prava[TMAZAT_OBSAH][$id]  = array(
				'id' => $id, 
				'name' => 'content_delete', 
				'val' => $p['content_delete']
				);

	$prava[TPROHLIZET_STATISTIKY][$id]  = array(
				'id' => $id, 
				'name' => 'statistics_view', 
				'val' => $p['statistics_view']
				);
	$prava[TPOSILAT_EMAILY][$id]  = array(
				'id' => $id, 
				'name' => 'send_email', 
				'val' => $p['send_email']
				);
						
	$prava[TNASTAVOVAT_PRISTUPY][$id]  = array(
				'id' => $id, 
				'name' => 'set_privilege', 
				'val' => $p['set_privilege']
				);
	$prava[TEXPORT_DAT][$id]  = array(
				'id' => $id, 
				'name' => 'export', 
				'val' => $p['export']
				);
	$prava[TIMPORT_DAT][$id]  = array(
				'id' => $id, 
				'name' => 'import', 
				'val' => $p['import']
				);
    $prava[TUPRAVOVAT_SEO][$id]  = array(
				'id' => $id, 
				'name' => 'seo', 
				'val' => $p['seo']
				);
    $prava[TUPRAVOVAT_PREKLADY][$id]  = array(
				'id' => $id, 
				'name' => 'translations', 
				'val' => $p['translations']
				);
	$prava[TPRAVO_JEN_NA_MUJ_OBSAH][$id]  = array(
				'id' => $id, 
				'name' => 'only_my', 
				'val' => $p['only_my']
				);
                		 
		}
		
    echo "<div id='message'></div>";

$table = new Table("tPrava");
$table->tr_head()->add(TROLE_UZIVATELU,"",0,"",$db->numRows($data) + 1);
$tr = $table->tr_head()->add(TCINNOST);

foreach($nazev AS $n)
    $tr->add(constant($n),"w80");

$i = 0;
$dis_settings = !$login_obj->UserPrivilege('settings_edit');

		
foreach($prava AS $nazev => $p){
    $tr = $table->tr();
    $tr->add($nazev,"tleft");

	foreach($p AS $row){
	   if($row['val']==1) 
	       $class = "zapnute fa fa-check";
	       else 
           $class = "vypnute fa fa-times";
       
       if($dis_settings)
            $tr->add("<span class='".$class."'></span>");
            else
            $tr->add("<a href='#' id='".$row['name']."-".$row['id']."' class='".$class."' title='".TKLIKNUTIM_ZAPNES_VYPNES."'></a>","center");


	   }

}
			
echo $table->get_html();



?>

<script type="text/javascript">
<!--

$(function(){
    
    $("#tPrava a.zapnute, #tPrava a.vypnute").click(function(){
        
        var a = $(this);
        
        var zapnuto = a.hasClass("zapnute");
        var data = a.attr('id').split("-"); 
        var typ = data[0];
        var id = data[1];
        
        $.msg();
        var td = $(this).parents("td");
        td.addClass("saved");
           
        $.post("<?php echo AJAX_GATEWAY;?>user.privilege.set", {typ: typ, id: id, val: zapnuto?0:1}, function(message) {
            if(zapnuto) 
            {
                a.removeClass("zapnute");
                a.removeClass("fa-check");
                a.addClass("vypnute");
                a.addClass("fa-times");
                
            }
            else
            {
                a.removeClass("vypnute");
                a.removeClass("fa-times");
                a.addClass("zapnute");
                a.addClass("fa-check");
            }
            
            $.msg( 'setClass', 'ok_message' );
            $.msg( 'replace', '<?php echo OK_ULOZENO;?>' );
            $.msg( 'unblock', 1000);
            td.removeClass("saved");
			});
        
        return false;
        
        
    })
    
    
    
})

// -->
</script>

