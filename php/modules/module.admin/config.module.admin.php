<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */


//------------------------------------------------
//definice polozek menu
//------------------------------------------------

$module_menu = array();

if($login_obj->UserPrivilege('translations'))
$module_menu[] = array(
    "page"  => "preklady-admin",
    "name"  => TPREKLADY_ADMINISTRACE
    );
    
if($login_obj->UserPrivilege('translations'))
$module_menu[] = array(
    "page"  => "preklady-napovedy",
    "name"  => TPREKLADY_NAPOVEDY
    );
    
if($login_obj->UserPrivilege('superadmin'))
$module_menu[] = array(
    "page"  => "domeny",
    "name"  => TSEZNAM_DOMEN
    );
    
if($login_obj->UserPrivilege('superadmin') || $login_obj->UserPrivilege('admin'))
$module_menu[] = array(
    "page"  => "prava",
    "name"  => TKONFIGURACE_PRAV
    );
    
if($login_obj->UserPrivilege('superadmin') || $login_obj->UserPrivilege('admin'))
$module_menu[] = array(
    "page"  => "zalohy_db",
    "name"  => TZALOHY_DATABAZE
    );    
    
         
//------------------------------------------------
//definice nadpisu h1 a textu k nadpisu
//------------------------------------------------ 
$module_h1 = array();
$module_h1[] = array(
    "page"  => "domeny",
    "action"=> "edit-settings",
    "verify_id_action" => 1,
    "name"  => TEDITACE_DOMENY
    );
$module_h1[] = array(
    "page"  => "domeny",
    "action"=> "new",
    "name"  => TNOVA_DOMENA
    );    
$module_h1[] = array(
    "page"  => "domeny",
    "name"  => TSEZNAM_DOMEN,
    "text"  => 377
    ); 

$module_h1[] = array(
    "page"  => "preklady-admin",
    "name"  => TPREKLADY_ADMINISTRACE,
    "text"  => 378
    );
$module_h1[] = array(
    "page"  => "preklady-admin",
    "action"=> "new",
    "name"  => TPRIDAT_PREKLAD
    );       
$module_h1[] = array(
    "page"  => "preklady-napovedy",
    "name"  => TPREKLADY_NAPOVEDY,
    "text"  => 379
    );
$module_h1[] = array(
    "page"  => "preklady-napovedy",
    "action"=> "new",
    "name"  => TPRIDAT_PREKLAD
    ); 
$module_h1[] = array(
    "page"  => "preklady-napovedy",
    "action"=> "edit-settings",
    "verify_id_action" => 1,
    "name"  => TEDITOVAT_PREKLAD
    ); 
    
$module_h1[] = array(
    "page"  => "prava",
    "name"  => TKONFIGURACE_PRAV,
    "text"  => 295
    );
    
$module_h1[] = array(
    "page"  => "zalohy_db",
    "name"  => TZALOHY_DATABAZE,
    "text"  => 380
    ); 
    
?>