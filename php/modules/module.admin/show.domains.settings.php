<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined('SECURITY_CMS')) exit;
if((!$this->is_access('domeny','edit-settings',0,1,'settings_view') &&
   !$this->is_access('domeny','new',0,0,'settings_add'))  || (!$login_obj->UserPrivilege('superadmin') && !$login_obj->UserPrivilege('admin')) 
    ) return;

$idDomain = $this->get_id_action();
$main_tools = array();

if($idDomain > 0){
    $data = $db->Query("SELECT d.* FROM ".TABLE_DOMENY." AS d WHERE idDomeny=".$idDomain);
    $dom = $db->getAssoc($data);
            
    $dom['moduly'] = array();
    $d = $db->query("SELECT idModulu FROM ".TABLE_DOMENY_MODULY." WHERE idDomeny=".$idDomain);
    while($m = $db->getAssoc($d))
        $dom['moduly'][] = $m['idModulu'];
        
                
    $dom['jazyky'] = array();
    $d = $db->query("SELECT idJazyka FROM ".TABLE_DOMENY_JAZYKY." WHERE idDomeny=".$idDomain);
    while($j = $db->getAssoc($d))
        $dom['jazyky'][] = $j['idJazyka'];
                     
            
    }
    else
    {
        $dom = array();		
		$dom['adresar'] = "";
		$dom['www'] = 1;
		$dom['domena'] = "";
        $dom['urlKoncovka'] = 'php';
        $dom['urlStruktura'] = 'id-nazev';
        $dom['zobrazit'] = 1;
        $dom['moduly'] = array();
        $dom['jazyky'] = array();
    }
    

$main_tools[] = array(
    'ikona' => 'back', 
    "nazev" => TZPET, 
    "aktivni" => 0, 
    "odkaz" => $this->get_link('domeny'), 
    "title" => TZPET_NA_SEZNAM_DOMEN
    );
    
echo main_tools($main_tools);		

$form = new Form();
$form->add_section(TZAKLADNI_UDAJE,"home");
$form->add_text(TDOMENA,'domena',$dom['domena'],165,true);
$form->add_radiobuttons(TZOBRAZIT,'zobrazit',$dom['zobrazit'],null,168);

$form->add_section(TPOKROCILE_NASTAVENI,"page-setting");  
$form->add_text(TADRESAR_WEBU,'adresar',$dom['adresar'],169);
$form->add_radiobuttons(TPRESMEROVAT_NA_WWW,'www',$dom['www'],null,170);
$form->add_radiobuttons(TSTRUKTURA_URL_ADRES,'urlStruktura',$dom['urlStruktura'],array('id-nazev' => TID_NAZEV,'nazev-id'=>TNAZEV_ID,'nazev'=>TNAZEV,'nazev-nazev' => TURL_NAZEV_NAZEV),172);
$form->add_selectbox(TKONCOVKA_URL_ADRES,'urlKoncovka',$dom['urlKoncovka'],array('' => TBEZ_KONCOVKY,'html'=>'html','htm'=>'htm','php'=>'php'),172);

		
//nabidka dostupnych modulu
		
$data2 = $db->Query("SELECT podmodul, nazev, idModulu, zakladni FROM ".TABLE_CMS_MODULY." ORDER BY zakladni DESC, admin DESC");
		
$moduly = $podmoduly = array();
while($m = $db->getAssoc($data2)){
    $m['nazev'] = defined($m['nazev']) ? constant($m['nazev']) : "";
    if($m['podmodul']==1)
        $podmoduly[] = $m;
        else
        $moduly[] = $m;    
    }
    
$form->add_section(TPRISTUP_K_MODULUM,"pass");


if(count($moduly)==0) 
    $form->add_plain_text(TMODULY,TNENALEZEN_ZADNY_ZAZNAM,174);
    else 
    {
        $default_modules = array();
        $vsechny_moduly = array();
        
			if($idDomain>0 ){
				foreach($moduly AS $mod){
				    
					$id = $mod['idModulu'];
					if($dom['moduly']!='' && $domain->UserModul($id, $dom['moduly'])) 
                        $default_modules[] = $id;

					if($mod['zakladni']==1)
						$vsechny_moduly[$id] =  "<b>".$mod['nazev']."</b>";
						else
						$vsechny_moduly[$id] =  $mod['nazev'];

					}
				}
				else{
				foreach($moduly AS $mod){
					$id = $mod['idModulu'];
					if($mod['zakladni']==1) 
                        $default_modules[] = $id;

					if($mod['zakladni']==1)
						$vsechny_moduly[$id] = "<b>".$mod['nazev']."</b>";
						else
						$vsechny_moduly[$id] = $mod['nazev'];

					}
				}
                
                $form->add_checkboxes(TMODULY,'moduly',$default_modules,$vsechny_moduly,174)->setSecureString(false);
			}


if(count($podmoduly)==0) 
	$form->add_plain_text(TPODMODULY,TNENALEZEN_ZADNY_ZAZNAM,175);
	else 
	{
	   $default_modules = array();
        $vsechny_moduly = array();
        		 
			if($idDomain>0 ){
				foreach($podmoduly AS $mod){
				    
					$id = $mod['idModulu'];
					if($dom['moduly']!='' && $domain->UserModul($id, $dom['moduly'])) 
                        $default_modules[] = $id;

					if($mod['zakladni']==1)
						$vsechny_moduly[$id] = "<b>".$mod['nazev']."</b>";
						else
						$vsechny_moduly[$id] =  $mod['nazev'];

					}
				}
				else{
				foreach($podmoduly AS $mod){
					$id = $mod['idModulu'];
					if($mod['zakladni']==1) 
                        $default_modules[] = $id;
					
				    if($mod['zakladni']==1)
						$vsechny_moduly[$id] =  "<b>".$mod['nazev']."</b>";
						else
						$vsechny_moduly[$id] =  $mod['nazev'];

					}
				}
                
                $form->add_checkboxes(TPODMODULY,'moduly',$default_modules,$vsechny_moduly,175);
			}


        
        $d = $db->query("SELECT j.idJazyka, j.nazev, j.jazyk, IF(r.idRelace IS NULL, 0, 1) AS checked 
            FROM ".TABLE_JAZYKY." AS j 
            LEFT JOIN ".TABLE_DOMENY_JAZYKY." AS r ON r.idJazyka = j.idJazyka AND r.idDomeny=".intval($idDomain)."
            WHERE j.aktivni = 1 
            ORDER BY j.idJazyka
            ");
        
        
        $jazyky = array();
        $jazyky_checked = array();

        while($j = $db->getAssoc($d))
        {
            if($j['checked'] == 1)
                $jazyky_checked[] = $j['idJazyka'];
            
            $jazyky[$j['idJazyka']] = "<img src='".RELATIVE_PATH."img/jazyky/".$j['jazyk'].".png' alt='".$j['jazyk']."' height='13' /> ".constant($j['nazev']);
        }

$form->add_checkboxes(TJAZYKY,'jazyky',$jazyky_checked,$jazyky)->setSecureString(false);

	
if($idDomain > 0){
    $form->add_section(TUZIVATELE_S_PRAVY_PRISTUPU,"users");

    $data = $db->Query("SELECT r.idRelace, u.jmeno, u.prijmeni 
			FROM ".TABLE_UZIVATELE_DOMENY." AS r 
            LEFT JOIN ".TABLE_UZIVATELE." AS u ON r.idUzivatele=u.idUzivatele
			WHERE r.idDomeny = ".$idDomain);
            
	$dis_delete = !$login_obj->UserPrivilege('settings_edit');
                
	if($db->numRows($data)==0)
        $form->add_plain_text(TUZIVATELE,TNENALEZEN_ZADNY_ZAZNAM);
        else
        {
            $uzivatele = array();
            $uzivatele_default = array();
            $html = "";
            while($users = $db->getAssoc($data)){
                if($dis_delete)
    				$disconnect = "";
    				else
    				$disconnect = tlacitko('#',TODEBRAT,"","times","user_disconnect","ud".$users['idRelace']);
            				
     			    $html .= "<div class='row_disconnect'>".$users['jmeno']." ".$users['prijmeni']." ".$disconnect."</div><br />";
     			}

                    
            $form->add_plain_text(TUZIVATELE,$html,177);        
        }		
    }

	
$res = ($login_obj->UserPrivilege('settings_edit') && $idDomain>0) || ($login_obj->UserPrivilege('settings_add') && $idDomain==0);

$form->add_submit()->set_disabled(!$res);

echo $form->get_html_code();

?>

<script type="text/javascript">
<!--

$(function(){
    
    $("a.user_disconnect").click(function(){
        $.msg();
        var a = $(this);
        var id = a.attr('id').substring(2);
        var domain_id = <?php echo $idDomain;?>;
        var div = a.parent("div.row_disconnect");
        var td = div.parent("td");
        
        $.post("<?php echo AJAX_GATEWAY;?>domains.user.disconnect",{id: id, domain_id: domain_id},function(data){
                div.fadeOut(function(){
                    $.msg( 'setClass', 'ok_message' );
                    $.msg( 'replace', '<?php echo OK_UZIVATEL_ODPOJEN;?>' );
                    div.remove();

                    if($("div.row_disconnect").length == 0)
                        td.text("<?php echo TNENALEZEN_ZADNY_ZAZNAM;?>");
                        
                    $.msg( 'unblock', 1000);    
                    });
                
                });
        
        
        return false;
        
    })
  
    
})


// -->
</script>


