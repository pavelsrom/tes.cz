<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined('SECURITY_CMS')) exit;
if(!$this->is_access('ankety','delete',0,1,'content_delete')) return;

$id = $this->get_id_action();
$nazev = $db->get(TABLE_ANKETY,'nazev','idAnkety='.$id);
			
$db->Query("DELETE FROM ".TABLE_ANKETY." WHERE idDomeny=".$domain->getId()." AND idAnkety=".$id);
$db->Query("DELETE FROM ".TABLE_ANKETY_OTAZKY." WHERE idAnkety=".$id);
$db->Query("DELETE FROM ".TABLE_PANELY_POLOZKY." WHERE podtyp='anketa' AND idPodtypu=".$id);

$log->add_log('delete','anketa',$id,$nazev);

$systemMessage->add_ok(OK_SMAZANO);
Redirect($this->get_link('ankety'));

?>