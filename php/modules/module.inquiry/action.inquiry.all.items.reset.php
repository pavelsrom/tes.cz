<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined('SECURITY_CMS')) exit;
if(!$this->is_access('ankety','reset',0,1,'content_delete')) return;

$idAnkety = $this->get_id_action();
$nazev = $db->get(TABLE_ANKETY,'nazev','idAnkety='.$idAnkety);
            
$db->Query("UPDATE ".TABLE_ANKETY_OTAZKY." SET hlasy=0 WHERE idAnkety=".$idAnkety);


$log->add_log('edit-content','anketa',$idAnkety,$nazev);

$systemMessage->add_ok(OK_VYNULOVANO);

Redirect($this->get_link('ankety',0,'edit-content',$idAnkety));	

?>