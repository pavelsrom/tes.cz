<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */


if(!defined('SECURITY_CMS')) exit;
    
if(!is_action() || 
    (!$this->is_access('ankety','edit-content',1,1,'content_view') &&
    !$this->is_access('ankety','new',0,1,'content_add'))
    ) return;

$barva = get_post('barva');
$idAnkety = $this->get_id_action();
$akce = $this->get_action();


$jazyky = $lang->get_langs();
$odpoved = get_array_post('nazev');

foreach($jazyky AS $idj => $j)
{			
    $o = $odpoved[$idj];
    if(strlen($o)<3){
        $systemMessage->add_error(ERROR_NEPLATNY_NAZEV);
        return;
    }
}

$idOdpovedi = $this->get_id_page();
	
if($idOdpovedi == 0)
{		
    $insert = array("barva" => $barva, "idAnkety" => $idAnkety); 
    
    $insert['priorita'] = $db->get(TABLE_ANKETY_ODPOVEDI, "MAX(priorita) + 1","idAnkety = ".$idAnkety);
               
    $db->insert(TABLE_ANKETY_ODPOVEDI, $insert);
    $idOdpovedi = $db->lastId();
}
else
{
    $insert = array("barva" => $barva);            
    $db->update(TABLE_ANKETY_ODPOVEDI, $insert, "idAnkety=".$idAnkety." AND idOdpovedi=".$idOdpovedi);
}

foreach($jazyky AS $idj => $j)
{
    $o = $odpoved[$idj];
    $ido = $db->get(TABLE_ANKETY_ODPOVEDI_NAZVY, "idOdpovedi","idJazyka='".$idj."' AND idOdpovedi='".$idOdpovedi."'");
    $update_odpovedi = array(
        "idJazyka" => $idj, 
        "nazev" => $o, 
        "idOdpovedi" => $idOdpovedi
        );
    
    if($ido > 0)
        $db->update(TABLE_ANKETY_ODPOVEDI_NAZVY, $update_odpovedi, "idOdpovedi='".$ido."' AND idJazyka=".$idj);
        else
        $db->insert(TABLE_ANKETY_ODPOVEDI_NAZVY, $update_odpovedi);
        
}

            
$nazev_ankety = $db->get(TABLE_ANKETY,'nazev',"idAnkety=".$idAnkety);
$log->add_log('edit-content','anketa',$idAnkety,$nazev_ankety);

$systemMessage->add_ok(OK_ULOZENO);
if($akce == 'new')
    $url = $this->get_link('ankety',0,'new',$idAnkety);
    else
    $url = $this->get_link('ankety',$idOdpovedi,'edit-content',$idAnkety);
    
Redirect($url);
            
?>