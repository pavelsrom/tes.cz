<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined('SECURITY_CMS')) exit;
if(!is_post('btnUlozitPolozkyAnkety') || !$this->is_access('ankety','edit-content',0,1,'content_edit')) return;

$nazev = $_POST['nazev'];
$barva = $_POST['barva'];
$idPolozky = $_POST['idPolozky'];
$idAnkety = $this->get_id_action();
			
for($i = 0; $i < count($nazev); $i++){
    $n = $db->secureString($nazev[$i]);
    $b = $db->secureString($barva[$i]);
    $id = $idPolozky[$i];
    $res = $db->Query("UPDATE ".TABLE_ANKETY_OTAZKY." SET nazev='".$n."', barva='".$b."' WHERE idAnkety=".$idAnkety." AND idPolozky=".$id);
    }
			
	
$nazev_ankety = $db->get(TABLE_ANKETY,'nazev',"idAnkety=".$idAnkety);
$log->add_log('edit-content','anketa',$idAnkety,$nazev_ankety);
        	
$systemMessage->add_ok(OK_ULOZENO);
Redirect($this->get_link('ankety',0,'edit-content',$idAnkety));

?>