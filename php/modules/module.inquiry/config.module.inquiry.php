<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */


//------------------------------------------------
//definice polozek menu
//------------------------------------------------

$module_menu = array();
$module_menu[] = array(
    "page"  => "ankety",
    "name"  => TSEZNAM_ANKET
    );
    
if($login_obj->UserPrivilege('settings_view'))
$module_menu[] = array(
    "page"  => "nastaveni",
    "name"  => TNASTAVENI
    );
    
//------------------------------------------------
//definice nadpisu h1 a textu k nadpisu
//------------------------------------------------ 
$module_h1 = array();
$module_h1[] = array(
    "page"  => "ankety",
    "action"=> "edit-content",
    "verify_id_action" => 1,
    "name"  => TEDITACE_ANKETY
    );
$module_h1[] = array(
    "page"  => "ankety",
    "action"=> "edit-settings",
    "verify_id_action" => 1,
    "name"  => TNASTAVENI_ANKETY
    );
$module_h1[] = array(
    "page"  => "ankety",
    "action"=> "new",
    "name"  => TNOVA_ANKETA
    );   
$module_h1[] = array(
    "page"  => "ankety",
    "action"=> "new",
    "verify_id_action" => 1,
    "name"  => TNOVA_ODPOVED_ANKETY
    );
$module_h1[] = array(
    "page"  => "ankety",
    "action"=> "edit-content",
    "verify_id_action" => 1,
    "verify_id_page" => 1,
    "name"  => TEDITACE_ODPOVEDI_ANKETY
    );
$module_h1[] = array(
    "page"  => "ankety",
    "name"  => TSEZNAM_ANKET
    ); 
$module_h1[] = array(
    "page"  => "nastaveni",
    "name"  => TNASTAVENI
    );



?>