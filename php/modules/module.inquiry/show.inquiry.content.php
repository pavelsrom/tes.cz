<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined('SECURITY_CMS')) exit;
if(!$this->is_access('ankety','edit-content',0,1,'content_view')) return;


$idInquiry = $this->get_id_action();

$data_inquiry = $db->Query("SELECT a.nazev
			FROM ".TABLE_ANKETY." AS a
			WHERE idAnkety=".$idInquiry." AND idDomeny=".$domain->getId()." LIMIT 1");
        
$a = $db->getAssoc($data_inquiry);
$main_tools = array();
        
               
if($login_obj->UserPrivilege('settings_view'))
    $main_tools[] = array(
        'ikona' => 'setting', 
        "nazev" => TNASTAVENI, 
        "aktivni" => 0, 
        "odkaz" => $this->get_link('ankety',0,'edit-settings',$idInquiry), 
        "title" => TNASTAVENI_ANKETY
        );
 
if($login_obj->UserPrivilege('content_add'))
    $main_tools[] = array(
        'ikona' => 'new', 
        "nazev" => TPRIDAT_ODPOVED, 
        "aktivni" => 0, 
        "odkaz" => $this->get_link('ankety',0,'new',$idInquiry));
            
$main_tools[] = array(
    'ikona' => 'back', 
    "nazev" => TZPET, 
    "aktivni" => 0, 
    "odkaz" => $this->get_link('ankety'), 
    "title" => TZPET_NA_SEZNAM_ANKET
    );
    

echo main_tools($main_tools);
      

//vypis polozek ankety
if(intval($idInquiry==0)) return;
		
$data = $db->Query("SELECT a.*, ot.otazka 
    FROM ".TABLE_ANKETY." AS a
    LEFT JOIN ".TABLE_ANKETY_OTAZKY." AS ot ON a.idAnkety = ot.idAnkety  
    WHERE a.idAnkety=".$idInquiry." 
        AND a.idDomeny=".$domain->getId()."
    ORDER BY ot.idJazyka
    LIMIT 1
    ");
		
if($db->numRows($data)==0) return;
		
$data_inquiry = $db->getAssoc($data);
		
$nazev = $data_inquiry['nazev'];
$otazka = $data_inquiry['otazka'];
$idInquiry = $data_inquiry['idAnkety'];
		
$data = $db->Query("SELECT odp.* 
            FROM ".TABLE_ANKETY_ODPOVEDI." AS odp
            WHERE idAnkety=".$idInquiry." 
            ORDER BY priorita, idOdpovedi
            ");
            

		
	
//showH3(TNAHLED_A_EDITACE_POLOZEK.' '.help(72),'content');
        
		

echo "<h2>".TANKETA.": ".$nazev."</h2><h3>".$otazka."</h3>";

$table = new Table('tList');
$table->tr_head()
    ->add(TID,'w25')
    ->add(TODPOVED)
    ->add("")
    ->add(TPOCET_HLASOVANI,'w25')
    ->add(TAKCE,'w150');
    
echo $table->get_html();


?>
<script type="text/javascript">
<!--

$(function(){
var oTable = $("#tList").dataTable({
    "sAjaxSource": "<?php echo AJAX_GATEWAY;?>answers.list",
    "bSort": false,
    "aoColumns": [
        {"sClass": "sl1"},{"sClass": "sl2"},{"sClass": "sl3"},{"sClass": "sl4"},{"sClass": "akce"}
        ],
    "fnServerData": function ( sSource, aoData, fnCallback ) {
            aoData.push({ "name": "id", "value": <?php echo $idInquiry;?> });
            $.getJSON( sSource, aoData, function (json) { fnCallback(json); } );
            }
 
})


//prida k bunce th span
$(".dataTable th").each(function(){
    $(this).html("<span>" + $(this).html() + "</span>");
})


$(document).on("click", "a.delete",function(){
        var id = $(this).attr('id').substring(6);
        var tr = $(this).parents("tr").first();
        var c = $(this).parents("tr").find("td").eq(1).text(); 
        
        var text = "<?php echo TOPRAVDU_SMAZAT;?>";

        custom_confirm(text.replace("%s",c) + '?', function(){
            $.msg();
            tr.addClass("deleted");
            
            $.post("<?php echo AJAX_GATEWAY;?>answers.delete",{id: id},function(data){
                tr.fadeOut(function(){
                    $.msg( 'setClass', 'ok_message' );
                    $.msg( 'replace', '<?php echo OK_SMAZANO;?>' );
                    oTable.fnDraw();
                    $.msg( 'unblock', 1000);
                    });
                });
            
            })
        
        return false;
        
        
    });



$(document).on("mouseover","a.up",function(){
    var tr = $(this).parents("tr").first();

    tr.addClass("exchange_from");
    tr.removeClass("light");
    tr_prev = tr.prevAll().first();
    tr_prev.addClass("exchange_to");
    tr_prev.find("td").eq(1).addClass("exchange_down");
    tr.find("td").eq(1).addClass("exchange_up");
})

$(document).on("mouseout","a.up, a.down", function(){
    $("table#tList tr").removeClass("exchange_to");
    $("table#tList tr").removeClass("exchange_from");
    $("table#tList tr td").removeClass("exchange_down");
    $("table#tList tr td").removeClass("exchange_up");
})


$(document).on("mouseover","a.down",function(){
    var tr = $(this).parents("tr").first();
    tr.addClass("exchange_to");
    tr.removeClass("light");
    tr_next = tr.nextAll().first();
    tr_next.addClass("exchange_from");
    tr_next.find("td").eq(1).addClass("exchange_up");
    tr.find("td").eq(1).addClass("exchange_down");
})

$(document).on("click","a.up", function(){
    var id = $(this).attr('id').substring(2);
    var id_ankety = <?php echo $idInquiry; ?>; 
    $.post("<?php echo AJAX_GATEWAY;?>answers.move.up",{id: id, idAnkety: id_ankety},function(data){
        oTable.fnDraw();
    })   
    
    return false;
})
   
$(document).on("click", "a.down", function(){
    var id = $(this).attr('id').substring(4);
    var id_ankety = <?php echo $idInquiry; ?>;
    $.post("<?php echo AJAX_GATEWAY;?>answers.move.down",{id: id, idAnkety: id_ankety},function(data){
        oTable.fnDraw();
    })
    
    return false;   
})


})

// -->
</script>