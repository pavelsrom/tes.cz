<?php

/**
 * @author Pavel Srom
 * @copyright 2013
 */

if(!defined('SECURITY_CMS')) exit;

if(!$this->is_access('ankety','edit-content',1,1,'content_view') &&
    !$this->is_access('ankety','new',0,1,'content_add')
    ) return;

$idInquiry = $this->get_id_action();
$idAnswer = $this->get_id_page();

$main_tools = array();
                           
$main_tools[] = array(
    'ikona' => 'back', 
    "nazev" => TZPET, 
    "aktivni" => 0, 
    "odkaz" => $this->get_link('ankety',0,"edit-content",$idInquiry), 
    "title" => TZPET_NA_SEZNAM_ODPOVEDI
    );
    
echo main_tools($main_tools);

//formular pro novou polozku
$form = new Form();
$form->add_section(TPRIDAT_NOVOU_POLOZKU,'add');

$jazyky = $lang->get_langs();
$i = 0;

if($idAnswer == 0)
{
    foreach($jazyky AS $idj => $j)
    {
        $help = $i == 0 ? 70 : 0; 
        $form->add_text(TODPOVED.' '.strtoupper($j['jazyk']),'nazev['.$idj.']','',$help,true);
        $i++;
    }
    
    $form->add_text(TBARVA,'barva','',71,true,'color');
}
else
{
    $data = $db->query("SELECT * 
        FROM ".TABLE_ANKETY_ODPOVEDI_NAZVY."
        WHERE idOdpovedi=".$idAnswer."
        ORDER BY idJazyka
        ");
 
    $nazvy = array();
    while($j = $db->getAssoc($data))
    {
        $nazvy[$j['idJazyka']] = $j['nazev'];
    }
 
    foreach($jazyky AS $idj => $j)
    {
        $help = $i == 0 ? 70 : 0; 
        $form->add_text(TODPOVED.' '.strtoupper($j['jazyk']),'nazev['.$idj.']',$nazvy[$idj],$help,true);
        $i++;
    }
 
    $barva = $db->get(TABLE_ANKETY_ODPOVEDI,"barva","idAnkety=".$idInquiry." AND idOdpovedi=".$idAnswer);
    $form->add_text(TBARVA,'barva',$barva,71,true,'color');   
}


  
if($login_obj->UserPrivilege('content_add'))
    $form->add_submit();
    else
    $form->add_submit()->set_disabled();

echo $form->get_html_code();



?>