<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined('SECURITY_CMS')) exit;
if(!$this->is_access('ankety','edit-settings',0,1,'content_view') &&
    !$this->is_access('ankety','new',0,0,'content_add')
    ) return;
    
$idInquiry = $this->get_id_action();
$main_tools = array();

if($idInquiry > 0) {
			$record1 = $db->Query("SELECT a.*, COUNT(p.idOtazky) AS pocet_polozek, 
				IF(a.od IS NULL OR a.od = '0000-00-00', '', DATE_FORMAT(a.od, '%d.%m.%Y')) AS od,
				IF(a.do IS NULL OR a.do = '0000-00-00', '', DATE_FORMAT(a.do, '%d.%m.%Y')) AS do,
				IF(((a.od <= CURDATE() OR a.od IS NULL OR a.od = '0000-00-00') 
					AND (a.do>= CURDATE() OR a.do IS NULL OR a.do = '0000-00-00'))  
					AND a.zobrazit=1, 'aktivni', 'neaktivni') AS stav 
				
			FROM ".TABLE_ANKETY." AS a 
			LEFT JOIN ".TABLE_ANKETY_OTAZKY." AS p ON a.idAnkety=p.idAnkety
			WHERE a.idDomeny=".$domain->getId()."
				AND a.idAnkety=".$idInquiry."
			GROUP BY a.idAnkety
			ORDER BY a.idAnkety");
            
			$inquiry = $db->getAssoc($record1);

            if($login_obj->UserPrivilege('content_view'))
                $main_tools[] = array(
                    'ikona' => 'content', 
                    "nazev" => TOBSAH, 
                    "aktivni" => 0, 
                    "odkaz" => $this->get_link('ankety',0,'edit-content',$idInquiry), 
                    "title" => TOBSAH_ANKETY
                    );
                
            
            $d = $db->query("SELECT idJazyka AS id, otazka FROM ".TABLE_ANKETY_OTAZKY." WHERE idAnkety=".$idInquiry);
            $jazyky_otazky = array();
            while($jo = $db->getAssoc($d))
                $jazyky_otazky[$jo['id']] = $jo['otazka'];
            
            $jazyky = $lang->get_langs();
            $inquiry['otazka'] = array();
            foreach($jazyky AS $idj => $j)
            {
                $inquiry['otazka'][$idj] = isset($jazyky_otazky[$idj]) ? $jazyky_otazky[$idj] : "";
            }
            
            
            
			}
			else
			{
			$inquiry = array();
			$inquiry['nazev'] = "";
			$inquiry['otazka'] = array();	
			$inquiry['od'] = "";
			$inquiry['do'] = "";
			$inquiry['zobrazit'] = 1;
            
            $jazyky = $lang->get_langs();         
            foreach($jazyky AS $idj => $j)
                {
                    $inquiry['otazka'][$idj] = "";
                }
            	
            }

$main_tools[] = array(
    'ikona' => 'back', 
    "nazev" => TZPET, 
    "aktivni" => 0, 
    "odkaz" => $this->get_link('ankety'), 
    "title" => TZPET_NA_SEZNAM_ANKET
    );
            
echo main_tools($main_tools);
$form = new Form();
$form->add_section(TZAKLADNI_UDAJE,'home');
$form->add_text(TNAZEV,'nazev',$inquiry['nazev'],63,true);

$jazyky = $lang->get_langs();
$i = 0;
foreach($jazyky AS $idj => $j)
{
    $help = $i == 0 ? 64 : 0; 
    $form->add_text(TOTAZKA.' '.strtoupper($j['jazyk']),'otazka['.$idj.']',$inquiry['otazka'][$idj],$help,true);
    $i++;
}

$form->add_radiobuttons(TZOBRAZOVAT,'zobrazit',$inquiry['zobrazit'],null,68);		

$form->add_section(TNACASOVANI_ZOBRAZENI,"date"); 
$form->add_text(TOD,'od',$inquiry['od'],65,false,"kalendar w55","od");
$form->add_text(TDO,'do',$inquiry['do'],66,false,"kalendar w55","do");

$res = $login_obj->UserPrivilege('content_edit');
$form->add_submit()->set_disabled(!$res);

echo $form->get_html_code();

?>