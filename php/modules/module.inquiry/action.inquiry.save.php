<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined('SECURITY_CMS')) exit;
if(!is_action() ||
    (!$this->is_access('ankety','edit-settings',0,1,'content_edit') &&
     !$this->is_access('ankety','new',0,0,'content_add'))
    ) return;
    
$nazev = get_post('nazev');

$od = get_post('od');
$do = get_post('do');

$zobrazit = get_int_post('zobrazit');
$idAnkety = $this->get_id_action();

if(!TestDate($od, true)){
    $systemMessage->add_error(ERROR_CHYBNE_DATUM);
    $od = "null";
	}
				
if(!TestDate($do, true)){
    $systemMessage->add_error(ERROR_CHYBNE_DATUM);
    $do = "null";
    }

if(strlen($nazev)=='')
    $systemMessage->add_error(ERROR_NEPLATNY_NAZEV);
	
$jazyky = $lang->get_langs();
$er = false;
$otazka = get_array_post("otazka");

foreach($jazyky AS $idj => $j)
{
    $o = $otazka[$idj];
    if(strlen($o)==0 && !$er){
        $systemMessage->add_error(ERROR_OTAZKA);
        $er = true;
    }
        
}
    			

if($systemMessage->error_exists()) return;



$update = array(
    "idDomeny"  => $domain->getId(),
    "nazev"     => $nazev,
    "od"        => GetUniDate($od),
    "do"        => GetUniDate($do),
    "zobrazit"  => $zobrazit
    );
			
if($idAnkety == 0){
    $db->insert(TABLE_ANKETY,$update);
    $idAnkety = $db->lastId();
    }
    else
	$db->update(TABLE_ANKETY,$update,"idAnkety=".$idAnkety." AND idDomeny=".$domain->getId());	            


foreach($jazyky AS $idj => $j)
{
    $o = $otazka[$idj];
    $ido = $db->get(TABLE_ANKETY_OTAZKY, "idOtazky","idJazyka='".$idj."' AND idAnkety='".$idAnkety."'");
    $update_otazky = array("idJazyka" => $idj, "otazka" => $o, "idAnkety" => $idAnkety);
    
    if($ido > 0)
        $db->update(TABLE_ANKETY_OTAZKY, $update_otazky, "idOtazky='".$ido."'");
        else
        $db->insert(TABLE_ANKETY_OTAZKY, $update_otazky);
        
}


$systemMessage->add_ok(OK_ULOZENO);
    
$log->add_log($this->get_url('id_action') == 0 ? 'create':'edit-content','anketa',$idAnkety,$nazev);

$url = $this->get_link('ankety',0,'edit-settings',$idAnkety);
Redirect($url);
            
            
?>