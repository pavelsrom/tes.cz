<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */


if(!defined('SECURITY_CMS')) exit;
if(!$this->is_access('helpdesk','edit-content',0,1,'content_view') &&
    !$this->is_access('helpdesk','new',0,0,'content_add')
    ) return;
    
$idPage = $this->get_id_action();
$main_tools = array();

if($idPage > 0) {

			$record1 = $db->Query("SELECT s.*, 
				IF(s.dotaz_datum IS NULL, '', DATE_FORMAT(s.dotaz_datum,'%d.%m.%Y')) AS dotaz_datum,
                IF(s.odpoved_datum IS NULL, '', DATE_FORMAT(s.odpoved_datum,'%d.%m.%Y')) AS odpoved_datum
			FROM ".TABLE_PODPORA." AS s
			WHERE id=".$idPage." 
				AND id_domeny=".$domain->getId()."
                AND id_jazyka=".WEB_LANG_ID."
			LIMIT 1");
			
			
			$page = $db->getAssoc($record1);

			}
			else
			{
			$page['jmeno'] = $login_obj->getName();
            $page['dotaz_datum'] = date("d.m.Y");
            $page['odpoved_datum'] = '';
            $page['odpoved_jmeno'] = $login_obj->getName();
            $page['email'] = $login_obj->getEmail();
            $page['dotaz'] = "";
            $page['odpoved'] = "";
			$page['aktivni'] = 1;
			$page['poznamka'] = "";
			
			}

$main_tools[] = array(
    'ikona' => 'back', 
    "nazev" => TZPET, 
    "aktivni" => 0, 
    "odkaz" => $this->get_link('helpdesk'), 
    "title" => TZPET_NA_SEZNAM_PRISPEVKU
    ); 

echo main_tools($main_tools);
$form = new Form();
$form->add_section(TZAKLADNI_UDAJE, 'home');
$form->add_textarea(TPOZNAMKA,'poznamka',$page['poznamka'],0,false,"h100");
$form->add_radiobuttons(TZOBRAZOVAT,'aktivni',$page['aktivni']);

$form->add_section(TDOTAZ, 'content');
$form->add_text(TJMENO_A_PRIJMENI,'jmeno',$page['jmeno'],0,true,"","nazev");
$form->add_text(TEMAIL,'email',$page['email'],0,false);
$form->add_textarea(TDOTAZ,'dotaz',$page['dotaz'],0,true,"h100");

$form->add_section(TODPOVED, "content");
$form->add_text(TJMENO_A_PRIJMENI,'odpoved_jmeno',$page['odpoved_jmeno']);
$form->add_textarea(TODPOVED,'odpoved',$page['odpoved'],0,false,"h100");
$form->add_plain_text(TODESLANO_TAZATELI,$page['odpoved_datum']=="" ? TNE : TANO." (".$page['odpoved_datum'].")");
//$form->add_next_actions();

$res = ($login_obj->UserPrivilege('content_edit') && $idPage>0) || ($login_obj->UserPrivilege('content_edit') && $idPage==0);

if($page['odpoved_datum'] == '')
    $form->add_submit(TULOZIT_A_ODESLAT_ODPOVED,'btnActionOdeslat');
    
$form->add_submit()->set_disabled(!$res);
    
echo $form->get_html_code();
        
?>
