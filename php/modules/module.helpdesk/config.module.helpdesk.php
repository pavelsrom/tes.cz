<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */


//------------------------------------------------
//definice polozek menu
//------------------------------------------------

$module_menu = array();
$module_menu[] = array(
    "page"  => "helpdesk",
    "name"  => TSEZNAM_PRISPEVKU." ".WEB_LANG
    );
$module_menu[] = array(
    "page"  => "nastaveni",
    "name"  => TNASTAVENI
    );
    
//------------------------------------------------
//definice nadpisu h1 a textu k nadpisu
//------------------------------------------------ 
$module_h1 = array();
$module_h1[] = array(
    "page"  => "helpdesk",
    "action"=> "edit-content",
    "verify_id_action" => 1,
    "name"  => TODPOVED." ".WEB_LANG
    );

$module_h1[] = array(
    "page"  => "helpdesk",
    "action"=> "new",
    "name"  => TNOVY_PRISPEVEK." ".WEB_LANG
    );    
$module_h1[] = array(
    "page"  => "helpdesk",
    "name"  => TSEZNAM_PRISPEVKU." ".WEB_LANG,
    "text"  => 357
    ); 
$module_h1[] = array(
    "page"  => "nastaveni",
    "name"  => TNASTAVENI
    );



?>