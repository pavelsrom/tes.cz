<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */


if(!defined('SECURITY_CMS')) exit;
if((!is_action() && !is_action('btnActionOdeslat')) ||
    (!$this->is_access('helpdesk','edit-content',0,1,'content_edit') &&
     !$this->is_access('helpdesk','new',0,0,'content_add'))
    ) return;
    
$id = $this->get_id_action();

$poznamka = get_post('poznamka');
$aktivni = get_int_post('aktivni');
           
$dotaz = get_post('dotaz');
$odpoved = get_post('odpoved');

$autentizace = get_int_post('autentizace');
            
$jmeno = get_post('jmeno');
$odpoved_jmeno = get_post('odpoved_jmeno');
$email = get_post('email');

$update = array();

if(!TestLength($jmeno,200,false))
    $systemMessage->add_error(TERROR_JMENO_A_PRIJMENI_DOTAZ);

if(!TestEmail($email))
    $systemMessage->add_error(ERROR_EMAIL);
			
if(trim($dotaz) == '')
    $systemMessage->add_error(TERROR_DOTAZ);

if(is_action('btnActionOdeslat') && $odpoved_jmeno == '')
    $systemMessage->add_error(TERROR_ODPOVED_JMENO);

if(is_action('btnActionOdeslat') && $odpoved == '')
    $systemMessage->add_error(TERROR_ODPOVED);

			
if($systemMessage->error_exists()) return;

$update = $update + array(
    "id_domeny"      => $domain->getId(),
    "jmeno"          => $jmeno,
    "email"          => $email,
    "dotaz"         => $dotaz,
    "odpoved"       => $odpoved,
    "odpoved_jmeno" => $odpoved_jmeno,
    "poznamka"      => $poznamka,
    "aktivni"       => $aktivni
    );			
		

if(is_action('btnActionOdeslat'))
{
    $update['odpoved_datum'] = "now";
    
}

        
if($id == 0){
    $update['dotaz_datum'] = "now";
    $update['ip'] = $_SERVER['REMOTE_ADDR'];
    $update['id_jazyka'] = WEB_LANG_ID;
    
    $db->insert(TABLE_PODPORA, $update);
    $id = $db->lastId();
    
    }
	else
    {
    
	$db->update(TABLE_PODPORA,$update,"id=".$id." AND id_domeny=".$domain->getId());
	}			

if(is_action('btnActionOdeslat'))
{
    //odeslani odpovedi 
    $id_jazyk = $db->get(TABLE_PODPORA, "id_jazyka","id=".$id);
    
    $text = serialize(TEXT_EMAILU_PRO_ODESLANI_ODPOVEDI);
    $text = $text[$id_jazyk];
    
    $text = str_replace(
        array("{odpoved}","{odpoved_jmeno}","{dotaz}","{dotaz_jmeno}"),
        array($odpoved,$odpoved_jmeno,$dotaz,$jmeno),
        $text
        );
        
    $predmet = serialize(PREDMET_EMAILU_PRO_ODESLANI_ODPOVEDI);
    $predmet = $predmet[$id_jazyk];
    
    $res = SendMail(KONTAKTNI_FORMULAR_EMAIL, NAZEV_WEBU, $email, $predmet, $text);
    
    if($res)
        $systemMessage->add_ok(OK_ODESLANO);
        else
        $systemMessage->add_ok(ERROR_ODESLANO);
        
    
}



$log->add_log($this->get_url('id_action') == 0 ? 'create':'edit-content','podpora',$id,$id);
 
$systemMessage->add_ok(OK_ULOZENO);

/*
set_next_action_code();
$dalsi_akce = get_next_action_code();
if($dalsi_akce == 0) //zpet na vypis
    $url = $this->get_link('helpdesk');
elseif($dalsi_akce == 1) //zpet na editaci
    $url = $this->get_link('helpdesk',0,'edit-content',$idStranky);
elseif($dalsi_akce == 2) //vytvorit dalsi
    $url = $this->get_link('helpdesk',0,'new');
*/

$url = $this->get_link('helpdesk');

Redirect($url)

?>