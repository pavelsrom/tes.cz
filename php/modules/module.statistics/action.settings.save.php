<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */
 
 
if(!defined('SECURITY_CMS')) exit;
if(!is_action() || !$this->is_access('nastaveni','',0,0,'settings_view')) return;	

save_settings($this->get_id());
    
$systemMessage->add_ok(OK_ULOZENO);
$log->add_log('edit-settings','statistiky-nastaveni');
Redirect($this->get_link('nastaveni'));


?>