<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined('SECURITY_CMS')) exit;
if(!$this->is_access('vyhledavani','',0,0,'statistics_view')) return;

$main_tools = array();
if($login_obj->UserPrivilege('settings_delete'))
    $main_tools[] = array(
        'ikona' => 'delete', 
        "nazev" => TVYNULOVAT, 
        "aktivni" => 0, 
        "odkaz" => "#",
        "id"    => "reset"
        );
		
$form = new FormFilter();
$form->add_text(THLEDAT, 'vyraz', "", 0, false, "", "vyraz");
echo $form->get_html_code(main_tools($main_tools));

$table = new Table('tList');
$table->tr_head()
    ->add(TVYRAZ)
    ->add(TCETNOST);
    
echo $table->get_html();
      
        
?>
<script type="text/javascript">
<!--

$(function(){
var oTable = $("#tList").dataTable({
    "sAjaxSource": "<?php echo AJAX_GATEWAY;?>find.words.list",
    "aoColumns": [{"sClass": "sl1"},{"sClass": "sl2"}],
    "fnServerData": function ( sSource, aoData, fnCallback ) {
            aoData.push({ "name": "vyraz", "value": $("#vyraz").val() });
            $.getJSON( sSource, aoData, function (json) { fnCallback(json); } );
            }
 
})

$("input#vyraz").bind('keyup change', function() {
        oTable.fnDraw(); 
        });

//prida k bunce th span
$(".dataTable th").each(function(){
    $(this).html("<span>" + $(this).html() + "</span>");
})

$("#reset").click(function(){
        var text = "<?php echo TOPRAVDU_CHCETE_VYNULOVAT_STATISTIKU;?>?";

        confirm(text, function(){
            $.msg();
            $.post("<?php echo AJAX_GATEWAY;?>find.words.reset",{},function(data){
                oTable.fnDraw();
                $.msg( 'setClass', 'ok_message' );
                $.msg( 'replace', data );
                $.msg( 'unblock', 1000);
                });
            
            })
        
        return false;
        
        
    });

})

// -->
</script>
      
            

