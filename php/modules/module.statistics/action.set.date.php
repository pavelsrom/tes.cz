<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined('SECURITY_CMS')) exit;
if(!is_action() ||
    (!$this->is_access('navstevnost-webu','',0,0,'statistics_view') &&
    !$this->is_access('navstevnost-stranek','',0,0,'statistics_view') &&
    !$this->is_access('stranky','',0,0,'statistics_view') &&
    !$this->is_access('zdroje','',0,0,'statistics_view'))
    ) return;


$start = get_post('od');
$end = get_post('do');
            
            
			if($start=='' || strlen($start)!=10) 
				if($end!=''){
					$s = explode('.', $end);
					$start = "01.".$s[1].".".$s[2];
					}
					else
					$start=strftime("01.%m.%Y", time());
				
			if($end=='' || strlen($end)!=10) 
				$end=strftime("%d.%m.%Y", time());
			
			$date_od = explode('.', $start);
			$date_do = explode('.', $end);
			$od = $date_od[2].$date_od[1].$date_od[0];
			$do = $date_do[2].$date_do[1].$date_do[0];			
			if($od > $do){
				$_POST['od'] = $end;
				$_POST['do'] = $start;
				}
				else{
				$_POST['od'] = $start;
				$_POST['do'] = $end;	
				}

			return;

?>