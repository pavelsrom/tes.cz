<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */
 
 
if(!defined('SECURITY_CMS')) exit;
if(!$this->is_access('nastaveni','',0,0,'settings_view')) return;

show_settings();
        
?>

<script type="text/javascript">

$(function(){
<!--
    
    $("a#overit_dostupnost").click(function(){
        $.msg();
        var email = $("#gaEmail").val();
        var pass = $("#gaHeslo").val();
        var profil = $("#gaProfil").val();
        
        $.get('<?php echo AJAX_GATEWAY;?>ga.connect.verify', {email: email, pass: pass, profil: profil}, function(data) {
            if(data.error != "")
            {
                $.msg( 'setClass', 'error_message' );
                $.msg( 'replace', data.error );
                $.msg( 'unblock', 3000);
            }
            else
            {
                $.msg( 'setClass', 'ok_message' );
                $.msg( 'replace', data.ok );
                $.msg( 'unblock', 2000);
            }
            
            
		},"json")
        
        return false;
        
    })
    
    
})

// -->
</script>
