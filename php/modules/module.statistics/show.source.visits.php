<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined('SECURITY_CMS')) exit;
if(!$this->is_access('zdroje')) return;

$od = get_secure_post('od');
$do = get_secure_post('do');

$form = new Form();
$form->set_action('#statistiky');
$form->add_section(TNASTAVENI,'date');
$form->add_text(TOD,'od',$od,126,false,'kalendar w55','od');
$form->add_text(TDO,'do',$do,127,false,'kalendar w55','do');		
		
$res = $login_obj->UserPrivilege('statistics_view');
$form->add_submit(TZPRACUJ)->set_disabled(!$res);

echo $form->get_html_code();

echo "<span id='statistiky'></span>";
        
if(!is_action() || !$login_obj->UserPrivilege('statistics_view')) return;


                
echo showH3(TZDROJE_NAVSTEVNOSTI,'stat', help(290,true));
echo '<div id="chart_pie"></div>';
echo showH3(TODKAZUJICI_STRANKY,'stat', help(291,true));
echo '<div id="table_referral"></div>';
echo showH3(TKLICOVA_SLOVA,'stat', help(292,true));
echo '<div id="table_keywords"></div>';
            
?>
            
<script type="text/javascript">
<!--

$(function(){
    $(document).on('click', "#table_referral th a" ,function(){
        $.msg();                
        if($(".miniloader img").attr('id')) return false;
                        
        var cl = $(this).attr('class');
        var sl_data = cl.split('_');
        var sloupec = sl_data[0];
        var smer = sl_data[1];
        $(this).html($(this).html() + "<div class='miniloader'><img src='<?php echo RELATIVE_PATH;?>img/loader4.gif' id='"+cl+"_loader' /></div>");
        
        $("#table_referral").load("<?php echo AJAX_GATEWAY;?>table.referral&od=<?php echo $od;?>&do=<?php echo $do; ?>&razeni=" + sloupec + "&smer=" + smer, function(){
            $.msg( 'unblock', 100);
        }); 
        
        return false; 
        })
                    
                    
    $(document).on('click',"#table_keywords th a" ,function(){
        $.msg();              
        if($(".miniloader img").attr('id')) return false;
                        
        var cl = $(this).attr('class');
        var sl_data = cl.split('_');
        var sloupec = sl_data[0];
        var smer = sl_data[1];
        $(this).html($(this).html() + "<div class='miniloader'><img src='<?php echo RELATIVE_PATH;?>img/loader4.gif' id='"+cl+"_loader' /></div>");
        $("#table_keywords").load("<?php echo AJAX_GATEWAY;?>table.keywords&od=<?php echo $od;?>&do=<?php echo $do; ?>&razeni=" + sloupec + "&smer=" + smer, function(){
            $.msg( 'unblock', 100);
        }); 
                        
        return false; 
        })


    $.getJSON("<?php echo AJAX_GATEWAY."graph.source.visits&od=".$od."&do=".$do; ?>",null,function(opts){
                    $('#chart_pie').highcharts(opts);
                });
  


    $("#table_referral, #table_keywords, #chart_pie").html("<div class='statistic-loader loader'><?php echo TCEKEJTE_PROSIM;?>...</div>");
    
    $("#table_referral").load("<?php echo AJAX_GATEWAY;?>table.referral&od=<?php echo $od;?>&do=<?php echo $do; ?>");
                    
    $("#table_keywords").load("<?php echo AJAX_GATEWAY;?>table.keywords&od=<?php echo $od;?>&do=<?php echo $do; ?>");       
    
})            

// -->
</script>
