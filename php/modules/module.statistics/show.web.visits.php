<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined('SECURITY_CMS')) exit;
if(!$this->is_access('navstevnost-webu')) return;

		
		
$od = get_secure_post('od');
$do = get_secure_post('do');
$typ = get_int_post('typ',1);
$showstat_type = array('0' => THODINY, '1' => TDNY, '2' => TMESICE, '3' => TROKY, '4' => TJAZYKY);

$form = new Form();
$form->set_action('#statistiky');
$form->add_section(TNASTAVENI,'date');
$form->add_text(TOD,'od',$od,126,false,'kalendar w55','od');
$form->add_text(TDO,'do',$do,127,false,'kalendar w55','do');
$form->add_selectbox(TTYP,'typ',$typ,$showstat_type,128,false,'','typ');

$res = $login_obj->UserPrivilege('statistics_view');
$form->add_submit(TZPRACUJ)->set_disabled(!$res);
echo $form->get_html_code();

echo "<span id='statistiky'></span>";
     
if(!is_action() || !$login_obj->UserPrivilege('statistics_view')) return;
                
echo showH3(TNAVSTEVNOST_WEBU,'stat');
echo '<div id="chart_visits"></div>';
echo showH3(TNOVI_VS_VRACEJICI_SE_NAVSTEVNICI,'stat',help(288,true));
echo '<div id="chart_newvisits"></div>';
            
            
?>
            
<script type="text/javascript">
<!--

$("#chart_visits, #chart_newvisits").html("<div class='statistic-loader loader'><?php echo TCEKEJTE_PROSIM;?>...</div>");
                  
$.getJSON("<?php echo AJAX_GATEWAY."graph.visits&od=".$od."&do=".$do."&typ=".$typ; ?>",null,function(opts){
    $('#chart_visits').highcharts(opts);
    }); 
                    
$.getJSON("<?php echo AJAX_GATEWAY."graph.new.visits&od=".$od."&do=".$do."&typ=".$typ; ?>",null,function(opts){
    $('#chart_newvisits').highcharts(opts);
    });
      
                      
// -->            
</script>
