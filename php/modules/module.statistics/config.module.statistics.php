<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */


//------------------------------------------------
//definice polozek menu
//------------------------------------------------

$module_menu = array();
$module_menu[] = array(
    "page"  => "navstevnost-webu",
    "name"  => TNAVSTEVNOST_WEBU
    );
$module_menu[] = array(
    "page"  => "navstevnost-stranek",
    "name"  => TNAVSTEVNOST_STRANEK." ".WEB_LANG
    );
$module_menu[] = array(
    "page"  => "zdroje",
    "name"  => TZDROJE_NAVSTEVNOSTI
    );
$module_menu[] = array(
    "page"  => "stranky",
    "name"  => TNEJLEPSI_STRANKY
    );
    
if($login_obj->UserPrivilege('statistics_view'))
$module_menu[] = array(
    "page"  => "vyhledavani",
    "name"  => TVYHLEDAVANOST_VYRAZU." ".WEB_LANG
    );
    
if($login_obj->UserPrivilege('settings_view'))
$module_menu[] = array(
    "page"  => "nastaveni",
    "name"  => TNASTAVENI
    );
    
//------------------------------------------------
//definice nadpisu h1 a textu k nadpisu
//------------------------------------------------ 
$module_h1 = array();
$module_h1[] = array(
    "page"  => "navstevnost-webu",
    "name"  => TNAVSTEVNOST_WEBU,
    "text"  => 372
    );
$module_h1[] = array(
    "page"  => "navstevnost-stranek",
    "name"  => TNAVSTEVNOST_STRANEK." ".WEB_LANG,
    "text"  => 373
    );
$module_h1[] = array(
    "page"  => "zdroje",
    "name"  => TZDROJE_NAVSTEVNOSTI,
    "text"  => 374
    );
$module_h1[] = array(
    "page"  => "stranky",
    "name"  => TNEJLEPSI_STRANKY,
    "text"  => 375
    );
$module_h1[] = array(
    "page"  => "vyhledavani",
    "name"  => TSTATISTIKY_VYHLEDAVANOSTI_SLOV." ".WEB_LANG,
    "text"  => 130
    );
$module_h1[] = array(
    "page"  => "nastaveni",
    "name"  => TNASTAVENI
    );



?>