<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */


//------------------------------------------------
//definice polozek menu
//------------------------------------------------

$module_menu = array();
$module_menu[] = array(
    "page"  => "rubriky",
    "name"  => TSTRANKY_S_CLANKY." ".WEB_LANG
    );
$module_menu[] = array(
    "page"  => "tagy",
    "name"  => TTAGY." ".WEB_LANG
    );

if($login_obj->UserPrivilege('settings_view'))
$module_menu[] = array(
    "page"  => "nastaveni",
    "name"  => TNASTAVENI
    );
   
    
    
//------------------------------------------------
//definice nadpisu h1 a textu k nadpisu
//------------------------------------------------ 
$module_h1 = array();
$module_h1[] = array(
    "page"  => "rubriky",
    "action"=> "edit-content",
    "verify_id_page" => 1,
    "verify_id_action" => 1,
    "name"  => TEDITACE_CLANKU
    );
$module_h1[] = array(
    "page"  => "rubriky",
    "action"=> "new",
    "verify_id_page" => 1,
    "name"  => TNOVY_CLANEK
    );    
$module_h1[] = array(
    "page"  => "rubriky",
    "verify_id_page" => 1,
    "name"  => TSEZNAM_CLANKU." ".WEB_LANG,
    "text"  => 0
    ); 
$module_h1[] = array(
    "page"  => "nastaveni",
    "name"  => TNASTAVENI
    ); 
$module_h1[] = array(
    "page"  => "rubriky",
    "name"  => TSEZNAM_STRANEK_S_CLANKY." ".WEB_LANG,
    "text"  => 356
    );
    
$module_h1[] = array(
    "page"  => "tagy",
    "name"  => TSEZNAM_TAGU." ".WEB_LANG,
    "text"  => 388
    );
    
$module_h1[] = array(
    "page"  => "tagy",
    "action"=> "new",
    "name"  => TNOVY_TAG
    );

$module_h1[] = array(
    "page"  => "tagy",
    "action"=> "edit-content",
    "name"  => TEDITACE_TAGU,
    "verify_id_page" => 1
    );

?>