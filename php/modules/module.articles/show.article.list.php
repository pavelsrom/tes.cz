<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */


if(!defined('SECURITY_CMS')) exit;
if(!$this->is_access('rubriky','',1,0)) return;


$idRubriky = $this->get_url('id_page');

$main_tools = array();
        
if($login_obj->UserPrivilege('settings_add'))
    $main_tools[] = array(
        'ikona' => 'new', 
        "nazev" => TPRIDAT_CLANEK, 
        "aktivni" => 0, 
        "odkaz" => $this->get_link('rubriky',$idRubriky,'new')
        );
        
$main_tools[] = array(
    'ikona' => 'back', 
    "nazev" => TZPET, 
    "aktivni" => 0, 
    "odkaz" => $this->get_link('rubriky'), 
    "title" => TZPET_NA_SEZNAM_RUBRIK);
        
        
$data_rubriky = $db->query("SELECT idStranky, nazev FROM ".TABLE_STRANKY." WHERE typ='clanky' AND idDomeny=".$domain->getId()." AND idJazyka=".WEB_LANG_ID." ORDER BY nazev");
$select_rubriky = array(0 => TVSE);
while($r = $db->getAssoc($data_rubriky))
    $select_rubriky[$r['idStranky']] = $r['nazev']; 
        
$vsechny_stavy = array('rozpracovano' => TROZPRACOVANO, 'kontrola' => TKE_KONTROLE, 'zamitnuto' => TZAMITNUTO, 'hotovo' => THOTOVO);
$form = new FormFilter();
$form->set_class('filter2');
$form->add_text(THLEDAT, 'vyraz', "", 0, false, "", "vyraz");
$form->add_selectbox(TRUBRIKA,'rubrika',$idRubriky, $select_rubriky, 0, false,"","rubrika");
$form->add_selectbox(TSTAV,'rubrika',"",array("" => TVSE) + $vsechny_stavy, 0, false,"kratky","stav");
$form->add_text(TTAG,'tag',"",0,false,"kratky","tag");
$form->add_selectbox(TTOP_CLANEK,'top',-1, array(-1 => TVSE, 1 => TANO, 0 => TNE),0,false,"kratky","top");
echo $form->get_html_code(main_tools($main_tools));

$table = new Table("tList");
$tr = $table->tr_head();
    $tr->add(TID, 'w25')
    ->add(TTITULEK)
    ->add(TRUBRIKA)
    ->add(TTAGY)
    ->add(TDATUM,"w25")
    ->add(TPRUMERNE_HODNOCENI_POCET_HLASOVANI,"w150")
    ->add(TSTAV, 'w110')
    ->add(TTOP_CLANEK, 'w25')
    ->add(TDISKUZE, 'w25')
    ->add(TPRIVATNI, 'w25')
    ->add(TAKTIVNI, 'w25');
    if(MODULE_ARTICLES_ARCHIVE)
        $tr->add(TV_ARCHIVU, 'w25');
    $tr->add(TAKCE,'w110')
    ;
echo $table->get_html();    

?>
<script type="text/javascript">
<!--

$(function(){
var oTable = $("#tList").dataTable({
    "sAjaxSource": "<?php echo AJAX_GATEWAY;?>articles.list",
    "aaSorting": [[ 0, "desc" ]],
    "aoColumns": [
        {"sClass": "sl1"},{"sClass": "tleft sl2"},{"sClass": "sl3"},{"sClass": "sl4 tleft", "bSortable": false},{"sClass": "sl4"},{"sClass": "sl5"},{"sClass": "sl8"},{"sClass": "sl6"},{"sClass": "sl7"},{"sClass": "sl8", "bSortable": false},{"sClass": "sl8"}<?php if(MODULE_ARTICLES_ARCHIVE) echo ',{"sClass": "sl8"}';?>,{"sClass": "akce", "bSortable": false}
        ],
    "fnServerData": function ( sSource, aoData, fnCallback ) {
            aoData.push({ "name": "vyraz", "value": $("#vyraz").val() });
            aoData.push({ "name": "tagy", "value": $("#tag").val() });
            aoData.push({ "name": "rubrika", "value": $("#rubrika option:selected").val() });
            aoData.push({ "name": "stav", "value": $("#stav option:selected").val() });
            aoData.push({ "name": "top", "value": $("#top option:selected").val() });
            $.getJSON( sSource, aoData, function (json) { fnCallback(json); } );
            }
 
})

$("input#vyraz, input#tag").bind('keyup change', function() {
        oTable.fnDraw(); 
        });
        
$("select#rubrika, select#stav, select#top").bind('change', function() {
        oTable.fnDraw(); 
        });

//prida k bunce th span
$(".dataTable th").each(function(){
    $(this).html("<span>" + $(this).html() + "</span>");
})

$(document).on("click","a.delete", function(){
        var id = $(this).attr('id').substring(6);
        var tr = $(this).parents("tr").first();
        var c = $.trim($(this).parents("tr").find("td").eq(1).text()); 
        
        var text = "<?php echo TOPRAVDU_SMAZAT;?>";

        custom_confirm(text.replace("%s",c) + '?', function(){
            $.msg();
            tr.addClass("deleted");
            $.post("<?php echo AJAX_GATEWAY;?>articles.delete",{id: id},function(data){
                tr.fadeOut(function(){
                    $.msg( 'setClass', 'ok_message' );
                    $.msg( 'replace', '<?php echo OK_SMAZANO;?>' );
                    oTable.fnDraw();
                    $.msg( 'unblock', 1000);
                    });
                });
            
            })
        
        return false;
        
        
    });

    /* IMPLEMENTACE NASEPTAVACE tagu */
    
        function formatItem(row) {
    		return row[0];
    	}
    	function formatResult(row) {
    		return row[0].replace(/(<.+?>)/gi, '');
    	}
        
    $("#tag").autocomplete({
            source: "<?php echo AJAX_GATEWAY;?>tags.autocomplete", 
            minLength: 1,
            delay: 500,
            select: function( event, ui ) {
                $("#tag").val(ui.item.value);
                oTable.fnDraw();
                }
    	});


})

// -->
</script>
