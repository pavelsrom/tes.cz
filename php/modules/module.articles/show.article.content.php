<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined('SECURITY_CMS')) exit;
if(!$this->is_access('rubriky','edit-content',1,1,'content_view') &&
    !$this->is_access('rubriky','new',1,0,'content_add')
    ) return;

$idClanku = $this->get_id_action(); 
$idPage = $this->get_id_page();

if(!$object_access->has_access($idClanku))
{
    $systemMessage->add_warning(TNEMATE_OPRAVNENI_MENIT_OBSAH);
    $systemMessage->show_messages();
}

$main_tools = array();

if($idClanku > 0) {
    $record1 = $db->Query("SELECT s.*, j.jazyk,
                IF(s.od IS NULL, '', DATE_FORMAT(s.od,'%d.%m.%Y')) AS publikovat_od,
				IF(s.do IS NULL, '', DATE_FORMAT(s.do,'%d.%m.%Y')) AS publikovat_do,
				IF(s.datum IS NULL, '', DATE_FORMAT(s.datum,'%d.%m.%Y')) AS datum
				
			FROM ".TABLE_STRANKY." AS s
            LEFT JOIN ".TABLE_JAZYKY." AS j ON s.idJazyka = j.idJazyka 
			WHERE idStranky=".$idClanku."
                AND s.typ='clanek' 
				AND idDomeny=".$domain->getId()."
                AND s.idJazyka=".WEB_LANG_ID."
			LIMIT 1");
			
    $page = $db->getAssoc($record1);
          
    if($login_obj->UserPrivilege('content_view'))
        $main_tools[] = array(
            'ikona' => 'preview', 
            "nazev" => TNAHLED, 
            "aktivni" => 0, 
            "odkaz" => UrlPage($page['url'], $idClanku,$page['jazyk']), 
            "target" => "blank"
            ); 
            
            
    $page['publikovat_od'] = $page['publikovat_od'] == '00.00.0000' ? "" : $page['publikovat_od'];
    $page['publikovat_do'] = $page['publikovat_do'] == '00.00.0000' ? "" : $page['publikovat_do'];
    $page['datum'] = $page['datum'] == '00.00.0000' ? "" : $page['datum'];
    }
    else
    {
    $page['nazev'] = "";
    $page['nazevProMenu'] = "";
    $page['datum'] = date("d.m.Y");
    $page['publikovat_od'] = "";
    $page['publikovat_do'] = "";
    $page['nadpis'] = "";
    $page['perex'] = "";
    $page['obrazek'] = "";
    $page['obrazek2'] = "";
    $page['zobrazit'] = 1;
    $page['doMenu'] = 1;
    $page['archivovat'] = 0;
    $page['url'] = "";
    $page['title'] = "";
    $page['description'] = "";
    $page['obsah'] = "";
    $page['idGalerie'] = 0;
    $page['idAnkety'] = 0;
    $page['idFormulare'] = 0;
    $page['diskuze'] = 0;
    $page['autentizace'] = 0;
    $page['idProfiluPanelu'] = 0;
    $page['stav'] = 'hotovo';
    $page['poznamka'] = '';
    $page['citace'] = '';
    $page['autor'] = $login_obj->getName();
       
    }

$main_tools[] = array(
    'ikona' => 'back', 
    "nazev" => TZPET, 
    "aktivni" => 0, 
    "odkaz" => $this->get_link('rubriky',$idPage), 
    "title" => TZPET_NA_SEZNAM_CLANKU
    );

echo main_tools($main_tools);


$data_rubriky = $db->query("SELECT idStranky, nazev FROM ".TABLE_STRANKY." WHERE typ='clanky' AND idDomeny=".$domain->getId()." ORDER BY nazev");
$select_rubriky = array(0 => "-- ".TVYBERTE." --");
while($r = $db->getAssoc($data_rubriky))
    $select_rubriky[$r['idStranky']] = $r['nazev']; 
      
$form = new Form();
$form->allow_upload();
$form->add_section(TZAKLADNI_UDAJE, "home");
$form->add_text(TTITULEK, 'titulek', $page['nazev'], 83, true,"","nazev");
$form->add_text(TNAZEV_STRANKY_V_MENU, 'menu_nazev', $page['nazevProMenu']);
$form->add_selectbox(TRUBRIKA,'rubrika',$idClanku == 0 ? $idPage : $page['idRodice'],$select_rubriky, 0, true);
$form->add_text(TAUTOR, 'autor', $page['autor']);
//$form->add_radiobuttons(TZOBRAZOVAT, 'zobrazit', $page['zobrazit'], array(1 => TANO, 0 => TNE),6);
$vsechny_stavy = array('rozpracovano' => TROZPRACOVANO, 'kontrola' => TKE_KONTROLE, 'zamitnuto' => TZAMITNUTO, 'hotovo' => THOTOVO);
$form->add_radiobuttons(TTOP_CLANEK,"doMenu",$page['doMenu'],array(1 => TANO, 0 => TNE), 447);

//operovat se vsemi stavy muze jen opravneni editor/vydavatel a vyssi
if($login_obj->minPrivilege('editor'))
{
    $form->add_selectbox(TSTAV,'stav',$page['stav'],$vsechny_stavy, 402);
    $form->add_textarea(TPOZNAMKA_REDAKCE,'poznamka',$page['poznamka'],399);
}
else
{
    $stavy = array('rozpracovano' => TROZPRACOVANO, 'kontrola' => TKE_KONTROLE);
    
    //pokud stav neni povolen pro redaktora, pak se zobrazi stav v textove forme,aby jej nebyl mozne prepnout
    //stav hotovo a zamitnuto nemuze redaktor prepnout.
    if(isset($stavy[$page['stav']]))
        $form->add_selectbox(TSTAV,'stav',$page['stav'],$stavy);
        else
        {
            $form->add_plain_text(TSTAV, $vsechny_stavy[$page['stav']], 402);
            $form->add_hidden('stav',$page['stav']);
        }
    if($page['poznamka']!='')
        $form->add_plain_text(TPOZNAMKA_REDAKCE,$page['poznamka'],400);
    
    $form->add_hidden('poznamka',$page['poznamka']);
    
}
//kvuli porovnani stavu se uklada i stary stav
$form->add_hidden('stary_stav',$page['stav']);




$form->add_section(TOBSAH, "content");
$form->add_editor(TPEREX, 'perex',$page['perex'],84, false, "h300 wysiwygEditorMini","wysiwygEditorPerex");

/*
$form->add_file(TNAHRAT_OBRAZEK." pro detail produktu", 'obrazek2', false, 85,"","obrazek_input_perex")->set_disabled($page['obrazek2']!='');
  
        
$image_class = "";
if($page['obrazek2']=='' || $page['obrazek2']==null){
    $img = TNENI_ULOZEN_ZADNY_OBRAZEK;
    }
    else
    {	
    $file = $domain->getDir().USER_DIRNAME_ARTICLES_PEREX.$page['obrazek2'];
				
    if(file_exists($file)) {
        $src = RELATIVE_PATH.$file."?".time(); 
        $img = "<img src='".$src."' alt='".$page['obrazek2']."' title='".$page['obrazek2']."' /><br /><br />";
        $image_class = "image";
        if($login_obj->UserPrivilege('content_delete') && $object_access->has_access($idClanku))
            $img .= tlacitko('#',TSMAZAT,"","times","delete delete-perex photo-delete","delete-perex".$idClanku);
            else
            $img .= icon_disabled('delete');
        }
        else 
        {
        $src="";
        $img = TOBRAZEK_NENALEZEN;
        if($login_obj->UserPrivilege('content_delete'))
            //$img .= "<a href='#' class='delete' id='delete".$idClanku."'>".icon('delete','','absolute top left')."</a></span>";
            $img .= tlacitko('#',TSMAZAT,"","times","delete delete-perex photo-delete","delete".$idClanku);
            else
            $img .= icon_disabled('delete');
  
            }
        }
                              

$form->add_plain_text(TOBRAZEK." pro detail produktu", $img)->set_class_td($image_class." relative tleft");
$form->add_hidden('obrazek2',$page['obrazek2']);
*/




$form->add_editor(TOBSAH, 'obsah',$page['obsah'],0,false,"wysiwygEditor h500")->allow_wet();
//$form->add_editor("Citace", 'citace',$page['citace'],0,false,"wysiwygEditor h300")->allow_wet();
$form->add_file(TNAHRAT_OBRAZEK, 'obrazek', false, 85,"","obrazek_input")->set_disabled($page['obrazek']!='');
  
        
$image_class = "";
if($page['obrazek']=='' || $page['obrazek']==null){
    $img = TNENI_ULOZEN_ZADNY_OBRAZEK;
    }
    else
    {	
    $file = $domain->getDir().USER_DIRNAME_ARTICLES_MINI.$page['obrazek'];
				
    if(file_exists($file)) {
        $src = RELATIVE_PATH.$file."?".time(); 
        $img = "<img src='".$src."' alt='".$page['obrazek']."' title='".$page['obrazek']."'  style='min-width: 300px;'/><br /><br />";
        $image_class = "image";
        if($login_obj->UserPrivilege('content_delete') && $object_access->has_access($idClanku))
            $img .= tlacitko('#',TSMAZAT,"","times","delete delete1 photo-delete","delete".$idClanku);
            else
            $img .= icon_disabled('delete');
        }
        else 
        {
        $src="";
        $img = TOBRAZEK_NENALEZEN;
        if($login_obj->UserPrivilege('content_delete'))
            //$img .= "<a href='#' class='delete' id='delete".$idClanku."'>".icon('delete','','absolute top left')."</a></span>";
            $img .= tlacitko('#',TSMAZAT,"","times","delete delete1 photo-delete","delete".$idClanku);
            else
            $img .= icon_disabled('delete');
  
            }
        }
                              

$form->add_plain_text(TOBRAZEK, $img)->set_class_td($image_class." relative tleft");
$form->add_hidden('obrazek',$page['obrazek']);
      

//vypis galerie - dalsi fotky k strance
show_photos($idClanku, $form);

if(MODULE_GALLERY){
    $sel = ShowSelectBoxGallery();
    $sel = array(0 => "-- ".TVYBERTE." --") + $sel;

    if(count($sel) == 1){
        $text_neni = "";
        if($login_obj->UserPrivilege('content_add'))
            $text_neni .= tlacitko(get_link('gallery',"galerie",0,"new"));
        $form->add_plain_text(TGALERIE, $text_neni);

        }
        else
        $form->add_selectbox(TGALERIE, 'idGalerie', $page['idGalerie'], $sel);
           
    }
    else
    $form->add_hidden('idGalerie', $page['idGalerie']);
  
if(MODULE_INQUIRY){
    $d = $db->Query("SELECT idAnkety AS id, nazev FROM ".TABLE_ANKETY." WHERE idDomeny=".$domain->getId());
    $sel = array(0 => "-- ".TVYBERTE." --");
    $pocet = $db->numRows($d);
            
    while($s = $db->getAssoc($d))
        $sel[$s['id']] = $s['nazev'];
                
    if(count($sel) == 1){
        $text_neni = "";
        if($login_obj->UserPrivilege('content_add'))
            $text_neni .= tlacitko(get_link('inquiry',"ankety",0,'new'));
        
        $form->add_plain_text(TANKETA, $text_neni);    
        }
        else
        $form->add_selectbox(TANKETA, 'idAnkety', $page['idAnkety'], $sel);
    
    }
    else
    $form->add_hidden('idAnkety', $page['idAnkety']);

if(MODULE_DISCUSSION)
    $form->add_radiobuttons(TDISKUZE, 'diskuze', $page['diskuze'], array(1 => TANO, 0=>TNE));
    else
    $form->add_hidden('diskuze', $page['diskuze']);
    

            
$data_panely = $db->Query("SELECT idProfilu,nazev FROM ".TABLE_PANELY_SABLONY." WHERE idDomeny=".$domain->getId()." AND zobrazit=1 AND idJazyka=".WEB_LANG_ID." ORDER BY vychozi DESC ");
                
$profily = array();
while($profil = $db->getAssoc($data_panely))
    $profily[$profil['idProfilu']] = $profil['nazev'];
/*
$form->add_section(TSABLONA_PANELU, 'panel-setting');
$form->add_selectbox(TSABLONA_PANELU, 'idProfiluPanelu', $page['idProfiluPanelu'], $profily, 135);         
*/

$form->add_section(TNACASOVANI_ZOBRAZENI_A_ARCHIVACE, 'date');
$form->add_text(TDATUM_CLANKU, 'datum', $page['datum'], 0, false, "kalendar","datum");
$form->add_text(TOD, 'od', $page['publikovat_od'], 89, false, "kalendar","od");
$form->add_text(TDO, 'do', $page['publikovat_do'], 88, false, "kalendar","do");

if(MODULE_ARTICLES_ARCHIVE)
    $form->add_radiobuttons(TARCHIVOVAT, 'archivovat', $page['archivovat'], null,150);
    


$default_values = array();
$default_id = array();
$values = array();
$data = $db->Query("SELECT s.idStranky,s.idRodice,s.url,s.nazev, IF(sc.idSouvisejicihoClanku IS NULL,0,1) AS souvisejici 
            FROM ".TABLE_STRANKY." AS s
            LEFT JOIN ".TABLE_SOUVISEJICI_CLANKY." sc ON s.idStranky=sc.idSouvisejicihoClanku AND sc.idClanku=".$idClanku."
            WHERE s.typ='clanek' 
                AND s.idStranky!=".$idClanku."
                AND s.idDomeny=".$domain->getId()."
                 AND s.idJazyka=".WEB_LANG_ID."
            GROUP BY s.idStranky
            ORDER BY sc.idRelace
            ");
            
            
if($idClanku > 0)
{
    

    while($c = $db->getAssoc($data))
    {
        if($c['souvisejici']==1 && $idClanku>0)
        { 
            $default_values[] = $c['idStranky'];
            $values[$c['idStranky']] = $c['nazev'];
        }
        //$values[$c['idStranky']] = $c['nazev'];
        $default_id[$c['idStranky']] = "souvisejici s".$c['idRodice'];
    }
}
else
{
    $values_post = get_array_post('souvisejici');
    foreach($values_post AS $ids)
    {
        $values[$ids] = $db->get(TABLE_STRANKY,"nazev","idStranky=".$ids);
    }

}

    
/*
$data_rubriky = $db->query("SELECT idStranky AS id FROM ".TABLE_STRANKY." WHERE idDomeny=".$domain->getId()." AND typ='clanky' AND idJazyka=".WEB_LANG_ID." ORDER BY nazev");


$rubriky_a = array();
if($db->numRows($data_rubriky) > 0)
    $rubriky_a[] = "<a href='#' class='souvisejici_tab' id='st0'>".TVSECHNY_RUBRIKY."</a>";
        


while($dr = $db->getAssoc($data_rubriky))
    $rubriky_a[] = "<a href='#' class='souvisejici_tab' id='st".$dr['id']."'>".$links->get($dr['id'],'nazev')."</a>";
*/


if(MODULE_RELATED_ARTICLES)
{
    $form->add_section(TSOUVISEJICI_CLANKY, 'content',help(403,true));
    
        
    if($db->numRows($data)==0)
        $form->add_plain_text("", TNENALEZEN_ZADNY_ZAZNAM);
        else
        $form->add_multiple_selectbox(TSOUVISEJICI_CLANKY, 'souvisejici', $default_values, $values, 149,false,"multiselect");
        
    //stitky
    show_tags_settings($idClanku, "stranka", WEB_LANG, $form);
    
}
else
{
    foreach($default_values AS $v)
        $form->add_hidden('souvisejici[]', $v);
}



//show_private_pages_settings($idClanku, $page, $form);



if($login_obj->UserPrivilege('seo'))
{
    $form->add_section(TSEO_PARAMETRY, 'seo', "", 8);
    $form->add_text(TURL_NAZEV, 'url', $page['url'], 9, $idClanku > 0);
    $form->add_text(TTITLE, 'title', $page['title'], 10,false,'','title');
    $form->add_textarea(TDESCRIPTION, 'description', $page['description'], 11);	
}
else
{
    $form->add_hidden('url', $page['url']);
    $form->add_hidden('title', $page['title'], 'title');
    $form->add_hidden('description', $page['description']);	
}
		
$res = (($login_obj->UserPrivilege('content_edit') && $idClanku>0) || ($login_obj->UserPrivilege('content_add') && $idClanku==0)) && $object_access->has_access($idClanku);

$form->add_next_actions();
$form->add_submit()->set_disabled(!$res);
    
echo $form->get_html_code();

?>


<script type="text/javascript">
<!--


$(function(){
    
    $("select.multiselect").multiselect({
        searchDelay: 500,
        nodeComparator: null,
        remoteParams: {id_clanku: <?php echo $idClanku;?>},
        remoteUrl: "<?php echo AJAX_GATEWAY;?>multiselect.search"
    });
    
    
    $("a.delete1").click(function(){
        var id = $(this).attr('id').substring(6);
        
        var text = "<?php echo TOPRAVDU_SMAZAT_TENTO_OBRAZEK;?>";
        var td = $(this).parents("td.image").first();
        
        custom_confirm(text, function(){
            $.msg();
            td.addClass("deleted");
            $.post("<?php echo AJAX_GATEWAY;?>articles.image.delete",{id: id},function(data){
                $.msg( 'setClass', 'ok_message' );
                $.msg( 'replace', '<?php echo OK_SMAZANO;?>' );
                $.msg( 'unblock', 1000);
                td.html("<?php echo TNENI_ULOZEN_ZADNY_OBRAZEK;?>");
                $("#obrazek_input").removeAttr('disabled');
                td.removeAttr("class");
                });
            
            })
            
        return false;
        
    })
    
    $("a.delete-perex").click(function(){
        var id = $(this).attr('id').substring(6);
        
        var text = "<?php echo TOPRAVDU_SMAZAT_TENTO_OBRAZEK;?>";
        var td = $(this).parents("td.image").first();
        
        custom_confirm(text, function(){
            $.msg();
            td.addClass("deleted");
            $.post("<?php echo AJAX_GATEWAY;?>articles.image.perex.delete",{id: id},function(data){
                $.msg( 'setClass', 'ok_message' );
                $.msg( 'replace', '<?php echo OK_SMAZANO;?>' );
                $.msg( 'unblock', 1000);
                td.html("<?php echo TNENI_ULOZEN_ZADNY_OBRAZEK;?>");
                $("#obrazek_input_perex").removeAttr('disabled');
                td.removeAttr("class");
                });
            
            })
            
        return false;
        
    })
    
    
    function zobrazit_souvisejici(id)
    {
        $("a.souvisejici_tab").removeClass("bold");
        $("a#st" + id).addClass("bold");
        if(id == 0)
        {
            $(".souvisejici").show();
        }
        else
        {
            $(".souvisejici").hide();
            $(".s" + id).show();
        }
        
    }
    
    
    $("a.souvisejici_tab").click(function(){
        var id = $(this).attr("id").substring(2);
        zobrazit_souvisejici(id);
        return false;
    })
    
    
    zobrazit_souvisejici(<?php echo $idPage; ?>);
    
})
// -->
</script>