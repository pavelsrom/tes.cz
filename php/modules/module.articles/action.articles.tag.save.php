<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */


if(!defined('SECURITY_CMS')) exit;
if(!is_action() ||
    (!$this->is_access('tagy','edit-content',1,0,'content_edit') &&
     !$this->is_access('tagy','new',0,0,'content_add'))
    ) return;
    
$idStranky = $this->get_id_page();
$titulek = $nadpis = get_post('nazev');
$url = get_post('url');
$title = get_post('title');

if($idStranky==0 && $title=='')
    $title = $titulek;
            
$desc = get_post('description');
            
$url = $idStranky==0 && $url=='' ? ModifyUrl($titulek) : ModifyUrl($url);
$url_new = get_url_nazev($idStranky, $url);
if($url!=$url_new)
    $systemMessage->add_warning(WARNING_URLNAZEV_MODIFIKOVAN." ".$url_new);
$url = $url_new;

if($url == "")
    $systemMessage->add_error(ERROR_NEPLATNY_URL_NAZEV);

$update = array();

if(!TestSentenceCZ($titulek, 150, false))
    $systemMessage->add_error(ERROR_PRAZDNY_TITULEK);
			
			
if($systemMessage->error_exists()) return;

$idStrankyStitky = $db->get(TABLE_STRANKY,"idStranky", "typ='tagy' AND idDomeny=".$domain->getId()." AND idJazyka=".WEB_LANG_ID);
$idStrankyStitky = intval($idStrankyStitky);

$update = $update + array(
    "idDomeny"      => $domain->getId(),
    "typ"           => "tag",
    "nazev"         => $titulek,
    "idRodice"      => $idStrankyStitky,
    "nazevProMenu"  => $titulek,
    "nadpis"        => $nadpis,
    "zobrazit"      => 1,
    "title"         => $title,
    "description"   => $desc,
    "url"           => $url,
    "idJazyka"      => WEB_LANG_ID
    );			
		

        
if($idStranky == 0){
    $update['idVytvoril'] = $login_obj->getId();
    $update['jmenoVytvoril'] = $login_obj->getName();
    $update['datumVytvoril'] = "now";
    
    $update['idAktualizoval'] = $login_obj->getId();
    $update['jmenoAktualizoval'] = $login_obj->getName();
    $update['datumAktualizoval'] = "now";
    
    $db->insert(TABLE_STRANKY, $update);
    $idStranky = $db->lastId();
    }
	else
    {
    $update['idAktualizoval'] = $login_obj->getId();
    $update['jmenoAktualizoval'] = $login_obj->getName();
    $update['datumAktualizoval'] = "now";
	$db->update(TABLE_STRANKY,$update,"idStranky=".$idStranky." AND idDomeny=".$domain->getId());
	}			

$log->add_log($this->get_url('id_page') == 0 ? 'create':'edit-content','tag',$idStranky,$titulek);
 
$systemMessage->add_ok(OK_ULOZENO);

if($this->get_action() == 'new')
    Redirect($this->get_link('tagy',0,'new',0));
    else
    Redirect($this->get_link('tagy',$idStranky,'edit-content'));

?>