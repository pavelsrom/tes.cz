<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */


if(!defined('SECURITY_CMS')) exit;
if(!$this->is_access('tagy','edit-content',1,0,'content_view') &&
    !$this->is_access('tagy','new',0,0,'content_add')
    ) return;
    
$idPage = $this->get_id_page();
$action = $this->get_action();

$main_tools = array();

if($idPage > 0) {

			$record1 = $db->Query("SELECT s.idStranky, s.nazev, s.title,s.description, s.url, s.nadpis
                
			FROM ".TABLE_STRANKY." AS s
			WHERE idStranky=".$idPage." 
				AND idDomeny=".$domain->getId()."
                AND idJazyka=".WEB_LANG_ID."
                AND typ='tag'
			LIMIT 1");
			
			
			$page = $db->getAssoc($record1);

            if($login_obj->UserPrivilege('content_view'))
                $main_tools[] = array(
                    'ikona' => 'preview', 
                    "nazev" => TNAHLED, 
                    "aktivni" => 0, 
                    "odkaz" => $links->get_url($idPage), 
                    'target' => "blank"
                    ); 

			}
			else
			{
			$page['nazev'] = "";
            $page['idRodice'] = 0;
            $page['nadpis'] = "";
            $page['url'] = "";
			$page['title'] = "";
			$page['description'] = "";
			}

$main_tools[] = array(
    'ikona' => 'back', 
    "nazev" => TZPET, 
    "aktivni" => 0, 
    "odkaz" => $this->get_link('tagy'), 
    "title" => TZPET_NA_SEZNAM
    ); 

echo main_tools($main_tools);
$form = new Form();
$form->allow_upload();
$form->add_section(TZAKLADNI_UDAJE, 'home');
$form->add_text(TTAG,'nazev',$page['nazev'],0,true,"","nazev");
$form->add_text(TNADPIS,'nadpis',$page['nadpis'],0,true,"","nadpis");
        
if($login_obj->UserPrivilege('seo'))
{
    $form->add_section(TSEO_PARAMETRY, 'seo', "", 8);
    $form->add_text(TURL_NAZEV, 'url', $page['url'], 9, $idPage > 0);
    $form->add_text(TTITLE, 'title', $page['title'], 10,false,'','title');
    $form->add_textarea(TDESCRIPTION, 'description', $page['description'], 11);	
}
else
{
    $form->add_hidden('url', $page['url']);
    $form->add_hidden('title', $page['title'], 'title');
    $form->add_hidden('description', $page['description']);	
}
		
$res = ($login_obj->UserPrivilege('content_edit') && $idPage>0) || ($login_obj->UserPrivilege('content_edit') && $idPage==0);

$form->add_submit()->set_disabled(!$res);
    
echo $form->get_html_code();
        
?>


<script type="text/javascript">
<!--

$(function(){
    
    

    var idn = <?php echo $idPage;?>;
    
    $('#nazev').change(function(){
        if(idn > 0) return;
        var n = $(this).val();
        $('#title').val(n);
        $('#nadpis').val(n);      
        });
    

})

// -->
</script>