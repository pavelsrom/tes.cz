<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */


//------------------------------------------------
//definice polozek menu
//------------------------------------------------

$module_menu = array();

$module_menu[] = array(
    "page"  => "akce",
    "name"  => TKALENDARNI_AKCE." ".WEB_LANG
    );
    
$module_menu[] = array(
    "page"  => "akce-typy",
    "name"  => TTYPY_KALENDARNICH_AKCI." ".WEB_LANG
    );  
    
if($login_obj->UserPrivilege('settings_view'))
$module_menu[] = array(
    "page"  => "nastaveni",
    "name"  => TNASTAVENI
    );     
    
//------------------------------------------------
//definice nadpisu h1 a textu k nadpisu
//------------------------------------------------ 
$module_h1 = array();

$module_h1[] = array(
    "page"  => "akce",
    "action"=> "edit-content",
    "verify_id_action" => 1,
    "name"  => TEDITACE_KALENDARNI_AKCE
    );
$module_h1[] = array(
    "page"  => "akce",
    "action"=> "new",
    "name"  => TNOVA_KALENDARNI_AKCE
    );   
$module_h1[] = array(
    "page"  => "akce",
    "action"=> "terminy",
    "verify_id_page" => 1,
    "name"  => TTERMINY_AKCE
    );  
$module_h1[] = array(
    "page"  => "akce",
    "action"=> "termin-new",
    "verify_id_page" => 1,
    "name"  => TNOVY_TERMIN
    ); 
$module_h1[] = array(
    "page"  => "akce",
    "action"=> "termin-edit",
    "verify_id_page" => 1,
    "verify_id_action" => 1,
    "name"  => TEDITACE_TERMINU
    );
$module_h1[] = array(
    "page"  => "akce",
    "name"  => TKALENDARNI_AKCE." ".WEB_LANG,
    "text"  => 0
    ); 
$module_h1[] = array(
    "page"  => "akce-typy",
    "name"  => TTYPY_KALENDARNICH_AKCI." ".WEB_LANG,
    "text"  => 0
    ); 
$module_h1[] = array(
    "page"  => "nastaveni",
    "name"  => TNASTAVENI
    );



?>