<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */


if(!defined('SECURITY_CMS')) exit;
if(!$this->is_access('akce','terminy',true)) return;

$main_tools = array();
        
$idAkce = $this->get_id_page();

if(!$object_access->has_access($idAkce))
{
    $systemMessage->add_warning(TNEMATE_OPRAVNENI_MENIT_OBSAH);
    $systemMessage->show_messages();
}

$nazev = $db->get(TABLE_STRANKY,'nazev',"idStranky=".$idAkce." AND idDomeny=".$domain->getId()." AND idJazyka=".WEB_LANG_ID." AND typ='akce'");
        
if($login_obj->UserPrivilege('content_add'))
    $main_tools[] = array(
        'ikona' => 'new', 
        "nazev" => TNOVY_TERMIN, 
        "aktivni" => 0, 
        "odkaz" => $this->get_link('akce',$idAkce,'termin-new')
        );
        
$main_tools[] = array(
        'ikona' => 'back', 
        "nazev" => TZPET_NA_SEZNAM, 
        "aktivni" => 0, 
        "odkaz" => $this->get_link('akce')
        );
        
        
echo '<h2>'.TKALENDARNI_AKCE.": ".$nazev.'</h2>';
    
echo main_tools($main_tools);
        
$table = new Table('tList');
$table->tr_head()
    ->add(TID,'w25')
    ->add(TOD)
    ->add(TDO)
    ->add(TSTAV,"w110")
    ->add(TAKCE,'w110');
    
echo $table->get_html();
        
       
?>

<script type="text/javascript">
<!--


$(function(){
var oTable = $("#tList").dataTable({
    "sAjaxSource": "<?php echo AJAX_GATEWAY;?>events.dates.list",
    "aoColumns": [
        {"sClass": "sl1"},{"sClass": "tleft sl2"},{"sClass": "sl3"},{"sClass": "sl4"},{"sClass": "akce", "bSortable": false}
        ],
    "fnServerData": function ( sSource, aoData, fnCallback ) {
            aoData.push({ "name": "id", "value": <?php echo $idAkce;?> });
            $.getJSON( sSource, aoData, function (json) { fnCallback(json); } );
            }
 
})


//prida k bunce th span
$(".dataTable th").each(function(){
    $(this).html("<span>" + $(this).html() + "</span>");
})


$(document).on("click","a.delete", function(){
        var id = $(this).attr('id').substring(6);
        var tr = $(this).parents("tr").first();
        var c = $.trim($(this).parents("tr").find("td").eq(1).text()); 
        
        var text = "<?php echo TOPRAVDU_SMAZAT;?>";

        custom_confirm(text.replace("%s",c) + '?', function(){
            $.msg();
            tr.addClass("deleted");
            $.post("<?php echo AJAX_GATEWAY;?>events.dates.delete",{id: id, id_akce: <?php echo $idAkce;?>},function(data){
                tr.fadeOut(function(){
                    $.msg( 'setClass', 'ok_message' );
                    $.msg( 'replace', '<?php echo OK_SMAZANO;?>' );
                    oTable.fnDraw();
                    $.msg( 'unblock', 1000);
                    });
                });
            
            })
        
        return false;
        
        
    });
    



})
// --> 
</script>
