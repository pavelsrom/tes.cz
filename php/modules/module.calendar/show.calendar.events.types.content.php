<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */


if(!defined('SECURITY_CMS')) exit;
if(!$this->is_access('akce-typy','edit-content',0,1,'content_view') &&
    !$this->is_access('akce-typy','new',0,0,'content_add')
    ) return;
    
$idPage = $this->get_id_action();

if(!$object_access->has_access($idPage))
{
    $systemMessage->add_warning(TNEMATE_OPRAVNENI_MENIT_OBSAH);
    $systemMessage->show_messages();
}

$main_tools = array();

if($idPage > 0) {

			$record1 = $db->Query("SELECT s.*
			FROM ".TABLE_STRANKY." AS s
			WHERE idStranky=".$idPage." 
				AND idDomeny=".$domain->getId()."
                AND idJazyka=".WEB_LANG_ID."
                AND typ='kalendar-typ'
			LIMIT 1");
			
			
			$page = $db->getAssoc($record1);

            if($login_obj->UserPrivilege('content_view'))
                $main_tools[] = array(
                    'ikona' => 'preview', 
                    "nazev" => TNAHLED, 
                    "aktivni" => 0, 
                    "odkaz" => $links->get_url($idPage), 
                    'target' => "blank"
                    ); 

			}
			else
			{
			$page['nazev'] = "";
            $page['barva'] = "";
            $page['datum'] = date("d.m.Y");
            $page['publikovat_od'] = "";
			$page['publikovat_do'] = "";
			$page['nadpis'] = "";
            $page['perex'] = "";
            $page['obrazek'] = "";
			$page['zobrazit'] = 1;
			$page['url'] = "";
			$page['title'] = "";
			$page['description'] = "";
			$page['obsah'] = "";
            $page['idGalerie'] = 0;
            $page['idAnkety'] = 0;
            $page['idFormulare'] = 0;
            $page['autentizace'] = 0;            
            $page['diskuze'] = 0;
			$page['idProfiluPanelu'] = 0;
			}

$main_tools[] = array(
    'ikona' => 'back', 
    "nazev" => TZPET, 
    "aktivni" => 0, 
    "odkaz" => $this->get_link('akce-typy'), 
    "title" => TZPET_NA_SEZNAM
    ); 

echo main_tools($main_tools);
$form = new Form();
$form->allow_upload();
$form->add_section(TZAKLADNI_UDAJE, 'home');
$form->add_text(TNAZEV,'nazev',$page['nazev'],2,true,"","nazev");
$form->add_text(TBARVA,'barva',$page['barva'],83,true,"color","barva");

$form->add_section(TOBSAH, "content");
$form->add_editor(TOBSAH,'obsah',$page['obsah'])->allow_wet();


show_private_pages_settings($idPage, $page, $form);


if($login_obj->UserPrivilege('seo'))
{
    $form->add_section(TSEO_PARAMETRY, 'seo', "", 8);
    $form->add_text(TURL_NAZEV, 'url', $page['url'], 9, $idPage > 0);
    $form->add_text(TTITLE, 'title', $page['title'], 10,false,'','title');
    $form->add_textarea(TDESCRIPTION, 'description', $page['description'], 11);	
}
else
{
    $form->add_hidden('url', $page['url']);
    $form->add_hidden('title', $page['title'], 'title');
    $form->add_hidden('description', $page['description']);	
}
		
$res = (($login_obj->UserPrivilege('content_edit') && $idPage>0) || ($login_obj->UserPrivilege('content_edit') && $idPage==0)) && $object_access->has_access($idPage);

$form->add_next_actions();
$form->add_submit()->set_disabled(!$res);
    
echo $form->get_html_code();
        
?>


<script type="text/javascript">
<!-- 
$(function(){   

    var idn = <?php echo $idPage;?>;
    
    $('#nazev').change(function(){
        if(idn > 0) return;
        var n = $(this).val();
        $('#title').val(n);
               
        });
  
    
})
// --> 
</script>