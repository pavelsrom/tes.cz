<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */


if(!defined('SECURITY_CMS')) exit;
if(!$this->is_access('akce', 'termin-edit',1,1,'content_view') &&
    !$this->is_access('akce', 'termin-new',1,0,'content_add')
    ) return;
    
$idPage = $this->get_id_page();
    
if(!$object_access->has_access($idPage))
{
    $systemMessage->add_warning(TNEMATE_OPRAVNENI_MENIT_OBSAH);
    $systemMessage->show_messages();
}
    
$nazev = $db->get(TABLE_STRANKY,'nazev',"idStranky=".$idPage." AND idDomeny=".$domain->getId()." AND idJazyka=".WEB_LANG_ID." AND typ='akce'");

if($nazev == '')
    return;

$idTerminu = $this->get_id_action();

$main_tools = array();

if($idTerminu > 0) {

			$record1 = $db->Query("SELECT 
                IF(od IS NULL OR od = '','',DATE_FORMAT(od,'%d.%m.%Y')) AS datum_od,
                IF(od IS NULL OR od = '','',DATE_FORMAT(od,'%H:%i')) AS cas_od,
                IF(do IS NULL OR do = '','',DATE_FORMAT(do,'%d.%m.%Y')) AS datum_do,
                IF(do IS NULL OR do = '','',DATE_FORMAT(do,'%H:%i')) AS cas_do
			FROM ".TABLE_KALENDAR_TERMINY." AS t
			WHERE idAkce=".$idPage." 
                AND idTerminu=".$idTerminu."
			LIMIT 1");
			
			
			$page = $db->getAssoc($record1);

            $page['cas_od'] = str_replace("00:00","",$page['cas_od']);
            $page['cas_do'] = str_replace("00:00","",$page['cas_do']);

			}
			else
			{
			$page['datum_od'] = "";
            $page['datum_do'] = "";
            $page['cas_od'] = "";
            $page['cas_do'] = "";
			}

$main_tools[] = array(
    'ikona' => 'back', 
    "nazev" => TZPET, 
    "aktivni" => 0, 
    "odkaz" => $this->get_link('akce',$idPage,'terminy'), 
    "title" => TZPET_NA_SEZNAM
    ); 

echo '<h2>'.TKALENDARNI_AKCE.": ".$nazev.'</h2>';


echo main_tools($main_tools);
$form = new Form();
$form->allow_upload();
$form->add_section(TZAKLADNI_UDAJE, 'home');
$form->add_text(TZACATEK_AKCE_DATUM,'datum_od',$page['datum_od'],0,true,"kalendar","od");
$form->add_text(TZACATEK_AKCE_CAS,'cas_od',$page['cas_od']);
$form->add_text(TKONEC_AKCE_DATUM,'datum_do',$page['datum_do'],0,true,"kalendar","do");
$form->add_text(TKONEC_AKCE_CAS,'cas_do',$page['cas_do']);

		
$res = (($login_obj->UserPrivilege('content_edit') && $idPage>0) || ($login_obj->UserPrivilege('content_edit') && $idPage==0)) && $object_access->has_access($idPage);

$form->add_submit()->set_disabled(!$res);
    
echo $form->get_html_code();
        
?>
