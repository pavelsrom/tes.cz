<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */


if(!defined('SECURITY_CMS')) exit;
if(!is_action() ||
    (!$this->is_access('akce-typy','edit-content',0,1,'content_edit') &&
     !$this->is_access('akce-typy','new',0,0,'content_add'))
     || !$object_access->has_access($this->get_id_action())
    ) return;
    
$idStranky = $this->get_id_action();
$titulek = $nadpis = get_post('nazev');
$url =get_post('url');
$title = get_post('title');
$barva = get_post('barva');
$autentizace = get_int_post('autentizace');

if($idStranky==0 && $title=='')
    $title = $titulek;
            
$desc = get_post('description');

$obsah = get_post('obsah','',false);
            
$url = $idStranky==0 && $url=='' ? ModifyUrl($titulek) : ModifyUrl($url);
$url_new = get_url_nazev($idStranky, $url);
if($url!=$url_new)
    $systemMessage->add_warning(WARNING_URLNAZEV_MODIFIKOVAN." ".$url_new);
$url = $url_new;

if($url == "")
    $systemMessage->add_error(ERROR_NEPLATNY_URL_NAZEV);

$update = array();

if(!TestSentenceCZ($titulek, 150, false))
    $systemMessage->add_error(ERROR_PRAZDNY_TITULEK);
			
			
if($systemMessage->error_exists()) return;

$idStrankyKalendare = $db->get(TABLE_STRANKY,"idStranky", "typ='kalendar-akci' AND idDomeny=".$domain->getId()." AND idJazyka=".WEB_LANG_ID);
$idStrankyKalendare = intval($idStrankyKalendare);

$update = $update + array(
    "idDomeny"      => $domain->getId(),
    "typ"           => "kalendar-typ",
    "barva"         => $barva,
    "nazev"         => $titulek,
    "idRodice"      => $idStrankyKalendare,
    "nazevProMenu"  => $titulek,
    "nadpis"        => $nadpis,
    "zobrazit"      => 1,
    "title"         => $title,
    "description"   => $desc,
    "url"           => $url,
    "obsah"         => $obsah,
    "obsahBezHtml"  => strip_tags($obsah),
    "idJazyka"      => WEB_LANG_ID,
    "autentizace"   => $autentizace
    );			
		

        
if($idStranky == 0){
    $update['idVytvoril'] = $login_obj->getId();
    $update['jmenoVytvoril'] = $login_obj->getName();
    $update['datumVytvoril'] = "now";
    
    $update['idAktualizoval'] = $login_obj->getId();
    $update['jmenoAktualizoval'] = $login_obj->getName();
    $update['datumAktualizoval'] = "now";
    
    $db->insert(TABLE_STRANKY, $update);
    $idStranky = $db->lastId();
    $object_access->add_allowed_object($idStranky);
    }
	else
    {
    $update['idAktualizoval'] = $login_obj->getId();
    $update['jmenoAktualizoval'] = $login_obj->getName();
    $update['datumAktualizoval'] = "now";
	$db->update(TABLE_STRANKY,$update,"idStranky=".$idStranky." AND idDomeny=".$domain->getId());
	}			

$skupiny = get_array_post('skupiny');
$db->delete(TABLE_STRANKY_SKUPINY,"WHERE idStranky=".$idStranky);
if(count($skupiny)>0)
{
    foreach($skupiny AS $s)
    {
        $insert = array("idStranky" => $idStranky, "idSkupiny" => $s);
        $db->insert(TABLE_STRANKY_SKUPINY,$insert);
    }
}

if(MODULE_CONTENT_BACKUP)
{
    $content_backup = new ContentBackup($domain->getId(),$idStranky,$login_obj->getId());
    $content_backup->AddStore($obsah);
}


$chybne_odkazy_indik = false;

if(MODULE_LINK_CHECKER)
{
    $chybne_odkazy = new LinksChecker($domain->getId(), $idStranky, "stranka", $obsah, false, KONTROLOVAT_ODKAZY_PRI_UKLADANI);
    $chybne_odkazy->set_base_url( $domain->getUrl() );
    $chybne_odkazy->process();
    $pocet_chybnych_obrazku = $chybne_odkazy->get_sum_error_images();
    $pocet_chybnych_odkazu = $chybne_odkazy->get_sum_error_links();
    
    if($pocet_chybnych_obrazku > 0)
        $systemMessage->add_warning(TPOCET_CHYBNYCH_OBRAZKU_NA_STRANCE.": ".$pocet_chybnych_obrazku);
        
    if($pocet_chybnych_odkazu > 0)
        $systemMessage->add_warning(TPOCET_CHYBNYCH_ODKAZU_NA_STRANCE.": ".$pocet_chybnych_odkazu);
    
    if(!$chybne_odkazy_indik && ($pocet_chybnych_obrazku > 0 || $pocet_chybnych_odkazu > 0)) 
        $chybne_odkazy_indik = true;
        
                
}


if($chybne_odkazy_indik)
    $systemMessage->add_warning(TODKAZY_ZKONTROLUJTE_A_OPRAVTE);
 
$log->add_log($this->get_url('id_action') == 0 ? 'create':'edit-content','kalendar-typ',$idStranky,$titulek);
 
$systemMessage->add_ok(OK_ULOZENO);


set_next_action_code();
$dalsi_akce = get_next_action_code();
if($dalsi_akce == 0) //zpet na vypis
    $url = $this->get_link('akce-typy');
elseif($dalsi_akce == 1) //zpet na editaci
    $url = $this->get_link('akce-typy',0,'edit-content',$idStranky);
elseif($dalsi_akce == 2) //vytvorit dalsi
    $url = $this->get_link('akce-typy',0,'new');



Redirect($url);

?>