<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */


if(!defined('SECURITY_CMS')) exit;
if(!$this->is_access('akce')) return;

$main_tools = array();
        
if($login_obj->UserPrivilege('content_add'))
    $main_tools[] = array(
        'ikona' => 'new', 
        "nazev" => TPRIDAT_AKCI, 
        "aktivni" => 0, 
        "odkaz" => $this->get_link('akce',0,'new')
        );

$data = $db->query("SELECT idStranky, nazev FROM ".TABLE_STRANKY." WHERE idDomeny=".$domain->getId()." AND idJazyka=".WEB_LANG_ID." AND typ='kalendar-typ'");

$selectbox_typy_akci = array(0 => "-- ".TVYBERTE." --");
while($t = $db->getAssoc($data))
    $selectbox_typy_akci[$t['idStranky']] = $t['nazev'];

            
$form = new FormFilter();
$form->set_class('filter3');
$form->add_text(THLEDAT, 'vyraz', "", 0, false, "", "vyraz");
$form->add_selectbox(TTYP_AKCE,'typ',0,$selectbox_typy_akci,0,false,"","typ_akce");
echo $form->get_html_code(main_tools($main_tools));
        
$table = new Table('tList');
$table->tr_head()
    ->add(TID,'w25')
    ->add(TNAZEV)
    ->add(TMISTO_KONANI)
    ->add(TTYP_AKCE)
    ->add(TTERMINY_AKCE)
    ->add(TPRIVATNI,'w25')
    ->add(TAKTIVNI,'w25')
    ->add(TAKCE,'w150');
    
echo $table->get_html();
        
       
?>
<script type="text/javascript">
<!--

$(function(){
var oTable = $("#tList").dataTable({
    "sAjaxSource": "<?php echo AJAX_GATEWAY;?>calendar.events.list",
    "aaSorting": [[ 0, "desc" ]],
    "aoColumns": [
        {"sClass": "sl1"},{"sClass": "tleft sl2"},{"sClass": "t-left sl3"},{"sClass": "t-left sl4"},{"sClass": "sl6"},{"sClass": "sl8", "bSortable": false},{"sClass": "sl7"},{"sClass": "akce", "bSortable": false}
        ],
    "fnServerData": function ( sSource, aoData, fnCallback ) {
            aoData.push({ "name": "vyraz", "value": $("#vyraz").val() });
            aoData.push({ "name": "typ_akce", "value": $("#typ_akce option:selected").val() });
            $.getJSON( sSource, aoData, function (json) { fnCallback(json); } );
            }
 
})

$("input#vyraz, select#typ_akce").bind('keyup change', function() {
        oTable.fnDraw(); 
        });

//prida k bunce th span
$(".dataTable th").each(function(){
    $(this).html("<span>" + $(this).html() + "</span>");
})


$(document).on("click","a.delete", function(){
        var id = $(this).attr('id').substring(6);
        var tr = $(this).parents("tr").first();
        var c = $.trim($(this).parents("tr").find("td").eq(1).text()); 
        
        var text = "<?php echo TOPRAVDU_SMAZAT;?>";

        custom_confirm(text.replace("%s",c) + '?', function(){
            $.msg();
            tr.addClass("deleted");
            $.post("<?php echo AJAX_GATEWAY;?>calendar.events.delete",{id: id},function(data){
                tr.fadeOut(function(){
                    $.msg( 'setClass', 'ok_message' );
                    $.msg( 'replace', '<?php echo OK_SMAZANO;?>' );
                    oTable.fnDraw();
                    $.msg( 'unblock', 1000);
                    });
                });
            
            })
        
        return false;
        
        
    });


})

// --> 
</script>
