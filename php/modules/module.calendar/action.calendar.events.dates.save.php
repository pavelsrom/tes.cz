<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */


if(!defined('SECURITY_CMS')) exit;
if(!is_action() ||
    (!$this->is_access('akce','termin-edit',1,1,'content_edit') &&
     !$this->is_access('akce','termin-new',1,0,'content_add'))
     || !$object_access->has_access($this->get_id_page())
    ) return;
    
$idAkce = $this->get_id_page();
$idTerminu = $this->get_id_action();

$nazev = $db->get(TABLE_STRANKY,'nazev',"idStranky=".$idAkce." AND idDomeny=".$domain->getId()." AND idJazyka=".WEB_LANG_ID." AND typ='akce'");

if($nazev == '') return;

$od = get_post('datum_od');
$do = get_post('datum_do');
$cas_od = get_post('cas_od');
$cas_do = get_post('cas_do');

$update_terminy = array();

if(!TestDate($od, false) || !TestDate($do, true)){
    $systemMessage->add_error(ERROR_CHYBNE_DATUM_AKCE);
    }
    else
    {
    if(!TestTwoDate($od, $do)){
        $systemMessage->add_warning(WARNING_CHYBNE_DATUM_AKCE);
        }
			
    $od = GetUniDate($od,$cas_od);
    $do = GetUniDate($do,$cas_do);
    if($od!='') 
        $update_terminy["od"] = $od;
        else    
        $update_terminy["od"] = "null";
    					
    if($do!='')
        $update_terminy["do"] = $do;
        else    
        $update_terminy["do"] = "null";
        
    if(over_terminy($idAkce, $update_terminy["od"], $update_terminy["do"], $idTerminu))
    {
        $systemMessage->add_error(TTERMINY_SE_PREKRYVAJI_S_JINYM_TERMINEM_AKCE);
    }
   }
    

if($systemMessage->error_exists()) return;

       
$update_terminy['idAkce'] = $idAkce;
if($idTerminu == 0)
    $db->insert(TABLE_KALENDAR_TERMINY,$update_terminy);
    else
    $db->update(TABLE_KALENDAR_TERMINY,$update_terminy,"idTerminu=".$idTerminu);

 
$log->add_log('edit-content','kalendar-akci',$idAkce,$nazev);
 
$systemMessage->add_ok(OK_ULOZENO);

if($this->get_action() == 'termin-new')
    Redirect($this->get_link('akce',$idAkce,'termin-new'));
    else
    Redirect($this->get_link('akce',$idAkce,'termin-edit',$idTerminu));

exit;

?>