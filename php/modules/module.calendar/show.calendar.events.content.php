<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */


if(!defined('SECURITY_CMS')) exit;
if(!$this->is_access('akce','edit-content',0,1,'content_view') &&
    !$this->is_access('akce','new',0,0,'content_add')
    ) return;
    
$idPage = $this->get_id_action();
$action = $this->get_action();

if(!$object_access->has_access($idPage))
{
    $systemMessage->add_warning(TNEMATE_OPRAVNENI_MENIT_OBSAH);
    $systemMessage->show_messages();
}

$main_tools = array();

if($idPage > 0) {

			$record1 = $db->Query("SELECT s.*, 
                IF(s.od IS NULL, '', DATE_FORMAT(s.od,'%d.%m.%Y')) AS publikovat_od,
				IF(s.do IS NULL, '', DATE_FORMAT(s.do,'%d.%m.%Y')) AS publikovat_do,
				IF(s.datum IS NULL, '', DATE_FORMAT(s.datum,'%d.%m.%Y')) AS datum
			FROM ".TABLE_STRANKY." AS s
			WHERE idStranky=".$idPage." 
				AND idDomeny=".$domain->getId()."
                AND idJazyka=".WEB_LANG_ID."
                AND typ='akce'
			LIMIT 1");
			
			
			$page = $db->getAssoc($record1);

            if($login_obj->UserPrivilege('content_view'))
                $main_tools[] = array(
                    'ikona' => 'preview', 
                    "nazev" => TNAHLED, 
                    "aktivni" => 0, 
                    "odkaz" => $links->get_url($idPage), 
                    'target' => "blank"
                    ); 

			}
			else
			{
			$page['nazev'] = "";
            $page['misto'] = "";
            $page['idRodice'] = 0;
            $page['datum'] = date("d.m.Y");
            $page['publikovat_od'] = "";
			$page['publikovat_do'] = "";
			$page['nadpis'] = "";
            $page['perex'] = "";
            $page['obrazek'] = "";
			$page['zobrazit'] = 1;
			$page['url'] = "";
			$page['title'] = "";
			$page['description'] = "";
			$page['obsah'] = "";
            $page['idGalerie'] = 0;
            $page['idAnkety'] = 0;
            $page['idFormulare'] = 0;
            $page['idAdresare'] = 0;
            $page['diskuze'] = 0;
			$page['idProfiluPanelu'] = 0;
            $page['autentizace'] = 0;             
            $page['termin_datum_od'] = "";
            $page['termin_datum_do'] = "";
            $page['termin_cas_od'] = "";
            $page['termin_cas_do'] = "";
			}


  
if($idPage > 0)
{
    $main_tools[] = array(
        'ikona' => 'date', 
        "nazev" => TTERMINY_AKCE, 
        "aktivni" => 0, 
        "odkaz" => $this->get_link('akce',$idPage,'terminy'), 
        "title" => TTERMINY_AKCE
        );  
}

$main_tools[] = array(
    'ikona' => 'back', 
    "nazev" => TZPET, 
    "aktivni" => 0, 
    "odkaz" => $this->get_link('akce'), 
    "title" => TZPET_NA_SEZNAM
    ); 
    

$data = $db->query("SELECT idStranky, nazev FROM ".TABLE_STRANKY." WHERE idDomeny=".$domain->getId()." AND idJazyka=".WEB_LANG_ID." AND typ='kalendar-typ'");

$selectbox_typy_akci = array(0 => "-- ".TVYBERTE." --");
while($t = $db->getAssoc($data))
    $selectbox_typy_akci[$t['idStranky']] = $t['nazev'];



echo main_tools($main_tools);
$form = new Form();
$form->allow_upload();
$form->add_section(TZAKLADNI_UDAJE, 'home');
$form->add_text(TNAZEV_AKCE,'titulek',$page['nazev'],0,true,"","nazev");
$form->add_selectbox(TTYP_AKCE,'typ',$page['idRodice'],$selectbox_typy_akci,0,true,"","typ_akce");

$form->add_text(TMISTO_KONANI,'misto',$page['misto'],0,false,"","misto");
$form->add_radiobuttons(TZOBRAZOVAT,'zobrazit',$page['zobrazit'],null,6);

$form->add_section(TTERMINY_AKCE, "date");
if($action == 'new')
{
    $form->add_text(TZACATEK_AKCE_DATUM,'termin_datum_od',$page['termin_datum_od'],0,true,"kalendar","od");
    $form->add_text(TZACATEK_AKCE_CAS,'termin_cas_od',$page['termin_cas_od']);
    $form->add_text(TKONEC_AKCE_DATUM,'termin_datum_do',$page['termin_datum_do'],0,false,"kalendar","do");
    $form->add_text(TKONEC_AKCE_CAS,'termin_cas_do',$page['termin_cas_do']);
}
else
{
    $data_terminy = $db->query("SELECT 
        IF(od IS NOT NULL AND od!='', DATE_FORMAT(od,'%d.%m.%Y %H:%i'), '') AS od,
        IF(do IS NOT NULL AND do!='', DATE_FORMAT(do,'%d.%m.%Y %H:%i'), '') AS do
        FROM ".TABLE_KALENDAR_TERMINY." 
        WHERE idAkce=".$idPage."
        ORDER BY od, idTerminu
        ");
        
    $pocet_terminu = $db->numRows($data_terminy);
    while($t = $db->getAssoc($data_terminy))
    {
        $termin = trim($t['od']);
        
        if($t['do']!='')
            $termin .= " - ".trim($t['do']);
        
        $termin = str_replace("00:00","",$termin);
            
        $terminy[] = $termin;
    }
    
    $editace_terminu = tlacitko($this->get_link('akce',$idPage,'terminy'),TEDITOVAT_NEBO_PRIDAT_TERMINY,"","calendar");
    
    $form->add_code("<tr><th></th><td>".implode("<br />",$terminy).$editace_terminu."</td></tr>");
}

$form->add_section(TOBSAH, "content");
$form->add_editor(TPEREX,'perex',$page['perex'],84,false,"h300 wysiwygEditorMini","wysiwygEditorPerex");
$form->add_editor(TOBSAH,'obsah',$page['obsah'])->allow_wet();
$form->add_file(TNAHRAT_OBRAZEK, 'obrazek', false, 85,"","obrazek_input")->set_disabled($page['obrazek']!='');
        
$image_class = "";
if($page['obrazek']=='' || $page['obrazek']==null)
{
    $img = TNENI_ULOZEN_ZADNY_OBRAZEK;
}
else
{	
    $file = $domain->getDir().USER_DIRNAME_CALENDAR.$page['obrazek'];
				
    if(file_exists($file)) 
    {
        $src = RELATIVE_PATH.$file."?".time(); 
        $img = "<img src='".$src."' alt='".$page['obrazek']."' title='".$page['obrazek']."' /><br /><br />";
        $image_class = "image";
        if($login_obj->UserPrivilege('content_delete'))
                $img .= tlacitko('#',TSMAZAT,"","times","delete photo-delete","delete".$idPage);
                else
                $img .= icon_disabled('delete');
            }
            else 
            {
            $src="";
            $img = TOBRAZEK_NENALEZEN;
            if($login_obj->UserPrivilege('content_delete'))
                $img .= tlacitko('#',TSMAZAT,"","times","delete photo-delete","delete".$idPage);
                else
                $img .= icon_disabled('delete');

                }
}

$form->add_plain_text(TOBRAZEK, $img)->set_class_td($image_class." relative tleft");			

if(MODULE_GALLERY){
    $sel = ShowSelectBoxGallery();
    $sel = array(0 => "-- ".TVYBERTE." --") + $sel;

    if(count($sel) == 1){
        $text_neni = "";
        if($login_obj->UserPrivilege('settings_add'))
            $text_neni .= tlacitko(get_link('gallery',"galerie",0,"new"));
        $form->add_plain_text(TGALERIE, $text_neni);

        }
        else
        $form->add_selectbox(TGALERIE, 'idGalerie', $page['idGalerie'], $sel);
           
    }
    else
    $form->add_hidden('idGalerie', $page['idGalerie']);
  
if(MODULE_INQUIRY){
    $d = $db->Query("SELECT idAnkety AS id, nazev FROM ".TABLE_ANKETY." WHERE idDomeny=".$domain->getId());
    $sel = array(0 => "-- ".TVYBERTE." --");
    $pocet = $db->numRows($d);
            
    while($s = $db->getAssoc($d))
        $sel[$s['id']] = $s['nazev'];
                
    if(count($sel) == 1){
        $text_neni = "";
        if($login_obj->UserPrivilege('settings_add'))
            $text_neni .= tlacitko(get_link('inquiry',"ankety",0,'new'));
        
        $form->add_plain_text(TANKETA, $text_neni);    
        }
        else
        $form->add_selectbox(TANKETA, 'idAnkety', $page['idAnkety'], $sel);
    
    }
    else
    $form->add_hidden('idAnkety', $page['idAnkety']);
        
if(MODULE_DISCUSSION)
    $form->add_radiobuttons(TDISKUZE, 'diskuze', $page['diskuze'], array(1 => TANO, 0=>TNE));
    else
    $form->add_hidden('diskuze', $page['diskuze']);
        

$data_panely = $db->Query("SELECT idProfilu,nazev FROM ".TABLE_PANELY_SABLONY." WHERE idDomeny=".$domain->getId()." AND zobrazit=1 AND idJazyka=".WEB_LANG_ID." ORDER BY vychozi DESC ");
                
$profily = array();
while($profil = $db->getAssoc($data_panely))
    $profily[$profil['idProfilu']] = $profil['nazev'];

$form->add_section(TSABLONA_PANELU, 'panel-setting');
$form->add_selectbox(TSABLONA_PANELU, 'idProfiluPanelu', $page['idProfiluPanelu'], $profily, 135);

show_private_pages_settings($idPage, $page, $form);
            
if($login_obj->UserPrivilege('seo'))
{
    $form->add_section(TSEO_PARAMETRY, 'seo', "", 8);
    $form->add_text(TURL_NAZEV, 'url', $page['url'], 9, $idPage > 0);
    $form->add_text(TTITLE, 'title', $page['title'], 10,false,'','title');
    $form->add_textarea(TDESCRIPTION, 'description', $page['description'], 11);	
}
else
{
    $form->add_hidden('url', $page['url']);
    $form->add_hidden('title', $page['title'], 'title');
    $form->add_hidden('description', $page['description']);	
}
		
$res = (($login_obj->UserPrivilege('content_edit') && $idPage>0) || ($login_obj->UserPrivilege('content_edit') && $idPage==0)) && $object_access->has_access($idPage);

$form->add_next_actions();
$form->add_submit()->set_disabled(!$res);
    
echo $form->get_html_code();
        
?>


<script type="text/javascript">
<!-- 
$(function(){
    
    $("a.delete").click(function(){
        var id = $(this).attr('id').substring(6);
        
        var text = "<?php echo TOPRAVDU_SMAZAT_TENTO_OBRAZEK;?>";
        var td = $(this).parents("td.image").first();
        
        custom_confirm(text, function(){
            $.msg();
            td.addClass("deleted");
            $.post("<?php echo AJAX_GATEWAY;?>calendar.events.image.delete",{id: id},function(data){
                $.msg( 'setClass', 'ok_message' );
                $.msg( 'replace', '<?php echo OK_SMAZANO;?>' );
                $.msg( 'unblock', 1000);
                td.html("<?php echo TNENI_ULOZEN_ZADNY_OBRAZEK;?>");
                $("#obrazek_input").removeAttr('disabled');
                td.removeAttr("class");
                });
            
            })
            
        return false;
        
    })
    

    var idn = <?php echo $idPage;?>;
    
    $('#nazev').change(function(){
        if(idn > 0) return;
        var n = $(this).val();
        $('#title').val(n);
               
        });
    

    
    
})
// --> 
</script>