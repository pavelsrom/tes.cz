<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */


if(!defined('SECURITY_CMS')) exit;
if(!$this->is_access('akce-typy')) return;

$main_tools = array();
        
if($login_obj->UserPrivilege('content_add'))
    $main_tools[] = array(
        'ikona' => 'new', 
        "nazev" => TPRIDAT_TYP_AKCE, 
        "aktivni" => 0, 
        "odkaz" => $this->get_link('akce-typy',0,'new')
        );
            
$form = new FormFilter();
$form->add_text(THLEDAT, 'vyraz', "", 0, false, "", "vyraz");
echo $form->get_html_code(main_tools($main_tools));
        
$table = new Table('tList');
$table->tr_head()
    ->add(TID,'w25')
    ->add(TNAZEV)
    ->add(TBARVA, 'w110')
    ->add(TPOUZITO, "w25")
    ->add(TPRIVATNI, "w25")
    ->add(TAKCE,'w110');
    
echo $table->get_html();
        
       
?>
<script type="text/javascript">
<!--

$(function(){
var oTable = $("#tList").dataTable({
    "sAjaxSource": "<?php echo AJAX_GATEWAY;?>events.types.list",
    "bSort": false,
    "aoColumns": [
        {"sClass": "sl1"},{"sClass": "tleft sl2"},{"sClass": "sl3", "bSortable": false},{"sClass": "sl4"},{"sClass": "sl5", "bSortable": false},{"sClass": "akce", "bSortable": false}
        ],
    "fnServerData": function ( sSource, aoData, fnCallback ) {
            aoData.push({ "name": "vyraz", "value": $("#vyraz").val() });
            $.getJSON( sSource, aoData, function (json) { fnCallback(json); } );
            }
 
})

$("input#vyraz").bind('keyup change', function() {
        oTable.fnDraw(); 
        });

//prida k bunce th span
$(".dataTable th").each(function(){
    $(this).html("<span>" + $(this).html() + "</span>");
})


$(document).on("click","a.delete", function(){
        var id = $(this).attr('id').substring(6);
        var tr = $(this).parents("tr").first();
        var c = $.trim($(this).parents("tr").find("td").eq(1).text()); 
        
        var text = "<?php echo TOPRAVDU_SMAZAT;?>";

        custom_confirm(text.replace("%s",c) + '?', function(){
            $.msg();
            tr.addClass("deleted");
            $.post("<?php echo AJAX_GATEWAY;?>events.types.delete",{id: id},function(data){
                tr.fadeOut(function(){
                    $.msg( 'setClass', 'ok_message' );
                    $.msg( 'replace', '<?php echo OK_SMAZANO;?>' );
                    oTable.fnDraw();
                    $.msg( 'unblock', 1000);
                    });
                });
            
            })
        
        return false;
        
        
    });
    
    

$(document).on("mouseover","a.up",function(){
    var tr = $(this).parents("tr").first();

    tr.addClass("exchange_from");
    tr.removeClass("light");
    tr_prev = tr.prevAll().first();
    tr_prev.addClass("exchange_to");
    tr_prev.find("td").eq(1).addClass("exchange_down");
    tr.find("td").eq(1).addClass("exchange_up");
})

$(document).on("mouseout","a.up, a.down", function(){
    $("table#tList tr").removeClass("exchange_to");
    $("table#tList tr").removeClass("exchange_from");
    $("table#tList tr td").removeClass("exchange_down");
    $("table#tList tr td").removeClass("exchange_up");
})


$(document).on("mouseover","a.down",function(){
    var tr = $(this).parents("tr").first();
    tr.addClass("exchange_to");
    tr.removeClass("light");
    tr_next = tr.nextAll().first();
    tr_next.addClass("exchange_from");
    tr_next.find("td").eq(1).addClass("exchange_up");
    tr.find("td").eq(1).addClass("exchange_down");
})

$(document).on("click","a.up", function(){
    var id = $(this).attr('id').substring(2);
    $.post("<?php echo AJAX_GATEWAY;?>events.types.move.up",{id: id},function(data){
        oTable.fnDraw();
    })   
    
    return false;
})
   
$(document).on("click","a.down", function(){
    var id = $(this).attr('id').substring(4);
    $.post("<?php echo AJAX_GATEWAY;?>events.types.move.down",{id: id},function(data){
        oTable.fnDraw();
    })
    
    return false;   
})


})

// --> 
</script>
