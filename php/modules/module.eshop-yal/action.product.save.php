<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined('SECURITY_CMS')) exit;
if(!is_action() || 
    (!$this->is_access('kategorie','edit-content',1,1,'content_edit') && 
    !$this->is_access('kategorie','new',1,0,'content_add'))
     || !$object_access->has_access($this->get_id_action())
    ) return;


$idBloku = $this->get_id_page();
$idStranky = $this->get_id_action();		
$titulek = $nadpis = get_post('nazev');
$cena = get_post('cena');
$carousel_popis = get_post('carousel_popis');

$dph = get_post('dph');
$zobrazit_cenu = get_post('zobrazit_cenu');

$file = get_post('obrazek');
$url = get_post('url');
$title = get_post('title');


if($idStranky==0 && $title=='')
    $title = $titulek;
        
              
            
$desc = get_post('description');
$perex = get_post('perex');
$rubrika = get_int_post('rubrika');
$znamka = get_int_post('znamka');
$zobrazit = get_post('zobrazit');
$barva = get_post('barva');

$idProfiluPanelu = get_int_post('idProfiluPanelu');
$idGalerie = get_int_post('idGalerie');    
$idEditboxu = get_int_post('idEditboxu');
$idAnkety = get_int_post('idAnkety');
$diskuze = get_int_post('diskuze');
$eshop_url = get_post('eshop_url');
$obsah = get_post('obsah',"");
$popis1 = get_post('popis1',"");
$popis2 = get_post('popis2',"");
$popis3 = get_post('popis3',"");
$popis4 = get_post('popis4',"");
$popis5 = get_post('popis5',"");
$popis6 = get_post('popis6',"");
$popis7 = get_post('popis7',"");
$autentizace = get_int_post("autentizace");
$znacka = get_int_post("znamka");
$procenta1 = get_int_post("procenta1");
$procenta2 = get_int_post("procenta2");
$procenta3 = get_int_post("procenta3");

$rok = get_int_post("rok");
$mesic = get_int_post("mesic");
$pocet_stran = get_int_post("pocet_stran");
$autor_knihy = get_post('autor_knihy');

$procenta1text = get_post('procenta1text',"");
$procenta2text = get_post('procenta2text',"");
$procenta3text = get_post('procenta3text',"");

$doporuceny = get_int_post("doporuceny");

$odrazky = get_post('odrazky',"");
$odrazka_vice = get_int_post("odrazka_vice");
            
$od = get_post('od');
$do = get_post('do');
$datum = get_post('datum');
$datum = GetUniDate($datum);

/*
$url = $idStranky==0 && $url=='' ? ModifyUrl($titulek) : ModifyUrl($url);
$url_zal = get_url_nazev($idStranky, $url);
if($url!=$url_zal)
    $systemMessage->add_warning(WARNING_URLNAZEV_MODIFIKOVAN." ".$url_zal);
$url = $url_zal;

if($url == "")
    $systemMessage->add_error(ERROR_NEPLATNY_URL_NAZEV);


$update = array();

if($datum!='')
    $update["datum"] = $datum;
	else
	$update["datum"] = "null";			
		
if(!TestSentenceCZ($titulek, 200, false))
    $systemMessage->add_error(ERROR_NEPLATNY_NAZEV);
    
if($rubrika <= 0)
    $systemMessage->add_error(ERROR_KATEGORIE_PRODUKTU_MUSI_BYT_ZADANA);
			
if(!TestDate($od, true) || !TestDate($do, true)){
    $systemMessage->add_error(ERROR_CHYBNE_DATUM);
    }
    else
    {
    if(!TestTwoDate($od, $do)){
        $systemMessage->add_warning(WARNING_CHYBNE_DATUM);
        }
			
    $od = GetUniDate($od);
    $do = GetUniDate($do);
    if($od!='') 
        $update["od"] = $od;
        else    
        $update["od"] = "null";
    					
    if($do!='')
        $update["do"] = $do;
        else    
        $update["do"] = "null";
    }

         	
if($systemMessage->error_exists()) return;


$update = $update + array(
    "idDomeny"      => $domain->getId(),
    "typ"           => "produkt",
    "nazev"         => $titulek,
    "idRodice"      => $rubrika,
    "nazevProMenu"  => $titulek,
    "nadpis"        => $nadpis,
    "perex"         => $perex,
    "perexBezHtml"  => strip_tags($perex),
    "zobrazit"      => $zobrazit,
    "title"         => $title,
    "description"   => $desc,
    "url"           => $url,
    "archivovat"    => 0,
    "idProfiluPanelu"=>$idProfiluPanelu,
    "idGalerie"     => $idGalerie,
    "idAnkety"      => $idAnkety,
    "idEditboxu"    => $idEditboxu,
    "diskuze"       => $diskuze,
    "obsah"         => $obsah,
    "obsahBezHtml"  => strip_tags($obsah),
    "autor"         => $login_obj->getName(),
    "idJazyka"      => WEB_LANG_ID,
    "cena"          => $cena,
    "zobrazit_cenu" => $zobrazit_cenu,
    "dph"           => $dph,
    "autentizace"   => $autentizace,
    "popis1"        => $popis1,
    "popis1BezHtml" => strip_tags($popis1),
    "popis2"        => $popis2,
    "popis2BezHtml" => strip_tags($popis2),
    "popis3"        => $popis3,
    "popis3BezHtml" => strip_tags($popis3),
    "popis4"        => $popis4,
    "popis4BezHtml" => strip_tags($popis4),
    "popis5"        => $popis5,
    "popis5BezHtml" => strip_tags($popis5),
    "popis6"        => $popis6,
    "popis6BezHtml" => strip_tags($popis6),
    "popis7"        => $popis7,
    "popis7BezHtml" => strip_tags($popis7),
    'carousel_popis'=> $carousel_popis,
    //"znamka"        => $znamka,
    "odrazka_vice"  => $odrazka_vice,
    "odrazky"       => $odrazky,
    "eshop_url"     => $eshop_url,
    "doporuceny"    => $doporuceny,
    "znamka"        => $znacka,
    "barva"         => $barva,
    "rok"           => $rok,
    "mesic"         => $mesic,
    "pocet_stran"   => $pocet_stran,
    "autor_knihy"   => $autor_knihy
    );
	
    
if($idStranky == 0){

    $update['idVytvoril'] = $login_obj->getId();
    $update['jmenoVytvoril'] = $login_obj->getName();
    $update['datumVytvoril'] = "now";
    
    $update['idAktualizoval'] = $login_obj->getId();
    $update['jmenoAktualizoval'] = $login_obj->getName();
    $update['datumAktualizoval'] = "now";
    
    $update['priorita'] = $db->get(TABLE_STRANKY, "MAX(priorita) + 1","idRodice = ".$rubrika);
    
     
    $db->insert(TABLE_STRANKY,$update);
    $idStranky = $db->lastId();
    $object_access->add_allowed_object($idStranky);
    }
    else
    {
    $update['idAktualizoval'] = $login_obj->getId();
    $update['jmenoAktualizoval'] = $login_obj->getName();
    $update['datumAktualizoval'] = "now";
    
    $db->update(TABLE_STRANKY,$update,"idStranky=".$idStranky." AND idDomeny=".$domain->getId());

	}
    */
 
//nahravani hlavniho obrazku
if(isset($_FILES['hlavni_obrazek']) && $_FILES['hlavni_obrazek']['error']!=4)
{

    $articles_path_maxi = $domain->getDir().USER_DIRNAME_HLAVNI_OBRAZEK;
                        
    include_once('php/classes/class.upload.php');
                        
    $uploader = new upload($_FILES['hlavni_obrazek'], 'cs_CS');
    $uploader->file_new_name_body = $idStranky;
    $uploader->file_overwrite = true;
    $uploader->image_resize = true;
    $uploader->image_x = intval(1280);
    $uploader->image_y = intval(498);
    $uploader->image_ratio = true;
    $uploader->image_convert = 'jpg';
    $uploader->process($articles_path_maxi); 
    $uploader->clean();
    $file = $idStranky.".jpg";
    
    $db->update(TABLE_STRANKY,array("hlavni_obrazek" => $file),"idDomeny=".$domain->getId()." AND idStranky=".$idStranky);
    
}

//nahravani hlavniho obrazku
if(isset($_FILES['soubor']) && $_FILES['soubor']['error']!=4)
{

    $pdf_path = $domain->getDir().USER_DIRNAME_PDF;
                        
    include_once('php/classes/class.upload.php');
                        
    $uploader = new upload($_FILES['soubor'], 'cs_CS');
    /*
    $uploader->file_new_name_body = $idStranky;
    */
    $uploader->file_overwrite = false;
    /*
    $uploader->image_resize = true;
    $uploader->image_x = intval(1280);
    $uploader->image_y = intval(498);
    $uploader->image_ratio = true;
    $uploader->image_convert = 'jpg';
    */
    $uploader->process($pdf_path); 
    $uploader->clean();
    
    $db->update(TABLE_STRANKY,array("soubor" => $uploader->file_dst_name),"idDomeny=".$domain->getId()." AND idStranky=".$idStranky);
    
}

    
    			
//ulozeni popisku fotek
$popisky = get_array_post("fotka");
$i=0;
foreach($popisky AS $idFotky => $p)
{
    $update = array(
        "popis" => $p,
        "priorita" => $i
        );
    
    $db->update(TABLE_STRANKY_FOTKY,$update,"id = ".$idFotky." AND idStranky=".$idStranky);
    $i++;
}







$skupiny = get_array_post('skupiny');
$db->delete(TABLE_STRANKY_SKUPINY,"WHERE idStranky=".$idStranky);
if(count($skupiny)>0)
{
    foreach($skupiny AS $s)
    {
        $insert = array("idStranky" => $idStranky, "idSkupiny" => $s);
        $db->insert(TABLE_STRANKY_SKUPINY,$insert);
    }
}


if(isset($_FILES['obrazek']) && $_FILES['obrazek']['error']!=4)
{

    $articles_path_mini = $domain->getDir().USER_DIRNAME_ESHOP_MINI;
    $articles_path_stredni = $domain->getDir().USER_DIRNAME_ESHOP_STREDNI;
    $articles_path_maxi = $domain->getDir().USER_DIRNAME_ESHOP_MAXI;
                        
    include_once('php/classes/class.upload.php');
                        
    $uploader = new upload($_FILES['obrazek'], 'cs_CS');
    $uploader->file_new_name_body = $idStranky;
    $uploader->file_overwrite = true;
    $uploader->image_resize = true;
    $uploader->image_x = intval(OBRAZEK_PRODUKTU_MAXI_X);
    $uploader->image_y = intval(OBRAZEK_PRODUKTU_MAXI_Y);
    $uploader->image_ratio = true;
    //$uploader->image_convert = 'jpg';
    $uploader->process($articles_path_maxi);
    
    $uploader = new upload($_FILES['obrazek'], 'cs_CS');
    $uploader->file_new_name_body = $idStranky;
    $uploader->file_overwrite = true;
    $uploader->image_resize = true;
    $uploader->image_ratio = true;
    //$uploader->image_convert = 'jpg';
    $uploader->image_x = intval(OBRAZEK_PRODUKTU_MINI_X);
    $uploader->image_y = intval(OBRAZEK_PRODUKTU_MINI_Y);
    $uploader->process($articles_path_mini);
    
    $uploader = new upload($_FILES['obrazek'], 'cs_CS');
    $uploader->file_new_name_body = $idStranky;
    $uploader->file_overwrite = true;
    $uploader->image_resize = true;
    $uploader->image_ratio = true;
    //$uploader->image_convert = 'jpg';
    $uploader->image_x = intval(OBRAZEK_PRODUKTU_STREDNI_X);
    $uploader->image_y = intval(OBRAZEK_PRODUKTU_STREDNI_Y);
    $uploader->process($articles_path_stredni);
    
    //print_r($_FILES['obrazek']);
    //exit;
    $ext_data = explode(".", $_FILES['obrazek']['name']);
    $ext = end($ext_data); 
    
    
    $uploader->clean();
    $file = $idStranky.".".$ext;
    
    $db->update(TABLE_STRANKY,array("obrazek" => $file),"idDomeny=".$domain->getId()." AND idStranky=".$idStranky." AND typ='produkt'");

}


$souvisejici = get_array_post('souvisejici');
    
$db->delete(TABLE_SOUVISEJICI_CLANKY,"WHERE idClanku=".$idStranky);

if(count($souvisejici)>0)
{
    foreach($souvisejici AS $s)
    {
        $insert_souvisejici = array("idClanku" => $idStranky, "idSouvisejicihoClanku" => $s);
        $db->insert(TABLE_SOUVISEJICI_CLANKY,$insert_souvisejici);
    }
}


//vytvoreni zalohy obsahu
if(MODULE_CONTENT_BACKUP)
{
    $content_backup = new ContentBackup($domain->getId(),$idStranky,$login_obj->getId());
    $content_backup->AddStore($obsah,$perex,$popis1, $popis2, $popis3, $popis4, $popis5);	
}


$chybne_odkazy_indik = false;

if(MODULE_LINK_CHECKER)
{
    $chybne_odkazy = new LinksChecker($domain->getId(), $idStranky, "stranka", $obsah, false, KONTROLOVAT_ODKAZY_PRI_UKLADANI);
    $chybne_odkazy->set_base_url( $domain->getUrl() );
    $chybne_odkazy->process();
    $pocet_chybnych_obrazku = $chybne_odkazy->get_sum_error_images();
    $pocet_chybnych_odkazu = $chybne_odkazy->get_sum_error_links();
    
    if($pocet_chybnych_obrazku > 0)
        $systemMessage->add_warning(TPOCET_CHYBNYCH_OBRAZKU_NA_STRANCE.": ".$pocet_chybnych_obrazku);
        
    if($pocet_chybnych_odkazu > 0)
        $systemMessage->add_warning(TPOCET_CHYBNYCH_ODKAZU_NA_STRANCE.": ".$pocet_chybnych_odkazu);
    
    if(!$chybne_odkazy_indik && ($pocet_chybnych_obrazku > 0 || $pocet_chybnych_odkazu > 0)) 
        $chybne_odkazy_indik = true;


    $chybne_odkazy = new LinksChecker($domain->getId(), $idStranky, "stranka", $perex, true, KONTROLOVAT_ODKAZY_PRI_UKLADANI);
    $chybne_odkazy->set_base_url( $domain->getUrl() );
    $chybne_odkazy->process();
    $pocet_chybnych_obrazku = $chybne_odkazy->get_sum_error_images();
    $pocet_chybnych_odkazu = $chybne_odkazy->get_sum_error_links();
    
    if($pocet_chybnych_obrazku > 0)
        $systemMessage->add_warning(TPOCET_CHYBNYCH_OBRAZKU_V_PEREXU.": ".$pocet_chybnych_obrazku);
        
    if($pocet_chybnych_odkazu > 0)
        $systemMessage->add_warning(TPOCET_CHYBNYCH_ODKAZU_V_PEREXU.": ".$pocet_chybnych_odkazu);
    
    if(!$chybne_odkazy_indik && ($pocet_chybnych_obrazku > 0 || $pocet_chybnych_odkazu > 0)) 
        $chybne_odkazy_indik = true;
    
}


if($chybne_odkazy_indik)
    $systemMessage->add_warning(TODKAZY_ZKONTROLUJTE_A_OPRAVTE);


$log->add_log($this->get_url('id_action') == 0 ? 'create':'edit-content','produkt',$idStranky,$titulek);

$systemMessage->add_ok(OK_ULOZENO);

set_next_action_code();

$dalsi_akce = get_next_action_code();
if($dalsi_akce == 0) //zpet na vypis
    $url = $this->get_link('kategorie',$idBloku);
elseif($dalsi_akce == 1) //zpet na editaci
    $url = $this->get_link('kategorie',$idBloku,'edit-content',$idStranky);
elseif($dalsi_akce == 2) //vytvorit dalsi
    $url = $this->get_link('kategorie',$idBloku,'new');
    
Redirect($url);

?>