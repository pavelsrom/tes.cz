<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */


if(!defined('SECURITY_CMS')) exit;
if(!$this->is_access('objednavky','',0,0)) return;

$main_tools = array();
        
/*
if($login_obj->UserPrivilege('settings_add'))
    $main_tools[] = array(
        'ikona' => 'new', 
        "nazev" => TPRIDAT_KATEGORII_PRODUKTU, 
        "aktivni" => 0, 
        "odkaz" => get_link('pages','stranky',0,"new",0,"produkty"));
*/

$data = $db->Query("SELECT id, nazev FROM ".TABLE_ESHOP_OBJEDNAVKY_STAVY." ORDER BY id");
$stavy = array(0=> "-- ".TVSECHNO." --");
while($sk = $db->getObject($data))
    $stavy[$sk->id] = $sk->nazev;

	
$form = new FormFilter();
$form->set_class('filter2');
$form->add_text(THLEDAT, 'vyraz', "", 0, false, "", "vyraz");
$form->add_code("<tr><th>".TDATUM."</th><td><input type='text' name='datum_od' id='datum_od' class='input  kalendar' style='width: 92px;' /> &ndash; <input type='text' name='datum_do' id='datum_do' class='input kalendar' style='width: 92px;'/></td></tr>");
$form->add_selectbox(TSTAV,'stav',0,$stavy,0,false,"","stav");
echo $form->get_html_code(main_tools($main_tools));


$table = new Table("tList");
$table->tr_head()
    ->add(TCISLO_FAKTURY, 'w110')
    //->add(TID_PLATBY_GOPAY, 'w110')
    ->add(TJMENO_A_PRIJMENI)
    ->add(TCENA_S_DPH, 'w110')
    ->add(TVYTVORENO)
    ->add(TZAPLACENO)
    ->add(TPOZNAMKA)
    ->add(TSTAV, 'w25')
    ->add(TAKCE, 'w110')
    ;
    
echo $table->get_html();

?>
<script type="text/javascript">
<!--

$(function(){
var oTable = $("#tList").dataTable({
    "sAjaxSource": "<?php echo AJAX_GATEWAY;?>order.list",
    "aaSorting": [[ 0, "desc" ]],
    "aoColumns": [
        {"sClass": "sl1"},{"sClass": "tleft sl2", "bSortable": false},{"sClass": "tright sl2"},{"sClass": "sl3"},{"sClass": "sl3"},{"sClass": "sl3", "bSortable": false},{"sClass": "sl4", "bSortable": false},{"sClass": "akce", "bSortable": false}
        ],
    "fnServerData": function ( sSource, aoData, fnCallback ) {
            aoData.push({ "name": "vyraz", "value": $("#vyraz").val() });
            aoData.push({ "name": "datum_od", "value": $("#datum_od").val() });
            aoData.push({ "name": "datum_do", "value": $("#datum_do").val() });
            aoData.push({ "name": "stav", "value": $("#stav option:selected").val() });
            $.getJSON( sSource, aoData, function (json) { fnCallback(json); } );
            }
 
})

$("input#vyraz, select#stav, input#datum_od, input#datum_do").bind('keyup change', function() {
        oTable.fnDraw(); 
        });

//prida k bunce th span
$(".dataTable th").each(function(){
    $(this).html("<span>" + $(this).html() + "</span>");
})

})

// -->
</script>
