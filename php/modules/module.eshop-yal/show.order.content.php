<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined('SECURITY_CMS')) exit;
if(!$this->is_access('objednavky','edit-content',0,1,'content_view') &&
    !$this->is_access('objednavky','new',0,0,'content_add')
    ) return;

$idClanku = $this->get_id_action(); 

$main_tools = array();

if($idClanku > 0) {
    $record1 = $db->Query("SELECT o.*, COUNT(op.id) AS pocet, s.nazev AS stav, u.hash,
                    DATE_FORMAT(datum,'%d.%m.%Y %H:%i') AS datum,
                    IF(zaplaceno IS NULL,' - ', DATE_FORMAT(zaplaceno,'%d.%m.%Y %H:%i')) AS zaplaceno
				FROM ".TABLE_ESHOP_OBJEDNAVKY." AS o
                LEFT JOIN ".TABLE_ESHOP_OBJEDNAVKY_POLOZKY." AS op ON o.id = op.id_objednavka
                LEFT JOIN ".TABLE_ESHOP_OBJEDNAVKY_STAVY." AS s ON s.id = o.id_stav
                LEFT JOIN ".TABLE_UZIVATELE." AS u ON u.idUzivatele = o.id_uzivatel
                WHERE o.id = ".$idClanku."
				GROUP BY o.id
			LIMIT 1");
			
    $page = $db->getAssoc($record1);
          
           

    }
    else
    {
    
    }

$main_tools[] = array(
    'ikona' => 'back', 
    "nazev" => TZPET, 
    "aktivni" => 0, 
    "odkaz" => $this->get_link('objednavky'), 
    "title" => TZPET_NA_SEZNAM
    );

echo main_tools($main_tools);

$data = $db->Query("SELECT id, nazev FROM ".TABLE_ESHOP_OBJEDNAVKY_STAVY." ORDER BY id");
$stavy = array(0=> "-- ".TVSECHNO." --");
while($sk = $db->getObject($data))
    $stavy[$sk->id] = $sk->nazev;


$form = new Form();
$form->add_section(TZAKLADNI_UDAJE, "home");

$form->add_plain_text(TID, $page['id']);
$form->add_plain_text(TCISLO_FAKTURY, $page['cislo_faktury']);
$form->add_plain_text(TJMENO_A_PRIJMENI, trim($page['jmeno']." ".$page['prijmeni']));
$form->add_plain_text(TTELEFON, $page['telefon']);
$form->add_plain_text(TEMAIL, $page['email']);
$form->add_selectbox(TSTAV,'stav',$page['id_stav'],$stavy);

$form->add_section(TFAKTURACNI_ADRESA, "adresa");
$form->add_plain_text(TFAKTURACNI_ADRESA, trim($page['ulice']." ".$page['cislo'].", ".$page['psc']." ".$page['mesto']));

$form->add_section(TADRESA_DODACI, "adresa_dod");
$form->add_plain_text(TDODACI_ADRESA, trim($page['ulice_dod']." ".$page['cislo_dod'].", ".$page['psc_dod']." ".$page['mesto_dod']));

$form->add_section(TFIREMNI_UDAJE, "firma");
$form->add_plain_text(TFIRMA, $page['firma']);
$form->add_plain_text(TICO, $page['ic']);
$form->add_plain_text(TDIC, $page['dic']);



$form->add_section(TINFORMACE_O_OBJEDNAVCE, "objednavka");

//$form->add_plain_text(TID_PLATBY_GOPAY, $page['id_platby']);
$form->add_plain_text(TZBOZI_S_DPH, price($page['zbozi_s_dph']));
$form->add_plain_text(TDOPRAVA, $page['doprava']);

if($page['id_doprava'] == 1)
    $form->add_plain_text(TPRODEJNA, $page['prodejna']);

$form->add_plain_text(TDOPRAVA_S_DPH, price($page['doprava_s_dph']));
$form->add_plain_text(TPLATBA, $page['platba']);
$form->add_plain_text(TPLATBA_S_DPH, price($page['platba_s_dph']));
$form->add_plain_text(TBALNE_S_DPH, price($page['balne_s_dph']));
$form->add_plain_text(TCELKEM_S_DPH, price($page['celkem_s_dph']));
$form->add_plain_text(TVYTVORENO, $page['datum']);
$form->add_plain_text(TZAPLACENO, $page['zaplaceno']);
$form->add_plain_text(TPOZNAMKA, $page['poznamka']);

$form->add_section(TPOLOZKY_OBJEDNAVKY, "home");

$data = $db->query("SELECT * FROM ".TABLE_ESHOP_OBJEDNAVKY_POLOZKY." WHERE id_objednavka=".$idClanku);


$form->add_code('<tr><th class="tleft">'.TNAZEV.'</th><th class="tright">'.TPOCET.'</th><th class="tright">'.TCENA_ZA_KS.'</th><th class="tright">'.TCELKEM_S_DPH.'</th></tr>');
$id_nasledujici_platby = 0;
$datum_nasledujici_platby = "";
while($p = $db->getObject($data))
{
    $form->add_code('<tr>
        <td>'.$p->nazev.'</td>
        <td class="tright">'.$p->pocet.'</td>
        <td class="tright">'.price($p->cena_s_dph).'</td>
        <td class="tright">'.price($p->cena_s_dph * $p->pocet).'</td>
        </tr>');
}


		
$res = (($login_obj->UserPrivilege('content_edit') && $idClanku>0) || ($login_obj->UserPrivilege('content_add') && $idClanku==0));

//$form->add_next_actions();
$form->add_submit()->set_disabled(!$res);
    
echo $form->get_html_code();

?>
