<?php

if(!defined('SECURITY_CMS')) exit;
if(!is_action('btnImport') || !$this->is_access('kategorie'))
     return;


$import_z_ftp = is_request("ftp");

if (empty($_FILES) && !$import_z_ftp) 
    $systemMessage->add_error(TNENI_NAHRAN_ZADNY_SOUBOR);

$type = $_FILES['soubor']['type'];

    
if(!stristr($type,'xml') && !$import_z_ftp)
{
    $systemMessage->add_error($_FILES['soubor']['name'].": ".TNELZE_NAHRAT." - ".ERROR_NEPODPOROVANY_FORMAT_SOUBORU); 

}

if($systemMessage->error_exists())
    return;


$db->optimize(TABLE_STRANKY);


//nacteni zdroje z ftp
if($import_z_ftp)
{
    //$xml = file_get_contents('ftp://'.FTP_LOGIN.':'.FTP_PASSWORD.'@'.FTP_HOST.":".FTP_PORT."/produkty.xml");
    $xml = ftp_get_file("/produkty.xml");
    //dd($xml);

    /*
    $conn = ftp_connect(FTP_HOST, FTP_PORT);
    $login_result = ftp_login($conn, FTP_LOGIN, FTP_PASSWORD);
    //$dir = ftp_nlist($conn, ".");
    //

    $h = fopen('php://temp', 'r+');

    ftp_fget($conn, $h, '/produkty.xml', FTP_BINARY, 0);
    $fstats = fstat($h);
    fseek($h, 0);
    $xml = fread($h, $fstats['size']); 
    fclose($h);
    ftp_close($conn);
    dd($xml);
    */

}
else
{
    $xmlSource = $_FILES['soubor']['tmp_name'];
    $xml = file_get_contents($xmlSource);
}

if(!$xml)
{
    $systemMessage->add_error("Chyba formátu xml souboru, nebo problém s otevřením souboru přes ftp"); 
}

//parsovani xml
include("php/classes/class.import.products.php");
$export = new ImportProducts();
$export->load($xml);

//ulozeni hodnot z xml
$export->save();

//zapsani do logu
$complete_log = $export->get_log_list(true);

$db->insert(TABLE_LOG_ERROR, array(
    "datum" => "now",
    "modul" => "cron",
    "get_param" => $import_z_ftp ? "FTP" : print_r($_FILES['soubor'], true),
    "popis" => $complete_log
));

//dd($export->get_log_list());

$systemMessage->set_messages( $export->get_log_list());

$url = $this->get_link('kategorie');
    
Redirect($url);