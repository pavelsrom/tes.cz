<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined('SECURITY_CMS')) exit;
if(!$this->is_access('kategorie','edit-content',1,1,'content_view') &&
    !$this->is_access('kategorie','new',1,0,'content_add')
    ) return;

$idClanku = $this->get_id_action(); 
$idPage = $this->get_id_page();

if(!$object_access->has_access($idClanku))
{
    $systemMessage->add_warning(TNEMATE_OPRAVNENI_MENIT_OBSAH);
    $systemMessage->show_messages();
}

$main_tools = array();

if($idClanku > 0) {
    $record1 = $db->Query("SELECT s.*, j.jazyk,
                IF(s.od IS NULL, '', DATE_FORMAT(s.od,'%d.%m.%Y')) AS publikovat_od,
				IF(s.do IS NULL, '', DATE_FORMAT(s.do,'%d.%m.%Y')) AS publikovat_do,
				IF(s.datum IS NULL, '', DATE_FORMAT(s.datum,'%d.%m.%Y')) AS datum
				
			FROM ".TABLE_STRANKY." AS s
            LEFT JOIN ".TABLE_JAZYKY." AS j ON s.idJazyka = j.idJazyka 
			WHERE idStranky=".$idClanku."
                AND s.typ='produkt' 
				AND idDomeny=".$domain->getId()."
                AND s.idJazyka=".WEB_LANG_ID."
			LIMIT 1");
			
    $page = $db->getAssoc($record1);
          
    if($login_obj->UserPrivilege('content_view'))
        $main_tools[] = array(
            'ikona' => 'preview', 
            "nazev" => TNAHLED, 
            "aktivni" => 0, 
            "odkaz" => UrlPage($page['url'], $idClanku,$page['jazyk']), 
            "target" => "blank"
            ); 
            
            
    $page['publikovat_od'] = $page['publikovat_od'] == '00.00.0000' ? "" : $page['publikovat_od'];
    $page['publikovat_do'] = $page['publikovat_do'] == '00.00.0000' ? "" : $page['publikovat_do'];
    $page['datum'] = $page['datum'] == '00.00.0000' ? "" : $page['datum'];
    }
    else
    {
    $page['nazev'] = "";
    $page['barva'] = 2; //sedá
    $page['datum'] = date("d.m.Y");
    $page['publikovat_od'] = "";
    $page['publikovat_do'] = "";
    $page['nadpis'] = "";
    $page['perex'] = "";
    $page['obrazek'] = "";
    $page['zobrazit'] = 1;
    $page['archivovat'] = 0;
    $page['url'] = "";
    $page['title'] = "";
    $page['description'] = "";
    $page['obsah'] = "";
    $page['popis1'] = "";
    $page['popis2'] = "";
    $page['popis3'] = "";
    $page['popis4'] = "";
    $page['popis5'] = "";
    $page['popis6'] = "";
    $page['popis7'] = "";
    $page['idGalerie'] = 0;
    $page['idAnkety'] = 0;
    $page['idFormulare'] = 0;
    $page['diskuze'] = 0;
    $page['autentizace'] = 0;
    $page['idProfiluPanelu'] = 0;
    $page['stav'] = 'rozpracovano';
    $page['poznamka'] = '';
    $page['odrazky'] = '';
    $page['odrazka_vice'] = 1;
    $page['zobrazit_cenu'] = 0;
    $page['cena'] = 99;
    $page['dph'] = 0;   
    $page['carousel_popis'] = '';
    $page['eshop_url'] = '';
    $page['procenta1'] = 0;
    $page['procenta2'] = 0;
    $page['procenta3'] = 0;
    $page['procenta1text'] = '';
    $page['procenta2text'] = 'Zeleniny';
    $page['procenta3text'] = 'Obilovin';
    $page['doporuceny'] = 0;
    $page['znamka'] = 0;
    $page['rok'] = date("Y");
    $page['mesic'] = date("m");
    $page['pocet_stran'] = 0;
    $page['autor_knihy'] = $login_obj->getName();
    $page['soubor'] = '';
    }

$main_tools[] = array(
    'ikona' => 'back', 
    "nazev" => TZPET, 
    "aktivni" => 0, 
    "odkaz" => $this->get_link('kategorie',$idPage), 
    "title" => TZPET_NA_SEZNAM_KATEGORII
    );

echo main_tools($main_tools);


$data_rubriky = $db->query("SELECT idStranky, nazev FROM ".TABLE_STRANKY." WHERE typ='produkty' AND idDomeny=".$domain->getId()." ORDER BY nazev");
$select_rubriky = array(0 => "-- ".TVYBERTE." --");
while($r = $db->getAssoc($data_rubriky))
    $select_rubriky[$r['idStranky']] = $r['nazev']; 
      
$form = new Form();
$form->allow_upload();
$form->add_section(TZAKLADNI_UDAJE, "home");
$form->add_plain_text(TNAZEV, $page['nazev']);
$form->add_plain_text(TKATEGORIE, $select_rubriky[$page['idRodice']]);

           
$form->add_plain_text("MO - cena s dph", $page['cena_s_dph']);
$form->add_plain_text("MO - cena bez dph", $page['cena_bez_dph']);
$form->add_plain_text("VO - cena s dph", $page['cena_s_dph_vo']);
$form->add_plain_text("VO - cena bez dph", $page['cena_bez_dph_vo']);
$form->add_plain_text("Sazba dph", $page['dph']);

$status = array(
    0 => "---",
    1 => "Novinka",
    2 => "Akce",
    3 => "Doporučeno"
);

$ano_ne =  array(1 => TANO, 0 => TNE);

//$form->add_radiobuttons(TDPH, 'dph', $page['dph'], array(0 => "0%", 15 => "15%", 21 => "21%"));
$form->add_plain_text("Status", $status[$page['status']]);
$form->add_plain_text(TZOBRAZOVAT, $ano_ne[$page['zobrazit']]);



//kvuli porovnani stavu se uklada i stary stav
//nahravani_hlavniho_obrazku($idClanku, $form);

/*
$form->add_section(TOBSAH, "content");
$form->add_editor(TPEREX, 'perex',$page['perex'],409, false, "h300 wysiwygEditorMini","wysiwygEditorPerex");
$form->add_editor(TPOPIS_PRODUKTU, 'obsah',$page['obsah'],0,false,"wysiwygEditor h500")->allow_wet();
*/

//$form->add_textarea(TPRODUKT_ZALOZKA1, 'popis1',$page['popis1'], 0, false, "h300");
//$form->add_editor(TPRODUKT_ZALOZKA1, 'popis1',$page['popis1'],0,false,"wysiwygEditor h500","w2")->allow_wet();
//$form->add_editor(TPRODUKT_ZALOZKA2, 'popis2',$page['popis2'],0,false,"wysiwygEditor h500","w2")->allow_wet();







//$form->add_editor(TPRODUKT_ZALOZKA5, 'popis5',$page['popis5'],0,false,"wysiwygEditor h500","w5")->allow_wet();
//$form->add_editor(TPRODUKT_ZALOZKA6, 'popis6',$page['popis6'],0,false,"wysiwygEditor h500","w6")->allow_wet();
//$form->add_editor(TPRODUKT_ZALOZKA7, 'popis7',$page['popis7'],0,false,"wysiwygEditor h500","w7")->allow_wet();
//$form->add_editor(TPRODUKT_ZALOZKA2, 'popis2',$page['popis2'],0,false,"wysiwygEditor h500","w2")->allow_wet();
//$form->add_editor(TPRODUKT_ZALOZKA3, 'popis3',$page['popis3'],0,false,"wysiwygEditor h500","w3")->allow_wet();


$form->add_file(TNAHRAT_OBRAZEK, 'obrazek', false, 85,"","obrazek_input")->set_disabled($page['obrazek']!='');
  
        
$image_class = "";
if($page['obrazek']=='' || $page['obrazek']==null){
    $img = TNENI_ULOZEN_ZADNY_OBRAZEK;
    }
    else
    {	
    $file = $domain->getDir().USER_DIRNAME_ESHOP_MINI.$page['obrazek'];
				
    if(file_exists($file)) {
        $src = RELATIVE_PATH.$file."?".time(); 
        $img = "<img src='".$src."' alt='".$page['obrazek']."' title='".$page['obrazek']."' style='width: 300px;'/><br /><br />";
        $image_class = "image";
        if($login_obj->UserPrivilege('content_delete') && $object_access->has_access($idClanku))
            $img .= tlacitko('#',TSMAZAT,"","times","delete photo-delete","delete".$idClanku);
            else
            $img .= icon_disabled('delete');
        }
        else 
        {
        $src="";
        $img = TOBRAZEK_NENALEZEN;
        if($login_obj->UserPrivilege('content_delete'))
            $img .= tlacitko('#',TSMAZAT,"","times","delete photo-delete","delete".$idClanku);
            else
            $img .= icon_disabled('delete'); 
            }
        }

$form->add_plain_text(TOBRAZEK, $img)->set_class_td($image_class." relative tleft");
$form->add_hidden('obrazek',$page['obrazek']);
      
//vypis galerie - dalsi fotky k strance
show_photos($idClanku, $form);

//$form->add_text("Popis carouselu","carousel_popis",$page['carousel_popis']);
      
      
if(MODULE_GALLERY){
    $sel = ShowSelectBoxGallery();
    $sel = array(0 => "-- ".TVYBERTE." --") + $sel;

    if(count($sel) == 1){
        $text_neni = "";
        if($login_obj->UserPrivilege('content_add'))
            $text_neni .= tlacitko(get_link('gallery',"galerie",0,"new"));
        $form->add_plain_text(TGALERIE, $text_neni);

        }
        else
        $form->add_selectbox(TGALERIE, 'idGalerie', $page['idGalerie'], $sel);
           
    }
    else
    $form->add_hidden('idGalerie', $page['idGalerie']);
  
if(MODULE_INQUIRY){
    $d = $db->Query("SELECT idAnkety AS id, nazev FROM ".TABLE_ANKETY." WHERE idDomeny=".$domain->getId());
    $sel = array(0 => "-- ".TVYBERTE." --");
    $pocet = $db->numRows($d);
            
    while($s = $db->getAssoc($d))
        $sel[$s['id']] = $s['nazev'];
                
    if(count($sel) == 1){
        $text_neni = "";
        if($login_obj->UserPrivilege('content_add'))
            $text_neni .= tlacitko(get_link('inquiry',"ankety",0,'new'));
        
        $form->add_plain_text(TANKETA, $text_neni);    
        }
        else
        $form->add_selectbox(TANKETA, 'idAnkety', $page['idAnkety'], $sel);
    
    }
    else
    $form->add_hidden('idAnkety', $page['idAnkety']);
        
if(MODULE_DISCUSSION)
    $form->add_radiobuttons(TDISKUZE, 'diskuze', $page['diskuze'], array(1 => TANO, 0=>TNE));
    else
    $form->add_hidden('diskuze', $page['diskuze']);
    

            
$data_panely = $db->Query("SELECT idProfilu,nazev FROM ".TABLE_PANELY_SABLONY." WHERE idDomeny=".$domain->getId()." AND zobrazit=1 AND idJazyka=".WEB_LANG_ID." ORDER BY vychozi DESC ");
                
$profily = array();
while($profil = $db->getAssoc($data_panely))
    $profily[$profil['idProfilu']] = $profil['nazev'];

    /*
$form->add_section(TSABLONA_PANELU, 'panel-setting');
$form->add_selectbox(TSABLONA_PANELU, 'idProfiluPanelu', $page['idProfiluPanelu'], $profily, 135);         

$form->add_section(TNACASOVANI_ZOBRAZENI_A_ARCHIVACE, 'date');
$form->add_text(TOD, 'od', $page['publikovat_od'], 89, false, "kalendar","od");
$form->add_text(TDO, 'do', $page['publikovat_do'], 88, false, "kalendar","do");
*/

/*
$default_values = array();
$default_id = array();
$values = array();
$data = $db->Query("SELECT s.idStranky,s.idRodice,s.url,s.nazev, IF(sc.idSouvisejicihoClanku IS NULL,0,1) AS souvisejici 
            FROM ".TABLE_STRANKY." AS s
            LEFT JOIN ".TABLE_SOUVISEJICI_CLANKY." sc ON s.idStranky=sc.idSouvisejicihoClanku AND sc.idClanku=".$idClanku."
            WHERE s.typ='produkt' 
                AND s.idStranky!=".$idClanku."
                AND s.idDomeny=".$domain->getId()."
                 AND s.idJazyka=".WEB_LANG_ID."
            GROUP BY s.idStranky
            ORDER BY sc.idRelace
            ");
            
            
if($idClanku > 0)
{
    



    while($c = $db->getAssoc($data))
    {
        if($c['souvisejici']==1 && $idClanku>0)
        { 
            $default_values[] = $c['idStranky'];
            $values[$c['idStranky']] = $c['nazev'];
        }
        //$values[$c['idStranky']] = $c['nazev'];
        $default_id[$c['idStranky']] = "souvisejici s".$c['idRodice'];
    }
}
else
{
    $values_post = get_array_post('souvisejici');
    foreach($values_post AS $ids)
    {
        $values[$ids] = $db->get(TABLE_STRANKY,"nazev","idStranky=".$ids);
    }

}


if(MODULE_RELATED_PRODUCTS)
{
    $form->add_section(TSOUVISEJICI_PRODUKTY, 'content');
    
        
    if($db->numRows($data)==0)
        $form->add_plain_text("", TNENALEZEN_ZADNY_ZAZNAM);
        else
        $form->add_multiple_selectbox(TSOUVISEJICI_PRODUKTY, 'souvisejici', $default_values, $values, 424,false,"multiselect");
        
    
}
else
{
    foreach($default_values AS $v)
        $form->add_hidden('souvisejici[]', $v);
}


*/

show_private_pages_settings($idClanku, $page, $form);

/*

if($login_obj->UserPrivilege('seo'))
{
    $form->add_section(TSEO_PARAMETRY, 'seo', "", 8);
    $form->add_text(TURL_NAZEV, 'url', $page['url'], 9, $idClanku > 0);
    $form->add_text(TTITLE, 'title', $page['title'], 10,false,'','title');
    $form->add_textarea(TDESCRIPTION, 'description', $page['description'], 11);	
}
else
{
    $form->add_hidden('url', $page['url']);
    $form->add_hidden('title', $page['title'], 'title');
    $form->add_hidden('description', $page['description']);	
}
*/

$form->add_hidden('url', $page['url']);
$form->add_hidden('title', $page['title'], 'title');
$form->add_hidden('description', $page['description']);	


		
$res = (($login_obj->UserPrivilege('content_edit') && $idClanku>0) || ($login_obj->UserPrivilege('content_add') && $idClanku==0)) && $object_access->has_access($idClanku);

$form->add_next_actions();
$form->add_submit()->set_disabled(!$res);
    
echo $form->get_html_code();

?>


<script type="text/javascript">
<!--
$(function(){
      
    $("select.multiselect").multiselect({
        searchDelay: 500,
        nodeComparator: null,
        remoteParams: {id_clanku: <?php echo $idClanku;?>},
        remoteUrl: "<?php echo AJAX_GATEWAY;?>multiselect.search"
    });
    
    $("a.delete").click(function(){
        var id = $(this).attr('id').substring(6);
        
        var text = "<?php echo TOPRAVDU_SMAZAT_TENTO_OBRAZEK;?>";
        var td = $(this).parents("td.image").first();
        
        custom_confirm(text, function(){
            $.msg();
            td.addClass("deleted");
            $.post("<?php echo AJAX_GATEWAY;?>product.image.delete",{id: id},function(data){
                $.msg( 'setClass', 'ok_message' );
                $.msg( 'replace', '<?php echo OK_SMAZANO;?>' );
                $.msg( 'unblock', 1000);
                td.html("<?php echo TNENI_ULOZEN_ZADNY_OBRAZEK;?>");
                $("#obrazek_input").removeAttr('disabled');
                td.removeAttr("class");
                });
            
            })
            
        return false;
        
    })
    

    
})
// -->
</script>