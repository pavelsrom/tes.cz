<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */


//------------------------------------------------
//definice polozek menu
//------------------------------------------------

$module_menu = array();
$module_menu[] = array(
    "page"  => "kategorie",
    "name"  => TSTRANKY_S_PRODUKTY." ".WEB_LANG
    );

$module_menu[] = array(
    "page"  => "objednavky",
    "name"  => TOBJEDNAVKY
    );
        

/*    
$module_menu[] = array(
    "page"  => "mesta",
    "name"  => TCISELNIK_MESTA
    );
  */  
if($login_obj->UserPrivilege('settings_view'))
$module_menu[] = array(
    "page"  => "nastaveni",
    "name"  => TNASTAVENI
    );
   
    
    
//------------------------------------------------
//definice nadpisu h1 a textu k nadpisu
//------------------------------------------------ 
$module_h1 = array();
$module_h1[] = array(
    "page"  => "kategorie",
    "action"=> "edit-content",
    "verify_id_page" => 1,
    "verify_id_action" => 1,
    "name"  => TEDITACE_PRODUKTU
    );
$module_h1[] = array(
    "page"  => "kategorie",
    "action"=> "new",
    "verify_id_page" => 1,
    "name"  => TNOVY_PRODUKT
    );    
$module_h1[] = array(
    "page"  => "kategorie",
    "verify_id_page" => 1,
    "name"  => TSEZNAM_PRODUKTU." ".WEB_LANG,
    "text"  => 0
    ); 

$module_h1[] = array(
    "page"  => "objednavky",
    //"verify_id_page" => 1,
    "name"  => TOBJEDNAVKY,
    "text"  => 0
    ); 

$module_h1[] = array(
    "page"  => "objednavky",
    "verify_id_page" => 1,
    "name"  => TOBJEDNAVKY,
    "text"  => 0
    );


    
/*
$module_h1[] = array(
    "page"  => "mesta",
    "action"=> "edit-content",
    "verify_id_page" => 1,
    "verify_id_action" => 1,
    "name"  => TEDITACE
    );
$module_h1[] = array(
    "page"  => "mesta",
    "action"=> "new",
    "verify_id_page" => 1,
    "name"  => TNOVY
    );    
$module_h1[] = array(
    "page"  => "mesta",
    "verify_id_page" => 1,
    "name"  => TCISELNIK_MESTA,
    "text"  => 0
    );
  */  
    
$module_h1[] = array(
    "page"  => "nastaveni",
    "name"  => TNASTAVENI
    ); 
$module_h1[] = array(
    "page"  => "kategorie",
    "name"  => TSTRANKY_S_PRODUKTY." ".WEB_LANG,
    "text"  => 408
    );


?>