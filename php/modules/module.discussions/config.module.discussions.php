<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */


//------------------------------------------------
//definice polozek menu
//------------------------------------------------

$module_menu = array();
$module_menu[] = array(
    "page"  => "diskuze",
    "name"  => TSEZNAM_DISKUZI." ".WEB_LANG
    );
$module_menu[] = array(
    "page"  => "blacklist",
    "name"  => TBLACKLIST
    );   
$module_menu[] = array(
    "page"  => "filtr",
    "name"  => TFILTR_PRISPEVKU
    );

if($login_obj->UserPrivilege('settings_view'))
$module_menu[] = array(
    "page"  => "nastaveni",
    "name"  => TNASTAVENI
    ); 
    
//------------------------------------------------
//definice nadpisu h1 a textu k nadpisu
//------------------------------------------------ 
$module_h1 = array();
$module_h1[] = array(
    "page"  => "diskuze",
    "verify_id_page" => 1,
    "name"  => TDISKUZNI_PRISPEVKY,
    "text"  => 360
    );
$module_h1[] = array(
    "page"  => "nastaveni",
    "name"  => TNASTAVENI
    );
$module_h1[] = array(
    "page"  => "diskuze",
    "action"=> "edit-content",
    "verify_id_action" => 1,
    "verify_id_page" => 1,
    "name"  => TDISKUZNI_PRISPEVEK
    );
$module_h1[] = array(
    "page"  => "diskuze",
    "action"=> "new",
    "verify_id_page" => 1,
    "name"  => TDISKUZNI_PRISPEVEK
    );
$module_h1[] = array(
    "page"  => "diskuze",
    "name"  => TSEZNAM_DISKUZNICH_FOR." ".WEB_LANG,
    "text"  => 359
    ); 
$module_h1[] = array(
    "page"  => "blacklist",
    "name"  => TBLACKLIST." - ".TSEZNAM_ZAKAZANYCH_IP,
    "text"  => 361
    ); 
$module_h1[] = array(
    "page"  => "blacklist",
    "action"=> "by-ip",
    "name"  => TBLACKLIST." - ".TSEZNAM_ZAKAZANYCH_IP,
    "text"  => 361
    );
$module_h1[] = array(
    "page"  => "blacklist",
    "action"=> "edit-settings",
    "verify_id_action" => 1,
    "name"  => TEDITOVAT_IP,
    "text"  => 362
    );
$module_h1[] = array(
    "page"  => "blacklist",
    "action"=> "new",
    "name"  => TPRIDAT_IP,
    "text"  => 362
    );   
$module_h1[] = array(
    "page"  => "filtr",
    "name"  => TFILTR_PRISPEVKU." - ".TSEZNAM_VYRAZU_FILTRU,
    "text"  => 363
    ); 
 $module_h1[] = array(
    "page"  => "filtr",
    "action"=> "edit-settings",
    "verify_id_action" => 1,
    "name"  => TEDITOVAT_VYRAZ_PRO_FILTR,
    "text"  => 364
    );
$module_h1[] = array(
    "page"  => "filtr",
    "action"=> "new",
    "name"  => TNOVY_VYRAZ_PRO_FILTR,
    "text"  => 364
    );    
    
 
    



?>