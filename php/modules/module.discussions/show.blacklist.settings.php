<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined('SECURITY_CMS')) exit;
if(!$this->is_access('blacklist','edit-settings',0,1,'settings_view') &&
    !$this->is_access('blacklist','new',0,0,'settings_view')
    ) return;

$ip = "";
if($this->get_action() == 'new')
    $ip = secureString($this->get_value());
    
    
$idAdresy = $this->get_id_action();

$main_tools = array();        
$main_tools[] = array(
    'ikona' => 'back', 
    "nazev" => TZPET, 
    "aktivni" => 0, 
    "odkaz" => $this->get_link('blacklist'), 
    "title"=>TZPET_NA_SEZNAM_ZAKAZANYCH_IP
    );
    
echo main_tools($main_tools);
        
if(preg_match("/^(\d{1,3}\.){3}\d{1,3}$/", $ip) && $idAdresy <= 0)
{
          
    $ipa = explode('.',$ip);
    if(count($ipa) == 4)
    {
        $varianty = array();
        $varianty[0] = $ipa[0].".*";
        $varianty[1] = $ipa[0].".".$ipa[1].".*";
        $varianty[2] = $ipa[0].".".$ipa[1].".".$ipa[2].".*";
        $varianty[3] = $ip;
    }
}
elseif($idAdresy > 0)
{
    $data = $db->Query("SELECT ip FROM ".TABLE_DISKUZE_BLACKLIST." WHERE idPolozky=".$idAdresy." AND idDomeny=".$domain->getId()." LIMIT 1");
    
    $ipdata = $db->getAssoc($data);
    $ip = secureString($ipdata['ip']);
}
else
$ip = "";
      
$form = new Form();
$form->add_section(TZAKLADNI_UDAJE,'home');
        
if(isset($varianty))
{
            
            ?> 
            <script type='text/javascript'>
            
            <!--
            $(function(){
                $('a.a').click(function(){
                    var id_a = $(this).attr('id');
                    var id_span = id_a.substring(1,id_a.length);
                    var ip = $("#ip" + id_span).text();
                    
                    $('#ip').val(ip);
                    
                    return false;
                })
            })
            
            // -->
            </script>
                
            <?php
            $code = "";
            foreach($varianty AS $idv => $v)
                $code .= tlacitko('#',TVYBRAT,"","plus","a","a".$idv)."&nbsp;&nbsp;&nbsp;<span id='ip".$idv."'>".$v."</span><br /><br />";
            
    $form->add_plain_text(TALTERNATIVY_IP,$code,273);
}

$form->add_text(TADRESA_IP,'ip',$ip,274,true,"","ip");

$res = $login_obj->UserPrivilege('settings_edit');
$form->add_submit()->set_disabled(!$res);

echo $form->get_html_code();

?>