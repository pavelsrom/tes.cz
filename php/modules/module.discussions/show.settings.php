<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined('SECURITY_CMS')) exit;
if(!$this->is_access('nastaveni',"",0,0,'settings_view')) return;

show_settings();
		

?>


<script type="text/javascript">

$(function(){
    
    function zobraz_smajliky()
    {
        var idThemes = $("#smajlici_select option:selected").val();
        if(idThemes == 0) return;
        $.post('<?php echo AJAX_GATEWAY;?>smiles', {idTematu: idThemes}, function(smiles) {
    		$('#smajlici').html(smiles);
    		});
    }
    
    $("#smajlici_select").change(zobraz_smajliky);
    zobraz_smajliky();
        
    })

</script>