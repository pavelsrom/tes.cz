<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined('SECURITY_CMS')) exit;
if(!$this->is_access('blacklist','by-ip')) return;

$ip = $this->get_value();      
$ip = $db->secureString($ip);
$ip_bez_hvezdicek = str_replace('*','',$ip);
                
$main_tools = array();
$main_tools[] = array(
    'ikona' => 'back', 
    "nazev" => TZPET, 
    "aktivni" => 0, 
    "odkaz" => $this->get_link('blacklist'), 
    "title" => TZPET_NA_SEZNAM_ZAKAZANYCH_IP
    );
            
echo main_tools($main_tools);

if(!TestIp($ip, false)){
    echo "<div class='error w45'><ul><li>".TNEPLATNA_IP."</li></ul></div>";
    return;
    }


$data = $db->Query("SELECT s.nazev, dp.*, IF(dp.datum!='', DATE_FORMAT(dp.datum, '%d.%m.%Y (%H:%i)'),'') AS datum, dp.zobrazit, dp.novy, dp.idStranky, dp.admin
			FROM ".TABLE_DISKUZE_POLOZKY." AS dp
			LEFT JOIN ".TABLE_STRANKY." AS s ON dp.idStranky=s.idStranky
			WHERE s.idDomeny=".$domain->getId()."
				AND dp.idPolozky IS NOT NULL
				AND dp.ip LIKE '".$ip_bez_hvezdicek."%'
			ORDER BY dp.datum DESC, dp.idPolozky DESC
				
			");
		
$idSmiles = intval(SMAJLICI);
        
$dis_content = !$login_obj->UserPrivilege('settings_view');
$dis_delete = !$login_obj->UserPrivilege('settings_delete');
        
$table = new Table();
$table->tr_head()
    ->add(TID,'w25')
    ->add(TJMENO,"t-left")
    ->add(TDISKUZNI_PRISPEVEK,"t-left")
    ->add(TDISKUZE,"t-left")
    ->add(TAKTIVNI,'w25')
    ->add(TAKCE,'w90');

	
if($idSmiles > 0){
    include_once('php/classes/class.smiles.php');
    $smiles = new C_Smiles($idSmiles);
    }
		
while($prispevek = $db->getAssoc($data)){
    $id = $prispevek['idPolozky'];
    $nazev = secureString($prispevek['jmeno']);
    $stranka = secureString($prispevek['nazev']);
    $text = secureString(strip_tags($prispevek['text']));
    $stav = secureString($prispevek['novy']);
    $zobrazit = $prispevek['zobrazit'];
    $idDiskuze = $prispevek['idStranky'];
            
    $zobrazit = ano_ne($zobrazit);
           
	if(isset($smiles)) 
        $text = $smiles->DeleteSmilesFromText($text);
                
    $stav_flag="";
	if($stav==1) $stav_flag = "<span class='stav0'>(".TNOVY.")</span>";
            
    if($prispevek['admin'] == 1)
        $class = "selected";
        else
        $class = "";
            
    $ikony = "";			
	if($dis_content)
	   $ikony .= icon_disabled('content');
	   else
	   $ikony .=  "<a href='".$this->get_link('diskuze',$idDiskuze,'edit-content',$id)."'>".icon('content')."</a>";
				
	if($dis_delete)
	   $ikony .=  icon_disabled('delete');
	   else
	   $ikony .= "<a href='#' id='delete".$id."' class='delete'>".icon('delete')."</a>";

		
    $table->tr($class)
            ->add($id)
            ->add($nazev,'tleft')
            ->add($text,'tleft')
            ->add($stranka,'tleft')
            ->add($zobrazit)
            ->add($ikony,"akce");	
			
}

echo $table->get_html();	
		

?>

<script type="text/javascript">
<!--
$(function(){
    
    $(document).on("click","a.delete", function(){
        var id = $(this).attr('id').substring(6);
        var tr = $(this).parents("tr").first();
        var c = $(this).parents("tr").find("td").eq(0).text(); 
        
        var text = "<?php echo TOPRAVDU_SMAZAT;?>";

        custom_confirm(text.replace("%s",c) + '?', function(){
            $.msg();
            tr.addClass("deleted");
            $.post("<?php echo AJAX_GATEWAY;?>contributions.delete",{id: id},function(data){
                tr.fadeOut(function(){
                    $.msg( 'setClass', 'ok_message' );
                    $.msg( 'replace', '<?php echo OK_SMAZANO;?>' );
                    $.msg( 'unblock', 1000);
                    });
                });
            
            })
        
        return false;
        
        
    });
    
})

// -->
</script>