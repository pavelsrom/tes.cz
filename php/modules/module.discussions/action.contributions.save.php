<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */


if(!defined('SECURITY_CMS')) exit;
if(!is_action() || 
    (!$this->is_access('diskuze','new',1,0,'content_add') && 
    !$this->is_access('diskuze','edit-content',1,1,'content_edit'))
    ) return;


$idDiskuze = $this->get_id_page();
$idPrispevku = $this->get_id_action();
            
$jmeno = get_post('jmeno');
$email = get_post('email');
$nadpis = get_post('nadpis');
$text = get_post('text');
$zobrazit = get_int_post('zobrazit');

if(!TestEmail($email,200,true))
    $systemMessage->add_error(ERROR_EMAIL);
        
if($systemMessage->error_exists()) return;

$update = array(
    "email"     => $email,
    "nadpis"    => $nadpis,
    "jmeno"     => $jmeno,
    "text"      => $text,
    "zobrazit"  => $zobrazit,
    "idStranky" => $idDiskuze
    );
            
if($idPrispevku > 0)
    $db->update(TABLE_DISKUZE_POLOZKY,$update,"idPolozky=".$idPrispevku);			
else
{
    $update["admin"] = 1;
    $update["novy"] = 0;
    $update["datum"] = "now";
    $update["ip"] = $_SERVER['REMOTE_ADDR'];
    
    $db->insert(TABLE_DISKUZE_POLOZKY,$update);              
    $idPrispevku = $db->lastId();
}

$nazev = $db->get(TABLE_STRANKY,'nazev','idStranky='.$idDiskuze);
$log->add_log($this->get_url('id_action') == 0 ? 'create':'edit-content','diskuze-prispevek',$idPrispevku,$nazev);
            
$systemMessage->add_ok(OK_ULOZENO);
$url = $this->get_link('diskuze',$idDiskuze,'edit-content',$idPrispevku);
Redirect($url);	


?>