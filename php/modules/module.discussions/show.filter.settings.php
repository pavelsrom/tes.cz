<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined('SECURITY_CMS')) exit;
if(!$this->is_access('filtr','edit-settings',0,1,'settings_view') &&
    !$this->is_access('filtr','new',0,0,'settings_add')
    ) return;

$idFiltru = $this->get_id_action();

$main_tools = array();
$main_tools[] = array(
    'ikona' => 'back', 
    "nazev" => TZPET, 
    "aktivni" => 0, 
    "odkaz" => $this->get_link('filtr'), 
    "title" => TZPET_NA_SEZNAM_VYRAZU_FILTRU
    );
            
echo main_tools($main_tools);
        
if($idFiltru > 0)
{
    $data = $db->Query("SELECT filtr, zobrazit FROM ".TABLE_DISKUZE_FILTR." WHERE idPolozky=".$idFiltru." AND idDomeny=".$domain->getId()." LIMIT 1");
    $filtr = $db->getAssoc($data);
    
    $vyraz = get_secure_post('vyraz',false,$filtr['filtr']);
    $zobrazit = get_int_post('zobrazit',$filtr['zobrazit']);
}
else
{
    $vyraz = get_secure_post('vyraz');
    $zobrazit = get_int_post('zobrazit',1);
}

$form = new Form();
$form->add_section(TZAKLADNI_UDAJE,'home');
$form->add_text(TVYRAZ_FILTRU,'vyraz',$vyraz,275,true);
$form->add_radiobuttons(TZOBRAZOVAT,'zobrazit',$zobrazit,null,276);

$res = $login_obj->UserPrivilege('settings_edit');
$form->add_submit()->set_disabled(!$res);
    
echo $form->get_html_code();

?>