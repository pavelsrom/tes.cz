<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined('SECURITY_CMS')) exit;
if(!$this->is_access('blacklist')) return;

$ip = $this->get_value();
        
$data = $db->Query("SELECT idPolozky, ip, 
            IF('".$ip."' != '' AND ip LIKE '".$db->secureString($ip)."%', 1, 0) AS ozn 
            FROM ".TABLE_DISKUZE_BLACKLIST." 
            WHERE idDomeny=".$domain->getId()." 
            ORDER BY idPolozky");
        
$main_tools = array();
        
if($login_obj->UserPrivilege('settings_add'))
    $main_tools[] = array(
        'ikona' => 'new', 
        "nazev" => TPRIDAT_IP_ADRESU, 
        "aktivni" => 0, 
        "odkaz" => $this->get_link('blacklist',0,'new')
        );
        
echo main_tools($main_tools);

$table = new Table();
$table->tr_head()
    ->add(TADRESA_IP)
    ->add(TPOCET_PRISPEVKU)
    ->add(TAKCE,'w90');
        
$dis_content = !$login_obj->UserPrivilege('settings_view');
$dis_delete = !$login_obj->UserPrivilege('settings_delete');

        
while($blacklist = $db->getAssoc($data)){
	$ip = $blacklist['ip'];
    $id = $blacklist['idPolozky'];
    $ozn = $blacklist['ozn'];
            
    if($ozn == 1) 
        $style = "style='background: yellow'"; else $style = "";
            
    $ip_bez_hvezdicek = str_replace('*','',$ip);
            
    $d = $db->Query("SELECT COUNT(dp.idPolozky) AS pocet_prispevku
                FROM ".TABLE_DISKUZE_POLOZKY." AS dp
                LEFT JOIN ".TABLE_STRANKY." AS d ON dp.idStranky = d.idStranky
                WHERE dp.idStranky IS NOT NULL
                    AND d.diskuze = 1
                    AND d.idDomeny = ".$domain->getId()."
                    AND dp.ip LIKE '".$ip_bez_hvezdicek."%'
                GROUP BY d.idDomeny
                ");
                
    $dd = $db->getAssoc($d);    
	$pocet_prispevku = intval($dd['pocet_prispevku']);

    $ikony = "";
    if($dis_content)
        $ikony .= icon_disabled('content');
        else
        $ikony .= "<a href='".$this->get_link('blacklist',0,'edit-settings',$id)."'>".icon('content')."</a>";
                
    if($dis_delete)
        $ikony .=  icon_disabled('delete');
        else
        $ikony .=  "<a href='#' id='delete".$id."' class='delete'>".icon('delete')."</a>";

    
    $table->tr()
        ->add("<a href='".$this->get_link('blacklist',0,'by-ip',$ip)."' title='".TZOBRAZIT_VSECHNY_PRISPEVKY_Z_IP."'>".$ip."</a>",'tleft')
        ->add($pocet_prispevku,'center')
        ->add($ikony,"akce");

			
		}

echo $table->get_html();	
       
?>

<script type="text/javascript">
<!--

$(function(){
    
    $(document).on("click","a.delete", function(){
        var id = $(this).attr('id').substring(6);
        var tr = $(this).parents("tr").first();
        var c = $(this).parents("tr").find("td").eq(0).text(); 
        
        var text = "<?php echo TOPRAVDU_SMAZAT;?>";

        custom_confirm(text.replace("%s",c) + '?', function(){
            $.msg();
            tr.addClass("deleted");
            $.post("<?php echo AJAX_GATEWAY;?>blacklist.delete",{id: id},function(data){
                tr.fadeOut(function(){
                    $.msg( 'setClass', 'ok_message' );
                    $.msg( 'replace', '<?php echo OK_SMAZANO;?>' );
                    $.msg( 'unblock', 1000);
                    });
                });
            
            })
        
        return false;
        
        
    });
    
})

// -->
</script>