<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined('SECURITY_CMS')) exit;
if(!is_action() || 
    (!$this->is_access('blacklist','new',0,0,'settings_add') && 
    !$this->is_access('blacklist','edit-settings',0,1,'settings_edit'))
    ) return;
    
$idAdresy = $this->get_id_action();
$ip = get_post('ip');
                        
if(!testIp($ip, false))
    $systemMessage->add_error(ERROR_IP);
            
if($idAdresy == 0)
    $dv = $db->Query("SELECT idPolozky FROM ".TABLE_DISKUZE_BLACKLIST." WHERE ip='".$ip."' AND idDomeny=".$domain->getId()." LIMIT 1");
    else
    $dv = $db->Query("SELECT idPolozky FROM ".TABLE_DISKUZE_BLACKLIST." WHERE ip='".$ip."' AND idDomeny=".$domain->getId()." AND idPolozky!=".$idAdresy." LIMIT 1");
            
if($db->numRows($dv)>0)
    $systemMessage->add_error(ERROR_IP_EXISTUJE);
            
if($systemMessage->error_exists()) return;
            
$update = array(
    "ip"        => $ip,
    "idDomeny"  => $domain->getId(),
    "zobrazit"  => 1
    );
            
if($idAdresy == 0){
    $db->insert(TABLE_DISKUZE_BLACKLIST,$update);
    $idAdresy = $db->lastId();
    }
    else
    $db->update(TABLE_DISKUZE_BLACKLIST,$update,"idPolozky = ".$idAdresy." AND idDomeny = ".$domain->getId());
          
$log->add_log($this->get_url('id_action') == 0 ? 'create':'edit-settings','diskuze-blacklist',$idAdresy,$ip);
          
$systemMessage->add_ok(OK_ULOZENO);				
$url = $this->get_link('blacklist',0,'edit-settings',$idAdresy);
Redirect($url);
            
?>