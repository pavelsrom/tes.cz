<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */


if(!defined('SECURITY_CMS')) exit;
if(!$this->is_access('diskuze','',1,0,'content_view')) return;

$idDiskuze = $this->get_id_page();

$main_tools = array();
        
if($login_obj->UserPrivilege('content_add'))
    $main_tools[] = array(
        'ikona' => 'new', 
        "nazev" => TPRIDAT_PRISPEVEK, 
        "aktivni" => 0, 
        "odkaz" => $this->get_link('diskuze',$idDiskuze,'new')
        );
        
$main_tools[] = array(
    'ikona' => 'back', 
    "nazev" => TZPET, 
    "aktivni" => 0, 
    "odkaz" => $this->get_link('diskuze'), 
    "title" => TZPET_NA_SEZNAM_DISKUZI
    );

$form = new FormFilter();
$form->add_text(THLEDAT, 'vyraz', "", 0, false, "", "vyraz");
echo $form->get_html_code(main_tools($main_tools));
        
$table = new Table('tList');
$table->tr_head()
    ->add(TID,'w25')
    ->add(TJMENO,'w110')
    ->add(TDISKUZNI_PRISPEVEK)
    ->add(TSTAV,"w90")
    ->add(TDATUM,'w80')
    ->add(TAKTIVNI,'w25')
    ->add(TAKCE,'w90');
    
echo $table->get_html();

?>
<script type="text/javascript">
<!--

$(function(){
    var oTable = $("#tList").dataTable({
        "sAjaxSource": "<?php echo AJAX_GATEWAY;?>contributions.list&idDiskuze=<?php echo $idDiskuze;?>",
        "aoColumns": [
            {"sClass": "sl1"},{"sClass": "sl2"},{"sClass": "sl3 tleft", "bSortable": false},{"sClass": "sl4"},{"sClass": "sl5"},{"sClass": "sl6"},{"sClass": "akce", "bSortable": false}
            ],
        "fnServerData": function ( sSource, aoData, fnCallback ) {
                aoData.push({ "name": "vyraz", "value": $("#vyraz").val() });
                $.getJSON( sSource, aoData, function (json) { fnCallback(json); } );
                }
     
        })
    
    $("input#vyraz").bind('keyup change', function() {
        oTable.fnDraw(); 
        });
    
    //prida k bunce th span
    $(".dataTable th").each(function(){
        $(this).html("<span>" + $(this).html() + "</span>");
        })
        
    $(document).on("click","a.delete", function(){
        var id = $(this).attr('id').substring(6);
        var tr = $(this).parents("tr").first();
        var c = $(this).parents("tr").find("td").eq(0).text(); 
        
        var text = "<?php echo TOPRAVDU_SMAZAT;?>";

        custom_confirm(text.replace("%s",c) + '?', function(){
            $.msg();
            tr.addClass("deleted");
            $.post("<?php echo AJAX_GATEWAY;?>contributions.delete",{id: id},function(data){
                tr.fadeOut(function(){
                    $.msg( 'setClass', 'ok_message' );
                    $.msg( 'replace', '<?php echo OK_SMAZANO;?>' );
                    oTable.fnDraw();
                    $.msg( 'unblock', 1000);
                    });
                });
            
            })
        
        return false;
        
        
    });
    

})

// -->
</script>
