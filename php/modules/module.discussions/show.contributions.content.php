<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined('SECURITY_CMS')) exit;
if(!$this->is_access('diskuze','edit-content',1,1,'content_view') &&
    !$this->is_access('diskuze','new',1,0,'content_add')
    ) return;
    
$idDiskuze = $this->get_id_page();
$idContribution = $this->get_id_action();
		
if($idDiskuze==0) return;
		
        
if($idContribution > 0){
                        
    $data = $db->Query("SELECT dp.*, 
    				IF(dp.datum!='', DATE_FORMAT(dp.datum,'%d.%m.%Y (%H:%i)'), '') AS datum,
    				s.nazev AS nazev_diskuze 
    			FROM ".TABLE_DISKUZE_POLOZKY." AS dp
    			LEFT JOIN ".TABLE_STRANKY." AS s ON dp.idStranky=s.idStranky
    			WHERE s.idDomeny=".$domain->getId()."
    				AND dp.idPolozky=".$idContribution."
    			LIMIT 1
    			");
    			
    		
    if($db->numRows($data)==0) return;
    $prispevek = $db->getAssoc($data);
            
    if($prispevek['novy']==0)
        $db->Query("UPDATE ".TABLE_DISKUZE_POLOZKY." SET novy=1 WHERE idPolozky=".$idContribution." LIMIT 1");
            
    }
    else
    {
                $prispevek = array();
                $prispevek['nadpis'] = "";
                $prispevek['ip'] = $_SERVER['REMOTE_ADDR'];
                $prispevek['admin'] = 1;
                $prispevek['text'] = "";
                $prispevek['email'] = EMAIL_WEBU;
                $prispevek['jmeno'] = $login_obj->getName();
                $prispevek['zobrazit'] = 1;
                $prispevek['datum'] = "";
    }

$odesilatel = $prispevek['email'];		

$ip_arr = explode('*',$prispevek['ip']);
$d = $db->query("SELECT idPolozky FROM ".TABLE_DISKUZE_BLACKLIST." WHERE ip LIKE '".$ip_arr[0]."%' LIMIT 1");
$prispevek['blacklist'] = $db->numRows($d);
$idLinku = 0;

if($db->numRows($d) > 0)
{
    $ipres = $db->getAssoc($d);
    $idLinku = $ipres['idPolozky'];
}


//pri nacteni teto zpravy bude zprava oznacena jako prijata, pri odeslani emailu bude oznacena jako odpovezeno
	
$main_tools = array();
$main_tools[] = array(
        'ikona' => 'back', 
        "nazev" => TZPET, 
        "aktivni" => 0, 
        "odkaz" => $this->get_link('diskuze',$idDiskuze), 
        "title" => TZPET_NA_SEZNAM_PRISPEVKU
        );
        
echo main_tools($main_tools);
		
$form = new Form();
$form->add_section(TZAKLADNI_UDAJE,'home');
     
if($prispevek['ip'] != ''){   
     
    if($prispevek['blacklist'] == 0){
        $doplneni = tlacitko($this->get_link('blacklist',0,'new',0,$prispevek['ip']),TZAKAZAT_PRISPEVKY_Z_TETO_IP,"<span class='stav1'>".TJE_POVOLENA."</span>","times-circle");
        }
        else
        {
        $doplneni = tlacitko($this->get_link('blacklist',0,'edit-settings',$idLinku),TPOVOLIT_PRISPEVKY_Z_TETO_IP,"<span class='stav0'>".TJE_ZAKAZANA."</span>","check-square-o");
        }
    }

$doplneni .= tlacitko($this->get_link('blacklist',0,'by-ip',0,$prispevek['ip']), TVYPSAT_VSECHNY_PRISPEVKY_Z_TETO_IP,"","navicon");
        
$form->add_plain_text(TADRESA_IP,($prispevek['ip']==''?TNEUVEDENO:$prispevek['ip'])." ".$doplneni,269);


$form->add_plain_text(TZADANO_ADMINISTRATOREM,$prispevek['admin']==0?TNE:TANO,270);          
$form->add_plain_text(TDATUM,($prispevek['datum']==''?TNEUVEDENO:$prispevek['datum']),271); 

$form->add_text(TJMENO,'jmeno',$prispevek['jmeno']);
$form->add_text(TEMAIL,'email',$prispevek['email']);
$form->add_text(TNADPIS,'nadpis',$prispevek['nadpis']);
$form->add_editor(TPRISPEVEK,'text',$prispevek['text'],0,false,"h100");
$form->add_radiobuttons(TZOBRAZOVAT,'zobrazit',$prispevek['zobrazit'],null,272);        
        
$res = ($idContribution == 0 && $login_obj->UserPrivilege('content_add')) || ($idContribution > 0 && $login_obj->UserPrivilege('content_edit'));

$form->add_submit()->set_disabled(!$res);
    
echo $form->get_html_code();
    
?>