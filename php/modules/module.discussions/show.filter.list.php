<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined('SECURITY_CMS')) exit;
if(!$this->is_access('filtr')) return;
    
$data = $db->Query("SELECT *
            FROM ".TABLE_DISKUZE_FILTR." 
            WHERE idDomeny=".$domain->getId()." 
            ORDER BY idPolozky");
        
$main_tools = array();
        
if($login_obj->UserPrivilege('settings_add'))
    $main_tools[] = array(
        'ikona' => 'new', 
        "nazev" => TPRIDAT_FILTR, 
        "aktivni" => 0, 
        "odkaz" => $this->get_link('filtr',0,'new')
        );
        
           
echo main_tools($main_tools);

$table = new Table();
$table->tr_head()
    ->add(TVYRAZ)
    ->add(TAKTIVNI,'w25')
    ->add(TAKCE,'w90');
    

$dis_content = !$login_obj->UserPrivilege('settings_view');
$dis_delete = !$login_obj->UserPrivilege('settings_delete');
            
       
while($filtr = $db->getAssoc($data)){
    $vyraz = $filtr['filtr'];
    $id = $filtr['idPolozky'];
    $aktivni = $filtr['zobrazit'];
    $ikony = "";		
    
    if($dis_content)
        $ikony .= icon_disabled('content');
        else
        $ikony .=  "<a href='".$this->get_link('filtr',0,'edit-settings',$id)."'>".icon('content')."</a>";
                
    if($dis_delete)
        $ikony .=  icon_disabled('delete');
        else
        $ikony .=  "<a href='#' id='delete".$id."' class='delete'>".icon('delete')."</a>";
			
		
    $table->tr()
        ->add($vyraz,'tleft')
        ->add(ano_ne($aktivni),'center') 
        ->add($ikony,"akce");			
    }
        
echo $table->get_html();
	

?>

<script type="text/javascript">
<!--

$(function(){
    
    $(document).on("click","a.delete", function(){
        var id = $(this).attr('id').substring(6);
        var tr = $(this).parents("tr").first();
        var c = $(this).parents("tr").find("td").eq(0).text(); 
        
        var text = "<?php echo TOPRAVDU_SMAZAT;?>";

        custom_confirm(text.replace("%s",c) + '?', function(){
            $.msg();
            tr.addClass("deleted");
            $.post("<?php echo AJAX_GATEWAY;?>filter.delete",{id: id},function(data){
                tr.fadeOut(function(){
                    $.msg( 'setClass', 'ok_message' );
                    $.msg( 'replace', '<?php echo OK_SMAZANO;?>' );
                    $.msg( 'unblock', 1000);
                    });
                });
            
            })
        
        return false;
        
        
    });
    
})

// -->
</script>