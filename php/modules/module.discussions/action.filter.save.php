<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined('SECURITY_CMS')) exit;
if(!is_action() || 
    (!$this->is_access('filtr','new',0,0,'settings_add') && 
    !$this->is_access('filtr','edit-settings',0,1,'settings_edit'))
    ) return;

$idFiltru = $this->get_id_action();
$vyraz = get_post('vyraz');
$zobrazit = get_int_post('zobrazit',1);
            
if($vyraz == '')
    $systemMessage->add_error(ERROR_VYRAZ_FILTRU_NESMI_BYT_PRAZDNY);
                
if($systemMessage->error_exists()) return;

$update = array(
    "filtr"     => $vyraz,
    "idDomeny"  => $domain->getId(),
    "zobrazit"  => $zobrazit
    );
           
if($idFiltru == 0){
    $db->insert(TABLE_DISKUZE_FILTR,$update);
    $idFiltru = $db->lastId();
    }
    else
    {
    $db->update(TABLE_DISKUZE_FILTR,$update,"idDomeny=".$domain->getId()." AND idPolozky = ".$idFiltru);
    }
        
$log->add_log($this->get_id_action() == 0 ? 'create':'edit-settings','diskuze-filtr',$idFiltru,$vyraz);
                
$systemMessage->add_ok(OK_ULOZENO);
Redirect($this->get_link('filtr',0,'edit-settings',$idFiltru));
            

?>