<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined('SECURITY_CMS')) exit;
if(!is_action() || 
    (!$this->is_access('provize','edit-content',0,1,'content_edit') && 
    !$this->is_access('provize','new',0,0,'content_add'))
     
    ) return;


$idStranky = $this->get_id_action();	
$stav = get_int_post("stav");	

if($systemMessage->error_exists()) return;


$update = array("id_stav" => $stav);
	    
if($idStranky == 0){

    
    $db->insert(TABLE_PROVIZE_ZADOSTI,$update);
    $idStranky = $db->lastId();

    }
    else
    {
    
    $db->update(TABLE_PROVIZE_ZADOSTI,$update,"id=".$idStranky);

	}


$log->add_log($this->get_url('id_action') == 0 ? 'create':'edit-content','provize',$idStranky);

$systemMessage->add_ok(OK_ULOZENO);

$url = $this->get_link('provize',0,'edit-content',$idStranky);

Redirect($url);

?>