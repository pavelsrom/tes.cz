<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined('SECURITY_CMS')) exit;
if(!$this->is_access('objednavky','edit-content',0,1,'content_view') &&
    !$this->is_access('objednavky','new',0,0,'content_add')
    ) return;

$idClanku = $this->get_id_action(); 

$main_tools = array();

if($idClanku > 0) {
    $record1 = $db->Query("SELECT o.*, COUNT(op.id) AS pocet, s.nazev AS stav, u.hash,
                    DATE_FORMAT(datum,'%d.%m.%Y %H:%i') AS datum,
                    IF(zaplaceno IS NULL,' - ', DATE_FORMAT(zaplaceno,'%d.%m.%Y %H:%i')) AS zaplaceno
				FROM ".TABLE_ESHOP_OBJEDNAVKY." AS o
                LEFT JOIN ".TABLE_ESHOP_OBJEDNAVKY_POLOZKY." AS op ON o.id = op.id_objednavka
                LEFT JOIN ".TABLE_ESHOP_OBJEDNAVKY_STAVY." AS s ON s.id = o.id_stav
                LEFT JOIN ".TABLE_UZIVATELE." AS u ON u.idUzivatele = o.id_uzivatel
                WHERE o.id = ".$idClanku."
				GROUP BY o.id
			LIMIT 1");
			
    $page = $db->getAssoc($record1);
          
           

    }
    else
    {
    
    }

$main_tools[] = array(
    'ikona' => 'back', 
    "nazev" => TZPET, 
    "aktivni" => 0, 
    "odkaz" => $this->get_link('objednavky'), 
    "title" => TZPET_NA_SEZNAM
    );

echo main_tools($main_tools);

$data = $db->Query("SELECT id, nazev FROM ".TABLE_ESHOP_OBJEDNAVKY_STAVY." ORDER BY id");
$stavy = array(0=> "-- ".TVSECHNO." --");
while($sk = $db->getObject($data))
    $stavy[$sk->id] = $sk->nazev;


$form = new Form();
$form->add_section(TZAKLADNI_UDAJE, "home");

$form->add_plain_text(TID, $page['id']);
$form->add_plain_text(TCISLO_FAKTURY, $page['cislo_faktury']);
//$form->add_plain_text(TSTAV, $page['stav']);
$form->add_selectbox(TSTAV,'stav',$page['id_stav'],$stavy);
$form->add_plain_text(TID_PLATBY_GOPAY, $page['id_platby']);
$form->add_plain_text(TJMENO_A_PRIJMENI, $page['jmeno_uzivatele']);
$form->add_plain_text(TCENA_BEZ_DPH, price($page['cena']));
$form->add_plain_text(TVYTVORENO, $page['datum']);
$form->add_plain_text(TZAPLACENO, $page['zaplaceno']);
$form->add_plain_text(TPOZNAMKA, $page['poznamka']);

$form->add_section("Položky objednávky", "home");

$data = $db->query("SELECT *, 
    IF(od IS NULL,'',DATE_FORMAT(od, '%d.%m.%Y')) AS od,
    IF(do IS NULL,'',DATE_FORMAT(do, '%d.%m.%Y')) AS do, 
    IF(datum_dalsi_platby IS NULL,'',DATE_FORMAT(datum_dalsi_platby, '%d.%m.%Y')) AS datum_dalsi_platby 
    FROM ".TABLE_ESHOP_OBJEDNAVKY_POLOZKY." WHERE id_objednavka=".$idClanku);


$form->add_code('<tr><th class="tleft">'.TNAZEV.'</th><th class="tleft">'.TCENA.'</th></tr>');
$id_nasledujici_platby = 0;
$datum_nasledujici_platby = "";
while($p = $db->getObject($data))
{
    $form->add_code('<tr><td>'.$p->nazev.'</td><td>'.price($p->cena).'</td></tr>');
    $id_nasledujici_platby = $id_nasledujici_platby == 0 ? $p->id_dalsi_objednavky : $id_nasledujici_platby;
    $datum_nasledujici_platby = $p->datum_dalsi_platby;
}


if($id_nasledujici_platby > 0)
{
    $form->add_section("Následující platba", "home");
    $form->add_plain_text("Následující platba", '<a class="tlacitko" href="'.$this->get_link('objednavky',0,'edit-content',$id_nasledujici_platby).'">Přejít na objednávku</a>');
    $form->add_plain_text("Datum následující platby", $datum_nasledujici_platby);
}

//provize
$data = $db->query("SELECT p.*, CONCAT(u.jmeno,' ',u.prijmeni ) AS jmeno 
    FROM ".TABLE_PROVIZE." AS p
    LEFT JOIN ".TABLE_UZIVATELE." AS u ON p.id_uzivatel = u.idUzivatele 
    WHERE id_objednavka=".$idClanku."
    ORDER BY p.stupen");

if($db->numRows($data) > 0)
{
    $form->add_section("Výpis provizí", "home");
    
    require_once("php/classes/class.table.php");
    $table = new Table();
    $tr = $table->tr_head()
        ->add(TJMENO_A_PRIJMENI,'tleft')
        ->add(TCELKEM,'tright')
        ->add("Úroveň provize",'tright')
        ->add(TSTAV,'w25 center');
    
    
    while($p = $db->getObject($data))
    {
        $table->tr()
            ->add($p->jmeno,'tleft')
            ->add(price($p->castka),'tright')
            ->add(constant("TTYP_PREDPLATNEHO_".$p->predplatne_typ)." / ".$p->stupen,'tright')
            ->add(ano_ne($p->aktivni));
    }
    
    $form->add_code('<tr><td colspan="2">'.$table->get_html().'</td></tr>');
    
}





		
$res = (($login_obj->UserPrivilege('content_edit') && $idClanku>0) || ($login_obj->UserPrivilege('content_add') && $idClanku==0));

//$form->add_next_actions();
$form->add_submit()->set_disabled(!$res);
    
echo $form->get_html_code();

?>
