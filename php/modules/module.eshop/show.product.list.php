<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */


if(!defined('SECURITY_CMS')) exit;
if(!$this->is_access('kategorie','',1,0)) return;


$idRubriky = $this->get_url('id_page');

$main_tools = array();
        
if($login_obj->UserPrivilege('settings_add'))
    $main_tools[] = array(
        'ikona' => 'new', 
        "nazev" => TNOVY_PRODUKT, 
        "aktivni" => 0, 
        "odkaz" => $this->get_link('kategorie',$idRubriky,'new')
        );
        
$main_tools[] = array(
    'ikona' => 'back', 
    "nazev" => TZPET, 
    "aktivni" => 0, 
    "odkaz" => $this->get_link('kategorie'), 
    "title" => TZPET_NA_SEZNAM_KATEGORII);
        
        
$data_rubriky = $db->query("SELECT idStranky, nazev FROM ".TABLE_STRANKY." WHERE typ='produkty' AND idDomeny=".$domain->getId()." AND idJazyka=".WEB_LANG_ID." ORDER BY nazev");

$select_rubriky = array(0 => "-- ".TVYBERTE." --");
while($r = $db->getAssoc($data_rubriky))
    $select_rubriky[$r['idStranky']] = $r['nazev']; 
        
$form = new FormFilter();
$form->set_class('filter2');
$form->add_text(THLEDAT, 'vyraz', "", 0, false, "", "vyraz");
$form->add_selectbox(TKATEGORIE,'rubrika',$idRubriky, $select_rubriky, 0, false,"","rubrika");
echo $form->get_html_code(main_tools($main_tools));

$table = new Table("tList");
$tr = $table->tr_head();
    $tr->add(TID, 'w25')
    ->add(TNAZEV)
    ->add(TKATEGORIE)
    ->add(TCENA_BEZ_DPH,"w110")
    ->add(TDPH,"w25")
    ->add(TCENA,"w110")
    ->add(TDISKUZE, 'w25')
    ->add(TPRIVATNI, 'w25')
    ->add(TAKTIVNI, 'w25')
    ->add(TAKCE,'w110');
echo $table->get_html();    

?>
<script type="text/javascript">
<!--

$(function(){
var oTable = $("#tList").dataTable({
    "sAjaxSource": "<?php echo AJAX_GATEWAY;?>product.list",
    "bSort" : false,
    "aoColumns": [
        {"sClass": "sl1"},{"sClass": "tleft sl2"},{"sClass": "tleft sl3"},{"sClass": "sl4 tright"},{"sClass": "sl4 tright"},{"sClass": "sl5 tright"},{"sClass": "sl6"},{"sClass": "sl8"},{"sClass": "sl8"},{"sClass": "akce"}
        ],
    "fnServerData": function ( sSource, aoData, fnCallback ) {
            aoData.push({ "name": "vyraz", "value": $("#vyraz").val() });
            aoData.push({ "name": "rubrika", "value": $("#rubrika option:selected").val() });
            $.getJSON( sSource, aoData, function (json) { fnCallback(json); } );
            }
 
})


$("select#rubrika").bind('change', function() {
        oTable.fnDraw(); 
        });

//prida k bunce th span
$(".dataTable th").each(function(){
    $(this).html("<span>" + $(this).html() + "</span>");
})

$(document).on("click","a.delete", function(){
        var id = $(this).attr('id').substring(6);
        var tr = $(this).parents("tr").first();
        var c = $.trim($(this).parents("tr").find("td").eq(1).text()); 
        
        var text = "<?php echo TOPRAVDU_SMAZAT;?>";

        custom_confirm(text.replace("%s",c) + '?', function(){
            $.msg();
            tr.addClass("deleted");
            $.post("<?php echo AJAX_GATEWAY;?>product.delete",{id: id},function(data){
                tr.fadeOut(function(){
                    $.msg( 'setClass', 'ok_message' );
                    $.msg( 'replace', '<?php echo OK_SMAZANO;?>' );
                    oTable.fnDraw();
                    $.msg( 'unblock', 1000);
                    });
                });
            
            })
        
        return false;
        
        
    });


$(document).on("mouseover","a.up",function(){
    var tr = $(this).parents("tr").first();

    tr.addClass("exchange_from");
    tr.removeClass("light");
    tr_prev = tr.prevAll().first();
    tr_prev.addClass("exchange_to");
    tr_prev.find("td").eq(1).addClass("exchange_down");
    tr.find("td").eq(1).addClass("exchange_up");
})

$(document).on("mouseout","a.up, a.down", function(){
    $("table#tList tr").removeClass("exchange_to");
    $("table#tList tr").removeClass("exchange_from");
    $("table#tList tr td").removeClass("exchange_down");
    $("table#tList tr td").removeClass("exchange_up");
})


$(document).on("mouseover","a.down",function(){
    var tr = $(this).parents("tr").first();
    tr.addClass("exchange_to");
    tr.removeClass("light");
    tr_next = tr.nextAll().first();
    tr_next.addClass("exchange_from");
    tr_next.find("td").eq(1).addClass("exchange_up");
    tr.find("td").eq(1).addClass("exchange_down");
})

$(document).on("click","a.up", function(){
    var id = $(this).attr('id').substring(2);
    $.post("<?php echo AJAX_GATEWAY;?>product.move.up",{id: id},function(data){
        oTable.fnDraw();
    })   
    
    return false;
})
   
$(document).on("click", "a.down", function(){
    var id = $(this).attr('id').substring(4);
    $.post("<?php echo AJAX_GATEWAY;?>product.move.down",{id: id},function(data){
        oTable.fnDraw();
    })
    
    return false;   
})





})

// -->
</script>
