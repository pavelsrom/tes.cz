<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined('SECURITY_CMS')) exit;
if(!$this->is_access('provize','edit-content',0,1,'content_view') &&
    !$this->is_access('provize','new',0,0,'content_add')
    ) return;

$idClanku = $this->get_id_action(); 

$main_tools = array();

if($idClanku > 0) {
    $record1 = $db->Query("SELECT SQL_CALC_FOUND_ROWS z.*, s.nazev AS stav, COUNT(p.id) AS pocet,
                    DATE_FORMAT(z.datum,'%d.%m.%Y %H:%i') AS datum
				FROM ".TABLE_PROVIZE_ZADOSTI." AS z
                LEFT JOIN ".TABLE_PROVIZE_ZADOSTI_STAVY." AS s ON s.id = z.id_stav
                LEFT JOIN ".TABLE_PROVIZE." AS p ON p.id_zadosti = z.id
                WHERE z.id = ".$idClanku."
				GROUP BY z.id
    			LIMIT 1");
			
    $page = $db->getAssoc($record1);
          
    }
    else
    {
    
    }

$main_tools[] = array(
    'ikona' => 'back', 
    "nazev" => TZPET, 
    "aktivni" => 0, 
    "odkaz" => $this->get_link('provize'), 
    "title" => TZPET_NA_SEZNAM
    );

echo main_tools($main_tools);

$data = $db->Query("SELECT id, nazev FROM ".TABLE_PROVIZE_ZADOSTI_STAVY." ORDER BY id");
$stavy = array(0=> "-- ".TVSECHNO." --");
while($sk = $db->getObject($data))
    $stavy[$sk->id] = $sk->nazev;



$form = new Form();
$form->add_section(TZAKLADNI_UDAJE, "home");

$form->add_plain_text(TID, $page['id']);
//$form->add_plain_text(TSTAV, $page['stav']);
$form->add_selectbox(TSTAV,'stav',$page['id_stav'],$stavy);
$form->add_plain_text(TJMENO_A_PRIJMENI, $page['jmeno_uzivatele']);
$form->add_plain_text('Účet uživatele', $page['ucet_uzivatele']);
$form->add_plain_text(TCELKEM, price($page['celkem']));
$form->add_plain_text(TVYTVORENO, $page['datum']);
$form->add_plain_text(TPOCET_PROVIZI, $page['pocet']);

$form->add_section("Výpis provizí", "home");

$data = $db->query("SELECT *, 
    IF(datum IS NULL,'',DATE_FORMAT(datum, '%d.%m.%Y')) AS datum 
    FROM ".TABLE_PROVIZE." 
    WHERE id_zadosti=".$idClanku."        
    ");


require_once("php/classes/class.table.php");
$table = new Table();
$tr = $table->tr_head()
    ->add(TDATUM,'center')
    ->add(TCELKEM,'tright')
    ->add("Úroveň provize",'tright')
    ->add(TSTAV,'w25 center')
    ->add("");



while($p = $db->getObject($data))
{
    $table->tr()
        ->add($p->datum,'center')
        ->add(price($p->castka),'tright')
        ->add($p->id_objednavka == 0 ? 'vložil admin' : constant("TTYP_PREDPLATNEHO_".$p->predplatne_typ)." / ".$p->stupen,'tright')
        ->add(ano_ne($p->aktivni),'w25')
        ->add("<a href='".$this->get_link('objednavky',0,'edit-content',$p->id_objednavka)."' target='_blank'>".icon('link','Zobrazit objednávku')."</a>","akce");
        
    
}

$form->add_code('<tr><td colspan="2">'.$table->get_html().'</td></tr>');

		
$res = ($login_obj->UserPrivilege('content_edit') && $idClanku>0) || ($login_obj->UserPrivilege('content_add') && $idClanku==0);

//$form->add_next_actions();
$form->add_submit()->set_disabled(!$res);
    
echo $form->get_html_code();

?>
