<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */


if(!defined('SECURITY_CMS')) exit;
if(!$this->is_access('kategorie','',0,0)) return;

$main_tools = array();
        
if($login_obj->UserPrivilege('settings_add'))
    $main_tools[] = array(
        'ikona' => 'new', 
        "nazev" => TPRIDAT_KATEGORII_PRODUKTU, 
        "aktivni" => 0, 
        "odkaz" => get_link('pages','stranky',0,"new",0,"produkty"));
	
$form = new FormFilter();
$form->add_text(THLEDAT, 'vyraz', "", 0, false, "", "vyraz");
echo $form->get_html_code(main_tools($main_tools));


$table = new Table("tList");
$table->tr_head()
    ->add(TID, 'w25')
    ->add(TNAZEV_KATEGORIE)
    ->add(TPOCET_PRODUKTU, 'w110')
    ->add(TPRIVATNI, 'w25')
    ->add(TAKTIVNI, 'w25')
    ->add(TAKCE, 'w90')
    ;
    
echo $table->get_html();

?>
<script type="text/javascript">
<!--

$(function(){
var oTable = $("#tList").dataTable({
    "sAjaxSource": "<?php echo AJAX_GATEWAY;?>category.list",
    "aoColumns": [
        {"sClass": "sl1"},{"sClass": "tleft sl2"},{"sClass": "tleft sl2"},{"sClass": "sl3", "bSortable": false},{"sClass": "sl4"},{"sClass": "akce", "bSortable": false}
        ],
    "fnServerData": function ( sSource, aoData, fnCallback ) {
            aoData.push({ "name": "vyraz", "value": $("#vyraz").val() });
            $.getJSON( sSource, aoData, function (json) { fnCallback(json); } );
            }
 
})

$("input#vyraz").bind('keyup change', function() {
        oTable.fnDraw(); 
        });

//prida k bunce th span
$(".dataTable th").each(function(){
    $(this).html("<span>" + $(this).html() + "</span>");
})

})

// -->
</script>
