<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */


if(!defined('SECURITY_CMS')) exit;
if(!$this->is_access('provize','',0,0)) return;

$main_tools = array();
        
$data = $db->Query("SELECT id, nazev FROM ".TABLE_PROVIZE_ZADOSTI_STAVY." ORDER BY id");
$stavy = array(0=> "-- ".TVSECHNO." --");
while($sk = $db->getObject($data))
    $stavy[$sk->id] = $sk->nazev;

	
$form = new FormFilter();
$form->set_class('filter2');
$form->add_text(THLEDAT, 'vyraz', "", 0, false, "", "vyraz");
$form->add_selectbox(TSTAV,'stav',0,$stavy,0,false,"","stav");
echo $form->get_html_code(main_tools($main_tools));


$table = new Table("tList");
$table->tr_head()
    ->add(TID, 'w110')
    ->add(TJMENO_A_PRIJMENI)
    ->add(TCELKEM, 'w110')
    ->add(TVYTVORENO, 'w110')
    ->add(TSTAV,'w110')
    ->add(TPOCET_PROVIZI,'w25')
    ->add(TAKCE, 'w110')
    ;
    
echo $table->get_html();

?>
<script type="text/javascript">
<!--

$(function(){
var oTable = $("#tList").dataTable({
    "sAjaxSource": "<?php echo AJAX_GATEWAY;?>provize.list",
    "aaSorting": [[ 0, "desc" ]],
    "aoColumns": [
        {"sClass": "sl1"},{"sClass": "tleft sl2", "bSortable": false},{"sClass": "tright sl2"},{"sClass": "sl3"},{"sClass": "sl3"},{"sClass": "sl4", "bSortable": false},{"sClass": "akce", "bSortable": false}
        ],
    "fnServerData": function ( sSource, aoData, fnCallback ) {
            aoData.push({ "name": "vyraz", "value": $("#vyraz").val() });
            aoData.push({ "name": "stav", "value": $("#stav option:selected").val() });
            $.getJSON( sSource, aoData, function (json) { fnCallback(json); } );
            }
 
})

$("input#vyraz, select#stav").bind('keyup change', function() {
        oTable.fnDraw(); 
        });

//prida k bunce th span
$(".dataTable th").each(function(){
    $(this).html("<span>" + $(this).html() + "</span>");
})

})

// -->
</script>
