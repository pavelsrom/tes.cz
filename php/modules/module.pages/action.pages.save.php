<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined('SECURITY_CMS')) exit;

if(!is_action() ||
    (!$this->is_access('stranky','edit-content',0,1,'content_edit') &&
     !$this->is_access('stranky','new',0,0,'content_add') &&
     !$this->is_access('404')
     ) || !$object_access->has_access($this->get_id_action())
    ) return;




$idRodice = get_int_post('idRodice');
$idStranky = $this->get_id_action();
$file = get_post('obrazek');

if($this->is_access('404'))
    $idStranky = $db->get(TABLE_STRANKY,"idStranky","idDomeny=".$domain->getId()." AND idJazyka=".WEB_LANG_ID." AND typ='404'");
				
$idVlastnika = $login_obj->getId();
$idGalerie = get_int_post('idGalerie');    
$idAnkety = get_int_post('idAnkety');
$diskuze = get_int_post('diskuze');
$obsah = get_post('obsah','',false);
$perex = get_post('perex','',false);
$idProfiluPanelu = get_int_post('idProfiluPanelu',-1);          
$idDomeny = $domain->getId();
$nazev = get_post('nazev');
$menu_nazev = get_post('menu_nazev');
$typ_stranky = get_post('typ','stranka');




$stary_typ_stranky = get_post('stary_typ');
$nadpis = get_post('nadpis');			
$url = get_post('url');
$menu_zobr = get_int_post('menu');
$stav = get_int_post('zobrazit',1);
$povolit_mazani = get_int_post('povolit_mazani',1);
$autentizace = get_int_post('autentizace');
$title = get_post('title');			
$description = get_post('description');		
       
$url = $idStranky==0 && $url=='' ? ModifyUrl($nazev) : ModifyUrl($url);

$url_new = get_url_nazev($idStranky, $url);
if($url!=$url_new)
    $systemMessage->add_warning(WARNING_URLNAZEV_MODIFIKOVAN." ".$url_new);
$url = $url_new;
    
if($url == "")
    $systemMessage->add_error(ERROR_NEPLATNY_URL_NAZEV);            

if(!TestLength($nazev, 200, false))
	$systemMessage->add_error(ERROR_NEPLATNY_NAZEV);
                
if(!TestLength($menu_nazev, 200, false) && $typ_stranky!='404' && $typ_stranky!='vyhledavani')
	$systemMessage->add_error(ERROR_NEPLATNY_NAZEV_PRO_MENU);
				
if(!TestLength($nadpis, 200, false))
	$systemMessage->add_error(ERROR_NEPLATNY_NADPIS);	
				
if(!TestLength($title, 100, true))
	$systemMessage->add_error(ERROR_NEPLATNY_TITULEK);
				
if(!TestLength($description, 200, true))
	$systemMessage->add_error(ERROR_NEPLATNY_DESCRIPTION);
							
if(!TestUrlPage($url) || ($url=='' && $idStranky > 0)) 
	$systemMessage->add_error(ERROR_NEPLATNY_URL_NAZEV);
          	
				
if($systemMessage->error_exists()) return;

				
if($login_obj->UserPrivilege('set_privilege') && $idStranky>0){
					
    $db->delete(TABLE_STRANKY_PRIVATNI_UZIVATELE,"idStranky=".$idStranky);

    $uzivatele = get_array_post('uzivatele');
					
    $values = array();

    if(count($uzivatele)>0){
        foreach($uzivatele AS $id)
			$values[] = "(".$idStranky.",".$id.")";
						
		$db->Query("INSERT INTO ".TABLE_STRANKY_PRIVATNI_UZIVATELE." (idStranky, idUzivatele) VALUES ".implode(',', $values));  
		}			
	}

$update = array(
    "nazev" => $nazev,
    "idDomeny" => $idDomeny,
    "nazevProMenu" => $menu_nazev,
    "nadpis" => $nadpis,
    "idRodice" => $idRodice,
    "zobrazit" => $stav,        
    "povolit_mazani" => $povolit_mazani,
    "autentizace" => $autentizace,
    "doMenu"    => $menu_zobr,
    "idProfiluPanelu" => $idProfiluPanelu,
    "typ"   => $typ_stranky,
    "idGalerie" => $idGalerie,
    "idAnkety" => $idAnkety,
    "diskuze" => $diskuze,
    "obsah" => $obsah,
    "obsahBezHtml" => strip_tags($obsah),
    "perex" => $perex,
    "perexBezHtml" => strip_tags($perex),
    "idJazyka" => WEB_LANG_ID
    );	
    
if(is_post('title'))
    $update['title'] = $title;
if(is_post('description'))
    $update['description'] = $description;
if(is_post('url'))
    $update['url'] = $url;   		
			
if($idStranky > 0){		
    $update['idAktualizoval'] = $login_obj->getId();
    $update['jmenoAktualizoval'] = $login_obj->getName();
    $update['datumAktualizoval'] = "now";
    $db->update(TABLE_STRANKY,$update,"idStranky=".$idStranky." AND idDomeny=".$idDomeny);

	} 
	else 
	{
    $update['idVytvoril'] = $login_obj->getId();
    $update['jmenoVytvoril'] = $login_obj->getName();
    $update['datumVytvoril'] = "now";
    
    $update['idAktualizoval'] = $login_obj->getId();
    $update['jmenoAktualizoval'] = $login_obj->getName();
    $update['datumAktualizoval'] = "now";
    
    $update['priorita'] = $db->get(TABLE_STRANKY, "MAX(priorita) + 1","idRodice = ".$idRodice);
    
	$result = $db->insert(TABLE_STRANKY,$update);
    $idStranky = $db->lastId();
    $object_access->add_allowed_object($idStranky);
	}


//nahravani hlavniho obrazku
if($file == '' && isset($_FILES['obrazek']) && $_FILES['obrazek']['error']!=4){
    $upload = $_FILES['obrazek'];
	$news_path = $domain->getDir().USER_DIRNAME_STRANKY;
                    
    include_once('php/classes/class.upload.php');
                    
    $uploader = new upload($_FILES['obrazek'], 'cs_CS');
    $uploader->file_new_name_body = $idStranky;
    $uploader->file_overwrite = true;
    $uploader->image_resize = true;
    $uploader->image_x = intval(OBRAZEK_STRANKY_X);
    $uploader->image_y = intval(OBRAZEK_STRANKY_Y);
    $uploader->image_ratio = true;
    $uploader->image_convert = 'jpg';
    $uploader->process($news_path);
	$uploader->clean();
    $file = $idStranky.".jpg";
                    
    $db->update(TABLE_STRANKY,array("obrazek" => $file),"idDomeny=".$domain->getId()." AND idStranky=".$idStranky." LIMIT 1");
				
}


//ulozeni popisku fotek
$popisky = get_array_post("fotka");
$i=0;
foreach($popisky AS $idFotky => $p)
{
    $update = array(
        "popis" => $p,
        "priorita" => $i
        );
    
    $db->update(TABLE_STRANKY_FOTKY,$update,"id = ".$idFotky." AND idStranky=".$idStranky);
    $i++;
}

//ulozeni odkazu na videa
$file_urls = get_array_post("file_url");
$file_desc = get_array_post("file_desc");

$db->delete(TABLE_STRANKY_ODKAZY,"WHERE idStranky=".$idStranky);
if(count($file_urls)>0)
{
    foreach($file_urls AS $il => $url)
    {
        $desc = $file_desc[$il];

        if($url == '')
        {
            if($desc == '')
                continue;
            
            if(!TestHttpUrl($url)) 
                $systemMessage->add_error(ERROR_NEPLATNE_URL_ODKAZU_NA_VIDEO);
                          
                            
            if($systemMessage->error_exists()) 
                return;
        }



        $insert = array("idStranky" => $idStranky, "url" => $url, "popis" => $desc, "priorita" => $il);
        $db->insert(TABLE_STRANKY_ODKAZY,$insert);
    }
}




CreateTypePage($idStranky, $typ_stranky, $stary_typ_stranky);

$skupiny = get_array_post('skupiny');
$db->delete(TABLE_STRANKY_SKUPINY,"WHERE idStranky=".$idStranky);
if(count($skupiny)>0)
{
    foreach($skupiny AS $s)
    {
        $insert = array("idStranky" => $idStranky, "idSkupiny" => $s);
        $db->insert(TABLE_STRANKY_SKUPINY,$insert);
    }
}

//vytvoreni zalohy obsahu
if(MODULE_CONTENT_BACKUP)
{
    $content_backup = new ContentBackup($idDomeny,$idStranky,$idVlastnika);
    $content_backup->AddStore($obsah);	
}


$chybne_odkazy_indik = false;

if(MODULE_LINK_CHECKER)
{
    $chybne_odkazy = new LinksChecker($domain->getId(), $idStranky, "stranka", $obsah, false, KONTROLOVAT_ODKAZY_PRI_UKLADANI);
    $chybne_odkazy->set_base_url( $domain->getUrl() );
    $chybne_odkazy->process();
    $pocet_chybnych_obrazku = $chybne_odkazy->get_sum_error_images();
    $pocet_chybnych_odkazu = $chybne_odkazy->get_sum_error_links();
    
    if($pocet_chybnych_obrazku > 0)
        $systemMessage->add_warning(TPOCET_CHYBNYCH_OBRAZKU_NA_STRANCE.": ".$pocet_chybnych_obrazku);
        
    if($pocet_chybnych_odkazu > 0)
        $systemMessage->add_warning(TPOCET_CHYBNYCH_ODKAZU_NA_STRANCE.": ".$pocet_chybnych_odkazu);
    
    if(!$chybne_odkazy_indik && ($pocet_chybnych_obrazku > 0 || $pocet_chybnych_odkazu > 0)) 
        $chybne_odkazy_indik = true;
        
                
}

$log->add_log($this->get_url('id_action') == 0 ? 'create':'edit-content','stranka',$idStranky,$nazev);


if($chybne_odkazy_indik)
    $systemMessage->add_warning(TODKAZY_ZKONTROLUJTE_A_OPRAVTE);


$systemMessage->add_ok(OK_ULOZENO);
					
if($this->is_access('404'))
    $url = $this->get_link('404');
else
{
    set_next_action_code();
    $dalsi_akce = get_next_action_code();
    if($dalsi_akce == 0)
        $url = $this->get_link('stranky');
    elseif($dalsi_akce == 1)
        $url = $this->get_link('stranky',0,'edit-content',$idStranky);
    elseif($dalsi_akce == 2)
        $url = $this->get_link('stranky',0,'new');
}

Redirect($url);	



?>