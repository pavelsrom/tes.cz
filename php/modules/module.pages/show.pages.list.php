<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined('SECURITY_CMS')) exit;
if(!$this->is_access('stranky')) return;

$main_tools = array();       
if($login_obj->UserPrivilege('settings_add'))
    $main_tools[] = array(
        'ikona' => 'new', 
        "nazev" => TPRIDAT_STRANKU, 
        "aktivni" => 0, 
        "odkaz" => $this->get_link('stranky',0,'new'));
        
$form = new FormFilter();
$form->add_text(THLEDAT, 'vyraz', "", 0, false, "", "vyraz");
echo $form->get_html_code(main_tools($main_tools));


$table = new Table("tList");
$table->tr_head()
    ->add(TID, 'w25')
    ->add(TNAZEV)
    ->add(TZANORENI,'tleft w25')
    ->add(TTYP,'tleft w90')
    ->add(TV_MENU,'w25')
    ->add(TPRIVATNI,'w25')
    ->add(TAKTIVNI,'w25')
    //->add(TPOSUN,'w70')
    ->add(TAKCE,'w110');

echo $table->get_html();

?>

<script type="text/javascript">
<!--


$(function(){
    
var oTable = $("#tList").dataTable({
    "bSortClasses": false,
    "bSort" : false,
    "sAjaxSource": "<?php echo AJAX_GATEWAY;?>pages.list",
    "aoColumns": [
        {"sClass": "sl1"},{"sClass": "sl2 tleft"},{"sClass": "sl3 tleft"},{"sClass": "sl3 tleft"},{"sClass": "sl4"},{"sClass": "sl5"},{"sClass": "sl6"},{"sClass": "sl8 akce"}
        ],
    "fnServerData": function ( sSource, aoData, fnCallback ) {
            aoData.push({ "name": "vyraz", "value": $("#vyraz").val() });
            $.getJSON( sSource, aoData, function (json) { fnCallback(json); } );
            }
 
})

$("input#vyraz").bind('keyup change', function() {
        oTable.fnDraw(); 
        });

//prida k bunce th span
$(".dataTable th").each(function(){
    $(this).html("<span>" + $(this).html() + "</span>");
})

$(".filter2 select").bind('change', function() {
    oTable.fnDraw();
    });


function get_level_class(cl)
{
    for(var i=0; i<cl.length; i++)
    {
        var u = cl[i].substring(0,6);
        cl_uroven = u == "uroven" ? cl[i] : "";
        if(cl_uroven != "") 
            return cl_uroven;
    }
    
    return "";
}

$(document).on("mouseover","a.up",function(){
    var tr = $(this).parents("tr").first();
    var cl = tr.attr("class").split(" ");
    var cl_uroven = get_level_class(cl);        
    if(cl_uroven == "") return;
    tr.addClass("exchange_from");
    tr.removeClass("light");
    tr_prev = tr.prevAll("." + cl_uroven).first();
    tr_prev.addClass("exchange_to");
    tr_prev.find("td").eq(1).addClass("exchange_down");
    tr.find("td").eq(1).addClass("exchange_up");
})

$(document).on("mouseout", "a.up, a.down", function(){
    $("table#tList tr").removeClass("exchange_to");
    $("table#tList tr").removeClass("exchange_from");
    $("table#tList tr td").removeClass("exchange_down");
    $("table#tList tr td").removeClass("exchange_up");
})


$(document).on("mouseover","a.down", function(){
    var tr = $(this).parents("tr").first();
    var cl = tr.attr("class").split(" ");
    var cl_uroven = get_level_class(cl);        
    if(cl_uroven == "") return;
    tr.addClass("exchange_to");
    tr.removeClass("light");
    tr_next = tr.nextAll("." + cl_uroven).first();
    tr_next.addClass("exchange_from");
    tr_next.find("td").eq(1).addClass("exchange_up");
    tr.find("td").eq(1).addClass("exchange_down");
})

$(document).on("click", "a.up", function(){
    var id = $(this).attr('id').substring(2);
    $.post("<?php echo AJAX_GATEWAY;?>page.move.up",{id: id},function(data){
        oTable.fnDraw();
    })   
    
    return false;
})
   
$(document).on("click", "a.down", function(){
    var id = $(this).attr('id').substring(4);
    $.post("<?php echo AJAX_GATEWAY;?>page.move.down",{id: id},function(data){
        oTable.fnDraw();
    })
    
    return false;   
})

$(document).on("click", "a.delete", function(){
        var id = $(this).attr('id').substring(6);
        var tr = $(this).parents("tr").first();
        var c = $.trim($(this).parents("tr").find("td").eq(1).text()); 
        
        var text = "<?php echo TOPRAVDU_SMAZAT;?>";

        custom_confirm(text.replace("%s",c) + '?', function(){
            $.msg();
            tr.addClass("deleted");
            $.post("<?php echo AJAX_GATEWAY;?>page.delete",{id: id},function(data){
                if(data.messages.error == "")
                {
                    tr.fadeOut(function(){
                    $.msg( 'setClass', 'ok_message' );
                    $.msg( 'replace', data.messages.ok );
                    oTable.fnDraw();
                    $.msg( 'unblock', 500);
                    });
                }
                else
                {
                    $.msg( 'setClass', 'error_message' );
                    $.msg( 'replace', data.messages.error );
                    $.msg( 'unblock', 2000);
                }
                
                },'json');
            
            })
        
        return false;
        
        
    });
    
    
$(document).on("click", "a.plus, a.minus", function(){
    
    var cl = $(this).attr('class');
    
    var id = $(this).attr('id').substring(cl=="plus" ? 4 : 5);
    
    
    $.post("<?php echo AJAX_GATEWAY;?>pages.plus.minus",{id:id,typ: cl},function(data){
        oTable.fnDraw();
           
    })
    return false;
    
})
    

})

// -->
</script>
