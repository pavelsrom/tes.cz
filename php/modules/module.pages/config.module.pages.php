<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */


//------------------------------------------------
//definice polozek menu
//------------------------------------------------

$module_menu = array();

$module_menu[] = array(
    "page"  => "stranky",
    "name"  => TSTRANKY." ".WEB_LANG
    );
    
if($login_obj->UserPrivilege('settings_view'))
$module_menu[] = array(
    "page"  => "404",
    "name"  => TCHYBOVA_STRANKA." ".WEB_LANG
    );       
    
$module_menu[] = array(
    "page"  => "nastaveni",
    "name"  => TNASTAVENI
    );

//------------------------------------------------
//definice nadpisu h1 a textu k nadpisu
//------------------------------------------------ 
$module_h1 = array();

$module_h1[] = array(
    "page"  => "stranky",
    "action"=> "edit-content",
    "verify_id_action" => 1,
    "name"  => TEDITACE_STRANKY." ".WEB_LANG
    );
$module_h1[] = array(
    "page"  => "stranky",
    "action"=> "new",
    "name"  => TNOVA_STRANKA." ".WEB_LANG
    );    
$module_h1[] = array(
    "page"  => "stranky",
    "name"  => TSTRANKY." ".WEB_LANG,
    "text"  => 33
    ); 

$module_h1[] = array(
    "page"  => "nastaveni",
    "name"  => TNASTAVENI
    );
$module_h1[] = array(
    "page"  => "404",
    "name"  => TCHYBOVA_STRANKA." ".WEB_LANG,
    "text"  => 17
    );

$module_h1[] = array(
    "page"  => "nastaveni",
    "name"  => TNASTAVENI
    );

?>