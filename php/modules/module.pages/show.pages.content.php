<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */


if(!defined('SECURITY_CMS')) exit;

if((!$this->is_access('stranky','edit-content',0,1,'content_view') &&
    !$this->is_access('stranky','new',0,0,'content_add') &&
    !$this->is_access('404')
    )
    ) return;

$idPage = $this->get_id_action();

if(!$object_access->has_access($idPage))
{
    $systemMessage->add_warning(TNEMATE_OPRAVNENI_MENIT_OBSAH);
    $systemMessage->show_messages();
}


 
if($this->is_access('404'))
    $idPage = $db->get(TABLE_STRANKY,"idStranky","idDomeny=".$domain->getId()." AND idJazyka=".WEB_LANG_ID." AND typ='404'");


$main_tools = array();

if($idPage > 0) {
    $record1 = $db->Query("SELECT s.*, j.jazyk,
				IF(s.nadpis='', s.nazev, s.nadpis) AS nadpis
			FROM ".TABLE_STRANKY." AS s
            LEFT JOIN ".TABLE_JAZYKY." AS j ON s.idJazyka = j.idJazyka 
			WHERE idStranky=".$idPage." 
				AND idDomeny=".$domain->getId()."
                AND s.idJazyka=".WEB_LANG_ID."
			LIMIT 1");
			
			
   $page = $db->getAssoc($record1);
			
            
   if($login_obj->UserPrivilege('content_view'))
        $main_tools[] = array(
            'ikona' => 'preview', 
            "nazev" => TNAHLED, 
            "aktivni" => 0, 
            "odkaz" => $links->get_url($idPage),
            "target" => "blank"); 

    
    }
    else
    {
    $page['nazev'] = "";
    $page['nadpis'] = "";
    $page['obrazek'] = "";
    $page['uvodni'] = 0;
    $page['autentizace'] = 0;
    $page['idRodice'] = 0;
    $page['idProfiluPanelu'] = 0;
    $page['zobrazit'] = 1;
    $page['doMenu'] = 1;
    $page['obsah'] = "";
    $page['perex'] = "";
    $page['idGalerie'] = 0;
    $page['idAnkety'] = 0;
    $page['diskuze'] = 0;
    $page['url'] = "";
    $page['title'] = "";
    $page['description'] = "";
    $page['nazevProMenu'] = "";
    $page['povolit_mazani'] = 1;
    $page['typ'] = "stranka"; 
    
    $preneseny_typ = $this->get_value();
    
    $page['typ'] = $preneseny_typ == 'clanky' && MODULE_ARTICLES ? $preneseny_typ : $page['typ'];
    $page['typ'] = $preneseny_typ == 'produkty' && MODULE_ESHOP ? $preneseny_typ : $page['typ'];
   
            
    }

$main_tools[] = array(
        'ikona' => 'back', 
        "nazev" => TZPET, 
        "aktivni" => 0, 
        "odkaz" => $this->get_link('stranky'), 
        "title" => TZPET_NA_SEZNAM_STRANEK);
        
echo main_tools($main_tools);          

$typ = array(
    'stranka' => TSTANDARDNI_STRANKA, 
    "prazdna-stranka" => TPRAZDNA_STRANKA
    );



    

$form = new Form();
$form->allow_upload();


//osetreni vypnuteho modulu Clanky
if(MODULE_ARTICLES)
{
    $typ["clanky"] = TSTRANKA_S_CLANKY;
}
else
{
    if($page['typ'] == 'clanky')
        $form->add_hidden('typ','clanky');   
}

if(MODULE_ESHOP)
{
    $typ["produkty"] = TSTRANKY_S_PRODUKTY;
}
else
{
    if($page['typ'] == 'produkty')
        $form->add_hidden('typ','produkty');   
}




$form->add_section(TZAKLADNI_UDAJE, "home");
$form->add_text(TNAZEV, 'nazev', $page['nazev'], 0, true, "", "nazev");        	

        
if($page['typ']!='404')
    $form->add_text(TNAZEV_STRANKY_V_MENU, 'menu_nazev', $page['nazevProMenu'], 0, true, "", "menu_nazev"); 

$form->add_text(THLAVNI_NADPIS_STRANKY, 'nadpis', $page['nadpis'], 3, true, "", "nadpis")->set_class_tr("prazdna-stranka-hidden"); 

if($idPage>0)
    $form->add_hidden('stary_typ',$page['typ']);
    

        
if($page['typ']!='404'){
            
    //overeni zda uz stranka s novinkami nebo galerii existuje - muze byt vytvorena jen jedna stranka s novinkami
    $dis_novinky = false;
    $dis_galerie = false;
    $dn = $db->Query("SELECT typ FROM ".TABLE_STRANKY." WHERE idDomeny=".$domain->getId()." AND (typ='novinky' OR typ='stranka-galerie') AND idJazyka=".WEB_LANG_ID);
            
    while($dna = $db->getAssoc($dn)){
        if($dna['typ']=='novinky') $dis_novinky = true;
        if($dna['typ']=='stranka-galerie') $dis_galerie = true;
        }
    
                
    $dis_clanky = false;
    if($idPage>0 && $page['typ']=='clanky'){
        $dc = $db->Query("SELECT idStranky FROM ".TABLE_STRANKY." WHERE idDomeny=".$domain->getId()." AND typ='clanek' AND idRodice=".$idPage." AND idJazyka=".WEB_LANG_ID." LIMIT 1");
                
        if($db->numRows($dc)>0)
            $dis_clanky = true;
                
        }
        
    $dis_produkty = false;
    if($idPage>0 && $page['typ']=='produkty'){
        $dc = $db->Query("SELECT idStranky FROM ".TABLE_STRANKY." WHERE idDomeny=".$domain->getId()." AND typ='produkt' AND idRodice=".$idPage." AND idJazyka=".WEB_LANG_ID." LIMIT 1");
                
        if($db->numRows($dc)>0)
            $dis_produkty = true;
                
        }
                 
    $radiobutton_typy = "";       
    if(in_array($page['typ'], array_keys($typ))){
        $i = 0;
        foreach($typ AS $n => $t){
            if($n == $page['typ']) $ch = "checked='checked'"; else $ch = "";
            $dis = "";
            if($n != 'clanky' && $dis_clanky) $dis = "disabled='disabled'";
            if($n != 'produkty' && $dis_produkty) $dis = "disabled='disabled'";  
             
            $radiobutton_typy .= "<input type='radio' class='radio typ_stranky' name='typ' value='".$n."' ".$ch." ".$dis." id='typ".$n."' /><label for='typ".$n."'>".$t."</label><br />";
            $i++;
            }
        $form->add_plain_text(TTYP, $radiobutton_typy ,137);
        }
        else
        {
        if($page['typ'] == 'archiv-clanku') $t = TARCHIV_CLANKU;
        elseif($page['typ'] == 'stranka-galerie') $t = TSTRANKA_S_GALERII;
        elseif($page['typ'] == 'prazdna-stranka') $t = TPRAZDNA_STRANKA;
        elseif($page['typ'] == 'sitemap') $t = TMAPA_WEBU;
        elseif($page['typ'] == 'obchodni-podminky') $t = TSTRANKA_OBCHODNI_PODMINKY;
        elseif($page['typ'] == 'objednavka') $t = TSTRANKA_OBJEDNAVKA;
        elseif($page['typ'] == 'novinky') $t = TNOVINKY;
        elseif($page['typ'] == 'stranka') $t = TSTANDARDNI_STRANKA;
        elseif($page['typ'] == 'clanky') $t = TCLANKY;
        elseif($page['typ'] == 'uvodni') $t = TUVODNI_STRANKA;
        elseif($page['typ'] == 'kalendar-akci') $t = TKALENDAR_AKCI;
        elseif($page['typ'] == 'tagy') $t = TSTRANKA_STITKY;
        elseif($page['typ'] == 'vyhledavani') $t = TSTRANKA_S_VYSLEDKY_VYHLEDAVANI;
        elseif($page['typ'] == '404') $t = TCHYBOVA_STRANKA;
        elseif($page['typ'] == 'email') $t = TSTRANKA_ODBER_NOVINEK;
        elseif($page['typ'] == 'produkty') $t = TSTRANKY_S_PRODUKTY;
        elseif($page['typ'] == 'login') $t = TPRIHLASOVACI_STRANKA;
        else
            $t = TSTANDARDNI_STRANKA;
            
        $help = "";
        
        $form->add_hidden('typ',$page['typ']);
        $form->add_plain_text(TTYP, $t, $help);            
        }
        
    
    //umisteni stranek
    $selectbox_pages = array(0 => "-- ".TKOREN_WEBU." --") + get_pages($idPage);
    
    
    $form->add_selectbox(TUMISTENI, 'idRodice', $page['idRodice'], $selectbox_pages,298);
    

    
    
    
    
    $form->add_radiobuttons(TZARADIT_DO_MENU, 'menu', $page['doMenu'], array(1 => TANO, 0 => TNE), 106);
           
    /*        
    if(MODULE_PRIVATE_PAGES)
        $form->add_radiobuttons(TPRIVATNI_STRANKA, 'autentizace', $page['autentizace'], array(1 => TANO, 0 => TNE), 5)->set_class_tr("prazdna-stranka-hidden");
        else
        $form->add_hidden('autentizace',$page['autentizace']);
    */
    		
    if($login_obj->minPrivilege("admin") && !in_array($page['typ'], array('uvodni','404')))
        $form->add_radiobuttons(TPOVOLIT_MAZANI, 'povolit_mazani', $page['povolit_mazani'], array(1 => TANO, 0 => TNE));
        else
        $form->add_hidden('povolit_mazani',$page['povolit_mazani']);

            
    if($page['typ']!='uvodni')
        $form->add_radiobuttons(TZOBRAZOVAT, 'zobrazit', $page['zobrazit'], array(1 => TANO, 0 => TNE), 6);
                
    }           
    elseif($page['typ']=='404'){
        $form->add_hidden('typ',$page['typ']);
        $form->add_hidden('idRodice',0);
        }

//nahravani_hlavniho_obrazku($idPage, $form);

$form->add_section(TOBSAH, "content")->set_class_tr("prazdna-stranka-hidden");
//$form->add_editor(TPEREX, 'perex', $page['perex'], 0, false, "wysiwygEditor h300 w100")->allow_wet()->set_class_tr("prazdna-stranka-hidden");
$form->add_editor(TOBSAH, 'obsah', $page['obsah'], 0, false, "wysiwygEditor h700 w100")->allow_wet()->set_class_tr("prazdna-stranka-hidden");

$form->add_file(TNAHRAT_OBRAZEK, 'obrazek', false, 85,"","obrazek_input")->set_disabled($page['obrazek']!='');
        
$image_class = "";
if($page['obrazek']=='' || $page['obrazek']==null)
{
    $img = TNENI_ULOZEN_ZADNY_OBRAZEK;
}
else
{	
    $file = $domain->getDir().USER_DIRNAME_STRANKY.$page['obrazek'];
				
    if(file_exists($file)) 
    {
        $src = RELATIVE_PATH.$file."?".time(); 
        $img = "<img src='".$src."' alt='".$page['obrazek']."' title='".$page['obrazek']."'  style='min-width: 300px;'/><br /><br />";
        $image_class = "image";
        if($login_obj->UserPrivilege('content_delete') && $object_access->has_access($idPage))
                $img .= tlacitko('#',TSMAZAT,"","times","delete photo-delete","delete".$idPage);
                else
                $img .= icon_disabled('delete');
            }
            else 
            {
            $src="";
            $img = TOBRAZEK_NENALEZEN;
            if($login_obj->UserPrivilege('content_delete'))
                $img .= tlacitko('#',TSMAZAT,"","times","delete photo-delete","delete".$idPage);
                else
                $img .= icon_disabled('delete');
                }
}

$form->add_plain_text(TOBRAZEK, $img)->set_class_td($image_class." relative tleft");

//vypis galerie - dalsi fotky k strance
show_photos($idPage, $form);
show_links($idPage, $form);

        
if($page['typ']!='404' && $page['typ']!='vyhledavani' && MODULE_GALLERY){
            
    $sel = ShowSelectBoxGallery();
    $sel = array(0 => "-- ".TVYBERTE." --") + $sel;

    if(count($sel) == 1){
        $text_neni = "";
        if($login_obj->UserPrivilege('content_add'))
            $text_neni .= tlacitko(get_link('gallery',"galerie",0,"new"));
        $form->add_plain_text(TGALERIE, $text_neni)->set_class_tr("prazdna-stranka-hidden");

        }
        else
        $form->add_selectbox(TGALERIE, 'idGalerie', $page['idGalerie'], $sel)->set_class_tr("prazdna-stranka-hidden");          
    }
    else
    $form->add_hidden('idGalerie', $page['idGalerie']);
    
  
if($page['typ']!='404' && $page['typ']!='vyhledavani' && MODULE_INQUIRY){
    $d = $db->Query("SELECT idAnkety AS id, nazev FROM ".TABLE_ANKETY." WHERE idDomeny=".$domain->getId());
    
    $sel = array(0 => "-- ".TVYBERTE." --");
    $pocet = $db->numRows($d);
            
    while($s = $db->getAssoc($d))
        $sel[$s['id']] = $s['nazev'];
                
    if(count($sel) == 1){
        $text_neni = "";
        if($login_obj->UserPrivilege('content_add'))
            $text_neni .= tlacitko(get_link('inquiry',"ankety",0,'new'));
        
        $form->add_plain_text(TANKETA, $text_neni)->set_class_tr("prazdna-stranka-hidden");    
        }
        else
        $form->add_selectbox(TANKETA, 'idAnkety', $page['idAnkety'], $sel)->set_class_tr("prazdna-stranka-hidden");
            
    }
    else
    $form->add_hidden('idAnkety', $page['idAnkety']);
    
        
if($page['typ']!='404' && $page['typ']!='vyhledavani' && MODULE_DISCUSSION)
    $form->add_radiobuttons(TDISKUZE, 'diskuze', $page['diskuze'], array(1 => TANO, 0=>TNE))->set_class_tr("prazdna-stranka-hidden");
    else
    $form->add_hidden('diskuze', $page['diskuze']);
   

$data_panely = $db->Query("SELECT idProfilu,nazev FROM ".TABLE_PANELY_SABLONY." WHERE idDomeny=".$domain->getId()." AND zobrazit=1 AND idJazyka=".WEB_LANG_ID." ORDER BY vychozi DESC ");
                
$profily = array();
while($profil = $db->getAssoc($data_panely))
    $profily[$profil['idProfilu']] = $profil['nazev'];

/*
$form->add_section(TSABLONA_PANELU, 'panel-setting')->set_class_tr("prazdna-stranka-hidden");
$form->add_selectbox(TSABLONA_PANELU, 'idProfiluPanelu', $page['idProfiluPanelu'], $profily, 135)->set_class_tr("prazdna-stranka-hidden"); 
;
*/         


/*
if($page['typ']!='404')
    show_private_pages_settings($idPage, $page, $form);
*/


if($login_obj->UserPrivilege('seo') && $page['typ']!='404')
{
    $form->add_section(TSEO_PARAMETRY, 'seo', "", 8);
    $form->add_text(TURL_NAZEV, 'url', $page['url'], 9, $idPage > 0);
    $form->add_text(TTITLE, 'title', $page['title'], 10,false,'','title');
    $form->add_textarea(TDESCRIPTION, 'description', $page['description'], 11);	
}
else
{
    $form->add_hidden('url', $page['url']);
    $form->add_hidden('title', $page['title'], 'title');
    $form->add_hidden('description', $page['description']);	
}

        
$res = (($login_obj->UserPrivilege('content_edit') && $idPage>0) || ($login_obj->UserPrivilege('content_add') && $idPage==0)) && $object_access->has_access($idPage);

if($page['typ']!='404')
    $form->add_next_actions();
    

    
$form->add_submit()->set_disabled(!$res);
//$form->set_jquery_ui_css();

echo $form->get_html_code();

?>


<script type="text/javascript">
<!--

$(function(){
    
    function zobraz_skryj_inputy_prazdne_stranky()
    {
        var v = $("input.typ_stranky:checked").val();
        
        if(v == 'prazdna-stranka')
            $(".prazdna-stranka-hidden").hide();
            else
            $(".prazdna-stranka-hidden").show();
        
        $("table.table2").each(function(){
            
            var L1 = $(this).find("tr").not(".mceLayout tr, .we-tools tr").length;
            var L2 = $(this).find("tr.prazdna-stranka-hidden").length;
            
            if(L1 == L2)
                v == 'prazdna-stranka' ? $(this).hide() : $(this).show();
        })
    }
    
    
    $("input.typ_stranky").click(zobraz_skryj_inputy_prazdne_stranky);
    zobraz_skryj_inputy_prazdne_stranky();    
    

    $("a.delete").click(function(){
        var id = $(this).attr('id').substring(6);
        
        var text = "<?php echo TOPRAVDU_SMAZAT_TENTO_OBRAZEK;?>";
        var td = $(this).parents("td.image").first();
        
        custom_confirm(text, function(){
            $.msg();
            td.addClass("deleted");
            $.post("<?php echo AJAX_GATEWAY;?>page.image.delete",{id: id},function(data){
                $.msg( 'setClass', 'ok_message' );
                $.msg( 'replace', '<?php echo OK_SMAZANO;?>' );
                $.msg( 'unblock', 1000);
                td.html("<?php echo TNENI_ULOZEN_ZADNY_OBRAZEK;?>");
                $("#obrazek_input").removeAttr('disabled');
                td.removeAttr("class");
                });
            
            })
            
        return false;
        
    })
    
    var ids = <?php echo $idPage;?>;
    $('#nazev').change(function(){
        if(ids > 0) return;
        var n = $(this).val();
        $('#menu_nazev, #nadpis, #title').val(n);        
        });

    
    
    
})


// -->
</script>