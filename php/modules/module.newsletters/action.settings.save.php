<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined('SECURITY_CMS')) exit;
if(!is_action() || !$this->is_access('nastaveni','',0,0,'settings_edit')) return;

save_settings($this->get_id());

$log->add_log('edit-settings','newsletter-nastaveni');

$systemMessage->add_ok(OK_ULOZENO);
Redirect($this->get_link('nastaveni'));

?>