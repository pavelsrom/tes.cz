<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined('SECURITY_CMS')) exit;
if(!is_action() || 
    (!$this->is_access('prijemci','new',0,0,'settings_add') && 
    !$this->is_access('prijemci','edit-settings',0,1,'settings_edit'))
    ) return;
    
$idPrijemce = $this->get_id_action();
$jmeno = get_post('jmeno');
$prijmeni = get_post('prijmeni');
$spolecnost = get_post('spolecnost');
$osloveni = get_post('osloveni');
$email = strtolower(get_post('email'));
$poznamka = get_post('poznamka');
$pohlavi = get_int_post('pohlavi');
$stav = get_int_post('stav');	
$skupiny = get_array_post('skupiny');
$jazyk = get_int_post("jazyk");	
			
//testovani duplicity emailu
$duplicita = $db->Query("SELECT email FROM ".TABLE_EMAILY_PRIJEMCI." WHERE email LIKE '".$email."' AND idPrijemce != ".$idPrijemce." AND idDomeny=".$domain->getId()." LIMIT 1");	
	
if($db->numRows($duplicita)!=0)
    $systemMessage->add_error(ERROR_EMAIL_EXISTUJE);
				
if(!TestEmail($email, 250, false)) 
    $systemMessage->add_error(ERROR_EMAIL);
	
if(count($skupiny) == 0)
    $systemMessage->add_error(ERROR_ZADNE_SKUPINY);
    		
if($systemMessage->error_exists()) 
    return;
			
//ZAPSANI DO DATABAZE

$update = array(
    "jmeno" => $jmeno,
    "prijmeni" => $prijmeni,
    "email" => $email,
    "osloveni" => $osloveni,
    "spolecnost" => $spolecnost,
    "pohlavi" => $pohlavi,
    "stav" => $stav,
    "poznamka" => $poznamka,
    "idJazyka" => $jazyk
    );

if($idPrijemce > 0){
	
    $db->update(TABLE_EMAILY_PRIJEMCI, $update, "idPrijemce=".$idPrijemce." AND idDomeny=".$domain->getId());

	}
	else
	{
	   
    $update["idDomeny"] = $domain->getId();
    $update["datum"] = "now";
    $update["hash"] = sha1(uniqid());

    $db->insert(TABLE_EMAILY_PRIJEMCI, $update);
	$idPrijemce = $db->lastId();
	}
          
//ulozeni skupin
$db->query("DELETE FROM ".TABLE_EMAILY_SKUPINY_PRIJEMCI." WHERE idPrijemce = ".$idPrijemce);                
if(count($skupiny)>0)
{
	$res_arr = array();
	foreach($skupiny AS $s)
        $db->insert(TABLE_EMAILY_SKUPINY_PRIJEMCI, array("idSkupiny" => $s, "idPrijemce" => $idPrijemce));
		
}

$log->add_log($this->get_url('id_action') == 0 ? 'create':'edit-settings','newsletter-prijemci',$idPrijemce,trim($jmeno." ".$prijmeni." (".$email.")"));
					
$systemMessage->add_ok(OK_ULOZENO);

Redirect($this->get_link('prijemci',0,'edit-settings',$idPrijemce));
                

?>