<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined('SECURITY_CMS')) exit;
if(!is_post('btnUlozitPoznamkuKampane') || 
    !$this->is_access('kampane','edit-settings',0,1,'settings_edit')
    )return;
    
$id = $this->get_id_action();
$pozn = get_post('poznamka');

$db->Query("UPDATE ".TABLE_EMAILY_KAMPANE." SET poznamka='".$pozn."' WHERE idDomeny=".$domain->getId()." AND idKampane=".$id);

$nazev = $db->get(TABLE_EMAILY_KAMPANE,'nazev','idKampane='.$id);
$log->add_log('edit-settings','newsletter-kampane',$id,$nazev);

$systemMessage->add_ok(OK_ULOZENO);				
Redirect($this->get_link('kampane',0,'edit-settings',$id));

?>