<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined('SECURITY_CMS')) exit;

if(!is_action() || 
    (!$this->is_access('skupiny','new',0,0,'settings_add') && 
    !$this->is_access('skupiny','edit-settings',0,1,'settings_edit'))
    ) return;
    
$nazev = get_post('nazev');
$verejna = get_int_post('verejna');
$idSkupiny = $this->get_id_action();
			
$set = "";
if(strlen($nazev)>100 || strlen($nazev)==0)
    $systemMessage->add_error(ERROR_NEPLATNY_NAZEV);
				
if($systemMessage->error_exists()) return;
			
$update = array(
    "idDomeny"=>$domain->getId(),
    "nazev" => $nazev,
    "verejna"=>$verejna
    );
            
if($idSkupiny==0){
    $db->insert(TABLE_EMAILY_SKUPINY, $update);
		
    $idSkupiny = $db->lastId();
    }
	else
	{

    $db->update(TABLE_EMAILY_SKUPINY, $update, "idDomeny=".$domain->getId()." AND idSkupiny=".$idSkupiny);	
	}

$log->add_log($this->get_url('id_action') == 0 ? 'create':'edit-settings','emaily-skupiny',$idSkupiny,$nazev);			
			
$systemMessage->add_ok(OK_ULOZENO);
Redirect($this->get_link('skupiny',0,'edit-settings',$idSkupiny));
			
?>