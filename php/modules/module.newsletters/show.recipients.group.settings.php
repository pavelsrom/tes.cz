<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined('SECURITY_CMS')) exit;
if(!$this->is_access('skupiny','edit-settings',0,1,'settings_view') &&
    !$this->is_access('skupiny','new',0,0,'settings_add')
    ) return;
    
$idGroup = $this->get_id_action();

if($idGroup>0){
    $data = $db->Query("SELECT *
				FROM ".TABLE_EMAILY_SKUPINY." AS s
				WHERE idDomeny=".$domain->getId()." 
					AND idSkupiny=".$idGroup." 
				LIMIT 1");
	$sk = $db->getAssoc($data);
           
    $main_tools = array();
    
    if($login_obj->UserPrivilege('import'))
        $main_tools[] = array(
            'ikona' => 'import', 
            "nazev" => TIMPORT_KONTAKTU, 
            "aktivni" => 0, 
            "odkaz" => $this->get_link('prijemci',0,'import')
            );
                
    if($login_obj->UserPrivilege('export'))
        $main_tools[] = array(
            'ikona' => 'export', 
            "nazev" => TEXPORT_KONTAKTU, 
            "aktivni" => 0, 
            "odkaz" => $this->get_link('prijemci',0,'export')
            );

    }
    else
    {
        $sk = array();
		$sk['nazev'] = get_secure_post('nazev');
        $sk['verejna'] = get_int_post('verejna',1);
    }
		
$main_tools[] = array(
        'ikona' => 'back', 
        "nazev" => TZPET, 
        "aktivni" => 0, 
        "odkaz" => $this->get_link('skupiny'), 
        "title" => TZPET_NA_SEZNAM_SKUPIN
        );
        
echo main_tools($main_tools);
        
$form = new Form();
$form->add_section(TZAKLADNI_UDAJE,'home');
$form->add_text(TNAZEV,'nazev',$sk['nazev'],0,true);		
$form->add_radiobuttons(TTYP,'verejna',$sk['verejna'],array(1=>TVEREJNA,0=>TPRIVATNI),230);

$disabled = (!$login_obj->UserPrivilege('settings_edit') && $idGroup>0) || (!$login_obj->UserPrivilege('settings_add') && $idGroup==0);
$form->add_submit()->set_disabled($disabled);		

echo $form->get_html_code();

?>