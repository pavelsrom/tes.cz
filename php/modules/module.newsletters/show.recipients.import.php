<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined('SECURITY_CMS')) exit;
if(!$this->is_access('prijemci','import',0,0,'import')) return;

$data_groups = GetGroupList();
		
$main_tools = array(); 

if($login_obj->UserPrivilege('settings_add'))
    $main_tools[] = array(
        'ikona' => 'new', 
        "nazev" => TPRIDAT_KONTAKT, 
        "aktivni" => 0, 
        "odkaz" => $this->get_link('prijemci',0,'new')
        );                     
                                   
if($login_obj->UserPrivilege('export'))
    $main_tools[] = array(
        'ikona' => 'export', 
        "nazev" => TEXPORT_KONTAKTU, 
        "aktivni" => 0, 
        "odkaz" => $this->get_link('prijemci',0,'export')
        );
                          
            
$main_tools[] = array(
    'ikona' => 'back', 
    "nazev" => TZPET, 
    "aktivni" => 0, 
    "odkaz" => $this->get_link('prijemci'), 
    "title" => TZPET_NA_SEZNAM_SKUPIN
    );
            

echo main_tools($main_tools);


$skupiny = array();
foreach($data_groups AS $id => $sk)
    $skupiny[$id] = $sk['nazev']." (".$sk['prijemci']." ".TPRIJEMCU.")";

$jazyk = get_int_post("jazyk", -1);
$pohlavi = get_int_post("pohlavi", -1);
$stav = get_int_post("stav", -1);
$skupina = get_int_post("skupina", -1);
$kodovani = get_post("kodovani", "auto");

$form = new Form();
$form->allow_upload();
$form->add_section(TKONFIGURACE, "page-setting",help(440,true));
$form->add_file(TSOUBOR,'soubor',true,441);
$form->add_selectbox(TKODOVANI,'kodovani',$kodovani,array("auto" => TAUTO, "cp1250" => "CP1250 (excel)", "utf-8" => "UTF-8"));
$form->add_selectbox(TJAZYK,'jazyk',$jazyk,array(-1 => TAUTO) + $lang->get_all_in_array());
$form->add_selectbox(TPOHLAVI,'pohlavi',$pohlavi,array(-1 => TAUTO, 0 => TNEUVEDENO, 1 => TMUZ, 2 => TZENA));
$form->add_selectbox(TSTAV,'stav',$stav,array(-1 => TAUTO, 0 => TNEPOTVRZEN, 1 => TAKTIVNI, 2 => TODHLASEN));
$form->add_selectbox(TSKUPINA,'skupina',$skupina,array(-1 => TAUTO) + $skupiny);

$form->add_submit(TIMPORTOVAT)->set_disabled(!$login_obj->UserPrivilege('import'));
		
echo $form->get_html_code();

?>