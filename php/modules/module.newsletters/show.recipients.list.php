<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */


if(!defined('SECURITY_CMS')) exit;
if(!$this->is_access('prijemci','',0,0,'settings_view')) return;

$main_tools = array();
        
if($login_obj->UserPrivilege('settings_add'))
    $main_tools[] = array(
        'ikona' => 'new', 
        "nazev" => TPRIDAT_KONTAKT, 
        "aktivni" => 0, 
        "odkaz" => $this->get_link('prijemci',0,'new')
        );                     
                                   
if($login_obj->UserPrivilege('import'))
    $main_tools[] = array(
        'ikona' => 'import', 
        "nazev" => TIMPORT_KONTAKTU, 
        "aktivni" => 0, 
        "odkaz" => $this->get_link('prijemci',0,'import')
        );
                
if($login_obj->UserPrivilege('export'))
    $main_tools[] = array(
        'ikona' => 'export', 
        "nazev" => TEXPORT_KONTAKTU, 
        "aktivni" => 0, 
        "odkaz" => $this->get_link('prijemci',0,'export')
        );
        
$prijemci = array(
    '3' => "-- ".TVSECHNO." --",
    '0' => TNEPOTVRZEN,
    '1' => TAKTIVNI,
    '2' => TODHLASEN
    );


$data = $db->Query("SELECT idSkupiny AS id, nazev FROM ".TABLE_EMAILY_SKUPINY." WHERE idDomeny=".$domain->getId());
$skupiny = array(0=> "-- ".TVSECHNO." --");
while($sk = $db->getObject($data))
    $skupiny[$sk->id] = $sk->nazev;
        
$form = new FormFilter();
$form->set_class('filter2');
$form->add_text(THLEDAT, 'vyraz', "", 0, false, "", "vyraz");
$form->add_selectbox(TSTAV,'stav','3',$prijemci,0,false,"","stav");
$form->add_selectbox(TSKUPINA,'skupina','0',$skupiny,0,false,"","skupina");
echo $form->get_html_code(main_tools($main_tools));
        
$table = new Table('tList');
$table->tr_head()
    ->add(TID,'w25')
    ->add(TJMENO_A_PRIJMENI)
    ->add(TEMAIL,'w150')
    ->add(TJAZYK,'w25')
    ->add(TSTAV,'w70')
    ->add(TAKCE,'w70');
    
echo $table->get_html();
    
?>
<script type="text/javascript">

$(function(){
var oTable = $("#tList").dataTable({
    "sAjaxSource": "<?php echo AJAX_GATEWAY;?>recipients.list",
    "aoColumns": [
        {"sClass": "sl1"},{"sClass": "sl2 tleft"},{"sClass": "sl3 tleft"},{"sClass": "sl3", "bSortable": false},{"sClass": "sl4"},{"sClass": "akce", "bSortable": false}
        ],
    "fnServerData": function ( sSource, aoData, fnCallback ) {
            aoData.push({ "name": "vyraz", "value": $("#vyraz").val() });
            aoData.push({ "name": "stav", "value": $("#stav option:selected").val() });
            aoData.push({ "name": "skupina", "value": $("#skupina option:selected").val() });                        
            $.getJSON( sSource, aoData, function (json) { fnCallback(json); } );
            }
 
})

$("input#vyraz").bind('keyup change', function() {
        oTable.fnDraw(); 
        });

//prida k bunce th span
$(".dataTable th").each(function(){
    $(this).html("<span>" + $(this).html() + "</span>");
})

$(".filter2 select").bind('change', function() {
    oTable.fnDraw();
    });
    
$("#tList").on("click", "a.delete", function(){
        var id = $(this).attr('id').substring(6);
        var tr = $(this).parents("tr").first();
        var c = $.trim($(this).parents("tr").find("td").eq(1).text()); 
        
        var text = "<?php echo TOPRAVDU_SMAZAT;?>";

        custom_confirm(text.replace("%s",c) + '?', function(){
            $.msg();
            tr.addClass("deleted");
            $.post("<?php echo AJAX_GATEWAY;?>recipients.delete",{id: id},function(data){
                tr.fadeOut(function(){
                    $.msg( 'setClass', 'ok_message' );
                    $.msg( 'replace', '<?php echo OK_SMAZANO;?>' );
                    oTable.fnDraw();
                    $.msg( 'unblock', 1000);
                    });
                });
            
            })
        
        return false;
        
        
    });

})
</script>
