<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined('SECURITY_CMS')) exit;
if(!$this->is_access('prijemci','export',0,0,'export')) return;

$data_groups = GetGroupList();
		
$main_tools = array(); 

if($login_obj->UserPrivilege('settings_add'))
    $main_tools[] = array(
        'ikona' => 'new', 
        "nazev" => TPRIDAT_KONTAKT, 
        "aktivni" => 0, 
        "odkaz" => $this->get_link('prijemci',0,'new')
        );                     
                                   
if($login_obj->UserPrivilege('import'))
    $main_tools[] = array(
        'ikona' => 'import', 
        "nazev" => TIMPORT_KONTAKTU, 
        "aktivni" => 0, 
        "odkaz" => $this->get_link('prijemci',0,'import')
        );
                          
            
$main_tools[] = array(
    'ikona' => 'back', 
    "nazev" => TZPET, 
    "aktivni" => 0, 
    "odkaz" => $this->get_link('prijemci'), 
    "title" => TZPET_NA_SEZNAM_SKUPIN
    );
            

echo main_tools($main_tools);


$skupiny = array();
foreach($data_groups AS $id => $sk)
    $skupiny[$id] = $sk['nazev']." (".$sk['prijemci']." ".TPRIJEMCU.")";

$soubor = get_secure_post('soubor',true,TKONTAKTY);
$jazyk = get_int_post("jazyk", -1);
$pohlavi = get_int_post("pohlavi", -1);
$stav = get_int_post("stav", -1);
$skupina = get_int_post("skupina", -1);
$kodovani = get_post("kodovani", "cp1250");

$form = new Form();
$form->add_section(TKONFIGURACE, "page-setting",help(439,true));
$form->add_text(TSOUBOR,'soubor',$soubor,225);
$form->add_selectbox(TKODOVANI,'kodovani',$kodovani,array("cp1250" => "CP1250 (excel)", "utf-8" => "UTF-8"));
$form->add_selectbox(TJAZYK,'jazyk',$jazyk,array(-1 => TVSECHNO) + $lang->get_all_in_array());
$form->add_selectbox(TPOHLAVI,'pohlavi',$pohlavi,array(-1 => TVSECHNO, 0 => TNEUVEDENO, 1 => TMUZ, 2 => TZENA));
$form->add_selectbox(TSTAV,'stav',$stav,array(-1 => TVSECHNO, 0 => TNEPOTVRZEN, 1 => TAKTIVNI, 2 => TODHLASEN),232);
$form->add_selectbox(TSKUPINA,'skupina',$skupina,array(-1 => TVSECHNO) + $skupiny,226);

$form->add_submit(TEXPORTOVAT)->set_disabled(!$login_obj->UserPrivilege('export'));
		
echo $form->get_html_code();

?>