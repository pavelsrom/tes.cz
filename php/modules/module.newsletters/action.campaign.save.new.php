<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined('SECURITY_CMS')) exit;
if(!is_action() || !$this->is_access('kampane','new',0,0,'content_add')) return;
       
    
$nazev = get_post('nazev');
$kod = get_post('kod');
$pozn = get_post('poznamka');
			
if($nazev==''){
	$systemMessage->add_error(ERROR_NEPLATNY_NAZEV);
	return;
	}			
		
$update = array(
    "idDomeny" => $domain->getId(),
    "nazev" => $nazev,
    "kod" => $kod,
    "poznamka" => $pozn
    );
    	
$db->insert(TABLE_EMAILY_KAMPANE,$update);           
$idKampane = $db->lastId();

   
$log->add_log('create','newsletter-kampane',$idKampane,$nazev);
                
$systemMessage->add_ok(OK_ULOZENO);
Redirect($this->get_link('kampane',0,'edit-settings',$idKampane));

?>