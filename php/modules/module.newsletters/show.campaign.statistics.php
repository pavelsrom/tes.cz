<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined('SECURITY_CMS')) exit;
if(!$this->is_access('kampane','statistiky',0,1,'statistics_view')) return;

$idKampane = $this->get_id_action();

$data = $db->Query("SELECT k.nazev
				FROM ".TABLE_EMAILY_KAMPANE." AS k 
				WHERE k.idKampane=".$idKampane." 
					AND idDomeny=".$domain->getId()."
				LIMIT 1");

$cam = $db->getAssoc($data);
          
$main_tools[] = array(
    'ikona' => 'setting', 
    "nazev" => TNASTAVENI_KAMPANE, 
    "aktivni" => 0, 
    "odkaz" => $this->get_link('kampane',0,'edit-settings',$idKampane)
    );
    
$main_tools[] = array(
    'ikona' => 'back', 
    "nazev" => TZPET, 
    "aktivni" => 0, 
    "odkaz" => $this->get_link('kampane'), 
    "title" => TZPET_NA_SEZNAM_KAMPANI
    );
    
echo main_tools($main_tools);   
        
//start a konec kampane a pocet odeslanych emailu
$data = $db->Query("SELECT DATE_FORMAT(MIN(datumOdeslani),'%d.%m.%Y %H:%i:%s') AS min, 
            DATE_FORMAT(MAX(datumOdeslani),'%d.%m.%Y %H:%i:%s') AS max, 
            COUNT(idPolozky) AS celkem 
            FROM ".TABLE_EMAILY_STAVY." WHERE idKampane=".$idKampane);
        
$d = $db->getAssoc($data);
$start = $d['min'];
$konec = $d['max'];
        
if($start == '')
    $start = $konec = TKAMPAN_NESPUSTENA;
            
$celkem_odeslano = $d['celkem'];

if($celkem_odeslano == 0)
    $celkem_odeslano = 0.000001;
        
//zjisteni dalsich informaci
$data = $db->Query("SELECT COUNT(idPolozky) AS celkem FROM ".TABLE_EMAILY_STAVY." WHERE idKampane=".$idKampane." AND uspesneOdeslani=1");
$d = $db->getAssoc($data);
$celkem_odeslano_uspesne = $d['celkem'];
$celkem_odeslano_uspesne_proc = round(($celkem_odeslano_uspesne / $celkem_odeslano) * 100, 1);
        
$data = $db->Query("SELECT COUNT(idPolozky) AS celkem FROM ".TABLE_EMAILY_STAVY." WHERE idKampane=".$idKampane." AND odhlasen=1");
$d = $db->getAssoc($data);
$celkem_odhlaseno = $d['celkem'];
$celkem_odhlaseno_proc = round(($celkem_odhlaseno / $celkem_odeslano) * 100, 1);
        
$data = $db->Query("SELECT COUNT(idPolozky) AS celkem FROM ".TABLE_EMAILY_STAVY." WHERE idKampane=".$idKampane." AND pocetPrecteni>0");
$d = $db->getAssoc($data);
$celkem_precteno = $d['celkem'];
$celkem_precteno_proc = round(($celkem_precteno / $celkem_odeslano) * 100, 1);

$data = $db->Query("SELECT COUNT(idPolozky) AS celkem FROM ".TABLE_EMAILY_STAVY." WHERE idKampane=".$idKampane." AND pocetPrecteni>1");
$d = $db->getAssoc($data);
$celkem_precteno_vicekrat = $d['celkem'];
$celkem_precteno_vicekrat_proc = round(($celkem_precteno_vicekrat / $celkem_odeslano) * 100, 1);

$data = $db->Query("SELECT COUNT(idPolozky) AS celkem FROM ".TABLE_EMAILY_STAVY." WHERE idKampane=".$idKampane." AND proklik=1");
$d = $db->getAssoc($data);
$celkem_prokliknutych_emailu = $d['celkem'];
$celkem_prokliknutych_emailu_proc = round(($celkem_prokliknutych_emailu / $celkem_odeslano) * 100, 1);
        
$data = $db->Query("SELECT COUNT(idPolozky) AS celkem FROM ".TABLE_EMAILY_STAVY." WHERE idKampane=".$idKampane." AND zobrazenoNaWebu>0");
$d = $db->getAssoc($data);
$celkem_zobrazeni_na_webu = $d['celkem'];
$celkem_zobrazeni_na_webu_proc = round(($celkem_zobrazeni_na_webu / $celkem_odeslano) * 100, 1);
        
$data = $db->Query("SELECT COUNT(idPolozky) AS celkem FROM ".TABLE_EMAILY_STAVY." WHERE idKampane=".$idKampane." AND zobrazenoNaWebuZtextovehoEmailu>0");
$d = $db->getAssoc($data);
$celkem_zobrazeni_na_webu_z_textoveho_emailu = $d['celkem'];
$celkem_zobrazeni_na_webu_z_textoveho_emailu_proc = round(($celkem_zobrazeni_na_webu_z_textoveho_emailu / $celkem_odeslano) * 100, 1);
        
$data = $db->Query("SELECT COUNT(idPolozky) AS celkem FROM ".TABLE_EMAILY_STAVY." WHERE idKampane=".$idKampane." AND konverze>0");
$d = $db->getAssoc($data);
$celkem_konverze = $d['celkem'];
$celkem_konverze_proc = round(($celkem_konverze / $celkem_odeslano) * 100, 1);


        
showH3(TUSPESNOST_KAMPANE, 'stat');

$table = new Table("","table2 tStatistika");
$table->tr()
    ->add_th(TPRVNI_ODESLANY_EMAIL,"",233)
    ->add_td($start,"",0,"",2);
    
$table->tr()
    ->add_th(TPOSLEDNI_ODESLANY_EMAIL,"",234)
    ->add_td($konec,"",0,"",2);
    
$table->tr()
    ->add_th(TCELKEM_ODESLANO,"",235)
    ->add_td(round($celkem_odeslano),"",0,"",2);
      
$table->tr()
    ->add_th()
    ->add_td('abs.')
    ->add_td('%');
    
$table->tr()
    ->add_th(TCELKEM_ODESLANO_USPESNE,"",236)
    ->add_td($celkem_odeslano_uspesne)
    ->add_td($celkem_odeslano_uspesne_proc." %");
    
$table->tr()
    ->add_th(TCELKEM_PRECTENO,"",237)
    ->add_td($celkem_precteno)
    ->add_td($celkem_precteno_proc." %");    

$table->tr()
    ->add_th(TCELKEM_PRECTENO_VICEKRAT,"",238)
    ->add_td($celkem_precteno_vicekrat)
    ->add_td($celkem_precteno_vicekrat_proc." %");

$table->tr()
    ->add_th(TCELKEM_PROKLIKNUTO,"",239)
    ->add_td($celkem_prokliknutych_emailu)
    ->add_td($celkem_prokliknutych_emailu_proc." %");    
                
$table->tr()
    ->add_th(TCELKEM_ZOBRAZENO_NA_WEBU,"",240)
    ->add_td($celkem_zobrazeni_na_webu)
    ->add_td($celkem_zobrazeni_na_webu_proc." %"); 
            
$table->tr()
    ->add_th(TCELKEM_ZOBRAZENO_NA_WEBU_Z_TEXTOVEHO_EMAILU,"",241)
    ->add_td($celkem_zobrazeni_na_webu_z_textoveho_emailu)
    ->add_td($celkem_zobrazeni_na_webu_z_textoveho_emailu_proc." %"); 
        
$table->tr()
    ->add_th(TCELKEM_ODHLASENO,"",242)
    ->add_td($celkem_odhlaseno)
    ->add_td($celkem_odhlaseno_proc." %"); 
    
$table->tr()
    ->add_th(TCELKEM_KONVERZI,"",241)
    ->add_td($celkem_konverze)
    ->add_td($celkem_konverze_proc." %"); 
            
echo $table->get_html();
        
        

?>