<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined('SECURITY_CMS')) exit;
if(!is_action() || !$this->is_access('prijemci','export',0,0,'export')) return;
		
$soubor = ToAscii(get_post('soubor'));
$format = 'csv';//get_post('format');
$skupina = get_int_post('skupina');
$stav = get_int_post('stav');
$jazyk = get_int_post('jazyk');
$pohlavi = get_int_post('pohlavi');

if($soubor == '') $soubor = "kontakty";
$soubor_data = explode('.', $soubor);
$ext = end($soubor_data);
$ext = strtolower($ext);
if($ext != $format) 
    $soubor .= ".".$format;
		
$vychozi_kodovani = "utf-8";	
$kodovani = get_post('kodovani');

if(($format=='xls' || $format=='csv') && $kodovani != 'utf-8')
    $db->Query("SET character_set_results = ".$kodovani."");


$like = array();
$like[] = "p.idDomeny=".$domain->getId();

if($skupina > 0)
    $like[] = "s.idSkupiny = ".$skupina;

if($stav >= 0 )
    $like[] = "p.stav = ".$stav;
    
if($pohlavi >= 0)
    $like[] = "p.pohlavi = ".$pohlavi;
    
if($jazyk > 0)
    $like[] = "p.idJazyka = ".$jazyk; 

    
$where = count($like) > 0 ? implode(" AND ", $like) : "";
							
$query = "SELECT p.idPrijemce, spolecnost, jmeno, prijmeni, osloveni, pohlavi, email, stav, s.idSkupiny AS skupina, j.jazyk
            FROM ".TABLE_EMAILY_SKUPINY_PRIJEMCI." AS s 
            LEFT JOIN ".TABLE_EMAILY_PRIJEMCI." AS p ON p.idPrijemce = s.idPrijemce
            LEFT JOIN ".TABLE_JAZYKY." AS j ON p.idJazyka = j.idJazyka 
            WHERE ".implode(" AND ", $like)."
            ORDER BY s.idSkupiny, email
            ";
			
$data = $db->Query($query);
if($db->numRows($data)==0)
{
    $systemMessage->add_warning(WARNING_NEJSOU_KONTAKTY_EXPORT_IMPORT);
    return;
	//Redirect($this->get_link('prijemci',0,'export'));
}			
			
//export do xls souboru
include_once('php/classes/class.export.php');


			
if($format == 'xls' || $format == 'csv' || $format=='xml'){
	if($format == 'xls') $export = new ExportXls(); 
	if($format == 'csv') $export = new ExportCsv(); 
	if($format == 'xml') $export = new ExportXml('polozky','polozka');
	
    $p = array();
    $p['email'] = iconv($vychozi_kodovani, $kodovani,TEMAIL);
    $p['jmeno'] = iconv($vychozi_kodovani, $kodovani,TJMENO);
    $p['prijmeni'] = iconv($vychozi_kodovani, $kodovani,TPRIJMENI);
    $p['spolecnost'] = iconv($vychozi_kodovani, $kodovani,TSPOLECNOST);
    $p['osloveni'] = iconv($vychozi_kodovani, $kodovani,TOSLOVENI);
    $p['pohlavi'] = iconv($vychozi_kodovani, $kodovani,TPOHLAVI);
    $p['stav'] = iconv($vychozi_kodovani, $kodovani,TSTAV);
    $p['jazyk'] = iconv($vychozi_kodovani, $kodovani,TJAZYK);
    $p['skupina'] = iconv($vychozi_kodovani, $kodovani,TSKUPINA);
    
    if($format == 'csv')
        $export->addRow($p);
        
    $rows = array();
    $skupiny = array();	
	while($prijemce = $db->getAssoc($data)){
		$p = array();
		$p['email'] = $prijemce['email'];
		$p['jmeno'] = $prijemce['jmeno'];
		$p['prijmeni'] = $prijemce['prijmeni'];
		$p['spolecnost'] = $prijemce['spolecnost'];
		$p['osloveni'] = $prijemce['osloveni'];
		$p['pohlavi'] = $prijemce['pohlavi'];
		$p['stav'] = $prijemce['stav'];
        $p['jazyk'] = $prijemce['jazyk'];
		//$p['skupina'] = $prijemce['skupina'];
		//if($format=='xls') $p = array_values($p);
        $rows[$prijemce['idPrijemce']] = $p;
		
        if(!isset($skupiny[$prijemce['idPrijemce']]))
            $skupiny[$prijemce['idPrijemce']] = array();
            
        $skupiny[$prijemce['idPrijemce']][] = $prijemce['skupina'];
        
		}

    foreach($rows AS $idp => $r)
    {
       $r["skupina"] = implode(",",$skupiny[$idp]); 
       $export->addRow($r); 
    }




    $log->add_log('export','newsletter-prijemci');

	$export->download($soubor);
	}
    
    

    
    
    
    

?>