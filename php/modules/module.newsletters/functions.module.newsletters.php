<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined('SECURITY_CMS')) exit;

//nahraje prilohu k emailu
function EmailUploadAttachment($idKampane){
		global $db;
		global $domain;
		global $systemMessage;
		global $login_obj;

		$prilohy = (array) $_FILES['prilohy'];
			
    
            
        $pocet_souboru = count($prilohy);
        
        if($pocet_souboru == 0)
            return false;
        
		if(count($_FILES)>0){
			//zjisteni poctu nahranych priloh
			$data = $db->Query("SELECT * FROM ".TABLE_EMAILY_PRILOHY." WHERE idKampane=".$idKampane);
			$pocet = $db->numRows($data);
			$attach_path = $domain->getDir().USER_DIRNAME_ATTACHMENT;
            $attach_dir = substr($attach_path, 0, -1);
			if(!file_exists($attach_dir)) {
				@mkdir($attach_dir, 0777);
				@chmod($attach_dir, 0777);
				}
				
			
			for($i=0; $i<$pocet_souboru; $i++){
	
				if(!isset($prilohy['tmp_name'][$i]) || $prilohy['tmp_name'][$i]=='' || $prilohy['name'][$i]=='') continue;
				$uploadError = GetUploadError($prilohy['error'][$i]);
				if($uploadError!=''){
					$systemMessage->add_error($uploadError);
					}

					
                    $upload_priloha = array(
                        'name' => $prilohy['name'][$i],
                        'type' => $prilohy['type'][$i],
                        'tmp_name' => $prilohy['tmp_name'][$i],
                        'error' => $prilohy['error'][$i],
                        'size' => $prilohy['size'][$i]
                        );
                    
                    
                    $file = $upload_priloha['name'];
                    $file_data = explode(".", $file);
                    $ext = end($file_data);
                    unset($file_data[count($file_data) - 1]);
                    $body = implode(".",$file_data);
                    
                    $body = ModifyUrl($body);
                    $body = trim($body, "-");
                    //$body = rand(10,99)."-".$body;
                    
                    include_once('php/classes/class.upload.php');
                    $uploader = new upload($upload_priloha, 'cs_CS');
                    $uploader->file_new_name_body = $body;
                    //$uploader->file_overwrite = true;
                    $uploader->process($attach_path);
                    $uploader->clean();
                    $velikost = $uploader->file_src_size;
                    $nazev = $uploader->file_dst_name;
                    $res = true;
                    
                    

					if($file!='')
						$res = $db->Query("INSERT INTO ".TABLE_EMAILY_PRILOHY." (idKampane, soubor, velikost) VALUES(".$idKampane.", '".$nazev."', ".$velikost.")");
						if($res)
							$systemMessage->add_ok(sprintf(OK_SOUBOR_BYL_PRILOZEN_K_EMAILU, $prilohy['name'][$i]));
							else
							$systemMessage->add_error(sprintf(ERROR_SOUBOR_SE_NEPODARILO_ULOZIT, $prilohy['name'][$i]));

					}
				}

			
		}
        
//aktualizuje data skupiny
	function GetGroupList($lang_id = 0){
		global $db;
		global $domain;
		
		$data_groups = array();
		$data_name_groups = $db->Query("SELECT * FROM ".TABLE_EMAILY_SKUPINY." WHERE idDomeny=".$domain->getId());
		while($sk = $db->getAssoc($data_name_groups))
			$data_groups[$sk['idSkupiny']] = array("nazev" => secureString($sk['nazev']), "prijemci" => 0);
            
		$where = "";
        if($lang_id > 0)
            $where = "AND p.idJazyka = ".$lang_id;
            
		$data = $db->Query("SELECT DISTINCT s.idSkupiny, s.nazev, COUNT(sp.idPrijemce) AS pocet 
			FROM ".TABLE_EMAILY_SKUPINY." AS s 
			LEFT JOIN ".TABLE_EMAILY_SKUPINY_PRIJEMCI." AS sp ON sp.idSkupiny = s.idSkupiny 
			LEFT JOIN ".TABLE_EMAILY_PRIJEMCI." AS p ON sp.idPrijemce = p.idPrijemce 
			WHERE s.idDomeny=".$domain->getId()."
                ".$where."    
				GROUP BY s.idSkupiny 
				ORDER BY s.idSkupiny");
				
		while($sk = $db->getAssoc($data))
			$data_groups[$sk['idSkupiny']]['prijemci'] = $sk['pocet'];
	
    
        return $data_groups;
	}


?>