<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */


if(!defined('SECURITY_CMS')) exit;
if(!is_action() || !$this->is_access('kampane','edit-settings',0,1,'content_edit')) return;

$id = $this->get_id_action();
$nazev = get_post('nazev');
$kod = get_post('kod');
$pozn = get_post('poznamka');
$predmet = get_post('predmet');
$obsah = get_post('obsah','',false);
$textovy_obsah = get_post('text');
$start_data = get_post('start');
$osloveni_muz = get_post('osloveniMuz');
$osloveni_zena = get_post('osloveniZena');
$osloveni_ostatni = get_post('osloveniOstatni');

$skupiny = get_array_post('skupiny');
$set = "";
$start = "";

$update = array();
    
$stav_kampane = $db->get(TABLE_EMAILY_KAMPANE,"stav","idKampane=".$id);       
				
if($stav_kampane == 'rozpracovano')
{		
    
    //kontrola spravnosti data zahajeni kampane
    $start = "null";
    if($start_data!='')
    {
    	$d = explode(".", $start_data);
    	if(count($d)==3 && checkdate($d[1], $d[0], $d[2]) && $d[2]>=2010)
    		$start = $d[2]."-".$d[1]."-".$d[0]; 
    		else
    			$systemMessage->add_error(ERROR_CHYBNE_DATUM_ZAHAJENI);
    
    }          
    		
    if($nazev == '') 
    	$systemMessage->add_error(ERROR_NEPLATNY_NAZEV); 
    				
    if($predmet == '') 
    	$systemMessage->add_error(ERROR_PREDMET);
    
    if($systemMessage->error_exists()) return;
    
    
    $update = $update + array(
        "nazev" => $nazev,
        "predmet" => $predmet,
    	"obsah" => $obsah,
    	"kod" => $kod,
        "textovyObsah" => $textovy_obsah,
        "start" => $start,
    	"poznamka" => $pozn,
        "osloveniMuz" => $osloveni_muz,
        "osloveniZena"=> $osloveni_zena,
        "osloveniOstatni" => $osloveni_ostatni
    	);
        
        
    $db->Query("DELETE FROM ".TABLE_EMAILY_SKUPINY_KAMPANE." WHERE idKampane=".$id);
		
    if(count($skupiny)>0)
    {
    	
    	foreach($skupiny AS $s)
        { 
            $insert = array(
                "idKampane" => $id,
                "idSkupiny" => $s
                ); 
            $db->insert(TABLE_EMAILY_SKUPINY_KAMPANE, $insert);
        }
    }   
        
}
else
$update = array("poznamka" => $pozn);
	
$db->update(TABLE_EMAILY_KAMPANE,$update,"idKampane=".$id." AND idDomeny=".$domain->getId());		



			
//upload souboru priloh	
EmailUploadAttachment($id);
	
//odeslani kampane pomoci cronu - naplneni fronty emailu
if(is_post('odeslat_kampan'))
{
    $kampan = C_Campaign::get_instance($id);
    $kampan->queue()->fill();
    $kampan->change_state('odesilano');
    
    if($start == 'null')
        $db->update(TABLE_EMAILY_KAMPANE,array("start" => date("Y-m-d")),"idKampane=".$id." AND idDomeny=".$domain->getId());
        
}
    			
                
if(MODULE_CONTENT_BACKUP)
{
    $content_backup = new ContentBackup($domain->getId(),$id,$login_obj->getId(),'newsletter');
    $content_backup->AddStore($obsah);
}


$chybne_odkazy_indik = false;

if(MODULE_LINK_CHECKER)
{

    $chybne_odkazy = new LinksChecker($domain->getId(), $id, "newsletter", $obsah, false, KONTROLOVAT_ODKAZY_PRI_UKLADANI);
    $chybne_odkazy->set_base_url( $domain->getUrl() );
    $chybne_odkazy->process();
    $pocet_chybnych_obrazku = $chybne_odkazy->get_sum_error_images();
    $pocet_chybnych_odkazu = $chybne_odkazy->get_sum_error_links();
       
    if($pocet_chybnych_obrazku > 0)
        $systemMessage->add_warning(TPOCET_CHYBNYCH_OBRAZKU_NA_STRANCE.": ".$pocet_chybnych_obrazku);
        
    if($pocet_chybnych_odkazu > 0)
        $systemMessage->add_warning(TPOCET_CHYBNYCH_ODKAZU_NA_STRANCE.": ".$pocet_chybnych_odkazu);
    
    if(!$chybne_odkazy_indik && ($pocet_chybnych_obrazku > 0 || $pocet_chybnych_odkazu > 0)) 
        $chybne_odkazy_indik = true;
        
                
}


if($chybne_odkazy_indik)
    $systemMessage->add_warning(TODKAZY_ZKONTROLUJTE_A_OPRAVTE);
                   

$log->add_log('edit-settings','newsletter-kampane',$id,$nazev);
  
                
if($systemMessage->error_exists())
	$systemMessage->add_warning(WARNING_ULOZENO);
	else
	$systemMessage->add_ok(OK_ULOZENO);
					
Redirect($this->get_link('kampane',0,'edit-settings',$id));	
            
            
?>