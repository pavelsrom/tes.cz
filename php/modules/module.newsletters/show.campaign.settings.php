<?php

/**
 * @author Pavel Srom
 * @copyright 2014
 */

if(!defined('SECURITY_CMS')) exit;
if(!$this->is_access('kampane','edit-settings',0,1,'content_view') &&
    !$this->is_access('kampane','new',0,0,'content_add')
    ) return;
    
$idCampain = $this->get_id_action();

$kampan = C_Campaign::get_instance($idCampain);

$main_tools = array();

if($idCampain > 0){
    $data = $db->Query("SELECT k.*, r.idRelace AS skupina, IF(datumOdeslano IS null OR datumOdeslano = '','',DATE_FORMAT(datumOdeslano,'%d.%m.%Y %H:%i')) AS konec
            FROM ".TABLE_EMAILY_KAMPANE." AS k 
            LEFT JOIN ".TABLE_EMAILY_SKUPINY_KAMPANE." AS r ON k.idKampane=r.idKampane 
            WHERE k.idKampane=".$idCampain." 
				AND k.idDomeny=".$domain->getId()."
            LIMIT 1"
            );
            
    $cam = $db->getAssoc($data);
    $data_groups = GetGroupList($cam['idJazyka']);
    $odeslat_kampan = array();        
}
else
{
    $cam = array();
	$cam['nazev']=get_secure_post('nazev');
	$cam['kod']=get_secure_post('kod');
	$cam['poznamka']=get_secure_post('poznamka');
 	$cam['stav'] = "rozpracovano";
    $cam['osloveniOstatni'] = get_secure_post('osloveniOstatni',true,TDEFAULTNI_OSLOVENI_OSTATNI);
    $cam['osloveniMuz'] = get_secure_post('osloveniMuz',true,TDEFAULTNI_OSLOVENI_MUZ);
    $cam['osloveniZena'] = get_secure_post('osloveniZena',true,TDEFAULTNI_OSLOVENI_ZENA);
    $odeslat_kampan = array();
}




$stav = $cam['stav'];

$main_tools[] = array(
    'ikona' => 'back', 
    "nazev" => TZPET, 
    "aktivni" => 0, 
    "odkaz" => $this->get_link('kampane'), 
    "title" => TZPET_NA_SEZNAM_KAMPANI
    );
    
echo main_tools($main_tools);

$form = new Form();
$form->add_section(TZAKLADNI_UDAJE,'home');
$form->allow_upload();

$stavy_aktivni = array("rozpracovano");
$disabled = !in_array($stav, $stavy_aktivni); 

$form->add_text(TNAZEV,'nazev',$cam['nazev'],208,true)->set_disabled($disabled);;
$form->add_text(TKOD_PRO_GOOGLE_ANALYTICS,'kod',$cam['kod'],209)->set_disabled($disabled);;
$form->add_textarea(TPOZNAMKA,'poznamka',$cam['poznamka']);
		
$campaign_status = array(
    "rozpracovano" => TROZPRACOVANO, 
    "odesilano" => TPRAVE_PROBIHA, 
    "ukonceno" => TUKONCENA
    );
    
$end_date = $stav == 'ukonceno' ? " (".$cam["konec"].")" : "";
$form->add_plain_text(TSTAV,$campaign_status[$stav].$end_date);
        
$width = 0.5;
if($idCampain > 0 )
{
    if($stav!='rozpracovano')
    {
        
        $kampan = C_Campaign::get_instance($idCampain);
        $zbyva_odeslat = $kampan->queue()->get_count_remaining_emails();
        $celkem = $kampan->get_sum_emails();
        $odeslano = $celkem - $zbyva_odeslat;
        $prokliknuto = $cam['kliknuto'];//$kampan->queue()->get_count_clicked_emails();
        $otevreno = $cam['otevreno'];//$kampan->queue()->get_count_opened_emails();
        $odhlaseno = $cam['odhlaseno'];//$kampan->queue()->get_count_unsubscribed_emails();
        
    	$width = $celkem>0?((100/$celkem) * $odeslano):0;
        $width_otevreno = $celkem>0?((100/$celkem) * $otevreno):0;
        $width_prokliknuto = $celkem>0?((100/$celkem) * $prokliknuto):0;
        $width_odhlaseno = $celkem>0?((100/$celkem) * $odhlaseno):0;
        
    	$zbyva_odeslat = $celkem - $odeslano;
    
        if($stav=='rozpracovano') 
            $width = 0.2;
    
        if($width < 1) 
            $width = 0.2;
             	
        if($width > 100) 
            $width = 100;
            
           
        
        $code = '
                <div class="graph_box">
                    <div class="graph_kontrola" style="width: '.floor($width).'%;"></div>
                    <div class="graph_text">'.floor($width).'% ('.$odeslano.' / '.$celkem.')</div>
                </div>
            ';
        $form->add_plain_text(TODESLANO_EMAILU,$code, 434);
        
        $code = '
                <div class="graph_box">
                    <div class="graph_kontrola" style="width: '.floor($width_otevreno).'%;"></div>
                    <div class="graph_text">'.floor($width_otevreno).'% ('.$otevreno.' / '.$celkem.')</div>
                </div>
            ';
        $form->add_plain_text(TPRECTENO_EMAILU,$code, 435);
        
        $code = '
                <div class="graph_box">
                    <div class="graph_kontrola" style="width: '.floor($width_prokliknuto).'%;"></div>
                    <div class="graph_text">'.floor($width_prokliknuto).'% ('.$prokliknuto.' / '.$celkem.')</div>
                </div>
            ';
        $form->add_plain_text(TPROKLINUTO_EMAILU,$code, 436);
        
        $code = '
                <div class="graph_box">
                    <div class="graph_kontrola" style="width: '.floor($width_odhlaseno).'%;"></div>
                    <div class="graph_text">'.floor($width_odhlaseno).'% ('.$odhlaseno.' / '.$celkem.')</div>
                </div>
            ';
        $form->add_plain_text(TODHLASENO_EMAILU,$code, 438);
        
        $form->add_textarea(TNEDORUCITELNE_EMAILY, 'nedorucitelne_emaily', implode(PHP_EOL, $kampan->queue()->get_undeliverable_emails()), 437)->set_readonly();
        
        
        			
    }
        


    $form->add_section(TODESILATEL_A_PRIJEMCI_EMAILU,'receive');   
    $form->add_plain_text(TODESILATEL, NAZEV_WEBU." (".EMAIL_WEBU.")");
        



    //skupiny do kterych patri uzivatel
           			
	$data_user_sk = $db->Query("SELECT idSkupiny FROM ".TABLE_EMAILY_SKUPINY_KAMPANE." WHERE idKampane=".$idCampain);
	$us = array();
	while($user_sk = $db->getAssoc($data_user_sk))
	   $us[] = $user_sk['idSkupiny'];
						
	
    $sk_vsechny = array();
    $sk_checked = array();
    
    foreach($data_groups AS $id => $sk){
       if(in_array($id, $us))
            $sk_checked[] = $id;
            
        $sk_vsechny[$id] = $sk['nazev']." (".$sk['prijemci']." ".TPRIJEMCU.")";
                            
       }
    
    $form->add_checkboxes(TCILOVA_SKUPINA,'skupiny',$sk_checked,$sk_vsechny,212,false,"sk")
        ->add_text_after(sprintf(TCELKEM_EMAILU_Z_TOHO_JEDINECNYCH, "<span id='celkem_emailu_ve_skupinach'>0</span>", "<span id='pocet_jedinecnych_emailu'>0</span>"))->set_disabled($disabled);

	

						
	if(count($data_groups)==0)
        $form->add_plain_text(TCILOVA_SKUPINA,TNENI_ZALOZENA_ZADNA_SKUPINA." (<a href='".$this->get_link('skupiny',0,'new')."'>".TZALOZIT_SKUPINU."</a>)");
			
            
            
            
            
    $form->add_section(TOBSAH,'content',help(429,true));
    
    $form->add_text(TOSLOVENI_MUZ, 'osloveniMuz', $cam['osloveniMuz'], 430, false,"","osloveni_muz")->set_disabled($disabled);
    $form->add_text(TOSLOVENI_ZENA, 'osloveniZena', $cam['osloveniZena'], 431, false,"","osloveni_zena")->set_disabled($disabled);
    $form->add_text(TOSLOVENI_OSTATNI, 'osloveniOstatni', $cam['osloveniOstatni'], 432, false,"","osloveni_ostatni")->set_disabled($disabled);
    $form->add_text(TPREDMET, 'predmet', $cam['predmet'], 213, true,"","predmet")->set_disabled($disabled);
    

       
				
	$form->add_textarea(TOBSAH." - plain text",'text',$cam['textovyObsah'],214,false,"h100","text")->set_disabled($disabled);
    $form->add_editor(TOBSAH." - html",'obsah',$cam['obsah'],215,false,"h800 w100")->allow_wet()->set_disabled($disabled);
    		
	$data_attach = $db->Query("SELECT * FROM ".TABLE_EMAILY_PRILOHY." WHERE idKampane=".$idCampain);
	$pocet = $db->numRows($data_attach);
			
	
    $form->add_file(TPRIDAT_PRILOHU,'prilohy[]',false,216)->set_multiple()->set_disabled($disabled);
	
			
			
	$code = "";		
	while($attach = $db->getAssoc($data_attach))
    {
        $code .= '<div class="priloha">';
		if($cam['stav']=='ukonceno')
		  $code .= "<a href='".RELATIVE_PATH.$domain->getDir().USER_DIRNAME_ATTACHMENT.$attach['soubor']."'>".$attach['soubor']."</a>";
		else
        {
            
            	
        $code .= "<a href='".RELATIVE_PATH.$domain->getDir().USER_DIRNAME_ATTACHMENT.$attach['soubor']."' target='_blank'>".$attach['soubor']."</a>";
        
        if($login_obj->UserPrivilege('content_delete') && !$disabled)
				$code .= tlacitko("#",TSMAZAT,"","times","delete","delete".$attach['idPrilohy']);
        
		}
					
		$code .= '</div>';		
						
	}

    $form->add_plain_text(TPRILOZENE_PRILOHY,$code);
    
    $form->add_section(TNACASOVANI_SPUSTENI,'date');           
			
    $date = explode('-',$cam['start']);
    $date_val = ((count($date)==3 && checkdate($date[1], $date[2], $date[0]))?strftime("%d.%m.%Y", strtotime($cam['start'])):'');
    
    
    $form->add_text(TDATUM_SPUSTENI,'start',$date_val,218,false,"kalendar")->set_disabled($disabled);
     
    $form->add_section(TPOSLAT_ZKUSEBNI_EMAIL,'email',help(222,true));
    $form->add_text(TEMAIL,'email',$login_obj->getEmail(),0,false,"","temail")
            ->add_text_after(tlacitko("#",TODESLAT_EMAIL,"","envelope-o","","odeslat_testovaci_email"));


    
    
    if($stav == 'rozpracovano')
    {
        $form->add_section(TODESLAT_KAMPAN,'email',help(433,true));
        $form->add_checkboxes("", 'odeslat_kampan',$odeslat_kampan, array(1 => TKAMPAN_JE_ZKONTROLOVANA));
        
        
    }
}
			
$disabled = !(($login_obj->UserPrivilege('content_edit') && $idCampain>0) || ($login_obj->UserPrivilege('content_add') && $idCampain==0));
       
$form->add_submit()->set_disabled($disabled);	

echo $form->get_html_code();

?>

<script type="text/javascript">
<!--  


$(function(){
    
    function get_email_count()
    {
        var sk = $('.sk input').serialize();
        
        $.post('<?php echo AJAX_GATEWAY;?>campaigns.emails.count', sk, function(data){
            var celkem =  data.celkem;
            var unikatni = data.unikatni;
            $('#celkem_emailu_ve_skupinach').text(celkem);
            $('#pocet_jedinecnych_emailu').text(unikatni);
        }, 'json');
    }
            
    get_email_count();        
    $('.sk input').click(get_email_count);
    
    
    $(document).on('click', '#odeslat_testovaci_email', function(){
        var text = $('#text').val();
        var editor = CKEDITOR.instances.wysiwygEditor;
        var html = editor.getData();
        
        var prijemce = $('#temail').val();
        var predmet = $('#predmet').val();
        var idKampane = <?php echo $idCampain;?>;
        osloveni_muz = $("#osloveni_muz").val();
        osloveni_zena = $("#osloveni_zena").val();
        osloveni_ostatni = $("#osloveni_ostatni").val();
        $.msg();
        $.post('<?php echo AJAX_GATEWAY;?>campaigns.test.email',{text: text, html: html, prijemce: prijemce, predmet: predmet, idKampane: idKampane, osloveni_muz: osloveni_muz, osloveni_zena: osloveni_zena, osloveni_ostatni: osloveni_ostatni}, 
        function(data){
     
            if(data.error!=''){
                $.msg( 'setClass', 'error_message' );
                $.msg( 'replace', data.error );
                $.msg( 'unblock', 1500);
                }
            else if(data.ok!='')
            {
                $.msg( 'setClass', 'ok_message' );
                $.msg( 'replace', data.ok );
                $.msg( 'unblock', 1000);
            }    

        },'json');
        
        return false;
    })
    
    $("a.delete").click(function(){
        var id = $(this).attr('id').substring(6);
        var text = "<?php echo TOPRAVDU_SMAZAT_PRILOHU;?>";
        var div = $(this).parents("div").first();
        
        custom_confirm(text, function(){
            $.msg();
            div.addClass("deleted");
            $.post("<?php echo AJAX_GATEWAY;?>campaign.attach.delete",{id: id},function(data){
                $.msg( 'setClass', 'ok_message' );
                $.msg( 'replace', '<?php echo OK_SMAZANO;?>' );
                $.msg( 'unblock', 1000);
                div.fadeOut();
                div.removeClass("deleted");
                });
            
            })
            
        return false;
        
    })
    
    
})

    
          
-->
</script>