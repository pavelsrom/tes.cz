<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */


//------------------------------------------------
//definice polozek menu
//------------------------------------------------

$module_menu = array();
$module_menu[] = array(
    "page"  => "kampane",
    "name"  => TEMAILOVE_KAMPANE
    );
$module_menu[] = array(
    "page"  => "skupiny",
    "name"  => TSEZNAM_SKUPIN_PRIJEMCU
    );
$module_menu[] = array(
    "page"  => "prijemci",
    "name"  => TSEZNAM_PRIJEMCU
    );
    
if($login_obj->UserPrivilege('settings_view'))
$module_menu[] = array(
    "page"  => "nastaveni",
    "name"  => TNASTAVENI
    );
    
//------------------------------------------------
//definice nadpisu h1 a textu k nadpisu
//------------------------------------------------ 
$module_h1 = array();
$module_h1[] = array(
    "page"  => "skupiny",
    "action"=> "edit-settings",
    "verify_id_action" => 1,
    "name"  => TEDITACE_SKUPINY
    );
$module_h1[] = array(
    "page"  => "skupiny",
    "action"=> "new",
    "name"  => TNOVA_SKUPINA
    );
$module_h1[] = array(
    "page"  => "skupiny",
    "name"  => TSEZNAM_SKUPIN_PRIJEMCU
    );    
$module_h1[] = array(
    "page"  => "kampane",
    "action"=> "edit-settings",
    "verify_id_action" => 1,
    "name"  => TEDITACE_KAMPANE
    );
$module_h1[] = array(
    "page"  => "kampane",
    "action"=> "new",
    "name"  => TNOVA_KAMPAN
    );
$module_h1[] = array(
    "page"  => "kampane",
    "name"  => TEMAILOVE_KAMPANE
    );    
$module_h1[] = array(
    "page"  => "kampane",
    "action"  => "statistiky",
    "verify_id_action" => 1,
    "name"  => TSTATISTIKY
    );

$module_h1[] = array(
    "page"  => "prijemci",
    "action"=> "edit-settings",
    "verify_id_action" => 1,
    "name"  => TNASTAVENI_POLOZKY
    );
$module_h1[] = array(
    "page"  => "prijemci",
    "action"=> "new",
    "name"  => TNOVY_PRIJEMCE
    );
$module_h1[] = array(
    "page"  => "prijemci",
    "action"=> "edit-settings",
    "verify_id_action" => 1,
    "name"  => TEDITOVAT_PRIJEMCE
    );       
$module_h1[] = array(
    "page"  => "prijemci",
    "name"  => TSEZNAM_PRIJEMCU
    );
$module_h1[] = array(
    "page"  => "prijemci",
    "action"  => "import",
    "name"  => TIMPORT_KONTAKTU
    );   
$module_h1[] = array(
    "page"  => "prijemci",
    "action"  => "export",
    "name"  => TEXPORT_KONTAKTU
    );
$module_h1[] = array(
    "page"  => "nastaveni",
    "name"  => TNASTAVENI
    );



?>