<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined('SECURITY_CMS')) exit;
if(!$this->is_access('prijemci','edit-settings',0,1,'settings_view') &&
    !$this->is_access('prijemci','new',0,0,'settings_add')
    ) return;
    
$idRecipient = $this->get_id_action();
		
if($idRecipient==0){
			$kontakt = array();
			$kontakt['jmeno'] = get_secure_post('jmeno');
			$kontakt['prijmeni'] = get_secure_post('prijmeni');
			$kontakt['spolecnost'] = get_secure_post('spolecnost');
            $kontakt['osloveni'] = get_secure_post('osloveni');
			$kontakt['email'] = get_secure_post('email');
            $kontakt['poznamka'] = get_secure_post('poznamka');
			$kontakt['stav'] = get_int_post('stav');
			$kontakt['pohlavi'] = get_int_post('pohlavi');
            $kontakt['jazyk'] = get_int_post('jazyk', $lang->get_id());
			}
			else{
			
			$data = $db->Query("SELECT *, idJazyka AS jazyk FROM ".TABLE_EMAILY_PRIJEMCI." WHERE idPrijemce=".$idRecipient." AND idDomeny=".$domain->getId());
			$kontakt = $db->getAssoc($data);
			
			}
		
$main_tools = array();
      
$main_tools[] = array(
    'ikona' => 'back', 
    "nazev" => TZPET, 
    "aktivni" => 0, 
    "odkaz" => $this->get_link('prijemci'), 
    "title" => TZPET_NA_SEZNAM_KONTAKTU
    );
    
echo main_tools($main_tools);

$form = new Form();
$form->add_section(TZAKLADNI_UDAJE, 'home');
$form->add_text(TEMAIL,'email',$kontakt['email'],0,true);
$form->add_text(TSPOLECNOST,'spolecnost',$kontakt['spolecnost']);
$form->add_text(TJMENO,'jmeno',$kontakt['jmeno']);
$form->add_text(TPRIJMENI,'prijmeni',$kontakt['prijmeni']);
$form->add_text(TOSLOVENI,'osloveni',$kontakt['osloveni']);

$form->add_selectbox(TJAZYK,'jazyk',$kontakt['jazyk'],$lang->get_all_in_array());
$form->add_radiobuttons(TPOHLAVI,'pohlavi',$kontakt['pohlavi'],array(0 => TNEUVEDENO, 1 => TMUZ, 2 => TZENA));
$form->add_radiobuttons(TSTAV,'stav',$kontakt['stav'],array(0 => TNEPOTVRZEN, 1 => TAKTIVNI, 2 => TODHLASEN),232);


$data_skupiny = $db->Query("SELECT s.idSkupiny AS id, s.nazev, IF(sp.idPrijemce IS NULL, 0, 1) AS checked
    FROM ".TABLE_EMAILY_SKUPINY." AS s
    LEFT JOIN ".TABLE_EMAILY_SKUPINY_PRIJEMCI." AS sp ON s.idSkupiny = sp.idSkupiny AND idPrijemce=".$idRecipient." 
    ORDER BY s.nazev
    ");

$skupiny_vsechny = array();
$skupiny_checked = array();
while($s = $db->getObject($data_skupiny))
{
    	$skupiny_vsechny[$s->id] = $s->nazev;
        if($s->checked)
            $skupiny_checked[] = $s->id; 	
    
}

if(count($skupiny_vsechny)>0)
    $form->add_checkboxes(TSKUPINA,'skupiny',$skupiny_checked,$skupiny_vsechny,231)->setListBelow();
    else
    $form->add_plain_text(TSKUPINA, TNENI_ZALOZENA_ZADNA_SKUPINA." (<a href='".$this->get_link('skupiny',0,'new')."'>".TZALOZIT_SKUPINU."</a>)");


$form->add_textarea(TPOZNAMKA,'poznamka',$kontakt['poznamka']);

$disabled = (!$login_obj->UserPrivilege('settings_add') && $idRecipient==0) || (!$login_obj->UserPrivilege('settings_edit') && $idRecipient>0);

$form->add_submit()->set_disabled($disabled);
			
echo $form->get_html_code();
    
        
?>