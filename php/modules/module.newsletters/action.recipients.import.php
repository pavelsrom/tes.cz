<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined('SECURITY_CMS')) exit;
if(!is_action() || !$this->is_access('prijemci','import',0,0,'import')) return;

$format = 'csv';//get_post('format');
$skupina = get_int_post('skupina');
$stav = get_int_post('stav');
$jazyk = get_int_post('jazyk');
$pohlavi = get_int_post('pohlavi');
$kodovani = get_post('kodovani');

			
$soubor = $_FILES['soubor']['name'];

$error = GetUploadError($_FILES['soubor']['error']);
            
if($error != ''){
	$systemMessage->add_error($error);
	}

	
if($systemMessage->error_exists())
    return;
    
$file_data = explode('.', $soubor);    			
$ext = strtolower(end($file_data));
$csv = file_get_contents($_FILES['soubor']['tmp_name']);

if($kodovani == "auto")
{
    if (preg_match('#[\x80-\x{1FF}\x{2000}-\x{3FFF}]#u', $csv))
        $kodovani = 'utf-8';           
    elseif (preg_match('#[\x7F-\x9F\xBC]#', $csv))
        $kodovani = 'cp1250';
    else
        $kodovani = '';
}

if($kodovani == "cp1250")
{
    $db->Query("SET character_set_client=cp1250");
}


if($ext != "csv")
{
    $systemMessage->add_error(ERROR_NEPODPOROVANY_FORMAT_SOUBORU);
}

if($kodovani == "")
{
    $systemMessage->add_error(ERROR_NEROZPOZNANE_KODOVANI);
}

$csv_radky = explode(PHP_EOL, $csv);

if(count($csv_radky) == 0)
{
    $systemMessage->add_error(ERROR_IMPORT_PRAZDNY_SOUBOR);
}



if($systemMessage->error_exists())
    return;
    
$nove = 0;
$aktualizace = 0;
$chyby = 0;
$celkem_zaznamu = 0;

foreach($csv_radky AS $radek)
{
    if(trim($radek) == '')
        continue;
    
    $celkem_zaznamu++;
    
    $r = explode(";", $radek);
    
   // print_r($r);
    //continue;
    
    //nacteni hodnot z radku
    $csv_email = $r[0];
    $csv_jmeno = isset($r[1]) ? $r[1] : "";
    $csv_prijmeni = isset($r[2]) ? $r[2] : "";
    $csv_spolecnost = isset($r[3]) ? $r[3] : "";
    $csv_osloveni = isset($r[4]) ? $r[4] : "";
    
    $csv_pohlavi = isset($r[5]) ? intval($r[5]) : 0;
    $csv_stav = isset($r[6]) ? intval($r[6]) : 1;
    $csv_jazyk = isset($r[7]) ? $lang->get_id_by_name($r[7]) : WEB_LANG_ID;
    $csv_skupiny = isset($r[8]) ? $r[8] : "";
    
    //validace
    if(!TestEmail($csv_email,250,false))
    {
        $chyby++;
        continue;
    }
    
    //prebiti hodnot, nastavenymi hodnotami pri importu
    $csv_pohlavi = $pohlavi > -1 ? $pohlavi : $csv_pohlavi; 
    $csv_stav = $stav > -1 ? $stav : $csv_stav;
    $csv_jazyk = $jazyk > 0 ? $jazyk : $csv_jazyk;
    $csv_skupiny = $skupina > 0 ? (array) $skupina : explode(",",$csv_skupiny);

    
    //overeni zda jiz email v databazi existuje
    $idPrijemce = $db->get(TABLE_EMAILY_PRIJEMCI,"idPrijemce","idDomeny=".$domain->getId()." AND email='".$db->secureString($csv_email)."'");
    
    $update = array(
        "idDomeny" => $domain->getId(),
        "email" => $csv_email,
        "spolecnost" => $csv_spolecnost,
        "jmeno" => $csv_jmeno,
        "prijmeni" => $csv_prijmeni,
        "osloveni" => $csv_osloveni,
        "stav" => $csv_stav,
        "pohlavi" => $csv_pohlavi,
        "idJazyka" => $csv_jazyk
        );
    
    if($idPrijemce == 0)
    {
        $update["datum"] = "now";
        $update["hash"] = md5(uniqid(rand(1,10000)));
            
        $db->insert(TABLE_EMAILY_PRIJEMCI, $update);
        $idPrijemce = $db->lastId();      
        
        $nove++;
    }
    else
    {
        $db->update(TABLE_EMAILY_PRIJEMCI, $update, "idPrijemce=".$idPrijemce);
        $aktualizace++;
    }
    
    //zarazeni prijemce do skupin
    $db->delete(TABLE_EMAILY_SKUPINY_PRIJEMCI, "idPrijemce=".$idPrijemce);
    
    foreach($csv_skupiny AS $ids)
    {
        $ids = intval(trim($ids));
        if($ids == 0)
            continue;
        
        $idr = $db->get(TABLE_EMAILY_SKUPINY_PRIJEMCI,"idRelace","idSkupiny=".$ids." AND idPrijemce=".$idPrijemce);
        if($idr > 0)
            continue;
            
        $insert = array("idPrijemce" => $idPrijemce, "idSkupiny" => $ids);
        $db->insert(TABLE_EMAILY_SKUPINY_PRIJEMCI, $insert);
        
    }
 
    
}

            
if($chyby>0)
    $systemMessage->add_error(sprintf(ERROR_X_CHYBNYCH_ZAZNAMU, $chyby));
                
if($aktualizace > 0)
    $systemMessage->add_ok(sprintf(ERROR_X_ZAZNAMU_NEVLOZENO,$aktualizace));

if($nove > 0)
    $systemMessage->add_ok(sprintf(OK_X_ZAZNAMU_NOVYCH,$nove));
 
                      
$uspech = $celkem_zaznamu - $chyby; 
if($uspech > 0)
    $systemMessage->add_ok(sprintf(OK_ULOZENO_X_Z_CELKOVEHO_POCTU, $uspech,$celkem_zaznamu));
     
$log->add_log('import','newsletter-prijemci');
            
Redirect($this->get_link('prijemci',0,'import'));	



?>