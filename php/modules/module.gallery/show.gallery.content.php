<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */
 
 

if(!defined('SECURITY_CMS')) exit;
if(!$this->is_access('galerie','edit-content',0,1,'content_view')) return;

$idGallery = $this->get_id_action();
$povoleno_nahravani = $login_obj->UserPrivilege('content_add') && $object_access->has_access($idGallery);

if(!$object_access->has_access($idGallery))
{
    $systemMessage->add_warning(TNEMATE_OPRAVNENI_MENIT_OBSAH);
    $systemMessage->show_messages();
}
                  
$main_tools = array();
      
if($login_obj->UserPrivilege('content_view'))
    $main_tools[] = array(
        'ikona' => 'setting', 
        "nazev" => TNASTAVENI_GALERIE, 
        "aktivni" => 0, 
        "odkaz" => $this->get_link('galerie',0,'edit-settings',$idGallery)
        );

if($povoleno_nahravani)
{
    $main_tools[] = array(
        'ikona' => 'copy', 
        "nazev" => TKOPIROVAT_FOTKY, 
        "aktivni" => 0, 
        "odkaz" => "#", 
        "title" => TKOPIROVAT_FOTKY_Z_GALERIE,
        "id"    => "copy_images"
        );
    
    /*    
    $main_tools[] = array(
        'ikona' => 'new', 
        "nazev" => TPRIDAT_FOTKY, 
        "aktivni" => 0, 
        "odkaz" => "#", 
        "title" => TPRIDAT_FOTKY
        //"kod_pred" => '<form method="post" action="" enctype="multipart/form-data"><input type="file" name="file_upload" id="uploadify" /></form>'
        );
    */
}
           
$main_tools[] = array(
    'ikona' => 'back', 
    "nazev" => TZPET, 
    "aktivni" => 0, 
    "odkaz" => $this->get_link('galerie'), 
    "title" => TZPET_NA_SEZNAM_GALERII
    );
        
echo main_tools($main_tools); 
   
echo "<div id='status-message-ok' class='hide'><div class='ok w45'><ul></ul></div></div>";     
echo "<div id='custom-queue'></div>";


showH3(TNAHRANE_FOTKY,'gallery');
		
$data = $db->Query("SELECT g.*, j.jazyk, j.idJazyka
    FROM ".TABLE_STRANKY." AS g
    LEFT JOIN ".TABLE_JAZYKY." AS j ON g.idJazyka = j.idJazyka 
    WHERE idRodice=".$idGallery." 
        AND typ='galerie-fotka' 
    ORDER BY priorita, idStranky DESC");
        

$d = $db->query("SELECT g.idStranky, g.nazev, j.jazyk, j.idJazyka, COUNT(gp.idStranky) AS pocet_fotek
    FROM ".TABLE_STRANKY." AS g
    LEFT JOIN ".TABLE_JAZYKY." AS j ON g.idJazyka = j.idJazyka 
    LEFT JOIN ".TABLE_STRANKY." AS gp ON g.idStranky=gp.idRodice
    WHERE g.typ='galerie' 
        AND g.idDomeny=".$domain->getId()."
    GROUP BY g.idStranky
        " 
        );


$select_copy  = "<select id='selectbox_copy' class='select'>";
$select_copy .= "<option value='0'>-- ".TVYBERTE." --</option>";
    
while($rg = $db->getAssoc($d))
    if($lang->is_allowed($rg['idJazyka']) && $idGallery != $rg['idStranky'])
        $select_copy .= "<option value='".$rg['idStranky']."'>".$rg['nazev']." ".strtoupper($rg['jazyk'])."</option>"; 

$select_copy .= "</select>";

$nazev_galerie = $db->get(TABLE_STRANKY,"nazev","idStranky=".$idGallery);

$ajax_graf =  "
    <div class='horni-tlacitka hide' id='kopirovani_fotek'>
        <div id='ukazatel_prubehu_kopirovani'>
            <div id='progress_bar'>
                <div class='krok'>1. ".TZKOPIROVAT_FOTKY_Z_GALERIE." ".$select_copy." ".TDO_GALERIE." <b>".$nazev_galerie."</b></div>
                <div class='krok'>
                    2. ".TZKOPIROVAT."&nbsp;                    
                    <input type='radio' name='cok' value='vse' id='cok1' checked='checked' class='cok'/>
                    <label for='cok1'>".TVSECHNY_FOTKY."</label> &nbsp;
                    <input type='radio' name='cok' value='vyber' id='cok2' class='cok'/>
                    <label for='cok2'>".TVYBRANE_FOTKY."</label>
                    </div>
                <div id='vyber_fotek'>".TKLIKNUTIM_FOTKU_VYBERETE."<div></div></div>
                <div class='krok'>3. <a href='#' id='spustit_kopirovani'>".TSPUSTIT_KOPIROVANI."</a></div>
                <div id='stav_kontroly_prubeh'>".TPROBIHA_KOPIROVANI_FOTEK."...</div>
                <div id='stav_kontroly_dokonceno'>".TKOPIROVANI_OBRAZKU_UKONCENO." (<a href='#' class='skryt'>".TOPAKOVAT."</a>)</div>
                
            </div>
        </div>
    </div>
    <div class='cleaner'></div>
    ";
	
if($povoleno_nahravani)	
    $ajax_graf .= "<div class='horni-tlacitka' id='upload'>
        <div class='ukazatel_prubehu_kopirovani'>
        
        <p><span class='fa fa-plus'></span> ".TPRIDANI_OBRAZKU_NAPOVEDA."</p>
        
        
        </div>
        </div>
        
        <form method='post' action='' enctype='multipart/form-data'>
            <input type='file' name='btnUpload' id='btnUpload' multiple/>
        </form>
        ";



echo $ajax_graf;

if($db->numRows($data)==0)
    echo "<h2>".TNENALEZEN_ZADNY_OBRAZEK."</h2>";
		
$pocet = $db->numRows($data);
$dis_content = !$login_obj->UserPrivilege('content_view'); 
$dis_delete = !$login_obj->UserPrivilege('content_delete');

$form = new Form();

$html = "<div class='galerie w100' id='sortable'>";


for($i = 0; $i < $pocet; $i++) {
    if (!mysql_data_seek($data, $i)) continue;
    if (!($img = $db->getAssoc($data))) continue;

    $html .= "<div class='image_box' id='img-".$img['idStranky']."'>";
    $html .= "<div class='image_box-in' id='ibi".$img['idStranky']."'>"; 		
    $html .= "<div class='image_edit hide' id='img_ed".$img['idStranky']."'>";
			
            
   $html .= "<span class='image_icon move_icon'>".icon('arrows')."</span>";
   
    if(POVOLIT_DETAIL_FOTKY)
        $html .= "<a class='image_icon' href='".$links->get_url($img['idStranky'])."' target='_blank'>".icon('link')."</a>";
            
    if($dis_content)
        $html .= icon_disabled('content');
        else
        $html .= "<a class='image_icon' href='".$this->get_link('galerie',$idGallery,'edit-image',$img['idStranky'])."' title='".TEDITACE_FOTKY."' >".icon('content')."</a>";
				
    if($dis_delete || !$object_access->has_access($idGallery))
        $html .= icon_disabled('delete');
		else
		$html .= "<a id='delete_foto".$img['idStranky']."' href='#' class='image_icon delete_foto' title='".TSMAZAT."'>".icon('delete')."</a>";
			
			
    $html .= "</div>";
			
    $html .= "<div class='image' title='".TKLIKNUTIM_FOTKU_OZNACITE."'>";
    $html .= "<img src='".RELATIVE_PATH.$domain->getDir().USER_DIRNAME_GALLERY_MINI.$img['obrazek']."' alt='' />";
    $html .= "</div>";
	
    $html .= "<div class='image_desc'>";
	//echo "<span class='popisek' id='p1".$img['idStranky']."' style='display: block'>".($img['nazev']!=''?"<b>".$img['nazev']."</b><br/>":"").cut_text($img['perexBezHtml'],80)."</span>";
    
    $html .= "<textarea name='nazev[".$img['idStranky']."]'>".secureString($img['nazev'])."</textarea>";
    
	$html .= "</div>";
	
    $html .= "</div>";
	$html .= "</div>";
	$vlevo = $img['priorita'];
	}

//$html .= "<div class='cleaner'></div>";	
$html .= "</div>";
$html .= "<div class='cleaner'></div>";

//echo $html;

$form->add_code($html);
$form->add_submit(TSMAZAT_VYBRANE_FOTKY,"btnSmazat","","smazat_vybrane");
$form->add_submit();

echo $form->get_html_code();


?>

<script type='text/javascript'>
<!--

		$(function() {       
          
          // Handle drag and drop events with jQuery
            var obj = $("#upload");
            obj.on('dragenter', function (e) 
            {
                e.stopPropagation();
                e.preventDefault();
            });
            obj.on('dragover', function (e) 
            {
                 e.stopPropagation();
                 e.preventDefault();
            });
            obj.on('drop', function (e) 
            {

                 e.preventDefault();
                 var files = e.originalEvent.dataTransfer.files;
             
                 //We need to send dropped files to Server
                 handleFileUpload(files,obj);
            });
            
            
            //If the files are dropped outside the div, file is opened in the browser window. To avoid that we can prevent �drop� event on document.
            $(document).on('dragenter', function (e) 
            {
                e.stopPropagation();
                e.preventDefault();
            });
            $(document).on('dragover', function (e) 
            {
              e.stopPropagation();
              e.preventDefault();
              //obj.css('border', '2px dotted #0B85A1');
            });
            $(document).on('drop', function (e) 
            {
                e.stopPropagation();
                e.preventDefault();
            });
            
            
            
            //vybrani fotek z disku pocitace
            $("#btnUpload").change(function(){
                var o = $(this);
        
                if(o.length == 0)
                    return;
                        
                var soubory = o[0].files;
                handleFileUpload(soubory,obj);
            })
            
            
            //Read the file contents using HTML5 FormData() when the files are dropped.
            function handleFileUpload(files,obj)
            {
               for (var i = 0; i < files.length; i++) 
               {
                    var fd = new FormData();
                    fd.append('file', files[i]);
                    fd.append("page_id", <?php echo $idGallery;?>);
                    
                    var status = new createStatusbar(obj); //Using this we can set progress.
                    status.setFileNameSize(files[i].name,files[i].size);
                    sendFileToServer(fd,status);
             
               }
            }
            
            //Send FormData() to Server using jQuery AJAX API
            function sendFileToServer(formData,status)
            {
                var uploadURL ="<?php echo AJAX_GATEWAY;?>photos.upload"; //Upload URL
                var extraData ={}; //Extra Data.
                var jqXHR=$.ajax({
                        xhr: function() {
                        var xhrobj = $.ajaxSettings.xhr();
                        if (xhrobj.upload) {
                                xhrobj.upload.addEventListener('progress', function(event) {
                                    var percent = 0;
                                    var position = event.loaded || event.position;
                                    var total = event.total;
                                    if (event.lengthComputable) {
                                        percent = Math.ceil(position / total * 100);
                                    }
                                    //Set progress
                                    status.setProgress(percent);
                                }, false);
                            }
                        return xhrobj;
                    },
                    url: uploadURL,
                    type: "POST",
                    dataType: 'json',
                    contentType:false,
                    processData: false,
                    cache: false,
                    data: formData,
                    success: function(data){
                        status.setProgress(100);
                        h = status.filename.html();
                        if(data.messages.error)
                        {
                            
                            status.filename.html( h + '<br><span class="red">' + data.messages.error + '</span>');
                        }
                        else
                        {
                            status.filename.html( h + '<br><span class="green">' + data.messages.ok + '</span>');
                            $("#sortable").append(data.file.html);
                        }
                        
                        //$("#status1").append("File upload Done<br>");           
                    }
                }); 
             
                status.setAbort(jqXHR);
            }
            
            
            var rowCount=0;
            function createStatusbar(obj)
            {
                 rowCount++;
                 var row="odd";
                 if(rowCount %2 ==0) row ="even";
                 this.statusbar = $("<div class='statusbar "+row+"'></div>");
                 this.filename = $("<div class='filename'></div>").appendTo(this.statusbar);
                 this.size = $("<div class='filesize'></div>").appendTo(this.statusbar);
                 this.progressBar = $("<div class='progressBar'><div></div></div>").appendTo(this.statusbar);
                 this.abort = $("<div class='tlacitko'><a href='#' class='fa fa-times abort'></a></div>").appendTo(this.statusbar);
                 //obj.appendTo(this.statusbar);
                 obj.find("p").after(this.statusbar);
             
                this.setFileNameSize = function(name,size)
                {
                    var sizeStr="";
                    var sizeKB = size/1024;
                    if(parseInt(sizeKB) > 1024)
                    {
                        var sizeMB = sizeKB/1024;
                        sizeStr = sizeMB.toFixed(2)+" MB";
                    }
                    else
                    {
                        sizeStr = sizeKB.toFixed(2)+" KB";
                    }
             
                    this.filename.html(name);
                    this.size.html(sizeStr);
                }
                this.setProgress = function(progress)
                {       
                    var progressBarWidth =progress*this.progressBar.width()/ 100;  
                    this.progressBar.find('div').animate({ width: progressBarWidth }, 10).html(progress + "% ");
                    if(parseInt(progress) >= 100)
                    {
                        this.abort.hide();
                    }
                }
                this.setAbort = function(jqxhr)
                {
                    var sb = this.statusbar;
                    this.abort.click(function(e)
                    {
                        e.stopPropagation();
                        jqxhr.abort();
                        sb.hide();
                        return false;
                    });
                }
            }
            
            
            
          
          
          
          
          
    		$('#sortable').sortable({
    			connectWith: '.image_box', 
                handle: ".move_icon",
    			opacity: 0.8, 
    			cursor: 'move', 
    			<?php if(!$login_obj->UserPrivilege('content_edit') || !$object_access->has_access($idGallery)) echo "disabled: true,"; ?>
    			placeholder: 'image-placeholder',
    			update: function(){
    						var idGallery = <?php echo $idGallery; ?>;
    						var sort = $(this).sortable('serialize');
    						sort += '&idGallery='+idGallery;
    	
    						$.post('<?php echo AJAX_GATEWAY; ?>photos.sort', sort, function(message) {
    							$('#message').html(message);
    		
    							});
    				 	}
    
    				}).disableSelection();
			
            $("#sortable").enableSelection();
            
            $("#sortable").on("click","a.delete_foto",function(){
                
                var idGallery = <?php echo $idGallery;?>;
                var idItem = $(this).attr('id').substring(11);

            	custom_confirm('<?php echo TOPRAVDU_SMAZAT_TENTO_OBRAZEK;?>',function(){
            	   $.msg();
            		$.post('<?php echo AJAX_GATEWAY;?>photos.delete', {idGallery: idGallery, idItem: idItem}, function(data) {
            			if(data!='')
                        {
            				$('#img-'+idItem).fadeOut("fast", function(){
            				    $(this).remove();
                                $.msg( 'setClass', 'ok_message' );
                                $.msg( 'replace', data );
                                
            				});
                        }
                        
                        $.msg( 'unblock', 800);
            		
            			});
            		})
                return false;
            })
		
        
            $("#sortable").on("mouseenter",".image_box-in",
                function(){
                    var ids = $(this).attr("id").substring(3);
    				lightness('img-' + ids);
                    $('#ibi' + ids).addClass('light'); 
	       			$('#img_ed'+ids).show(); 
		      		$('#p2'+ids).show();            
                    }
                )
                
            $("#sortable").on("mouseleave",".image_box-in",
                function(){
                    var ids = $(this).attr("id").substring(3);
                    $('#ibi' + ids).removeClass('light'); 
				    $('#img_ed'+ids).hide();
				    $('#p2'+ids).hide();
                    
                    }
                    
                
                )
            
            $("#smazat_vybrane").hide();
    
            $("#smazat_vybrane").click(function(){
                
                var idItem = [];
                $(".selected").each(function(){
                    id = $(this).find(".image_box-in").attr("id").substring(3);
                    idItem.push(id);
                })
                
                var idGallery = <?php echo $idGallery;?>;
                

            	custom_confirm('<?php echo TOPRAVDU_SMAZAT_VYBRANE_FOTKY;?>',function(){
            	   $.msg();
            		$.post('<?php echo AJAX_GATEWAY;?>photos.delete', {idGallery: idGallery, idItem: idItem}, function(data) {
            		  
            			if(data!='')
                        {
                            for	(index = 0; index < idItem.length; index++) {
                				$('#img-'+idItem[index]).fadeOut("fast", function(){
                				    $(this).remove();
                                    
                                    
                				});
                            }
                            
                            $.msg( 'setClass', 'ok_message' );
                            $.msg( 'replace', data );
                            
                            
                        }
                        
                        $.msg( 'unblock', 800);
            		
            			});
                        
            		})
                
                return false;
            })
                
            $("#sortable").on("click",".image",function(){
                box = $(this).parents(".image_box").first();
                if(box.hasClass("selected"))
                    box.removeClass("selected");
                    else
                    box.addClass("selected");
                
                l = $(".selected").length;
                
                if(l > 0)
                    $("#smazat_vybrane").val("<?php echo TSMAZAT_VYBRANE_FOTKY;?> (" + l + ")").show();
                    else
                    $("#smazat_vybrane").hide();
                
                return false;
            })
                      
            
                

      
    //vraci true pokud jsou vybrany vsechny fotky
    function vse()
    {
        return $("input.cok:checked").val() == 'vse'; 
    }
  
    $("#selectbox_copy").change(function(){
        var idg = $(this).val();
        if(idg == 0)
            $("#vyber_fotek").html("");
            else
            $.post("<?php echo AJAX_GATEWAY;?>photos.get",{id_gallery: idg},function(data){
                $("#vyber_fotek div").html(data);
            })
    })
    
    
    if(vse()) 
        $("#vyber_fotek").hide();
        else 
        $("#vyber_fotek").show();
    
    $("input.cok").change(function(){
        if(vse()) 
            $("#vyber_fotek").hide();
            else 
            $("#vyber_fotek").show();           
            
    })
    
    
    $(document).on("click","a.nahled_obrazku", function(){
        $(this).toggleClass("selected");
        return false;
    })
       
   $("#copy_images a").click(function(e){
    e.preventDefault();
    $("#kopirovani_fotek").slideDown();
    
   })
        
    $("a#spustit_kopirovani").click(function(){

        $.msg();
        var zkopirovano_fotek = 0;
        var procenta = 0;
        var vsechny_fotky = vse();
        
        
        var zdrojova_galerie = $("#selectbox_copy").val();       
        var photos = new Array();
        
        var i = 0;
        var first_id = 0;
        $("#vyber_fotek div a").each(function(){
            if($(this).hasClass("selected") || vsechny_fotky)
            {
                var id = $(this).attr("id").substring(2);
                photos[i] = id;
                if(i == 0) 
                    first_id = id;
                    
                i++;
            }
 
        });
    
            
        $.post("<?php echo AJAX_GATEWAY;?>photos.copy", {photos: photos, id_gallery_target: <?php echo $idGallery?>, id_gallery_source: zdrojova_galerie} , function(data){
            if(data.ok != '')
            {
                $.msg( 'setClass', 'ok_message' );
                $.msg( 'replace', data.ok );
                $.msg( 'unblock', 4000);
                setTimeout(function(){location.reload();}, 4000);
                ; 
            }
            else
            {
                $.msg( 'setClass', 'error_message' );
                $.msg( 'replace', data.error );
                $.msg( 'unblock', 4000);
            }
                             
        },'json')
        return false;
            
    })
           
    $("#upload").click(function(){
        $('input#btnUpload').trigger('click');
        return true;
    })


    
		
        
        
});
 
 
// -->       
</script>