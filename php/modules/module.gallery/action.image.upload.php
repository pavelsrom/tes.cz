<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined('SECURITY_CMS')) exit;
if(!is_post('btnUploadSouboru') ||
    !$this->is_access('galerie','edit-content',0,1,'content_add') 
    ) return;

$idGalerie = $this->get_id_action();
$nazev_galerie = $db->get(TABLE_STRANKY,'nazev','idStranky='.$idGalerie);


$pocet_nahranych=0;
for($i=0; $i<MAX_UPLOAD_FILES; $i++)
{
    $soubor = $db->secureString($_FILES['file']['name'][$i]);
    $nazev = $db->secureString(strip_tags($_POST['nazev'][$i]));
    $popis = $db->secureString($_POST['popis'][$i]);
				
    if($_FILES['file']['tmp_name'][$i] == '' || $_FILES['file']['name'][$i] == '')
        continue;
					
				
    $upload_obrazek = array(
        'name' => $_FILES['file']['name'][$i],
        'type' => $_FILES['file']['type'][$i],
        'tmp_name' => $_FILES['file']['tmp_name'][$i],
        'error' => $_FILES['file']['error'][$i],
        'size' => $_FILES['file']['size'][$i]
        );
                
    $nazev_souboru_arr = explode('.', $upload_obrazek['name']);
    $nazev_souboru = $nazev_souboru_arr[0];
					
    $articles_path_mini = $domain->getDir().USER_DIRNAME_GALLERY_MINI;
    $articles_path_stredni = $domain->getDir().USER_DIRNAME_GALLERY_STREDNI;
    $articles_path_maxi = $domain->getDir().USER_DIRNAME_GALLERY_MAXI;
             
                
    if(GetUploadError($upload_obrazek['error']) != '') 
        continue;
                    
    include_once('php/classes/class.upload.php');
                
    $db->Query("INSERT INTO ".TABLE_STRANKY." (idDomeny,typ, obsah, obsahBezHtml,zobrazit, idRodice,doMenu, description, datum) VALUES (".$domain->getId().",'galerie-fotka','".$popis."','".strip_tags($popis)."',1,".$idGalerie.",0,'".strip_tags($popis)."',NOW()) ");
                
    $idObrazku = $db->lastId();
    $file = $idObrazku."_".$nazev_souboru.".jpg";
                    
    $uploader = new upload($upload_obrazek, 'cs_CS');
    $uploader->file_new_name_body = $idObrazku."_".$nazev_souboru;
    $uploader->file_overwrite = true;
    $uploader->image_resize = true;
    $uploader->image_x = intval(OBRAZEK_GALERIE_MAXI_X);
    $uploader->image_y = intval(OBRAZEK_GALERIE_MAXI_Y);
    $uploader->image_ratio = true;
    $uploader->image_convert = 'jpg';
    $uploader->process($articles_path_maxi);
                
    $uploader->file_new_name_body = $idObrazku."_".$nazev_souboru;
    $uploader->image_resize = true;
    $uploader->image_convert = 'jpg';
    $uploader->image_x = intval(OBRAZEK_GALERIE_MEDIUM_X);
    $uploader->image_y = intval(OBRAZEK_GALERIE_MEDIUM_Y);
    $uploader->process($articles_path_stredni);
                    
    $uploader->file_new_name_body = $idObrazku."_".$nazev_souboru;
    $uploader->image_resize = true;
    $uploader->image_convert = 'jpg';
    $uploader->image_x = intval(OBRAZEK_GALERIE_MINI_X);
    $uploader->image_y = intval(OBRAZEK_GALERIE_MINI_Y);
    $uploader->process($articles_path_mini);
                   
    $uploader->clean();

                
    if($nazev == '')
        $nazev = $file;

    $url = ModifyUrl($nazev);
                
    //overeni url nazvu stranky, pokud existuje, tak se automaticky priradi novy url nazev
    if($domain->data['urlStruktura']=='nazev')
    {  
        $where = "";
        if($idObrazku>0)
        $where = "AND idStranky != ".$idObrazku; 
                        
        $verify_url_name = $db->Query("SELECT url FROM ".TABLE_STRANKY." WHERE url LIKE '".$url."%' AND idDomeny=".$domain->getId()." ".$where." ORDER BY url");
                    
        $url_zal = $url;
        $j=1;
        while($u = $db->getAssoc($verify_url_name))
        {
            if($url_zal==$u['url'])
                $url_zal = $url."-".$j;
            $j++;
        }
                    
        if($url!=$url_zal)
            $systemMessage->add_warning(WARNING_URLNAZEV_MODIFIKOVAN." ".$url_zal);
                        
        $url = $url_zal;
                    
    }
                
    $nazev_galerie = $db->get(TABLE_STRANKY,"nazev","idStranky=".$idGalerie);
                
    $db->Query("UPDATE ".TABLE_STRANKY." SET obrazek='".$file."', url='".$url."', nazev='".$nazev."', nazevProMenu='".$nazev."', nadpis='".$nazev."', title='".$nazev_galerie." - ". $nazev."' WHERE idStranky = ".$idObrazku." AND idDomeny=".$domain->getId()." LIMIT 1");
                
    $pocet_nahranych++;
    $systemMessage->add_ok($_FILES['file']['name'][$i]." - ".OK_ULOZENO);
    
    $log->add_log('upload','galerie-fotka',$idObrazku,$nazev_galerie);   
                
    }
							
$url = $this->get_link('galerie',0,'edit-content',$idGalerie);
Redirect($url);

?>