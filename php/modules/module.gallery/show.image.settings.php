<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined('SECURITY_CMS')) exit;
if(!$this->is_access('galerie','edit-image',1,1,'content_edit')) return;

$idGallery = $this->get_id_page();
$idItem = $this->get_id_action();
        
if($idGallery == 0 || $idItem==0) return;
		
if(!$object_access->has_access($idGallery))
{
    $systemMessage->add_warning(TNEMATE_OPRAVNENI_MENIT_OBSAH);
    $systemMessage->show_messages();
}
        
$data = $db->Query("SELECT s.nazev, s.obsah, s.url, s.zobrazit, s.obrazek, IF(s.datum IS NOT NULL, DATE_FORMAT(s.datum,'%d.%m.%Y, %H:%i' ), '') AS vytvoreno, j.jazyk
			FROM ".TABLE_STRANKY." AS s 
            LEFT JOIN ".TABLE_JAZYKY." AS j ON s.idJazyka = j.idJazyka 
			WHERE s.idRodice=".$idGallery."
                AND s.idStranky=".$idItem."
				AND s.idDomeny=".$domain->getId()."
				AND s.typ = 'galerie-fotka'
			LIMIT 1
			");
			
if($db->numRows($data)==0) 
{
    echo TNENALEZENO;
    return;
}
        
$img = $db->getAssoc($data);
		
$main_tools = array();
            
$main_tools[] = array(
    'ikona' => 'preview', 
    "nazev" => TNAHLED, 
    "aktivni" => 0, 
    "odkaz" => UrlPage($img['url'], $idItem, $img['jazyk'])
    );
    
$main_tools[] = array(
    'ikona' => 'back', 
    "nazev" => TZPET_NA_OBSAH, 
    "aktivni" => 0, 
    "odkaz" => $this->get_link('galerie',0,'edit-content',$idGallery),
    "title" => TZPET_NA_OBSAH_GALERIE
    );
    
$main_tools[] = array(
    'ikona' => 'back', 
    "nazev" => TZPET_NA_SEZNAM, 
    "aktivni" => 0, 
    "odkaz" => $this->get_link('galerie'),
    "title" => TZPET_NA_SEZNAM_GALERII
    );
            
            
echo main_tools($main_tools);

$form = new Form();  
$form->add_section(TZAKLADNI_UDAJE,"home");        
$form->add_plain_text(TFOTKA,"<img src='".RELATIVE_PATH.$domain->getDir().USER_DIRNAME_GALLERY_STREDNI.$img['obrazek']."' class='input w100'/>")->set_class_td('center');
$form->add_text(TNAZEV, 'nazev',$img['nazev']);
$form->add_editor(TPOPIS, 'obsah', $img['obsah'],0,false,'wysiwygEditor h300');
$form->add_radiobuttons(TZOBRAZOVAT,'zobrazit',$img['zobrazit'],array(1=>TANO,0=>TNE),55);
$form->add_hidden('nazev_obrazku',$img['obrazek']);
        
$res = $login_obj->UserPrivilege('content_edit') && $object_access->has_access($idGallery);
$form->add_submit()->set_disabled(!$res);

echo $form->get_html_code();

?>