<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */


//------------------------------------------------
//definice polozek menu
//------------------------------------------------

$module_menu = array();
$module_menu[] = array(
    "page"  => "galerie",
    "name"  => TSEZNAM_GALERII." ".WEB_LANG
    );
    
if($login_obj->UserPrivilege('settings_view'))
$module_menu[] = array(
    "page"  => "nastaveni",
    "name"  => TNASTAVENI
    );
    
//------------------------------------------------
//definice nadpisu h1 a textu k nadpisu
//------------------------------------------------ 
$module_h1 = array();
$module_h1[] = array(
    "page"  => "galerie",
    "action"=> "edit-settings",
    "verify_id_action" => 1,
    "name"  => TEDITOVAT_NASTAVENI_GALERIE_SLOZKY." ".WEB_LANG
    );
$module_h1[] = array(
    "page"  => "galerie",
    "action"=> "edit-content",
    "verify_id_action" => 1,
    "name"  => TOBSAH_GALERIE." ".WEB_LANG,
    "text"  => 296
    );
$module_h1[] = array(
    "page"  => "galerie",
    "action"=> "new",
    "name"  => TVYTVORIT_GALERII." ".WEB_LANG
    );    
$module_h1[] = array(
    "page"  => "galerie",
    "name"  => TSEZNAM_GALERII." ".WEB_LANG,
    "text"  => 358
    ); 
$module_h1[] = array(
    "page"  => "galerie",
    "action"=> "new-folder",
    "name"  => TVYTVORIT_SLOZKU." ".WEB_LANG
    ); 

$module_h1[] = array(
    "page"  => "galerie",
    "action"=> "edit-image",
    "verify_id_action" => 1,
    "verify_id_page" => 1,
    "name"  => TEDITACE_FOTKY
    );
$module_h1[] = array(
    "page"  => "nastaveni",
    "name"  => TNASTAVENI
    );



?>