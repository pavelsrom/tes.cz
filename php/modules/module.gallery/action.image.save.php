<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined('SECURITY_CMS')) exit;
if(!is_action() || !$this->is_access('galerie','edit-image',1,1,'content_edit') || !$object_access->has_access($this->get_id_page())) return;


$id = $this->get_id_action();
$idGalerie = $this->get_id_page();
$zobrazit = get_int_post('zobrazit');
$popis = get_post('obsah',"",false);
$nazev = get_post('nazev');
$nazev_obrazku = get_post('nazev_obrazku');
            
if($nazev == '') $nazev = $nazev_obrazku;
if($nazev == '') $nazev = $id.$idGalerie;
                       
$url = ModifyUrl($nazev);
$idObrazku = $id;
             
//overeni url nazvu stranky, pokud existuje, tak se automaticky priradi novy url nazev
$url = $idObrazku==0 && $url=='' ? ModifyUrl($nazev) : ModifyUrl($url);
$url_new = get_url_nazev($idObrazku, $url);
if($url!=$url_new)
    $systemMessage->add_warning(WARNING_URLNAZEV_MODIFIKOVAN." ".$url_new);
$url = $url_new;
        
if($url == "")
    $systemMessage->add_error(ERROR_NEPLATNY_URL_NAZEV);
   
$update = array(
    "nazev"         => $nazev,
    "nazevProMenu"  => $nazev,
    "obsah"         => $popis,
    "obsahBezHtml"  => strip_tags($popis),
    "url"           => $url,
    "nadpis"        => $nazev,
    "title"         => $nazev,
    "zobrazit"      => $zobrazit,
    "description"   => strip_tags($popis)    
    );         
              
$update['idAktualizoval'] = $login_obj->getId();
$update['jmenoAktualizoval'] = $login_obj->getName();
$update['datumAktualizoval'] = "now";
      
$db->update(TABLE_STRANKY,$update,"idRodice=".$idGalerie." AND idStranky=".$id." AND idDomeny=".$domain->getId());
			
$nazev = $db->get(TABLE_STRANKY,'nazev',"idStranky=".$idGalerie);
$log->add_log('edit-content','galerie-fotka',$id,$nazev);   

$systemMessage->add_ok(OK_ULOZENO);
$url = $this->get_link('galerie',$idGalerie,'edit-image',$id);
Redirect($url);

?>