<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined('SECURITY_CMS')) exit;
if(!is_action() || !$this->is_access('galerie','edit-content',0,1,'content_edit') || !$object_access->has_access($this->get_id_action())
    ) return;
    
$idGalerie = $this->get_id_action();
$nazvy = get_array_post("nazev");

foreach($nazvy AS $idObrazku => $nazev)
{
    if($nazev == '') 
        $nazev = $idObrazku."-".$idGalerie;
    
    $url = ModifyUrl($nazev);

    //overeni url nazvu stranky, pokud existuje, tak se automaticky priradi novy url nazev
    $url = $idObrazku==0 && $url=='' ? ModifyUrl($nazev) : ModifyUrl($url);
    $url_new = get_url_nazev($idGalerie, $url);
    //if($url!=$url_new)
    //  $systemMessage->add_warning(WARNING_URLNAZEV_MODIFIKOVAN." ".$url_new);
    $url = $url_new;
            
    //if($url == "")
    //    $systemMessage->add_error(ERROR_NEPLATNY_URL_NAZEV);
    
    
    if($systemMessage->error_exists())
        continue;
       
    $update = array(
        "nazev"         => $nazev,
        "nazevProMenu"  => $nazev,
        "url"           => $url,
        "nadpis"        => $nazev,
        "title"         => $nazev
        );         
                  
    $update['idAktualizoval'] = $login_obj->getId();
    $update['jmenoAktualizoval'] = $login_obj->getName();
    $update['datumAktualizoval'] = "now";
          
    $db->update(TABLE_STRANKY,$update,"idRodice=".$idGalerie." AND idStranky=".$idObrazku." AND idDomeny=".$domain->getId());
    			
    $nazev = $db->get(TABLE_STRANKY,'nazev',"idStranky=".$idGalerie);
    $log->add_log('edit-content','galerie-fotka',$idObrazku,$nazev); 
    
         
    
    
}

$systemMessage->add_ok(OK_ULOZENO);         
   
Redirect($_SERVER['HTTP_REFERER']);	

?>