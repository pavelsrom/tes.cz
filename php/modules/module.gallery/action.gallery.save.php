<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined('SECURITY_CMS')) exit;
if(!is_action() ||
    (!$this->is_access('galerie','edit-settings',0,1,'content_edit') &&
     !$this->is_access('galerie','new',0,0,'content_add')) &&
     !$this->is_access('galerie','new-folder',0,0,'content_add')
     || !$object_access->has_access($this->get_id_action())
    ) return;
    
$idStranky = $this->get_id_action();
$idRodice = get_int_post('idRodice');
				
$autentizace = get_int_post("autentizace");
$idVlastnika = $login_obj->getId();
$idProfiluPanelu = get_int_post('idProfiluPanelu',-1);
$stav = get_int_post('zobrazit');
$idDomeny = $domain->getId();
$idGalerie = get_int_post('idGalerie');    
$idAnkety = get_int_post('idAnkety');
$diskuze = get_int_post('diskuze');
$obsah = get_post('obsah');       
$nazev = get_post('nazev');
$menu_nazev = $nazev;
$typ_stranky = get_post('typ','galerie');
$nadpis = get_post('nadpis');			
$url = get_post('url');
$menu_zobr = 0;
$title = get_post('title');
$description = get_post('description');

$url = $idStranky==0 && $url=='' ? ModifyUrl($nazev) : ModifyUrl($url);
$url_new = get_url_nazev($idStranky, $url);
if($url!=$url_new)
    $systemMessage->add_warning(WARNING_URLNAZEV_MODIFIKOVAN." ".$url_new);
$url = $url_new;

if($url == "")
    $systemMessage->add_error(ERROR_NEPLATNY_URL_NAZEV);

if(!TestLength($nazev, 150, false))
    $systemMessage->add_error(ERROR_NEPLATNY_NAZEV);
                
if(!TestLength($nadpis, 150, false))
    $systemMessage->add_error(ERROR_NEPLATNY_NADPIS);
	
if(!TestLength($title, 100, true))
    $systemMessage->add_error(ERROR_NEPLATNY_TITULEK);
				
if(!TestLength($description, 200, true))
    $systemMessage->add_error(ERROR_NEPLATNY_DESCRIPTION);
				
if(!TestUrlPage($url) || ($url=='' && $idStranky > 0)) 
    $systemMessage->add_error(ERROR_NEPLATNY_URL_NAZEV);		
            	
if($systemMessage->error_exists()) return;
        
$hloubka = $db->get(TABLE_STRANKY,"(hloubka + 1)","idStranky=".$idRodice." AND idDomeny=".$domain->getId()." AND typ='galerie-slozka'");
$hloubka = intval($hloubka);
           
$update = array(
    "nazev"         => $nazev,
    "nadpis"        => $nadpis,
    "nazevProMenu"  => $nadpis,
    "title"         => $title,
    "description"   => $description,
    "url"           => $url,
    "zobrazit"      => $stav,
    "doMenu"        => $menu_zobr,
    "autentizace"   => $autentizace,
    "idProfiluPanelu"=>$idProfiluPanelu,
    "typ"           => $typ_stranky,
    "idRodice"      => $idRodice,
    "hloubka"       => $hloubka,
    "idGalerie"     => $idGalerie,
    "idAnkety"      => $idAnkety,
    "diskuze"       => $diskuze,
    "obsah"         => $obsah,
    "obsahBezHtml"  => strip_tags($obsah),
    "idDomeny"      => $idDomeny,
    "idJazyka"      => WEB_LANG_ID
    );

		
if($idStranky > 0)
{
    $update['idAktualizoval'] = $login_obj->getId();
    $update['jmenoAktualizoval'] = $login_obj->getName();
    $update['datumAktualizoval'] = "now";
	$db->update(TABLE_STRANKY,$update,"idStranky=".$idStranky." AND idDomeny=".$idDomeny);
    }
	else 
	{
    $update['idVytvoril'] = $login_obj->getId();
    $update['jmenoVytvoril'] = $login_obj->getName();
    $update['datumVytvoril'] = "now";
    
    $update['idAktualizoval'] = $login_obj->getId();
    $update['jmenoAktualizoval'] = $login_obj->getName();
    $update['datumAktualizoval'] = "now";
    
    
    $db->insert(TABLE_STRANKY,$update);
	$idStranky = $db->lastId();	
    $object_access->add_allowed_object($idStranky);
	}

$skupiny = get_array_post('skupiny');
$db->delete(TABLE_STRANKY_SKUPINY,"WHERE idStranky=".$idStranky);
if(count($skupiny)>0)
{
    foreach($skupiny AS $s)
    {
        $insert = array("idStranky" => $idStranky, "idSkupiny" => $s);
        $db->insert(TABLE_STRANKY_SKUPINY,$insert);
    }
}



if(MODULE_CONTENT_BACKUP)
{
    $content_backup = new ContentBackup($domain->getId(),$idStranky,$login_obj->getId());
    $content_backup->AddStore($obsah);
}

$chybne_odkazy_indik = false;

if(MODULE_LINK_CHECKER)
{
    $chybne_odkazy = new LinksChecker($domain->getId(), $idStranky, "stranka", $obsah, false, KONTROLOVAT_ODKAZY_PRI_UKLADANI);
    $chybne_odkazy->set_base_url( $domain->getUrl() );
    $chybne_odkazy->process();
    $pocet_chybnych_obrazku = $chybne_odkazy->get_sum_error_images();
    $pocet_chybnych_odkazu = $chybne_odkazy->get_sum_error_links();
    
    if($pocet_chybnych_obrazku > 0)
        $systemMessage->add_warning(TPOCET_CHYBNYCH_OBRAZKU_NA_STRANCE.": ".$pocet_chybnych_obrazku);
        
    if($pocet_chybnych_odkazu > 0)
        $systemMessage->add_warning(TPOCET_CHYBNYCH_ODKAZU_NA_STRANCE.": ".$pocet_chybnych_odkazu);
    
    if(!$chybne_odkazy_indik && ($pocet_chybnych_obrazku > 0 || $pocet_chybnych_odkazu > 0)) 
        $chybne_odkazy_indik = true;
        
                
}


if($chybne_odkazy_indik)
    $systemMessage->add_warning(TODKAZY_ZKONTROLUJTE_A_OPRAVTE);
    
$log->add_log($this->get_url('id_action') == 0 ? 'create':'edit-settings',$typ_stranky,$idStranky,$nazev);    

$systemMessage->add_ok(OK_ULOZENO);         


set_next_action_code();

$dalsi_akce = get_next_action_code();
if($dalsi_akce == 0) //zpet na vypis
    $url = $this->get_link('galerie');
elseif($dalsi_akce == 1) //zpet na editaci
    $url = $this->get_link('galerie',0,'edit-settings',$idStranky);
elseif($dalsi_akce == 2) //vytvorit dalsi
    $url = $this->get_link('galerie',0,'new');
   
   
Redirect($url);	

?>