<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined('SECURITY_CMS')) exit;
if(!$this->is_access('galerie','edit-settings',0,1,'content_view') &&
    !$this->is_access('galerie','new',0,0,'content_add') &&
    !$this->is_access('galerie','new-folder',0,0,'content_add')
    ) return;

$idGallery = $this->get_id_action();

if(!$object_access->has_access($idGallery))
{
    $systemMessage->add_warning(TNEMATE_OPRAVNENI_MENIT_OBSAH);
    $systemMessage->show_messages();
}


$typ = $this->get_action()=='new' ? 'galerie' : "";
$typ = $this->get_action()=='new-folder' ? 'galerie-slozka' : $typ;

$main_tools = array();

if($idGallery > 0) {
			$record1 = $db->Query("SELECT g.*
            FROM ".TABLE_STRANKY." AS g
			WHERE g.idStranky=".$idGallery." 
				AND g.idDomeny=".$domain->getId()."
                AND g.idJazyka=".WEB_LANG_ID."
			LIMIT 1");
            
			$gallery = $db->getAssoc($record1);

            $typ = $gallery['typ'];

            if($login_obj->UserPrivilege('content_view') && $typ == 'galerie')
                $main_tools[] = array(
                    'ikona' => 'content', 
                    "nazev" => TOBSAH_GALERIE, 
                    "aktivni" => 0, 
                    "odkaz" => $this->get_link('galerie',0,'edit-content',$idGallery)
                    );

			}
			else
            {
			$gallery['nazev'] = "";
            $gallery['nadpis'] = "";
            $gallery['url'] = "";
            $gallery['title'] = "";
            $gallery['description'] = "";
            $gallery['idProfiluPanelu'] = 0;
            $gallery['typ'] = $typ;
			$gallery['zobrazit'] = 1;
            $gallery['idRodice'] = 0;
			$gallery['obsah'] = "";
            $gallery['idAnkety'] = 0;
            $gallery['idFormulare'] = 0;
            $gallery['diskuze'] = 0;
            $gallery['autentizace'] = 0;
			}
           
$main_tools[] = array(
    'ikona' => 'back', 
    "nazev" => TZPET, 
    "aktivni" => 0, 
    "odkaz" => $this->get_link('galerie'), 
    "title" => TZPET_NA_SEZNAM_GALERII
    );
                    
echo main_tools($main_tools);
       
        
$d = $db->Query("SELECT idStranky FROM ".TABLE_STRANKY." WHERE typ='stranka-galerie' AND idDomeny=".$domain->getId()." AND idJazyka=".WEB_LANG_ID." LIMIT 1");
$idRoot = 0;
while($root = $db->getAssoc($d))
    $idRoot = $root['idStranky'];
        
$slozky_selectbox = get_gallery($idGallery);
$slozky_selectbox = array($idRoot => "-- ".TKOREN_GALERIE." --") + $slozky_selectbox;


$form = new Form();        
$form->add_section(TZAKLADNI_UDAJE,'home');
$form->add_text(TNAZEV,'nazev',$gallery['nazev'],0,true,"",'nazev');
$form->add_text(THLAVNI_NADPIS_STRANKY,'nadpis',$gallery['nadpis'],3,true,"",'nadpis');
$form->add_selectbox(TZANORENI_DO_SLOZKY, 'idRodice',$gallery['idRodice'],$slozky_selectbox,267);

/*
$d = $db->query("SELECT idStranky, g.nazev, j.jazyk, j.idJazyka
    FROM ".TABLE_STRANKY." AS g
    LEFT JOIN ".TABLE_JAZYKY." AS j ON g.idJazyka=j.idJazyka 
    WHERE typ='galerie' 
        AND idDomeny=".$domain->getId()."
        " 
        );

$sel = array(0 => "-- ".TVYBERTE." --");
while($rg = $db->getAssoc($d))
    if($lang->is_allowed($rg['idJazyka']) && $idGallery != $rg['idStranky'])
        $sel[$rg['idStranky']] = $rg['nazev']." ".strtoupper($rg['jazyk']); 

$form->add_selectbox(TPOUZIT_FOTKY_Z_GALERIE,'kopie',0,$sel,382);
*/


$form->add_radiobuttons(TZOBRAZOVAT,'zobrazit',$gallery['zobrazit']);

$form->add_section(TOBSAH, "content");
$form->add_editor(TOBSAH,'obsah',$gallery['obsah'],0,false,"wysiwygEditor h500")->allow_wet();   

       
  
if(MODULE_INQUIRY){
    $d = $db->Query("SELECT idAnkety AS id, nazev FROM ".TABLE_ANKETY." WHERE idDomeny=".$domain->getId());
    $sel = array(0 => "-- ".TVYBERTE." --");
    $pocet = $db->numRows($d);
            
    while($s = $db->getAssoc($d))
        $sel[$s['id']] = $s['nazev'];
                
    if(count($sel) == 1){
        $text_neni = "";
        if($login_obj->UserPrivilege('content_add'))
            $text_neni .= tlacitko(get_link('inquiry',"ankety",0,'new'));
        
        $form->add_plain_text(TANKETA, $text_neni);    
        }
        else
        $form->add_selectbox(TANKETA, 'idAnkety', $gallery['idAnkety'], $sel);
            
    }
    else
    $form->add_hidden('idAnkety', $gallery['idAnkety']);
        
       
    
if(MODULE_DISCUSSION)
    $form->add_radiobuttons(TDISKUZE, 'diskuze', $gallery['diskuze'], array(1 => TANO, 0=>TNE));
    else
    $form->add_hidden('diskuze', $gallery['diskuze']);


$data_panely = $db->Query("SELECT idProfilu,nazev FROM ".TABLE_PANELY_SABLONY." WHERE idJazyka=".WEB_LANG_ID." AND idDomeny=".$domain->getId()." AND zobrazit=1 ORDER BY vychozi DESC ");
                    
$profily = array();
while($profil = $db->getAssoc($data_panely))
    $profily[$profil['idProfilu']] = $profil['nazev'];
    
$form->add_section(TSABLONA_PANELU, 'panel-setting');
$form->add_selectbox(TSABLONA_PANELU, 'idProfiluPanelu', $gallery['idProfiluPanelu'], $profily, 135);          
show_private_pages_settings($idGallery, $gallery, $form);
        
if($login_obj->UserPrivilege('seo'))
{
    $form->add_section(TSEO_PARAMETRY, 'seo', "", 8);
    $form->add_text(TURL_NAZEV, 'url', $gallery['url'], 9, $idGallery > 0);
    $form->add_text(TTITLE, 'title', $gallery['title'], 10,false,'','title');
    $form->add_textarea(TDESCRIPTION, 'description', $gallery['description'], 11);	
}
else
{
    $form->add_hidden('url', $gallery['url']);
    $form->add_hidden('title', $gallery['title'], 'title');
    $form->add_hidden('description', $gallery['description']);	
}

$form->add_hidden('typ',$gallery['typ']);

$res = (($login_obj->UserPrivilege('content_edit') && $idGallery>0) || ($login_obj->UserPrivilege('content_add') && $idGallery==0)) && $object_access->has_access($idGallery);

$form->add_next_actions();
$form->add_submit()->set_disabled(!$res);
    
echo $form->get_html_code();

?>

<script type="text/javascript">
<!--
$(function(){
    
    var idg = <?php echo $idGallery;?>;
    
    $('#nazev').change(function(){
        if(idg > 0) return;
        var n = $(this).val();
        $('#menu_nazev, #nadpis, #title').val(n);
                
        });
    
})
// -->
</script>
