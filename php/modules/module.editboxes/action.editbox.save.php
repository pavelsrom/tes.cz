<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined('SECURITY_CMS')) exit;
if(!is_action() || 
    (!$this->is_access('editboxy','edit-content',0,1,'content_edit') &&
   !$this->is_access('editboxy','new',0,0,'content_add') &&
   !$this->is_access('editboxy','edit-content-kolotoc',0,1,'content_edit') &&
   !$this->is_access('editboxy','new-kolotoc',0,0,'content_add') 
    ) || !$object_access->has_access($this->get_id_action(),"editbox")
    ) return;
   
    
$idPolozky = $this->get_id_action();
$zobrazit = get_int_post('zobrazit');
$povolit_mazani = get_int_post('povolit_mazani');
$sledovatProkliky = get_int_post('sledovatProkliky');
$nazev = get_post('nazev');
$obsah = get_post('obsah');
$od = get_post('od');
$do = get_post('do');
$stridani = get_post('stridani','zadne');
$prodleva = get_int_post('prodleva',5);
$rychlost_efektu = get_int_post('rychlostEfektu',1);
$efekt = get_post('efekt','none');  
$editboxy = get_array_post('editboxy');

$file = get_post('obrazek');



if($nazev=='')
    $systemMessage->add_error(ERROR_NEPLATNY_NAZEV);
			
if(!TestDate($od, true) || !TestDate($do, true))
	$systemMessage->add_error(ERROR_CHYBNE_DATUM);
    else
	{
		if(!TestTwoDate($od, $do))
		      $systemMessage->add_warning(WARNING_CHYBNE_DATUM);
	
		$od = GetUniDate($od);
		$do = GetUniDate($do);

	}
				
if($systemMessage->error_exists()) return;

$update = array(
    "idDomeny"      => $domain->getId(),
    "nazev"         => $nazev,
    "obsah"         => $obsah,
    "zobrazit"      => $zobrazit,
    "povolit_mazani"=> $povolit_mazani,
    "od"            => $od,
    "do"            => $do,
    "samorotujiciProdleva"=>$prodleva,
    "samorotujiciEfekt"=>$efekt,
    "samorotujiciRychlostEfektu"=>$rychlost_efektu,
    "sledovatProkliky"=>$sledovatProkliky,
    "stridani"      => $stridani,
    "idJazyka"      => WEB_LANG_ID
    );	
			
if($idPolozky == 0){
    $db->insert(TABLE_EDITBOXY,$update);
    $idPolozky = $db->lastId();
    $object_access->add_allowed_object($idPolozky,"editbox");            
    }
else				
	$db->update(TABLE_EDITBOXY,$update,"idEditboxu=".$idPolozky);

//nahrani fotky slidu
if($file == '' && isset($_FILES['obrazek']) && $_FILES['obrazek']['error']!=4){
    $upload = $_FILES['obrazek'];
    $news_path = $domain->getDir().USER_DIRNAME_EDITBOXES_MAXI;
                    
    require_once('php/classes/class.upload.php');
                    
    $uploader = new upload($_FILES['obrazek'], 'cs_CS');
    $uploader->file_new_name_body = $idPolozky;
    $uploader->file_overwrite = true;
    $uploader->image_resize = true;
    $uploader->image_x = intval(OBRAZEK_SLIDU_MAXI_X);
    $uploader->image_y = intval(OBRAZEK_SLIDU_MAXI_Y);
    $uploader->image_ratio = true;
    $uploader->image_convert = 'jpg';
    $uploader->process($news_path);

    $news_path = $domain->getDir().USER_DIRNAME_EDITBOXES_MINI;
    $uploader = new upload($_FILES['obrazek'], 'cs_CS');
    $uploader->file_new_name_body = $idPolozky;
    $uploader->file_overwrite = true;
    $uploader->image_resize = true;
    $uploader->image_x = intval(OBRAZEK_SLIDU_MINI_X);
    $uploader->image_y = intval(OBRAZEK_SLIDU_MINI_Y);
    $uploader->image_ratio = true;
    $uploader->image_convert = 'jpg';
    $uploader->process($news_path);


    $uploader->clean();
    $file = $idPolozky.".jpg";
                    
    $db->update(TABLE_EDITBOXY,array("obrazek" => $file),"idDomeny=".$domain->getId()." AND idEditboxu=".$idPolozky." LIMIT 1");
                
}

                
$data = $db->delete(TABLE_EDITBOXY_POLOZKY,"WHERE idKolotoce=".$idPolozky);

$i = 0;
if(count($editboxy) > 0)
    foreach($editboxy AS $ide)
        if($ide > 0)
        {
            $insert = array(
                "idKolotoce" => $idPolozky, 
                "idEditboxu" => intval($ide), 
                "poradi" => $i
                );
                
            $db->insert(TABLE_EDITBOXY_POLOZKY,$insert);
            $i++;
        }

if($stridani == 'zadne')
{
    if(MODULE_CONTENT_BACKUP)
    {
        $content_backup = new ContentBackup($domain->getId(),$idPolozky,$login_obj->getId(),'editbox');
        $content_backup->AddStore($obsah);
    }
    
    $chybne_odkazy_indik = false;

    if(MODULE_LINK_CHECKER)
    {
        $chybne_odkazy = new LinksChecker($domain->getId(), $idPolozky, "editbox", $obsah, false, KONTROLOVAT_ODKAZY_PRI_UKLADANI);
        $chybne_odkazy->set_base_url( $domain->getUrl() );
        $chybne_odkazy->process();
        $pocet_chybnych_obrazku = $chybne_odkazy->get_sum_error_images();
        $pocet_chybnych_odkazu = $chybne_odkazy->get_sum_error_links();
        
        if($pocet_chybnych_obrazku > 0)
            $systemMessage->add_warning(TPOCET_CHYBNYCH_OBRAZKU_NA_STRANCE.": ".$pocet_chybnych_obrazku);
            
        if($pocet_chybnych_odkazu > 0)
            $systemMessage->add_warning(TPOCET_CHYBNYCH_ODKAZU_NA_STRANCE.": ".$pocet_chybnych_odkazu);
        
        if(!$chybne_odkazy_indik && ($pocet_chybnych_obrazku > 0 || $pocet_chybnych_odkazu > 0)) 
            $chybne_odkazy_indik = true;
            
                    
    }
    
    
    if($chybne_odkazy_indik)
        $systemMessage->add_warning(TODKAZY_ZKONTROLUJTE_A_OPRAVTE);
    
    
}

$log->add_log($this->get_url('id_action') == 0 ? 'create':'edit-settings',$stridani=='zadne'?'editbox':'editbox-kolotoc',$idPolozky,$nazev);

$systemMessage->add_ok(OK_ULOZENO);

set_next_action_code();


$dalsi_akce = get_next_action_code();
if($dalsi_akce == 0) //zpet na vypis
    $url = $this->get_link('editboxy');
elseif($dalsi_akce == 1) //zpet na editaci
    $url = $this->get_link('editboxy',0,$stridani == 'zadne' ? 'edit-content' : 'edit-content-kolotoc',$idPolozky);
elseif($dalsi_akce == 2) //vytvorit dalsi
    $url = $this->get_link('editboxy',0,$stridani == 'zadne' ? 'new' : 'new-kolotoc');


Redirect($url);

?>