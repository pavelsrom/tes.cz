<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */


//------------------------------------------------
//definice polozek menu
//------------------------------------------------

$module_menu = array();
$module_menu[] = array(
    "page"  => "editboxy",
    "name"  => TSEZNAM_EDITBOXU." ".WEB_LANG
    );

$module_menu[] = array(
    "page"  => "nastaveni",
    "name"  => TNASTAVENI
    );

    
//------------------------------------------------
//definice nadpisu h1 a textu k nadpisu
//------------------------------------------------ 
$module_h1 = array();
$module_h1[] = array(
    "page"  => "editboxy",
    "action"=> "edit-content-kolotoc",
    "verify_id_action" => 1,
    "name"  => TEDITACE_EDITBOXU_KOLOTOCE." ".WEB_LANG
    );
$module_h1[] = array(
    "page"  => "editboxy",
    "action"=> "edit-content",
    "verify_id_action" => 1,
    "name"  => TEDITACE_EDITBOXU." ".WEB_LANG
    );
$module_h1[] = array(
    "page"  => "editboxy",
    "action"=> "new",
    "name"  => TNOVY_EDITBOX." ".WEB_LANG
    );  
$module_h1[] = array(
    "page"  => "editboxy",
    "action"=> "new-kolotoc",
    "name"  => TNOVY_EDITBOX_KOLOTOC." ".WEB_LANG
    );  
$module_h1[] = array(
    "page"  => "editboxy",
    "name"  => TSEZNAM_EDITBOXU." ".WEB_LANG,
    "text"  => 86
    ); 
$module_h1[] = array(
    "page"  => "nastaveni",
    "name"  => TNASTAVENI
    );


?>