<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined('SECURITY_CMS')) exit;
if(!$this->is_access('editboxy','edit-content-kolotoc',0,1,'content_view') &&
   !$this->is_access('editboxy','new-kolotoc',0,0,'content_add') 
    ) return;
    
$idEditbox = $this->get_id_action();

if(!$object_access->has_access($idEditbox,"editbox"))
{
    $systemMessage->add_warning(TNEMATE_OPRAVNENI_MENIT_OBSAH);
    $systemMessage->show_messages();
}


$main_tools = array();

if($idEditbox==0){
    $editbox = array();
    $editbox['nazev'] = "";
    $editbox['samorotujiciEfekt'] = 'none';
    $editbox['samorotujiciRychlostEfektu'] = 1;
    $editbox['samorotujiciProdleva'] = 5;
    $editbox['stridani'] = 'nahodne';
    $editbox['sledovatProkliky'] = 0;
    $editboxy = array();
    $editbox['zobrazit'] = 1;
    $editbox['povolit_mazani'] = 1;
    $editbox['publikovat_od'] = "";
    $editbox['publikovat_do'] = "";

}
else
{
			
$data = $db->Query("SELECT e.*, 
                    IF(od IS NULL OR od = '0000-00-00', '', DATE_FORMAT(od,'%d.%m.%Y')) AS publikovat_od,
				    IF(do IS NULL OR do = '0000-00-00', '', DATE_FORMAT(do,'%d.%m.%Y')) AS publikovat_do
				FROM ".TABLE_EDITBOXY." AS e
				WHERE e.idDomeny=".$domain->getId()."
                    AND e.idJazyka=".WEB_LANG_ID." 
					AND e.idEditboxu=".$idEditbox
                    
                    );

$editbox = $db->getAssoc($data);
$editboxy = array();
}

$main_tools[] = array(
    'ikona' => 'back', 
    "nazev" => TZPET, 
    "aktivni" => 0, 
    "odkaz" => $this->get_link('editboxy'), 
    "title" => TZPET_NA_SEZNAM_EDITBOXU
    );

echo main_tools($main_tools);
$form = new Form();
$form->add_section(TZAKLADNI_UDAJE, 'home');
$form->add_text(TNAZEV,'nazev',$editbox['nazev'],0,true);

if($login_obj->minPrivilege("admin"))
    $form->add_radiobuttons(TPOVOLIT_MAZANI, 'povolit_mazani', $editbox['povolit_mazani'], array(1 => TANO, 0 => TNE));
    else
    $form->add_hidden('povolit_mazani',$editbox['povolit_mazani']);

$form->add_radiobuttons(TZOBRAZOVAT,'zobrazit',$editbox['zobrazit']);
$form->add_section(TOBSAH,'content');

        
$data = $db->Query("SELECT e.idEditboxu, e.nazev, IF(ep.idEditboxu IS NULL, 0, 1) AS checked 
            FROM ".TABLE_EDITBOXY." AS e
            LEFT JOIN ".TABLE_EDITBOXY_POLOZKY." AS ep ON e.idEditboxu = ep.idEditboxu AND ep.idKolotoce=".$idEditbox."
            WHERE e.idDomeny=".$domain->getId()."
                AND e.stridani='zadne'
                AND e.idJazyka=".WEB_LANG_ID."
            GROUP BY e.idEditboxu
            ORDER BY checked DESC, ep.poradi, ep.idRelace
            
            ");
            
        
if($db->numRows($data) > 0){
    
    $editboxy_checked = array();
    $editboxy_vsechny = array();
        
    while($e = $db->getAssoc($data)){
          
        if($e['checked']==1 || in_array($e['idEditboxu'],$editboxy) ) 
            $editboxy_checked[] = $e['idEditboxu'];
            
           
        $editboxy_vsechny[$e['idEditboxu']] = $e['nazev'];
                               
        }

  
    $form->add_checkboxes(TOBSAH,'editboxy',$editboxy_checked,$editboxy_vsechny,365,false,"sortable_checkbox","",true)->set_class_td('sortable');
    }
else
$form->add_plain_text(TOBSAH,TNEBYL_NALEZEN_ZADNY_EDITBOX_S_OBSAHEM." <a href='".$this->get_link('editboxy',0,'new')."'>".TZDE."</a>");

$form->add_section(TNACASOVANI_ZOBRAZENI,'date');
$form->add_text(TOD,'od',$editbox['publikovat_od'],89,false,"kalendar","od");
$form->add_text(TDO,'do',$editbox['publikovat_do'],88,false,"kalendar","do");
$form->add_section(TNASTAVENI,'panel-setting');
$form->add_radiobuttons(TSLEDOVAT_POCET_ZOBRAZENI_A_PROKLIKU,'sledovatProkliky',$editbox['sledovatProkliky'],null,366);

$stridani = array('nahodne' => TNAHODNE, 'kolotoc' => TKOLOTOC, 'samorotujici' => TSAMOROTUJICI_KOLOTOC);
$efekty = array(
            'none' => '--- '.TBEZ_EFEKTU.' ---',
            'blindX' => TROLETA_VODOROVNE,
            'blindY' => TROLETA_SVISLE,
            'blindZ' => TROLETA_SIKMO,
            'cover' => TPREKRYTI,
            'curtainX' => TOPONA_VODOROVNE,
            'curtainY' => TOPONA_SVISLE,
            'fade' => TPLYNULY_PRECHOD,
            'fadeZoom' => TPLYNULY_PRECHOD_SE_ZVETSOVANIM,
            'growX' => TROZDELIT_VODOROVNE,
            'growY' => TROZDELIT_SVISLE,
            'scrollUp' => TPRETOCIT_NAHORU,
            'scrollDown' => TPRETOCIT_DOLU,
            'scrollLeft' => TPRETOCIT_VLEVO,
            'scrollRight' => TPRETOCIT_VPRAVO,
            'scrollHorz' => TPRETOCIT_VODOROVNE,
            'scrollVert' => TPRETOCIT_SVISLE,
            'shuffle' => TZAMICHAT,
            'slideX' => TVYKLOUZNUTI_VODOROVNE,
            'slideY' => TVYKLOUZNUTI_SVISLE,
            'toss' => TODHOZENI,
            'turnUp' => TPREKRYT_NAHORU,
            'turnDown' => TPREKRYT_DOLU,
            'turnLeft' => TPREKRYT_VLEVO,
            'turnRight' => TPREKRYT_VPRAVO,
            'uncover' => TODKRYT,
            'wipe' => TPREKRYT_SIKMO,
            'zoom' => TZVETSIT
        
            );

 
$form->add_radiobuttons(TSTRIDANI_EDITBOXU,'stridani',$editbox['stridani'],$stridani,368,"clSamorotujici");
$form->add_selectbox(TEFEKT, 'efekt', $editbox['samorotujiciEfekt'], $efekty,369,false,"","efekt")->set_class_tr("nastaveni_samorotujici");
$form->add_text(TRYCHLOST_EFEKTU, 'rychlostEfektu', $editbox['samorotujiciRychlostEfektu'],371,false,"","rychlost_efektu")->set_class_tr("nastaveni_samorotujici");
$form->add_text(TPRODLEVA, 'prodleva', $editbox['samorotujiciProdleva'], 370,false,"","prodleva")->set_class_tr("nastaveni_samorotujici");
$form->add_plain_text(TNAHLED,"<div id='show'></div>")->set_class_tr('nastaveni_samorotujici');
			
$res = (($login_obj->UserPrivilege('content_edit') && $idEditbox>0) || ($idEditbox==0 && $login_obj->UserPrivilege('content_add')) && $object_access->has_access($idEditbox,"editbox"));
$form->add_submit()->set_disabled(!$res);
    
echo $form->get_html_code();

?>

<script type='text/javascript'>  
<!-- 
            
            var path = '<?php echo RELATIVE_PATH;?>';
            $('<img />')[0].src = path + 'js/cycle/img/beach1.jpg';
            $('<img />')[0].src = path + 'js/cycle/img/beach2.jpg';
            $('<img />')[0].src = path + 'js/cycle/img/beach3.jpg';
            $('<img />')[0].src = path + 'js/cycle/img/beach4.jpg';
            $('<img />')[0].src = path + 'js/cycle/img/beach5.jpg';
            
          
            $(function(){
                $('.sortable').sortable({
        			opacity: 0.8, 
        			cursor: 'move',
        			<?php if(!$login_obj->UserPrivilege('content_edit')) echo "disabled: true,"; ?>
        			placeholder: 'placeholder-checkbox', 
        			items: 'div.checkbox-div'
                    //stop: create_panels_of_blocks
        
        		}).disableSelection();
                
                
                function prepni_samorotujici()
                {
                    v = $("#stridani3").is(":checked");
                    if(v)
                        $('.nastaveni_samorotujici').show();
                        else
                        $('.nastaveni_samorotujici').hide();
                }
                
                
                //if($('#stridani3').attr('checked')) 
                $('.clSamorotujici').on('ifChecked', function(){
                    prepni_samorotujici();
                })
                
                $('.clSamorotujici').click(function(){
                    if($(this).val()=='samorotujici') 
                        $('.nastaveni_samorotujici').show();
                        else
                        $('.nastaveni_samorotujici').hide();
                    })                                                           

                prepni_samorotujici();


            	$('#efekt, #prodleva, #rychlost_efektu').change(function() {
     	            prodleva = $('#prodleva').val() * 1000;
                    rychlost_efektu = $('#rychlost_efektu').val() * 1000;
            		fx = $.trim($('#efekt option:selected').val());
            		start();
                    return false;
            	});
            	
            	var markup = '<div id="slideshow">'
            		+ '<img src="' + path + 'js/cycle/img/beach1.jpg"><img src="' + path + 'js/cycle/img/beach2.jpg"><img src="' + path + 'js/cycle/img/beach3.jpg">'
            		+ '<img src="' + path + 'js/cycle/img/beach4.jpg"><img src="' + path + 'js/cycle/img/beach5.jpg">'
            		+ '</div>';
            		
            	
                function start() {
            		$('#slideshow').cycle('stop').remove();
                    $('#show').append(markup);
            		$('#slideshow').cycle({
            			fx: fx,
            			timeout: prodleva,
                        speed: rychlost_efektu,
                        height: "200px",
                        width: "300px",
                        cleartype: 1
            		});
            	}
       
                var prodleva = $('#prodleva').val() * 1000;
                var rychlost_efektu = $('#rychlost_efektu').val() * 1000;
          		var fx = $.trim($('#efekt option:selected').val());

                start();
                
                    
                })
                
// -->
</script>