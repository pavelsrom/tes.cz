<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined('SECURITY_CMS')) exit;
if(!$this->is_access('editboxy','edit-content',0,1,'content_view') &&
   !$this->is_access('editboxy','new',0,0,'content_add') 
    ) return;


$idEditbox = $this->get_id_action();

if(!$object_access->has_access($idEditbox,"editbox"))
{
    $systemMessage->add_warning(TNEMATE_OPRAVNENI_MENIT_OBSAH);
    $systemMessage->show_messages();
}


if($idEditbox > 0){
			$data = $db->Query("SELECT *,
				IF(od IS NULL OR od = '0000-00-00', '', DATE_FORMAT(od,'%d.%m.%Y')) AS publikovat_od,
				IF(do IS NULL OR do = '0000-00-00', '', DATE_FORMAT(do,'%d.%m.%Y')) AS publikovat_do,
                IF(s.zobrazeno=0,0,ROUND((s.prokliky/s.zobrazeno)*100,2)) AS pomer
				FROM ".TABLE_EDITBOXY." AS s
                WHERE s.idEditboxu=".$idEditbox."
                    AND s.idDomeny=".$domain->getId()."
                    AND s.idJazyka=".WEB_LANG_ID."
                LIMIT 1");
			 
			$editbox = $db->getAssoc($data); 
           
			}
			else
			{
			$editbox = array();
			$editbox['nazev'] = "";
			$editbox['zobrazit'] = 1;
            $editbox['povolit_mazani'] = 1;
            $editbox['vytvoreno'] = '';
            $editbox['obrazek'] = "";
			$editbox['stav'] = '';
            $editbox['sledovatProkliky'] = 0;
			$editbox['obsah'] = "";
			$editbox['publikovat_od'] = "";
			$editbox['publikovat_do'] = "";
			}
		

$main_tools[] = array(
    'ikona' => 'back', 
    "nazev" => TZPET, 
    "aktivni" => 0, 
    "odkaz" => $this->get_link('editboxy'), 
    "title" => TZPET_NA_SEZNAM_EDITBOXU
    );
    
echo main_tools($main_tools);

$form = new Form();
$form->allow_upload();

$form->add_section(TZAKLADNI_UDAJE,'home');
$form->add_text(TNAZEV,'nazev',$editbox['nazev'],0,true);

if($login_obj->minPrivilege("admin"))
    $form->add_radiobuttons(TPOVOLIT_MAZANI, 'povolit_mazani', $editbox['povolit_mazani'], array(1 => TANO, 0 => TNE));
    else
    $form->add_hidden('povolit_mazani',$editbox['povolit_mazani']);

$form->add_radiobuttons(TZOBRAZOVAT,'zobrazit',$editbox['zobrazit']);
$form->add_section(TOBSAH,'content');
$form->add_editor(TOBSAH,'obsah',$editbox['obsah'])->allow_wet();

$form->add_file(TNAHRAT_OBRAZEK, 'obrazek', false, 85, "", "obrazek_input")->set_disabled($editbox['obrazek']!='');
        
$image_class = "";
if($editbox['obrazek']=='' || $editbox['obrazek']==null)
{
    $img = TNENI_ULOZEN_ZADNY_OBRAZEK;
}
else
{	
    $file = $domain->getDir().USER_DIRNAME_EDITBOXES_MINI.$editbox['obrazek'];
				
    if(file_exists($file)) 
    {
        $src = RELATIVE_PATH.$file."?".time(); 
        $img = "<img src='".$src."' alt='".$editbox['obrazek']."' title='".$editbox['obrazek']."' style='min-width: 300px;'/><br /><br />";
        $image_class = "image";

        if($login_obj->UserPrivilege('content_delete'))
                $img .= tlacitko('#',TSMAZAT,"","times","delete photo-delete","delete".$idEditbox);
                else
                $img .= icon_disabled('delete');
            }
            else 
            {
            $src="";
            $img = TOBRAZEK_NENALEZEN;
            if($login_obj->UserPrivilege('content_delete'))
                $img .= tlacitko('#',TSMAZAT,"","times","delete photo-delete","delete".$idEditbox);
                else
                $img .= icon_disabled('delete');
                }
}

$form->add_plain_text(TOBRAZEK, $img)->set_class_td($image_class." relative tleft");	



$form->add_section(TNACASOVANI_ZOBRAZENI,'date');
$form->add_text(TOD,'od',$editbox['publikovat_od'],89,false,"kalendar","od");
$form->add_text(TDO,'do',$editbox['publikovat_do'],88,false,"kalendar","do");
$form->add_section(TNASTAVENI,'page-setting');
$form->add_radiobuttons(TSLEDOVAT_POCET_ZOBRAZENI_A_PROKLIKU,'sledovatProkliky',$editbox['sledovatProkliky'],null,366);
		
$res = (($login_obj->UserPrivilege('content_edit') && $idEditbox>0) || ($idEditbox==0 && $login_obj->UserPrivilege('content_add')) && $object_access->has_access($idEditbox,"editbox"));

$form->add_next_actions();
$form->add_submit()->set_disabled(!$res);
    
echo $form->get_html_code();

?>

<script type="text/javascript">
<!-- 
$(function(){
    
    $("a.delete").click(function(){
        var id = $(this).attr('id').substring(6);
        
        var text = "<?php echo TOPRAVDU_SMAZAT_TENTO_OBRAZEK;?>";
        var td = $(this).parents("td.image").first();
        
        custom_confirm(text, function(){
            $.msg();
            td.addClass("deleted");
            $.post("<?php echo AJAX_GATEWAY;?>image.delete",{id: id},function(data){
                $.msg( 'setClass', 'ok_message' );
                $.msg( 'replace', '<?= OK_SMAZANO ?>' );
                $.msg( 'unblock', 1000);
                td.html("<?php echo TNENI_ULOZEN_ZADNY_OBRAZEK;?>");
                $("#obrazek_input").removeAttr('disabled');
                td.removeAttr("class");
                });
            
            })
            
        return false;
        
    })
    
})
// --> 
</script>