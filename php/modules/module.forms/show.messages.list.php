<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined('SECURITY_CMS')) exit;
if(!$this->is_access('zpravy','',0,0,'content_view')) return;


$dataf = $db->Query("SELECT * FROM ".TABLE_FORMULARE." WHERE idDomeny=".$domain->getId());
		
$form_data = array();
$form_data[0] = "-- ".TVSECHNY_ZPRAVY." --";

while($formular = $db->getAssoc($dataf))
    $form_data[$formular['idFormulare']] = $formular['nazev'];   
   
$polozky = array(
    '0'=>"-- ".TVSECHNY_ZPRAVY." --", 
    'nova'=>TNOVA, 
    'prijata'=>TPRIJATA, 
    'odpovezeno'=>TODPOVEZENA
    );
           
           
$form = new FormFilter();
$form->set_class('filter2');
$form->add_text(THLEDAT, 'vyraz', "", 0, false, "", "vyraz");
$form->add_selectbox(TZPRAVY_Z_FORMULARE,'formular',0,$form_data,0,false,"","formular");
$form->add_selectbox(TSTAV,'stav','0',$polozky,0,false,"","stav");
echo $form->get_html_code();

        
echo "<table class='table' id='tList'>";
echo "<thead>";
echo "<tr>
            <th class='w25'>".TID."</th>
            <th>".TODESILATEL."</th>
            <th class='w80'>".TDATUM."</th>
            <th class='w80'>".TSTAV."</th>
            <th class='w150'>".TAKCE."</th>
    </tr>";
echo "</thead>";
echo "<tbody></tbody>";
echo "</table>";


?>
<script type="text/javascript">
<!--


$(function(){
var oTable = $("#tList").dataTable({
    "sAjaxSource": "<?php echo AJAX_GATEWAY;?>messages.list",
    "aoColumns": [
        {"sClass": "sl1"},{"sClass": "sl2"},{"sClass": "sl3"},{"sClass": "sl4"},{"sClass": "sl5", "bSortable": false}
        ],
    "fnServerData": function ( sSource, aoData, fnCallback ) {
            aoData.push({ "name": "vyraz", "value": $("#vyraz").val() });
            aoData.push({ "name": "formular", "value": $("#formular option:selected").val() });
            aoData.push({ "name": "stav", "value": $("#stav option:selected").val() });                        
            $.getJSON( sSource, aoData, function (json) { fnCallback(json); } );
            }
 
})

$("input#vyraz").bind('keyup change', function() {
        oTable.fnDraw(); 
        });

//prida k bunce th span
$(".dataTable th").each(function(){
    $(this).html("<span>" + $(this).html() + "</span>");
})

$(".filter2 select").bind('change', function() {
    oTable.fnDraw();
    });


})

// -->
</script>
