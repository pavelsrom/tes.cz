<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined('SECURITY_CMS')) exit;
if(!$this->is_access('formulare','delete',0,1,'content_delete')) return;

$idFormulare = $this->get_id_action(); 
			
//overeni zda zaznam existuje, kvuli zneuziti mazani z tabulky formulare_relace_pf
$data =$db->Query("SELECT * FROM ".TABLE_FORMULARE." WHERE idFormulare=".$idFormulare." AND idDomeny=".$domain->getId());
			
if($db->numRows($data)==0){
    $systemMessage->add_error(ERROR_NARUSENI_SYSTEMU);
    error_handler_hacking($this->moduleName, ERROR_NARUSENI_SYSTEMU);
    Redirect($this->get_link('formulare'));
    }

$f = $db->getAssoc($data);
$nazev = $f['nazev'];
			
$db->Query("DELETE FROM ".TABLE_FORMULARE." WHERE idFormulare=".$idFormulare." AND idDomeny=".$domain->getId());
$db->Query("DELETE FROM ".TABLE_PANELY_POLOZKY." WHERE podtyp='formular' AND idPodtypu=".$idFormulare);
		
$log->add_log('delete','formular',$idFormulare,$nazev);
        	
$systemMessage->add_ok(OK_SMAZANO);		
Redirect($this->get_link('formulare'));

?>