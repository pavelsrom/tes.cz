<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined('SECURITY_CMS')) exit;
if(!$this->is_access('formulare')) return;
		
$main_tools = array();
        
if($login_obj->UserPrivilege('settings_add'))
    $main_tools[] = array(
        'ikona' => 'new', 
        "nazev" => TPRIDAT, 
        "aktivni" => 0, 
        "odkaz" => $this->get_link('formulare',0,'new'), 
        "title" => TPRIDAT_FORMULAR
        );

$form = new FormFilter();
$form->add_text(THLEDAT, 'vyraz', "", 0, false, "", "vyraz");
echo $form->get_html_code(main_tools($main_tools));
 
$table = new Table("tList");
            
$table->tr_head()
    ->add(TID,'w25')
    ->add(TNAZEV)
    ->add(TNOVYCH_CELKEM_ULOZENYCH_ZPRAV,'w150')
    ->add(TAKTIVNI,'w25')
    ->add(TAKCE,'w150');
    
echo $table->get_html();

?>
<script type="text/javascript">
<!--

$(function(){
var oTable = $("#tList").dataTable({
    "sAjaxSource": "<?php echo AJAX_GATEWAY;?>forms.list",
    "aoColumns": [
        {"sClass": "sl1"},{"sClass": "sl2 tleft"},{"sClass": "sl3"},{"sClass": "sl4"},{"sClass": "sl5", "bSortable": false}
        ],
    "fnServerData": function ( sSource, aoData, fnCallback ) {
            aoData.push({ "name": "vyraz", "value": $("#vyraz").val() });
            $.getJSON( sSource, aoData, function (json) { fnCallback(json); } );
            }
 
})

$("input#vyraz").bind('keyup change', function() {
        oTable.fnDraw(); 
        });

//prida k bunce th span
$(".dataTable th").each(function(){
    $(this).html("<span>" + $(this).html() + "</span>");
})

})

// -->
</script>

