<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined('SECURITY_CMS')) exit;
if(!$this->is_access('formulare','edit-settings',0,1,'settings_view') && 
    !$this->is_access('formulare','new',0,0,'settings_add')) return;

$idForm = $this->get_id_action();
$main_tools = array();

if($idForm==0){
			$formular = array();
			$formular['nazev'] = "";
			$formular['email'] = EMAIL_WEBU;
			$formular['tlacitko'] = TODESLAT;
			$formular['textPoOdeslani'] = TDEKUJEME_ZA_ODESLANI_FORMULARE;
			$formular['potvrzeni'] = "";
			$formular['potvrzeniPredmet'] = "";
			$formular['posilatPotvrzeni'] = 0;
			$formular['ukladatZpravy'] = 1;
			$formular['antispam'] = 1;
			$formular['idSkupinEmailu'] = 0;
			$formular['zobrazit'] = 1;

			}
			else
			{
				
				
				$data = $db->Query("SELECT f.*
					FROM ".TABLE_FORMULARE." AS f 
					WHERE f.idFormulare=".$idForm." 
						AND f.idDomeny=".$domain->getId()." 
					LIMIT 1
					");
						
				$formular = $db->getAssoc($data);
                
                if($login_obj->UserPrivilege('content_view'))
                    $main_tools[] = array(
                        'ikona' => 'content', 
                        "nazev" => TOBSAH, 
                        "aktivni" => 0, 
                        "odkaz" => $this->get_link('formulare',0,'edit-content',$idForm), 
                        "title" => TOBSAH_FORMULARE
                        );
                

				
			}

$main_tools[] = array(
    'ikona' => 'back', 
    "nazev" => TZPET, 
    "aktivni" => 0, 
    "odkaz" => $this->get_link('formulare'), 
    "title" => TZPET_NA_SEZNAM_FORMULARU
    );
    
echo main_tools($main_tools);

$form = new Form();
$form->add_section(TZAKLADNI_UDAJE,'home');            		
$form->add_text(TNAZEV,'nazev',$formular['nazev'],74,true);
$form->add_text(TEMAIL_PRO_ODESLANI,'email',$formular['email'],80);
$form->add_text(TNAZEV_ODESILAJICIHO_TLACITKA,'tlacitko',$formular['tlacitko'],81);
$form->add_text(TPOTVRZENI_PO_USPESNEM_ODESLANI,'textPoOdeslani',$formular['textPoOdeslani'],90);
$form->add_radiobuttons(TUKLADAT_ZPRAVY,'ukladat',$formular['ukladatZpravy'],null,91);
$form->add_radiobuttons(TANISPAMOVA_OCHRANA,'antispam',$formular['antispam'],null,92);
$form->add_radiobuttons(TZOBRAZOVAT,'zobrazit',$formular['zobrazit'],null,93);

$form->add_section(TPOTVRZOVACI_EMAIL,'email');
$form->add_radiobuttons(TPOSILAT_POTVRZENI,'posilatPotvrzeni',$formular['posilatPotvrzeni'],null,94);
$form->add_text(TPREDMET_POTVRZENI,'predmet',$formular['potvrzeniPredmet'],95);
$form->add_editor(TOBSAH_POTVRZENI,'potvrzeni',$formular['potvrzeni'],96);

		
if(MODULE_NEWSLETTER){
    $data_sk = $db->Query("SELECT idSkupiny, nazev FROM ".TABLE_EMAILY_SKUPINY." WHERE idDomeny=".$domain->getId());
    $skupiny = array();
    $sk_form = array();
    $sk_checked = array();
    
    while($sk = $db->getAssoc($data_sk))
        $skupiny[$sk['idSkupiny']] = $sk['nazev'];
        
    if($formular['idSkupinEmailu']!='')
		$sk_form = explode('#',$formular['idSkupinEmailu']);

    foreach($skupiny AS $id => $s)
    {		
        if(in_array($id,$sk_form))
			$sk_checked[] = $id;	
	}
    
    $form->add_section(TZARAZENI_EMAILU_DO_SKUPINY,'group');
    	
    if(count($skupiny)==0)
    {
        $form->add_plain_text(TAUTOMATICKY_RADIT_DO_SKUPINY,TNEJSOU_VYTVORENY_ZADNE_SKUPINY);
    }
    else
    {
        $form->add_checkboxes(TAUTOMATICKY_RADIT_DO_SKUPINY,'skupiny',$sk_checked,$skupiny,97);
    }
	
			
	$data_pocet = $db->Query("SELECT idZpravy AS pocet_emailu FROM ".TABLE_FORMULARE_ZPRAVY." 
				WHERE idFormulare=".$idForm."
					AND email IS NOT NULL
					AND email != '' 
				GROUP BY email");
                
	$pocet = $db->numRows($data_pocet);
			
	if($pocet>0)
        $form->add_note(sprintf(TFORMULAR_INFO1, $pocet));
 
    $form->add_selectbox(TMANUALNI_PREVOD_UZIVATELU_DO_SKUPINY, 'export', '', array('' => TVYBERTE)+ $skupiny, 98);
 }


$res = ($login_obj->UserPrivilege('settings_add') && $idForm==0) || ($login_obj->UserPrivilege('settings_edit') && $idForm>0);
$form->add_submit()->set_disabled(!$res);
	
echo $form->get_html_code();
	
	

?>