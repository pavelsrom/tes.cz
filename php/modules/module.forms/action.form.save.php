<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */


if(!defined('SECURITY_CMS')) exit;
if(!is_action() || 
    (!$this->is_access('formulare','edit-settings',0,1,'settings_edit') && 
    !$this->is_access('formulare','new',0,0,'settings_add'))
    ) return;


$idFormulare = $this->get_id_action();
$nazevFormulare = get_post('nazev');
$zobrazit = get_int_post('zobrazit');
$email = get_post('email');
$tlacitko = get_post('tlacitko');
$nazev = get_post('nazev');
$predmet = get_post('predmet');
$textPoOdeslani = get_post('textPoOdeslani');
$posilatPotvrzeni = get_int_post('posilatPotvrzeni');
$antispam = get_int_post('antispam');
$potvrzeni = get_post('potvrzeni',"",false);
$ukladat = get_int_post('ukladat');
$skupiny = isset($_POST['skupiny'])?$_POST['skupiny']:array();			
$idSkupinyExport = get_int_post('export');
$set = "";
if($nazev=='')
    $systemMessage->add_error(ERROR_NEPLATNY_NAZEV_FORMULARE);
    else
    $set .= "nazev='".$nazev."',";
				
if($tlacitko=='')
    $systemMessage->add_error(ERROR_NEPLATNY_NAZEV_TLACITKA);
    else	
    $set .= "tlacitko='".$tlacitko."',";
			
if(!TestEmail($email, 150, true))
    $systemMessage->add_error(ERROR_EMAIL);
    else
    $set .= "email='".$email."',";
			
			
//export uloznych odesilatelu do skupiny prijemcu v modulu Sprava prijemcu
			if($idSkupinyExport>0){
				$data1 = $db->Query("SELECT email FROM ".TABLE_FORMULARE_ZPRAVY." 
					WHERE idFormulare=".$idFormulare."
						AND email IS NOT NULL
						AND email != ''
					GROUP BY email
					");
				
				$values = "";	
				$delete = "";
				while($o = $db->getAssoc($data1)){
					if(!TestEmail($o['email'],70,false)) continue;
					
					//overeni zda email uz v prijemcich existuje
					$data_verify = $db->Query("SELECT idPrijemce FROM ".TABLE_EMAILY_PRIJEMCI." WHERE idDomeny=".$domain->getId()." AND email='".$o['email']."'");
					
					if($db->numRows($data_verify)==0){
						//pokud email v tabulce neexistuje prida se novy zaznam
						$db->Query("INSERT INTO ".TABLE_EMAILY_PRIJEMCI." (idDomeny, email, hash, registrace, poznamka) 
							VALUES(".$domain->getId().",'".$o['email']."',MD5('".time().$o['email']."'),NOW(),'Kontakt manu�ln� p�eveden z formul��e ".$nazevFormulare."')");
						$idPrijemce = $db->lastId();
						}
						else
						{
						$data_idPrijemce = $db->getAssoc($data_verify);
						
						$idPrijemce = $data_idPrijemce['idPrijemce'];
						$delete[] = $idPrijemce;
						}
					
					//pridani prislusnych zaznamu do tabulky emaily relace sp
					
					$values[] = "(".$idPrijemce.", ".$idSkupinyExport.")";	
				}
				
				//smazani existujiciho zaznamu z tabulky
				if(count($delete)>0)
					$db->Query("DELETE FROM ".TABLE_EMAILY_SKUPINY_PRIJEMCI." 
						WHERE idSkupiny=".$idSkupinyExport." 
						AND idPrijemce IN (".implode(',',$delete).")");
				
				if(count($values)>0)	
					$db->Query("INSERT INTO ".TABLE_EMAILY_SKUPINY_PRIJEMCI." (idPrijemce, idSkupiny) VALUES ".implode(',',$values));
				
				$systemMessage->add_ok(OK_PREVEDENO_EMAILU.count($values));	
				
				}
			
			
	
if($systemMessage->error_exists() && $idFormulare==0) return;
			
    if($idFormulare>0){
				
				$set .= "
					textPoOdeslani='".$textPoOdeslani."', 
					zobrazit=".$zobrazit.",
					potvrzeniPredmet='".$predmet."',
					potvrzeni='".$potvrzeni."',
					posilatPotvrzeni=".$posilatPotvrzeni.",
					ukladatZpravy=".$ukladat.",
					antispam=".$antispam.",
                    idSkupinEmailu='".implode('#',$skupiny)."'
				"; 
				$db->Query("UPDATE ".TABLE_FORMULARE." SET ".$set." WHERE idFormulare=".$idFormulare." AND idDomeny=".$domain->getId());
				
    			}
				else
				{
	
				$db->Query("INSERT INTO ".TABLE_FORMULARE." (idDomeny,nazev,email,antispam, tlacitko,textPoOdeslani,zobrazit, potvrzeni, potvrzeniPredmet, posilatPotvrzeni, ukladatZpravy, idSkupinEmailu) VALUES(".$domain->getId().",'".$nazev."','".$email."',".$antispam.",'".$tlacitko."','".$textPoOdeslani."',".$zobrazit.",'".$potvrzeni."', '".$predmet."',".$posilatPotvrzeni.",".$ukladat.",'".implode('#',$skupiny)."')");
                

				$idFormulare = $db->lastId();

				}



$log->add_log($this->get_url('id_action') == 0 ? 'create':'edit-settings','formular',$idFormulare,$nazev);

$systemMessage->add_ok(OK_ULOZENO);

if($posilatPotvrzeni==1 && $potvrzeni=='')
    $systemMessage->add_warning(WARNING_PRAZDNE_POTVRZENI);
					
$url = $this->get_link('formulare',0,'edit-settings',$idFormulare);				
Redirect($url);
			

?>