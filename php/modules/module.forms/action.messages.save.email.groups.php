<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined('SECURITY_CMS')) exit;
if(!is_post('btnEmailDoSkupiny') ||
    !$this->is_access('zpravy','edit-content',0,1,'content_view')
    ) return;

$email = get_post('email');
$formular = get_post('formular');
$skupiny = isset($_POST['skupiny'])?(array)$_POST['skupiny']:array();
$idZpravy = $this->get_id_action();
		
if(!TestEmail($email,150,false) || $idZpravy==0) {
    $systemMessage->add_error(ERROR_EMAIL);
	Redirect($this->get_link('zpravy'));
	}
			
$data1 = $db->Query("SELECT idPrijemce FROM ".TABLE_EMAILY_PRIJEMCI." WHERE idDomeny=".$domain->getId()." AND email='".$email."' LIMIT 1");

if($db->numRows($data1)==0){
    $db->Query("INSERT INTO ".TABLE_EMAILY_PRIJEMCI."(idDomeny,email,hash, registrace, poznamka) VALUES(".$domain->getId().",'".$email."',MD5('".time().$email."'),NOW(),'".TKONTAKT_PREVEDEN_Z_FORMULARE." ".$formular."')");
    $idPrijemce = $db->lastId();
    }
    else
    {
    $data_pr = $db->getAssoc($data1);
    $idPrijemce = $data_pr['idPrijemce'];	
    $db->Query("DELETE FROM ".TABLE_EMAILY_SKUPINY_PRIJEMCI." WHERE idPrijemce=".$idPrijemce);
    }
	
$values = array();
foreach($skupiny AS $s)
    $values[] = "(".$s.",".$idPrijemce.")";
				
			
if(count($values)>0)
    $db->Query("INSERT INTO ".TABLE_EMAILY_SKUPINY_PRIJEMCI."(idSkupiny,idPrijemce) VALUES ".implode(',',$values));


$log->add_log('edit-settings','formular-zprava',$idZpravy,$formular);
				
$systemMessage->add_ok(OK_ULOZENO);
				
$url = $this->get_link('zpravy',0,'edit-content',$idZpravy);
Redirect($url);
				

?>