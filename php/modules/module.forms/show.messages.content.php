<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined('SECURITY_CMS')) exit;
if(!$this->is_access('zpravy','edit-content',0,1,'content_view')) return;

$idMessage = $this->get_id_action();

if($idMessage==0) return;
		
$data = $db->Query("SELECT z.*, f.nazev, 
			IF(datum IS NULL, NULL, DATE_FORMAT(z.datum,'%d.%m.%Y %H:%i')) AS datum_prijeti 
			FROM ".TABLE_FORMULARE_ZPRAVY." AS z 
			LEFT JOIN ".TABLE_FORMULARE." AS f ON z.idFormulare=f.idFormulare 
			WHERE z.idZpravy=".$idMessage." 
			AND f.idDomeny=".$domain->getId());
			
if($db->numRows($data)==0) return;
$message = $db->getAssoc($data);
		
//pri nacteni teto zpravy bude zprava oznacena jako prijata, pri odeslani emailu bude oznacena jako odpovezeno
$db->Query("UPDATE ".TABLE_FORMULARE_ZPRAVY." SET stav='prijata' WHERE stav='nova' AND idZpravy=".$idMessage);
		
$main_tools = array();
$main_tools[] = array(
    'ikona' => 'back', 
    "nazev" => TZPET, 
    "aktivni" => 0, 
    "odkaz" => $this->get_link('zpravy'), 
    "title" => TZPET_NA_SEZNAM_ZPRAV
    );
    
echo main_tools($main_tools);
		
$odesilatel = secureString($message['email']);
		
$message_state = array(
    '0'=>"-- ".TVSECHNY_ZPRAVY." --", 
    'nova'=>TNOVA, 
    'prijata'=>TPRIJATA, 
    'odpovezeno'=>TODPOVEZENA
    );
	
$form = new Form();
$form->add_section(TPRIJATA_ZPRAVA,'receive'); 	
$form->add_plain_text(TNAZEV_FORMULARE, $message['nazev'],99);
$form->add_plain_text(TDATUM_A_CAS_PRIJETI_ZPRAVY,$message['datum_prijeti'],100);
$form->add_plain_text(TSTAV,$message_state[$message['stav']],101);
$form->add_plain_text(TODESILATEL,$odesilatel,102);
$form->add_plain_text(TOBSAH,$message['zprava'],103);

$form->add_section(TODPOVED,'reply');
		
$puvodniZprava =  "<br/>------------------------------ Původní zpráva ------------------------------<br />";
$puvodniZprava .= TODESILATEL.": ".$odesilatel;
$puvodniZprava .= "<br />".TPUVODNI_ZPRAVA.": ".$message['zprava'];
$puvodniZprava .= "<br/>----------------------------------------------------------------------------<br />";
        
$zprava =  get_secure_post('odpoved',false,$message['odpoved']);
$predmet = get_secure_post('predmet',true,$message['predmetOdpovedi']);
		
if(!TestEmail(EMAIL_WEBU,200,false))
    $mujEmail = $login_obj->getName()." &lt;".$login_obj->getEmail()."&gt;";
    else
    $mujEmail = NAZEV_WEBU." &lt;".EMAIL_WEBU."&gt;";

$form->add_plain_text(TVAS_EMAIL,$mujEmail);
		
if($message['stav']!='odpovezeno'){
    $form->add_text(TPREDMET,'predmet',"");
     $form->add_editor(TODPOVED,'zprava',$message['odpoved'].$puvodniZprava, 104);
    $form->add_hidden('komu',$odesilatel);
    
    if($login_obj->UserPrivilege('send_email'))
        $form->add_submit(TODESLAT,'btnOdeslatEmail','send_button');
        else
        $form->add_submit(TODESLAT,'btnOdeslatEmail','send_button')->set_disabled();
			
			
    }
    else
    {
        $form->add_plain_text(TPREDMET,$predmet);
        $form->add_plain_text(TODPOVED,$zprava);
    }

echo $form->get_html_code();
		
        
		
$email_sk = array();
$data_sk = $db->Query("SELECT * FROM ".TABLE_EMAILY_SKUPINY." WHERE idDomeny=".$domain->getId());
		
$data_pr = $db->Query("SELECT s.idSkupiny FROM ".TABLE_EMAILY_SKUPINY." AS s 
			LEFT JOIN ".TABLE_EMAILY_SKUPINY_PRIJEMCI." AS r ON s.idSkupiny=r.idSkupiny
			LEFT JOIN ".TABLE_EMAILY_PRIJEMCI." AS p ON r.idPrijemce=p.idPrijemce
			WHERE s.idDomeny=".$domain->getId()."
				AND p.email='".$odesilatel."'");
				
while($sk_pr = $db->getAssoc($data_pr))
    $email_sk[] = $sk_pr['idSkupiny']; 
		
$form = new Form();	
if(MODULE_NEWSLETTER && $odesilatel!=''){
    $form->add_section(TZARAZENI_EMAILU_DO_SKUPINY,'group');
		
    if($db->numRows($data_sk)>0)
    {
        $ch = array();
        $vsechny_sk=array();
		while($sk = $db->getAssoc($data_sk))
        {  
		  if(in_array($sk['idSkupiny'],$email_sk))
				$ch[] = $sk['idSkupiny'];
        
            $vsechny_sk[$sk['idSkupiny']] = $sk['nazev'];
		}
                
        $form->add_checkboxes(TAUTOMATICKY_RADIT_DO_SKUPINY,'skupiny',$ch,$vsechny_sk);
    }
    else
    $form->add_plain_text(TAUTOMATICKY_RADIT_DO_SKUPINY,TNEJSOU_VYTVORENY_ZADNE_SKUPINY);
	

    $form->add_hidden('email',$odesilatel);
    $form->add_hidden('formular',$message['nazev']);
    
    if($login_obj->UserPrivilege('content_edit'))
        $form->add_submit(TULOZIT,'btnEmailDoSkupiny');
        else
        $form->add_submit(TULOZIT,'btnEmailDoSkupiny')->set_disabled();
    
	echo $form->get_html_code();			
    }

?>