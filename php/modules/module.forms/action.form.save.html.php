<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined('SECURITY_CMS')) exit;
if(!is_post('btnUlozitFormularHtml') || 
    !$this->is_access('formulare','edit-html',0,1,'content_edit')
    ) return;


$idFormulare = $this->get_id_action(); 
$html = get_post('html');
				
$db->Query("UPDATE ".TABLE_FORMULARE." SET html='".$html."' WHERE idFormulare=".$idFormulare." AND idDomeny=".$domain->getId());
			
$nazev = $db->get(TABLE_FORMULARE,'nazev','idFormulare='.$idFormulare);
$log->add_log('edit-content','formular-html',$idFormulare,$nazev);
            
$systemMessage->add_ok(OK_ULOZENO);		
$url = $this->get_link('formulare',0,'edit-html',$idFormulare);
Redirect($url);

?>