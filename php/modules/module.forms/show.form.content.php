<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined('SECURITY_CMS')) exit;
if(!$this->is_access('formulare','edit-content',0,1,'content_view')) return;

$idForm = $this->get_id_action();

if($idForm==0) return;
		
$data_form = $db->Query("SELECT f.nazev
			FROM ".TABLE_FORMULARE." AS f
			WHERE idFormulare=".$idForm." AND idDomeny=".$domain->getId()." LIMIT 1");
        
$formular = $db->getAssoc($data_form);
$main_tools = array();
                    
if($login_obj->UserPrivilege('settings_view'))
    $main_tools[] = array(
        'ikona' => 'setting', 
        "nazev" => TNASTAVENI, 
        "aktivni" => 0, 
        "odkaz" => $this->get_link('formulare',0,'edit-settings',$idForm), 
        "title" => TNASTAVENI_FORMULARE
        );
                
$main_tools[] = array(
    'ikona' => 'back', 
    "nazev" => TZPET, 
    "aktivni" => 0, 
    "odkaz" => $this->get_link('formulare'), 
    "title" => TZPET_NA_SEZNAM_FORMULARU
    );
        
        
        
$disabled = !$login_obj->UserPrivilege('content_edit');
$disabled_delete = !$login_obj->UserPrivilege('content_delete');       

echo "<div id='message'></div>";

echo "<p>".TFORMULAR_INFO2." <a href='".$this->get_link('formulare',0,'edit-html',$idForm)."'>".TOBSAH_FORMULARE."</a>. ".TFORMULAR_INFO3."</p>";
	
echo main_tools($main_tools);
echo "<ul id='form-builder' class='w45'><li></li></ul>";

?> 
<script type="text/javascript">
<!--

         $(function(e){ 
            $('#form-builder').formbuilder({
        	    'save_url': '<?php echo AJAX_GATEWAY;?>form.save',
        	    'load_url': '<?php echo AJAX_GATEWAY;?>form.load&idForm=<?php echo $idForm; ?>',
                'disabled': <?php echo $disabled;?>,
                'disabled_delete': <?php echo $disabled_delete;?>,
                'id_form': <?php echo $idForm; ?>
        	    });

            $('#form-builder').sortable({
                connectWith: 'li', 
				opacity: 0.8, 
				cursor: 'move',
                placeholder: 'form-placeholder',
                
                });    
            
               

            $("#field_control, #field_control_bot").msDropDown({visibleRows:10, rowHeight:12});
            
            $("#form-builder .frm-holder").hide();
            
            if(1 == <?php echo $disabled ? "1" : ""; ?>)
                $("#save-form").remove();// addClass("<?php echo $disabled ? "disabled" : ""; ?>"); 
            
            });
            
            
            // -->
</script>