<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */


//------------------------------------------------
//definice polozek menu
//------------------------------------------------

$module_menu = array();
$module_menu[] = array(
    "page"  => "formulare",
    "name"  => TSEZNAM_FORMULARU
    );
$module_menu[] = array(
    "page"  => "zpravy",
    "name"  => TFORMULAROVE_ZPRAVY
    );    
    
//------------------------------------------------
//definice nadpisu h1 a textu k nadpisu
//------------------------------------------------ 
$module_h1 = array();
$module_h1[] = array(
    "page"  => "formulare",
    "action"=> "edit-content",
    "verify_id_action" => 1,
    "name"  => TEDITACE_FORMULARE
    );
$module_h1[] = array(
    "page"  => "formulare",
    "action"=> "edit-html",
    "verify_id_action" => 1,
    "name"  => TEDITACE_FORMULARE_HTML
    );
    
$module_h1[] = array(
    "page"  => "formulare",
    "action"=> "edit-settings",
    "verify_id_action" => 1,
    "name"  => TNASTAVENI_FORMULARE
    );
$module_h1[] = array(
    "page"  => "formulare",
    "action"=> "new",
    "name"  => TNOVY_FORMULAR
    );    
$module_h1[] = array(
    "page"  => "formulare",
    "name"  => TSEZNAM_FORMULARU
    ); 
$module_h1[] = array(
    "page"  => "zpravy",
    "action"=> "edit-content",
    "verify_id_action" => 1,
    "name"  => TZPRAVA
    );
$module_h1[] = array(
    "page"  => "zpravy",
    "name"  => TFORMULAROVE_ZPRAVY
    ); 
    



?>