<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */


if(!defined('SECURITY_CMS')) exit;
if(!$this->is_access('formulare','edit-html',0,1,'content_view')) return;

$idForm = $this->get_id_action();

if($idForm==0) return;
		
$data = $db->Query("SELECT * FROM ".TABLE_FORMULARE." WHERE idFormulare=".$idForm." AND idDomeny=".$domain->getId()." LIMIT 1");
		
if($db->numRows($data)==0) return;
		
$formular = $db->getAssoc($data);


$main_tools[] = array(
    'ikona' => 'back', 
    "nazev" => TZPET, 
    "aktivni" => 0, 
    "odkaz" => $this->get_link('formulare'), 
    "title" => TZPET_NA_SEZNAM_FORMULARU
    );



		echo "<p>".TVLASTNI_FORMULAR_PRAVIDLA;
		echo "<ul>";
		echo "<li>".TVLASTNI_FORMULAR_PRAVIDLA_A."</li>";
		echo "<li>".TVLASTNI_FORMULAR_PRAVIDLA_B." <b><i>&lt;input type='hidden' name='idFormulare' value='".$idForm."' /&gt;</i></b></li>";
		echo "<li>".TVLASTNI_FORMULAR_PRAVIDLA_C." name='<b>btnSendForm</b>'</li>";
		echo "<li>".TVLASTNI_FORMULAR_PRAVIDLA_D."</b></li>";
		echo "</ul>";
		echo "</p>";

echo main_tools($main_tools);		

		echo "<form method='post' action=''>";
		
		echo "<table class='table2'>";
		echo "<tr><th>".TDOSTUPNE_POLOZKY."</th><td>";
		
		$data_pol = $db->Query("SELECT * FROM ".TABLE_FORMULARE_POLOZKY." WHERE idFormulare=".$idForm." ORDER BY idPolozky");
		
		$polozky = array('0'=>'-- '.TVYBERTE_POLOZKU.' --');
		while($pol = $db->getAssoc($data_pol))
			$polozky[$pol['idPolozky']] = $pol['nazev'];
			
		echo ShowSelectBoxToString('polozky', $polozky, 0, false, "get_attr_name(".$idForm.",\"polozky\",\"name\")", "polozky");
			  
		echo "</td></tr>";
		echo "<tr><td colspan='2'><span id='name'></span></td></tr>";
		
		
		echo "</table>";
		echo "<textarea id='editarea' name='html' rows='' cols='' class='w100 h500'>".htmlspecialchars($formular['html'], ENT_QUOTES, 'UTF-8')."</textarea>";
		echo "<div class='dolni-tlacitka'>";
		if($idForm>0)
			echo "<input type='hidden' name='idFormulare' value='".$idForm."' />";
		
		if($login_obj->UserPrivilege('content_add') && $idForm==0)
			echo "<input type='submit' name='btnUlozitFormularHtml' value='".TULOZIT."' class='tool_button save_button' title='".TULOZIT."'/>";
		elseif($login_obj->UserPrivilege('content_add') && $idForm>0)
			echo "<input type='submit' name='btnUlozitFormularHtml' value='".TULOZIT."' class='tool_button save_button' title='".TULOZIT."' />";
			else
			echo "<input type='submit' name='btnUlozitFormularHtml' value='".TULOZIT."' class='tool_button_disabled save_button_disabled' disabled='disabled' title='".TULOZIT."'/>";
			
		echo "</div>";
		echo "</form>";
?>