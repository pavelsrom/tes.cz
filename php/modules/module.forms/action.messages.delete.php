<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */
 
 
if(!defined('SECURITY_CMS')) exit;
if(!$this->is_access('zpravy','delete',0,1,'content_delete')) return;

$id = $this->get_id_action();
$data = $db->Query("SELECT f.* FROM ".TABLE_FORMULARE." AS f 
				LEFT JOIN ".TABLE_FORMULARE_ZPRAVY." AS z ON f.idFormulare = z.idFormulare 
				WHERE f.idDomeny=".$domain->getId()." AND z.idZpravy=".$id." 
				LIMIT 1");

if($db->numRows($data)==0){
    $systemMessage->add_error(ERROR_NARUSENI_SYSTEMU);
    error_handler_hacking($this->moduleName, ERROR_NARUSENI_SYSTEMU);
    Redirect($this->get_link('zpravy'));
    }

$f = $db->getAssoc($data);
$nazev_formulare = $f['nazev'];			
$db->Query("DELETE FROM ".TABLE_FORMULARE_ZPRAVY." WHERE idZpravy=".$id);

$log->add_log('delete','formular-zprava',$id,$nazev_formulare);

$systemMessage->add_ok(OK_SMAZANO);    				
Redirect($this->get_link('zpravy'));

?>