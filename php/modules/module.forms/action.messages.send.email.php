<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */


if(!defined('SECURITY_CMS')) exit;
if(!is_post('btnOdeslatEmail') || 
    !$this->is_access('zpravy','edit-content',0,1,'send_email')) return;

$idZpravy = $this->get_id_action();
$zprava = get_post('zprava');
$komu = get_post('komu');
$predmet = get_post('predmet');
			
if(!TestEmail($komu,150,false)){
    $systemMessage->add_error($komu." ".ERROR_EMAIL_PRIJEMCE);
    $url = $this->get_link('zpravy',0,'edit-content',$idZpravy);
    Redirect($url);	
    }

if($zprava==''){
    $systemMessage->add_error(ERROR_VYPLNENI_ZPRAVY);
    return;
    }
			
				
			
if(!TestEmail(EMAIL_WEBU, 150, false))
{

    $mujEmail = $login_obj->getEmail();
    $mojeJmeno = $login_obj->getName();
}
else
{
    $mujEmail = EMAIL_WEBU;
    $mojeJmeno= NAZEV_WEBU;	
}
			
$db->Query("UPDATE ".TABLE_FORMULARE_ZPRAVY." SET odpoved='".$zprava."', predmetOdpovedi='".$predmet."' WHERE idZpravy=".$idZpravy);
            
$res2 = SendMail($mujEmail,$mojeJmeno,$komu, $predmet,$zprava, $zprava);

				
if($res2)
    {
    $db->Query("UPDATE ".TABLE_FORMULARE_ZPRAVY." SET stav='odpovezeno' WHERE stav='prijata' AND idZpravy=".$idZpravy);
    $systemMessage->add_ok(OK_ODESLANO);
    
    $log->add_log('send','formular-zprava',$idZpravy,$komu);
    
    }
    else 
    $systemMessage->add_error(ERROR_ODESLANO);

				
$url = $this->get_link('zpravy',0,'edit-content',$idZpravy);
Redirect($url);	


?>