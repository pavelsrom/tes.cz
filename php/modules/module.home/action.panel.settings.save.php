<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

//zabezpeceni
if(!defined('SECURITY_CMS')) exit;
if(!is_action() ||
    (!$this->is_access('panely_nastaveni','edit-settings',0,1,array('settings_edit','superadmin')) &&
     !$this->is_access('panely_nastaveni','new',0,0,array('settings_add','superadmin'))
    )
    ) return;
    

//inicializace promennych
$idPanelu = $this->get_id_action();
$nazev = get_post('nazev');
$systemovyNazev = get_post('systemovy_nazev');
$zobrazit = get_int_post('zobrazit');
            
                    		
if($nazev=='')
    $systemMessage->add_error(ERROR_NEPLATNY_NAZEV);				
    
if($systemMessage->error_exists()) return;

$update = array(
        "idDomeny"=>$domain->getId(),
        "nazev" => $nazev,
        "systemovyNazev" => $systemovyNazev,
        "zobrazit" => $zobrazit
        );
   
    
if($idPanelu>0)
    $db->update(TABLE_PANELY,$update,"idPanelu=".$idPanelu." AND idDomeny=".$domain->getId());
    else
    {
    $db->insert(TABLE_PANELY,$update);
    $idPanelu = $db->lastId();
    }


$log->add_log($this->get_id_action() == 0 ? 'create':'edit-settings','panel',$idPanelu,$nazev);
    
$systemMessage->add_ok(OK_ULOZENO);

if($this->get_id_action() == 0)
    $url = $this->get_link('panely_nastaveni',0,'new');
    else
    $url = $this->get_link('panely_nastaveni',0,'edit-settings',$idPanelu);
    
Redirect($url);

?>