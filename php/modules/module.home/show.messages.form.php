<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined('SECURITY_CMS')) exit;
if(!$this->is_access('zpravy')) return;


$zprava = $this->get_post('zprava');
$predmet = $this->get_secure_post('predmet', true); 
		
$data = $db->Query("SELECT TRIM(CONCAT(jmeno,' ',prijmeni)) AS jmeno, email 
    FROM ".TABLE_UZIVATELE." 
    WHERE idUzivatele=".$login_obj->getId()." 
    LIMIT 1
    ");

$data_email = $db->getAssoc($data);
$email = $data_email['email'];
$jmeno = $data_email['jmeno'];

if($email!='') 
    $email = "&lt;".$email."&gt";
              
$form = new Form();
$form->add_section(TZPRAVA_PRO_ADMINISTRATORA,'email');        
$form->add_plain_text(TOD, $jmeno." ".$email);
$form->add_plain_text(TKOMU, FROM_ADMIN_NAME);
$form->add_text(TPREDMET, 'predmet', $predmet);
$form->add_editor(TZPRAVA, 'zprava', $zprava);
$form->add_submit(TODESLAT, 'btnZpravaProAdmina', 'send_button');
echo $form->get_html_code();

?>