<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */



if(!defined('SECURITY_CMS')) exit;
if(!$this->is_access('sablony', 'edit-content', 0, 1, 'settings_view') && 
   !$this->is_access('sablony', 'new', 0, 0, 'settings_view')) 
    return;


$idTemplate = $this->get_id_action();

if($idTemplate>0){
		  
    if($login_obj->UserPrivilege('admin') || $login_obj->UserPrivilege('superadmin'))
        $where = "(idDomeny=".$domain->getId()." OR idDomeny=0)";
        else
        $where = "idDomeny=".$domain->getId();
    
    $data = $db->Query("SELECT s.*    
                FROM ".TABLE_HTML_SABLONY." AS s 
                WHERE idSablony=".$idTemplate." AND ".$where);
                
    $templ = $db->getAssoc($data);
      
    }
    else
    {
    $templ = array();
    $templ['nazev']="";
    $templ['obsah']="";
    $templ['popis']="";
    $templ['systemova']=1;
            
    }

$main_tools = array();
$main_tools[] = array(
        'ikona' => 'back', 
        "nazev" => TZPET, 
        "aktivni" => 0, 
        "odkaz" => $this->get_link('sablony'), 
        "title" => TZPET_NA_SEZNAM_SABLON);

echo main_tools($main_tools);


$form = new Form();
$form->add_section(TZAKLADNI_UDAJE, "home");
$form->add_text(TNAZEV, 'nazev', $templ['nazev'], 0, true);

if($login_obj->UserPrivilege('admin') || $login_obj->UserPrivilege('superadmin'))
    $form->add_radiobuttons(TSYSTEMOVA_SABLONA,'systemova',$templ['systemova']);

$form->add_text(TPOZNAMKA, 'poznamka', $templ['popis'], 14);
$form->add_editor(TOBSAH, 'obsah', $templ['obsah'], 15);

$res = ($login_obj->UserPrivilege('settings_edit') && $idTemplate>0) || ($login_obj->UserPrivilege('settings_add') && $idTemplate==0);
$form->add_submit()->set_disabled(!$res);
    
echo $form->get_html_code();		

?>