<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */


if(!defined('SECURITY_CMS')) exit;
if(!is_action() ||
    (!$this->is_access('panely','edit-settings',0,1,'settings_edit') &&
     !$this->is_access('panely','new',0,0,'settings_add')
    )
    ) return;

$nazev = get_post('nazev');
$zobrazit = get_int_post('zobrazit');
$vychozi = get_int_post('vychozi');
$idProfilu = $this->get_id_action();
	      
if($nazev=='')
    $systemMessage->add_error(ERROR_NEPLATNY_NAZEV);        
             
if($systemMessage->error_exists()) return;
			
$update = array(
    "idDomeny" => $domain->getId(),
    "nazev" => $nazev,
    "zobrazit" => $zobrazit,
    "vychozi" => $vychozi,
    "idJazyka"=> WEB_LANG_ID
    );
            
if($idProfilu == 0){
    $db->insert(TABLE_PANELY_SABLONY,$update);
    $idProfilu = $db->lastId();
    }
    else
    {				
    $db->update(TABLE_PANELY_SABLONY,$update,"idProfilu=".$idProfilu." AND idDomeny=".$domain->getId());	
    }
	
$systemMessage->add_ok(OK_ULOZENO);

$log->add_log($this->get_url('id_action') == 0 ? 'create':'edit-settings','sablona-panelu',$idProfilu,$nazev);

$url = $this->get_link('panely',0,'edit-settings',$idProfilu); 
Redirect($url);   

?>