<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */



if(!defined('SECURITY_CMS')) exit;
if(!$this->is_access('presmerovani', 'edit-content', 0, 1, 'settings_view') && 
   !$this->is_access('presmerovani', 'new', 0, 0, 'settings_view')) 
    return;

$idRule = $this->get_id_action();

if($idRule>0){
    
    $data = $db->Query("SELECT s.*    
                FROM ".TABLE_PRESMEROVANI." AS s 
                WHERE id=".$idRule." 
                    AND idDomeny=".$domain->getId());
                
    $rule = $db->getAssoc($data);
      
    }
    else
    {
    $rule = array();
    $rule['nazev']="";
    $rule['zdroj']="";
    $rule['cil']="";
    $rule['aktivni']=1;
            
    }

$main_tools = array();
$main_tools[] = array(
        'ikona' => 'back', 
        "nazev" => TZPET, 
        "aktivni" => 0, 
        "odkaz" => $this->get_link('presmerovani'), 
        "title" => TZPET_NA_SEZNAM_PRESMEROVANI);

echo main_tools($main_tools);


$form = new Form();
$form->add_section(TZAKLADNI_UDAJE, "home");
$form->add_text(TZDROJOVA_URL, 'zdroj', $rule['zdroj'], 456, true);
$form->add_text(TCILOVA_URL, 'cil', $rule['cil'], 456, true);

$form->add_radiobuttons(TAKTIVNI,'aktivni',$rule['aktivni']);

$res = ($login_obj->UserPrivilege('settings_edit') && $idRule>0) || ($login_obj->UserPrivilege('settings_add') && $idRule==0);
$form->add_submit()->set_disabled(!$res);
    
echo $form->get_html_code();		

?>