<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */


if(!defined('SECURITY_CMS')) exit;
if(!$this->is_access('nastaveni',"",0,0,'settings_view')) return;

$table = new Table("tNastaveni");
$table->tr_head()
    ->add(TID,"w25")
    ->add(TNAZEV,"t-left")
    ->add(THODNOTA,"w150")
    ->add(TAKCE,"w70");
  
$moduly = $modules->get_allowed_modules();

$where = "";
if(!$login_obj->UserPrivilege('superadmin'))
    $where = "AND n.admin=0"; 
    

foreach($moduly AS $m)
{
    $data = $db->query("SELECT n.idNastaveni AS id, n.nazev,n.typ,IF(nd.hodnota IS NULL, n.hodnota, nd.hodnota) AS hodnota, n.idNapovedy
        FROM ".TABLE_NASTAVENI." AS n
        LEFT JOIN ".TABLE_NASTAVENI_DOMENY." AS nd ON n.idNastaveni=nd.idNastaveni AND nd.idDomeny=".$domain->getId()."
        WHERE n.idModulu=".$m->get_id()."
            AND n.aktivni=1
            ".$where."
        ORDER BY n.poradi, n.idNastaveni       
        ");
        
    if($db->numRows($data) == 0) continue;
    
    if($login_obj->UserPrivilege('settings_edit') && !in_array($m->get_module_system_name(),array('home','admin',''))) 
        $icon = "<a href='".get_link($m->get_module_system_name(),'nastaveni')."'>".icon('content')."</a>";
    elseif(in_array($m->get_module_system_name(),array('home','admin','')) || $m->function)
        $icon = "";           
    else 
        $icon = icon_disabled("content");
    
    $table->tr("sekce",$m->moduleSystemName)
        ->add($m->get_module_name(),"t-left nazev-sekce",0,"",3)
        ->add($icon,"akce");
    
    while($n = $db->getObject($data))
    {
        
        $hodnota = $n->hodnota;
        
        if($n->typ == 'radio')
            $hodnota = $n->hodnota == 1 ? TANO : TNE;
        elseif($n->typ == 'input')
            $hodnota = secureString($n->hodnota);
        elseif($n->typ == 'editor' || $n->typ == 'textarea')
            $hodnota = cut_text(strip_tags($n->hodnota), 200);
        elseif($n->typ == 'barva')
            $hodnota = "<span class='barva' style='background: #".$n->hodnota."'></span>"; 
        elseif($n->typ == 'smajlici')
            $hodnota = $db->get(TABLE_SMAJLICI,'nazev',"idTematu=".intval($n->hodnota)); 
        elseif($n->typ == 'ga')
            {
                $prazdne = GA_PROFIL=="" || GA_EMAIL=="" || GA_HESLO=="";
                $hodnota = $prazdne ? "" : "<i>".TZADANO."</i>";
                
                        
            }
        elseif($n->typ == 'strom')
            {
                $hodnota = unserialize($hodnota);
                $prazdne = false;
                $hodnota_text = "";
                $jazyky = $lang->get_langs();
                
                
                foreach($jazyky AS $idj => $j)
                    if(isset($hodnota[$idj]) && $hodnota[$idj] > 0)
                        $hodnota_text .= strtoupper($jazyky[$idj]['jazyk']).": ".$db->get(TABLE_STRANKY,"nazev","idStranky=".$hodnota[$idj])."<br />";
                        else
                        $hodnota_text .= strtoupper($jazyky[$idj]['jazyk']).": <i class='stav0'>".TNEZADANO."</i><br />";

                
                $hodnota = $hodnota_text;
                
                        
            }
        elseif($n->typ == 'textarea-lang' || $n->typ == 'editor-lang' || $n->typ == 'input-lang')
            {
                $hodnota = unserialize($hodnota);
                $prazdne = false;
                $hodnota_text = "";
                $jazyky = $lang->get_langs();
                
                
                foreach($jazyky AS $idj => $j)
                    if(isset($hodnota[$idj]) && $hodnota[$idj] != '')
                        $hodnota_text .= strtoupper($jazyky[$idj]['jazyk']).": ".TZADANO."<br />";
                        else
                        $hodnota_text .= strtoupper($jazyky[$idj]['jazyk']).": <i class='stav0'>".TNEZADANO."</i><br />";

                
                $hodnota = $hodnota_text;
                
                        
            }
                
        if($hodnota == "")
            $hodnota = "<i class='stav0'>".TNEZADANO."</i>";
        elseif($hodnota == "0" && $n->typ != 'radio')
            $hodnota = "<i class='stav0'>".TNEZADANO."</i>";
        elseif($hodnota == "" && $n->typ == 'ga')
            $hodnota = "<i class='stav0'>".TNEZADANO."</i>";
        
        if($login_obj->UserPrivilege('settings_edit')) 
            $icon = "<a href='".$this->get_link('nastaveni',0,"edit-settings",$n->id)."'>".icon('content')."</a>";           
            else 
            $icon = icon_disabled("content");
        
        $napoveda = "";
        if($n->idNapovedy > 0)
            $napoveda = "<br /><span class='popisek'>".strip_tags(help($n->idNapovedy,true))."</span>";
        
        $table->tr()
            ->add($n->id)
            ->add("<strong>".constant($n->nazev)."</strong>".$napoveda,"t-left")
            ->add($hodnota)
            ->add($icon,"akce");
            
    }
    
    
    
}
    
    
    
echo $table->get_html();


?>