<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */


if(!defined('SECURITY_CMS')) exit;
if(!$this->is_access('preklady-webu')) return;
     
$main_tools = array();
if($login_obj->UserPrivilege('superadmin'))
    $main_tools[] = array(
        'ikona' => 'new', 
        "nazev" => TPRIDAT_PREKLAD, 
        "aktivni" => 0, 
        "odkaz" => $this->get_link('preklady-webu',0,'new')
        );

$form = new FormFilter();
$form->add_text(THLEDAT, 'vyraz', "", 0, false, "", "vyraz");
echo $form->get_html_code(main_tools($main_tools));
        
$table = new Table("tList");
$table->tr_head()
    ->add(TID,"w25")
    ->add(TKONSTANTA,"w150")
    ->add(TPREKLADY)
    ->add(TAKCE);


echo $table->get_html();

$jazyky = $lang->get_langs();

?>

<script type="text/javascript">
<!--


$(function(){
    var oTable = $("#tList").dataTable({
        "sAjaxSource": "<?php echo AJAX_GATEWAY;?>translation.web.list",
        "aoColumns": [{"sClass": "sl1"},{"sClass": "sl2 tleft"}, {"sClass": "sl3","bSortable": false}, {"sClass": "sl4 akce","bSortable": false}],
        "fnServerData": function ( sSource, aoData, fnCallback ) {
                aoData.push({ "name": "vyraz", "value": $("#vyraz").val() });
                $.getJSON( sSource, aoData, function (json) { fnCallback(json); } );
                }
     
    })
    
    $("input#vyraz").bind('keyup change', function() {
            oTable.fnDraw(); 
            });
    
    //prida k bunce th span
    $(".dataTable th").each(function(){
        $(this).html("<span>" + $(this).html() + "</span>");
    })
    
    
    $(document).on("click","a.save", function(){
        var id = $(this).attr('id').substring(4);
        <?php
        foreach($jazyky AS $j)
            echo "var ".$j['jazyk']." = $(\"#".$j['jazyk']."\" + id).val();\n";
        
        ?>
        
        var a = $(this);
        a.parent("td").find("span.miniloader").remove();
        a.before("<span class='miniloader'><?php echo TUKLADAM;?>...</span>");
        
        var tr = $(this).parents("tr").first();
        $.msg();
        tr.addClass("saved");
            
        $.post("<?php echo AJAX_GATEWAY;?>translation.web.save",{<?php foreach($jazyky AS $j) echo $j['jazyk'].": ".$j['jazyk'].", "; ?> id: id},function(data){
            a.parent("td").find("span.miniloader").fadeOut(function(){
                $.msg( 'setClass', 'ok_message' );
                $.msg( 'replace', '<?php echo OK_ULOZENO;?>' );
                tr.removeClass("saved");
                $.msg( 'unblock', 1000);
                });
        });
        
        return false;
        
        
    });
    
    $(document).on("click","a.delete", function(){
        var id = $(this).attr('id').substring(6);
        var tr = $(this).parents("tr").first();
        var c = $(this).parents("tr").find("td").first().text(); 
        
        var text = "<?php echo TOPRAVDU_SMAZAT;?>";

        custom_confirm(text.replace("%s",c) + '?', function(){
            $.msg();
            tr.addClass("deleted");
            
            $.post("<?php echo AJAX_GATEWAY;?>translation.web.delete",{id: id},function(data){
                tr.fadeOut(function(){
                    $.msg( 'setClass', 'ok_message' );
                    $.msg( 'replace', '<?php echo OK_SMAZANO;?>' );
                    oTable.fnDraw();
                    $.msg( 'unblock', 1000);
                    });
                });
            
            })
        
        return false;
        
        
    });
    
    
})
    
  // -->  
</script>





        