<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined('SECURITY_CMS')) exit;
if(!$this->is_access('nastaveni', 'edit-settings',0, 1, 'settings_view')) return;

$idNastaveni = $this->get_id_action();

$where = "";
if(!$login_obj->UserPrivilege('superadmin'))
    $where = "AND n.admin=0"; 

$data = $db->query("SELECT if(nd.hodnota IS NULL, n.hodnota,nd.hodnota) AS hodnota, n.typ, m.konstanta AS modul, n.nazev, n.idNapovedy
    FROM ".TABLE_NASTAVENI." AS n
    LEFT JOIN ".TABLE_CMS_MODULY." AS m ON n.idModulu=m.idModulu
    LEFT JOIN ".TABLE_NASTAVENI_DOMENY." AS nd ON n.idNastaveni=nd.idNastaveni AND nd.idDomeny=".$domain->getId()."
    WHERE n.idNastaveni=".$idNastaveni."
        AND n.aktivni=1
        ".$where."
    LIMIT 1
    ");

if($db->numRows($data) == 0) return;

$main_tools = array();
$main_tools[] = array(
        'ikona' => 'back', 
        "nazev" => TZPET, 
        "aktivni" => 0, 
        "odkaz" => $this->get_link('nastaveni'), 
        "title" => TZPET_NA_SEZNAM_NASTAVENI);

echo main_tools($main_tools);


$nastaveni = $db->getAssoc($data);
if(!defined($nastaveni['modul'])) return;
if(!constant($nastaveni['modul'])) return;

$typ = $nastaveni['typ'];
$hodnota = $nastaveni['hodnota'];
$nazev = constant($nastaveni['nazev']);
$napoveda = $nastaveni['idNapovedy'] > 0 ? strip_tags(help($nastaveni['idNapovedy'],true)) : "";
$name = "hodnota";

$form = new Form();
$form->add_section($nazev,'home',$napoveda);

$nazev = THODNOTA;
if($typ == 'input')
{
    $form->add_text($nazev,$name,$hodnota);
}
elseif($typ == 'barva')
{
    $form->add_text($nazev,$name,$hodnota,0,false,"color");
}
elseif($typ == 'editor')
{
    $form->add_editor($nazev,$name,$hodnota);
}
elseif($typ == "textarea")
{
    $form->add_textarea($nazev,$name,$hodnota);
}
elseif($typ == "radio")
{
    $form->add_radiobuttons($nazev,$name,$hodnota);
}
elseif($typ == "ga")
{
    $rc4 = new rc4crypt;
    $hodnota = unserialize($hodnota);
    $hodnota["GA_HESLO"] = $rc4->endecrypt($domain->get('hash'),$hodnota["GA_HESLO"],'de');
    
    if(!isset($hodnota["GA_HESLO"]))
        $hodnota["GA_HESLO"] = "";
    
    if(!isset($hodnota["GA_PROFIL"]))
        $hodnota["GA_PROFIL"] = "";
        
    if(!isset($hodnota["GA_EMAIL"]))
        $hodnota["GA_EMAIL"] = "";
        
    $form->add_text(TEMAIL,'gaEmail',$hodnota["GA_EMAIL"],131,false,'','gaEmail');
    $form->add_password(THESLO,'gaHeslo',$hodnota["GA_HESLO"],132,false,'','gaHeslo');
    $form->add_text(TID_PROFILU,'gaProfil',$hodnota["GA_PROFIL"],133,false,'','gaProfil');
    $form->add_plain_text("","<a href='#' id='overit_dostupnost'>".TOVERIT_DOSTUPNOST."</a>");	
}
elseif($typ == "smajlici")
{
    $smiles = array();
    $data_smajlici = $db->Query("SELECT idTematu, nazev FROM ".TABLE_SMAJLICI." WHERE zobrazit=1");
    while($smile = $db->getAssoc($data_smajlici))
        $smiles[$smile['idTematu']] = $smile['nazev'];
        
    $form->add_selectbox($nazev,$name,$hodnota,array('' => "-- ".TVYBERTE." --") + $smiles,0,false,"","smajlici_select");

    $form->add_plain_text("","<div id='smajlici'></div>");
}
elseif($typ == "strom")
{
    $hodnota = unserialize($hodnota);
    $typh = isset($hodnota['typ']) ? $hodnota['typ'] : "";
    $l = $lang->get_langs();
    
    foreach($l AS $jazyk)
    {

        $stranky = array(0 => "-- ".TVYBERTE." --") + get_pages(0, $jazyk['idJazyka']);
        $h = isset($hodnota[$jazyk['idJazyka']]) ? intval($hodnota[$jazyk['idJazyka']]) : 0;
        $form->add_selectbox(TSTRANKY." ".strtoupper($jazyk['jazyk']),$name."[".$jazyk['idJazyka']."]",$h,$stranky);
    }
    
    $form->add_hidden('typ',$typh);
            
}
elseif($typ == "textarea-lang" || $typ == 'editor-lang'  || $typ == 'input-lang')
{

            $hodnota = unserialize($hodnota);

            $typh = isset($hodnota['typ']) ? $hodnota['typ'] : "";
            $l = $lang->get_langs();
            $j = 0;
            foreach($l AS $jazyk)
            {
                $napoveda = $j == 0 ? $napoveda : 0;
                $h = isset($hodnota[$jazyk['idJazyka']]) ? $hodnota[$jazyk['idJazyka']] : "";
                                
                if($typ == 'editor-lang')
                    $form->add_editor($nazev." ".strtoupper($jazyk['jazyk']),$name."[".$jazyk['idJazyka']."]",$h,$napoveda);
                elseif($typ == 'textarea-lang')
                    $form->add_textarea($nazev." ".strtoupper($jazyk['jazyk']),$name."[".$jazyk['idJazyka']."]",$h,$napoveda);
                    else
                    $form->add_text($nazev." ".strtoupper($jazyk['jazyk']),$name."[".$jazyk['idJazyka']."]",$h,$napoveda);
                   
               $j++; 
            }
            
            $form->add_hidden('typ_'.$idNastaveni,$typh);
                    
}


$form->add_submit();
echo $form->get_html_code();


?>

<script type="text/javascript">
<!--


$(function(){
    $("#smajlici_select").change(function(){
        var idThemes = $("#smajlici_select option:selected").val();
        $.post('<?php echo AJAX_GATEWAY;?>smiles', {idTematu: idThemes}, function(smiles) {
    		$('#smajlici').html(smiles);
    		});
        })
        
    })
    
    
    $("a#overit_dostupnost").click(function(){
        $.msg();
        var email = $("#gaEmail").val();
        var pass = $("#gaHeslo").val();
        var profil = $("#gaProfil").val();
        
        $.get('<?php echo AJAX_GATEWAY;?>ga.connect.verify', {email: email, pass: pass, profil: profil}, function(data) {
            if(data.error != "")
            {
                $.msg( 'setClass', 'error_message' );
                $.msg( 'replace', data.error );
                $.msg( 'unblock', 3000);
            }
            else
            {
                $.msg( 'setClass', 'ok_message' );
                $.msg( 'replace', data.ok );
                $.msg( 'unblock', 2000);
            }
            
            
		},"json")
        
        return false;
        
    })
    
// -->
</script>