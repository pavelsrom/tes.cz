<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

//zabezpeceni
if(!defined('SECURITY_CMS')) exit;
if(!is_action() ||
    (!$this->is_access('sablony','edit-content',0,1,'content_edit') &&
     !$this->is_access('sablony','new',0,0,'content_add')
    )
    ) return;
    

//inicializace promennych
$idSablony = $this->get_id_action();
$nazev = get_post('nazev');
$pozn = get_post('poznamka');
$obsah = get_post('obsah', "", false);
$systemova = get_int_post('systemova');
            
if($login_obj->UserPrivilege('admin') || $login_obj->UserPrivilege('superadmin') && $systemova == 1)               	
    $idDomeny = 0;
    else
    $idDomeny = $domain->getId(); 
                    		
if($nazev=='')
    $systemMessage->add_error(ERROR_NEPLATNY_NAZEV);			
	
if($systemMessage->error_exists()) return;

$update = array(
        "idDomeny"=>$idDomeny,
        "nazev" => $nazev,
        "obsah" => $obsah,
        "popis" => $pozn,
        "systemova" => $systemova,
        "idJazyka"=> WEB_LANG_ID
        );
			
if(!$login_obj->UserPrivilege('admin') && !$login_obj->UserPrivilege('superadmin'))
{
    $where = " AND idDomeny=".$idDomeny;
    $update["systemova"] = $systemova;
}
    else
    $where = "";        
    
if($idSablony>0)
    $db->update(TABLE_HTML_SABLONY,$update,"idSablony=".$idSablony.$where);
    else
    {
    $db->insert(TABLE_HTML_SABLONY,$update);
    $idSablony = $db->lastId();
    }


$log->add_log($this->get_id_action() == 0 ? 'create':'edit-content','sablona-stranky',$idSablony,$nazev);
    
$systemMessage->add_ok(OK_ULOZENO);

if($this->get_id_action() == 0)
    $url = $this->get_link('sablony',0,'new');
    else
    $url = $this->get_link('sablony',0,'edit-content',$idSablony);
    
Redirect($url);

?>