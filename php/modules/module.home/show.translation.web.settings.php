<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */


if(!defined('SECURITY_CMS')) exit;
if(!$this->is_access('preklady-webu','edit-settings',0,1,'translations') &&
   !$this->is_access('preklady-webu','new',0,0,'translations') 
    ) return;
    
$idPrekladu = $this->get_id_action();
$jazyky = $lang->get_langs();
   
$main_tools = array();
        
if($idPrekladu == 0){
    $preklad = array();
	$preklad['konstanta'] = "";
    
    foreach($jazyky AS $j)
	   $preklad[$j['jazyk']] = "";
			
	}
	else
	{			 
	$data = $db->Query("SELECT * FROM ".TABLE_PREKLADY." WHERE idPrekladu=".$idPrekladu);
	$preklad = $db->getAssoc($data);
    }
		
  
$main_tools[] = array(
    'ikona' => 'back', 
    "nazev" => TZPET, 
    "aktivni" => 0, 
    "odkaz" => $this->get_link('preklady-webu'), 
    "title" => TZPET_NA_PREKLADY
    );

echo main_tools($main_tools);
        
$form = new Form();
$form->add_section(TZAKLADNI_UDAJE, "home");
if($idPrekladu==0)
    $form->add_text(TKONSTANTA,'konstanta',$preklad['konstanta'],0,true);
    else
    $form->add_plain_text(TKONSTANTA,$preklad['konstanta']);
    
foreach($jazyky AS $j)
    $form->add_text(strtoupper($j['jazyk']),$j['jazyk'],$preklad[$j['jazyk']]);


$disabled = !(($login_obj->UserPrivilege('settings_add') && $idPrekladu==0) || ($login_obj->UserPrivilege('settings_edit') && $idPrekladu>0));

$form->add_submit()->set_disabled($disabled);
echo $form->get_html_code();
   
?>