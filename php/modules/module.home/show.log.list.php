<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */


if(!defined('SECURITY_CMS')) exit;
if(!$this->is_access('log') || !MODULE_LOG) return;

$data_uzivatele = $db->query("SELECT CONCAT(prijmeni,' ',jmeno) AS jmeno, idUzivatele AS id FROM ".TABLE_UZIVATELE." ORDER BY prijmeni");

$uzivatele = array(0 => "-- ".TVSECHNO." --");
while($u = $db->getAssoc($data_uzivatele))
    $uzivatele[$u['id']] = $u['jmeno'];


$akce = array(
    0 => "-- ".TVSECHNO." --",
    1 => TVYTVARENI_EDITACE_MAZANI,
    2 => TVYTVARENI_EDITACE,
    3 => TEDITACE,
    4 => TJEN_MAZANI,
    5 => TPRIHLASENI_ODHLASENI, 
    6 => TOSTATNI_LOGOVANE_AKCE
    );

$form = new FormFilter();
$form->set_class('filter2');
$form->add_code("<tr><th>".TDATUM."</th><td><input type='text' name='datum_od' id='datum_od' class='input  kalendar' style='width: 92px;' /> &ndash; <input type='text' name='datum_do' id='datum_do' class='input kalendar' style='width: 92px;'/></td></tr>");
$form->add_selectbox(TUZIVATEL,'uzivatel',0,$uzivatele,0,false,"","uzivatel");
//$form->add_selectbox(ucfirst(TJAZYK),'jazyk',0,$jazyky,0,false,"","jazyk");
$form->add_selectbox(TAKCE,'akce',0,$akce,0,false,"","akce");
echo $form->get_html_code();



$table = new Table("tList");
$table->tr_head()
      ->add(TDATUM, 'w150')
      ->add(TUZIVATEL,'w150')
      ->add(TLOGOVANA_AKCE,'tleft') 
      ;

echo $table->get_html();

?>

<script type="text/javascript">
<!--

$(function(){
var oTable = $("#tList").dataTable({
    "bSortClasses": false,
    "sAjaxSource": "<?php echo AJAX_GATEWAY;?>logs.list",
    "aoColumns": [
        {"sClass": "sl1","bSortable": false},{"sClass": "sl2","bSortable": false},{"sClass": "sl3 tleft","bSortable": false}
        ],
    "fnServerData": function ( sSource, aoData, fnCallback ) {
            aoData.push({ "name": "datum_od", "value": $("#datum_od").val() });
            aoData.push({ "name": "datum_do", "value": $("#datum_do").val() });
            aoData.push({ "name": "uzivatel", "value": $("#uzivatel option:selected").val() });
            aoData.push({ "name": "akce", "value": $("#akce option:selected").val() });                        
            $.getJSON( sSource, aoData, function (json) { fnCallback(json); } );
            }
 
})

$("input#datum_od,input#datum_do").bind('keyup change', function() {
        oTable.fnDraw(); 
        });

//prida k bunce th span
$(".dataTable th").each(function(){
    $(this).html("<span>" + $(this).html() + "</span>");
})

$(".filter2 select").bind('change', function() {
    oTable.fnDraw();
    });


})

// -->
</script>


