<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */


if(!defined('SECURITY_CMS')) exit;
if(!$this->is_access('panely')) return;


$main_tools = array();
        
if($login_obj->UserPrivilege('settings_add'))
    $main_tools[] = array(
        'ikona' => 'new', 
        "nazev" => TPRIDAT_SABLONU, 
        "aktivni" => 0, 
        "odkaz" => $this->get_link('panely',0,'new', 0)
        );

$form = new FormFilter();
$form->add_text(THLEDAT, 'vyraz', "", 0, false, "", "vyraz");
echo $form->get_html_code(main_tools($main_tools));   


$table = new Table("tList");
$table->tr_head()
      ->add(TID, 'w25')
      ->add(TNAZEV)
      ->add(TVYCHOZI,'w25')
      ->add(TAKTIVNI,'w25')
      ->add(TAKCE,"w70");
   
echo $table->get_html();   


?>

<script type="text/javascript">
<!--

$(function(){
var oTable = $("#tList").dataTable({
    "sAjaxSource": "<?php echo AJAX_GATEWAY;?>panels.templates.list",
    "aoColumns": [
        {"sClass": "sl1"},{"sClass": "sl2 tleft"},{"sClass": "sl3"},{"sClass": "sl4"},{"sClass": "akce", "bSortable": false}
        ],
    "fnServerData": function ( sSource, aoData, fnCallback ) {
            aoData.push({ "name": "vyraz", "value": $("#vyraz").val() });
            $.getJSON( sSource, aoData, function (json) { fnCallback(json); } );
            }
 
})

$("input#vyraz").bind('keyup change', function() {
        oTable.fnDraw(); 
        });

//prida k bunce th span
$(".dataTable th").each(function(){
    $(this).html("<span>" + $(this).html() + "</span>");
})



$(document).on("click","a.delete", function(){
        var id = $(this).attr('id').substring(6);
        var tr = $(this).parents("tr").first();
        var c = $.trim($(this).parents("tr").find("td").eq(1).text()); 
        
        var text = "<?php echo TOPRAVDU_SMAZAT;?>";

        custom_confirm(text.replace("%s",c) + '?', function(){
            $.msg();
            tr.addClass("deleted");
            $.post("<?php echo AJAX_GATEWAY;?>panels.templates.delete",{id: id},function(data){
                if(data.error == "")
                {
                    tr.fadeOut(function(){
                    $.msg( 'setClass', 'ok_message' );
                    $.msg( 'replace', data.ok );
                    oTable.fnDraw();
                    $.msg( 'unblock', 500);
                    });
                }
                else
                {
                    $.msg( 'setClass', 'error_message' );
                    $.msg( 'replace', data.error );
                    $.msg( 'unblock', 2000);
                }
                
                              
                },'json');
            
            })
        
        return false;
        
        
    });
    

})

// -->
</script>
