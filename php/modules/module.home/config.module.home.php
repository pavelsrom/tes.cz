<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */


//------------------------------------------------
//definice polozek menu
//------------------------------------------------

$module_menu = array();
$module_menu[] = array(
    "page"  => "dashboard",
    "name"  => TZAKLADNI_PREHLED
    );

if($login_obj->UserPrivilege('settings_view'))
$module_menu[] = array(
    "page"  => "nastaveni",
    "name"  => TNASTAVENI_MODULU
    );
    
$module_menu[] = array(
    "page"  => "osobniUdaje",
    "name"  => TOSOBNI_UDAJE
    );
    
if($login_obj->UserPrivilege('settings_view'))
$module_menu[] = array(
    "page"  => "preklady-webu",
    "name"  => TPREKLADY_WEBU
    );
    
if($login_obj->UserPrivilege('superadmin'))
$module_menu[] = array(
    "page"  => "panely_nastaveni",
    "name"  => TNASTAVENI_PANELU
    ); 
    
$module_menu[] = array(
    "page"  => "sablony",
    "name"  => TSABLONY_STRANEK." ".WEB_LANG
    );
    
$module_menu[] = array(
    "page"  => "panely",
    "name"  => TSABLONY_PANELU." ".WEB_LANG
    );
    
if(MODULE_LINK_CHECKER)
$module_menu[] = array(
    "page"  => "odkazy",
    "name"  => TCHYBNE_ODKAZY
    );
    
if(MODULE_LOG)
$module_menu[] = array(
    "page"  => "log",
    "name"  => TLOGOVANI_AKCI
    );

$module_menu[] = array(
    "page"  => "presmerovani",
    "name"  => TPRESMEROVANI
    );
    
    /*
$module_menu[] = array(
    "page"  => "Zpravy",
    "name"  => TNAPSAT_ADMINISTRATOROVI
    );
    */
    
    
//------------------------------------------------
//definice nadpisu h1 a textu k nadpisu
//------------------------------------------------ 
$module_h1 = array();
$module_h1[] = array(
    "page"  => "OsobniUdaje",
    "name"  => TOSOBNI_UDAJE,
    "text"  => 354
    );  
$module_h1[] = array(
    "page"  => "sablony",
    "action"=> "edit-content",
    "verify_id_action" => 1,
    "name"  => TEDITACE_SABLONY." ".WEB_LANG
    ); 
$module_h1[] = array(
    "page"  => "sablony",
    "action"  => "new",
    "name"  => TNOVA_SABLONA." ".WEB_LANG
    ); 
$module_h1[] = array(
    "page"  => "Sablony",
    "name"  => TSEZNAM_SABLON." ".WEB_LANG,
    "text"  => 16
    ); 
    
$module_h1[] = array(
    "page"  => "panely_nastaveni",
    "name"  => TNASTAVENI_PANELU,
    "text"  => 176
    ); 
$module_h1[] = array(
    "page"  => "panely_nastaveni",
    "name"  => TNASTAVENI_PANELU,
    "action"  => "edit-content",
    "verify_id_action" => 1,
    "text"  => 176
    ); 
    
$module_h1[] = array(
    "page"  => "Panely",
    "action"  => "edit-settings",
    "verify_id_action" => 1,
    "name"  => TEDITOVAT_SABLONU_PANELU." ".WEB_LANG
    );

$module_h1[] = array(
    "page"  => "panely_nastaveni",
    "action"  => "new",
    "name"  => TNOVA_SABLONA." ".WEB_LANG
    );
$module_h1[] = array(
    "page"  => "panely_nastaveni",
    "name"  => TSEZNAM_SABLON_PANELU." ".WEB_LANG,
    "text"  => 352
    );
$module_h1[] = array(
    "page"  => "Zpravy",
    "name"  => TNAPSAT_ADMINISTRATOROVI
    );
$module_h1[] = array(
    "page"  => "dashboard",
    "name"  => TZAKLADNI_PREHLED
    );
$module_h1[] = array(
    "page"  => "log",
    "name"  => TLOGOVANI_AKCI,
    "text"  => 350
    );
$module_h1[] = array(
    "page"  => "odkazy",
    "name"  => TCHYBNE_ODKAZY,
    "text"  => 351
    );
$module_h1[] = array(
    "page"  => "preklady-webu",
    "name"  => TPREKLADY_WEBU,
    "text"  => 353
    );
$module_h1[] = array(
    "page"  => "preklady-webu",
    "action"=> "new",
    "name"  => TPRIDAT_PREKLAD
    ); 
    
$module_h1[] = array(
    "page"  => "nastaveni",
    "name"  => TNASTAVENI_MODULU,
    "text"  => 355
    );
$module_h1[] = array(
    "page"  => "nastaveni",
    "action"=> "edit-settings",
    "verify_id_action" => 1,
    "name"  => TEDITACE_NASTAVENI
    );


$module_h1[] = array(
    "page"  => "presmerovani",
    "action"  => "edit-content",
    "verify_id_action" => 1,
    "name"  => TEDITOVAT_PRAVIDLO
    );

$module_h1[] = array(
    "page"  => "presmerovani",
    "action"  => "new",
    "name"  => TNOVE_PRAVIDLO
    );
$module_h1[] = array(
    "page"  => "presmerovani",
    "name"  => TSEZNAM_PRAVIDEL_PRESMEROVANI,
    "text"  => 455
    );


?>