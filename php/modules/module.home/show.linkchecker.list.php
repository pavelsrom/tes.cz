<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */


if(!defined('SECURITY_CMS')) exit;
if(!$this->is_access('odkazy') || !MODULE_LINK_CHECKER) return;


$pocet_odkazu = intval($db->get(TABLE_ODKAZY,'COUNT(idOdkazu)',"idDomeny=".$domain->getId()));
$pocet_odkazu_pro_jednorazove_zkontrolovani = POCET_ODKAZU_PRO_JEDNORAZOVOU_KONTROLU_AJAXEM;


$ajax_graf =  "
    <div class='horni-tlacitka'>
        <div id='ukazatel_prubehu'>
            <div id='progress_bar'>
                <div id='stav_kontroly_start'><a href='#' class='btnKontrola'>".TZKONTROLOVAT_VSECHNY_ODKAZY."</a></div>
                <div id='stav_kontroly_prubeh'>".TPROBIHA_KONTROLA_ODKAZU."...</div>
                <div id='stav_kontroly_dokonceno'>".TKONTROLA_DOKONCENA." (<a href='#' class='btnKontrola'>".TOPAKOVAT."</a>)</div>
                <div id='graph_box'>
                    <div id='graph_kontrola'></div>
                    <div id='graph_text'>0% (0 / ".$pocet_odkazu.")</div>
                </div>
                <div id='vysledek_kontroly'>
                    ".TPOCET_CHYBNYCH_ODKAZU_NA_STRANKY.": <span id='pocet_chybnych_odkazu'>0</span><br />
                    ".TPOCET_CHYBNYCH_ODKAZU_NA_OBRAZKY.": <span id='pocet_chybnych_obrazku'>0</span>
                </div>
            </div>
        </div>
    </div>
    <div class='cleaner'></div>
    ";

$jen_chybne = array(
    0 => "-- ".TVSECHNO." --",
    1 => TJEN_CHYBNE_ODKAZY,
    2 => TJEN_SPRAVNE_ODKAZY
    );

$form = new FormFilter();
$form->add_selectbox(TODKAZY, 'jen_chybne', 1, $jen_chybne, false, "", "","jen_chybne");
echo $form->get_html_code($ajax_graf);


$table = new Table("tList");
$table->tr_head()
      ->add(TTYP)
      ->add(TNAZEV,'tleft')
      ->add(TJAZYK,'tcenter')
      ->add(TNALEZENE_ODKAZY,'tleft')
      ->add(TAKCE,'w70')
      ;

echo $table->get_html();

?>

<script type="text/javascript">
<!--


$(function(){
var oTable = $("#tList").dataTable({
    "bSortClasses": false,
    "sAjaxSource": "<?php echo AJAX_GATEWAY;?>linkchecker.list",
    "aoColumns": [
        {"sClass": "sl1", "bSortable": false},{"sClass": "sl2 tleft", "bSortable": false},{"sClass": "sl3 tleft", "bSortable": false},{"sClass": "sl2 tleft", "bSortable": false},{"sClass": "akce", "bSortable": false}
        ],
    "fnServerData": function ( sSource, aoData, fnCallback ) {
            aoData.push({ "name": "jen_chybne", "value": $("#jen_chybne option:selected").val() });
            $.getJSON( sSource, aoData, function (json) { fnCallback(json); } );
            }
 
})

//prida k bunce th span
$(".dataTable th").each(function(){
    $(this).html("<span>" + $(this).html() + "</span>");
})

$(".filter select").bind('change', function() {
    oTable.fnDraw();
    });



    var pocet_chybnych_odkazu = 0;
    var pocet_chybnych_odkazu_obrazku = 0; 
    var limit = <?php echo $pocet_odkazu_pro_jednorazove_zkontrolovani; ?>;
    var pocet_vsech_odkazu = <?php echo $pocet_odkazu; ?>;
    var zkontrolovano_odkazu = 0;
    var procenta = 0;
    var pocet_spusteni = <?php echo ceil($pocet_odkazu / $pocet_odkazu_pro_jednorazove_zkontrolovani); ?>;
    
    function novy_cyklus_kontroly( i )
    {
        //vyhodnoceni
        if(i == pocet_spusteni) 
        {
        
        $("#stav_kontroly_start, #stav_kontroly_prubeh").hide();
        $("#stav_kontroly_dokonceno").fadeIn(); 
        
        oTable.fnDraw();
        return true;
        }
            
        //spousteni skriptu
        if(i == (pocet_spusteni - 1))
        {
            limit = limit - ((pocet_spusteni * limit) - pocet_vsech_odkazu);  
        }
                
                
        $.post("<?php echo AJAX_GATEWAY;?>linkchecker.process&limit=" + limit, null , function(data){
            if(data)
            {
                pocet_chybnych_odkazu += data.chybne_odkazy;
                pocet_chybnych_odkazu_obrazku += data.chybne_odkazy_obrazku;
                zkontrolovano_odkazu += limit;
                procenta = Math.round(100 / pocet_vsech_odkazu * zkontrolovano_odkazu);
                if(procenta < 1) procenta = 1;
                if(procenta > 100) procenta = 100;
                $("#graph_kontrola").css("width",  procenta + "%");
                $("#graph_text").text(procenta + "% (" + zkontrolovano_odkazu + " / " + pocet_vsech_odkazu + ")" );
                $("#pocet_chybnych_odkazu").html(pocet_chybnych_odkazu);
                $("#pocet_chybnych_obrazku").html(pocet_chybnych_odkazu_obrazku);
                
                if(pocet_chybnych_odkazu > 0)
                    $("#pocet_chybnych_odkazu").css('color','red');
                    
                if(pocet_chybnych_odkazu_obrazku > 0)
                    $("#pocet_chybnych_obrazku").css('color','red');    
                
                novy_cyklus_kontroly(i + 1);
            }
            else
                return false;
                
        }, 'json')
            
    }
        
    $("#ukazatel_prubehu a.btnKontrola").click(function(){
        pocet_chybnych_odkazu = 0;
        pocet_chybnych_odkazu_obrazku = 0; 
        zkontrolovano_odkazu = 0;
        $("#graph_text").text("0% (0 / " + pocet_vsech_odkazu + ")");
        $("#graph_kontrola").css('width','0.5%');
        $("#stav_kontroly_start, #stav_kontroly_dokonceno").hide();
        $("#stav_kontroly_prubeh").fadeIn();
        $("#ukazatel_prubehu").animate({height: '85px'},300);
        $("#vysledek_kontroly").fadeIn();
        novy_cyklus_kontroly(0);
        return false;
            
    })







})

// -->
</script>
