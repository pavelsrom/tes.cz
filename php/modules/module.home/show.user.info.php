<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */


if(!defined('SECURITY_CMS')) exit;
if(!$this->is_access('osobniudaje')) return;


$data = $db->Query("SELECT u.*, p.nazev AS prava, TRIM(CONCAT(u.jmeno,' ', u.prijmeni)) AS jmeno,
            CONCAT(u.ulice,', ',u.mesto,', ',u.psc) AS adresa,
            CONCAT(u.nastaveniDashboardLevy,u.nastaveniDashboardPravy) AS dashboard 
			FROM ".TABLE_UZIVATELE." AS u
			LEFT JOIN ".TABLE_CMS_PRAVA." AS p ON u.idPrava=p.idPrava
			WHERE idUzivatele=".$login_obj->getId());
			
if($db->numRows($data)==0) return;
$user = $db->getAssoc($data);

$prava = array(
    TVYTVARET_OBJEKTY       => 'settings_add',
	TMAZAT_OBJEKTY          => 'settings_delete',
    TPROHLIZET_NASTAVENI    => 'settings_view',
    TEDITOVAT_NASTAVENI     => 'settings_edit', 
   	TPRIDAVAT_OBSAH         => 'content_add', 
	TPROHLIZET_OBSAH        => 'content_view', 
	TEDITOVAT_OBSAH         => 'content_edit', 
	TMAZAT_OBSAH            => 'content_delete', 
	TPROHLIZET_STATISTIKY   => 'statistics_view', 
	TPOSILAT_EMAILY         => 'send_email', 
	TNASTAVOVAT_PRISTUPY    => 'set_privilege', 
	TEXPORT_DAT             => 'export', 
	TIMPORT_DAT             => 'import', 
	TUPRAVOVAT_SEO          => 'seo', 
	TUPRAVOVAT_PREKLADY     => 'translations'
    );

$prava_seznam = "";
foreach($prava AS $popis => $p)
    if($login_obj->UserPrivilege($p))
        $prava_seznam .= "- ".$popis."<br />";


	
$form = new Form();
$form->add_section(TZAKLADNI_UDAJE,'home');
$form->add_plain_text(TID_UZIVATELE, $user['idUzivatele']);
$form->add_plain_text(TJMENO_A_PRIJMENI, $user['jmeno']);
//$form->add_plain_text(TADRESA, $user['adresa']);
$form->add_plain_text(TPRIHLASOVACI_EMAIL, $user['email']);
$form->add_plain_text(TPRISTUPOVA_PRAVA, constant($user['prava']));
$form->add_plain_text(TSEZNAM_OPRAVNENI, $prava_seznam);

$form->add_section(TZMENA_HESLA,'pass');        
$form->add_password(TSTARE_HESLO, 'stareHeslo');
$form->add_password(TNOVE_HESLO, 'noveHeslo1');
$form->add_password(TNOVE_HESLO." - ".TZNOVU, 'noveHeslo2');
$form->add_submit();


$form->add_section(TNASTAVENI_ADMINISTRACE,'panel-setting'); 
$form->add_selectbox(TJAZYK_ADMINISTRACE,'jazyk',$user['nastaveniJazyk'],array('cz' => TCESKY,'en'=> TANGLICKY));
$form->add_selectbox(TVZHLED_ADMINISTRACE,'vzhled',$user['nastaveniVzhled'],array('gray' => TVYCHOZI));

$data = $db->query("SELECT * FROM ".TABLE_DASHBOARD." WHERE zobrazit=1 ORDER BY nazev");
$nastaveni_uzivatele = $user['dashboard']; 
$dashboard = array();
$dashboard_checked = array();
while($d = $db->getAssoc($data))
{
    if($d['modul']!='' && !constant($d['modul'])) 
        continue;
    
    $id = $d['idBoxu'];
    $nazev = constant($d['nazev']);
    if(strstr($nastaveni_uzivatele,"#".$id."#"))
        $dashboard_checked[] = $id;
    $dashboard[$id] = $nazev;
}

$form->add_section(TNASTAVENI_DASHBOARDU,'panel-setting');  
$form->add_checkboxes(TBOXY,'dashboard',$dashboard_checked,$dashboard,299);

echo $form->get_html_code();


?>