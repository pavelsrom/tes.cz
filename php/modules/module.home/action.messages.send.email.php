<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined('SECURITY_CMS')) exit;
if(!$this->is_post('btnZpravaProAdmina')) return;

$zprava = $this->get_post('zprava');             
$predmet = $this->get_post('predmet');

if($zprava == '') {
    $systemMessage->add_error(ERROR_VYPLNENI_ZPRAVY);
    return;
    }


$data = $db->Query("SELECT u.*,p.nazev AS prava FROM ".TABLE_UZIVATELE." AS u 
				LEFT JOIN ".TABLE_CMS_PRAVA." AS p ON p.idPrava=u.idPrava 
				WHERE idUzivatele=".$login_obj->getId());
				
if($db->numRows($data)!=1){
    $systemMessage->add_error(ERROR_CHYBA_SYSTEMU);
    error_handler_hacking($this->moduleName, ERROR_CHYBA_SYSTEMU);
    return;
    }
				
$user = $db->getAssoc($data);
			
$info = "Id uživatele: ".$user['idUzivatele']."<br />";
$info .= "Práva uživatele: ".$user['prava']."<br />";
$info .= "Doména: ".$domain->getName()."<br /><br />";
            
$reg = "%(<[body].*?>)%is";

$zprava = preg_replace($reg,'<body>'.$info.'${1}',$zprava);
        
$res = SendMail($user['email'], $user['jmeno']." ".$user['prijmeni'], FROM_ADMIN_EMAIL, $predmet, $zprava, $zprava);

$log->add_log('send','administrator-email',0,FROM_ADMIN_EMAIL);
            
if($res)
{
    $systemMessage->add_ok(OK_ZPRAVA_ODESLANA);
    $url = $this->get_link('zpravy');
    Redirect($url);
    }
    else
    {
    $systemMessage->add_error(ERROR_ODESLANO);
    return;	
    }


?>