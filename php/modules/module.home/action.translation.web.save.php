<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined('SECURITY_CMS')) exit;
if(!is_action() || 
    (!$this->is_access('preklady-webu','edit-settings',0,1,array('settings_edit','superadmin')) &&
   !$this->is_access('preklady-webu','new',0,0,array('settings_add','superadmin'))) 
    ) return;
    
$idPrekladu = $this->get_id_action();
$jazyky = $lang->get_langs();
$konstanta = get_post('konstanta');

$insert = array();
foreach($jazyky AS $j)
    if(is_post($j['jazyk']))
        $insert[$j['jazyk']] = get_post($j['jazyk']);
        
if(!TestStringEN($konstanta, 240, false)) 
    $systemMessage->add_error(ERROR_KONSTANTA_MUSI_BYT_VYPLNENA); 

$test = $db->query("SELECT * FROM ".TABLE_PREKLADY." WHERE konstanta='".$konstanta."' AND idPrekladu!=".$idPrekladu);
if($db->numRows($test) > 0)
    $systemMessage->add_error(ERROR_KONSTANTA_JIZ_EXISTUJE);

if($systemMessage->error_exists()) return;

if($idPrekladu == 0)
{
    $insert = $insert + array("konstanta" => $konstanta);
    $db->insert(TABLE_PREKLADY, $insert);
    $idPrekladu = $db->lastId();
}
else
{
    $db->update(TABLE_PREKLADY,$insert,"idPrekladu=".$idPrekladu);
}

$log->add_log('create',"preklady-web",$idPrekladu,$konstanta);



$systemMessage->add_ok(OK_ULOZENO);

$url = $this->get_link('preklady-webu',0,'new'); 
Redirect($url);

?>