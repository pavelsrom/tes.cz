<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */



if(!defined('SECURITY_CMS')) exit;
if(!$this->is_access('panely_nastaveni', 'edit-settings', 0, 1, array('settings_view','superadmin')) && 
   !$this->is_access('panely_nastaveni', 'new', 0, 0, array('settings_add','superadmin'))) 
    return;


$idPanelu = $this->get_id_action();

if($idPanelu>0){
   
    $data = $db->Query("SELECT * FROM ".TABLE_PANELY." WHERE idPanelu=".$idPanelu." AND idDomeny=".$domain->getId());
                
    $panel = $db->getAssoc($data);
      
    }
    else
    {
    $panel = array();
    $panel['nazev'] = "";
    $panel['systemovyNazev'] = "";
    $panel['zobrazit'] = 1;
            
    }

$main_tools = array();
$main_tools[] = array(
        'ikona' => 'back', 
        "nazev" => TZPET, 
        "aktivni" => 0, 
        "odkaz" => $this->get_link('panely_nastaveni'), 
        "title" => TZPET_NA_SEZNAM_PANELU);

echo main_tools($main_tools);


$form = new Form();
$form->add_section(TZAKLADNI_UDAJE, "home");
$form->add_text(TNAZEV." - ".TKONSTANTA, 'nazev', $panel['nazev'], 0, true);
$form->add_text(TSYSTEMOVY_NAZEV, 'systemovy_nazev', $panel['systemovyNazev'], 0, true);
$form->add_radiobuttons(TZOBRAZIT,'zobrazit',$panel['zobrazit']);

$res = ($login_obj->UserPrivilege('settings_edit') && $idPanelu>0) || ($login_obj->UserPrivilege('settings_add') && $idPanelu==0);
$form->add_submit()->set_disabled(!$res);
    
echo $form->get_html_code();		

?>