<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */


//zabezpeceni pristupu
if(!defined('SECURITY_CMS')) exit;
if(!$this->is_access('osobniudaje') || !is_action()) return;


//inicializace promennych
$pass = get_post('stareHeslo');
$npass1 = get_post('noveHeslo1');
$npass2 = get_post('noveHeslo2');
	
$vzhled = get_post('vzhled','gray');
$jazyk = get_post('jazyk','cz');  
    
$dashboard_novy = isset($_POST['dashboard']) ? (array) $_POST['dashboard'] : array();
$dashboard_stary = $db->get(TABLE_UZIVATELE,"CONCAT(nastaveniDashboardLevy,'x',nastaveniDashboardPravy)","idUzivatele=".$login_obj->getId());
$dashboard_data = $db->query("SELECT idBoxu FROM ".TABLE_DASHBOARD."");
while($d = $db->getAssoc($dashboard_data))
    $dashboard_vsechny[] = $d['idBoxu'];

$dd = explode("x",$dashboard_stary);
$l = $dd[0];
$p = $dd[1];

$zobrazit_boxy = "";
foreach($dashboard_vsechny AS $idBoxu)
{
    if(in_array($idBoxu, $dashboard_novy) && strstr($dashboard_stary,"#".$idBoxu."#")) continue;
    
    if(in_array($idBoxu, $dashboard_novy) && !strstr($dashboard_stary,"#".$idBoxu."#"))
        $l .= "#".$idBoxu."#"; 
        
    if(!in_array($idBoxu, $dashboard_novy) && strstr($dashboard_stary,"#".$idBoxu."#"))
    {
        $l = str_replace("#".$idBoxu."#","#",$l);
        $p = str_replace("#".$idBoxu."#","#",$p);
    }
    
}

$l = str_replace(array("##","###"),array("#","#"), $l);
$p = str_replace(array("##","###"),array("#","#"), $p);

$update = array(
    "nastaveniDashboardLevy" => $l, 
    "nastaveniDashboardPravy" => $p,
    "nastaveniJazyk" => $jazyk,
    "nastaveniVzhled"=> $vzhled 
    );

if($npass1 != "")
{            
    //overeni spravnosti hodnot
    if(!TestStringEN($npass1, 100, false)) 
        $systemMessage->add_error(ERROR_NEPLATNE_HESLO);

    			
    if($npass1 != $npass2)
        $systemMessage->add_error(ERROR_HESLA_NESOUHLASI);

    		
    $data1 = $db->Query("SELECT * FROM ".TABLE_UZIVATELE." WHERE idUzivatele=".$login_obj->getId()."");
    
    $hash = $db->get(TABLE_UZIVATELE,"heslo","idUzivatele=".$login_obj->getId());
    
    //if($db->numRows($data1)==0)
    if(!password_verify($npass1,$hash))
        $systemMessage->add_error(ERROR_NEPLATNE_HESLO);
        
    if($systemMessage->error_exists()) 
        return;
        
        
    $update["heslo"] = password_hash($npass1, PASSWORD_DEFAULT);
}    

$_SESSION['jazyk'] = $jazyk;		
        
//ulozeni do db	
$db->update(TABLE_UZIVATELE,$update,"idUzivatele=".$login_obj->getId());

$log->add_log('edit-settings','osobni-udaje');

$systemMessage->add_ok(OK_ULOZENO);   
Redirect($this->get_link("osobniudaje"));	


?>