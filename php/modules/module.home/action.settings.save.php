<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */




if(!defined('SECURITY_CMS')) exit;
if(!is_action() || !$this->is_access('nastaveni','edit-settings',0,1,'settings_edit')) return;

$idNastaveni = $this->get_id_action();


$typ = $db->get(TABLE_NASTAVENI,'typ',"idNastaveni=".$idNastaveni);



if($typ == 'ga')
{
    $hash = $domain->get('hash');
    $h = array(
        "GA_PROFIL" => get_post('gaProfil'),
        "GA_HESLO"  => get_post('gaHeslo'),
        "GA_EMAIL"  => get_post('gaEmail')
        );
    
    $rc4 = new rc4crypt;
    $h['GA_HESLO'] = $rc4->endecrypt($hash,$h['GA_HESLO']);
        
    $hodnota = serialize($h);
            
}
elseif($typ == "strom")
{
    $hodnoty = get_array_post('hodnota');
    $typh = get_post('typ');
    
    foreach($hodnoty AS $idj => $h)
    {
        $id_rodic_stary = $db->get(TABLE_STRANKY,"idStranky","idDomeny=".$domain->getId()." AND typ='".$typh."' AND idJazyka=".$idj);
        $id_rodic_stary = intval($id_rodic_stary);
        $id_rodic = intval($h);
        $update = array("idRodice" => $id_rodic);
        $where = " idDomeny=".$domain->getId()." AND idRodice=".$id_rodic_stary." AND idJazyka=".$idj." AND typ!='clanky'";
        $where1 = "idDomeny=".$domain->getId()." AND idJazyka=".$idj." AND typ='".$typh."'";
        $where2 = "idDomeny=".$domain->getId()." AND idJazyka=".$idj." AND idStranky=".$id_rodic;
        
        if(in_array($typh, array("sitemap",'vyhledavani','objednavka','obchodni-podminky','email')))
        {
            
        }
        else
        {
            if($typh == 'stranka-galerie')
                $where .= " AND typ IN ('galerie','galerie-slozka','galerie-fotka')";
            elseif($typh == 'kalendar-akci')
                $where .= " AND typ IN ('akce','kalendar-typ')";
            elseif($typh == 'novinky')
                $where .= " AND typ = 'novinka'";
            elseif($typh == 'tagy')
                $where .= " AND typ = 'tag'";  
            else
                continue;
                     
            $db->update(TABLE_STRANKY,$update, $where); 
        }
        
        $db->update(TABLE_STRANKY,array('typ' => 'stranka'),$where1);
        $db->update(TABLE_STRANKY,array('typ' => $typh),$where2);
        
    }
    
    $hodnota = array();
    $hodnoty['typ'] = $typh;
    $hodnota = serialize($hodnoty);
    
}
elseif($typ == 'textarea-lang' || $typ == 'editor-lang' || $typ == 'input-lang')
{
    $hodnoty = get_array_post('hodnota');
    $typh = get_post('typ');
    $hodnoty['typ'] = $typh;  
    $hodnota = serialize($hodnoty);

    
}
else
    $hodnota = get_post('hodnota');

$idRelace = $db->get(TABLE_NASTAVENI_DOMENY,'idRelace',"idNastaveni = ".$idNastaveni." AND idDomeny=".$domain->getId());
$idRelace = intval($idRelace);

if($idRelace > 0)
    $db->update(TABLE_NASTAVENI_DOMENY,array("hodnota" => $hodnota),"idRelace=".$idRelace);
    else
    $db->insert(TABLE_NASTAVENI_DOMENY,array("hodnota" => $hodnota,"idNastaveni" => $idNastaveni,"idDomeny" => $domain->getId()));
    

$log->add_log('edit-settings','nastaveni');            

$systemMessage->add_ok(OK_ULOZENO);
Redirect( $this->get_link("nastaveni",0,"edit-settings",$idNastaveni) );

?>