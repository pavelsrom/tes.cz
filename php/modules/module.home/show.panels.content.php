<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

if(!defined('SECURITY_CMS')) exit;
if(!$this->is_access('panely','edit-settings',0,1,'settings_view') &&
   !$this->is_access('panely','new',0,0,'settings_add')
    ) return;


$idProfilu = $this->get_id_action();
$main_tools[] = array(
                'ikona' => 'back', 
                "nazev" => TZPET, 
                "aktivni" => 0, 
                "odkaz" => $this->get_link('panely'), 
                "title" => TZPET_NA_SEZNAM_SABLON
                );

if($idProfilu > 0) {
    $data = $db->Query("SELECT p.*
            FROM ".TABLE_PANELY_SABLONY." AS p 
			WHERE p.idDomeny=".$domain->getId()."
				AND p.idProfilu=".$idProfilu."
			GROUP BY p.idProfilu
			ORDER BY p.idProfilu");
            
    $panel = $db->getAssoc($data);
    }
    else
    {
        $panel = array();
        $panel['nazev'] = "";
        $panel['vychozi'] = 0;
        $panel['zobrazit'] = 1;       
   	}
		
echo main_tools($main_tools);

$form = new Form();
$form->add_section(TZAKLADNI_UDAJE, "home");
$form->add_text(TNAZEV, 'nazev', $panel['nazev'], 0, true,"","nazev");  

if($login_obj->UserPrivilege("superadmin"))
    $form->add_radiobuttons(TVYCHOZI_SABLONA, 'vychozi', $panel['vychozi'], null, 300,"vychozi");
    else
    $form->add_hidden('vychozi',$panel['vychozi'])->set_class("vychozi");      
 

$form->add_radiobuttons(TZOBRAZOVAT, 'zobrazit', $panel['zobrazit'], null, 0,"zobrazit");      
 
$res = ($login_obj->UserPrivilege('settings_edit') && $idProfilu>0) || ($login_obj->UserPrivilege('settings_add') && $idProfilu==0);
$form->add_submit()->set_disabled(!$res)->set_id("save");
				
if($idProfilu == 0)
{
    echo $form->get_html_code();
    return;
}




//---------------------------------------
// systemove funkce
//---------------------------------------

function GetBlockItem($typ, $id, $nazev, $nadpis=""){
    $html = "";
    
	$html .= "<div class='s-".$typ." blok-item' id='".$typ."_".$id."'>";
	$html .= "<span class='blok-typ'>".ucfirst($nadpis)."</span><br />";
	$html .= "<span class='blok-nazev'>".$nazev."</span>";
	$html .= "</div>";
    
    return $html;
	}
    
//vraci false pokud neni box obsahem zadneho z panelu, v opacnem pripade vraci systemovy nazev panelu
function je_zarazen_do_panelu($box,$panely)
{
    foreach($panely AS $sys => $p)
    {
        foreach($p AS $pp)
            
            if($pp['sys'] == $box)
                return $sys;
            
    }
    
    return false;
}


//-----------------------------------------
//nacteni boxu podle typu
//-----------------------------------------

$boxy = array();

$data_typy = $db->query("SELECT t.*, m.konstanta 
    FROM ".TABLE_PANELY_POLOZKY_TYPY." AS t
    LEFT JOIN ".TABLE_CMS_MODULY." AS m ON t.idModulu = m.idModulu
    ");
while($t = $db->getObject($data_typy))
{
    if($t->konstanta != "" && defined($t->konstanta) && !constant($t->konstanta)) continue;
    
    if($t->systemovy == 1)
    {
        $boxy[$t->systemovyNazev][1] = array(
            'nazev' => constant($t->nazev), 
            "sys" => "1".$t->systemovyNazev,
            "popis" => "",
            "typ" => $t->systemovyNazev
            );

        
    }
    else
    {
        if($t->systemovyNazev == 'anketa')
            $data = $db->Query("SELECT idAnkety AS id, nazev FROM ".TABLE_ANKETY." WHERE zobrazit=1 AND idDomeny=".$domain->getId());
        elseif($t->systemovyNazev == 'formulare')
            $data = $db->Query("SELECT idFormulare AS id, nazev FROM ".TABLE_FORMULARE." WHERE zobrazit=1 AND idDomeny=".$domain->getId());
        elseif($t->systemovyNazev == 'editbox')
            $data = $db->Query("SELECT idEditboxu AS id, nazev FROM ".TABLE_EDITBOXY." WHERE zobrazit=1 AND idDomeny=".$domain->getId()." AND idJazyka=".WEB_LANG_ID);    
        elseif($t->systemovyNazev == 'facebook')
            $data = $db->Query("SELECT idPluginu AS id, nazev FROM ".TABLE_FACEBOOK_PLUGINY." WHERE zobrazit=1 AND idDomeny=".$domain->getId());
                
        while($b = $db->getAssoc($data)){
    		$boxy[$t->systemovyNazev][$b['id']] = array(
                'nazev' => constant($t->nazev),
                "sys"   => $b['id'].$t->systemovyNazev,
                'typ'   => $t->systemovyNazev,
                "popis" => $b['nazev']
                );
    		}
        
    }
    
    
}

//------------------------------------
//nacteni panelu
//------------------------------------

$panely = array();
$panely_nazvy = array();

$data_panely = $db->query("SELECT * FROM ".TABLE_PANELY." WHERE zobrazit=1 AND idDomeny=".$domain->getId()." ORDER BY priorita");

while($p = $db->getObject($data_panely))
{
    $panely[$p->systemovyNazev] = array();
    $panely_nazvy[$p->systemovyNazev] = constant($p->nazev);  
}
    
    
foreach($panely AS $idp => $panel)
{
    $data = $db->Query("SELECT pt.systemovyNazev AS podtyp, pp.idObjektu AS id, pt.nazev, pt.systemovy
        FROM ".TABLE_PANELY_POLOZKY." AS pp
        LEFT JOIN ".TABLE_PANELY_POLOZKY_TYPY." AS pt ON pp.idTypu = pt.idTypu 
        LEFT JOIN ".TABLE_PANELY." AS p ON pp.idPanelu = p.idPanelu		
        WHERE pp.idProfilu=".$idProfilu."
            AND p.systemovyNazev = '".$idp."'
        GROUP BY pp.idBloku 
		ORDER BY pp.priorita
        ");
        
    while($p = $db->getAssoc($data))
        if(isset($boxy[$p['podtyp']][$p['id']]))
            $panely[$idp][] = array(
                'id'=>$p['id'], 
                'typ'=>$p['podtyp'], 
                'sys' => $p['id'].$p['podtyp'],
                'nazev'=>constant($p['nazev']),
                "popis" => $boxy[$p['podtyp']][$p['id']]['popis']
                );

     
  
        
}

//---------------------------------------------
//zobrazeni panelu s nevyuzitymi boxy
//---------------------------------------------

if(count($panely) > 2)
    $width = 100 / count($panely);
    else
    $width = 30;  



$html = "";
$html .= "<div id='bloky' class='blok-group'>";
$html .= help(453,true);
$html .= "<div class='group-header'>".TSEZNAM_VYTVORENYCH_OBJEKTU."</div>";
$html .= "<div id='bloky-box-in' class='box-items'>";


foreach($boxy AS $systemovyNazev => $bx)
{
    foreach($bx AS $idx => $b)
    {
        if(!je_zarazen_do_panelu($b['sys'],$panely)) 
        {
            $html .= GetBlockItem($systemovyNazev, $idx, $b['popis'], $b['nazev']);
        }				
    }    
}

$html .= "<div class='cleaner'></div>"; 	
$html .= "</div>";	
$html .= "</div>";
$html .= "<div class='cleaner'></div>";

//---------------------------------------------
//zobrazeni panelu s pouzitymy boxy
//---------------------------------------------




$html .= "<div class='panely'>";
foreach($panely AS $systemovyNazev => $panel)
{
    $html .= "<div style='width: ".$width."%' class='f-left'>";
    $html .= "<div id='".$systemovyNazev."' class='blok-group sort-panel'>";
	$html .= "<div class='group-header'>".$panely_nazvy[$systemovyNazev]."</div>";
	$html .= "<div id='".$systemovyNazev."-box-in' class='box-items panel-obsah'>";
    
    foreach($panel AS $p)
    {
        if(je_zarazen_do_panelu($p['sys'],$panely) == $systemovyNazev) 
            $html .= GetBlockItem($p['typ'], $p['id'], $boxy[$p['typ']][$p['id']]['popis'], $boxy[$p['typ']][$p['id']]['nazev']);	
    }
    $html .= "</div>";
    $html .= "</div>";
    $html .= "</div>";
}
	

$html .= "</div>";




$form->add_section(TOBSAH_PANELU,'panel-setting',help(301,true));
$form->add_code("<td><td colspan='2'>".$html."</td></td>");
echo $form->get_html_code();


?>

<script type="text/javascript">
<!--

$(function(){
    
    function create_panels_of_blocks(){
        $.msg();
        
        var nazev = $("#nazev").val();
        var zobrazit = $("input.zobrazit:checked").val();
        var vychozi = $("input.vychozi:checked").val();
        if(!vychozi)
            vychozi = $("input.vychozi").val();
        
        var id_profilu = <?php echo $idProfilu; ?>;
        var res = {};
        
    	$(".sort-panel").each(function(){
    	   var id = $(this).attr("id");
           var serial = $(this).find('.box-items').sortable("toArray");
    	   res[id] = serial;
    	})
        
        res["idProfilu"] = id_profilu;
        res["nazev"] = nazev;
        res["zobrazit"] = zobrazit;
        res["vychozi"] = vychozi;
        
    	$.post("<?php echo AJAX_GATEWAY;?>panels.template.save", res, function(data){
    	   
    	   if(data.error != "")
           {
                $.msg( 'setClass', 'error_message' );
                $.msg( 'replace', data.error );
                $.msg( 'unblock', 2000);
           }
           else
           {
                $.msg( 'setClass', 'ok_message' );
                $.msg( 'replace', data.ok );
                $.msg( 'unblock', 1000);
           }
            
    	},"json");
        
        
        return false;
        
       }
    
    //$("#ulozit_nastaveni_panelu").click(create_panels_of_blocks);
    
    $(function() {
		$('.blok-group .box-items').sortable({
			connectWith: '.blok-group .box-items', 
			opacity: 0.8, 
			cursor: 'move',
			<?php if(!$login_obj->UserPrivilege('settings_edit')) echo "disabled: true,"; ?>
			placeholder: 'placeholder-blok', 
			items: 'div:not(.group-header)'
            //stop: create_panels_of_blocks

		}).disableSelection();
	});
    
    //$("#save").click(create_panels_of_blocks);
    $("form").submit(function(){
        create_panels_of_blocks();
        return false;
        });
    
    
    
})

// -->
</script>