<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */

//zabezpeceni
if(!defined('SECURITY_CMS')) exit;
if(!is_action() ||
    (!$this->is_access('presmerovani','edit-content',0,1,'content_edit') &&
     !$this->is_access('presmerovani','new',0,0,'content_add')
    )
    ) return;
    

//inicializace promennych
$idRule = $this->get_id_action();
$zdroj = get_post('zdroj');
$cil = get_post('cil');
$aktivni = get_int_post('aktivni');
            
$idDomeny = $domain->getId(); 

$link_checker = new LinksChecker($idDomeny);
$link_checker->set_base_url($domain->getUrl());


$url = get_url_for_redirect($cil);
$res = $link_checker->check_url($url);

                   		
if($zdroj=='')
    $systemMessage->add_error(TNEPLATNA_ZDROJOVA_URL);			

if($cil=='')
    $systemMessage->add_error(TNEPLATNA_CILOVA_URL);

if($res["code"] >= 400)
    $systemMessage->add_warning(TCILOVA_URL_NEEXISTUJE);
	
if($systemMessage->error_exists()) return;


$update = array(
        "idDomeny"=>$idDomeny,
        "zdroj" => $zdroj,
        "cil" => $cil,
        "aktivni" => $aktivni
        );
			

if($idRule>0)
    $db->update(TABLE_PRESMEROVANI,$update,"id=".$idRule." AND idDomeny=".$idDomeny);
    else
    {
    $db->insert(TABLE_PRESMEROVANI,$update);
    $idRule = $db->lastId();
    }


$log->add_log($this->get_id_action() == 0 ? 'create':'edit-content','presmerovani',$idRule,$zdroj." => ".$cil);
    
$systemMessage->add_ok(OK_ULOZENO);

if($this->get_id_action() == 0)
    $url = $this->get_link('presmerovani',0,'new');
    else
    $url = $this->get_link('presmerovani',0,'edit-content',$idRule);
    
Redirect($url);

?>