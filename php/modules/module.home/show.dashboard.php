<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */


if(!defined('SECURITY_CMS')) exit;
if(!$this->is_access('dashboard')) return;



$data = $db->query("SELECT nastaveniDashboardLevy AS l, nastaveniDashboardPravy AS p FROM ".TABLE_UZIVATELE." WHERE idUzivatele=".$login_obj->getId()." LIMIT 1");
$u = $db->getObject($data);

$levy = array();
$pravy = array();

if($u->l == "" && $u->p == "")
{
    //defaultni nastaveni, pokud uzivatel jeste nemanipuloval s dashboardem
    $data = $db->query("SELECT * FROM ".TABLE_DASHBOARD." WHERE zobrazit=1 ORDER BY poradi");
    while($d = $db->getAssoc($data))
    {
        if($d['umisteni'] == 'l')
        {
            $levy[] = array('nazev' => constant($d['nazev']), 'ajax' => $d['systemovyNazev'], 'id' => $d['idBoxu']);
        }
        if($d['umisteni'] == 'p')
        {
            $pravy[] = array('nazev' => constant($d['nazev']), 'ajax' => $d['systemovyNazev'], 'id' => $d['idBoxu']);
        }
            
    }
}
else
{
    //nacteni nastaveni uzivatele
    $l = explode("#",$u->l);
    $p = explode("#",$u->p);
    $l[] = 0;
    $p[] = 0;
    
    foreach($l AS $idp => $pol)
        if(trim($pol) == "")
            unset($l[$idp]);
            
    foreach($p AS $idp => $pol)
        if(trim($pol) == "")
            unset($p[$idp]);
            
    $levy_z = $pravy_z = array();
    
    $data = $db->query("SELECT *, 
        IF(idBoxu IN (".implode(",",$p)."), 'p', IF(idBoxu IN (".implode(",",$l)."),'l','')) AS umisteni 
        FROM ".TABLE_DASHBOARD." WHERE zobrazit=1");

    while($d = $db->getAssoc($data))
    {
        if($d['modul']!="" && defined($d['modul']) && !constant($d['modul'])) continue;
        
        if($d['umisteni'] == 'l')
        {
            $levy_z[$d['idBoxu']] = array('nazev' => constant($d['nazev']), 'ajax' => $d['systemovyNazev'], 'id' => $d['idBoxu']);
        }
        if($d['umisteni'] == 'p')
        {
            $pravy_z[$d['idBoxu']] = array('nazev' => constant($d['nazev']), 'ajax' => $d['systemovyNazev'], 'id' => $d['idBoxu']);
        }
            
    }
    
    foreach($l AS $pol)
    {
        //if(!isset($pol['id']) || $pol['id'] == 0) continue;
        if(!isset($levy_z[$pol])) continue;
        $levy[] = $levy_z[$pol];
    }
    
    foreach($p AS $pol)
    {
        //if(!isset($pol['id']) || $pol['id'] == 0) continue;
        if(!isset($pravy_z[$pol])) continue;
        $pravy[] = $pravy_z[$pol];
    }
    
}



?>

<div id="dashboard">
<div class="dashboard-left dashboard-panel">
<div class="dashboard-left-in dashboard-in">

<div></div>
<?php

foreach($levy AS $box)
{
    if(intval($box['id']) == 0) continue;
    echo '<div id="'.$box['id'].'" class="dashboard_box">';
    echo '<div class="box_in"><h2>'.$box['nazev'].'</h2><div id="'.$box['ajax'].'" class="box-obsah loader">'.TCEKEJTE_PROSIM.'</div></div>';
    echo '<div class="box_icon hide"><a href="#" title="'.TODEBRAT_BOX_Z_NASTENKY.'">'.icon("delete").'</a></div>';
    echo '</div>';
}

?>
</div>
</div>


<div class="dashboard-right dashboard-panel">
<div class="dashboard-right-in dashboard-in">
<div></div>

<?php

foreach($pravy AS $box)
{
    if(intval($box['id']) == 0) continue;
    echo '<div id="'.$box['id'].'" class="dashboard_box">';
    echo '<div class="box_in"><h2>'.$box['nazev'].'</h2><div id="'.$box['ajax'].'" class="box-obsah loader">'.TCEKEJTE_PROSIM.'</div></div>';
    echo '<div class="box_icon hide"><a href="#" title="'.TODEBRAT_BOX_Z_NASTENKY.'">'.icon("delete").'</a></div>';
    echo '</div>';
}

?>
</div>
</div>

</div>
<div class="cleaner"></div>


<script type="text/javascript">
<!--
$(function() {
		$('#dashboard .dashboard-right-in, #dashboard .dashboard-left-in').sortable({
			connectWith: '.dashboard-panel .dashboard-in', 
			opacity: 0.8, 
			cursor: 'move',
			placeholder: 'placeholder',
            update: function(){
                var cl = $(this).hasClass("dashboard-right-in");
                if(cl) return;
                
                var levy = $("#dashboard .dashboard-left-in").sortable("toArray");
                var pravy = $("#dashboard .dashboard-right-in").sortable("toArray");

                $.post("<?php echo AJAX_GATEWAY?>dashboard.sort",{levy: levy,pravy: pravy});
            }

		}).disableSelection();
        
        
        $("#dashboard .box-obsah").each(function(){
                      
            var id = $(this).attr("id");
            if(id == 'visits')
            {
                box_s1 = $(this);               

                $.getJSON("<?php echo AJAX_GATEWAY."dashboard.graph.visits&od=".date("d.m.Y",strtotime("-15 days"))."&do=".date("d.m.Y",strtotime("-1 day"))."&typ=1"; ?>",null,function(opts){
                    
                    box_s1.removeClass("loader");
                    box_s1.html("");
                    $('#visits').highcharts(opts);
                });
                
                $(this).after("<br /><br /><a class='f-right' href='<?php echo get_link("statistics","navstevnost-webu");?>' title='<?php echo TPREJIT_NA_STATISTIKY;?>'><?php echo TPREJIT_NA_STATISTIKY;?></a><div class='cleaner'></div>");
                
                
            }
            else if(id == 'visits_month')
            {
                
                box_s2 = $(this);    

                $.getJSON("<?php echo AJAX_GATEWAY."dashboard.graph.visits&od=".date("d.m.Y",strtotime("-1 year"))."&do=".date("d.m.Y",strtotime("-1 day"))."&typ=2"; ?>",null,function(opts){
                    
                    box_s2.removeClass("loader");
                    box_s2.html("");
                    $('#visits_month').highcharts(opts);
                });
                
                $(this).after("<br /><br /><a class='f-right' href='<?php echo get_link("statistics","navstevnost-webu");?>' title='<?php echo TPREJIT_NA_STATISTIKY;?>'><?php echo TPREJIT_NA_STATISTIKY;?></a><div class='cleaner'></div>");
                
                
            }
            else
            {
                var ajax = "dashboard." + id;     
                $(this).load("<?php echo AJAX_GATEWAY;?>" + ajax,{},function(){
                    $(this).removeClass("loader");
                });
            }
            
        })
        
        
        $("#dashboard .dashboard_box").hover(
            function(){
                $(this).find(".box_icon").show();
            },
            function(){
                $(this).find(".box_icon").hide();
            }
        
            );
            
            
        $("#dashboard .box_icon a").click(function(){
            var p = $(this).parents(".dashboard_box");
            $.post("<?php echo AJAX_GATEWAY;?>dashboard.delete",{id: p.attr("id")}, function(){
                p.fadeOut(function(){
                    p.remove();   
                });
            })
            
            return false;
        })

        
        
	});

//-->
</script>

