<?php

/**
 * @author Pavel Srom
 * @copyright 2014
 */

include_once("../classes/qp.php");
include_once("../classes/class.keywords.ranking.php");

//maximalni pocet merenych klicovych slov
define("LIMIT_POCET_KLICOVYCH_SLOV", 1);

$data = $db->query("SELECT id, slovo 
    FROM ".TABLE_KLICOVA_SLOVA." 
    ORDER BY aktualizace, id
    LIMIT ".LIMIT_POCET_KLICOVYCH_SLOV
    );
    
/*
$kw = "tvorba webových aplikací opava";
$k = new KeywordsRanking($kw);
echo "pozice: ".$k->get_position_in_google();
exit;
*/
    
$slova = array();

while($s = $db->getAssoc($data))
{
    $slova[] = $s['slovo'];
    $k = new KeywordsRanking($s['slovo']);
    
    $pozice_google = $k->get_position_in_google();
    $pozice_seznam = $k->get_position_in_seznam();  
    
    $insert = array(
        "idSlova" => $s['id'],
        "google"  => $pozice_google,
        "seznam"  => $pozice_seznam,
        "datum"   => "now"
        );
        
    $db->insert(TABLE_KLICOVA_SLOVA_POZICE, $insert);
    $db->update(TABLE_KLICOVA_SLOVA,array("aktualizace" => "now"),"id=".$s['id']);
}

echo implode(", ", $slova);

?>