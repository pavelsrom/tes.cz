<?php

/**
 * @author Pavel Šrom
 * @copyright 2018
 */

//CRON PRO AUTOMATICKE STRHAVANI PLATEB PREDPLATNEHO
//https://www.betterminds.cz/php/cron/cron.web.php?action=aktivuj.provize&zpet=15
error_reporting(E_ALL);


define("POCET_DNI", get_int_get("zpet",15));

//zjisteni klicoveho datumu
$datum = date("Y-m-d 23:59:59", strtotime("- ".POCET_DNI." days"));

//aktivuje provize u vsech objednavek, ktere byly zaplaceny pred vice nez 14 dny
/*
$data = $db->query("UPDATE ".TABLE_PROVIZE." AS p
    LEFT JOIN ".TABLE_ESHOP_OBJEDNAVKY." AS o ON p.id_objednavka = o.id
    LEFT JOIN ".TABLE_UZIVATELE." AS u ON u.idUzivatele = p.id_uzivatel
    SET p.aktivni=1
    WHERE o.id_stav = 2 
        AND o.zaplaceno <= '".$datum."' 
        AND u.aktivni = 1
        
");
*/

$data = $db->query("SELECT o.id, o.id_stav 
    FROM ".TABLE_ESHOP_OBJEDNAVKY." AS o
    LEFT JOIN ".TABLE_UZIVATELE." AS u ON u.idUzivatele = o.id_uzivatel
    WHERE o.zaplaceno <= '".$datum."' 
        AND u.aktivni = 1
    GROUP BY o.id    
");


while($o = $db->getObject($data))
{
    //pokud neni objednavka ve stavu zaplaceno po 15 dnech, pak dojde ke smazani vsech NEAKTIVNICH provizi, ktere cekaji na pripsani
    if($o->id_stav != 2)
    {
        $db->delete(TABLE_PROVIZE,"aktivni=0 AND id_objednavka=".$o->id);
    }
    else
    {
        
        //zjisteni aktivovanych provizi a odeslani upozorneni na schvaleni provize
        $data_provize = $db->query("SELECT p.*, u.email 
            FROM ".TABLE_PROVIZE." AS p
            LEFT JOIN ".TABLE_UZIVATELE." AS u ON u.idUzivatele = p.id_uzivatel 
            WHERE p.id_objednavka=".$o->id." 
                AND p.aktivni=0
                ");
                
        while($p = $db->getObject($data_provize))
        {
            
            $predmet = PREDMET_EMAILU_PROVIZE_SCHVALENA;
            $zprava = TEXT_EMAILU_PROVIZE_SCHVALENA;   
            $zprava = str_replace("[castka]",price($p->castka), $zprava);           
        
            //nacteni html sablony
            $sablona = file_get_contents(PRE_PATH.$domain->getDir()."email.html");
            $zprava = str_replace("{obsah}",$zprava,$sablona);

            //odeslani emailu
            $odeslano = SendMail(SMTP_LOGIN_3, NAZEV_WEBU, $p->email, $predmet, $zprava,'',null, false, 3);
            
        }
        
        
        //aktivace provizi dane objednavky
        $db->update(TABLE_PROVIZE,array("aktivni" => 1),"id_objednavka=".$o->id);
        
        
        
        
        
        
    }
    
    
    
}





?>