<?php

/**
 * @author Pavel Srom
 * @copyright 2014
 */

//UKAZKA URL pro cron: www.mujweb.cz/php/cron/cron.php?action=automatic.payments

define('PRE_PATH','../../');


include_once('../classes/class.db.php');
include_once('../classes/class.domain.web.php');
include_once('../classes/class.url.web.php');

include_once('../functions/functions.admin.php');
include_once('../functions/functions.system.php');
include_once('../functions/functions.error.handlers.php');

include_once('../config/config.db.php');
include_once('../config/config.paths.php');
include_once('../config/config.admin.php');
include_once('../config/config.others.php');

define('LANG',get_request("lang","cz"));

$db = new C_Db();	
$db->Connect();




error_settings();
$domain = new C_Domains();

if($domain->getId() == 0) 
    exit;

loadSettings($domain->getId(),"",false);
loadTranslations(LANG, true);

$links = new C_Url();


loadSettings($domain->getId());
//loadTranslations(CMS_LANG);
GetActiveModules();


$action = get_request("action");



include("cron.".$action.".php");



?>