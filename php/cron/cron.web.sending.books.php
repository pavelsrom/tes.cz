<?php

/**
 * @author Pavel Šrom
 * @copyright 2018
 */

//CRON PRO AUTOMATICKE STRHAVANI PLATEB PREDPLATNEHO

error_reporting(E_ALL);

include(PRE_PATH.$domain->getDir()."funkce.php");

$limit = get_int_get('limit', 30);

$aktualni_mesic = date("m");
$aktualni_mesic = intval($aktualni_mesic);

$aktualni_rok = date("Y");


//1. nacteni dat

$data = $db->query("SELECT k.id, k.hash AS hash_produktu, u.hash AS hash_uzivatele, k.mesic, k.rok, k.id_uzivatel, k.id_objednavka, u.email_knihy AS email, k.pokus
    FROM ".TABLE_ESHOP_ZAKOUPENE_KNIHY." AS k
    LEFT JOIN ".TABLE_ESHOP_OBJEDNAVKY." AS o ON k.id_objednavka = o.id
    LEFT JOIN ".TABLE_UZIVATELE." AS u ON k.id_uzivatel = u.idUzivatele
    WHERE u.aktivni = 1
        AND o.id_stav = 2
        AND k.mesic = ".$aktualni_mesic."
        AND k.rok = ".$aktualni_rok."
        AND k.odeslano IS NULL
        AND k.pokus < 3
    GROUP BY k.id
    ORDER BY k.vlozeno
    LIMIT ".$limit."
    ");
    
    

//2. nacteni dat o knize v danem mesici
$produkt = $db->get(TABLE_STRANKY, array("idStranky AS id", "soubor"), "typ='produkt' AND zobrazit=1 AND rok=".$aktualni_rok." AND mesic=".$aktualni_mesic." AND soubor!='' AND soubor IS NOT NULL");

if($produkt === false)
{
    add_log("cron.sending.books","Produkt ".$aktualni_mesic."/".$aktualni_rok." nenalezen");
    exit;
}

$url_knihy = PRE_PATH.$domain->getDir().USER_DIRNAME_PDF.$produkt->soubor;

//3. odesilani emailu
$pocet_odeslanych = 0;
while($z = $db->getObject($data))
{
    //priprava emailu a nacteni udaju
    $obdobi = $z->mesic."/".$z->rok;
    $email = $z->email;  

    $predmet = PREDMET_EMAILU_KNIHA;
    $predmet = str_replace("[obdobi]",$obdobi, $predmet);
    
    $zprava = TEXT_EMAILU_KNIHA;   
    $zprava = str_replace("[obdobi]",$obdobi, $zprava);
    
    $url_confirm = get_url_download($z->hash_uzivatele, $z->hash_produktu);

    $prvni = strpos($zprava,'{');
    $posledni = strpos($zprava,'}');
    $nazev_odkazu = substr($zprava,$prvni+1,$posledni-$prvni-1);
    $zprava = str_replace('{'.$nazev_odkazu.'}','<a href="'.$url_confirm.'" class="button">'.$nazev_odkazu.'</a>',$zprava);


    //nacteni html sablony
    $sablona = file_get_contents(PRE_PATH.$domain->getDir()."email.html");
    $zprava = str_replace("{obsah}",$zprava,$sablona);
   
    $priloha = array(
        0 => array(
            "file" => $url_knihy,
            "name" => $produkt->soubor
            )
        );
    
    
    //odeslani emailu

    $odeslano = SendMail(SMTP_LOGIN_2, NAZEV_WEBU, $email, $predmet, $zprava,'',$priloha, false, 2);
    
    //zapsani informaci o odeslani
    if($odeslano)
        $update = array(
            "odeslano" => "now",
            "id_produkt" => $produkt->id,
            "pokus" => $z->pokus + 1
            );
        else
        $update = array(
            "id_produkt" => $produkt->id,
            "pokus" => $z->pokus + 1
            );
    
    $db->update(TABLE_ESHOP_ZAKOUPENE_KNIHY, $update, "id=".$z->id);
    
   
    $pocet_odeslanych++;
}

echo $pocet_odeslanych;

if($pocet_odeslanych > 0)
    add_log("cron.sending.books","Obdobi: ".$obdobi.", Odeslano: ".$pocet_odeslanych);



?>