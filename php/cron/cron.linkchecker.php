<?php

/**
 * @author Pavel Srom
 * @copyright 2014
 */

if(!defined('SECURITY_CMS'))
    exit;

define('EMAIL_PRO_ODESLANI_REPORTU_AUTOMATICKE_KONTROLY_ODKAZU',EMAIL_WEBU);

$reset = is_get('reset');
$email = get_request('email', EMAIL_PRO_ODESLANI_REPORTU_AUTOMATICKE_KONTROLY_ODKAZU);
$limit = get_int_get('limit', 100);

define('POCET_ODKAZU_PRO_ZKONTROLOVANI', $limit);


include_once('../classes/class.links.checker.php');
include_once('../classes/class.phpmailer.php');
include_once('../classes/class.table.php');


$chybne_odkazy = new LinksChecker($idDomeny,0, "", "", false, true);

if($reset)
    $db->query("UPDATE ".TABLE_ODKAZY." SET datumKontroly = NULL");

 
$result = $chybne_odkazy->process_cron(POCET_ODKAZU_PRO_ZKONTROLOVANI);

$pocet_chybnych_obrazku = $chybne_odkazy->get_sum_error_images();
$pocet_chybnych_odkazu = $chybne_odkazy->get_sum_error_links();


//odeslani emailu kontroly


$url = $domain->getUrl()."admin/home?page=odkazy"; 

$d = $db->query("SELECT idOdkazu,
    IF(datumKontroly IS NULL OR datumKontroly = '0000-00-00' OR DATE_FORMAT(datumKontroly,'%d%m%Y') != DATE_FORMAT(NOW(),'%d%m%Y'), 1, 0) AS zbyva_zkontrolovat,
    IF(datumKontroly IS NOT NULL AND datumKontroly != '0000-00-00' AND DATE_FORMAT(datumKontroly,'%d%m%Y') = DATE_FORMAT(NOW(),'%d%m%Y'), 1, 0) AS zkontrolovano,
    IF(kod LIKE '4%' OR kod LIKE '5%', 1, 0) chybne
    FROM ".TABLE_ODKAZY." 
    WHERE idDomeny=".$idDomeny);

$odkazu_celkem = 0;
$zbyva_zkontrolovat = 0;
$odkazu_chybnych = 0;
$zkontrolovano = 0;

while($k = $db->getObject($d))
{
    if($k->zbyva_zkontrolovat == 1)
        $zbyva_zkontrolovat++;
        
    if($k->zkontrolovano == 1)
        $zkontrolovano++;
        
    if($k->chybne == 1)
        $odkazu_chybnych++;   
        
    $odkazu_celkem++;
}

//echo "chyby ".$odkazu_chybnych."-";
//echo "zbyva zkontrolovat ".$zbyva_zkontrolovat."<br>";
echo "zkontrolovano: ".$zkontrolovano." / ".$odkazu_celkem."<br />";
if($email == "") {
    echo "neni definovan email - report nebude poslan";
    exit;
    }

if($zbyva_zkontrolovat > 0)
{
    echo "nejsou zkontrolovany jeste vsechny odkazy - report nebude poslan";
    exit;
}

if($odkazu_chybnych == 0)
{
    echo "nenalezen zadny chybny odkaz - report nebude poslan";
    exit;
}

if(!$result)
{
    echo "nebyl nalezen zadny odkaz ke kontrole - report nebude poslan";
    exit;
}

$obsah = "";

$datum = date("d.m.Y, H:i", time());
$obsah .= "<p>Dobrý den,<br />dne ".$datum." byla provedena kontrola odkazů. Počet nalezených chybných odkazů: <b>".$odkazu_chybnych.".</b><br />";
//$obsah .= "<br /><b>".$pocet_chybnych_odkazu." chybných odkazů na stránky</b> a <b>".$pocet_chybnych_obrazku." chybných odkazů na obrázky</b>.";
$obsah .= "<br /> Všechny chybné odkazy si zobrazíte na adrese <a href='".$url."'>".$url."</a></p>";



$data = $db->query("SELECT * 
    FROM ".TABLE_ODKAZY." 
    WHERE (kod LIKE '4%' OR kod LIKE '5%') 
        AND idDomeny=".$idDomeny." 
    GROUP BY CONCAT(idObjektu, typ) 
    ORDER BY typ, datumKontroly
    ");


$celkem = $db->numRows($data);
if($celkem == 0) exit;
 

$i=0;

 
$typy = array(
    'stranka'   => TSTRANKA, 
    'novinka'   => TNOVINKA, 
    'editbox'   => TEDITBOX, 
    'clanek'    => TCLANEK, 
    'galerie'   => TGALERIE,
    'mapa-webu' => TMAPA_WEBU,
    'novinky'   => TNOVINKY,
    'clanky'    => TCLANKY,
    'archiv-clanku'=>TARCHIV_CLANKU,
    '404'       => TCHYBOVA_STRANKA,
    'stranka-galerie'=>TSTRANKA_S_GALERII,
    'galerie-fotka'=>TFOTKA,
    'galerie-slozka'=>TSLOZKA_GALERIE,
    'newsletter'=> TEMAILOVA_KAMPAN,
    'editbox'   => TEDITBOX,
    'akce'      => TAKCE,
    'kalendar-typ' => TTYP_AKCE,
    'tagy'      => TTAGY,
    'kalendar-akci'=>TKALENDAR_AKCI
    );


$style = "background:#797979; padding: 5px; border-bottom: 1px solid black";

$table = new Table();
$table->set_style("font-size: 11px; text-align: left; border-top: 1px solid gray;border-collapse:collapse");
$table->tr_head()
    ->add("Typ","",0,"",0,$style)
    ->add("Název stránky/editboxu/kampaně","",0,"",0,$style)
    ->add("Nalezené chybné odkazy","",0,"",0,$style)
    ->add("Odkaz na web","",0,"",0,$style)
    ->add("Odkaz do administrace","",0,"",0,$style);

    
while($o = $db->getAssoc($data))
{

    $pom = array();
    
    $odkaz_na_web = "";
    $odkaz_do_administrace = "";
     
    $idObjektu = $o['idObjektu'];
    $idJazyka = $o['idJazyka'];
    
    $nazev = "";
    
    if($o['typ'] == 'stranka')
    {
        $s = $db->get(TABLE_STRANKY,array('typ','nazev','idRodice AS rodic'),'idStranky='.$idObjektu);
        $odkaz_na_web = $links->get_url($idObjektu);
        $typ = $s->typ;    
        $typ_nazev = $typy[$typ];
        $nazev = $s->nazev;
        
        switch($typ)
        {
            case 'stranka':
            case 'clanky':
            case 'novinky':
            case 'sitemap':
            case '404':
            case 'archiv-clanku':
            case 'uvodni':
            case 'stranka-galerie':
            case 'vyhledavani': 
                {
                    if((MODULE_PAGES && !in_array($typ,array('clanky','novinky','archiv-clanku'))) || 
                    (MODULE_ARTICLES && in_array($typ, array('clanky','archiv-clanku'))) || 
                    (MODULE_NEWS && in_array($typ,array('novinky'))))
                        $odkaz_do_administrace = get_link('pages','stranky',0,'edit-content',$idObjektu,"",$idJazyka,$domain->getUrl());
                            
                    break;
                }
            case 'galerie':
            case 'galerie-slozka':
                {
                    if(MODULE_GALLERY)
                        $odkaz_do_administrace = get_link('gallery','galerie',0,'edit-settings',$idObjektu,"",$idJazyka,$domain->getUrl());
                    break;
                }
            case 'novinka':
                {
                    if(MODULE_NEWS)
                        $odkaz_do_administrace = get_link('news','novinky',0,'edit-content',$idObjektu,"",$idJazyka,$domain->getUrl());
                    break;
                }
            case 'clanek':
                {
                    if(MODULE_ARTICLES)
                        $odkaz_do_administrace = get_link('articles','rubriky',$s->rodic,'edit-content',$idObjektu,"",$idJazyka,$domain->getUrl());
                    break;
                }
            default: break;
                   
                
                
        }
        
        
    }
    
    if($o['typ'] == 'editbox' && MODULE_EDITBOX)
    {
        $d = $db->query("SELECT nazev, stridani FROM ".TABLE_EDITBOXY." WHERE idEditboxu=".$idObjektu);
        $de = $db->getAssoc($d);
        $nazev = $de['nazev'];
        $typ_nazev = $typy[$o['typ']];
        
        $odkaz_do_administrace = get_link('editboxes','editboxy',0,$de['stridani']=='zadne' ? 'edit-content' : 'edit-content-kolotoc',$idObjektu,"",$idJazyka,$domain->getUrl());
        
    }
    
    if($o['typ'] == 'newsletter' && MODULE_NEWSLETTER)
    {
        $nazev = $db->get(TABLE_EMAILY_KAMPANE,'nazev','idKampane='.$idObjektu);
        $typ_nazev = $typy[$o['typ']];
        
        $odkaz_do_administrace = get_link('newsletters','kampane',0,'edit-settings',$idObjektu,"",$idJazyka,$domain->getUrl());
        
    }
    
    $link = "";
    if($odkaz_na_web != "")
        $link = "<a href='".$domain->getUrl().$links->get_url($idObjektu)."'>web</a>";
       
    
    if($odkaz_do_administrace == "")
        $ikony = "";
    	else	
    	$ikony = "<a href='".$odkaz_do_administrace."'>Upravit</a>";
    	
    
    $data_chybne_odkazy = $db->query("SELECT o.* FROM ".TABLE_ODKAZY." AS o
            WHERE o.idObjektu=".$idObjektu." 
                AND o.typ='".$o['typ']."' 
                AND (kod LIKE '4%' OR kod LIKE '5%')  
            ORDER BY o.perex");
        
        $chybne_odkazy = "";
        $chybne_odkazy_v_perexu = "";
        
        while($l = $db->getAssoc($data_chybne_odkazy))
        {
            if(in_array($l['typSouboru'],array('file','image')))
                $anchor = "[".TSOUBOR_OBRAZEK."]";
                else
                $anchor = $l['text'];
            
            $anchor = "<b>".$anchor."</b>";

            $style = $l['kod'] < 600 && $l['kod'] >= 400 ? "color: red;" : "";
                

            if($l['perex'] == 1)
                $chybne_odkazy_v_perexu .= "<li style='".$style."'>".$anchor." - ".$l['url']."</li>";
                else
                $chybne_odkazy .= "<li style='".$style."'>".$anchor." - ".$l['url']."</li>";
        }
    
    $seznam_odkazu = "";
    if($chybne_odkazy!="")
        $seznam_odkazu = TV_OBSAHU.':<br /><ul>'.$chybne_odkazy.'</ul>';
        
    $seznam_odkazu_v_perexu = "";
    if($chybne_odkazy_v_perexu!="")
        $seznam_odkazu_v_perexu = TV_PEREXU.':<br /><ul>'.$chybne_odkazy_v_perexu.'</ul>';    
    
    $style = "border-bottom:1px solid #DEDEDD; padding: 5px;";
    $table->tr()
        ->add($typ_nazev,"",0,"",0,$style)
        ->add(secureString($nazev),"",0,"",0,$style)
        ->add($seznam_odkazu_v_perexu.$seznam_odkazu,"",0,"",0,$style)
        ->add($link,"",0,"",0,$style)
        ->add($ikony,"",0,"",0,$style);

    
    
}

$obsah .= $table->get_html();

$predmet = "Report: kontrola odkazů systémem Profiwebik - web ".$domain->get('domena')." - den ".$datum;

SendMail(FROM_ADMIN_EMAIL,FROM_ADMIN_NAME,$email,$predmet,$obsah);

echo "report odeslan";

exit;




?>