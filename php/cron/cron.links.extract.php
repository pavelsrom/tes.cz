<?php

/**
 * @author Pavel Srom
 * @copyright 2012
 */


//tento skript je vhodne spoustet, pokud se nasazuje funkcionalita pro kontrolu chybnych odkazu na bezici web
//skript prohled� veskere obsahy stranek, vyextrahuje odkazy a zaradi je do fronty odkazu pro pozdejsi zpracovani.

include_once('../classes/class.links.checker.php');



$db->query("

CREATE TABLE IF NOT EXISTS `odkazy` (
  `idOdkazu` int(11) NOT NULL AUTO_INCREMENT,
  `idDomeny` int(11) NOT NULL DEFAULT '0',
  `idJazyka` int(11) NOT NULL DEFAULT '1',
  `text` text COLLATE utf8_czech_ci NOT NULL,
  `url` text COLLATE utf8_czech_ci NOT NULL,
  `absolutniUrl` text COLLATE utf8_czech_ci NOT NULL,
  `idObjektu` int(11) NOT NULL DEFAULT '0',
  `typ` enum('stranka','akce','novinka','clanek','editbox') COLLATE utf8_czech_ci NOT NULL DEFAULT 'stranka',
  `perex` enum('a','n') COLLATE utf8_czech_ci NOT NULL DEFAULT 'n',
  `kod` int(11) NOT NULL DEFAULT '0',
  `datumKontroly` datetime NOT NULL,
  `typSouboru` enum('html','image','file') COLLATE utf8_czech_ci NOT NULL DEFAULT 'html',
  `typObsahu` varchar(250) COLLATE utf8_czech_ci NOT NULL DEFAULT '',
  `typOdkazu` varchar(10) COLLATE utf8_czech_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`idOdkazu`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

");



if(!defined('TABLE_ODKAZY'))
    define('TABLE_ODKAZY', "odkazy");
    

//extrakce odkazu ze stranek
$data = $db->query("SELECT idStranky AS id, 'stranka' AS typ, perex, obsah, idJazyka AS jazyk FROM ".TABLE_STRANKY." WHERE idDomeny=".$idDomeny);


while($s = $db->getObject($data))
{

        
    $k_obsah = new LinksChecker($idDomeny, $s->id, $s->typ, $s->obsah, false, false);
    $k_obsah->set_lang($s->jazyk);
    $k_obsah->process();
    
    if($s->perex != "")
    {
        $k_perex = new LinksChecker($idDomeny, $s->id, $s->typ, $s->perex, true, false);
        $k_perex->set_lang($s->jazyk);
        $k_perex->process();
    }    
}

//extrakce odkazu z kampani
$data = $db->query("SELECT idKampane AS id, 'newsletter' AS typ, '' AS perex, obsah, idJazyka AS jazyk FROM ".TABLE_EMAILY_KAMPANE." WHERE idDomeny=".$idDomeny);

while($s = $db->getObject($data))
{    
    $k_obsah = new LinksChecker($idDomeny, $s->id, $s->typ, $s->obsah, false, false);
    $k_obsah->set_lang($s->jazyk);
    $k_obsah->process();
  
}

//extrakce odkazu z editboxu
$data = $db->query("SELECT idEditboxu AS id, 'editbox' AS typ, '' AS perex, obsah, idJazyka AS jazyk FROM ".TABLE_EDITBOXY." WHERE idDomeny=".$idDomeny);

while($s = $db->getObject($data))
{
  
        
    $k_obsah = new LinksChecker($idDomeny,$s->id, $s->typ, $s->obsah, false, false);
    $k_obsah->set_lang($s->jazyk);
    $k_obsah->process();
   
}

exit;

?>