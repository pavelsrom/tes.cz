<?php

/**
 * @author Pavel Šrom
 * @copyright 2018
 */

//CRON PRO AUTOMATICKE STRHAVANI PLATEB PREDPLATNEHO

define("POCET_POKUSU_O_STRZENI_PLATBY", 2);

error_reporting(E_ALL);



$limit = get_int_get('limit', 100);

require_once PRE_PATH.'vendor/autoload.php';
include(PRE_PATH."php/classes/class.gopay.php");
include_once(PRE_PATH.'php/classes/class.basket.php');//nakupni kosik
include_once(PRE_PATH.'php/classes/class.subscription.php'); //predplatne
include_once(PRE_PATH.'php/classes/class.provize.php'); //provizni system



$gopay = new C_Gopay();


/*
1. Vyber vsechny zaznamy z tabulky objednavky_polozky, ktere splnuji nasledujici podminky: 
- datum_dalsi_platby <= DNES 
- id_dalsi_objednavky IS NULL - jeste nebyla vytvorena nasledna objednavka
- nebylo provedeno vice nez X pokusu o platbu
- objednavka je ve stavu zaplaceno
- uzivatel je aktivni
- byly aktivovany automaticke platby, tj. stav STARTED
*/




$data = $db->query("SELECT op.id_objednavka, op.id, op.datum_dalsi_platby, op.dalsi_platba_pokus, o.id_uzivatel, TRIM(CONCAT(u.jmeno,' ',u.prijmeni)) AS user_name, u.email, u.hash
    FROM ".TABLE_ESHOP_OBJEDNAVKY_POLOZKY." AS op
    LEFT JOIN ".TABLE_ESHOP_OBJEDNAVKY." AS o ON op.id_objednavka = o.id
    LEFT JOIN ".TABLE_UZIVATELE." AS u ON u.idUzivatele = o.id_uzivatel
    WHERE op.datum_dalsi_platby IS NOT NULL 
        AND op.datum_dalsi_platby <= CURDATE()
        AND op.id_dalsi_objednavky IS NULL 
        AND o.id_stav = 2
        AND u.aktivni = 1
    GROUP BY op.id  
    ORDER BY op.dalsi_platba_pokus    
    LIMIT ".$limit."   
    ");
    


    
/* 
2. V cyklu se prochazeji vsechny nactene polozky
*/


while($p = $db->getObject($data))
{

    
    print_r($p);
    /*
    continue;
    */

    //inicializace zpracovani
    $user_id = $p->id_uzivatel;
    
    //---------------------------------------------------------------------
    //2a. zjisteni id rodicovske platby
    $d = $db->query("SELECT id_platby AS id 
        FROM ".TABLE_ESHOP_OBJEDNAVKY." 
        WHERE id_uzivatel = ".$p->id_uzivatel." 
            AND stav_opakovane_platby = 'STARTED' 
        ORDER BY datum DESC
        LIMIT 1
        ");
    
    $id_rodicovske_platby = 0;
    if($db->numRows($d) > 0)
    {
        $platba = $db->getObject($d);
        $id_rodicovske_platby = $platba->id;
    }
    
    //2b. Pokud id rodicovske platby neexistuje, bude odeslan link na obnoveni platby
    if($id_rodicovske_platby == 0)
    {
        continue;
        
    }

       
    //---------------------------------------------------------------------    
    //2c. vytvoreni nove objednavky a vypocitani noveho predplaceneho obdobi na zaklade datum_dalsi_platby

    $predplatne = new C_Subscription($user_id);
    $kosik = new C_Basket($user_id);
    $provize = new C_Provize($user_id, false); 
    
    $kosik->remove_all_items();  
    $kosik->add_special_item( $predplatne->get_text_next_subscription(), $predplatne->get_price(), $p->id, 1, 'prodlouzeni');
    $id_objednavky = $kosik->create_order(false,true,$p->user_name);
    
    
       
    
    //---------------------------------------------------------------------
    //2d. pokus o strzeni platby

    $response = $gopay->automatic_payment(
        $id_rodicovske_platby,
        $id_objednavky, 
        $kosik->get_order_data(), 
        $kosik->get_order_items_data()
        );

    //zapocitani pokusu o strzeni platby
    $pokus = $p->dalsi_platba_pokus + 1;
    

    //pokud strzeni platby probehlo v poradku
    if ($response->hasSucceed()) {
        
        //nastavi objednavce id platby gopay
        $kosik->set_order_id_payment($id_objednavky, $response->json['id']);
        
        $update = array(
            //"dalsi_platba_pokus" => $pokus, 
            "id_dalsi_objednavky" => $id_objednavky
        );
        
        $db->update(TABLE_ESHOP_OBJEDNAVKY_POLOZKY, $update, "id=".$p->id);
               

    } 
    //pokud strzeni platby neprobehlo
    else 
    {


        //pokud nevysel ani 3 pokus o strzeni platby, pak bude odeslan email s upozornenim registrovanemu uzivateli
        if($pokus >= POCET_POKUSU_O_STRZENI_PLATBY)
        {
            
            //upravi stav objednavky na 4 = automaticke prodlouzeni pozastaveno
            /*
            if(isset($response->errors) && is_array($response->errors))
            {
                $error = $response->errors[0];

                $update = [
                        "stav" => $error->error_name,
                        "stav_opakovane_platby" => $error->error_name,
                        "poznamka_interni" => $error->message,
                        "id_stav" => 4
                    ];
                
                $db->update(TABLE_ESHOP_OBJEDNAVKY, $update, "id=".$p->id_objednavka);

            }
            */
            

            //odesle email s informaci o tom, ze nedoslo k prodlouzeni predplatneho
            $predmet = PREDMET_EMAILU_NEUSPESNA_AUTOMATICKA_PLATBA;
            $zprava = TEXT_EMAILU_NEUSPESNA_AUTOMATICKA_PLATBA;   
            $prvni = strpos($zprava,'{');
            $posledni = strpos($zprava,'}');
            $nazev_odkazu = substr($zprava,$prvni+1,$posledni-$prvni-1);
            $url = PROTOKOL.$_SERVER['HTTP_HOST'].$links->get_url(ID_KOSIK)."?confirm_registrace=".$p->hash;
            $zprava = str_replace('{'.$nazev_odkazu.'}','<a href="'.$url.'" class="button">'.$nazev_odkazu.'</a>',$zprava);

            $sablona = file_get_contents(PRE_PATH.$domain->getDir()."email.html");
            $zprava = str_replace("{obsah}",$zprava,$sablona);
            $email = $p->email;

            $odeslano = SendMail(SMTP_LOGIN_1, NAZEV_WEBU, $email, $predmet, $zprava);

            //deaktivace uzivatele
            $db->update(TABLE_UZIVATELE, array("aktivni" => 0), "hash = '".$p->hash."'");

            //smaze zalozenou novou objednavku
            $kosik->delete_order($id_objednavky);
        }
        else
        {
            $update = array(
                "dalsi_platba_pokus" => $pokus, 
                //"id_dalsi_objednavky" => $id_objednavky
            );
            
            $db->update(TABLE_ESHOP_OBJEDNAVKY_POLOZKY, $update, "id=".$p->id);
        }
        
        
    }
    
    
    
    

    //echo $response;
    
    add_log("gopay - autopayment", $response, $response->statusCode, $user_id);
    

    
}




?>