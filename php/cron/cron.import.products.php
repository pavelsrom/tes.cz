<?php 

error_reporting(E_ALL);

include_once(PRE_PATH.'php/classes/class.import.products.php');//export produktu

$export = new ImportProducts(true);

//nacteni ze souboru, pro testovaci ucely
$file = "test.xml";
$xmlSource = "../../".$file;
$xml = file_get_contents($xmlSource);

//parsovani xml
$export->load($xml);

//ulozeni hodnot z xml
$export->save();

//zapsani do logu
$complete_log = $export->get_log_list(true);

$db->insert(TABLE_LOG_ERROR, array(
    "datum" => "now",
    "modul" => "cron",
    "get_param" => $file,
    "popis" => $complete_log
));

//odeslani emailu v pripade, ze je problem pri importu

SendMail(EMAIL_WEBU, NAZEV_WEBU, "info@profiwebik.cz", "Chyba při importu produktů na eshop", $complete_log);