<?php

/**
 * @author Pavel Srom
 * @copyright 2010
 */


include_once('../classes/class.gapi.php');

//vraci pole s prumernou navstevnosti a celkovou navstevnosti a poctem sledovanych dni
//vstupem jsou data z google analytics, interval definovany od, do, format je RRRRMMDD
function navstevnost(& $data, $od,$do){
    
    $suma = 0;
    $prumer = 0;
    $pocet_dni = 0;
      
    
    foreach($data AS $datum => $navstevy){
        if($datum >= $od && $datum <= $do){
            $suma += $navstevy;
            $pocet_dni++;
            }
        
        }
    
    if($pocet_dni>0)
        $prumer = round($suma / $pocet_dni , 2);
        
    return array('suma'=>$suma, 'pocet'=>$pocet_dni, 'prumer'=>$prumer); 
    
    
    
}

//funkce vraci procentni rozdil
function rozdil_procent($navstevnost_minula, $navstevnost_soucasna, $html=true){
    
    $rozdil = 0;
    
    if($navstevnost_minula > 0)
        $rozdil = round((( 100 / $navstevnost_minula ) * $navstevnost_soucasna) - 100, 2);
        else
        return "";
    
    if($html){
        
        if($rozdil<0)
            $rozdil = "pokles o <span style='color:red'> ".$rozdil." %</span>";
        elseif($rozdil>=0)
            $rozdil = "nárust o <span style='color:green'> +".$rozdil." %</span>";
        else $rozdil = "";       
        }
    
    return $rozdil;
       
}



$vcera = date('d.m.Y', strtotime('-1 day'));

$predevcirem = date('Ymd', strtotime('-2 day'));
$posledniDen = date('Ymd', strtotime('-1 day'));
$predposledniTydenOd = date('Ymd', strtotime('last sunday -2 week +1 day'));
$predposledniTydenDo = date('Ymd', strtotime('last sunday -1 week'));
$posledniTydenOd = date('Ymd', strtotime('last sunday -1 week +1 day'));
$posledniTydenDo = date('Ymd', strtotime('last sunday'));
$posledniMesicOd = date('Ymd', strtotime('-1 month first day'));
$posledniMesicDo = date('Ymd', strtotime('-1 month last day'));
$predposledniMesicOd = date('Ymd', strtotime('-2 month first day'));
$predposledniMesicDo = date('Ymd', strtotime('-2 month last day'));


$start = $predposledniMesicOd = date('Y-m-d', strtotime('-2 month first day'));
$end = $predposledniMesicOd = date('Y-m-d', strtotime('-1 day'));

$data = $db->Query("SELECT d.gaEmail, d.gaHeslo, d.gaProfil, d.domena, d.nazev, s.* 
    FROM ".TABLE_DOMENY." AS d 
    LEFT JOIN ".TABLE_STATISTIKY_NASTAVENI." AS s ON d.idDomeny=s.idDomeny
    WHERE d.gaEmail!='' 
        AND d.gaHeslo!='' 
        AND d.gaProfil!='' 
        AND s.email!='' 
        AND ((s.reportRustu=1 AND s.reportRustuHranice>0) OR (s.reportPoklesu=1 AND s.reportPoklesuHranice>0) OR s.reportDenni=1 OR s.reportTydenni=1 OR s.reportMesicni=1)
        AND d.zobrazit=1 
    ");

if($db->numRows($data)==0) exit;



while($dom = $db->getAssoc($data)){
    try{
    	$ga = new gapi($dom['gaEmail'],$dom['gaHeslo']);
    	}
    	catch(exception $e){
      
        //echo '{"error": "'.$error.'"}';
    	continue;	
    	}
    	
    $idProfile = $dom['gaProfil'];
    $maxRecords = 500;
    
    $filter = null;
    
    //sbírání dat pro pie graf odkud návštěvníci chodí.
    $ga->requestReportData($idProfile,array('date'),array('visits'),null,null,null,$start,$end,1,$maxRecords);
    $res = $ga->getResults();    
    
    $domena = $dom['domena'];
    $web = $dom['nazev'];
    
    //print_r($res);
    
    $v = array();
    foreach($res AS $r)
        $v[$r->dimensions['date']] = $r->metrics['visits'];

    if($dom['reportRustu']==1 && isset($v[$posledniDen])){
        if($dom['reportRustuHranice'] < $v[$posledniDen]){

            $d1 = navstevnost($v,$posledniTydenOd,$posledniTydenDo);
            $d2 = navstevnost($v,$posledniMesicOd,$posledniMesicDo);
           
            $prumerMinulyTyden = $d1['prumer'];
            $prumerMinulyMesic = $d2['prumer'];
            
            $predmet = $domena." - Upozornění na překročení hranice návštěvností webu";
            $zprava = "Dobrý den,<br />
                na webu ".$web." došlo včera dne ".$vcera." k překročení hranice návštěvnosti ".$dom['reportRustuHranice'].". Počet návštěv byl ".$v[$posledniDen].". "; 
                
            if($prumerMinulyTyden>0)
                $zprava .= "To je ".rozdil_procent($prumerMinulyTyden,$v[$posledniDen])." oproti průměrné návštěvnosti minulého týdne (průměr ".$prumerMinulyTyden." návštěv) a ".rozdil_procent($prumerMinulyMesic,$v[$posledniDen])." oproti průměrné návštěvnosti minulého měsíce (průměr ".$prumerMinulyMesic." návštěv).";
                else
                $zprava .= "Návštěvnost minulého týdne byla 0 návštěv.";
                
            SendMail($admin->ADMIN_EMAIL,$admin->FROM_ADMIN_NAME,$dom['email'],$predmet,$zprava,$admin->SMTP,$zprava);
            }
        }
        
    if($dom['reportPoklesu']==1 && isset($v[$posledniDen])){
        if($dom['reportPoklesuHranice'] > $v[$posledniDen]){

            $d1 = navstevnost($v,$posledniTydenOd,$posledniTydenDo);
            $d2 = navstevnost($v,$posledniMesicOd,$posledniMesicDo);
           
            $prumerMinulyTyden = $d1['prumer'];
            $prumerMinulyMesic = $d2['prumer'];
            
            $predmet = $domena." - Upozornění na překročení hranice návštěvností webu";
            $zprava = "Dobrý den,<br />
                na webu ".$web." došlo včera dne ".$vcera." k poklesu návštěvnosti pod hranici ".$dom['reportRustuHranice']." návštěv. Počet návštěv byl ".$v[$posledniDen].". ";
                
            if($prumerMinulyTyden>0)
                $zprava .= "To je ".rozdil_procent($prumerMinulyTyden,$v[$posledniDen])." oproti průměrné návštěvnosti minulého týdne (průměr ".$prumerMinulyTyden." návštěv) a ".rozdil_procent($prumerMinulyMesic,$v[$posledniDen])." oproti průměrné návštěvnosti minulého měsíce (průměr ".$prumerMinulyMesic." návštěv).";
                else
                $zprava .= "Návštěvnost minulého týdne byla 0 návštěv.";
                
            SendMail($admin->ADMIN_EMAIL,$admin->FROM_ADMIN_NAME,$dom['email'],$predmet,$zprava,$admin->SMTP,$zprava);
            
            }
        }
        
    if($dom['reportDenni']==1 && isset($v[$posledniDen])){
            $d1 = navstevnost($v,$posledniTydenOd,$posledniTydenDo);
            $d2 = navstevnost($v,$posledniMesicOd,$posledniMesicDo);
           
            $prumerMinulyTyden = $d1['prumer'];
            $prumerMinulyMesic = $d2['prumer'];
            
            $predmet = $domena." - Denní informace o návštěvnosti";
            $zprava = "Dobrý den,<br />
                na webu ".$web." bylo včera dne ".$vcera." dosaženo ".$v[$posledniDen]." návštěv. ";
            
            if($prumerMinulyTyden>0)
                $zprava .= "To je ".rozdil_procent($prumerMinulyTyden,$v[$posledniDen])." oproti průměrné návštěvnosti minulého týdne (průměr ".$prumerMinulyTyden." návštěv) a ".rozdil_procent($prumerMinulyMesic,$v[$posledniDen])." oproti průměrné návštěvnosti minulého měsíce (průměr ".$d2['prumer']." návštěv).";
                else
                $zprava .= "Návštěvnost minulého týdne byla 0 návštěv.";
                
            SendMail($admin->ADMIN_EMAIL,$admin->FROM_ADMIN_NAME,$dom['email'],$predmet,$zprava,$admin->SMTP,$zprava);
            
        }
        
    if($dom['reportTydenni']==1 && intval(date('N'))==1){
            $d1 = navstevnost($v,$posledniTydenOd,$posledniTydenDo);
            $d2 = navstevnost($v,$posledniMesicOd,$posledniMesicDo);
            $d3 = navstevnost($v,$predposledniTydenOd,$predposledniTydenDo);
            
            $pTydenOd = date('d.m.Y', strtotime('last sunday -1 week +1 day'));
            $pTydenDo = date('d.m.Y', strtotime('last sunday'));
            
            $ppTydenOd = date('d.m.Y', strtotime('last sunday -2 week +1 day'));
            $ppTydenDo = date('d.m.Y', strtotime('last sunday -1 week'));
            
            $prumerMinulyTyden = $d1['prumer'];
            $prumerPredminulyTyden = $d3['prumer'];
            $prumerMinulyMesic = $d2['prumer'];
            
            $predmet = $domena." - Týdenní informace o návštěvnosti";
            $zprava = "Dobrý den,<br />
                na webu ".$web." bylo minulý týden, tj. ve dnech ".$pTydenOd." - ".$pTydenDo." dosaženo ".$d1['suma']." návštěv (průměr ".$prumerMinulyTyden." návštěv). ";
                
            if($prumerPredminulyTyden >0)
                $zprava .= "To je ".rozdil_procent($prumerPredminulyTyden,$prumerMinulyTyden)." oproti průměrné návštěvnosti předminulého týdne (".$ppTydenOd." - ".$ppTydenDo."), kdy bylo dosaženo ".$d3['suma']." návštěv (průměr ".$d3['prumer']." návštěv) a ".rozdil_procent($prumerMinulyMesic,$prumerMinulyTyden)." oproti průměrné návštěvnosti minulého měsíce, která dosahovala ".$d2['prumer']." návštěv.";
                else
                $zprava .= "Návštěvnost předminulého týdne (".$ppTydenOd." - ".$ppTydenDo.") byla 0 návštěv.";
                
            SendMail($admin->ADMIN_EMAIL,$admin->FROM_ADMIN_NAME,$dom['email'],$predmet,$zprava,$admin->SMTP,$zprava);
            
        }    
        
    if($dom['reportMesicni']==1 && intval(date('d'))==1){
            $d1 = navstevnost($v,$posledniMesicOd,$posledniMesicDo);
            $d2 = navstevnost($v,$predposledniMesicOd,$predposledniMesicDo);

            $prumerMinulyMesic = $d1['prumer'];
            $prumerPredminulyMesic = $d2['prumer'];
            
            $pMesicOd = date('d.m.Y', strtotime('-1 month first day'));
            $pMesicDo = date('d.m.Y', strtotime('-1 month last day'));
            $ppMesicOd = date('d.m.Y', strtotime('-2 month first day'));
            $ppMesicDo = date('d.m.Y', strtotime('-2 month last day'));
            
            $predmet = $domena." - Měsíční informace o návštěvnosti";
            $zprava = "Dobrý den,<br />
                na webu ".$web." bylo minulý měsíc, tj. ve dnech ".$pMesicOd." - ".$pMesicDo." dosaženo ".$d1['suma']." návštěv (průměr ".$d1['prumer']." návštěv). ";
                
            if($prumerPredminulyMesic>0)
                $zprava .= "To je ".rozdil_procent($prumerPredminulyMesic,$prumerMinulyMesic)." oproti průměrné návštěvnosti předminulého měsíce (".$ppMesicOd." - ".$ppMesicDo."), kdy bylo dosaženo celkového počtu ".$d2['suma']." návštěv a průměru ".$d2['prumer']." návštěv.";
                else
                $zprava .= "Předminulý měsíc (".$ppMesicOd." - ".$ppMesicDo.") bylo dosaženo 0 návštěv.";
                
            SendMail($admin->ADMIN_EMAIL,$admin->FROM_ADMIN_NAME,$dom['email'],$predmet,$zprava,$admin->SMTP,$zprava);
            
        }     
        
    
    }
    

?>