<?php

/**
 * @author Pavel Srom
 * @copyright 2010
 */

DEFINE('PRE_PATH','../../');
define('DOMAIN_ID', 1);


//nacteni konfigurace
include_once('../config/config.paths.php');
include_once('../config/config.web.php');
include_once('../config/config.db.php');
include_once('../config/config.others.php');

//nacteni knihoven
include_once('../functions/functions.system.php');
include_once('../functions/functions.admin.php');
include_once('../functions/functions.input.formats.php');
include_once('../functions/functions.error.handlers.php');

include_once('../classes/class.db.php');
include_once('../classes/class.domain.web.php');

$db = new C_Db();	
$db->Connect();
    
$domain = new C_Domains();
loadSettings(DOMAIN_ID);

//nacteni vyrazu filtru
$filter_terms = explode(PHP_EOL,RSS_VYRAZY);




function filter($text, $filter_terms)
    {
        global $db;
        
        $bool = false;
        
        $interpunkce = array(',','.','!','?','\"','\'','%','$','@',':');
        $text = strip_tags($text);
        
        if(!function_exists('mb_split')) return array('bad' => $bool, 'text' => $text);
        
        
        mb_internal_encoding("UTF-8");
        if(count($filter_terms) == 0) return array('bad' => $bool, 'text' => $text);
        
       
        $slova = explode(' ', $text);
       
        if(count($slova)==0) return array('bad' => false, 'text' => $text);

        

        foreach($slova AS $ids => $s)
            foreach($filter_terms AS $idv => $v){
                $testovane_slovo = implode('', mb_split('/[\\W]/u', trim($s)));
                
                $testovane_slovo = str_replace(
                    array('.',',','?','!','\'','\"','[',']','{','}','(',')',':','@','#','$','%','^','&','*','+','-','_' ),
                    array('','','','','','','','','','','','','','','','','','','','','','',''),
                    $testovane_slovo
                    );
                
                $testovane_slovo = str_replace(array('Ě','Š','Č','Ř','Ž','Ý','Á','Í','É','Ú','Ů','Ď','Ť','Ň','Ó'),
                    array('ě',"š","č","ř","ž","ý","á","í","é","ú","ů","ď","ť","ň"), $testovane_slovo); 

                $vyraz_filtru = "/^[\\W\\d]*".str_replace('*','[\\wĚŠČŘŽÝÁÍÉÚŮĎŤŇÓěščřžýáíéúůďťňó]*',$v)."[\\W\\d]*$/iu";

                if(preg_match($vyraz_filtru,$testovane_slovo))
                    return true;
                
                }
        
        return false;
        
        
    }


function strtodate_x($date_string)
{
   if(trim($date_string) == "") return ""; 
   if( ($comma_pos = strpos($date_string, ',')) !== FALSE )
      $date_string = substr($date_string, $comma_pos + 1);
   return date("Y-m-d H:i", strtotime($date_string));
}

$db->Query("TRUNCATE ".TABLE_RSS);

$urls = explode(PHP_EOL,RSS_URL);
$data = array();
foreach($urls AS $u)
{
    $ud = explode(";",$u);
    $data[] = array(
        "adresa" => $ud[0],
        "aplikovatFiltr" => $ud[1], 
        "url" => $ud[2]
        );
        
        
}



foreach($data AS $d)
{
    
    $url = $d['url'];
    $adresa = $d['adresa'];
    $aplikovatFiltr = $d['aplikovatFiltr'];
    
    $xml_content = getRemoteFileCurl($url);
     
    $xml = new DOMDocument();
    $xml->loadXML ($xml_content);
    $xml->xinclude();
    $xml = simplexml_import_dom($xml);
    if(!$xml) continue;
    if(!isset($xml->channel->item) || count($xml->channel->item)==0) continue;
    
    foreach($xml->channel->item AS $item)
    {
        
        $ns_dc = $item->children('http://purl.org/dc/elements/1.1/');
        $datum = isset($ns_dc->date) ? DateValue($ns_dc->date, true) : "";
        $datum = isset($item->pubDate) ? strtodate_x($item->pubDate) : $datum;
        
        $titulek = isset($item->title) ? $db->secureString(strip_tags($item->title)) : "";
        $popis = isset($item->description) ? $db->secureString(strip_tags($item->description)) : "";
        
        
        if($aplikovatFiltr == 1 && !filter($titulek, $filter_terms) && !filter($popis, $filter_terms)) continue;
        
        
        
        
        
        $url = isset($item->link) ? $db->secureString($item->link) : "";
        
        if($titulek=='' || $url=='') continue;
        
        $db->Query("INSERT INTO ".TABLE_RSS." (titulek,perex,url,datum,adresa) VALUES ('".$titulek."', '".$popis."', '".$url."', '".$datum."', '".$adresa."')");
    }
   
    
    
}

?>