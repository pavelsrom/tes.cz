<?php

/**
 * @author Pavel Srom
 * @copyright 2010
 */


class C_Gallery{
	

	//obsahuje polozky galerie
	public $items;
	
	//indikuje existenci galerie s danym id
	public $exists;
	    
	public function C_Gallery($idGallery = 0, $typ='stranka'){
		global $domain;
		global $db;
		global $links;
        global $page;
		global $private_pages;
        global $pagination;
        
		$this->items = array();
		$this->exists = false;
        
		$idGallery = intval($idGallery);
		              
        if($typ == 'stranka')
            $limit = (($pagination->get_current_page() - 1) * intval(POCET_FOTEK_V_JEDNOM_RADKU) * intval(POCET_RADKU_NA_STRANKU)).",".(intval(POCET_FOTEK_V_JEDNOM_RADKU) * intval(POCET_RADKU_NA_STRANKU));
        elseif($typ == 'panel')
            $limit = POCET_FOTEK_V_BOXU;
        else return;
        
        
        if($idGallery > 0)
            $cond = "AND gp.idRodice=".$idGallery." AND gp.typ = 'galerie-fotka'";
            else{
                
                $idRodice = intval($page->get_id());
                
                if($page->get_type() == 'galerie-fotka' && $typ=='stranka')
                {
                    
                //zjisteni rodice aktualniho obrazku
                $d = $db->Query("SELECT idRodice FROM ".TABLE_STRANKY." WHERE idDomeny=".$domain->getId()." AND idStranky=".$idRodice." AND zobrazit=1 LIMIT 1");
                
                if($db->numRows($d) > 0)
                    {
                        $rd = $db->getAssoc($d);
                        $idRodice = $rd['idRodice'];
                        $limit = "0,100";
                    }
                                    
                }
                
                
                if($typ=='panel')
                    $cond = "AND gp.typ IN ('galerie-fotka')";
                    else
                    $cond = "AND gp.idRodice=".$idRodice." AND gp.typ IN ('galerie-slozka','galerie','galerie-fotka')";
            
            }
        
        
        //zjisteni privatnich stranek
        if(MODULE_PRIVATE_PAGES)
            {
                
                
                $d = $db->query("SELECT idStranky AS id
                    FROM ".TABLE_STRANKY." AS gp
                    WHERE zobrazit=1 
                        AND gp.idDomeny=".$domain->getId()."
                        AND gp.idJazyka=".WEB_LANG_ID."
                        AND gp.zobrazit=1 
                        ".$cond."
                        ");
                        
                $zakazane_stranky_id = array();
                while($s = $db->getObject($d))
                    if(!$private_pages->has_access($s->id))
                        $zakazane_stranky_id[] = $s->id; 
                
                $db->free($d);
                        
                $cond .= count($zakazane_stranky_id) > 0 ? " AND gp.idStranky NOT IN (".implode(",",$zakazane_stranky_id).")" : ""; 
                
                        
            }
        
        
        
		$data = $db->Query("SELECT SQL_CALC_FOUND_ROWS gp.idStranky, gp.nadpis AS nazev, gp.obrazek AS soubor, gp.obsah AS popis, IF(gp.datum IS NULL,'',DATE_FORMAT(gp.datum, '%d.%m.%Y')) AS datum, gp.typ, gp.idRodice AS rodic,
        IF(gp.typ = 'galerie', 
            (SELECT sl.obrazek FROM ".TABLE_STRANKY." AS sl 
                WHERE gp.idStranky=sl.idRodice 
                    AND sl.typ='galerie-fotka' 
                    AND sl.zobrazit=1
                ORDER BY priorita
                LIMIT 1
            ),
            '') AS obrazek_pro_slozku
            
			FROM ".TABLE_STRANKY." AS gp
            LEFT JOIN ".TABLE_STRANKY." AS sl ON gp.idStranky=sl.idRodice AND sl.typ='galerie-fotka' AND sl.zobrazit=1 
			WHERE gp.idDomeny=".$domain->getId()."
                AND gp.zobrazit=1 
                ".$cond."
            GROUP BY gp.idStranky    
			ORDER BY gp.priorita, gp.idStranky DESC
            LIMIT ".$limit."
			");
		
        $celkem_data = $db->Query("SELECT FOUND_ROWS()");
        $celkem_arr = $db->getRow($celkem_data);

		$pagination->set_number_items( intval($celkem_arr[0]));
       
		$this->exists = $db->numRows($data)>0; 

        $i = 0;
		while($gal = $db->getAssoc($data)){
			$this->items[$i] = array(
                'id' => $gal['idStranky'],
				'nazev'=>$gal['nazev'],
				'popis'=>$gal['popis'],
                'rodic'=>$gal['rodic'],
                'typ'=>$gal['typ'],
				'obrazek_mini'=>RELATIVE_URL_USER_DOMAIN.USER_DIRNAME_GALLERY_MINI.$gal['soubor'],
                'obrazek_stredni'=>RELATIVE_URL_USER_DOMAIN.USER_DIRNAME_GALLERY_STREDNI.$gal['soubor'],
                'obrazek_maxi'=>RELATIVE_URL_USER_DOMAIN.USER_DIRNAME_GALLERY_MAXI.$gal['soubor'],
				'datum'=>$gal['datum'],
                'obrazek_slozka' => ($gal['obrazek_pro_slozku']!='' ? $domain->getDir().USER_DIRNAME_GALLERY_MINI.$gal['obrazek_pro_slozku'] : '')
				); 
            $i++;
			}

	}
	
    private function getFolders()
    {
        global $links;
        global $domain;
        global $pagination;
        
        $cols = intval(POCET_FOTEK_V_JEDNOM_RADKU);
        $rows = intval(POCET_RADKU_NA_STRANKU);
        
		$pocet = $cols * $rows;
        $pagination->set_number_items_on_page($pocet);
               
		$stranka = $pagination->get_current_page();
		$od = ($stranka-1) * $pocet;
        
        $html = '';
			
		$html .= "<span id='galerie'></span>"; 
			

		$html .= "<table class='galerie'>\n";
        
        $i=1;
		$pocet_fotek=0;
		$k=0;
        
        foreach($this->items AS $img){
            $id = $img['id'];
			if($k<$od) {
				$k++;
				continue;
				} 
			
            	
			if($pocet==$pocet_fotek) break;

			if($i==1) {
			     $popisky = array();
			     $html .= "<tr>\n";
                 }
			
			$html .= "<td>\n";
			
            if($img['typ'] == 'galerie')
            {
                $html .= '<a class="slozka slozka-galerie" style="background: url('.$img['obrazek_slozka'].') top left no-repeat;" href="'.$links->get_url($id).'"><strong>'.$img['nazev'].'</strong><span></span></a>';
            }
            else
            {
			$html .= "<a href='".$links->get_url($id)."' class='slozka'>";
			//$html .= "<img src='".$domain->getDir()."images/folder.png"."' alt='".$img['nazev']."' class='folder'/>";
            /*
            if($img['obrazek_slozka']!='')
                $html .= "<img src='".$img['obrazek_slozka']."' alt='".$img['nazev']."' class='folder-img'/>";
            */
            
                $html .= '<strong>'.$img['nazev'].'</strong>';
                
            $html .= "</a>\n";
            }
            
            $popis = "";
            $popis .= "<td class='info'>";
	        $popis .= "<a href='".$links->get_url($id)."'>".$img['nazev']."</a>";
            	
            $popis .= "</td>\n";
            
            $popisky[] = $popis;
            
            
			
			$html .= "</td>\n";
			
			if(($i%$cols)==0) {
				$html .= "</tr>\n";
                
                //$html .= "<tr>".implode('',$popisky)."</tr>";
                
				$i=1;
				}
				else
				$i++;
			
			$pocet_fotek++;
			}
			
		$i--;
		if($i!=0){
			$i = $cols - $i;	
		}
	
		if($i>0){
		      $html_konec = "";
		    for($j=0;$j<$i;$j++)
	           $html_konec .= "<td></td>\n";
			
			$html .= $html_konec."</tr>\n";
            
            //$html .= "<tr>".implode('',$popisky).$html_konec."</tr>";
            
			}
		
		$html .= "</table>\n";	
		
		
		//strankovani		
		$html .= $pagination->set_url_tail("#galerie")->get_html();
        
        return $html;
        
    }
    
    private function GetGallery()
    {
        global $links;
        global $domain;
        global $pagination;
        
        $cols = intval(POCET_FOTEK_V_JEDNOM_RADKU);
        $rows = intval(POCET_RADKU_NA_STRANKU);
        
		$pocet = $cols * $rows;
        $pagination->set_number_items_on_page($pocet);
        
               
		$stranka = $pagination->get_current_page();
		$od = ($stranka-1) * $pocet;
        
        $html = '';
			
		$html .= "<span id='galerie'></span>"; 
			
		
        $i=1;
		$pocet_fotek=0;
		$k=0;
        
        foreach($this->items AS $img){
            $id = $img['id'];
            
            $n = explode("-",$img['nazev']);
            $n0 = isset($n[0]) ? '<strong>'.trim($n[0]).'</strong>' : ''; 
            $n1 = isset($n[1]) ? ' <em>'.trim($n[1]).'</em>' : ''; 
            
            $html .= '<div class="col6 fotka"><img src="'.$img['obrazek_mini'].'" alt="">
            <div class="imgPopisovac">
              <h3>'.$n0.$n1.'</h3><a href="'.get_share_link($img['nazev'],PROTOKOL.$_SERVER['HTTP_HOST'].$img['obrazek_maxi']).'" class="fb-share">'.TSDILET.'</a><a href="'.$img['obrazek_maxi'].'" data-lightbox="roadtrip" data-title="'.strip_tags($img['popis']).'">'.TDETAIL.'</a>
            </div>
          </div>';
			
			
            
		}
		
	
		
		
		//strankovani		
		$html .= $pagination->set_url_tail("#galerie")->get_html();
	
        
        return $html;
    }
    
    private function getImage()
    {
        global $links;
        global $domain;
        global $page;
        
        $idStranky = intval($page->get_id());

        
        $html = "";
   
        $html .= "<div id='gDetail'>";
        
        
        foreach($this->items AS $i => $o)
        {
            $id = $o['id'];
            if($id == $idStranky)
            {
                $rozmery = getimagesize($o['obrazek_maxi']);
        
                $html .= "<table class='tDetail'>";       
                $html .= "<tr><td>
                    <div  style='position: relative;'>
                    <img src='".$o['obrazek_stredni']."' alt='".$o['nazev']."' title='".$o['nazev']."'/>";
                
                $predchozi_id = 0;
                if(isset($this->items[$i - 1]))
                    $predchozi_id = $this->items[$i - 1]['id']; 
                
                $dalsi_id = 0;
                if(isset($this->items[$i + 1]))
                    $dalsi_id = $this->items[$i + 1]['id']; 
                
                
                
                $url_predchozi = "";
                if($predchozi_id>0)
                    $url_predchozi = $links->get_url($predchozi_id);
                
                $url_dalsi = "";
                if($dalsi_id > 0)
                    $url_dalsi = $links->get_url($dalsi_id);
                    
                $html .= "<div id='gToolbar'>
                    <div class='gToolbarIn'>";
                    
                if($url_predchozi != '')
                    $html .= "<a href='".$url_predchozi."#gDetail' title='Zpet' class='gZpet'></a>";
                    else
                    $html .= "<span class='gZpet'></span>";
                    
                $html .= "<a href='".$o['obrazek_maxi']."' title='<b>".$o['nazev']."</b><br />".$o['popis']."' class='gZvetsit fotka'></a>";
                
                if($url_dalsi != '')
                    $html .= "<a href='".$url_dalsi."#gDetail' title='Vpred' class='gVpred'></a>";
                    else
                    $html .= "<span class='gVpred'></span>";
                    
                $html .= "</div></div>";
                    
                $html .= "</div></td></tr>";
                $html .= "<tr class='info'><td>
                    Rozměry: ".$rozmery[0]." x ".$rozmery[1]." px | 
                    Velikost: ".floor(filesize($o['obrazek_maxi'])/1024)." kB |
                    <a href='php/scripts/download_image.php?id=".$idStranky."' title='".$o['nazev']."'>".TDOWNLOAD."</a>
                    </td></tr>";
                $html .= "<tr><td>".$o['popis']."</td></tr>";
                $html .= "</table>";     
            }
            else
            {
                $html .= "<a href='".$o['obrazek_maxi']."' title='<b>".$o['nazev']."</b><br />".$o['popis']."' class='fotka' style='display: none;'></a>";
            }
        }
        
        
        $html .= "</div>";
        
        return $html;
    }
	
	public function GetGalleryPage(){
		
		global $domain;
        global $page;
		

		$html = '';

        if($page->get_type() == 'galerie-slozka' || $page->get_type() == 'stranka-galerie')
            $html .= $this->getFolders();
        elseif($page->get_type() == 'galerie')
            $html .= $this->getGallery();
        elseif($page->get_type() == 'galerie-fotka')
            $html .= $this->getImage();    
        else
        {
            $html .= $this->getGallery();

        }
            
		return $html;
		
	}
	
	
	
	
	
	
	//vrati blok galerie do panelu
	public function GetGalleryBlock($cols=1){
		
		global $domain;
        global $links;
		
		$pocet = intval(POCET_FOTEK_V_BOXU);
		
		if(count($this->items) == 0) return '';
		
		if($pocet > count($this->items)) $pocet = count($this->items);
		
		if($pocet == 0) return '';
		
		$rand = array_rand($this->items, $pocet);

			
		$html = "<h4>".TPOSLEDNI_FOTOGRAFIE."</h4>";
        $html .= '<div class="posledni-fotka">';
        
		$mini = "";
		
			
		if(!is_array($rand)) $rand = array($rand);
			
		//$html .= "<table>\n";
		
        
        
		$i=1;
		foreach($rand AS $id)
        {
			//if($i==1) $html .= "<tr>\n";
            
            $odkaz_na_galerii = $links->get_url($this->items[$id]['rodic']);
            $nazev_galerie = $db->get(TABLE_STRANKY,'nazev','idStranky='.$this->items[$id]['rodic']);


			$mini = $this->items[$id]['obrazek_stredni'];
			
			//$html .= "<td>";
            $html .= '<span class="datum">'.$this->items[$id]['datum'].'</span>';
            $html .= '<p><strong>'.$nazev_galerie.'</strong></p>';
            
            $html .= '<div class="obrazek">
                            <a href="'.$odkaz_na_galerii.'">
                                <img src="'.$mini.'" alt="'.$this->items[$id]['nazev'].'"/>
                            </a>
                        </div>';
            
            
            /*
			$html .= "</td>\n";
			if(($i%$cols)==0) {
				$html .= "</tr>\n";
				$i=1;
				}
				else
				$i++;
			
			}
            */
			
    		$i--;
    		if($i!=0){
    			$i = $cols - $i;	
    		}
	   
        /*
		if($i>0){
			for($j=1; $j<=$i; $j++){
				$html .= "<td></td>\n";
				}
			$html .= "</tr>\n";
			}
		*/	
        
        }
		$html .= "</div>\n";
		
		if($odkaz_na_galerii!='')
			$html .= "<div class='tlacitko-box'><a href='".$odkaz_na_galerii."' class='tlacitko'>".TZOBRAZIT_CELOU_GALERII."</a></div>";

		return $html;
		
	
	}
    
	
	
}


?>