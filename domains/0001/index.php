<?php

/**
 * @author Pavel Srom
 * @copyright 2013
 */

/*
ini_set("display_errors",1);
error_reporting(E_ALL);
*/


include_once($domain->getDir().'php/class.web.tree.php');

include_once('config.php');
include_once('php/classes/class.editbox.php');
include_once($domain->getDir().'php/class.news.php');



$h1 = $page->get_h1();
$perex = $page->get_perex();
$content = $page->get_content();
$gallery = $page->get_photos();
//$special_content = $page->get_special_content();

//$tree = new C_WebTree();
//$menu = $tree->ShowMenu(0, 'first-level');

$layout->set_default_path();
$layout->add_js("https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.min.js");
$layout->add_js("https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine-ie11.min.js");

$layout->add_js("https://unpkg.com/swiper@7/swiper-bundle.min.js");
$layout->add_js("fslightbox.js");
$layout->add_js("app.js");


$favicon = RELATIVE_URL_USER_DOMAIN."img/favicon.png";
$layout->set_favicon($favicon);

$layout->add_css_to_head("https://unpkg.com/swiper@7/swiper-bundle.min.css");
$layout->add_css_to_head("main.css?2");

//include("header.php");
include("uvod.php");
//include("footer.php");

exit;


?>