<?php 



echo $layout->get_header();

?>
<body>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=<?= GA_KOD ?>"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->



		<header x-data="{ menuOpen: false }">
			<div class="container">
				<div class="logo">
					<img src="<?= RELATIVE_URL_USER_DOMAIN ?>img/logo.svg" alt="" />
				</div>
				<div class="navigation">
					<button class="open-nav" @click="menuOpen = true">
						<svg viewBox="0 -53 384 384" xmlns="http://www.w3.org/2000/svg">
							<path
								d="m368 154.667969h-352c-8.832031 0-16-7.167969-16-16s7.167969-16 16-16h352c8.832031 0 16 7.167969 16 16s-7.167969 16-16 16zm0 0"
							></path>
							<path
								d="m368 32h-352c-8.832031 0-16-7.167969-16-16s7.167969-16 16-16h352c8.832031 0 16 7.167969 16 16s-7.167969 16-16 16zm0 0"
							></path>
							<path
								d="m368 277.332031h-352c-8.832031 0-16-7.167969-16-16s7.167969-16 16-16h352c8.832031 0 16 7.167969 16 16s-7.167969 16-16 16zm0 0"
							></path>
						</svg>
					</button>
					<nav x-show="menuOpen">
						<button class="close-nav" @click="menuOpen = false">
							<svg
								viewBox="0 0 311 311.07733"
								xmlns="http://www.w3.org/2000/svg"
							>
								<path
									d="m16.035156 311.078125c-4.097656 0-8.195312-1.558594-11.308594-4.695313-6.25-6.25-6.25-16.382812 0-22.632812l279.0625-279.0625c6.25-6.25 16.382813-6.25 22.632813 0s6.25 16.382812 0 22.636719l-279.058594 279.058593c-3.136719 3.117188-7.234375 4.695313-11.328125 4.695313zm0 0"
								></path>
								<path
									d="m295.117188 311.078125c-4.097657 0-8.191407-1.558594-11.308594-4.695313l-279.082032-279.058593c-6.25-6.253907-6.25-16.386719 0-22.636719s16.382813-6.25 22.636719 0l279.058594 279.0625c6.25 6.25 6.25 16.382812 0 22.632812-3.136719 3.117188-7.230469 4.695313-11.304687 4.695313zm0 0"
								></path>
							</svg>
						</button>
						<ul>

						<?php 


						$data = $db->query("SELECT s.nadpis, d.domena, s.idDomeny AS id
							FROM ".TABLE_STRANKY." AS s
							LEFT JOIN ".TABLE_DOMENY." AS d ON s.idDomeny = d.idDomeny
							WHERE s.typ = 'uvodni'
								AND s.idJazyka = ".WEB_LANG_ID."
								AND d.zobrazit = 1
							GROUP BY s.idStranky
							ORDER BY d.priorita
							");

						while($s = $db->getObject($data))
						{
							echo '<li>';
							if($domain->getId() == $s->id)
								echo '<span>'.$s->nadpis.'</span>';
								else
								echo '<a href="https://www.'.$s->domena.'/'.(LANG == 'cz' ? '' : LANG.'/').'">'.$s->nadpis.'</a>';
								
							echo '</li>';
						}

						?>

						</ul>
					</nav>
				</div>
				<div class="languages">
					<?= getLangs() ?>
				</div>
			</div>
		</header>
		<div class="swiper">
			<div class="swiper-wrapper">
				<?php 

				$slider_id = $db->get(TABLE_EDITBOXY, "idEditboxu", "idDomeny=".$domain->getId()." AND stridani = 'samorotujici'");
				$slider = new C_Editbox($slider_id);
				$slider_items = $slider->get_items();

				foreach($slider_items AS $item)
				{
					echo '<div class="swiper-slide">
							<img src="'.RELATIVE_PATH."domains/000".$domain->getId()."/".USER_DIRNAME_EDITBOXES_MAXI.$item['obrazek'].'" alt="" />
						</div>';

				}


				?>

			</div>
			<div class="swiper-pagination"></div>
		</div>
		<div class="headline">
			<?= $h1 ?>
		</div>

		<?php 

			$news = new C_News();
			echo $news->GetNewsOnStartPage();


		?>


		
		<section class="references">
			<div class="container">
				<div class="center">
					<?= editable('TREFERENCE', TREFERENCE, 'preklad', true, 'h2') ?>
				</div>

				<ul class="gallery">
					<?php 

					foreach($gallery AS $g)
					{
						echo '<li>
								<a data-fslightbox href="'.RELATIVE_PATH."domains/000".$domain->getId()."/".USER_DIRNAME_STRANKY_MAXI.$g['soubor'].'">
									<img src="'.RELATIVE_PATH."domains/000".$domain->getId()."/".USER_DIRNAME_STRANKY_MINI.$g['soubor'].'" alt="" />
								</a>
								<p>'.ss($g['popis']).'</p>
							</li>';
					}

					?>

					
				</ul>
			</div>
		</section>
		<section class="studies">
			<div class="container">
				<div class="center">
					<?= editable('TPRIPADOVE_STUDIE', TPRIPADOVE_STUDIE, 'preklad', true, 'h2') ?>
				</div>
				<ul class="videos">

				<?php 

				$odkazy = $page->get_links();
				foreach($odkazy AS $o)
				{
					$cl = count($odkazy) == 1 ? 'full-video' : '';
					echo '<li class="'.$cl.'">
							<div class="video-container">
								<iframe
									width="560"
									height="315"
									src="'.$o['url'].'"
									title="YouTube video player"
									frameborder="0"
									allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
									allowfullscreen
								></iframe>
							</div>
							<p>'.ss($o['popis']).'</p>
						</li>';
				}


				?>
					
				</ul>
			</div>
		</section>
		<section class="contact">
			<div class="contact-text">
				<?= $content ?>
			</div>
			<div class="contact-map">
				<img src="<?= RELATIVE_URL_USER_DOMAIN ?>img/mapa.jpg" alt="" />
			</div>
		</section>
		<?= $layout->get_footer() ?>
	</body>
</html>

