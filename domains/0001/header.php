<?php 



echo $layout->get_header();

?>
    	<body class="<?= $page->is_home() ? 'homepage' : '' ?>">
		<header>
			<div class="top-line">
				<div class="container">
					<div class="top-info">
						<a href="tel:<?= str_replace(" ", "", TELEFON_WEBU)?>"><?= TELEFON_WEBU ?></a>
						<span>|</span>
						<a href="mailto:<?= EMAIL_WEBU ?>"><?= EMAIL_WEBU ?></a>
						<span>|</span>
						<a href="https://www.facebook.com/Bornitflexi/" target="_blank">
							<svg
								aria-hidden="true"
								focusable="false"
								data-prefix="fab"
								data-icon="facebook-square"
								class="svg-inline--fa fa-facebook-square fa-w-14"
								role="img"
								xmlns="http://www.w3.org/2000/svg"
								viewBox="0 0 448 512"
							>
								<path
									d="M400 32H48A48 48 0 0 0 0 80v352a48 48 0 0 0 48 48h137.25V327.69h-63V256h63v-54.64c0-62.15 37-96.48 93.67-96.48 27.14 0 55.52 4.84 55.52 4.84v61h-31.27c-30.81 0-40.42 19.12-40.42 38.73V256h68.78l-11 71.69h-57.78V480H400a48 48 0 0 0 48-48V80a48 48 0 0 0-48-48z"
								></path>
							</svg>
						</a>
					</div>
					<div class="languages">
						<?= getLangs() ?>
					</div>
				</div>
			</div>
			<div class="main-header" x-data="{ menuOpen: false }">
				<div class="container">
					<div class="logo">
					<?php 

					if(!$page->is_home())
						echo '<a href="'.RELATIVE_PATH.'">';

					?>
					<img src="<?= RELATIVE_URL_USER_DOMAIN ?>img/logo.png" alt="" />
					<?php 

					if(!$page->is_home())
						echo '</a>';

					?>

					</div>
					<button class="open-nav" @click="menuOpen = true">
						<svg
							height="384pt"
							viewBox="0 -53 384 384"
							width="384pt"
							xmlns="http://www.w3.org/2000/svg"
						>
							<path
								d="m368 154.667969h-352c-8.832031 0-16-7.167969-16-16s7.167969-16 16-16h352c8.832031 0 16 7.167969 16 16s-7.167969 16-16 16zm0 0"
							/>
							<path
								d="m368 32h-352c-8.832031 0-16-7.167969-16-16s7.167969-16 16-16h352c8.832031 0 16 7.167969 16 16s-7.167969 16-16 16zm0 0"
							/>
							<path
								d="m368 277.332031h-352c-8.832031 0-16-7.167969-16-16s7.167969-16 16-16h352c8.832031 0 16 7.167969 16 16s-7.167969 16-16 16zm0 0"
							/>
						</svg>
					</button>
					<div class="navigation" :class="menuOpen ? 'openNavigation' : ''">
						<button class="close-nav" @click="menuOpen = false">
							<svg
								height="311pt"
								viewBox="0 0 311 311.07733"
								width="311pt"
								xmlns="http://www.w3.org/2000/svg"
							>
								<path
									d="m16.035156 311.078125c-4.097656 0-8.195312-1.558594-11.308594-4.695313-6.25-6.25-6.25-16.382812 0-22.632812l279.0625-279.0625c6.25-6.25 16.382813-6.25 22.632813 0s6.25 16.382812 0 22.636719l-279.058594 279.058593c-3.136719 3.117188-7.234375 4.695313-11.328125 4.695313zm0 0"
								/>
								<path
									d="m295.117188 311.078125c-4.097657 0-8.191407-1.558594-11.308594-4.695313l-279.082032-279.058593c-6.25-6.253907-6.25-16.386719 0-22.636719s16.382813-6.25 22.636719 0l279.058594 279.0625c6.25 6.25 6.25 16.382812 0 22.632812-3.136719 3.117188-7.230469 4.695313-11.304687 4.695313zm0 0"
								/>
							</svg>
						</button>
						<nav>
							<?= $menu ?>
						</nav>
						<div class="mobile-info">
							<a href="tel:<?= str_replace(" ", "", TELEFON_WEBU)?>"><?= TELEFON_WEBU ?></a>
							<a href="mailto:<?= EMAIL_WEBU ?>"><?= EMAIL_WEBU ?></a>
							<a href="https://www.facebook.com/Bornitflexi/" target="_blank">
								<svg
									aria-hidden="true"
									focusable="false"
									data-prefix="fab"
									data-icon="facebook-square"
									class="svg-inline--fa fa-facebook-square fa-w-14"
									role="img"
									xmlns="http://www.w3.org/2000/svg"
									viewBox="0 0 448 512"
								>
									<path
										d="M400 32H48A48 48 0 0 0 0 80v352a48 48 0 0 0 48 48h137.25V327.69h-63V256h63v-54.64c0-62.15 37-96.48 93.67-96.48 27.14 0 55.52 4.84 55.52 4.84v61h-31.27c-30.81 0-40.42 19.12-40.42 38.73V256h68.78l-11 71.69h-57.78V480H400a48 48 0 0 0 48-48V80a48 48 0 0 0-48-48z"
									></path>
								</svg>
							</a>
						</div>
					</div>
				</div>
			</div>
		</header>