<?php

/**
 * @author Pavel Srom
 * @copyright 2009
 */




session_start();
mb_internal_encoding("UTF-8");

define('PRE_PATH','');
define('SECURITY',1);



//nacteni konfigurace
include_once('php/config/config.paths.php');
include_once('php/config/config.web.php');
include_once('php/config/config.db.php');
include_once('php/config/config.others.php');

//nacteni knihoven
include_once('php/functions/functions.system.php');
include_once('php/functions/functions.web.php');
include_once('php/functions/functions.input.formats.php');
include_once('php/functions/functions.error.handlers.php');



//nacteni systemovych trid
include_once('php/classes/class.db.php');
include_once('php/classes/class.domain.web.php');
include_once('php/classes/class.messages.php');
include_once('php/classes/class.page.php');
include_once('php/classes/class.panels.php');
include_once('php/classes/class.layout.php');
include_once("php/classes/class.cache.processor.php");



//include_once('php/classes/class.links.web.php');
include_once('php/classes/class.langs.web.php');
include_once('php/classes/class.tags.web.php');
include_once('php/classes/class.private.pages.web.php');

include_once('php/classes/class.url.web.php');
include_once('php/classes/class.pagination.php');
include_once('php/classes/class.template.php');

/*
require 'php/classes/latte.php';
$latte = new Latte\Engine;
*/



$db = new C_Db();	
$db->Connect();




//error_settings();
error_reporting(E_ALL);

$userSystemMessage = new C_SystemMessage('user');
$userSystemMessage->set_for_web();

$domain = new C_Domains();
$domain->setDir(DIRNAME_DOMAINS."0001/");


if($domain->getId() == 0)
{
    Redirect('',503);
    exit;
}



Redirect301();



$lang = new C_WebLangs(LANG, $domain->getId());

define('WEB_LANG_ID',$lang->get_id());
define('DOMAIN_ID',$domain->getId());
define('RELATIVE_URL_USER_DOMAIN', $domain->getRelativePath().$domain->getDir());
define('ABSOLUTE_URL_USER_DOMAIN', $domain->getUrl().$domain->getDir());

//$latte->setTempDirectory(RELATIVE_URL_USER_DOMAIN.USER_DIRNAME_TEMP);


//presmerovani domeny na verzi s www
if($domain->RedirectWww()){		  
    $url = PROTOKOL."www.".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
    Redirect($url);
    }



if(stristr($_SERVER['REQUEST_URI'],'index.php'))
    Redirect($domain->getUrl());



GetActiveModules();
loadSettings(DOMAIN_ID,"",false);
loadTranslations(LANG, true);

//$links = new C_Links();
$links = new C_Url();

$private_pages = new C_PrivatePages($domain->getId());





//RSS, Sitemap, Robots
if(isset($_GET['file']))
{
    include("php/classes/class.xmlfile.php");
    $f = new C_XmlFile($_GET['file']);
    $f->generate();
    exit;
}







if (isOnline() && $_SERVER['SERVER_NAME'] != $domain->getName() && $_SERVER['SERVER_NAME'] != "www.".$domain->getName()){
    
    Redirect(PROTOKOL.$domain->getName()."/");
	}


zapocitatHlasovaniAnkety();

//zapocitani prokliku do statistik emailingu
zapocitatProklikEmailoveKampane();

$pagination = new C_Patination();
$page = new C_Page();
$layout = new C_Layout();

$cache_processor = new CacheProcessor();



if(!$private_pages->has_access($page->get_id()))
{
    $private_pages->redirect(true);
}
//print_r($_SESSION);


//implementace modulu INLINE EDITACE
$layout->set_js_path(RELATIVE_PATH);
$layout->set_css_path(RELATIVE_PATH);
 
//$layout->add_js('js/jquery-migrate.js'); 

if(MODULE_INLINE_EDIT && logged_into_cms())
{
    $layout->add_js('js/jquery.js');
    
    $_SESSION['useruploads'] = ABSOLUTE_URL_USER_DOMAIN.USER_DIRNAME_USERUPLOADS;
    
    //NASTAVENI INLINE EDITACE
    //povoleni inline editace na webu
    $_SESSION['ine'] = isset($_SESSION['ine']) ? $_SESSION['ine'] : true;
    
    //pozice panelu pro inline editaci - top, bottom
    $_SESSION['ine_panel_position'] = isset($_SESSION['ine_panel_position']) ? $_SESSION['ine_panel_position'] : 'top';
    
    //stav panelu pro inline editaci - hide, nebo prazdna hodnota
    $_SESSION['ine_panel_state'] = isset($_SESSION['ine_panel_state']) ? $_SESSION['ine_panel_state'] : 'hide';
    
    
    
    include("php/functions/functions.inputs.php");
    define('AJAX_GATEWAY_INLINE_EDIT', RELATIVE_PATH."php/ajax/ajax.system.gateway.php?weblang=".WEB_LANG_ID."&lang=".LANG."&module_id=33&module=inlineedit&action=");
    $_SESSION['AJAX_GATEWAY_INLINE_EDIT'] = AJAX_GATEWAY_INLINE_EDIT;

    //nacteni editoru    
    $layout->add_js("ckeditor/ckeditor.js");
    $layout->add_js("ckfinder/ckfinder.js");
    
    if(INLINE_EDIT_PLUS)
    {    
    
    //$layout->add_js("inline_edit/js/floating.js");
    
    //nacteni knihoven jquery ui
    /*
    $layout->add_css("php/layout/gray/jquery-ui.css");
    $layout->add_js("js/jquery-ui.js");
    $layout->add_js("js/jquery.ui.datepicker-cs.min.js");
    $layout->add_js("js/highcharts/js/highcharts.js");
    $layout->add_js("js/jquery.blockUI.js");
    */ 
    $layout->add_js("js/jquery.blockUI.js");
    $layout->add_css("inline_edit/css/inline_edit.css");
    $layout->add_css("https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css");  
    }
    
    $layout->add_js("inline_edit/inline_edit.js.php?relpath=".RELATIVE_PATH."&domainpath=".RELATIVE_PATH.$domain->getDir()."&pageid=".$page->get_id());
    
      
    //definice stranek pro ckeditor
    $pages_items = 'var pages_items = '.get_pages_for_editor().';'; 
    //$pages_items = 'var pages_items = [];';  

    $layout->add_inline_js($pages_items);

}

$index = $domain->getDir()."/index.php";
//$index = 'domains/0001/index.php';


zapocitej_zobrazeni_stranky();
    



if(file_exists($index)){
	include($index);
	exit;
	}
	else{
		//echo "Problém s vygenerováním webu ".$domain->getName();
        Redirect('',503);
		exit;	
}





?>