<?php


session_start();
mb_internal_encoding("UTF-8");

//ini_set('session.gc_maxlifetime', 3000);

//nacteni konfigurace
DEFINE('PRE_PATH','');
DEFINE('SECURITY_CMS','');
DEFINE('ADMIN_SCRIPT','carmeo'); //cms.php



include_once('php/config/config.paths.php');
include_once('php/config/config.admin.php');
include_once('php/config/config.db.php');
include_once('php/config/config.others.php');

include_once(CMS_THEME_PATH.'config.php');

//nacteni knihoven
include_once('php/functions/functions.show.php');
include_once('php/functions/functions.system.php');
include_once('php/functions/functions.admin.php');
include_once('php/functions/functions.inputs.php');
include_once('php/functions/functions.input.formats.php');
include_once('php/functions/functions.error.handlers.php');

include_once('php/classes/class.db.php');
include_once('php/classes/class.messages.php');
include_once('php/classes/class.domain.admin.php');
include_once('php/classes/class.langs.admin.php');
include_once('php/classes/class.login.php');
include_once('php/classes/class.modules.php');
include_once('php/classes/class.url.web.php');
include_once('php/classes/class.form.admin.php');
include_once('php/classes/class.table.php');
include_once('php/classes/class.inputfilter.php');
include_once('php/classes/class.content.backup.php');
include_once('php/classes/class.links.checker.php');
include_once('php/classes/class.log.php');
include_once('php/classes/class.tags.php');
include_once('php/classes/class.object.access.php');
include_once('php/classes/class.private.pages.admin.php');
include_once('php/classes/class.newsletters.php');



$systemMessage = new C_SystemMessage('cms');

$db = new C_Db();	
$db->Connect();

error_settings();

$domain = new C_Domains();



loadSettings($domain->getId());
loadTranslations(CMS_LANG);


$login_obj = new C_User();


if(!$login_obj->logged())
{
    include(CMS_THEME_PATH.'cms_login.php');
    return;
}

//dd($_GET);

$lang = new C_WebLangs($login_obj->getLanguages(), $domain->getId(), WEB_LANG_ID);
$log = new LogItem(0,$domain->getId(), WEB_LANG_ID, $login_obj->getId(), $login_obj->getName());
$object_access = new C_ObjectAccess();


//var_dump($object_access->has_access(131));


define("WEB_LANG",strtoupper($lang->get_active_lang('jazyk')));
define('LANG', strtolower(WEB_LANG));


//pokud se uzivatel automaticky prihlasuje pres generovany link, pak je presmerovany na web z duvodu inline editace
if($login_obj->logged() && $login_obj->autologin)
{
    Redirect($domain->getUrl());
}


if(!$lang->is_allowed())
{
    $systemMessage->add_error(TBYL_JSTE_AUTOMATICKY_ODHLASEN_Z_DUVODU_PRISTUPU_K_NEPOVOLENE_JAZYKOVE_MUTACI);
    $login_obj->logout();
}

if(isset($_GET['logout']))
{
    $log->add_log('logout');
    $login_obj->logout();
}
            
//prepne domenu na jinou
if(isset($_GET['nastavitDomenu'])){
    $idd = intval($_GET['nastavitDomenu']);
   
	if(!$domain->SetActiveDomain($idd) || !$domain->UserDomainsById($domain->getId()))
		$login_obj->Logout();
	        			
	Redirect(RELATIVE_PATH.ADMIN_SCRIPT."/");
}

$links = new C_Url();

//nastaveni session pro wysiwyg editor
define('RELATIVE_URL_USER_DOMAIN', RELATIVE_PATH.$domain->getDir());
define('ABSOLUTE_URL_USER_DOMAIN', $domain->getUrl().$domain->getDir());


//nastaveni cest pro tinymce
//$_SESSION['useruploads'] = ABSOLUTE_URL_USER_DOMAIN.USER_DIRNAME_USERUPLOADS;


$adm = $login_obj->UserPrivilege('superadmin') || $login_obj->UserPrivilege('admin'); 


//print_r($domain->data['moduly']);


$modules = new C_Modules($domain->data['moduly'], $adm);
$aktModule = $modules->GetSystemNameOfActiveModule();



if(isset($_POST['btnLogin']))
    $log->add_log('login');

/*
$c = "<a href='ftp://ftp.cpc.ncep.noaa.gov/long/misc/SSW_Poster_new.pdf'>test</a>";
$l = new LinksChecker($domain->getId());
$l->set_content($c);
$r = $l->check_url('www.sundial.damia.net');
print_r($r);
exit;
*/



if($aktModule != "")
{
    $idAktivnihoModulu = $modules->GetActiveModule()->get_id();

    define('AJAX_GATEWAY', RELATIVE_PATH."php/ajax/ajax.system.gateway.php?weblang=".WEB_LANG_ID."&lang=".CMS_LANG."&module_id=".$idAktivnihoModulu."&module=".$modules->GetActiveModule()->moduleSystemName."&action=");
    define('AJAX_GATEWAY_WET', RELATIVE_PATH."php/ajax/ajax.system.gateway.php?weblang=".WEB_LANG_ID."&lang=".CMS_LANG."&module=wet&action=");
}
else
{
    define('AJAX_GATEWAY','');
    define('AJAX_GATEWAY_WET','');
}


$pom = explode('?', $_SERVER['REQUEST_URI']);
$pom1 = explode('/', $pom[0]);
$nejsme_v_modulu = end($pom1) == 'admin' || end($pom1) == '';


//unset($pom[count($pom) - 1]);
if(count($pom)==1 && $nejsme_v_modulu && $login_obj->logged() && $modules->GetCountModules() > 0 && $aktModule == '') 
    {
        Redirect($modules->GetFirstUrl());
    }	
elseif($modules->GetCountModules() > 0 && $aktModule == '' && $login_obj->logged())
    {
        include(CMS_THEME_PATH.'cms_404.php');
        exit;
    }
elseif($modules->GetCountModules() == 0)
    {
	    $systemMessage->add_error(TNEMATE_POVOLEN_ZADNY_MODUL);
		$login_obj->Logout();	
	}

 
include(CMS_THEME_PATH.'cms_page.php');	

?>