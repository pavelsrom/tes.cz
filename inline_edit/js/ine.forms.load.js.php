//----------------------------------------------------------
// nacte ajaxem formular pro editaci stranky 
// nebo vytvoreni nove stranky
//----------------------------------------------------------
    
function nacti_formular_editace_stranky(id_stranky)
{
    $.post(INE_AJAX_LOAD, {id_page: id_stranky, id_ine_page: INE_PAGE_ID}, function(data){
        ine_zrus_loader();
        nastav_ajax_pro_ulozeni( data.url.save );
        nastav_ajax_pro_upload( data.url.upload );
        nastav_ajax_pro_smazani_fotky_clanku( data.url.photo_article );
        nastav_ajax_pro_smazani_fotky_galerie( data.url.photo_gallery );
        
        INE_EL_FORM.html( data.html );        
        nastav_vysku_formulare();
        
        INE_EL_FORM.find(".save_button").click(function(e){
            e.preventDefault();
            ine_zobraz_loader();
            ulozit_formular(false,false);
            
            })
        
        
        /*
        aktivuj_tabs();
        aktivuj_tooltip();
        aktivuj_kalendare();
        aktivuj_sortable();

        var dialog_buttons = {}; 
        dialog_buttons["<?php echo TULOZIT;?>"] = function(){
            ulozit_formular(false,false);
        };
        dialog_buttons["<?php echo TZRUSIT;?>"] = function(){
            $( this ).dialog( "close" );
        };
                        
        INE_EL_FORM.dialog("option","buttons", dialog_buttons);
        */

    },'json');
}




//----------------------------------------------------------
// vyvola confirmacni okno pro overeni smazani stranky 
// a stranku smaze
//----------------------------------------------------------
function nacti_formular_smazat_stranku()
{
 
    
    $.post(INE_AJAX_LOAD, {id_page: INE_PAGE_ID}, function(data){
        
        ine_zrus_loader();
        nastav_ajax_pro_ulozeni( data.url );
        
        INE_EL_FORM.html( data.html );        
        nastav_vysku_formulare();
        
        
        INE_EL_FORM.find(".delete_button").click(function(e){
            e.preventDefault();
            ine_zobraz_loader();
            smazat_stranku();
            })
        
    },'json');       
    
}


function nacti_formular_hledat_stranku()
{
    $.post(INE_AJAX_LOAD, {}, function(data){
        
        ine_zrus_loader();
        nastav_ajax_pro_ulozeni( data.url.autocomplete );
        
        INE_EL_FORM.html( data.html );        
        nastav_vysku_formulare();
        
        var timer = null;
        
        INE_EL_FORM.find("#ine_find_input").on("keyup",function(e){
            e.preventDefault();
            search = $(this).val();
                    
            // cancel any previously-set timer
            if (timer) {
                clearTimeout(timer);
            }
            
            timer = setTimeout(function() {
                autocomplete_div = $("#ine_find_result");
                $.get(INE_AJAX_SAVE, {'search': search}, function(response){
                    autocomplete_div.html(response)
                });
            }, 400);
            
        })
        
        
    },'json');  
}


function nacti_formular_navstevnost_webu()
{
    var d1 = "";
    var d2 = "";
    var uid = "";
    if(d1 == "" && d2 == "")
    {
        uid = "&" + (new Date()).getTime();
    }
    
    $.getJSON(INE_AJAX_LOAD + uid,{od: "", do: "", typ: 1},function(opts){                    

        INE_EL_FORM.highcharts(opts);
        INE_EL_FORM.dialog({
            //close: null,
            buttons: null
            });
    });
    
    
}


function nacti_formular_navstevnost_stranky()
{
    var d1 = "";
    var d2 = "";
    var uid = "";
    if(d1 == "" && d2 == "")
    {
        uid = "&" + (new Date()).getTime();
    }
    
    $.getJSON(INE_AJAX_LOAD + uid,{od: "", do: "", typ: 1, id: INE_PAGE_ID}, function(opts){                    

        INE_EL_FORM.highcharts(opts);
        INE_EL_FORM.dialog({
            //close: null,
            buttons: null
            });
    });
}


function nacti_formular_zalohy_stranky()
{
    $.post(INE_AJAX_LOAD, {id_page: INE_PAGE_ID}, function(data){
        ine_zrus_loader();
        nastav_ajax_pro_ulozeni( data.url.save );
        INE_EL_FORM.html( data.html ); 
        
        nastav_vysku_formulare();
        
        
        $("#ine_form #ine_zalohy .ine_sl1 ul li a").first().click();
        
        INE_EL_FORM.find(".save_button").click(function(e){
            e.preventDefault();
            ine_zobraz_loader();
            uloz_formular_zalohy_stranky();

            })
            
        
        
                   
        //aktivuj_tabs();

        /*        
        var dialog_buttons = {}; 
        dialog_buttons["<?php echo TULOZIT;?>"] = function(){
            uloz_formular_zalohy_stranky();
        };
        dialog_buttons["<?php echo TZRUSIT;?>"] = function(){
            $( this ).dialog( "close" );
        };
                        
        INE_EL_FORM.dialog("option","buttons", dialog_buttons);
        */
        
    },'json');
}




function nacti_formular_preklady_webu()
{
    $.post(INE_AJAX_LOAD, null, function(data){
        
        nastav_ajax_pro_ulozeni( data.url.save );
        INE_EL_FORM.html( data.html );        
                
        var dialog_buttons = {}; 
        dialog_buttons["<?php echo TULOZIT;?>"] = function(){
            uloz_formular_preklady_webu();
        };
        dialog_buttons["<?php echo TZRUSIT;?>"] = function(){
            $( this ).dialog( "close" );
        };
                        
        INE_EL_FORM.dialog("option","buttons", dialog_buttons);
        
    },'json');
    
    
}

function nacti_formular_nastaveni_webu()
{
    $.post(INE_AJAX_LOAD, null, function(data){
        ine_zrus_loader();
        nastav_ajax_pro_ulozeni( data.url.save );
        INE_EL_FORM.html( data.html );        
        nastav_vysku_formulare();
        
        INE_EL_FORM.find(".save_button").click(function(e){
            e.preventDefault();
            ine_zobraz_loader();
            uloz_formular_nastaveni_webu();

            })
            
        
        //INE_EL_FORM.
        
        //aktivuj_tabs();
        //aktivuj_tooltip();
        /*
        var dialog_buttons = {}; 
        dialog_buttons["<?php echo TULOZIT;?>"] = function(){
            uloz_formular_nastaveni_webu();
        };
        dialog_buttons["<?php echo TZRUSIT;?>"] = function(){
            $( this ).dialog( "close" );
        };
          */              
        //INE_EL_FORM.dialog("option","buttons", dialog_buttons);
        
    },'json');
}

function nastav_vysku_formulare()
{
    max_h = $(window).height() - 150;
    $("#ine_form").css({maxHeight: max_h});
        
}

    
