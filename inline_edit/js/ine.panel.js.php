
//***********************************************************************************
//nacteni panelu ajaxem
//***********************************************************************************
   
    
    $.post(INE_PATH_AJAX_GATEWAY + "panel",{id_page: INE_PAGE_ID}, function(data){
        $("body").prepend(data);
        /*
        $("#ine_vyhledavani input").autocomplete({
            minLength: 1,
            source: INE_PATH_AJAX_GATEWAY + "find.pages",
            position: { my: "right top", at: "right bottom", collision: "none" },
            delay: 500,
            select: function( event, ui ) {
                location.href = ui.item.url;
                return false;
                }
        })
        .data( "ui-autocomplete" )._renderItem = function( ul, item ) {
            return $( "<li>" )
            .append( "<a>" + item.type + ": " + item.name + "</a>" )
            .appendTo( ul );
            };
        */
        INE_EL_FORM = $("#ine_form #ine_content");
        INE_EL_PANEL = $("#ine_panel");
        INE_EL_PANEL_IN = $("#ine_panel_in");
        INE_PAGE_NAME = $("#ine_page_name").html();
    });
    
    
    
    
    
    
//***********************************************************************************
//zobrazovaci a chovani ikonek v levem sloupecku
//***********************************************************************************
    
    $(document).on("click", "#ine_panel a.ine_icon_mini", function(){
        var id = $(this).attr("id");
        
        if(id != "hide_panel" && id != "show_panel")
        {        
            var is_hidden = INE_EL_PANEL.hasClass("hide_panel");
            INE_EL_PANEL.removeClass();
            
            INE_EL_PANEL.addClass(id);
            if(is_hidden)
                INE_EL_PANEL.addClass("hide_panel");
                
        }
        
        if(id == "position_top")
        {
            INE_EL_PANEL.removeClass("position_top");
            INE_EL_PANEL.addClass("position_bottom");
            INE_EL_FORM.hide();
            
            /*
            $("#ine_vyhledavani input").autocomplete("option", {
                position: { my: "right top", at: "right bottom", collision: "none" },
            })
            */
            
            $.get(INE_PATH_AJAX_GATEWAY + "change.state",{typ: "position", value: "bottom"})
            
        }
        else if(id == "position_bottom")
        {
            /*
            $("#ine_vyhledavani input").autocomplete("option", {
                position: { my: "right bottom", at: "right top", collision: "none" },
            })
            */
            
            INE_EL_FORM.hide();
            
            INE_EL_PANEL.addClass("position_top");
            INE_EL_PANEL.removeClass("position_bottom");
            $.get(INE_PATH_AJAX_GATEWAY + "change.state",{typ: "position", value: "top"})
        }   
        else if(id == "hide_panel")
        {
            INE_EL_FORM.hide();
            INE_EL_PANEL.find(".ine_active").removeClass("ine_active");
            
            INE_EL_PANEL_IN.hide();
            INE_EL_PANEL.addClass("hide_panel");
            INE_EL_PANEL.animate({height: 45},500,function(){
                
                INE_EL_PANEL_IN.fadeIn();

                })
            
            $.get(INE_PATH_AJAX_GATEWAY + "change.state",{typ: "state", value: "hide"})
            
        }
        else if(id == "show_panel")
        {
            INE_EL_PANEL_IN.hide();
            INE_EL_FORM.hide();
            INE_EL_PANEL.find(".ine_active").removeClass("ine_active");
                
            INE_EL_PANEL.animate({height: 142},500,function(){
                INE_EL_PANEL.removeClass("hide_panel");
                INE_EL_PANEL_IN.fadeIn();
            });
    
            $.get(INE_PATH_AJAX_GATEWAY + "change.state",{typ: "state", value: "show"})    

            
        }
 
        return false;
        
    })
    
    
    
    $(document).on("click","a.ine_stav",function(){
        
        a = $(this);
        id = a.attr("id");
        //alert(id);
        
        if(id == 'ine_aktivni')
            $.get(INE_PATH_AJAX_GATEWAY + "change.state",{typ: "ine"},function(){
                location.reload();
            })
        else if(id == 'ine_stranka_aktivni')
            $.get(INE_PATH_AJAX_GATEWAY + "page.state",{id_page: INE_PAGE_ID},function(){
                location.reload();
            })
        return false;
        
    })
    
    //zalozky v casti pro zalohy
    $(document).on("click","#ine_zalohy .ine_sl1 ul li a", function(){
        a = $(this);
        $("#ine_zalohy .ine_sl2").show();
        href = a.attr("href");
        $("#ine_zalohy .ine_sl1 ul li a").removeClass("ine_active");
        a.addClass("ine_active");
        $(".ine_zaloha").hide();
        $(href).show();
       
        return false;
        
        
        
    })
    
    
    