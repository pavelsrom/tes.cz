

//inicializace editoru obrazku a souboru
    CKEDITOR.disableAutoInline = true;
    CKFinder.setupCKEditor( null, INE_PATH_RELATIVE + 'ckfinder/' );
    
    //***********************************************************************************
    // definici ukladani pomoci ajaxu, aktivace oblasti pro editor
    //***********************************************************************************
    
    $("*[contenteditable='true']" ).each(function( index ) {

        var content_id = $(this).attr("id");
        //alert(content_id);
        CKEDITOR.inline( content_id, {
            language: 'cs',
            entities: false,
            htmlEncodeOutput: false,
            entities_greek: false,
            entities_latin: false,
            toolbarGroups: [
            { name: 'document',    groups: [ 'print','preview','doctools','templates' ] },
            { name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
            { name: 'editing',     groups: [ 'find' ] },
            { name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align' ] },
            { name: 'tools', groups: ['mode','maximize'] },
            '/',
            { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
            { name: 'links' },
            { name: 'insert' },
            { name: 'styles'}
           
            ],
            //removePlugins: 'autosave,save,font,newpage,placeholder,docprops,bidi,symbol,youtube',
            removeButtons: 'Font,FontSize',
            extraPlugins: 'sourcedialog',
            baseUrl: INE_PATH_DOMAIN + 'userfiles/',
            templates_files: [INE_PATH_AJAX_GATEWAY + 'templates.list'],
            on: {
                blur: function( event ) {
                    
                    var content = event.editor.getData();
                    $.post(INE_PATH_AJAX_GATEWAY + "content.update",{content : content, id : content_id},function(data){
                        if(data.error)
                        {
                            alert(data.error);
                        }
                    },
                    "json"
                    );
                    

                }
            }
        } );

    });