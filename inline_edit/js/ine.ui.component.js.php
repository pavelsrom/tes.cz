//----------------------------------------------------------    
//funkce aktivuje dialogove okno pro formular
//----------------------------------------------------------



function ine_zobraz_loader(text)
{
    if(!text)
        text = '<?php echo TCEKEJTE_PROSIM;?>';
        
    $("#ine_content").hide();
    $("#ine_loader").html("<p>" + text + "</p>").fadeIn(250);
    
}

function ine_zrus_loader()
{
    
    
    $("#ine_loader").fadeOut(250,function(){
        $("#ine_content").show();
    });
    
}


function aktivuj_sortable()
{
    s = INE_EL_FORM.find("#ine_sortable");
    
    existuje_sortable_list = s.length > 0;
    
    if(!existuje_sortable_list)
        return false;
        
    s.sortable({
        placeholder: "ui-state-highlight"
    });
    
    
    
}


//----------------------------------------------------------    
//funkce aktivuje dynamickou napovedu k novemu formulari
//----------------------------------------------------------

function aktivuj_tooltip()
{
    INE_EL_FORM.find("span.jtip").tooltip({
            tooltipClass: "ine_napoveda",
            position: { my: "left+10 top", at: "right center" },
            open: function(event, ui){
                id = $(this).attr("id").substring(2);
                //reseni podle: http://bugs.jqueryui.com/ticket/8740
                var _elem = ui.tooltip;
                $.get(INE_PATH_AJAX_GATEWAY + "qtip", {id: id},function(help){
                    _elem.find(".ui-tooltip-content").html(help);
                })

            },
            content: "<?php echo TCEKEJTE_PROSIM;?>"
            
        });
                
}
    
    
    
//----------------------------------------------------------    
//funkce aktivuje kalendare pro inputy k novemu formulari
//----------------------------------------------------------
    
function aktivuj_kalendare()
{
    INE_EL_FORM.find("input.kalendar").datepicker({
        dateFormat: 'dd.mm.yy',
		firstDay: 1
    })
}


   

function aktivuj_zpravu(messages)
{
    if(messages.error && messages.error!='')
    {
        aktivuj_error_zpravu(messages.error);
    }
    else if(messages.warning && messages.warning!='')
    {
        aktivuj_warning_zpravu(messages.warning);
    }
    else if(messages.ok && messages.ok!='')
    {
        aktivuj_ok_zpravu(messages);
    }
    
    
}


function presmeruj_na_stranku(url)
{
    location.href = url;
}


function aktivuj_ok_zpravu(message)
{
    var m = $("#ine_system_message"); 
    m.html("<p class='ine_ok'>" + message.ok + "</p>");
    m.fadeIn();
    setTimeout(function(){
        m.fadeOut();
        }, 2000); 
    
    if(message.redirect_url)
        location.href = message.redirect_url;
        
}


function aktivuj_error_zpravu(text)
{

    var m = $("#ine_system_message"); 
    m.html("<p class='ine_error'>" + text + "</p>");
    m.fadeIn();
    setTimeout(function(){m.fadeOut()}, 5000);
    
}


function aktivuj_warning_zpravu(text)
{
    var m = $("#ine_system_message"); 
    m.html("<p class='ine_warning'>" + text + "</p>");
    m.fadeIn();
    setTimeout(function(){m.fadeOut()}, 5000);
    
}


function zobraz_vybrany_obrazek_z_disku(kde)
{
    
    
    
    
}