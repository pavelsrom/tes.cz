//***********************************************************************************
// otevreni a nacteni formulare v dialogovem modalnim okne
//***********************************************************************************
    
    
    $(document).on("click","a.ine_icon",function(){
        a = $(this);
        
        if(a.hasClass("ine_disabled"))
        {
            return false;
        }
        
        
        if(a.hasClass("ine_active"))
        {
            a.removeClass("ine_active");
            $("#ine_form").slideUp(500, function(){
                $("#ine_form").hide();
                });
            return false;
        }
        
        $("#ine_form").show();
        
        var action = $(this).attr("id").substring(11);
        var title = $(this).attr("title");
        var href = $(this).attr("href");
        INE_AJAX_LOAD = href;
        
        if(action != 'odhlasit' && action != 'do-cms')
        {
            $("a.ine_icon").removeClass("ine_active");
            $(this).addClass("ine_active");
        }
        
        if(action != 'do-cms')
            ine_zobraz_loader();
        
        //alert(action);
        if(action == 'editace-stranky')
        {
            nacti_formular_editace_stranky(INE_PAGE_ID);
        }  
        else if(action == 'najit-stranku')
        {
            nacti_formular_hledat_stranku();
        }      
        else if(action == 'smazat-stranku')
        {
            nacti_formular_smazat_stranku();
        }
        else if(action == 'navstevnost-webu')
        {
            nacti_formular_navstevnost_webu();
        }
        else if(action == 'navstevnost-stranky')
        {
            nacti_formular_navstevnost_stranky();
        }
        else if(action == 'zaloha-obsahu')
        {
            nacti_formular_zalohy_stranky();
        }
        else if(action == 'preklady-webu')
        {
            nacti_formular_preklady_webu();
        }    
        else if(action == 'nastaveni-webu')
        {
            nacti_formular_nastaveni_webu();
        }
        else if(action == 'odhlasit')
        {
            return true;
        }
        else if(action == 'do-cms')
        {
            return true;
        }
        else if(action == 'pridat-stranku' || action == 'pridat-jine1')
        {
            nacti_formular_editace_stranky(0);
        }
        
        
        
        return false;
        
            

    })
    
    
    
    
    
    //***********************************************************************************
    //***********************************************************************************
    //
    // funkce pro mazani objektu - fotek
    //
    //***********************************************************************************
    //***********************************************************************************  


    $(document).on("click","#ine_form a.delete", function(){
        var text = "<?php echo TOPRAVDU_SMAZAT_TENTO_OBRAZEK;?>";
        var td = $(this).parents("td.image").first();
        var span = td.find("span");
        
        
        ine_confirm(text, function(){
            td.addClass("deleted");
            $.post(INE_PATH_AJAX_GATEWAY + "image.delete",{id: <?php echo $pageId;?>},function(data){
                span.html("<?php echo TNENI_ULOZEN_ZADNY_OBRAZEK;?>");
                $("#obrazek_input").removeAttr('disabled');
                //td.removeAttr("class");
                });
            
            })
            
        return false;
    })
    
    
    //***********************************************************************************
    //***********************************************************************************
    //
    // funkce pro mazani objektu - fotek
    //
    //***********************************************************************************
    //***********************************************************************************  


    $(document).on("click","#ine_form #ine_sortable a.ine_delete", function(){
        var text = "<?php echo TOPRAVDU_SMAZAT_TENTO_OBRAZEK;?>";
        var box = $(this).parents("div.ine_fotka").first();
        var id_fotky = box.attr("id").substring(1);
        
               
        
        ine_confirm(text, function(){
            box.addClass("deleted");
            $.post(INE_AJAX_DELETE_PHOTO_GALLERY,{idItem: id_fotky}, function(data){
                aktivuj_zpravu(data.messages);
                if(data.messages.ok && data.messages.ok != '')
                {
                    box.fadeOut(500,function(){
                        $(this).remove()
                        });
                }
            },'json');
            
        })
            
        return false;
    })
    
    
    
    
    
    //***********************************************************************************
    //***********************************************************************************
    //
    // funkce pro automaticky hromadny upload fotek
    //
    //***********************************************************************************
    //***********************************************************************************  

    
    $(document).on("change","#ine_form input[name=fotky]", function(){
        
        
        
        //inicializace progressbaru
        var progressbar = $( "#ine_progressbar" );
        var progressLabel = $( ".progress-label" );
        
        //progressbar.fadeIn();
        
        $(".ine_progresbar_box").fadeIn();
        progressLabel.show();
        $("#ine_protokol").html("");
        
        progressbar.progressbar({
            value: false,
            change: function() {
                val = progressbar.progressbar( "value" ) || 0;
                progressLabel.text( val + "%" );
            },
            complete: function() {
                progressLabel.text( "<?php echo TDOKONCENO;?>" );
            }
        });
        
        
        
        var o = $(this);
        
        if(o.length == 0)
            return;
                
        var soubory = o[0].files; 
        var pocet_souboru = soubory.length;
        
        var prog = Math.round((100 / pocet_souboru) + 0.5); 
        var pocet_uploadu = 0;
               
        $.each(soubory, function(i, file){
            formData = new FormData();
            formData.append("id_ine_page", INE_PAGE_ID);
            formData.append("Filedata", file);
            
            
            $.ajax({
                url: INE_AJAX_UPLOAD,
                type: 'POST',
                async: false,
                xhr: function() {
                    myXhr = $.ajaxSettings.xhr();
                    if(myXhr.upload){
                        myXhr.upload.addEventListener('progress',function(prog){
                            //var value = Math.round((~~((prog.loaded / prog.total) * 100)) / pocet_souboru);
                            
                        }, false);
                    }
                    return myXhr;
                },
                data: formData,
                cache: false,
                dataType: 'json',
                contentType: false,
                processData: false,
                complete: function(){
                    var val = progressbar.progressbar( "value" ) || 0;
                    if(val > 100)
                        val = 100;
                                        
                    progressbar.progressbar( "value", val + prog);
                    pocet_uploadu++;
                    
                    if(pocet_uploadu == pocet_souboru)
                    {
                        /*
                        progressbar.fadeOut(1000,function(){
                            progressbar.progressbar("destroy");
                        })
                        */
                    }
                    
                    
                },
                success : function(res) {
                    if(res.file.html)
                    {
                        $("#ine_sortable").prepend(res.file.html);
                    }
                    
                    m = res.messages.error && res.messages.error != "" ? res.messages.error : res.messages.ok;
                    if(!m)
                        m = "---";
                        
                    $("#ine_protokol").prepend(res.file.name + ": " + m + "<br />");
                    
                    
                    //aktivuj_zpravu(res.messages);
                    //if(successFn) successFn(res);
                },
                error: function(jqXHR, textStatus, errorThrown)
                {
                    alert(textStatus + ": " + errorThrown);
                    // Handle errors here
                    //console.log('ERRORS: ' + textStatus);
                    // STOP LOADING SPINNER
                }
            });
        });
        
        
        
        
        
    })
    
    
    
    
    
    //***********************************************************************************
    //***********************************************************************************
    //
    // funkce pro zobrazeni nahledu fotky po vyberu fotky ve formulari
    //
    //***********************************************************************************
    //*********************************************************************************** 
    
    $(document).on("change","#ine_form input#obrazek_input", function(e){
        
        //pokud neni podporovan objekt fileReader pak se obrazek nezobrazi
        if(!INE_PODPORA_FILE_READER)
            return true;
        
        //alert($("#ine_form input#obrazek_input").attr("class"));
        var file = $("#ine_form input#obrazek_input").get(0).files[0];
        
        e.preventDefault();
       // var file = upload.files[0];
        var holder = $("#ine_form table td.ine_nahled");
        reader = new FileReader();
        
        reader.onload = function (event) {
            var img = new Image();
            img.src = event.target.result;
            // note: no onload required since we've got the dataurl...I think! :)

            holder.html('');
            holder.append(img);
        };
          
        reader.readAsDataURL(file);
        return false;
        
        
    })
    
    
    