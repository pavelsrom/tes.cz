<?php

/**
 * @author Pavel Srom
 * @copyright 2014
 */



?>


function nastav_ajax_pro_ulozeni(url)
{
    INE_AJAX_SAVE = url;
}

function nastav_ajax_pro_upload(url)
{
    INE_AJAX_UPLOAD = url;
}

function nastav_ajax_pro_smazani_fotky_galerie(url)
{
    INE_AJAX_DELETE_PHOTO_GALLERY = url;
}

function nastav_ajax_pro_smazani_fotky_clanku(url)
{
    INE_AJAX_DELETE_PHOTO_ARTICLE = url;
}

