

// zjisteni zda jsou podporovany urcite prvky html5
var INE_PODPORA_FILE_READER = typeof window.FileReader !== 'undefined';
var INE_PODPORA_UPLOAD_FILE = window.File && window.FileList && INE_PODPORA_FILE_READER;



//cesty k domene, k adresari inline_edit, k ajaxovym skriptum atd.
var INE_PATH_RELATIVE = "<?php echo $relpath; ?>";
var INE_PATH_DOMAIN = "<?php echo $domainpath;?>";
var INE_PATH_AJAX_GATEWAY = "<?php echo $ajax_gateway_path; ?>";



//id aktivni stranky
var INE_PAGE_ID = <?php echo $pageId;?>



//elementy
//tyto hodnoty budou aktualizovany az pri nacteni html kodu ajaxem v souboru ine.panel.js
var INE_EL_PANEL = null;
var INE_EL_FORM = null; 
var INE_EL_PANEL_IN = null;

var INE_PAGE_NAME = null;
var INE_AJAX_LOAD = null;
var INE_AJAX_SAVE = null;
var INE_AJAX_UPLOAD = null;
var INE_AJAX_DELETE_PHOTO_GALLERY = null;
var INE_AJAX_DELETE_PHOTO_ARTICLE = null;
