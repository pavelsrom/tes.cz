function ulozit_formular(successFn,progressFn) 
{
        var o = $('#obrazek_input');
        var o_name = o.attr("name");
        
        //aktivuj_loader();
        
        var formData = new FormData();
        formData.append("id_ine_page", INE_PAGE_ID);
        
        if(o.length > 0)
        {
            $.each(o[0].files, function(i, file){ 
                formData.append(o_name/*'file-'+i*/, file);
            });
        }
        
        var data = INE_EL_FORM.find("form").serializeArray();
        
        $.each(data, function(id,v){
            formData.append(v.name.toString(), v.value);
        })
        
        var fotky = $(".ine_fotka"); 
        if(fotky.length > 0)
        {
            fotky.each(function(){
                input = $(this).find("input");
                input_name = input.attr("name");
                input_value = input.val();
                textarea = $(this).find("textarea");
                textarea_name = input.attr("name");
                textarea_value = input.val();

                formData.append(input_name, input_value);
                formData.append(textarea_name, textarea_value);
            })
            
        }
        

        
        $.ajax({
            url: INE_AJAX_SAVE,
            type: 'POST',
            xhr: function() {
                myXhr = $.ajaxSettings.xhr();
                
                if(myXhr.upload){
                    myXhr.upload.addEventListener('progress',function(){}, false);
                }
                
                return myXhr;
            },
            data: formData,
            cache: false,
            dataType: 'json',
            contentType: false,
            processData: false,
            success : function(res) {
                ine_zrus_loader();
                aktivuj_zpravu(res.messages);
                //presmeruj_na_stranku(res.redirect);
                
                //if(successFn) successFn(res);
            }
        });
        
        

}



function smazat_stranku()
{
    /*
    INE_EL_FORM.dialog({
        close: null,
        buttons: null
        });
    */        
    $.post(INE_AJAX_SAVE,{id: INE_PAGE_ID}, function(data){
        ine_zrus_loader();
        aktivuj_zpravu(data.messages);
        
    },'json')

}




/*
function aktivuj_upload_fotky()
{
    if(!podpora_nahravani_obrazku) 
        return false;
        
    var fileselect = $("#obrazek_input");
    $(document).on("select",fileselect,function(){
        // fetch FileList object
		var files = e.target.files || e.dataTransfer.files;

		// process all File objects
		for (var i = 0, f; f = files[i]; i++) {
			//ParseFile(f);
			UploadFile(f);
		}
    })
    
}
*/

// upload JPEG files
function UploadFile(file) {

		var xhr = new XMLHttpRequest();
		if (xhr.upload && file.type == "image/jpeg") {

			// progress bar
            /*
			xhr.upload.addEventListener("progress", function(e) {
				var pc = parseInt(100 - (e.loaded / e.total * 100));
				progress.style.backgroundPosition = pc + "% 0";
			}, false);
            */
            
			// file received/failed
			xhr.onreadystatechange = function(e) {
				if (xhr.readyState == 4) {
					progress.className = (xhr.status == 200 ? "success" : "failure");
				}
			};

			// start upload
			xhr.open("POST", "<?php echo $ajax_gateway_path; ?>page.save", true);
			xhr.setRequestHeader("X_FILENAME", file.name);
			xhr.send(file);

		}

	} 
    

//aktualizuje obsah stranky
function uloz_formular_zalohy_stranky()
{

    //var a = INE_EL_FORM.tabs("option", "active");
    //ids = $(".ine_zaloha").eq(a).attr("id").substring(9);
    //aktivuj_loader();
    
    //id_zalohy = $(".ine_zaloha:visible").attr("id").substring(9);
    id_zalohy = $(".ine_datum_zalohy.ine_active").attr("href").substring(10);

    $.getJSON(INE_AJAX_SAVE,{id_zalohy: id_zalohy, id_stranky: INE_PAGE_ID},function(data){
        $(".perex").html(data.html.perex);
        $(".obsah").html(data.html.obsah);
        ine_zrus_loader();
        aktivuj_zpravu(data.messages);
    });
}




function uloz_formular_preklady_webu()
{

    aktivuj_loader();
    preklady = INE_EL_FORM.find("form").serializeArray();
    $.post(INE_AJAX_SAVE,{preklady: preklady},function(data){
        aktivuj_zpravu(data.messages);
    },'json')
}

function uloz_formular_nastaveni_webu()
{
    //alert("xx");
    //aktivuj_loader();

    nastaveni = INE_EL_FORM.find("form").serializeArray();
    
    $.post(INE_AJAX_SAVE,{nastaveni: nastaveni},function(data){
        ine_zrus_loader();
        aktivuj_zpravu(data.messages);
    },'json')
}
