<?php

session_start();
include_once('../php/config/config.paths.php');
include_once('../php/config/config.web.php');
include_once('../php/config/config.db.php');
include_once('../php/classes/class.db.php');
include_once('../php/config/config.others.php');


//nacteni knihoven
include_once('../php/functions/functions.system.php');
$db = new C_Db();	
$db->Connect();

$relpath = $_GET["relpath"];
$domainpath = $_GET["domainpath"];
$ajax_gateway_path =  $_SESSION['AJAX_GATEWAY_INLINE_EDIT'];
$pageId = $_GET['pageid'];

$jazyk = "cz";
loadTranslations($jazyk);

//nacte js konstanty
include("js/ine.const.js.php");

//nacte funkce pro jadro
include("js/ine.core.js.php");

//nacte funkce pro praci s komponentami ui
if(INLINE_EDIT_PLUS)
    include("js/ine.ui.component.js.php");

//nacte funkce pro ukladani formularu
if(INLINE_EDIT_PLUS)
    include("js/ine.forms.save.js.php");

//nacte funkce zodpovedne za nacitani formularu
if(INLINE_EDIT_PLUS)
    include("js/ine.forms.load.js.php");


//-----------------------------------------------------------
// onload js
//-----------------------------------------------------------

echo "$(function(){";

//nacte html kod pro panel
if(INLINE_EDIT_PLUS)
    include("js/ine.panel.js.php");

//inicializuje ckeditor a ckfinder pro inline editaci
include("js/ine.content.edit.js.php");

//nacte funkci pro rozdeleni uloh pro nacteni formularu
if(INLINE_EDIT_PLUS)
    include("js/ine.forms.js.php");


echo "})";





?>
